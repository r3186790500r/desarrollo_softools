﻿Imports Newtonsoft.Json

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref=" ConfiguracionCatalogos"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class ConfiguracionCatalogos
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ConfiguracionCatalogos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ConfiguracionCatalogos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Catalogo = New Catalogos With {.Codigo = Read(lector, "CATA_Codigo")}
            Secuencia = Read(lector, "Secuencia")
            Nombre = Read(lector, "Nombre")
            Obligatorio = Read(lector, "Obligatorio")
            ParteLlave = Read(lector, "Parte_Llave")
            Tipo = Read(lector, "Tipo")
            LongitudCampo = Read(lector, "Longitud_Campo")
            NumeroDecimales = Read(lector, "Numero_Decimales")
            UtilizarCombo = Read(lector, "Utilizar_Combo")
            CodigoCatalogoCombo = Read(lector, "Codigo_Catalogo_Combo")
            CampoCodigoCombo = Read(lector, "Campo_Codigo_Combo")
            CampoNombreCombo = Read(lector, "Campo_Nombre_Combo")
            Visible = Read(lector, "Visible")
            SugerirCodigo = Read(lector, "Sugerir_Codigo")
        End Sub

        ''' <summary>
        ''' Obtiene o establece el Catalogo de la configuracion catalogos
        ''' </summary>
        <JsonProperty>
        Public Property Catalogo As Catalogos

        ''' <summary>
        ''' Obtiene o establece la Secuencia de la configuracion catalogos
        ''' </summary>
        <JsonProperty>
        Public Property Secuencia As Short

        ''' <summary>
        ''' Obtiene o establece el Nombre de la configuracion catalogos
        ''' </summary>
        <JsonProperty>
        Public Property Nombre As String

        ''' <summary>
        ''' Obtiene o establece si es Obligatorio  en la configuracion catalogos
        ''' </summary>
        <JsonProperty>
        Public Property Obligatorio As Short

        ''' <summary>
        ''' Obtiene o establece el Tipo de la configuracion catalogos
        ''' </summary>
        <JsonProperty>
        Public Property Tipo As Short

        ''' <summary>
        ''' Obtiene o establece la Longitud Campo de la configuracion catalogos
        ''' </summary>
        <JsonProperty>
        Public Property LongitudCampo As Short
        ''' <summary>
        ''' Obtiene o establece el Numero Decimales de la configuracion catalogos
        ''' </summary>
        <JsonProperty>
        Public Property NumeroDecimales As Short

        ''' <summary>
        ''' Obtiene o establece el  Utilizar Combo de la configuracion catalogos
        ''' </summary>
        <JsonProperty>
        Public Property UtilizarCombo As Short

        ''' <summary>
        ''' Obtiene o establece el Codigo Catalogo Combode la configuracion catalogos
        ''' </summary>
        <JsonProperty>
        Public Property CodigoCatalogoCombo As Short

        ''' <summary>
        ''' Obtiene o estableceel Campo Nombre Combo de la configuracion catalogos
        ''' </summary>
        <JsonProperty>
        Public Property CampoNombreCombo As Short

        ''' <summary>
        ''' Obtiene o establece si es Visible en la configuracion catalogos
        ''' </summary>
        <JsonProperty>
        Public Property Visible As Short

        ''' <summary>
        ''' Obtiene o establece el  Sugerir Codigo de la configuracion catalogos
        ''' </summary>
        <JsonProperty>
        Public Property SugerirCodigo As Short
        ''' <summary>
        ''' Obtiene o establece la Parte Llave de la configuracion catalogos
        ''' </summary>
        <JsonProperty>
        Public Property ParteLlave As Short
        ''' <summary>
        ''' Obtiene o establece  Campo Codigo Combo de la configuracion catalogos
        ''' </summary>
        <JsonProperty>
        Public Property CampoCodigoCombo As Short

#Region "Filtros"
        <JsonProperty>
        Public Property CodigoCatalogo As Integer
    End Class
#End Region
End Namespace
