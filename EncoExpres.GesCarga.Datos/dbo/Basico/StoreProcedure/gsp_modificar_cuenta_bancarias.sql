﻿PRINT 'gsp_modificar_cuenta_bancarias'
GO
DROP PROCEDURE gsp_modificar_cuenta_bancarias
GO
CREATE PROCEDURE gsp_modificar_cuenta_bancarias
(
@par_EMPR_Codigo SMALLINT,
@par_Codigo SMALLINT,
@par_BANC_Codigo SMALLINT, 
@par_Numero_Cuenta VARCHAR(30),
@par_CATA_TICB_Codigo NUMERIC,
@par_Nombre VARCHAR(50),
@par_TERC_Codigo NUMERIC,
@par_PLUC_Codigo NUMERIC,
@par_Sobregiro_Autorizado MONEY,
@par_Saldo_Actual MONEY,
@par_Estado SMALLINT,
@par_USUA_Codigo_Modifica SMALLINT 
)
AS
BEGIN
UPDATE Cuenta_Bancarias
SET
BANC_Codigo = @par_BANC_Codigo,
Numero_Cuenta = @par_Numero_Cuenta,
CATA_TICB_Codigo = @par_CATA_TICB_Codigo,
Nombre =  @par_Nombre,
TERC_Codigo  = @par_TERC_Codigo,
PLUC_Codigo = @par_PLUC_Codigo,
Sobregiro_Autorizado = @par_Sobregiro_Autorizado,
Saldo_Actual = @par_Saldo_Actual,
Estado =   @par_Estado,
USUA_Modifica =  @par_USUA_Codigo_Modifica,
Fecha_Modifica = GETDATE()

WHERE
EMPR_Codigo = @par_EMPR_Codigo
AND Codigo = @par_Codigo

SELECT @par_Codigo AS Codigo

END
GO