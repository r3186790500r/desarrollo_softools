﻿EncoExpresApp.controller("ConsultarRutasCtrl", ['$scope', '$timeout', 'RutasFactory', '$linq', 'blockUI', '$routeParams', 'PuestoControlesFactory', 'ValorCatalogosFactory', 'ConceptoGastosFactory',
    'PeajesFactory', 'SitiosCargueDescargueFactory',
    function ($scope, $timeout, RutasFactory, $linq, blockUI, $routeParams, PuestoControlesFactory, ValorCatalogosFactory, ConceptoGastosFactory,
        PeajesFactory, SitiosCargueDescargueFactory) {

        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Despachos' }, { Nombre: 'Rutas' }];

        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        $scope.InputValor = true;
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        $scope.ListadoTipoRuta = [];
        $scope.ListadoSitiosCargueDescargue = [];
        $scope.ListadoSitiosCargueDescargueFiltrado = [];
        $scope.ListaCargueDescargue = [];
        $scope.ListadoEstados = [
            { Nombre: '(TODAS)', Codigo: -1 },
            { Nombre: 'ACTIVA', Codigo: 1 },
            { Nombre: 'INACTIVA', Codigo: 0 }
        ];
        $scope.ModalCargueDescargue = {
            Sitio: '',
            TipoVehiculo: '',
            ValorCargue: '',
            ValorDescargue: '',
            AplicaAnticipo: 0
        }
        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        //Combo Sitios Cargue y Descargue:
        $scope.ListadoSitiosCargueDescargue = SitiosCargueDescargueFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Sync: true }).Datos;
        $scope.ListadoSitiosCargueDescargue.push({ Codigo: -1, Nombre: '(Seleccione un sitio...)' });
        $scope.ModalCargueDescargue.Sitio = $linq.Enumerable().From($scope.ListadoSitiosCargueDescargue).First('$.Codigo==-1');
        //Fin Combo Sitios Cargue y Descargue

        /*Cargar el combo de tipo ruta*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_RUTA } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoRuta = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoRuta = response.data.Datos;
                        $scope.TipoRuta = $scope.ListadoTipoRuta[0]
                    } else {
                        $scope.ListadoTipoRutas = []
                    }
                }
            }, function (response) {
            });

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_RUTAS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };


        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarRutas';
            }
        };
        //-------------------------------------------------GASTOS RUTA--------------------------------------------------------//
        $scope.isEditGastosRuta = false;
        $scope.ListaGastosRuta = [];
        $scope.TMPRuta = {};
        $scope.TMPOrigenDestino = {};
        $scope.ModalGastosRuta = {

            Concepto: '',
            Valor: '',
            TipoVehiculo: '',
        }
        //-- Conceptos Gastos Conductor
        $scope.ListadoConceptosGastos = [];
        ConceptoGastosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_DEFINITIVO, ConceptoSistema: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            for (var i = 0; i < response.data.Datos.length; i++) {
                                if (response.data.Datos[i].ConceptoSistema == 1) {
                                    if (response.data.Datos[i].Codigo == CODIGO_GASTO_COMBUSTIBLE_RENDIMIENTO_GALON || response.data.Datos[i].Codigo == CODIGO_GASTO_VIATICOS) {
                                        $scope.ListadoConceptosGastos.push({ Nombre: response.data.Datos[i].Nombre, Codigo: response.data.Datos[i].Codigo });
                                    }
                                } else {
                                    $scope.ListadoConceptosGastos.push({ Nombre: response.data.Datos[i].Nombre, Codigo: response.data.Datos[i].Codigo });
                                }
                            }
                            $scope.ModalGastosRuta.Concepto = $linq.Enumerable().From($scope.ListadoConceptosGastos).First('$.Codigo == ' + response.data.Datos[0].Codigo);
                        }
                        else {
                            $scope.ListadoConceptosGastos = [];
                        }
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        //-- Catalogo Tipo Vehiculo
        $scope.ListadoTipoVehiculo = [];
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_VEHICULO }, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            if (response.data.Datos[i].Codigo != CODIGO_TIPO_VEHICULO_NO_APLICA) {
                                $scope.ListadoTipoVehiculo.push({ Nombre: response.data.Datos[i].Nombre, Codigo: response.data.Datos[i].Codigo });
                            }
                        }
                        try { $scope.ModalGastosRuta.TipoVehiculo = $linq.Enumerable().From($scope.ListadoTipoVehiculo).First('$.Codigo == ' + response.data.Datos[0].Codigo); } catch (e) { $scope.ModalGastosRuta.TipoVehiculo = $scope.ListadoTipoVehiculo[0]}
                        $scope.ModalCargueDescargue.TipoVehiculo = $linq.Enumerable().From($scope.ListadoTipoVehiculo).First('$.Codigo == ' + response.data.Datos[0].Codigo);
                    }
                    else {
                        $scope.ListadoTipoVehiculo = [];
                    }
                }
            }, function (response) {
            });

        //--Modal Gastos Ruta
        $scope.AbrirGastosRuta = function (item) {
            $scope.TMPRuta = item;
            showModal('modalGastosRuta');
            
            document.getElementById("TipoVehiculo").focus();
            console.log('enfocando')
            $scope.LimpiarmodalGastosRuta();
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: item.Codigo,
                ConsultaLegalizacionGastosRuta: true
            }
            blockUI.delay = 1000;
            RutasFactory.Obtener(filtros).
                then(function (response) {
                    console.log(item);
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos !== undefined) {
                            $scope.ListaGastosRuta = response.data.Datos.LegalizacionGastosRuta;
                            for (var i = 0; i < $scope.ListaGastosRuta.length; i++) {
                                $scope.ListaGastosRuta[i].editable = false;
                                $scope.ListaGastosRuta[i].Valor = MascaraValores($scope.ListaGastosRuta[i].Valor);
                                $scope.ListaGastosRuta[i].AplicaAnticipo = $scope.ListaGastosRuta[i].AplicaAnticipo == 1 ? true : false;
                            }
                            $scope.TMPOrigenDestino = { Ruta: item.Nombre };
                            OrdenarIndicesGastosRuta();
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }

        //--Modal Sitios Cargue Descargue:
        $scope.AbrirCarguesyDescargues = function (item) {
            $scope.MensajesErrorCargueDescargue = [];
            $scope.ListadoSitiosCargueDescargueFiltrado = [];
            $scope.ListadoSitiosCargueDescargue.forEach(itemSitio => {
                if (itemSitio.Ciudad != undefined) {
                    if (itemSitio.Ciudad.Codigo == item.CiudadOrigen.Codigo) {
                        $scope.ListadoSitiosCargueDescargueFiltrado.push(itemSitio);
                    } else if (itemSitio.Ciudad.Codigo == item.CiudadDestino.Codigo) {
                        $scope.ListadoSitiosCargueDescargueFiltrado.push(itemSitio);
                    }
                }
            });
            $scope.ListadoSitiosCargueDescargueFiltrado.push({ Codigo: -1, Nombre: '(Seleccione un sitio...)' });
            $scope.ModalCargueDescargue.Sitio = $linq.Enumerable().From($scope.ListadoSitiosCargueDescargueFiltrado).First('$.Codigo==-1');
            $scope.TMPRuta = item;



            $scope.ModalCargueDescargue = {
                Sitio: $linq.Enumerable().From($scope.ListadoSitiosCargueDescargueFiltrado).First('$.Codigo==-1'),
                TipoVehiculo: $scope.ListadoTipoVehiculo[0],
                ValorCargue: '',
                ValorDescargue: '',
                AplicaAnticipo: 0
            }
            $scope.ListaCargueDescargue = [];

            showModal('modalCarguesDescargues');

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: item.Codigo,

            }
            blockUI.delay = 1000;
            SitiosCargueDescargueFactory.ConsultarSitiosRutas(filtros).
                then(function (response) {

                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListaCargueDescargue = response.data.Datos
                        $scope.ListaCargueDescargue.forEach(item => {
                            item.AplicaAnticipo = item.AplicaAnticipo == 1 ? true : false;
                            item.ValorCargue = MascaraValores(item.ValorCargue);
                            item.ValorDescargue = MascaraValores(item.ValorDescargue);
                        });
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }

        $scope.AdicionarCargueDescargue = function () {
            if (DatosRequeridosCargueDescargue()) {
                $scope.ListaCargueDescargue.push({
                    Sitio: $scope.ModalCargueDescargue.Sitio,
                    TipoVehiculo: $scope.ModalCargueDescargue.TipoVehiculo,
                    ValorCargue: $scope.ModalCargueDescargue.ValorCargue,
                    ValorDescargue: $scope.ModalCargueDescargue.ValorDescargue,
                    AplicaAnticipo: $scope.ModalCargueDescargue.AplicaAnticipo
                });
            }

        }

        function DatosRequeridosCargueDescargue() {
            $scope.MensajesErrorCargueDescargue = [];
            var continuar = true;

            if ($scope.ModalCargueDescargue.Sitio == undefined || $scope.ModalCargueDescargue.Sitio == "" || $scope.ModalCargueDescargue.Sitio == null || $scope.ModalCargueDescargue.Sitio.Codigo == -1) {
                $scope.MensajesErrorCargueDescargue.push('Debe seleccionar un Sitio Cargue/Descargue');
                continuar = false;
            } else {
                if ($scope.ListaCargueDescargue.length > 0) {
                    $scope.ListaCargueDescargue.forEach(item => {
                        if (item.Sitio.Codigo == $scope.ModalCargueDescargue.Sitio.Codigo) {
                            if (item.TipoVehiculo.Codigo == $scope.ModalCargueDescargue.TipoVehiculo.Codigo) {
                                $scope.MensajesErrorCargueDescargue.push('Este Sitio Cargue/Descargue y este Tipo Vehículo ya existen para esta Ruta.');
                                continuar = false;
                            }
                        }
                    });
                }
            }

            if ($scope.ModalCargueDescargue.ValorCargue == undefined || $scope.ModalCargueDescargue.ValorCargue == "" || $scope.ModalCargueDescargue.ValorCargue == null) {
                $scope.MensajesErrorCargueDescargue.push('Debe ingresar un valor de cargue');
                continuar = false;
            }

            if ($scope.ModalCargueDescargue.ValorDescargue == undefined || $scope.ModalCargueDescargue.ValorDescargue == "" || $scope.ModalCargueDescargue.ValorDescargue == null) {
                $scope.MensajesErrorCargueDescargue.push('Debe ingresar un valor de descargue');
                continuar = false;
            }

            return continuar;
        }

        $scope.ConfirmarEliminarCargueDescargue = function (index) {
            $scope.TmpEliminarCargueDescargue = index;
            showModal('modalEliminarCargueDescargue');
        };

        $scope.EliminarCargueDescargue = function (indice) {
            $scope.ListaCargueDescargue.splice(indice, 1);
            closeModal('modalEliminarCargueDescargue');
        };


        $scope.GuardarCargueDescargue = function () {
            var SitiosCargueDescargue = [];

            if (DatosRequeridosListaSitiosCargueDescargue()) {
                $scope.ListaCargueDescargue.forEach(item => {
                    SitiosCargueDescargue.push({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Ruta: { Codigo: $scope.TMPRuta.Codigo },
                        Sitio: { Codigo: item.Sitio.Codigo },
                        TipoVehiculo: { Codigo: item.TipoVehiculo.Codigo },
                        ValorCargue: MascaraNumero(item.ValorCargue),
                        ValorDescargue: MascaraNumero(item.ValorDescargue),
                        AplicaAnticipo: item.AplicaAnticipo == true ? 1 : 0,
                        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                    });
                });
            }

            SitiosCargueDescargueFactory.GuardarSitiosRuta(SitiosCargueDescargue).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        ShowSuccess('Los Sitios de Cargue/Descargue se guardaron satisfactoriamente');
                        closeModal('modalCarguesDescargues')
                    } else {
                        ShowError(response.statusText);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                }
                );

        }

        function DatosRequeridosListaSitiosCargueDescargue() {
            var continuar = true;
            var count = 0;
            $scope.ListaCargueDescargue.forEach(item => {
                if (item.ValorCargue == undefined || item.ValorCargue == null || item.ValorCargue == '' || isNaN(MascaraNumero(item.ValorCargue)) || item.ValorDescargue == undefined || item.ValorDescargue == null || item.ValorDescargue == '' || isNaN(MascaraNumero(item.ValorDescargue))) {
                    count++;
                }
            });

            if (count > 0) {
                $scope.MensajesErrorCargueDescargue.push('Debe ingresar todos los valores en la lista de Sitios Cargue/Descargue');
                continuar = false;
            }

            return continuar;
        }

        //Fin Modal Sitios Cargue Descargue

        $scope.AdicionarGastosRuta = function () {
            if (DatosRequeridosGastosRuta()) {
                if ($scope.isEditGastosRuta == false) {
                    $scope.ListaGastosRuta.push($scope.ModalGastosRuta);
                    OrdenarIndicesGastosRuta();
                }
                else {
                    $scope.ListaGastosRuta[$scope.ModalGastosRuta.indice] = $scope.ModalGastosRuta;
                }
                $scope.LimpiarmodalGastosRuta();
            }

        }
        function DatosRequeridosGastosRuta() {
            $scope.MensajesErrorGastos = [];
            var continuar = true;

            if ($scope.ModalGastosRuta.Concepto == undefined || $scope.ModalGastosRuta.Concepto == "" || $scope.ModalGastosRuta.Concepto == null) {
                $scope.MensajesErrorGastos.push('Debe seleccionar un concepto');
                continuar = false;
            }
            if ($scope.ModalGastosRuta.Valor == undefined || $scope.ModalGastosRuta.Valor == "" || $scope.ModalGastosRuta.Valor == null) {
                $scope.MensajesErrorGastos.push('Debe ingresar valor');
                continuar = false;
            }

            if ($scope.ModalGastosRuta.TipoVehiculo == undefined || $scope.ModalGastosRuta.TipoVehiculo == "" || $scope.ModalGastosRuta.TipoVehiculo == null) {
                $scope.MensajesErrorGastos.push('Debe seleccionar un Tipo de Vehículo');
                continuar = false;
            }

            var indice = $scope.ModalGastosRuta.indice != undefined ? $scope.ModalGastosRuta.indice : -1;

            if (continuar == true) {
                for (var i = 0; i < $scope.ListaGastosRuta.length; i++) {
                    var item = $scope.ListaGastosRuta[i];
                    if ($scope.isEditGastosRuta == true) {
                        if (indice != i) {
                            if (item.Concepto.Codigo == $scope.ModalGastosRuta.Concepto.Codigo && item.TipoVehiculo.Codigo == $scope.ModalGastosRuta.TipoVehiculo.Codigo) {
                                $scope.MensajesErrorGastos.push('El concepto y tipo de vehículo ya estan agregados');
                                continuar = false;
                                break;
                            }
                        }
                    }
                    else {
                        if (item.Concepto.Codigo == $scope.ModalGastosRuta.Concepto.Codigo && item.TipoVehiculo.Codigo == $scope.ModalGastosRuta.TipoVehiculo.Codigo) {
                            $scope.MensajesErrorGastos.push('El concepto y tipo de vehículo ya estan agregados');
                            continuar = false;
                            break;
                        }
                    }
                }
            }

            return continuar;
        }

        $scope.LimpiarmodalGastosRuta = function () {
            for (var i = 0; i < $scope.ListaGastosRuta.length; i++) {
                $scope.ListaGastosRuta[i].editable = false;
            }
            $scope.InputValor = true;
            $scope.ModalGastosRuta = {
                Concepto: '',
                Valor: '',
                TipoVehiculo: '',
            };
            $scope.ModalGastosRuta.Concepto = $linq.Enumerable().From($scope.ListadoConceptosGastos).First('$.Codigo == ' + $scope.ListadoConceptosGastos[0].Codigo);
            $scope.ModalGastosRuta.TipoVehiculo = $linq.Enumerable().From($scope.ListadoTipoVehiculo).First('$.Codigo == ' + $scope.ListadoTipoVehiculo[0].Codigo);
            $scope.MensajesErrorGastos = [];
            $scope.isEditGastosRuta = false;
        }

        function OrdenarIndicesGastosRuta() {
            for (var i = 0; i < $scope.ListaGastosRuta.length; i++) {
                $scope.ListaGastosRuta[i].indice = i;
            }
        }

        $scope.EditarGastoRuta = function (item) {
            $scope.InputValor = false;
            $scope.LimpiarmodalGastosRuta();
            //$scope.ModalGastosRuta = angular.copy(item);
            //$scope.ModalGastosRuta.Concepto = $linq.Enumerable().From($scope.ListadoConceptosGastos).First('$.Codigo == ' + item.Concepto.Codigo);
            //$scope.ModalGastosRuta.Valor = item.Valor;
            //$scope.ModalGastosRuta.TipoVehiculo = $linq.Enumerable().From($scope.ListadoTipoVehiculo).First('$.Codigo == ' + item.TipoVehiculo.Codigo);
            $scope.isEditGastosRuta = true;
            item.Valor = MascaraValores(item.Valor);
            $scope.MaskNumero(); $scope.MaskValores();
        }

        $scope.ActualizarGastoRuta = function (item) {
            $scope.isEditGastosRuta = false;
            $scope.MaskNumero(); $scope.MaskValores();
        }

        //--Eliminar
        $scope.TmpEliminarGastosRuta = {};
        $scope.ConfirmarEliminarGastosRuta = function (item) {
            $scope.TmpEliminarGastosRuta = { Indice: item.indice };
            showModal('modalEliminarGastosRuta');
        };

        $scope.EliminarGastosRuta = function (indice) {
            $scope.ListaGastosRuta.splice(indice, 1);
            OrdenarIndicesGastosRuta();
            closeModal('modalEliminarGastosRuta');
        };

        //--Almacena Informacion
        $scope.GuardarGastosRuta = function () {
            if ($scope.ListaGastosRuta.length > 0) {
                var tmpGastosRuta = [];
                for (var i = 0; i < $scope.ListaGastosRuta.length; i++) {
                    tmpGastosRuta.push({
                        Concepto: $scope.ListaGastosRuta[i].Concepto,
                        Valor: $scope.MaskNumeroGrid($scope.MaskValoresGrid($scope.ListaGastosRuta[i].Valor)),
                        TipoVehiculo: $scope.ListaGastosRuta[i].TipoVehiculo,
                        AplicaAnticipo: $scope.ListaGastosRuta[i].AplicaAnticipo == true ? 1 : 0
                    });
                }
                var GastosRuta = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.TMPRuta.Codigo,
                    LegalizacionGastosRuta: tmpGastosRuta,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                };

                RutasFactory.InsertarLegalizacionGastosRuta(GastosRuta).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            ShowSuccess('Los gastos se agregaron satisfactoriamente')
                            closeModal('modalGastosRuta')
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            } else {

                ShowError('Debe agregar al menos un gasto');
            }
        }
        //-------------------------------------------------GASTOS RUTA--------------------------------------------------------//
        //-------------------------------------------------PEAJES RUTA--------------------------------------------------------//
        $scope.ListadoPeajes = [];
        PeajesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoPeajes = response.data.Datos;
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        $scope.AbrirPeajes = function (item) {

            showModal('modalPeajes')
            document.getElementById("AgregarP").focus();
            $scope.Peajes = '';
            $scope.Modal = item;
            $scope.Modal.PeajeRutas = [];
            $scope.Modal.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: item.Codigo,
                ConsultarPeajes: true
            }
            blockUI.delay = 1000;
            RutasFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos !== undefined) {
                            $scope.Modal.PeajeRutas = response.data.Datos.PeajeRutas;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }

        $scope.AgregarPeaje = function () {
            if ($scope.Peajes !== '' && $scope.Peajes !== undefined && $scope.Peajes !== null) {
                var cont = 0
                for (var i = 0; i < $scope.Modal.PeajeRutas.length; i++) {
                    var item = $scope.Modal.PeajeRutas[i];
                    if (item.Peaje.Codigo == $scope.Peajes.Codigo) {
                        cont++
                        break;
                    }
                }
                if (cont > 0) {
                    ShowError('Este peaje ya se encuentra ingresado, por favor seleccione uno diferente ')
                } else {
                    $scope.Modal.PeajeRutas.push({ Peaje: $scope.Peajes })
                }
            } else {
                ShowError('Por favor seleccione un peaje')

            }
            $scope.Peajes = ''
        }

        $scope.EliminarPeaje = function (indexPeaje, item) {
            $scope.Modal.PeajeRutas.splice(indexPeaje, 1)
        }

        $scope.InsertarPeaje = function () {
            //for (var i = 0; i < $scope.Modal.PeajeRutas.length; i++) {
            //    $scope.Modal.PeajeRutas[i].TiempoEstimado = parseFloat($scope.Modal.PeajeRutas[i].TiempoEstimado);
            //}
            // if ($scope.Modal.PeajeRutas.length > 0) {
            var PeajeRutas = {}
            RutasFactory.InsertarPeaje($scope.Modal).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Los peajes se agregaron satisfactoriamente')
                        closeModal('modalPeajes')
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            // } else {

            // ShowError('Debe agregar al menos un peaje');
            //}
        }

        //-------------------------------------------------PEAJES RUTA--------------------------------------------------------//

        //-----------------------------------------------TRAYECTOS RUTA-------------------------------------------------------//
        $scope.AbrirTrayectos = function (item) {
            $scope.MensajesErrorTrayecto = [];
            showModal('modalTrayectos')
            $scope.Trayecto = '';
            $scope.ModalTrayectos = item;
            $scope.ModalTrayectos.TrayectosRuta = [];
            $scope.ModalTrayectos.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: item.Codigo
            }
            blockUI.delay = 1000;
            RutasFactory.ConsultarTrayectos(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos !== undefined) {
                            $scope.ModalTrayectos.TrayectosRuta = []
                            response.data.Datos.forEach(item => {
                                $scope.ModalTrayectos.TrayectosRuta.push({
                                    RutaTrayecto: {
                                        Codigo: item.RutaTrayecto.Codigo,
                                        Nombre: item.RutaTrayecto.Nombre,
                                        CiudadOrigen: item.RutaTrayecto.CiudadOrigen,
                                        CiudadDestino: item.RutaTrayecto.CiudadDestino
                                    },
                                    Orden: item.Orden,
                                    PorcentajeFlete: item.PorcentajeFlete
                                });
                            });
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }

        $scope.AutocompleteTrayectos = function (value) {
            var filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Nombre: value,
                Estado: ESTADO_ACTIVO,
                Sync: true
            }

            var ResponseTrayectos = []
            ResponseTrayectos = RutasFactory.Consultar(filtros).Datos;

            return ResponseTrayectos
        }

        $scope.AgregarTrayecto = function () {
            if ($scope.Trayecto !== '' && $scope.Trayecto !== undefined && $scope.Trayecto !== null) {
                var cont = 0
                for (var i = 0; i < $scope.ModalTrayectos.TrayectosRuta.length; i++) {
                    var item = $scope.ModalTrayectos.TrayectosRuta[i];
                    if (item.RutaTrayecto.Codigo == $scope.Trayecto.Codigo) {
                        cont++
                        break;
                    }
                }
                if (cont > 0) {
                    ShowError('Este trayecto ya se encuentra ingresado, por favor seleccione uno diferente ')
                } else {
                    $scope.ModalTrayectos.TrayectosRuta.push({
                        RutaTrayecto: {
                            Codigo: $scope.Trayecto.Codigo,
                            Nombre: $scope.Trayecto.Nombre,
                            CiudadOrigen : $scope.Trayecto.CiudadOrigen,
                            CiudadDestino : $scope.Trayecto.CiudadDestino
                        },
                        Orden: $scope.ModalTrayectos.TrayectosRuta.length == 0 ? 0 : $linq.Enumerable().From($scope.ModalTrayectos.TrayectosRuta).Max('$.Orden') + 1,
                        PorcentajeFlete: ''
                    });
                }
            } else {
                ShowError('Por favor seleccione un trayecto')

            }
            $scope.Trayecto = ''
        }
        $scope.EliminarTrayecto = function (index, item) {
            $scope.ModalTrayectos.TrayectosRuta.splice(index, 1)
        }
        $scope.InsertarTrayectos = function () {
            var TrayectosGuardar = []
            if (DatosRequeridosTrayectos()) {
                for (var i = 0; i < $scope.ModalTrayectos.TrayectosRuta.length; i++) {
                    TrayectosGuardar.push({
                        RutaTrayecto: {
                            Codigo: $scope.ModalTrayectos.TrayectosRuta[i].RutaTrayecto.Codigo,
                            Nombre: $scope.ModalTrayectos.TrayectosRuta[i].RutaTrayecto.Nombre
                        },
                        Orden: $scope.ModalTrayectos.TrayectosRuta[i].Orden,
                        PorcentajeFlete: parseFloat($scope.ModalTrayectos.TrayectosRuta[i].PorcentajeFlete).toFixed(4)
                    });
                }

                var EntidadTrayectos = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.ModalTrayectos.Codigo,
                    Trayectos: TrayectosGuardar
                }
                RutasFactory.InsertarTrayectos(EntidadTrayectos).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            ShowSuccess('Los trayectos se agregaron satisfactoriamente')
                            closeModal('modalTrayectos')
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }

        }

        function DatosRequeridosTrayectos() {
            $scope.MensajesErrorTrayecto = [];
            var TotalPorcentajes = 0
            var continuar = true
            for (var i = 0; i < $scope.ModalTrayectos.TrayectosRuta.length;i++) {
                for (var j = 0; j < $scope.ModalTrayectos.TrayectosRuta.length; j++) {
                    if (i != j) {
                        if ($scope.ModalTrayectos.TrayectosRuta[i].Orden == $scope.ModalTrayectos.TrayectosRuta[j].Orden) {
                            continuar = false;
                            $scope.MensajesErrorTrayecto.push('El número de orden en los trayectos no se puede repetir');
                        }
                    }
                }
                TotalPorcentajes = parseFloat(TotalPorcentajes) + parseFloat($scope.ModalTrayectos.TrayectosRuta[i].PorcentajeFlete)
            }
            var primerTrayecto = $linq.Enumerable().From($scope.ModalTrayectos.TrayectosRuta).First('$.Orden==' + $linq.Enumerable().From($scope.ModalTrayectos.TrayectosRuta).Min('$.Orden'))
            var ultimoTrayecto = $linq.Enumerable().From($scope.ModalTrayectos.TrayectosRuta).First('$.Orden==' + $linq.Enumerable().From($scope.ModalTrayectos.TrayectosRuta).Max('$.Orden'))

            if ($scope.ModalTrayectos.CiudadOrigen.Codigo != primerTrayecto.RutaTrayecto.CiudadOrigen.Codigo) {
                continuar = false;
                $scope.MensajesErrorTrayecto.push('La ciudad origen del primer trayecto debe coincidir con la ciudad origen de la ruta');
            }
            if ($scope.ModalTrayectos.CiudadDestino.Codigo != ultimoTrayecto.RutaTrayecto.CiudadDestino.Codigo) {
                continuar = false;
                $scope.MensajesErrorTrayecto.push('La ciudad destino del último trayecto debe coincidir con la ciudad destino de la ruta');
            }

            if (TotalPorcentajes != 100) {
                continuar = false;
                $scope.MensajesErrorTrayecto.push('La sumatoria de porcentajes flete debe ser de 100%');
            }
            return continuar
        }

        $scope.MaskPorcentajeTrayectos = function (option, minDecimales, CantidadDecimales) {
            //return valor.inputmask('Regex', { regex: "^[0-9]{1,6}(\\.\\d{1,2})?$" });

            option = option.toString()
            var numeroarray = []
            var numero = ''
            var contpunto = 0
            for (var i = 0; i < option.length; i++) {
                patron = /[0123456789/.]/
                if (i == 0 && option[i] == '-') {
                    numeroarray.push(option[i])
                }
                else if (patron.test(option[i])) {
                    if (patron.test(option[i]) == ' ') {
                        option[i] = '';
                    } else {
                        if (option[i] == '.') {
                            if (contpunto == 0) {
                                numeroarray.push(option[i])
                            }
                            contpunto++

                        } else {
                            numeroarray.push(option[i])
                        }
                    }
                }
            }

            for (var i = 0; i < numeroarray.length; i++) {
                numero = numero + numeroarray[i]
            }
            if (contpunto > 0) {
                var parteEntera = numero.split('.')[0];
                var parteDecimal = numero.split('.')[1];
                var arrayParteEntera = parteEntera.split('');
                numero = (arrayParteEntera[0] == undefined ? '' : arrayParteEntera[0]) + (arrayParteEntera[1] == undefined ? '' : arrayParteEntera[1]) + '.' + parteDecimal
            } else {
                
                var arrayParteEntera = numero.split('');
                numero = (arrayParteEntera[0] == undefined ? '' : arrayParteEntera[0]) + (arrayParteEntera[1] == undefined ? '' : arrayParteEntera[1])
            }

            if (CantidadDecimales > 0) {
                var arregloNumeros = numero.split('')
                var PsocionPunto = 0
                numero = ''
                for (var i = 0; i < arregloNumeros.length; i++) {
                    if (arregloNumeros[i] == '.') {
                        PsocionPunto = i
                    }
                    if (PsocionPunto > 0) {
                        if (!(i <= (PsocionPunto + CantidadDecimales))) {
                            arregloNumeros[i] = ''
                        }
                    }
                    numero = numero + arregloNumeros[i]
                }

            }
            return numero

        }
        //-----------------------------------------------FIN TRAYECTOS RUTA-------------------------------------------------------//

        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }
        };


        PuestoControlesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoPuestoControles = []
                        response.data.Datos.forEach(function (item) {
                            var registro = item;
                            $scope.ListadoPuestoControles.push(registro);
                        });
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });




        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoRutas = [];
            if ($scope.Buscando) {
                if ($scope.MensajesError.length == 0) {
                    filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CodigoAlterno: $scope.CodigoAlterno,
                        Nombre: $scope.Nombre,
                        Codigo: $scope.Codigo,
                        TipoRuta: $scope.TipoRuta,
                        Estado: $scope.ModalEstado.Codigo,
                        NombreCiudadDestino: $scope.CiudadDestino,
                        NombreCiudadOrigen: $scope.CiudadOrigen,
                        Pagina: $scope.paginaActual,
                        RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                    }
                    blockUI.delay = 1000;

                    console.log('data', filtros)
                    RutasFactory.Consultar(filtros).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.length > 0) {
                                    response.data.Datos.forEach(function (item) {
                                        var registro = item;
                                        $scope.ListadoRutas.push(registro);
                                    });

                                    $scope.ListadoRutas.forEach(function (item) {
                                        $scope.codigo = item.Codigo;
                                    });
                                    $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                    $scope.Buscando = false;
                                    $scope.ResultadoSinRegistros = '';
                                }
                                else {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }
                                var listado = [];
                                response.data.Datos.forEach(function (item) {
                                    listado.push(item);
                                });
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            blockUI.stop();
        }
        $scope.AbrirPuestosControl = function (item) {
            showModal('modalPuestoControl')
            $scope.PuestoControl = ''
            $scope.Modal = item
            $scope.Modal.PuestosControl = []
            $scope.Modal.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: item.Codigo,
                ConsultaPuestosControl: true
            }
            blockUI.delay = 1000;
            RutasFactory.Obtener(filtros).
                then(function (response) {
                   
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos !== undefined) {
                            $scope.Modal.PuestosControl = angular.copy(response.data.Datos.PuestosControl)
                            console.log($scope.Modal.PuestosControl)


                            for (var i = 0; i < $scope.Modal.PuestosControl.length; i++) { 

                                $scope.Modal.PuestosControl[i].HoraPuesto = new Date($scope.Modal.PuestosControl[i].TiempoEstimado).getHours().toString();

                                if (new Date($scope.Modal.PuestosControl[i].TiempoEstimado).getMinutes().toString().length == 1) {
                                    $scope.Modal.PuestosControl[i].HoraPuesto += ':0' + new Date($scope.Modal.PuestosControl[i].TiempoEstimado).getMinutes().toString();
                                } else {
                                    $scope.Modal.PuestosControl[i].HoraPuesto += ':' + new Date($scope.Modal.PuestosControl[i].TiempoEstimado).getMinutes().toString();
                                } 

                            }


                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

            console.log('datos: ', $scope.Modal.PuestosControl )
        }
        $scope.AgregarPuesto = function () {
            if ($scope.PuestoControl !== '' && $scope.PuestoControl !== undefined && $scope.PuestoControl !== null) {
                var cont = 0
                for (var i = 0; i < $scope.Modal.PuestosControl.length; i++) {
                    var item = $scope.Modal.PuestosControl[i]
                    if (item.Codigo == $scope.PuestoControl.Codigo) {
                        cont++
                        break;
                    }
                }
                if (cont > 0) {
                    ShowError('Este puesto de control ya se encuentra ingresado, por favor seleccione uno diferente ')
                } else {
                    $scope.Modal.PuestosControl.push($scope.PuestoControl)
                }
            } else {
                ShowError('Por favor seleccione el puesto de control')

            }
            $scope.PuestoControl = ''

        }
        $scope.EliminarPuestoControl = function (indexTramo, item) {
            $scope.Modal.PuestosControl.splice(indexTramo, 1)
        }
        $scope.InsertarPuestosControl = function () {

            for (var i = 0; i < $scope.Modal.PuestosControl.length; i++) {

                $scope.Modal.PuestosControl[i].TiempoEstimado = new Date();
                var Horasminutos = [];
                Horasminutos = $scope.Modal.PuestosControl[i].HoraPuesto.split(':');
                if (Horasminutos.length > 0) {
                    $scope.Modal.PuestosControl[i].TiempoEstimado.setHours(Horasminutos[0]);
                    $scope.Modal.PuestosControl[i].TiempoEstimado.setMinutes(Horasminutos[1]);
                }


            } 
        
            if ($scope.Modal.PuestosControl.length > 0) {
                RutasFactory.InsertarPuestosControl($scope.Modal).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            ShowSuccess('Los puestos de control se agregaron satisfactoriamente')
                            closeModal('modalPuestoControl')
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            } else {

                ShowError('Debe agregar al menos 1 puesto de control');
            }
        }
        /*------------------------------------------------------------------------------------Eliminar Rutas-----------------------------------------------------------------------*/
        $scope.EliminarRutas = function (codigo, Nombre, CodigoAlterno) {
            $scope.AuxiCodigo = codigo
            $scope.AuxiNombre = Nombre
            $scope.AuxiCodigoAlterno = CodigoAlterno
            $scope.ModalErrorCompleto = ''
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false
            showModal('modalEliminarRutas');
        };

        $scope.Eliminar = function () {
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.AuxiCodigo,
            };

            RutasFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se eliminó la ruta ' + $scope.AuxiNombre);
                        closeModal('modalEliminarRutas');
                        Find();
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    var result = '';
                    result = InternalServerError(response.statusText);
                    showModal('modalMensajeEliminarRutas');
                    $scope.ModalError = 'No se puede eliminar la ruta ' + $scope.AuxiNombre + ' ya que se encuentra relacionada con ' + result;
                    $scope.ModalErrorCompleto = response.statusText;

                });

        };

        $scope.ValidarConcepto = function () {
            if ($scope.ModalGastosRuta.Concepto.Codigo == CODIGO_GASTO_COMBUSTIBLE_RENDIMIENTO_GALON
                || $scope.ModalGastosRuta.Concepto.Codigo == CODIGO_GASTO_VIATICOS) {
                $scope.ModalGastosRuta.Valor = MascaraValores(0);
            }
        }
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /* Obtener parametros*/
        if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
            $scope.Codigo = $routeParams.Codigo;
            Find();
        }
        $scope.CerrarModal = function () {
            closeModal('modalEliminarRutas');
            closeModal('modalMensajeEliminarRutas');
        }

   
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope);
        };
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item)
        };

        $scope.MaskMonedaGrid = function (item) {
            return MascaraMoneda(item)
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope);
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };

        //--------Gestionar Orden Cargue(Cargue y Descargue)-----------//
        $scope.MaskHora = function () {
            MascaraHorasGeneral($scope)
        };
        $scope.MaskHoraGrid = function (item) {
            return MascaraHora(item)
        };

    }]);