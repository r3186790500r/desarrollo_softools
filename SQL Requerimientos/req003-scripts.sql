USE [GESCARGA50]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

DROP INDEX if exists [dbo].[Vehiculos].idx_Sucursal;
GO

ALTER TABLE [dbo].[Vehiculos]
DROP COLUMN if exists [Sucursal];
GO

ALTER TABLE [dbo].[Vehiculos]
ADD [Sucursal] varchar(15);
GO

CREATE UNIQUE NONCLUSTERED INDEX idx_Sucursal
ON [dbo].[Vehiculos]([Sucursal])
WHERE [Sucursal] IS NOT NULL AND [Estado]=1;
GO

ALTER PROCEDURE [dbo].[gsp_insertar_vehiculos]
	@par_EMPR_Codigo [smallint],
	@Codigo [numeric](18, 0) = 0,
	@par_Placa [varchar](20),
	@par_Codigo_Alterno [varchar](20) = null,
	@par_Emergencias [smallint] = null,
	@par_SEMI_Codigo [numeric](18, 0) = null,
	@par_TERC_Codigo_Propietario [numeric](18, 0) = null,
	@par_TERC_Codigo_Tenedor [numeric](18, 0) = null,
	@par_TERC_Codigo_Conductor [numeric](18, 0) = null,
	@par_TERC_Codigo_Afiliador [numeric](18, 0) = null,
	@par_COVE_Codigo [numeric](18, 0) = null,
	@par_MAVE_Codigo [numeric](18, 0) = null,
	@par_LIVE_Codigo [numeric](18, 0) = null,
	@par_Modelo [smallint] = null,
	@par_Modelo_Repotenciado [smallint] = null,
	@par_CATA_TIVE_Codigo [numeric](18, 0) = null,
	@par_Cilindraje [numeric](18, 0) = null,
	@par_CATA_TICA_Codigo [numeric](18, 0) = null,
	@par_Numero_Ejes [smallint] = null,
	@par_Numero_Motor [varchar](50) = null,
	@par_Numero_Serie [varchar](50) = null,
	@par_Peso_Bruto [numeric](18, 0) = null,
	@par_Kilometraje [numeric](18, 0) = null,
	@par_Fecha_Actuliza_Kilometraje [datetime] = null,
	@par_ESSE_Numero [numeric](18, 0) = null,
	@par_Fecha_Ultimo_Cargue [datetime] = null,
	@par_Estado [smallint] = null,
	@par_USUA_Codigo_Crea [smallint] = null,
	@par_CATA_TIDV_Codigo [numeric](18, 0) = null,
	@par_Tarjeta_Propiedad [varchar](50) = null,
	@par_Capacidad [numeric](18, 2) = null,
	@par_Capacidad_Galones [numeric](18, 2) = null,
	@par_Peso_Tara [numeric](18, 0) = null,
	@par_TERC_Codigo_Proveedor_GPS [numeric](18, 0) = null,
	@par_Usuario_GPS [varchar](50) = null,
	@par_Clave_GPS [varchar](50) = null,
	@par_TelefonoGPS [numeric](18, 0) = null,
	@par_CATA_CAIV_Codigo [numeric](18, 0) = null,
	@par_Justificacion_Bloqueo [varchar](150) = null,
	@par_TERC_Codigo_Aseguradora_SOAT [numeric](18, 0) = null,
	@par_Referencia_SOAT [varchar](50) = null,
	@par_Fecha_Vencimiento_SOAT [date] = null,
	@par_URL_GPS [varchar](200) = null,
	@par_Identificador_GPS [varchar](50) = null,
	@par_CapacidadVolumen [numeric](18, 0) = null,
	@par_Fecha_Vinculacion [datetime] = null,
	@par_Fecha_Inactivacion [datetime] = null,
	@par_CATA_TICO_Codigo [numeric](18, 0) = null,
	@par_Sucursal [varchar](50) = null
WITH EXECUTE AS CALLER
AS
BEGIN                              
IF EXISTS(SELECT * FROM dbo.Vehiculos where placa = @par_Placa AND EMPR_Codigo = @par_EMPR_Codigo)                              
BEGIN                              
 select 1 as Codigo                              
END                              
ELSE                              
BEGIN                              
 EXEC gsp_generar_consecutivo  @par_EMPR_Codigo, 20, 0, @Codigo OUTPUT                                
 INSERT INTO Vehiculos                              
      (EMPR_Codigo                              
      ,Codigo                              
      ,Placa                              
      ,Codigo_Alterno                              
                          
      ,SEMI_Codigo                              
      ,TERC_Codigo_Propietario                              
      ,TERC_Codigo_Tenedor  
      ,TERC_Codigo_Conductor            
      ,TERC_Codigo_Afiliador                              
      ,COVE_Codigo                              
      ,MAVE_Codigo                
      ,LIVE_Codigo                              
      ,Modelo                              
      ,Modelo_Repotenciado                              
      ,CATA_TIVE_Codigo                              
      ,Cilindraje                              
      ,CATA_TICA_Codigo                              
      ,Numero_Ejes                              
      ,Numero_Motor                              
      ,Numero_Serie                              
  ,Peso_Bruto                              
      ,Kilometraje                              
      ,Fecha_Actuliza_Kilometraje                              
      ,ESSE_Numero               
      ,Fecha_Ultimo_Cargue                              
      ,Estado                              
                                    
      ,USUA_Codigo_Crea                              
      ,Fecha_Crea            
   ,Fecha_Modifica                    
      ,CATA_TIDV_Codigo                              
      ,Tarjeta_Propiedad                              
      ,Capacidad_Kilos                              
      ,Capacidad_Galones                              
,Peso_Tara                              
      ,TERC_Codigo_Proveedor_GPS                              
      ,Usuario_GPS                              
      ,Clave_GPS                              
      ,CATA_CAIV_Codigo                              
      ,Justificacion_Bloqueo                              
      ,Telefono_GPS                              
  ,TERC_Codigo_Aseguradora_SOAT                         
 ,Referencia_SOAT                          
 ,Fecha_Vencimiento_SOAT                         
 , URL_GPS                      
 ,Identificador_GPS              
 ,Reportar_RNDC      
 ,Capacidad_Volumen
 ,CATA_TICO_Codigo
 ,Sucursal
      )                              
   VALUES                              
      (@par_EMPR_Codigo                              
      ,@Codigo                              
      ,@par_Placa                              
      ,ISNULL(@par_Codigo_Alterno,'')                              
                          
      ,ISNULL(@par_SEMI_Codigo,0)                              
      ,ISNULL(@par_TERC_Codigo_Propietario,0)                              
      ,ISNULL(@par_TERC_Codigo_Tenedor,0)                              
    ,ISNULL(@par_TERC_Codigo_Conductor,0)                              
      ,ISNULL(@par_TERC_Codigo_Afiliador,0)                              
      ,ISNULL(@par_COVE_Codigo,0)                              
      ,ISNULL(@par_MAVE_Codigo,0)                              
      ,ISNULL(@par_LIVE_Codigo,0)                              
      ,ISNULL(@par_Modelo,0)                              
      ,@par_Modelo_Repotenciado                              
      ,ISNULL(@par_CATA_TIVE_Codigo,2200)                              
      ,ISNULL(@par_Cilindraje,0)                              
      ,ISNULL(@par_CATA_TICA_Codigo,2300)                              
      ,ISNULL(@par_Numero_Ejes,0)                              
      ,ISNULL(@par_Numero_Motor,'')                              
      ,ISNULL(@par_Numero_Serie,0)                              
      ,ISNULL(@par_Peso_Bruto,0)                              
      ,ISNULL(@par_Kilometraje,0)                              
      ,ISNULL(@par_Fecha_Actuliza_Kilometraje,'01/01/1900')                              
      ,ISNULL(@par_ESSE_Numero,0)            ,ISNULL(@par_Fecha_Ultimo_Cargue,'01/01/1900')                              
      ,ISNULL(@par_Estado,0)                              
                                    
      ,ISNULL(@par_USUA_Codigo_Crea,0)                              
      ,ISNULL(@par_Fecha_Vinculacion,GETDATE())      
   ,@par_Fecha_Inactivacion      
      ,ISNULL(@par_CATA_TIDV_Codigo,2100)                              
      ,isnull(@par_Tarjeta_Propiedad,'')                              
      ,isnull(@par_Capacidad,0)                              
      ,@par_Capacidad_Galones                              
      ,@par_Peso_Tara                              
      ,ISNULL(@par_TERC_Codigo_Proveedor_GPS,0)                              
      ,@par_Usuario_GPS                              
      ,@par_Clave_GPS                              
      ,ISNULL(@par_CATA_CAIV_Codigo,2400)                              
      ,@par_Justificacion_Bloqueo                              
   ,@par_TelefonoGPS                              
        , @par_TERC_Codigo_Aseguradora_SOAT                        
  ,@par_Referencia_SOAT                        
  ,@par_Fecha_Vencimiento_SOAT                        
  ,@par_URL_GPS                      
  ,@par_Identificador_GPS             
  ,1  ,        
  ISNULL(@par_CapacidadVolumen,0)    
  ,@par_CATA_TICO_Codigo
  ,@par_Sucursal
      )                    
                    
INSERT INTO Plan_Puntos_Vehiculos                    
(                    
EMPR_Codigo,                  
VEHI_Codigo,                    
Fecha,                    
CATA_CFTR_Codigo,                    
Total_Puntos,                    
Fecha_Actualiza_Puntos,--NULL                    
Estado,                    
USUA_Codigo_Crea,                    
Fecha_Crea,                    
USUA_Codigo_Modifica,--NULL                    
Fecha_Modifica--NULL                    
)                    
VALUES (                    
@par_EMPR_Codigo,                    
@Codigo,                    
GETDATE(),                    
18801,                    
0,                    
NULL,--NULL                    
1,                    
@par_USUA_Codigo_Crea,                    
GETDATE(),                    
NULL,--NULL                    
NULL--NULL                    
)                    
                
IF NOT EXISTS(SELECT * FROM Usuarios WHERE TERC_Codigo_Conductor =@par_TERC_Codigo_Conductor AND EMPR_Codigo = @par_EMPR_Codigo ) AND EXISTS(SELECT * FROM Validacion_Procesos WHERE EMPR_Codigo = @par_EMPR_Codigo and PROC_Codigo = 35 AND Estado = 1)      
  
    
      
        
          
BEGIN                
 INSERT INTO Usuarios                
 (                
 EMPR_Codigo,                
 Codigo_Usuario,                
 Codigo_Alterno,                
 Nombre,                
 Descripcion,                
 Habilitado,                
 Clave,                
 Dias_Cambio_Clave,                
 Manager,                
 Login,                
 CATA_APLI_Codigo,                
 TERC_Codigo_Empleado,                
 TERC_Codigo_Cliente,                
 TERC_Codigo_Conductor,                
 TERC_Codigo_Proveedor,                
 TERC_Codigo_Transportador,                
 Fecha_Ultimo_Ingreso,                
 Intentos_Ingreso,                
 Fecha_Ultimo_Cambio_Clave,                
 Mensaje_Banner,                
 Fecha_Ultima_Actividad                
 )                
 SELECT  EMPR_Codigo,                
 Numero_Identificacion,                
 Numero_Identificacion,                
 Nombre +' '+ Apellido1+' '+ Apellido2,                
 'CONDUCTOR',                
 1,                
 Numero_Identificacion,                
 360,                
 0,                
 0,                
 203,                
 0,                
 0,                
 Codigo,                
 0,                
 NULL,                
 NULL,                
 0,                
 NULL,                
 '',                
 NULL                
 FROM Terceros WHERE Codigo =@par_TERC_Codigo_Conductor AND EMPR_Codigo = @par_EMPR_Codigo                
END               
INSERT INTO Vehiculo_Novedades (    
 EMPR_Codigo,    
 VEHI_Codigo,    
 CATA_NOVE_Codigo,    
 Estado,    
 Observaciones,    
 USUA_Codigo_Crea,    
 Fecha_Crea    
 )    
 VALUES(    
 @par_EMPR_Codigo,    
 @Codigo,    
 22307,--Creaci�n Registro    
 1,    
 'Creaci�n Veh�culo',    
@par_USUA_Codigo_Crea,    
GETDATE()    
 )    
                
 select @Codigo as Codigo                              
 END                           
END    
GO

ALTER PROCEDURE [dbo].[gsp_obtener_vehiculo]
	@par_EMPR_Codigo [smallint],
	@par_Codigo [numeric](18, 0) = null,
	@par_Placa [varchar](20) = null,
	@par_Emergencias [smallint] = 0,
	@par_Estado [smallint] = null,
	@par_Sucursal [varchar](50) = null
WITH EXECUTE AS CALLER
AS
BEGIN                  
                  
SELECT                   
1 AS Obtener,                  
VEHI.EMPR_Codigo,                  
VEHI.Codigo,                  
VEHI.Placa,                  
VEHI.Codigo_Alterno,                  
VEHI.SEMI_Codigo,            
SEMI.Placa as PlacaSemirremolque,                 
VEHI.TERC_Codigo_Propietario,                  
VEHI.TERC_Codigo_Tenedor,            
ISNULL(TENE.Razon_Social,'') + ' ' + ISNULL(TENE.Nombre,'') + ' ' + ISNULL(TENE.Apellido1,'') + ' ' + ISNULL(TENE.Apellido2,'') As NombreTenedor,                 
VEHI.TERC_Codigo_Conductor,             
ISNULL(COND.Razon_Social,'') + ' ' + ISNULL(COND.Nombre,'') + ' ' + ISNULL(COND.Apellido1,'') + ' ' + ISNULL(COND.Apellido2,'') As NombreConductor,             
COND.Numero_Identificacion As IdentificacionConductor,            
ISNULL(COND.Celulares, '') AS Celulares,              
VEHI.TERC_Codigo_Afiliador,                  
VEHI.COVE_Codigo,                  
VEHI.MAVE_Codigo,                  
VEHI.LIVE_Codigo,                  
VEHI.Modelo,                  
ISNULL(VEHI.Modelo_Repotenciado,0) AS Modelo_Repotenciado,                  
VEHI.CATA_TIVE_Codigo,                  
TIVE.Campo1 AS TipoVehiculo,                  
VEHI.Cilindraje,                  
VEHI.CATA_TICA_Codigo,                  
VEHI.Numero_Ejes,                  
VEHI.Numero_Motor,                  
VEHI.Numero_Serie,                  
VEHI.Peso_Bruto,                  
VEHI.Kilometraje,                  
VEHI.Fecha_Actuliza_Kilometraje,                  
VEHI.ESSE_Numero,                  
Fecha_Ultimo_Cargue,                  
VEHI.Estado,                  
'' AS EstadoVehiculo,                  
ISNULL(VEHI.Tarjeta_Propiedad,0) AS Tarjeta_Propiedad,                  
ISNULL(VEHI.Capacidad_Kilos,0) AS Capacidad,                  
ISNULL(VEHI.Capacidad_Galones,0) AS CapacidadGalones,                  
ISNULL(VEHI.Peso_Tara,0) AS Peso_Tara,                  
ISNULL(VEHI.TERC_Codigo_Proveedor_GPS,0) AS TERC_Codigo_Proveedor_GPS,                  
ISNULL(VEHI.Usuario_GPS,'') AS Usuario_GPS,                  
ISNULL(VEHI.Clave_GPS,'') AS Clave_GPS,                  
ISNULL(VEHI.CATA_CAIV_Codigo,2400) AS CATA_CAIV_Codigo,                  
ISNULL(VEHI.Justificacion_Bloqueo,'') AS Justificacion_Bloqueo,                  
ISNULL(VEHI.Telefono_GPS,0) AS TelefonoGPS,                  
ISNULL(VEHI.CATA_TIDV_Codigo,2100) AS CATA_TIDV_Codigo,             
VEHI.TERC_Codigo_Aseguradora_SOAT,            
VEHI.Referencia_SOAT,            
VEHI.Fecha_Vencimiento_SOAT,        
VEHI.URL_GPS  ,      
VEHI.Identificador_GPS  ,    
ISNULL(VEHI.Capacidad_Volumen,0) As Capacidad_Volumen ,
VEHI.CATA_TICO_Codigo,
VEHI.Sucursal
FROM                   
Vehiculos AS VEHI          
          
LEFT JOIN Semirremolques SEMI ON                    
VEHI.EMPR_Codigo = SEMI.EMPR_Codigo                      
AND VEHI.SEMI_Codigo = SEMI.Codigo                  
          
LEFT JOIN Valor_Catalogos TIVE ON                    
VEHI.EMPR_Codigo = TIVE.EMPR_Codigo                      
AND VEHI.CATA_TIVE_Codigo = TIVE.Codigo            
          
LEFT JOIN Terceros TENE ON                    
VEHI.EMPR_Codigo = TENE.EMPR_Codigo                      
AND VEHI.TERC_Codigo_Tenedor = TENE.Codigo            
          
LEFT JOIN Terceros COND ON                    
VEHI.EMPR_Codigo = COND.EMPR_Codigo                      
AND VEHI.TERC_Codigo_Conductor = COND.Codigo            
                  
WHERE VEHI.EMPR_Codigo = @par_EMPR_Codigo                  
AND VEHI.Codigo = ISNULL(@par_Codigo,VEHI.Codigo)                  
AND VEHI.Placa = ISNULL (@par_Placa,VEHI.Placa)
AND VEHI.Estado = ISNULL (@par_Estado,VEHI.Estado)
AND (
    (VEHI.Sucursal is null AND @par_Sucursal is null) 
    OR 
    (VEHI.Sucursal is not null AND VEHI.Sucursal = ISNULL(@par_Sucursal,VEHI.Sucursal))
    )
                      
END                  
GO

ALTER PROCEDURE [dbo].[gsp_modificar_vehiculos]
	@par_EMPR_Codigo [smallint],
	@Codigo [numeric](18, 0),
	@par_Codigo_Alterno [varchar](20) = null,
	@par_SEMI_Codigo [numeric](18, 0) = null,
	@par_TERC_Codigo_Propietario [numeric](18, 0) = null,
	@par_TERC_Codigo_Tenedor [numeric](18, 0) = null,
	@par_TERC_Codigo_Conductor [numeric](18, 0) = null,
	@par_TERC_Codigo_Afiliador [numeric](18, 0) = null,
	@par_COVE_Codigo [numeric](18, 0) = null,
	@par_MAVE_Codigo [numeric](18, 0) = null,
	@par_LIVE_Codigo [numeric](18, 0) = null,
	@par_Modelo [smallint] = null,
	@par_Modelo_Repotenciado [smallint] = null,
	@par_CATA_TIVE_Codigo [numeric](18, 0) = null,
	@par_Cilindraje [numeric](18, 2) = null,
	@par_CATA_TICA_Codigo [numeric](18, 0) = null,
	@par_Numero_Ejes [smallint] = null,
	@par_Numero_Motor [varchar](50) = null,
	@par_Numero_Serie [varchar](50) = null,
	@par_Peso_Bruto [numeric](18, 2) = null,
	@par_Kilometraje [numeric](18, 0) = null,
	@par_Fecha_Actuliza_Kilometraje [datetime] = null,
	@par_ESSE_Numero [numeric](18, 0) = null,
	@par_Fecha_Ultimo_Cargue [datetime] = null,
	@par_Estado [smallint] = null,
	@par_CATA_ESVE_Codigo [numeric](18, 0) = null,
	@par_USUA_Codigo_Crea [smallint] = null,
	@par_CATA_TIDV_Codigo [numeric](18, 0) = null,
	@par_Tarjeta_Propiedad [varchar](50) = null,
	@par_Capacidad [numeric](18, 2) = null,
	@par_Capacidad_Galones [numeric](18, 2) = null,
	@par_Peso_Tara [numeric](18, 2) = null,
	@par_TERC_Codigo_Proveedor_GPS [numeric](18, 0) = null,
	@par_Usuario_GPS [varchar](50) = null,
	@par_Clave_GPS [varchar](50) = null,
	@par_TelefonoGPS [numeric](18, 0) = null,
	@par_CATA_CAIV_Codigo [numeric](18, 0) = null,
	@par_Justificacion_Bloqueo [varchar](150) = null,
	@par_Foto [image] = null,
	@par_TERC_Codigo_Aseguradora_SOAT [numeric](18, 0) = null,
	@par_Referencia_SOAT [varchar](50) = null,
	@par_Fecha_Vencimiento_SOAT [date] = null,
	@par_URL_GPS [varchar](200) = null,
	@par_Identificador_GPS [varchar](50) = null,
	@par_CapacidadVolumen [numeric](18, 0) = null,
	@par_CATA_TICO_Codigo [numeric](18, 0) = null,
	@par_Sucursal [varchar](50) = null
WITH EXECUTE AS CALLER
AS
BEGIN                      
                      
                      
update Vehiculos set                      
           Codigo_Alterno = ISNULL(@par_Codigo_Alterno,'')                      
           ,SEMI_Codigo = ISNULL(@par_SEMI_Codigo,0)                      
           ,TERC_Codigo_Propietario = ISNULL(@par_TERC_Codigo_Propietario,0)                      
           ,TERC_Codigo_Tenedor =ISNULL(@par_TERC_Codigo_Tenedor,0)                      
           ,TERC_Codigo_Conductor = ISNULL(@par_TERC_Codigo_Conductor,0)                      
           ,TERC_Codigo_Afiliador = ISNULL(@par_TERC_Codigo_Afiliador,0)                      
           ,COVE_Codigo = ISNULL(@par_COVE_Codigo,0)                      
           ,MAVE_Codigo = ISNULL(@par_MAVE_Codigo,0)                      
           ,LIVE_Codigo = ISNULL(@par_LIVE_Codigo,0)                      
           ,Modelo = ISNULL(@par_Modelo,0)                      
           ,Modelo_Repotenciado = @par_Modelo_Repotenciado                      
           ,CATA_TIVE_Codigo = ISNULL(@par_CATA_TIVE_Codigo,2200)                      
           ,Cilindraje = ISNULL(@par_Cilindraje,0)                      
           ,CATA_TICA_Codigo = ISNULL(@par_CATA_TICA_Codigo,2300)                      
           ,Numero_Ejes = ISNULL(@par_Numero_Ejes,0)                      
           ,Numero_Motor = ISNULL(@par_Numero_Motor,'')                      
           ,Numero_Serie = ISNULL(@par_Numero_Serie,0)                      
           ,Peso_Bruto = ISNULL(@par_Peso_Bruto,0)                      
           ,Kilometraje = ISNULL(@par_Kilometraje,0)                      
           ,Fecha_Actuliza_Kilometraje = ISNULL(@par_Fecha_Actuliza_Kilometraje,'01/01/1900')                      
           ,ESSE_Numero = ISNULL(@par_ESSE_Numero,0)                      
           ,Fecha_Ultimo_Cargue =   ISNULL(@par_Fecha_Ultimo_Cargue,'01/01/1900')                      
           ,Estado = ISNULL(@par_Estado,0)                      
                                
           ,USUA_Codigo_Modifica = ISNULL(@par_USUA_Codigo_Crea,0)                      
           ,Fecha_Modifica = GETDATE()                      
           ,CATA_TIDV_Codigo = ISNULL(@par_CATA_TIDV_Codigo,2100)                   
           ,Tarjeta_Propiedad = Isnull(@par_Tarjeta_Propiedad,'')                      
           ,Capacidad_Kilos = Isnull(@par_Capacidad,0)                      
     ,Capacidad_Galones = @par_Capacidad_Galones                      
           ,Peso_Tara = @par_Peso_Tara                      
           ,TERC_Codigo_Proveedor_GPS = ISNULL(@par_TERC_Codigo_Proveedor_GPS,0)                      
           ,Usuario_GPS = @par_Usuario_GPS                      
           ,Clave_GPS = @par_Clave_GPS                      
         ,CATA_CAIV_Codigo = isnull(@par_CATA_CAIV_Codigo,2400)                      
           ,Justificacion_Bloqueo = @par_Justificacion_Bloqueo                      
           ,Telefono_GPS = @par_TelefonoGPS                      
           ,TERC_Codigo_Aseguradora_SOAT = @par_TERC_Codigo_Aseguradora_SOAT                      
           ,Referencia_SOAT = @par_Referencia_SOAT                      
           ,Fecha_Vencimiento_SOAT = @par_Fecha_Vencimiento_SOAT                     
			,URL_GPS = @par_URL_GPS                
		   ,Identificador_GPS = @par_Identificador_GPS         
		   ,Reportar_RNDC = 1    
			,Capacidad_Volumen = ISNULL(@par_CapacidadVolumen,0)
			,CATA_TICO_Codigo = @par_CATA_TICO_Codigo
			,Sucursal = @par_Sucursal
     where EMPR_Codigo = @par_EMPR_Codigo                      
     AND Codigo = @Codigo                
            
IF NOT EXISTS(SELECT * FROM Usuarios WHERE TERC_Codigo_Conductor =@par_TERC_Codigo_Conductor AND EMPR_Codigo = @par_EMPR_Codigo ) AND EXISTS(SELECT * FROM Validacion_Procesos WHERE EMPR_Codigo = @par_EMPR_Codigo and PROC_Codigo = 35 AND Estado = 1)       
  
    
     
BEGIN            
 INSERT INTO Usuarios            
 (            
 EMPR_Codigo,            
 Codigo_Usuario,            
 Codigo_Alterno,            
 Nombre,            
 Descripcion,            
 Habilitado,            
 Clave,            
 Dias_Cambio_Clave,            
 Manager,            
 Login,            
 CATA_APLI_Codigo,            
 TERC_Codigo_Empleado,            
 TERC_Codigo_Cliente,            
 TERC_Codigo_Conductor,            
 TERC_Codigo_Proveedor,            
 TERC_Codigo_Transportador,            
 Fecha_Ultimo_Ingreso,            
 Intentos_Ingreso,            
 Fecha_Ultimo_Cambio_Clave,            
 Mensaje_Banner,            
 Fecha_Ultima_Actividad            
 )            
 SELECT  EMPR_Codigo,            
 Numero_Identificacion,            
 Numero_Identificacion,            
 Nombre +' '+ Apellido1+' '+ Apellido2,            
 'CONDUCTOR',            
 1,            
 Numero_Identificacion,            
 360,            
 0,            
 0,            
 203,            
 0,            
 0,            
 Codigo,            
 0,            
 NULL,            
 NULL,            
 0,            
 NULL,            
 '',            
 NULL            
 FROM Terceros WHERE Codigo =@par_TERC_Codigo_Conductor AND EMPR_Codigo = @par_EMPR_Codigo            
END            
select @Codigo as Codigo                      
END                      
GO

