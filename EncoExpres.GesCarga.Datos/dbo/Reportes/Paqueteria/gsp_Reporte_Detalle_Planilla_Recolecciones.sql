﻿PRINT 'gsp_Reporte_Detalle_Planilla_Recolecciones'
GO
DROP PROCEDURE gsp_Reporte_Detalle_Planilla_Recolecciones
GO 
CREATE PROCEDURE gsp_Reporte_Detalle_Planilla_Recolecciones
(
	@par_EMPR_Codigo NUMERIC,
	@par_ENPD_Numero NUMERIC
)
AS 
BEGIN
	SELECT 
	EREC.Numero_Documento,
	EREC.Fecha,
	EREC.Fecha_Recoleccion,
	ISNULL(CLIE.Razon_Social,'')+' '+ISNULL(CLIE.Nombre,'') +' '+ISNULL(CLIE.Apellido1,'')+' '+ISNULL(CLIE.Apellido2,'') AS NombreCliente,
	EREC.Nombre_Contacto,
	CIUD.Nombre AS CIUD_Nombre,
	EREC.Barrio,
	EREC.Direccion,
	EREC.Telefonos,
	EREC.Mercancia,
	ISNULL(UEPT.Descripcion, '') AS UnidadEmpaque,
	EREC.Cantidad,
	EREC.Peso,
	EREC.Observaciones

	FROM Detalle_Planilla_Recolecciones DEPR, Encabezado_Recolecciones EREC

	LEFT JOIN Terceros CLIE ON
	EREC.EMPR_Codigo = CLIE.EMPR_Codigo
	AND EREC.TERC_Codigo_Cliente = CLIE.Codigo

	LEFT JOIN Ciudades CIUD ON
	EREC.EMPR_Codigo = CIUD.EMPR_Codigo
	AND EREC.CIUD_Codigo = CIUD.Codigo

	LEFT JOIN Unidad_Empaque_Producto_Transportados UEPT ON
	EREC.EMPR_Codigo = UEPT.EMPR_Codigo
	AND EREC.UEPT_Codigo = UEPT.Codigo

	WHERE DEPR.EMPR_Codigo = @par_EMPR_Codigo
	and DEPR.ENPR_Numero = @par_ENPD_Numero
	AND DEPR.EREC_Numero = EREC.Numero
END
GO