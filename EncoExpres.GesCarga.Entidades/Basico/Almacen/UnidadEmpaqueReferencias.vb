﻿Imports Newtonsoft.Json

Namespace Basico.Almacen

    ''' <summary>
    ''' Clase <see cref=" UnidadEmpaqueReferencias"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class UnidadEmpaqueReferencias
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="UnidadEmpaqueReferencias"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="UnidadEmpaqueReferencias"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            NombreCorto = Read(lector, "Nombre_Corto")
            Nombre = Read(lector, "Nombre")
            Estado = Read(lector, "Estado")
            Obtener = Read(lector, "Obtener")

            If Obtener = 0 Then
                TotalRegistros = Read(lector, "TotalRegistros")
            End If
        End Sub

        <JsonProperty>
        Public Property NombreCorto As String
        <JsonProperty>
        Public Property Nombre As String
    End Class
End Namespace
