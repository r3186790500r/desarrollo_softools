﻿PRINT 'gsp_consultar_orden_servicio_documentos'
GO
DROP PROCEDURE gsp_consultar_orden_servicio_documentos
GO
CREATE PROCEDURE gsp_consultar_orden_servicio_documentos 
(    
@par_EMPR_Codigo SMALLINT,    
@par_Numero NUMERIC,
@par_TIDO_Codigo NUMERIC    
)    
AS    
BEGIN    
    
SELECT     
EMPR_Codigo,   
Numero,       
TIDO_Codigo,
CDGD_Codigo,    
ISNULL(Referencia, 0) AS Referencia,    
ISNULL(Emisor, 0) AS Emisor,    
ISNULL(Fecha_Emision, '') AS Fecha_Emision,    
ISNULL(Fecha_Vence, '') AS Fecha_Vence,    
ISNULL(Archivo, 0) AS Archivo,   
ISNULL(Nombre_Documento, '') AS Nombre_Documento,    
ISNULL(Extension_Documento, '') AS Extension_Documento  
    
FROM    
Gestion_Documental_Documentos    
    
WHERE    
EMPR_Codigo = @par_EMPR_Codigo    
AND Numero = @par_Numero    
AND TIDO_Codigo = @par_TIDO_Codigo   
END    
GO