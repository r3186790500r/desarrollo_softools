﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.Almacen

Namespace Basico.Almacen

    ''' <summary>
    ''' Clase <see cref="RepositorioReferenciaProveedores"/>
    ''' </summary>
    Public NotInheritable Class RepositorioReferenciaProveedores
        Inherits RepositorioBase(Of ReferenciaProveedores)

        Public Overrides Function Consultar(filtro As ReferenciaProveedores) As IEnumerable(Of ReferenciaProveedores)
            Dim lista As New List(Of ReferenciaProveedores)
            Dim item As ReferenciaProveedores

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If
                If Not String.IsNullOrWhiteSpace(filtro.CodigoAlterno) Then
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", filtro.CodigoAlterno)
                End If
                If Not String.IsNullOrWhiteSpace(filtro.ReferenciaProveedor) Then
                    conexion.AgregarParametroSQL("@par_Referencia_Proveedor", filtro.ReferenciaProveedor)
                End If
                If Not String.IsNullOrWhiteSpace(filtro.NombreProveedor) Then
                    conexion.AgregarParametroSQL("@par_Proveedor", filtro.NombreProveedor)
                End If
                If Not String.IsNullOrWhiteSpace(filtro.CodigoRefencia) Then
                    conexion.AgregarParametroSQL("@par_Referencia", filtro.CodigoRefencia)
                End If
                If Not String.IsNullOrWhiteSpace(filtro.NombreReferencia) Then
                    conexion.AgregarParametroSQL("@par_Nombre_Referencia", filtro.NombreReferencia)
                End If

                If filtro.Estado >= 0 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_proveedor_referencia_almacenes]")

                While resultado.Read
                    item = New ReferenciaProveedores(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As ReferenciaProveedores) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                conexion.AgregarParametroSQL("@par_REAL_Codigo", entidad.Referencia.Codigo)
                conexion.AgregarParametroSQL("@par_TERC_Proveedor", entidad.Proveedor.Codigo)
                conexion.AgregarParametroSQL("@par_Referencia_Proveedor", entidad.ReferenciaProveedor)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_proveedor_referencia_almacenes")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As ReferenciaProveedores) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                conexion.AgregarParametroSQL("@par_REAL_Codigo", entidad.Referencia.Codigo)
                conexion.AgregarParametroSQL("@par_TERC_Proveedor", entidad.Proveedor.Codigo)
                conexion.AgregarParametroSQL("@par_Referencia_Proveedor", entidad.ReferenciaProveedor)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Modfica", entidad.UsuarioModifica.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_movificar_proveedor_referencia_almacenes")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As ReferenciaProveedores) As ReferenciaProveedores
            Dim item As New ReferenciaProveedores

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                If Not IsNothing(filtro.Proveedor) Then
                    If filtro.Proveedor.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Proveedor", filtro.Proveedor.Codigo)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_proveedor_referencia_almacenes]")

                While resultado.Read
                    item = New ReferenciaProveedores(resultado)
                End While

            End Using

            Return item
        End Function

        Public Function Anular(entidad As ReferenciaProveedores) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_proveedor_referencia_almacenes]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Codigo")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function

    End Class

End Namespace