﻿EncoExpresApp.controller("GestionarOrdenCompraCtrl", ['$scope', '$timeout', '$routeParams', 'blockUI', '$linq', 'AlmacenesFactory', 'OrdenCompraFactory', 'ReferenciaAlmacenesFactory', 'TercerosFactory', 'ReferenciaProveedoresFactory', 'ReferenciasFactory', 'ValorCatalogosFactory',
    function ($scope, $timeout, $routeParams, blockUI, $linq, AlmacenesFactory, OrdenCompraFactory, ReferenciaAlmacenesFactory, TercerosFactory, ReferenciaProveedoresFactory, ReferenciasFactory, ValorCatalogosFactory) {
        //----------------------------- Decalaracion Variables ------------------------------------------------------// 
        $scope.MensajesError = [];
        $scope.MapaSitio = [{ Nombre: 'Almacén' }, { Nombre: 'Documentos' }, { Nombre: 'Ordenes compra' }];
        $scope.Titulo = 'NUEVA ORDEN COMPRA';
        $scope.ListadoDetalle = [];
        $scope.ListadoProveedores = [];
        $scope.ListadoReferenciaProveedor = [];
        $scope.ListadoReferenciasProveedor = [];
        $scope.ListadoReferenciasAlmacenes = [];
        $scope.ListadoReferencias = [];
        $scope.ListadoAlmacenes = [];
        $scope.ListadoReferenciaAlmacenProveedor = [];
        $scope.DeshabilitarDiasPlazo = false
        $scope.ListadoReferenciaProveedorAlmacen = [];
        $scope.ExisteReferencia = false;
        $scope.ListadoEstados = [
            { Nombre: 'DEFINITIVO', Codigo: 1 },
            { Nombre: 'BORRADOR', Codigo: 0 }
        ];

        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 0');
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_ORDEN_COMPRA);

        //Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Numero: 0,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            TipoDocumento: 325,
            Cumplido: 1,
            Oficina: {
                Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
                Nombre: $scope.Sesion.UsuarioAutenticado.Oficinas.Nombre,
            },
        };
        $scope.Fecha = new Date();


        /* Obtener parametros del enrutador*/
        if ($routeParams.Numero !== undefined) {
            $scope.Modelo.Numero = $routeParams.Numero;
        }
        else {
            $scope.Modelo.Numero = 0;
        }
        $scope.DeshabilitarPorcentajeDescuento = true
        $scope.ValorTransporte = 0;
        $scope.Subtotal = 0;
        $scope.ValorIva = 0;
        $scope.ValorDescuento = 0;
        $scope.ValorTotal = 0;
        $scope.Modelo.ValorAnticipo = 0;
        $scope.ValorFlete = 0;
        $scope.PorcentajeAnticipo = 0;
        $scope.Modelo.ValorDescuento = 0;
        $scope.Modelo.ValorIva = 0;
        $scope.Modelo.ValorTotal = 0;


        //Metodo para cerrar la modal de Novedades
        $scope.CerrarModalDetalle = function () {

            closeModal('modalDetalleOrdenCompra');
        };

        $scope.AsignarProveedor = function (Proveedor) {
            if (Proveedor != undefined || Proveedor != null) {
                if (angular.isObject(Proveedor)) {
                    $scope.LongitudProveedor = Proveedor.NombreCompleto.length;
                    $scope.AutocompleteValidoProveedor = true;
                }
                else if (angular.isString(Proveedor)) {
                    $scope.LongitudProveedor = Proveedor.length;
                    $scope.AutocompleteValidoProveedor = false;
                }
            }
        };


        //Metodo para volver a la página de consulta
        $scope.VolverMaster = function () {
            if ($scope.Modelo.Numero > 0) {
                document.location.href = '#!ConsultarOrdenCompra/' + $scope.Modelo.Numero;
            }
            else {
                document.location.href = '#!ConsultarOrdenCompra';
            }
        };
        //-----------------------------------------------------Cargar Combos/Autocomplete----------------------------------------------------------------
        //CARGA LOS PROVEDORES
        TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Autocomplete: 1, Proveedores: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoProveedores = response.data.Datos;
                        if ($scope.CodigoProveedor !== undefined && $scope.CodigoProveedor !== '' && $scope.CodigoProveedor !== 0 && $scope.CodigoProveedor !== null) {
                            $scope.Proveedor = $linq.Enumerable().From($scope.ListadoProveedores).First('$.Codigo ==' + $scope.CodigoProveedor);
                        } else {
                            $scope.Proveedor = '';
                        }
                    }
                    else {
                        $scope.ListadoProveedores = [];
                        $scope.Proveedor = null;
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        //CARGAR LOS ALMACENES
        AlmacenesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoAlmacenes = [];
                    $scope.ListadoAlmacenes = response.data.Datos

                    if ($scope.CodigoAlmacen !== undefined && $scope.CodigoAlmacen !== '' && $scope.CodigoAlmacen !== 0 && $scope.CodigoAlmacen !== null) {
                        $scope.Almacenes = $linq.Enumerable().From($scope.ListadoAlmacenes).First('$.Codigo ==' + response.data.Datos.Almacen.Codigo);
                    }
                    else {
                        $scope.Almacenes = response.data.Datos[0];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });


        //CARGAR LISTADO REFERENCIAS
        //ReferenciasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
        //    then(function (response) {
        //        if (response.data.ProcesoExitoso === true) {
        //            $scope.ListadoReferencias = [];
        //            $scope.ListadoReferencias = response.data.Datos
        //        }
        //    }, function (response) {
        //        ShowError(response.statusText);

        //    });



        //Cargar Autocomplete de Cuentas PUC
        ReferenciasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoReferencias = [];
                        $scope.ListadoReferencias = response.data.Datos;
                        if ($scope.ListadoReferencias.length > 0) {
                            $scope.Referencia = $linq.Enumerable().From($scope.ListadoReferencias).First('$.Codigo ==' + $scope.Nombre);
                            $scope.Referencia = $scope.Referencia.CodigoReferencia;
                        } else {
                            $scope.Referencia = '';
                        }
                    }
                    else {
                        $scope.ListadoReferencias = [];
                        $scope.ListadoReferencias = null;
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        $scope.ListadoFormaPago = ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_FORMA_PAGO_VENTAS }, Sync: true }).Datos;
        $scope.FormaPago = $scope.ListadoFormaPago[0];


        // VALIDAR LA REFERENCIA
        $scope.ValidarReferencia = function (Referencia) {
            if (Referencia != undefined && Referencia != '' && Referencia != null) {
                ReferenciasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_DEFINITIVO, Referencia: Referencia }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $scope.MensajesErrorDetalle = [];
                            if (response.data.Datos.Codigo > 0) {
                                // $scope.Referencia = response.data.Datos;
                                $scope.Referencia = response.data.Datos.CodigoReferencia; // Codigo Referencia Almacen
                                $scope.CodigoReferencia = response.data.Datos.Codigo; // Codigo Interno Referencia Almacen
                                $scope.ReferenciaNombre = response.data.Datos.Nombre; // Nombre Referencia Almacen
                                $scope.ReferenciaProveedor = response.data.Datos.ReferenciaProveedor.NombreReferencia; // Nombre Referencia Proveedor
                                $scope.CodigoReferenciaProveedor = response.data.Datos.ReferenciaProveedor.Codigo; // Codigo Referencia roveedor

                                $scope.UnidadMedida = response.data.Datos.UnidadMedida.NombreCorto;
                                $scope.UnidadEmpaque = response.data.Datos.UnidadEmpaque.NombreCorto;
                                $scope.Iva = response.data.Datos.PorcentajeIva;
                                $scope.NombreReferencia = response.data.Datos;


                            }
                            ValidarReferenciaAlmacen($scope.CodigoReferencia)
                            ValidarReferenciaProveedor($scope.CodigoReferenciaProveedor)
                        }
                        else {
                            ShowError('el nombre de la referencia ingresada no es valida');
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }

        }

        function ValidarReferenciaAlmacen(Referencia) {
            if (Referencia != undefined && Referencia != '' && Referencia != null && Referencia != 0) {
                ReferenciaAlmacenesFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Almacen: $scope.Almacenes, Referencia: { Codigo: Referencia } }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                $scope.CodigoReferencia = response.data.Datos.Referencia.Codigo;
                            } else {
                                $scope.MensajesErrorDetalle.push('La referencia  no se encuentra asociada al almacén ' + $scope.Almacenes.Nombre);
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }

        }

        function ValidarReferenciaProveedor(Referencia) {
            if (Referencia != undefined && Referencia != '' && Referencia != null && Referencia != 0) {
                ReferenciaProveedoresFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: Referencia, Proveedor: $scope.Proveedor }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                $scope.CodigoReferenciaProveedor = response.data.Datos;
                            } else {
                                $scope.MensajesErrorDetalle.push('La referencia  no se encuentra asociada al proveedor ' + $scope.Proveedor.Nombre);
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }
        function CargarReferencias() {
            $scope.MensajesErrorDetalle = [];
            $scope.ReferenciaNombre = '';
            showModal('modalDetalleOrdenCompra');
        }


        // Metodo para guardar /modificar
        $scope.Guardar = function () {
            $scope.MaskNumero();
            closeModal('modalConfirmacionGuardarOrden');

            $scope.Modelo.Fecha = $scope.Fecha;
            $scope.Modelo.Almacen = { Codigo: $scope.Almacenes.Codigo };
            $scope.Modelo.ContactoRecibe = $scope.ContactoRecibe;
            $scope.Modelo.Proveedor = { Codigo: $scope.Proveedor.Codigo };
            $scope.Modelo.ContactoEntrega = $scope.Contacto;
            $scope.Modelo.FechaEntrega = $scope.FechaEntrega;
            $scope.Modelo.FormaPago = $scope.FormaPago;
            $scope.Modelo.DiasCredito = $scope.DiasCredito;
            $scope.Modelo.Observaciones = $scope.Observaciones;
            $scope.Modelo.PorcentajeAnticipo = $scope.PorcentajeAnticipo;
            $scope.Modelo.ValorAnticipo = $scope.Modelo.ValorAnticipo;
            $scope.Modelo.Subtotal = $scope.Subtotal;
            $scope.Modelo.ValorDescuento = $scope.Modelo.ValorDescuento;
            $scope.Modelo.ValorIva = $scope.Modelo.ValorIva;
            $scope.Modelo.ValorTotal = $scope.Modelo.ValorTotal;
            $scope.Modelo.ValorFlete = $scope.ValorFlete;
            $scope.ValorTransporte = $scope.ValorTransporte;
            $scope.Modelo.EstadoDocumento = { Codigo: $scope.Estado.Codigo };
            $scope.Modelo.PorcentajeDescuento = $scope.Descuento;
            $scope.Modelo.ValorAntesDescuento = $scope.Modelo.ValorAntesDescuento;
            $scope.Modelo.Detalles = $scope.ListadoDetalle;

            if (DatosRequeridos()) {
                OrdenCompraFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {

                                if ($scope.Modelo.Numero == 0) {
                                    ShowSuccess('Se guardó la orden de compra No.' + response.data.Datos);
                                    location.href = '#!ConsultarOrdenCompra/' + response.data.Datos;

                                }

                                else {
                                    ShowSuccess('Se modificó la orden de compra No.' + response.data.Datos);
                                    location.href = '#!ConsultarOrdenCompra/' + $scope.Modelo.Numero;
                                }

                            }
                            else {
                                ShowError(response.data.MensajeOperacion);
                            }
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };


        $scope.ConfirmacionGuardarOrden = function () {
            showModal('modalConfirmacionGuardarOrden');
        };
        // Valida los datos requeridos 
        function DatosRequeridos() {
            window.scrollTo(top, top);
            $scope.MensajesError = [];
            var continuar = true;

            if ($scope.Fecha == '' || $scope.Fecha == null || $scope.Fecha == undefined) {
                $scope.MensajesError.push('Debe ingresar la fecha de la orden de compra');
                continuar = false;
            }
            if ($scope.Almacenes == '' || $scope.Almacenes == null || $scope.Almacenes == undefined) {
                $scope.MensajesError.push('Debe ingresar el almacen');
                continuar = false;
            }
            if ($scope.Estado == '' || $scope.Estado == null || $scope.Estado == undefined) {
                $scope.MensajesError.push('Debe ingresar el estado');
                continuar = false;
            }
            if ($scope.Proveedor == '' || $scope.Proveedor == null || $scope.Proveedor == undefined) {
                $scope.MensajesError.push('Debe ingresar el proveedor');
                continuar = false;
            }
            if ($scope.FormaPago == '' || $scope.FormaPago == null || $scope.FormaPago == undefined) {
                $scope.MensajesError.push('Debe ingresar la forma de pago');
                continuar = false;
            }
            else {
                if ($scope.DiasCredito == undefined || $scope.DiasCredito == "" || $scope.DiasCredito == 0 || $scope.DiasCredito == null) {
                    if ($scope.FormaPago.Codigo !== CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CREDITO) {
                        $scope.Modelo.DiasPlazoPago = 0;
                    }
                    else {
                        $scope.MensajesError.push('Para la forma de pago (crédito) debe ingresar los dias de plazo');
                        continuar = false;
                    }
                }

            }
            if ($scope.FechaEntrega == '' || $scope.FechaEntrega == null || $scope.FechaEntrega == undefined) {
                $scope.MensajesError.push('Debe ingresar la fecha entrega');
                continuar = false;
            }
            if ($scope.Observaciones == '' || $scope.Observaciones == null || $scope.Observaciones == undefined) {
                $scope.Observaciones = '';
            }
            if ($scope.ValorFlete == '' || $scope.ValorFlete == null || $scope.ValorFlete == undefined) {
                $scope.ValorFlete = 0;
            }
            if ($scope.PorcentajeAnticipo == '' || $scope.PorcentajeAnticipo == null || $scope.PorcentajeAnticipo == undefined) {
                $scope.PorcentajeAnticipo = 0;
            }
            if ($scope.Modelo.ValorAnticipo == '' || $scope.Modelo.ValorAnticipo == null || $scope.Modelo.ValorAnticipo == undefined) {
                $scope.Modelo.ValorAnticipo = 0;
            }

            if ($scope.ListadoDetalle.length == 0 || $scope.ListadoDetalle == undefined) {
                $scope.MensajesError.push('Debe ingresar minimo un detalle');
                continuar = false;
            }

            $scope.Fecha.setHours(0);
            $scope.Fecha.setMinutes(0);
            $scope.Fecha.setSeconds(0);
            $scope.Fecha.setMilliseconds(0);


            $scope.FechaEntrega.setHours(0);
            $scope.FechaEntrega.setMinutes(0);
            $scope.FechaEntrega.setSeconds(0);
            $scope.FechaEntrega.setMilliseconds(0);

            if ($scope.FechaEntrega < $scope.Fecha) {
                $scope.MensajesError.push('La fecha entrega debe ser igual o mayor a la fecha de la orden de compra');
                continuar = false;
            }

            return continuar

        }

        // Habilitar el campo dias de pago cuando la forma de pago es CREDITO
        $scope.ValidarFormaPago = function (FormaPago) {
            if (FormaPago.Codigo !== CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CREDITO) {
                $scope.DeshabilitarDiasPlazo = true
            } else {
                $scope.DeshabilitarDiasPlazo = false
            }

        }

        function DatosRequeridosDetalle() {
            $scope.MensajesErrorDetalle = [];
            var continuar = true;
            if ($scope.Referencia == '' || $scope.Referencia == null || $scope.Referencia == undefined) {
                $scope.MensajesErrorDetalle.push('Debe ingresar una  referencia');
                continuar = false;
            }
            if ($scope.ReferenciaProveedor == '' || $scope.ReferenciaProveedor == null || $scope.ReferenciaProveedor == undefined) {
                $scope.MensajesErrorDetalle.push('Debe ingresar la referencia proveedor');
                continuar = false;
            }
            else if ($scope.CodigoReferenciaProveedor == undefined || $scope.CodigoReferenciaProveedor == null || $scope.CodigoReferenciaProveedor == 0) {
                $scope.MensajesErrorDetalle.push('Debe ingresar una referencia proveedor válida');
                continuar = false;
            }
            if ($scope.Cantidad == '' || $scope.Cantidad == null || $scope.Cantidad == undefined || $scope.Cantidad == 0) {
                $scope.MensajesErrorDetalle.push('Debe ingresar la cantidad');
                continuar = false;
            }
            if ($scope.ValorUnitario == '' || $scope.ValorUnitario == null || $scope.ValorUnitario == undefined) {
                $scope.MensajesErrorDetalle.push('Debe ingresar el valor unitario');
                continuar = false;
            } else if ($scope.ValorUnitario == 0) {
                $scope.MensajesErrorDetalle.push('Debe ingresar el valor unitario debe ser mayor a 0');
                continuar = false;
            }

            if ($scope.ValorUnitario == '' || $scope.ValorUnitario == null || $scope.ValorUnitario == undefined) {
                $scope.ValorUnitario = 0
            }
            if ($scope.Descuento == '' || $scope.Descuento == null || $scope.Descuento == undefined) {
                $scope.Descuento = 0
            }
            if ($scope.ValorDescuento == '' || $scope.ValorDescuento == null || $scope.ValorDescuento == undefined) {
                $scope.ValorDescuento = 0
            }
            if ($scope.ValorNeto == '' || $scope.ValorNeto == null || $scope.ValorNeto == undefined) {
                $scope.ValorNeto = 0
            }
            if ($scope.Iva == '' || $scope.Iva == null || $scope.Iva == undefined) {
                $scope.Iva = 0
            }
            if ($scope.ValorIva == '' || $scope.ValorIva == null || $scope.ValorIva == undefined) {
                $scope.ValorIva = 0
            }
            if ($scope.cantidadRecibida == '' || $scope.cantidadRecibida == null || $scope.cantidadRecibida == undefined) {
                $scope.cantidadRecibida = 0
            }
            if ($scope.CantidadFaltante == '' || $scope.CantidadFaltante == null || $scope.CantidadFaltante == undefined) {
                $scope.CantidadFaltante = 0
            }
            $scope.ListadoDetalle.forEach(function (item) {
                if ($scope.Referencia.Codigo == item.ReferenciaAlmacen.Codigo) {
                    $scope.MensajesErrorDetalle.push('La referencia ' + item.ReferenciaAlmacen.Nombre + ' ya ha sido ingresada, por favor ingrese una diferente');
                    continuar = false;
                }
            })
            return continuar

        }

        //-------------------------------------------------------------------------------------------------------//

        //--Eliminar detalle orden compra
        $scope.ConfirmacionEliminarDetalle = function (Concepto, indice) {
            $scope.MensajeEliminar = { indice };
            $scope.VarAuxi = indice
            $scope.Concepto = Concepto

            //$scope.ReferenciaAlmacen = Concepto.ReferenciaAlmacen
            $scope.ReferenciaAlmacen = Concepto.ReferenciaAlmacen.Codigo;
            //= { NombreReferencia: detalle.ReferenciaAlmacen.Referencia.CodigoReferencia, Codigo: detalle.ReferenciaAlmacen.Codigo };
            var IndiceAuxi = ''
            showModal('modalEliminarDetalle');
        };

        $scope.EliminarDetalle = function (indice) {
            $scope.ListadoDetalle.forEach(function (item) {
                if ($scope.Concepto.$$hashKey == item.$$hashKey) {
                    $scope.ListadoDetalle.splice($scope.VarAuxi, 1);
                    IndiceAuxi = item.$$hashKey
                }
            });
            $scope.CalcularDetalle();
            $scope.CalcularAnticipo();
            $scope.Calcular();
            closeModal('modalEliminarDetalle');
        };
        //---------------------------------------------------------------------------------------------------------------------------------------------------
        //Insertar detalle en el grid 
        $scope.ModalDetalle = function () {

            if ($scope.Almacenes == undefined || $scope.Almacenes == null || $scope.Almacenes == '' || $scope.Almacenes.Codigo == 0) {
                window.scrollTo(top, top);
                $scope.MensajesError.push('Debe ingresar un almacén');
            } else if ($scope.Proveedor == undefined || $scope.Proveedor == null || $scope.Proveedor == '' || $scope.Proveedor.Codigo == 0) {
                window.scrollTo(top, top);
                $scope.MensajesError.push('Debe ingresar un proveedor');
            }
            else {
                var continuar = true
                $scope.ListadoReferenciasAlmacenes = [];
                $scope.ListadoReferenciasProveedor = [];
                $scope.ListadoReferenciaAlmacenProveedor = [];
                $scope.ListadoReferenciaProveedorAlmacen = [];
                $scope.MensajesErrorDetalle = [];
                $scope.Referencia = '';
                $scope.ReferenciaProveedor = '';
                $scope.ValorUnitario = 0;
                $scope.Cantidad = 0;
                $scope.Dimension = 0;
                $scope.ValorNeto = 0;
                $scope.Descuento = 0;
                $scope.ValorDescuento = 0;
                $scope.ValorAntesDescuento = 0;
                $scope.UnidadMedida = '';
                $scope.UnidadEmpaque = '';
                $scope.NombreReferencia = '';
                $scope.Iva = 0;
                $scope.ValorIva = 0;
                $scope.ValorTotal = 0;
                $scope.cantidadRecibida = 0;
                $scope.CantidadFaltante = 0;
                CargarReferencias()
            }
        }

        $scope.AsignarValorIVA = function () {
            $scope.Iva = $scope.Referencia.ValorIVA
        }

        $scope.AdicionarDetalle = function () {
            if (DatosRequeridosDetalle()) {
                $scope.CalcularDetalle();
                $scope.MaskNumero();
                adicionar();
                $scope.Calcular();
                closeModal('modalDetalleOrdenCompra');
            }

        }

        function adicionar() {
            $scope.ListadoDetalle.push({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
                ReferenciaAlmacen: { NombreReferencia: $scope.Referencia, Codigo: $scope.CodigoReferencia }, // Codigo de la referencia
                ReferenciaNombre: $scope.ReferenciaNombre, // Nombre referencia
                ReferenciaProveedor: $scope.ReferenciaProveedor,  // Nombre Referencia proveedor
                ValorUnitario: $scope.ValorUnitario,
                Cantidad: $scope.Cantidad,
                Dimension: $scope.Dimension,
                ValorNeto: $scope.ValorNeto,
                UnidadMedida: $scope.UnidadMedida,
                UnidadEmpaque: $scope.UnidadEmpaque,
                ValorAntesDescuento: $scope.ValorAntesDescuento,
                Subtotal: $scope.Subtotal,
                PorcentajeDescuento: $scope.Descuento,
                ValorDescuento: $scope.ValorDescuento,
                PorcentajeIva: $scope.Iva,
                ValorIva: $scope.ValorIva,
                ValorTotal: $scope.ValorTotal,
                CantidadRecibida: $scope.cantidadRecibida,
                CantidadFaltante: $scope.CantidadFaltante,
                CantidadFaltante: $scope.CantidadFaltante,

            });
        }

        if ($scope.Modelo.Numero > 0) {
            // Consultar los datos de la orden de compra correspondientes al número que llego como parametro del enrutador
            $scope.Titulo = 'EDITAR ORDEN COMPRA';
            $scope.Deshabilitar = true;
            $scope.Bloquear = true;
            Obtener();
        }
        //------------------------------------------------------------------------------------------------------------------------------------------------
        function Obtener() {

            blockUI.start('Cargando orden de compra número ' + $scope.Modelo.Numero);
            $scope.ListaConceptos = [];
            $timeout(function () {
                blockUI.message('Cargando orden de compra número ' + $scope.Modelo.Numero);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Modelo.Numero,
            };
            blockUI.delay = 1000;
            OrdenCompraFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        console.log("ORden Compra: ", response.data.Datos);
                        //Obtiene los datos del ENCABEZADO 
                        $scope.Modelo.Numero = response.data.Datos.Numero;
                        //Formatear campo fecha a dd/mm/aaaa
                        var fecha = new Date(response.data.Datos.Fecha);
                        fecha.setHours(0);
                        fecha.setMinutes(0);
                        fecha.setMilliseconds(0);

                        $scope.Fecha = fecha;

                        if ($scope.ListadoAlmacenes.length > 0) {
                            $scope.Almacenes = $linq.Enumerable().From($scope.ListadoAlmacenes).First('$.Codigo ==' + response.data.Datos.Almacen.Codigo);
                        } else {
                            $scope.CodigoAlmacen = response.data.Datos.Almacen.Codigo
                        }
                        $scope.ContactoRecibe = response.data.Datos.ContactoRecibe

                        if ($scope.ListadoProveedores.length > 0) {
                            $scope.Proveedor = $linq.Enumerable().From($scope.ListadoProveedores).First('$.Codigo ==' + response.data.Datos.Proveedor.Codigo);
                        }
                        else {
                            $scope.CodigoProveedor = response.data.Datos.Proveedor.Codigo
                        }
                        $scope.Contacto = response.data.Datos.ContactoEntrega

                        //Formatear campo fecha a dd/mm/aaaa
                        var fechaentrega = new Date(response.data.Datos.FechaEntrega);
                        fechaentrega.setHours(0);
                        fechaentrega.setMinutes(0);
                        fechaentrega.setMilliseconds(0);

                        $scope.FechaEntrega = fechaentrega
                        $scope.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo ==' + response.data.Datos.FormaPago.Codigo);
                        $scope.DiasCredito = response.data.Datos.DiasCredito;
                        $scope.Observaciones = response.data.Datos.Observaciones;

                        // Obtiene la información del DETALLE
                        $scope.PorcentajeAnticipo = response.data.Datos.PorcentajeAnticipo;
                        $scope.Modelo.ValorAnticipo = response.data.Datos.ValorAnticipo;
                        $scope.Subtotal = response.data.Datos.Subtotal;
                        $scope.Modelo.ValorDescuento = response.data.Datos.ValorDescuento;
                        $scope.Modelo.ValorIva = response.data.Datos.ValorIva;
                        $scope.Modelo.ValorTotal = response.data.Datos.ValorTotal;
                        $scope.PorcentajeDescuento = response.data.Datos.PorcentajeDescuento;
                        $scope.Modelo.ValorAntesDescuento = response.data.Datos.ValorAntesDescuento;
                        $scope.ValorFlete = response.data.Datos.ValorFlete;
                        $scope.Estado = response.data.Datos.Estado;

                        if (response.data.Datos.Estado.Codigo !== ESTADO_ANULADO) {
                            $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado.Codigo);
                        }
                        else {
                            $scope.ListadoEstados.push({ Nombre: 'Anulado', Codigo: ESTADO_ANULADO });
                            $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado.Codigo);
                            $scope.DeshabilitarPorcentajeDescuento = true;
                            $scope.Deshabilitar = true;
                            $scope.DeshabilitarDiasPlazo = true;
                        }
                        // Cargamos la información del detalle en grid
                        response.data.Datos.Detalles.forEach(detalle => {
                            detalle.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
                            detalle.Oficina = $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo;
                            detalle.ReferenciaNombre = detalle.ReferenciaAlmacen.NombreReferencia;  //$scope.ReferenciaNombre; // Nombre referencia
                            detalle.ReferenciaAlmacen = { NombreReferencia: detalle.ReferenciaAlmacen.Referencia.CodigoReferencia, Codigo: detalle.ReferenciaAlmacen.Codigo }; // Codigo de la referencia
                            detalle.ReferenciaProveedor = detalle.ReferenciaProveedor.NombreReferencia;  // Nombre Referencia proveedor
                            detalle.UnidadMedida = detalle.UnidadMedida.Nombre;
                            detalle.UnidadEmpaque = detalle.UnidadEmpaque.Nombre;
                            detalle.ValorUnitario = detalle.ValorUnitario;
                            detalle.Cantidad = detalle.Cantidad;
                            detalle.Dimension = detalle.Dimension;
                            detalle.ValorNeto = detalle.ValorNeto;
                            detalle.ValorAntesDescuento = detalle.ValorAntesDescuento;
                            detalle.Subtotal = detalle.ValorNeto;
                            //detalle.PorcentajeDescuento = detalle.Descuento;
                            detalle.PorcentajeDescuento = $scope.PorcentajeDescuento;
                            detalle.ValorDescuento = detalle.ValorDescuento;
                            detalle.PorcentajeIva = detalle.PorcentajeIva;
                            detalle.ValorIva = detalle.ValorIva;
                            detalle.ValorTotal = detalle.ValorTotal;
                            detalle.CantidadRecibida = detalle.cantidadRecibida;
                            detalle.CantidadFaltante = $scope.CantidadFaltante;
                        });
                        $scope.ListadoDetalle = response.data.Datos.Detalles;

                    }
                    //Borrador
                    if (response.data.Datos.Estado.Codigo == 0) {
                        $scope.Deshabilitar = false;
                        $scope.Bloquear = true;
                    }
                    //Definitivo
                    else if (response.data.Datos.Estado.Codigo = 1) {
                        $scope.Deshabilitar = true;
                        $scope.DeshabilitarPorcentajeDescuento = true;
                        $scope.DeshabilitarDiasPlazo = true;
                    }
                    $scope.MaskValores();


                }, function (response) {
                    ShowError(response.statusText);
                    //ShowError('No se logro consultar la Factura de Transporte Especial No.' + $scope.Modelo.Numero + '. Por favor contacte el administrador del sistema.');
                    document.location.href = '#!ConsultarOrdenCompra';
                });

            blockUI.stop();
        };
        $scope.CalcularDetalle = function () {
            $scope.MaskNumero();
            $scope.ValorParcial = Math.ceil($scope.ValorUnitario) * Math.ceil($scope.Cantidad)
            $scope.ValorDescuento = Math.ceil(Math.ceil($scope.Descuento) * Math.ceil($scope.ValorParcial)) / 100
            $scope.ValorNeto = Math.ceil($scope.ValorParcial) - Math.ceil($scope.ValorDescuento)
            $scope.ValorIva = Math.ceil(Math.ceil($scope.ValorNeto) * Math.ceil($scope.Iva)) / 100
            $scope.ValorAntesDescuento = Math.ceil($scope.ValorUnitario) * Math.ceil($scope.Cantidad)
            $scope.Subtotal = Math.ceil($scope.ValorAntesDescuento) - Math.ceil($scope.ValorDescuento)
            $scope.ValorTotal = Math.ceil($scope.ValorNeto) + Math.ceil($scope.ValorIva)
            $scope.MaskValores();
        }

        $scope.Calcular = function () {
            $scope.MaskNumero();
            $scope.Subtotal = 0;
            $scope.Modelo.ValorDescuento = 0;
            $scope.Modelo.ValorIva = 0;
            $scope.Modelo.ValorAntesDescuento = 0;
            $scope.Modelo.ValorTotal = 0;
            if ($scope.ListadoDetalle.length > 0) {
                for (var i = 0; i < $scope.ListadoDetalle.length; i++) {
                    var item = $scope.ListadoDetalle[i]
                    $scope.Modelo.ValorDescuento = Math.ceil($scope.Modelo.ValorDescuento) + Math.ceil(item.ValorDescuento)
                    $scope.Modelo.ValorIva = Math.ceil($scope.Modelo.ValorIva) + Math.ceil(item.ValorIva)
                    $scope.Modelo.ValorAntesDescuento = Math.ceil($scope.Modelo.ValorAntesDescuento) + Math.ceil(item.ValorAntesDescuento)
                    $scope.Subtotal = Math.ceil($scope.Subtotal) + Math.ceil(item.Subtotal)
                    if ($scope.Modelo.ValorDescuento > 0) {
                        $scope.DeshabilitarPorcentajeDescuento = true
                        $scope.PorcentajeDescuento = 0;
                    }
                    else {
                        $scope.DeshabilitarPorcentajeDescuento = false
                    }
                }
            }
            $scope.Modelo.ValorTotal = Math.ceil($scope.Subtotal) + Math.ceil($scope.Modelo.ValorIva)
            $scope.ValorAnticipo = (Math.ceil($scope.Modelo.ValorTotal) * Math.ceil($scope.PorcentajeAnticipo)) / 100
            $scope.MaskValores();
        }

        $scope.CalcularAnticipo = function () {
            $scope.Modelo.ValorAnticipo = 0
            $scope.Modelo.ValorAnticipo = (Math.ceil($scope.Modelo.ValorTotal) * Math.ceil($scope.PorcentajeAnticipo)) / 100
        }


        $scope.CalcularDescuento = function () {
            $scope.Modelo.ValorDescuento = 0;
            if ($scope.PorcentajeDescuento != undefined && $scope.PorcentajeDescuento != null && $scope.PorcentajeDescuento != '') {
                $scope.Modelo.ValorDescuento = 0
                $scope.Calcular()
                $scope.Modelo.ValorDescuento = (Math.ceil($scope.PorcentajeDescuento * $scope.Modelo.ValorTotal)) / 100
                $scope.Subtotal = Math.ceil($scope.Subtotal) - Math.ceil($scope.Modelo.ValorDescuento)
                $scope.Modelo.ValorTotal = Math.ceil($scope.Subtotal) + Math.ceil($scope.Modelo.ValorIva)
            }
        }

        $scope.MaskNumero = function () {
            try { $scope.PorcentajeAnticipo = MascaraNumero($scope.PorcentajeAnticipo) } catch (e) { }
            try { $scope.ValorFlete = MascaraNumero($scope.ValorFlete) } catch (e) { }
            try { $scope.DiasCredito = MascaraNumero($scope.DiasCredito) } catch (e) { }
            try { $scope.Cantidad = MascaraNumero($scope.Cantidad) } catch (e) { }
            try { $scope.Dimension = MascaraNumero($scope.Dimension) } catch (e) { }
            try { $scope.ValorUnitario = MascaraNumero($scope.ValorUnitario) } catch (e) { }
            try { $scope.Descuento = MascaraNumero($scope.Descuento) } catch (e) { }
            try { $scope.ValorDescuento = MascaraNumero($scope.ValorDescuento) } catch (e) { }
            try { $scope.ValorIva = MascaraNumero($scope.ValorIva) } catch (e) { }
            try { $scope.ValorNeto = MascaraNumero($scope.ValorNeto) } catch (e) { }
            try { $scope.ValorTotal = MascaraNumero($scope.ValorTotal) } catch (e) { }
            try { $scope.ValorNeto = MascaraNumero($scope.ValorNeto) } catch (e) { }

        };
        $scope.MaskValores = function () {
            try { $scope.PorcentajeAnticipo = MascaraValores($scope.PorcentajeAnticipo) } catch (e) { }
            try { $scope.ValorFlete = MascaraValores($scope.ValorFlete) } catch (e) { }
            try { $scope.ValorUnitario = MascaraValores($scope.ValorUnitario) } catch (e) { }
            try { $scope.Descuento = MascaraValores($scope.Descuento) } catch (e) { }
            try { $scope.ValorDescuento = MascaraValores($scope.ValorDescuento) } catch (e) { }
            try { $scope.ValorIva = MascaraValores($scope.ValorIva) } catch (e) { }
            try { $scope.ValorNeto = MascaraValores($scope.ValorNeto) } catch (e) { }
            try { $scope.ValorTotal = MascaraValores($scope.ValorTotal) } catch (e) { }
            try { $scope.ValorTotal = MascaraValores($scope.ValorTotal) } catch (e) { }
            try { $scope.ValorNeto = MascaraValores($scope.ValorNeto) } catch (e) { }
            try { $scope.ValorTransporte = MascaraNumero($scope.ValorTransporte) } catch (e) { }
        };
        $scope.MaskMayus = function (option) {
            try { $scope.ContactoRecibe = $scope.ContactoRecibe.toUpperCase() } catch (e) { }
            try { $scope.Contacto = $scope.Contacto.toUpperCase() } catch (e) { }
        };
        $scope.MaskMoneda = function () {
            $scope.Iva = MascaraDecimales($scope.Iva)
        }
        $scope.MaskValorIVA = function () {
            $scope.Iva = MascaraNumero($scope.Iva)
        }
    }]);