﻿Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources
Imports EncoExpres.GesCarga.Entidades.Paqueteria
Imports EncoExpres.GesCarga.Fachada.Paqueteria
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Fachada.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Paqueteria
    Public NotInheritable Class LogicaPlanillaPaqueteria
        Inherits LogicaBase(Of PlanillaPaqueteria)

        ReadOnly _persistencia As IPersistenciaBase(Of PlanillaPaqueteria)

        Sub New(persistencia As IPersistenciaBase(Of PlanillaPaqueteria))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As PlanillaPaqueteria) As Respuesta(Of IEnumerable(Of PlanillaPaqueteria))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of PlanillaPaqueteria))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of PlanillaPaqueteria))(consulta)
        End Function

        Public Overrides Function Obtener(filtro As PlanillaPaqueteria) As Respuesta(Of PlanillaPaqueteria)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of PlanillaPaqueteria)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of PlanillaPaqueteria)(consulta)
        End Function

        Public Overrides Function Guardar(entidad As PlanillaPaqueteria) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long = 0

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If Not IsNothing(entidad.ParametrosCalculos) Then
                validarDatos = CalcularAnticipo(entidad)
                If validarDatos.ProcesoExitoso Then
                    entidad = validarDatos.Datos
                    validarDatos = CalcularTotales(entidad)
                    If validarDatos.ProcesoExitoso Then
                        entidad = validarDatos.Datos
                    End If
                End If
            End If

            If entidad.Planilla.Numero = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Numero > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Private Function DatosRequeridos(entidad As PlanillaPaqueteria) As Respuesta(Of PlanillaPaqueteria)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of PlanillaPaqueteria) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of PlanillaPaqueteria) With {.ProcesoExitoso = True}

        End Function

        Public Function Anular(entidad As PlanillaPaqueteria) As Respuesta(Of PlanillaPaqueteria)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo = CType(_persistencia, PersistenciaPlanillaPaqueteria).Anular(entidad)

            If IsNothing(anulo) Then
                Return New Respuesta(Of PlanillaPaqueteria) With {.ProcesoExitoso = False, .MensajeOperacion = Recursos.FalloOperacionAnularRegistro}
            End If
            Return New Respuesta(Of PlanillaPaqueteria)(anulo)
        End Function

        Public Function EliminarGuia(entidad As PlanillaPaqueteria) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim BorrarGuia As Boolean = False

            BorrarGuia = CType(_persistencia, PersistenciaPlanillaPaqueteria).EliminarGuia(entidad)

            If Not BorrarGuia Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, Recursos.OperacionAnulo)}
        End Function

        Public Function EliminarRecoleccion(entidad As PlanillaPaqueteria) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim BorrarGuia As Boolean = False

            BorrarGuia = CType(_persistencia, PersistenciaPlanillaPaqueteria).EliminarRecoleccion(entidad)

            If Not BorrarGuia Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, Recursos.OperacionAnulo)}
        End Function

        Public Function ObtenerPlanilla(filtro As PlanillaPaqueteria) As Respuesta(Of IEnumerable(Of PlanillaPaqueteria))
            Dim consulta = CType(_persistencia, PersistenciaPlanillaPaqueteria).ObtenerPlanilla(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of PlanillaPaqueteria))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of PlanillaPaqueteria))(consulta) With {.ProcesoExitoso = True}
        End Function

        Public Function ConsultarPlanillasPendientesRecepcionar(filtro As PlanillaPaqueteria) As Respuesta(Of IEnumerable(Of PlanillaPaqueteria))
            Dim consulta = CType(_persistencia, PersistenciaPlanillaPaqueteria).ConsultarPlanillasPendientesRecepcionar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of PlanillaPaqueteria))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of PlanillaPaqueteria))(consulta)
        End Function


        Public Function CalcularAnticipo(entidad As PlanillaPaqueteria) As Respuesta(Of PlanillaPaqueteria)
            Dim strError As String = String.Empty

            If IsNothing(entidad) Then
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
                Return New Respuesta(Of PlanillaPaqueteria)(entidad) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If
            If entidad.ParametrosCalculos.SugerirAnticipoPlanilla = True And entidad.ParametrosCalculos.DeshabilitarAnticipoPlanillaPaqueteria = False Then
                Dim sumValores As Double = 0.0
                Dim filtroRutas As New Rutas
                With filtroRutas
                    .CodigoEmpresa = entidad.CodigoEmpresa
                    .Codigo = entidad.Planilla.Ruta.Codigo
                    .ConsultaLegalizacionGastosRuta = True
                End With
                Dim persistenciaRutas As IPersistenciaBase(Of Rutas)
                Dim ListaGastosRuta = CType(persistenciaRutas, PersistenciaRutas).Obtener(filtroRutas).LegalizacionGastosRuta
                If Not IsNothing(entidad.Planilla.Vehiculo) Then
                    If entidad.Planilla.Vehiculo.TipoDueno.Codigo = 2102 Then 'CODIGO_CATALOGO_TIPO_DUENO_PROPIO
                        For Each item In ListaGastosRuta
                            If item.TipoVehiculo.Codigo = entidad.Planilla.Vehiculo.TipoVehiculo.Codigo Then
                                If item.AplicaAnticipo = "1" Then
                                    sumValores += item.Valor
                                End If
                            End If
                        Next
                        If Not IsNothing(entidad.Planilla.Vehiculo) Then
                            If Not entidad.Planilla.Vehiculo.Codigo = 0 AndAlso Not IsNothing(entidad.Planilla.Vehiculo.Placa) Then
                                Dim persistenciaVehiculos As IPersistenciaBase(Of Vehiculos)
                                Dim filtroVehiculos As New Vehiculos
                                With filtroVehiculos
                                    .CodigoEmpresa = entidad.CodigoEmpresa
                                    .Codigo = entidad.Planilla.Vehiculo.Codigo
                                End With
                                Dim responseVehiculo = CType(persistenciaVehiculos, PersistenciaVehiculos).Obtener(filtroVehiculos)
                                Dim responseSemirremolque As Semirremolques
                                Dim TotalEjes As Integer = 0
                                If Not IsNothing(responseVehiculo) Then
                                    If Not IsNothing(entidad.Planilla.Semirremolque) Then
                                        If Not entidad.Planilla.Semirremolque.Codigo = 0 AndAlso Not IsNothing(entidad.Planilla.Semirremolque.Placa) Then
                                            Dim persistenciaSemi As IPersistenciaBase(Of Semirremolques)
                                            Dim filtroSemi As New Semirremolques
                                            With filtroSemi
                                                .CodigoEmpresa = entidad.CodigoEmpresa
                                                .Codigo = entidad.Planilla.Semirremolque.Codigo
                                            End With
                                            responseSemirremolque = CType(persistenciaSemi, PersistenciaSemirremolques).Obtener(filtroSemi)
                                            If Not IsNothing(responseSemirremolque) Then
                                                TotalEjes = responseSemirremolque.NumeroEjes
                                            End If
                                        Else
                                            TotalEjes = 0
                                        End If
                                    End If
                                    Dim totalPejaes As Integer = 0
                                    With filtroRutas
                                        .CodigoEmpresa = entidad.CodigoEmpresa
                                        .Codigo = entidad.Planilla.Ruta.Codigo
                                        .ConsultaLegalizacionGastosRuta = False
                                        .ConsultarPeajes = True
                                    End With
                                    Dim ResponseRutas = CType(persistenciaRutas, PersistenciaRutas).Obtener(filtroRutas)
                                    If Not IsNothing(ResponseRutas) Then
                                        For Each item In ResponseRutas.PeajeRutas
                                            If item.Peaje.Estado = 1 Then
                                                Dim filtroPeajes As New Peajes
                                                With filtroPeajes
                                                    .CodigoEmpresa = entidad.CodigoEmpresa
                                                    .Codigo = item.Peaje.Codigo
                                                    .ConsultarTarifasPeajes = True
                                                End With
                                                Dim persistenciaPeajes As IPersistenciaBase(Of Peajes)
                                                Dim ResponsePeajes = CType(persistenciaPeajes, PersistenciaPeajes).Obtener(filtroPeajes)
                                                For Each itemPeajes In ResponsePeajes.TarifasPeajes
                                                    If itemPeajes.TipoVehiculo.Codigo = entidad.Planilla.Vehiculo.TipoVehiculo.Codigo Then
                                                        If TotalEjes > 0 Then
                                                            If TotalEjes = 1 Then
                                                                sumValores += itemPeajes.ValorRemolque1Eje
                                                            ElseIf TotalEjes = 2 Then
                                                                sumValores += itemPeajes.ValorRemolque2Ejes
                                                            ElseIf TotalEjes = 3 Then
                                                                sumValores += itemPeajes.ValorRemolque3Ejes
                                                            ElseIf TotalEjes >= 4 Then
                                                                sumValores += itemPeajes.ValorRemolque4Ejes
                                                            End If
                                                        Else
                                                            sumValores += itemPeajes.Valor
                                                        End If
                                                    End If
                                                Next
                                            End If
                                        Next
                                    End If
                                End If
                            End If
                        End If
                        entidad.Planilla.ValorAnticipo = sumValores
                    End If
                End If

                Return New Respuesta(Of PlanillaPaqueteria)(entidad) With {.ProcesoExitoso = True}
            End If
            Return New Respuesta(Of PlanillaPaqueteria)(entidad) With {.ProcesoExitoso = True}

        End Function
        Public Function CalcularTotales(entidad As PlanillaPaqueteria) As Respuesta(Of PlanillaPaqueteria)
            Dim strError As String = String.Empty

            If IsNothing(entidad) Then
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
                Return New Respuesta(Of PlanillaPaqueteria)(entidad) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Dim Credito As New TotalesRemesa
            With Credito
                .Cantidad = 0
                .FormaPago = "Crédito"
                .ValorCliente = 0
                .ValorTransportador = 0
                .ValorSeguro = 0
            End With
            Dim Contado As New TotalesRemesa
            With Contado
                .Cantidad = 0
                .FormaPago = "Contado"
                .ValorCliente = 0
                .ValorTransportador = 0
                .ValorSeguro = 0
            End With
            Dim ContraEntrega As New TotalesRemesa
            With ContraEntrega
                .Cantidad = 0
                .FormaPago = "Contra Entrega"
                .ValorCliente = 0
                .ValorTransportador = 0
                .ValorSeguro = 0
            End With
            entidad.Planilla.Peso = 0
            entidad.Planilla.Cantidad = 0
            entidad.Planilla.ValorImpuestos = 0
            For Each item In entidad.ParametrosCalculos.ListadoRemesasFiltradas
                'If item.Seleccionado Then -- LOGICA ORIGINAL, NUNCA SE ASIGNA ESTE VALOR EN FRONT
                If True = False Then
                    If item.FormaPago.Codigo = 4901 Then
                        Credito.Cantidad += 1
                        Credito.ValorCliente += item.TotalFleteCliente
                        Credito.ValorTransportador += item.ValorFleteTransportador
                        Credito.ValorSeguro += item.ValorSeguroCliente
                    End If
                    If item.FormaPago.Codigo = 4902 Then
                        Contado.Cantidad += 1
                        Contado.ValorCliente += item.TotalFleteCliente
                        Contado.ValorTransportador += item.ValorFleteTransportador
                        Contado.ValorSeguro += item.ValorSeguroCliente
                    End If
                    If item.FormaPago.Codigo = 4903 Then
                        ContraEntrega.Cantidad += 1
                        ContraEntrega.ValorCliente += item.TotalFleteCliente
                        ContraEntrega.ValorTransportador += item.ValorFleteTransportador
                        ContraEntrega.ValorSeguro += item.ValorSeguroCliente
                    End If
                    entidad.Planilla.Cantidad += item.CantidadCliente
                    entidad.Planilla.Peso += item.PesoCliente
                End If
            Next
            If Not IsNothing(entidad.ParametrosCalculos.ListadoRemesasGuardadas) Then
                For Each item In entidad.ParametrosCalculos.ListadoRemesasGuardadas
                    If item.FormaPago.Codigo = 4901 Then
                        Credito.Cantidad += 1
                        Credito.ValorCliente += item.TotalFleteCliente
                        Credito.ValorTransportador += item.ValorFleteTransportador
                        Credito.ValorSeguro += item.ValorSeguroCliente
                    End If
                    If item.FormaPago.Codigo = 4902 Then
                        Contado.Cantidad += 1
                        Contado.ValorCliente += item.TotalFleteCliente
                        Contado.ValorTransportador += item.ValorFleteTransportador
                        Contado.ValorSeguro += item.ValorSeguroCliente
                    End If
                    If item.FormaPago.Codigo = 4903 Then
                        ContraEntrega.Cantidad += 1
                        ContraEntrega.ValorCliente += item.TotalFleteCliente
                        ContraEntrega.ValorTransportador += item.ValorFleteTransportador
                        ContraEntrega.ValorSeguro += item.ValorSeguroCliente
                    End If
                    entidad.Planilla.Cantidad += item.CantidadCliente
                    entidad.Planilla.Peso += item.PesoCliente
                Next
            End If
            entidad.ParametrosCalculos.ListadoTotalesRemesas = {Credito}
            entidad.ParametrosCalculos.ListadoTotalesRemesas = entidad.ParametrosCalculos.ListadoTotalesRemesas.Concat({Contado})
            entidad.ParametrosCalculos.ListadoTotalesRemesas = entidad.ParametrosCalculos.ListadoTotalesRemesas.Concat({ContraEntrega})
            If entidad.ParametrosCalculos.FleteTarifaTransporte > 0 Then
                entidad.Planilla.ValorFleteTransportador = entidad.ParametrosCalculos.FleteTarifaTransporte
            End If
            If Not IsNothing(entidad.Planilla.ValorAnticipo) Then
                If entidad.Planilla.ValorAnticipo > entidad.Planilla.ValorFleteTransportador Then
                    entidad.Planilla.ValorAnticipo = 0
                    entidad.Planilla.ValorPagarTransportador = Math.Round(entidad.Planilla.ValorFleteTransportador, 0)
                Else
                    entidad.Planilla.ValorPagarTransportador = entidad.Planilla.ValorFleteTransportador - entidad.Planilla.ValorAnticipo
                End If
            Else
                entidad.Planilla.ValorPagarTransportador = Math.Round(entidad.Planilla.ValorFleteTransportador, 0)
            End If
            Dim ListadoImpuestosFiltrado As IEnumerable(Of DetalleImpuestosPlanilla)
            If entidad.ParametrosCalculos.ListadoImpuestos.Count > 0 Then
                For Each item In entidad.ParametrosCalculos.ListadoImpuestos
                    Dim impuesto As New DetalleImpuestosPlanilla
                    With impuesto
                        .Nombre = item.Nombre
                        .Codigo = item.Codigo
                        .valor_base = item.valor_base
                        .Valor_tarifa = item.Valor_tarifa
                    End With
                    If item.valor_base < entidad.Planilla.ValorFleteTransportador Then
                        impuesto.valor_base = entidad.Planilla.ValorFleteTransportador
                    End If
                    impuesto.ValorImpuesto = impuesto.Valor_tarifa * impuesto.valor_base
                    impuesto.ValorImpuesto = Math.Round(impuesto.ValorImpuesto * 100, 0) / 100
                    If Not IsNothing(ListadoImpuestosFiltrado) Then
                        ListadoImpuestosFiltrado = ListadoImpuestosFiltrado.Concat({impuesto})
                    Else
                        ListadoImpuestosFiltrado = {impuesto}
                    End If
                Next
            End If
            If ListadoImpuestosFiltrado.Count > 0 Then
                For Each item In ListadoImpuestosFiltrado
                    entidad.Planilla.ValorImpuestos += item.ValorImpuesto
                Next
            End If

            entidad.Planilla.ValorPagarTransportador = entidad.Planilla.ValorPagarTransportador - entidad.Planilla.ValorImpuestos
            entidad.Planilla.Peso = Math.Round(entidad.Planilla.Peso, 2)
            entidad.Planilla.ValorFleteTransportador = Math.Round(entidad.Planilla.ValorFleteTransportador, 0)
            entidad.Planilla.ValorPagarTransportador = Math.Round(entidad.Planilla.ValorPagarTransportador, 0)
            entidad.Planilla.ValorImpuestos = Math.Round(entidad.Planilla.ValorImpuestos, 0)
            entidad.ParametrosCalculos.ListadoImpuestos = ListadoImpuestosFiltrado

            Return New Respuesta(Of PlanillaPaqueteria)(entidad) With {.ProcesoExitoso = True}

        End Function
    End Class
End Namespace

