﻿Imports Newtonsoft.Json

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref=" ItemFormularioInspecciones"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class ItemFormularioInspecciones
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ItemFormularioInspecciones"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ItemFormularioInspecciones"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            CodigoFormulario = Read(lector, "ENFI_Codigo")
            Seccion = Read(lector, "SFIN_Codigo")
            Orden = Read(lector, "Orden_Formulario")
            Relevante = Read(lector, "Relevante")
            Tamano = Read(lector, "Tamaño")
            TipoControl = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TCFI_Codigo"), .Nombre = Read(lector, "Nombre_Tipo_Control")}
            EstadoDocumento = New ValorCatalogos With {.Codigo = Read(lector, "Estado")}
            ExigeCumplimiento = Read(lector, "Exige_Cumplimiento")
        End Sub
        <JsonProperty>
        Public Property ExigeCumplimiento As Short
        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property Orden As String
        <JsonProperty>
        Public Property Seccion As Integer
        <JsonProperty>
        Public Property CodigoFormulario As Integer
        <JsonProperty>
        Public Property Relevante As Short
        <JsonProperty>
        Public Property Tamano As Integer
        <JsonProperty>
        Public Property TipoControl As ValorCatalogos
        <JsonProperty>
        Public Property EstadoDocumento As ValorCatalogos
    End Class
End Namespace
