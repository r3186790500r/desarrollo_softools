﻿EncoExpresApp.factory('PlanillaPaqueteriaFactory', ['$http', function ($http) {

    var urlService = GetUrl('PlanillaPaqueteria');
    var factory = {};
    var request = {
        method: '',
        url: '',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        data: {}
    };

    factory.Consultar = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Consultar';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.Obtener = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Obtener';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };

    factory.Anular = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Anular';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.Guardar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Guardar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.GenerarManifiesto = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/GenerarManifiesto';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.EliminarGuia = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/EliminarGuia';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.EliminarGuiaRetornoContado = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/EliminarGuiaRetornoContado';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.EliminarRecoleccion = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/EliminarRecoleccion';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.ConsultarPlanillasPendientesRecepcionar = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarPlanillasPendientesRecepcionar';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.CalcularAnticipo = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/CalcularAnticipo';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.CalcularTotales = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/CalcularTotales';
        request.data = entidad;if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };

    factory.ObtenerPlanilla = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ObtenerPlanilla';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };

    return factory;
}]);