﻿EncoExpresApp.controller("GestionarCierreContableCtrl", ['$scope', 'ValorCatalogosFactory', '$timeout', '$linq', 'CierreContableDocumentosFactory', 'blockUI', 'TipoDocumentosFactory',
    function ($scope, ValorCatalogosFactory, $timeout, $linq, CierreContableDocumentosFactory, blockUI, TipoDocumentosFactory) {

        $scope.MapaSitio = [{ Nombre: 'Contabilidad' }, { Nombre: 'Procesos' }, { Nombre: 'Cierre Contable' }];

        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        $scope.Codigo = 0;
        $scope.ListadoTipoDocumentos = [];
        $scope.ListadoTipoDocumentosGrid = [];
        $scope.DeshabilitarFiltroTipoDocumentos = true;
        var filtros = {};

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CIERRE_CONTABLE); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/
        /*Cargar el combo de meses*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_MES } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Nombre: 'Seleccione mes', Codigo: 0 })
                        $scope.ListadoMeses = response.data.Datos
                        $scope.Mes = $linq.Enumerable().From($scope.ListadoMeses).First('$.Codigo == 0');
                    } else {
                        $scope.ListadoMeses = []
                    }
                }
            }, function (response) {
            });
        $scope.ListadoEstadosCierre = [];
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 180 } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Nombre: "Todos", Codigo: -1 });
                        $scope.ListadoEstadosCierre = response.data.Datos
                        $scope.ModeloEstado = $linq.Enumerable().From($scope.ListadoEstadosCierre).First('$.Codigo == -1');
                    } else {
                        $scope.ListadoMeses = []
                    }
                }
            }, function (response) {
            });

        /*Cargar el combo de Tipo Documentos*/
        TipoDocumentosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,  Estado: 1, CierreContable: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Nombre: 'Seleccione', Codigo: 0 })
                        console.log(response.data.Datos);
                        $scope.ListadoTipoDocumentos = response.data.Datos

                        $scope.TipoDocumento = $linq.Enumerable().From($scope.ListadoTipoDocumentos).First('$.Codigo == 0');
                    } else {
                        $scope.ListadoTipoDocumentos = [];
                    }
                }
            }, function (response) {
            });

        /*Metodo buscar*/
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                if (DatosRequeridos()) {
                    Find()
                }
            }
        };
        /*-------------------------------------------------------------------------------------------Funcion Buscar----------------------------------------------------------------*/
        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoTipoDocumentosGrid = [];

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Ano: $scope.Ano,
                Mes: $scope.Mes,
                TipoDocumentos: $scope.TipoDocumento,
                EstadoCierre: $scope.ModeloEstado
            };

            if ($scope.MensajesError.length == 0) {
                blockUI.delay = 1000;
                CierreContableDocumentosFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                response.data.Datos.forEach(function (item) {
                                    $scope.ListadoTipoDocumentosGrid.push(item);
                                    item.Nombre = item.TipoDocumentos.Nombre
                                    item.Codigo = item.TipoDocumentos.Codigo
                                });
                                $scope.Codigo = 1;
                                $scope.DeshabilitarFiltroTipoDocumentos = false;
                                $scope.ResultadoSinRegistros = "";
                            }
                            else {
                                //TipoDocumentosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Pagina: 1, RegistrosPagina: 100, Estado: 1, CierreContable: 1 }).
                                //    then(function (response) {
                                //        if (response.data.ProcesoExitoso === true) {
                                //            if (response.data.Datos.length > 0) {
                                //                response.data.Datos.forEach(function (item) {
                                //                    if (item.Codigo > 0) {
                                //                        item.EstadoCierre = { Codigo: 18001 };
                                //                        $scope.ListadoTipoDocumentosGrid.push(JSON.parse(JSON.stringify(item)));
                                //                    }
                                //                });
                                //                $scope.Codigo = 0;
                                //                $scope.DeshabilitarFiltroTipoDocumentos = true;
                                //                $scope.TipoDocumento = $linq.Enumerable().From($scope.ListadoTipoDocumentos).First('$.Codigo == 0');
                                //            } else {
                                //                $scope.ListadoTipoDocumentosGrid = [];
                                //            }
                                //        }
                                //    }, function (response) {
                                    //});
                                $scope.ResultadoSinRegistros = "Sin Registros";
                            }
                        }
                    }, function (response) {
                        $scope.Buscando = false;
                    });
            } else {
                $scope.Buscando = false;
            }
            blockUI.stop();
        };

        /*-------------------------------------------------------------------------------------------Funcion Guardar----------------------------------------------------------------*/
        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            $scope.ListadoDocumentosCierre = [];

            $scope.ListadoTipoDocumentosGrid.forEach(function (item) {
                var Datos = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Ano: $scope.Ano,
                    Mes: $scope.Mes,
                    TipoDocumentos: { Codigo: item.Codigo },
                    EstadoCierre: item.EstadoCierre,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                };
                $scope.ListadoDocumentosCierre.push(Datos)
            });

            CierreContableDocumentosFactory.Guardar({ Codigo: $scope.Codigo, ListadoDocumentosCierre: $scope.ListadoDocumentosCierre }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Codigo == 0) {
                                ShowSuccess('Se guardó el cierre contable gestionado para el mes de ' + $scope.Mes.Nombre + ' del año ' + $scope.Ano);
                            } else {
                                ShowSuccess('Se modificó el cierre contable gestionado para el mes de ' + $scope.Mes.Nombre + ' del año ' + $scope.Ano);
                            }
                            Find();
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }
                    else {
                        ShowError(response.statusText);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

        };

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;

            if ($scope.Ano === undefined || $scope.Ano === '' || $scope.Ano === null || $scope.Ano === 0) {
                $scope.MensajesError.push('Debe ingresar el año');
                continuar = false;
            } else {
                if ($scope.Ano < 2017) {
                    $scope.MensajesError.push('Debe ingresar un año superior o igual a 2017');
                    continuar = false;
                }
            }
            if ($scope.Mes === undefined || $scope.Mes === '' || $scope.Mes === null || $scope.Mes.Codigo === 0) {
                $scope.MensajesError.push('Debe seleccionar mes');
                continuar = false;
            }

            return continuar;
        }

        /*-----------------------------------------------------------------------------Funcion cambiar estado-----------------------------------------------------------------------------*/
        $scope.CambiarEstado = function (item) {
            if (item.EstadoCierre.Codigo === 18002) {
                item.EstadoCierre.Codigo = 18001
            } else {
                item.EstadoCierre.Codigo = 18002
            }
            //$scope.ListadoTipoDocumentosGrid.push(item);
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };

    }]);