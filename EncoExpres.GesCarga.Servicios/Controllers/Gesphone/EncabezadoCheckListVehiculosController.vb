﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Gesphone
Imports EncoExpres.GesCarga.Negocio.Gesphone

''' <summary>
''' Controlador <see cref="EncabezadoCheckListVehiculosController"/>
''' </summary>
<Authorize>
Public Class EncabezadoCheckListVehiculosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As EncabezadoCheckListVehiculos) As Respuesta(Of IEnumerable(Of EncabezadoCheckListVehiculos))
        Return New LogicaEncabezadoCheckListVehiculos(CapaPersistenciaEncabezadoCheckListVehiculos, CapaPersistenciaDetalleCheckListVehiculos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As EncabezadoCheckListVehiculos) As Respuesta(Of EncabezadoCheckListVehiculos)
        Return New LogicaEncabezadoCheckListVehiculos(CapaPersistenciaEncabezadoCheckListVehiculos, CapaPersistenciaDetalleCheckListVehiculos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As EncabezadoCheckListVehiculos) As Respuesta(Of Long)
        Return New LogicaEncabezadoCheckListVehiculos(CapaPersistenciaEncabezadoCheckListVehiculos, CapaPersistenciaDetalleCheckListVehiculos).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As EncabezadoCheckListVehiculos) As Respuesta(Of Boolean)
        Return New LogicaEncabezadoCheckListVehiculos(CapaPersistenciaEncabezadoCheckListVehiculos, CapaPersistenciaDetalleCheckListVehiculos).Anular(entidad)
    End Function
End Class
