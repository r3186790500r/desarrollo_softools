﻿Imports EncoExpres.GesCarga.Entidades.ControlTrafico
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources

Namespace ControlTrafico
    Public NotInheritable Class LogicaSeguimientoTransportadoraGPS
        Inherits LogicaBase(Of SeguimientoTransportadoraGPS)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of SeguimientoTransportadoraGPS)

        Sub New(persistencia As IPersistenciaBase(Of SeguimientoTransportadoraGPS))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As SeguimientoTransportadoraGPS) As Respuesta(Of IEnumerable(Of SeguimientoTransportadoraGPS))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of SeguimientoTransportadoraGPS))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of SeguimientoTransportadoraGPS))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As SeguimientoTransportadoraGPS) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            If entidad.Codigo = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function
        Public Function Anular(entidad As SeguimientoTransportadoraGPS) As Respuesta(Of Boolean)
            Throw New NotImplementedException()
        End Function
        Public Overrides Function Obtener(filtro As SeguimientoTransportadoraGPS) As Respuesta(Of SeguimientoTransportadoraGPS)
            Throw New NotImplementedException()
        End Function
        Private Function DatosRequeridos(entidad As SeguimientoTransportadoraGPS) As Respuesta(Of SeguimientoTransportadoraGPS)
            Throw New NotImplementedException()
        End Function

    End Class

End Namespace