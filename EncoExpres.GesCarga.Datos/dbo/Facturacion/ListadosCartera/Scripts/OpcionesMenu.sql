﻿Print 'Listado Cartera Resumida'
GO
Insert Into Menu_Aplicaciones(
EMPR_Codigo,
Codigo,
MOAP_Codigo,
Nombre,
Padre_Menu,
Orden_Menu,
Mostrar_Menu,
Aplica_Habilitar,
Aplica_Consultar,
Aplica_Actualizar,
Aplica_Eliminar_Anular,
Aplica_Imprimir,
Aplica_Contabilidad,
Opcion_Permiso,
Opcion_Listado,
Aplica_Ayuda,
Url_Pagina,
Url_Ayuda,
Imagen
)
select Codigo, 900301, 90, 'Cartera Resumida', 9003, 10, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 'lstCarteraResumida', '#', NULL  from Empresas
GO
-------------------------------------------------------------------
Print 'Listado Cartera Detallada'
GO
Insert Into Menu_Aplicaciones(
EMPR_Codigo,
Codigo,
MOAP_Codigo,
Nombre,
Padre_Menu,
Orden_Menu,
Mostrar_Menu,
Aplica_Habilitar,
Aplica_Consultar,
Aplica_Actualizar,
Aplica_Eliminar_Anular,
Aplica_Imprimir,
Aplica_Contabilidad,
Opcion_Permiso,
Opcion_Listado,
Aplica_Ayuda,
Url_Pagina,
Url_Ayuda,
Imagen
)
select Codigo, 900302, 90, 'Cartera Detallada', 9003, 20, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 'lstCarteraDetallada', '#', NULL  from Empresas
GO