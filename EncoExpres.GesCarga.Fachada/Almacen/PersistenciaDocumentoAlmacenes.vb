﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Almacen
Imports EncoExpres.GesCarga.Repositorio.Almacen

Namespace Almacen
    Public NotInheritable Class PersistenciaDocumentoAlmacenes
        Implements IPersistenciaBase(Of DocumentoAlmacenes)
        Public Function Consultar(filtro As DocumentoAlmacenes) As IEnumerable(Of DocumentoAlmacenes) Implements IPersistenciaBase(Of DocumentoAlmacenes).Consultar
            Return New RepositorioDocumentoAlmacenes().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As DocumentoAlmacenes) As Long Implements IPersistenciaBase(Of DocumentoAlmacenes).Insertar
            Return New RepositorioDocumentoAlmacenes().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As DocumentoAlmacenes) As Long Implements IPersistenciaBase(Of DocumentoAlmacenes).Modificar
            Return New RepositorioDocumentoAlmacenes().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As DocumentoAlmacenes) As DocumentoAlmacenes Implements IPersistenciaBase(Of DocumentoAlmacenes).Obtener
            Return New RepositorioDocumentoAlmacenes().Obtener(filtro)
        End Function
        Public Function Anular(entidad As DocumentoAlmacenes) As Boolean
            Return New RepositorioDocumentoAlmacenes().Anular(entidad)
        End Function

    End Class
End Namespace
