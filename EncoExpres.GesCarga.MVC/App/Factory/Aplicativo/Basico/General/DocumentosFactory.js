﻿EncoExpresApp.factory('DocumentosFactory', ['$http', function ($http) {
    var urlService = GetUrl('Documentos');
    var factory = {};
    var request = {
        method: '',
        url: '',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        data: {}
    }


    factory.InsertarTemporal = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/InsertarTemporal';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.Obtener = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Obtener';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.EliminarDocumento = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/EliminarDocumento';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.GuardarDocumentoTercero = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/GuardarDocumentoTercero';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.InsertarDocumentosCumplidoRemesa = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/InsertarDocumentosCumplidoRemesa';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.EliminarDocumentoCumplidoRemesa = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/EliminarDocumentoCumplidoRemesa';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.InsertarDocumentoRemesa = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/InsertarDocumentoRemesa';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.EliminarDocumentoRemesa = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/EliminarDocumentoRemesa';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    return factory;
}]);