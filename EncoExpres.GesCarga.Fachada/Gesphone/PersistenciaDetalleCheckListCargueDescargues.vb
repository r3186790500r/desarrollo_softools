﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Gesphone
Imports EncoExpres.GesCarga.Repositorio.Gesphone

Namespace Gesphone
    Public NotInheritable Class PersistenciaDetalleCheckListCargueDescargues
        Implements IPersistenciaBase(Of DetalleCheckListCargueDescargues)

        Public Function Consultar(filtro As DetalleCheckListCargueDescargues) As IEnumerable(Of DetalleCheckListCargueDescargues) Implements IPersistenciaBase(Of DetalleCheckListCargueDescargues).Consultar
            Return New RepositorioDetalleCheckListCargueDescargues().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As DetalleCheckListCargueDescargues) As Long Implements IPersistenciaBase(Of DetalleCheckListCargueDescargues).Insertar
            Return New RepositorioDetalleCheckListCargueDescargues().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As DetalleCheckListCargueDescargues) As Long Implements IPersistenciaBase(Of DetalleCheckListCargueDescargues).Modificar
            Return New RepositorioDetalleCheckListCargueDescargues().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As DetalleCheckListCargueDescargues) As DetalleCheckListCargueDescargues Implements IPersistenciaBase(Of DetalleCheckListCargueDescargues).Obtener
            Return New RepositorioDetalleCheckListCargueDescargues().Obtener(filtro)
        End Function
        Public Function Anular(entidad As DetalleCheckListCargueDescargues) As Boolean
            Return New RepositorioDetalleCheckListCargueDescargues().Anular(entidad)
        End Function
    End Class

End Namespace

