﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 09/01/2019
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	
PRINT 'gsp_modificar_encabezado_inspeccion_preoperacional'
GO
DROP PROCEDURE gsp_modificar_encabezado_inspeccion_preoperacional
GO
CREATE PROCEDURE gsp_modificar_encabezado_inspeccion_preoperacional 
(      
@par_EMPR_Codigo SMALLINT,      
@par_Numero NUMERIC,      
@par_TIDO_Codigo NUMERIC,       
@par_EFIN_Codigo NUMERIC,      
@par_Fecha_Inspeccion DATETIME,      
@par_Tiempo_Vigencia SMALLINT,      
@par_Codigo_Alterno VARCHAR(20) = NULL,      
@par_VEHI_Codigo NUMERIC,      
@par_SEMI_Codigo NUMERIC,      
@par_TERC_Codigo_Cliente NUMERIC,      
@par_TERC_Codigo_Conductor NUMERIC,      
@par_TERC_Codigo_Transportador NUMERIC,      
@par_Fecha_Hora_Inicio_Inspeccion DATETIME = NULL,      
@par_Fecha_Hora_Fin_Inspeccion DATETIME = NULL,      
@par_SICD_Codigo_Cargue NUMERIC,      
@par_SICD_Codigo_Descargue NUMERIC,      
@par_Observaciones VARCHAR(150) = NULL,      
@par_TERC_Codigo_Responsable NUMERIC,      
@par_Firma IMAGE = NULL,      
@par_TERC_Codigo_Autoriza NUMERIC = NULL,  
@par_Fecha_Autoriza DATETIME,      
@par_Estado SMALLINT,      
@par_USUA_Codigo_Modifica SMALLINT    
)      
AS      
BEGIN        
 UPDATE Encabezado_Inspecciones   
 SET               
 EFIN_Codigo = @par_EFIN_Codigo,      
 Fecha_Inspeccion = @par_Fecha_Inspeccion,      
 Tiempo_Vigencia = @par_Tiempo_Vigencia,      
 Codigo_Alterno = ISNULL(@par_Codigo_Alterno,''),      
 VEHI_Codigo = @par_VEHI_Codigo,      
 SEMI_Codigo = @par_SEMI_Codigo,      
 TERC_Codigo_Cliente = @par_TERC_Codigo_Cliente,      
 TERC_Codigo_Conductor = @par_TERC_Codigo_Conductor,      
 TERC_Codigo_Transportador = @par_TERC_Codigo_Transportador,      
 Fecha_Hora_Inicio_Inspeccion = @par_Fecha_Hora_Inicio_Inspeccion,      
 Fecha_Hora_Fin_Inspeccion = @par_Fecha_Hora_Fin_Inspeccion,      
 SICD_Codigo_Cargue = @par_SICD_Codigo_Cargue,      
 SICD_Codigo_Descargue = @par_SICD_Codigo_Descargue,      
 Observaciones = ISNULL(@par_Observaciones,''),      
 TERC_Codigo_Responsable = @par_TERC_Codigo_Responsable,
 TERC_Codigo_Autoriza = @par_TERC_Codigo_Autoriza,  
 Fecha_Autoriza = @par_Fecha_Autoriza,         
 Firma = @par_Firma,         
 Estado = @par_Estado,      
 Fecha_Modifica = GETDATE(),      
 USUA_Codigo_Modifica = @par_USUA_Codigo_Modifica     
 WHERE      
 EMPR_Codigo = @par_EMPR_Codigo      
 AND Numero = @par_Numero      
         
SELECT  Numero from Encabezado_Inspecciones where Numero = @par_Numero  
      
END      
GO