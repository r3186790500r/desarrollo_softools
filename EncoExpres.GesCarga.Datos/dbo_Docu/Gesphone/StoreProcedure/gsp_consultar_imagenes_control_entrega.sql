﻿-- Creado por: Anyelo Amado
-- Fecha Creación: 18/10/2021 
-- Módulo: Gesphone

PRINT 'gsp_consultar_imagenes_control_entrega'
GO
DROP PROCEDURE gsp_consultar_imagenes_control_entrega
GO
CREATE PROCEDURE dbo.gsp_consultar_imagenes_control_entrega      
(                      
  @par_EMPR_Codigo SMALLINT,
  @par_Numero INTEGER
)                      
AS                       
BEGIN
 
  SELECT 0 AS EsFirma, Foto AS Imagen
  FROM Foto_Detalle_Distribucion_Remesas    
  WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENRE_Numero = @par_Numero  
                  
END    
GO 