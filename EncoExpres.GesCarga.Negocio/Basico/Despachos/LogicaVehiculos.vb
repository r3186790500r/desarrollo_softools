﻿Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Fachada.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios
Imports EncoExpres.GesCarga.Entidades.Basico.General
Namespace Basico.Despachos
    Public NotInheritable Class LogicaVehiculos
        Inherits LogicaBase(Of Vehiculos)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of Vehiculos)

        Sub New(persistencia As IPersistenciaBase(Of Vehiculos))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As Vehiculos) As Respuesta(Of IEnumerable(Of Vehiculos))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Vehiculos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of Vehiculos))(consulta)
        End Function

        Public Function ConsultarPropietarios(filtro As Vehiculos) As Respuesta(Of IEnumerable(Of PropietarioVehiculo))
            Dim consulta = CType(_persistencia, PersistenciaVehiculos).ConsultarPropietarios(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of PropietarioVehiculo))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of PropietarioVehiculo))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As Vehiculos) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Codigo = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Function GuardarPropietarios(entidad As Vehiculos) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If


            valor = CType(_persistencia, PersistenciaVehiculos).InsertarPropietarios(entidad)



            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Function GuardarNovedad(entidad As Vehiculos) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            Dim consulta = CType(_persistencia, PersistenciaVehiculos).GuardarNovedad(entidad)

            If consulta.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function


        Public Overrides Function Obtener(filtro As Vehiculos) As Respuesta(Of Vehiculos)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of Vehiculos)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of Vehiculos)(consulta)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="entidad"></param>
        ''' <returns></returns>
        Private Function DatosRequeridos(entidad As Vehiculos) As Respuesta(Of Vehiculos)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of Vehiculos) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of Vehiculos) With {.ProcesoExitoso = True}

        End Function
        Public Function Anular(entidad As Vehiculos) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaVehiculos).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function

        Public Function ObtenerVehiculoEstudioSeguridad(filtro As EstudioSeguridad) As Respuesta(Of EstudioSeguridad)

            Dim consulta = CType(_persistencia, PersistenciaVehiculos).ConsultarVehiculoEstudioSeguridad(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of EstudioSeguridad)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of EstudioSeguridad)(consulta)
        End Function

        Public Function ConsultarDocumentosProximosVencer(filtro As VehiculosDocumentos) As Respuesta(Of IEnumerable(Of VehiculosDocumentos))
            Dim consulta = CType(_persistencia, PersistenciaVehiculos).ConsultarDocumentosProximosVencer(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of VehiculosDocumentos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of VehiculosDocumentos))(consulta)
        End Function

        Public Function ConsultarDocumentos(filtro As VehiculosDocumentos) As Respuesta(Of IEnumerable(Of VehiculosDocumentos))
            Dim consulta = CType(_persistencia, PersistenciaVehiculos).ConsultarDocumentos(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of VehiculosDocumentos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of VehiculosDocumentos))(consulta)
        End Function

        Public Function ConsultarDocumentosListaDocumentosPendientes(filtro As VehiculosDocumentos) As Respuesta(Of IEnumerable(Of VehiculosDocumentos))
            Dim consulta = CType(_persistencia, PersistenciaVehiculos).ConsultarDocumentosListaDocumentosPendientes(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of VehiculosDocumentos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of VehiculosDocumentos))(consulta)
        End Function

        Public Function ConsultarDocumentosSoatRTM(entidad As VehiculosDocumentos) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim VehiculoValido As Boolean = False

            VehiculoValido = CType(_persistencia, PersistenciaVehiculos).ConsultarDocumentosSoatRTM(entidad)

            If Not VehiculoValido Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function
        Public Function GenerarPlanitilla(entidad As Vehiculos) As Respuesta(Of Vehiculos)
            Dim mensajeGuardo = "Genero Archivo"
            Dim url As New Vehiculos
            url = CType(_persistencia, PersistenciaVehiculos).GenerarPlanitilla(entidad)
            Return New Respuesta(Of Vehiculos)(url)
        End Function
        Public Function ConsultarVehiculoListaNegra(filtro As Vehiculos) As Respuesta(Of IEnumerable(Of Vehiculos))
            Dim consulta = CType(_persistencia, PersistenciaVehiculos).ConsultarVehiculosListaNegra(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Vehiculos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of Vehiculos))(consulta)

        End Function

        Public Function ConsultarEstadoListaNegra(entidad As VehiculosDocumentos) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim VehiculoValido As Boolean = False

            VehiculoValido = CType(_persistencia, PersistenciaVehiculos).ConsultarEstadoListaNegra(entidad)

            If Not VehiculoValido Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function

        Public Function ConsultarAuditoria(filtro As Vehiculos) As Respuesta(Of IEnumerable(Of Vehiculos))
            Dim consulta = CType(_persistencia, PersistenciaVehiculos).ConsultarAuditoria(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Vehiculos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of Vehiculos))(consulta)

        End Function


    End Class


End Namespace