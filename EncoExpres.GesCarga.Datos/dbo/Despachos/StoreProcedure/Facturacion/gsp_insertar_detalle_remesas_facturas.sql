﻿Print 'gsp_insertar_detalle_remesas_facturas'
GO
DROP PROCEDURE gsp_insertar_detalle_remesas_facturas
GO
CREATE PROCEDURE gsp_insertar_detalle_remesas_facturas (
@par_EMPR_Codigo	smallint,
@par_ENFA_Numero	numeric,
@par_ENRE_Numero	numeric,
@par_ENOS_Numero	numeric
)
AS
BEGIN

	INSERT INTO Detalle_Remesas_Facturas (
	EMPR_Codigo,
	ENFA_Numero,
	ENRE_Numero,
	ENOS_Numero
	)
	VALUES(
	@par_EMPR_Codigo,
	@par_ENFA_Numero,
	@par_ENRE_Numero,
	@par_ENOS_Numero
	)

	--Amarra remesa con la factura
	UPDATE Encabezado_Remesas SET ENFA_Numero = @par_ENFA_Numero 
	WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero = @par_ENRE_Numero

	SELECT TOP 1 ENFA_Numero FROM Detalle_Remesas_Facturas
	WHERE EMPR_Codigo = @par_EMPR_Codigo
	AND ENFA_Numero = @par_ENFA_Numero
	AND ENRE_Numero = @par_ENRE_Numero

END
GO