﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.ControlViajes
Imports EncoExpres.GesCarga.Negocio.ControlViajes

''' <summary>
''' Controlador <see cref="InspeccionPreoperacionalController"/>
''' </summary>
<Authorize>
Public Class InspeccionPreoperacionalController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As InspeccionPreoperacional) As Respuesta(Of IEnumerable(Of InspeccionPreoperacional))
        Return New LogicaInspeccionPreoperacional(CapaPersistenciaInspeccionPreoperacional).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As InspeccionPreoperacional) As Respuesta(Of InspeccionPreoperacional)
        Return New LogicaInspeccionPreoperacional(CapaPersistenciaInspeccionPreoperacional).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As InspeccionPreoperacional) As Respuesta(Of Long)
        Return New LogicaInspeccionPreoperacional(CapaPersistenciaInspeccionPreoperacional).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As InspeccionPreoperacional) As Respuesta(Of Boolean)
        Return New LogicaInspeccionPreoperacional(CapaPersistenciaInspeccionPreoperacional).Anular(entidad)
    End Function

    <HttpPost>
    <ActionName("ObtenerDocumentosDespachoActivosVehiculo")>
    Public Function ObtenerDocumentosDespachoActivosVehiculo(ByVal entidad As InspeccionPreoperacional) As Respuesta(Of InspeccionPreoperacional)
        Return New LogicaInspeccionPreoperacional(CapaPersistenciaInspeccionPreoperacional).ObtenerDocumentoActivo(entidad)
    End Function

    <HttpPost>
    <ActionName("ObtenerUltimaInspeccion")>
    Public Function ObtenerUltimaInspeccion(ByVal entidad As InspeccionPreoperacional) As Respuesta(Of InspeccionPreoperacional)
        Return New LogicaInspeccionPreoperacional(CapaPersistenciaInspeccionPreoperacional).ObtenerUltimaInspeccion(entidad)
    End Function
End Class
