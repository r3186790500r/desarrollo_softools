﻿'Imports Microsoft.VisualBasic
'Imports System.Data
'Imports System.Data.SqlClient
'Imports System

'Namespace Entidades.ReporteMinisterio
'    Public Class Empresas

'#Region "Declaracion Variables"
'        Dim strSQL As String
'        Dim objGeneral As General
'        Public strError As String
'        Public bolModificar As Boolean

'        Private lonCodigo As Long
'        Private strNombre As String
'        Private objCataTipoIdentificacion As Catalogo
'        Private strNumeroIdentificacion As String
'        Private strRepresentanteLegal As String

'        Private objCataTipoIdentificacionRepresentante As Catalogo
'        Private strIdentificacionRepresentante As String
'        Private objCiudad As Ciudad
'        Private strDireccion As String
'        Private strTelefono1 As String

'        Private strTelefono2 As String
'        Private strFax As String
'        Private strEmail As String
'        Private strPaginaWeb As String
'        Private objAseguradora As Tercero

'        Private strNombreAseguradora As String
'        Private strNitAseguradora As String
'        Private strNumeroPoliza As String
'        Private dteFechaPoliza As Date
'        Private intAplicaReteIva As Integer

'        Private dblPorcentajeReteIva As Double
'        Private dblSalarioMinimo As Double
'        Private dblValorSeguroSocial As Double
'        Private dblValorParafiscales As Double
'        Private dblSeguroCamionero As Double

'        Private dblPorcentajeAsistenciaCarretera As Double
'        Private dblPorcentajeVariacionAnticipos As Double
'        Private objTercero As Tercero
'        Private intFleteHorarios As Integer
'        Private strCodigoRegionalMinisterio As String

'        Private lonCodigoMinisterio As Long
'        Private strRangoNumeracionManifiesto As String
'        Private dteFechaUltimoEnvio As Date
'        Private objCataTipoNumeracion As Catalogo
'        Private dblPorcentajeIntermediacion As Double

'        Private dblRetencionManifiesto As Double
'        Private dblTarifaRetencionIcaManifiesto As Double
'        Private objCataDocumentoInicialTransporte As Catalogo
'        Private objCataTipoParametrizacionMargenRentabilidad As Catalogo
'        Private dblMargenRentabilidadDespachoNacionalVehiculoPropio As Double

'        Private dblMargenRentabilidadDespachoUrbanoVehiculoPropio As Double
'        Private dblMargenRentabilidadDespachoNacionalVehiculoTercero As Double
'        Private dblMargenRentabilidadDespachoUrbanoVehiculoTercero As Double
'        Private dblMultaDiaEntregaCumplido As Double
'        Private intDiasMaximoCumplimiento As Integer

'        Private strMensaje As String
'        Private objUsuario As Usuario
'        Private intNumeroMaximoUsuarios As Integer
'        Private objCataTipoComisionConductor As Catalogo
'        Private dblPorcentajeGeneralComisionConductor As Double

'        Private intMesesVigenciaEstudioSeguridadPropios As Integer
'        Private intMesesVigenciaEstudioSeguridadTerceros As Integer
'        Private strPrefijo As String
'        Private dblTasaRepresentativaMercado As Double
'        Private strObservacionManifiesto As String

'        Private objCataPeriodoMovimientoContableAnulacion As Catalogo
'        Private dblPorcentajeSeguro As Double
'        Private objCataPais As Catalogo
'        Private dblMontoCubrePoliza As Double
'        Private dblValorTopeRetefuenteServicios As Double

'        Private dblValorComercialMinimo As Double
'        Private dblValorSeguroMinimo As Double
'        Private strUsuarioManifiestoElectronico As String
'        Private strClaveManifiestoElectronico As String
'        Private lonNitManifiestoElectronico As Long

'        Private intDiasInactivaConductorUltimoCargue As Byte
'        Private intCantidadViajesTrafico As Integer
'        Private dblRetencionContratistas As Double
'        Private dblValorBaseRetencionContratistas As Double
'        Private lonCodigoUIAF As Long

'        Public strCodigoUsuario As String
'        Public dblRetencionCREE As Double
'        Private intMaximoCuotasRecuperacionCartera As Short

'        Public Const COMISION_CONDUCTOR_GENERAL As Short = 1
'        Public Const COMISION_CONDUCTOR_POR_RUTA As Short = 2
'        Public Const COMISION_CONDUCTOR_POR_VEHICULO As Short = 3

'        Public Const MARGEN_RENTABILIDAD_POR_EMPRESA As Integer = 1
'        Public Const MARGEN_RENTABILIDAD_POR_CLIENTE As Integer = 2

'#End Region

'#Region "Constructor"

'        Public Sub New()
'            Me.objGeneral = New General()
'            Me.lonCodigo = 1
'            Me.strNombre = ""
'            Me.objCataTipoIdentificacion = New Catalogo(Me, "TIID")
'            Me.strRepresentanteLegal = ""

'            Me.objCataTipoIdentificacionRepresentante = New Catalogo(Me, "TIID")
'            Me.strIdentificacionRepresentante = ""
'            Me.objCiudad = New Ciudad(Me)
'            Me.strDireccion = ""
'            Me.strTelefono1 = ""

'            Me.strTelefono2 = ""
'            Me.strFax = ""
'            Me.strEmail = ""
'            Me.strPaginaWeb = ""
'            Me.objAseguradora = New Tercero(Me)

'            Me.strNombreAseguradora = ""
'            Me.strNitAseguradora = ""
'            Me.strNumeroPoliza = ""
'            Me.intAplicaReteIva = 0
'            Me.dblPorcentajeReteIva = 0

'            Me.dblSalarioMinimo = 0
'            Me.dblValorParafiscales = 0
'            Me.dblValorSeguroSocial = 0
'            Me.dblSeguroCamionero = 0
'            Me.dblPorcentajeAsistenciaCarretera = 0

'            Me.dblPorcentajeVariacionAnticipos = 0
'            Me.objTercero = New Tercero(Me)
'            Me.intFleteHorarios = 0

'            Me.strCodigoRegionalMinisterio = ""
'            Me.lonCodigoMinisterio = 0
'            Me.strRangoNumeracionManifiesto = ""
'            Me.objCataTipoNumeracion = New Catalogo(Me, "TINU")

'            Me.dblPorcentajeIntermediacion = 0
'            Me.dblRetencionManifiesto = 0
'            Me.dteFechaPoliza = Date.Today()

'            Me.dteFechaUltimoEnvio = Date.MinValue
'            Me.objCataDocumentoInicialTransporte = New Catalogo(Me, "DITR")
'            Me.dblTarifaRetencionIcaManifiesto = 0
'            Me.strMensaje = ""

'            Me.objCataTipoParametrizacionMargenRentabilidad = New Catalogo(Me, "TPMR")
'            Me.dblMargenRentabilidadDespachoNacionalVehiculoPropio = 0
'            Me.dblMargenRentabilidadDespachoUrbanoVehiculoPropio = 0
'            Me.dblMargenRentabilidadDespachoNacionalVehiculoTercero = 0
'            Me.dblMargenRentabilidadDespachoUrbanoVehiculoTercero = 0

'            Me.dblMultaDiaEntregaCumplido = 0
'            Me.intDiasMaximoCumplimiento = 0
'            Me.objCataTipoComisionConductor = New Catalogo(Me, "TCCO")
'            Me.dblPorcentajeGeneralComisionConductor = 0
'            Me.intMesesVigenciaEstudioSeguridadPropios = 0

'            Me.intMesesVigenciaEstudioSeguridadTerceros = 0
'            Me.strMensaje = ""
'            Me.strPrefijo = ""
'            Me.dblTasaRepresentativaMercado = 0
'            Me.objCataPeriodoMovimientoContableAnulacion = New Catalogo(Me, "PMCA")

'            Me.objCataPeriodoMovimientoContableAnulacion.Campo1 = Tesoreria.PRIMER_PERIODO_ABIERTO  ' Remplazar por un campo se capture desde la página
'            Me.strObservacionManifiesto = String.Empty
'            Me.dblPorcentajeSeguro = 0
'            Me.objCataPais = New Catalogo(Me, "PAIS")
'            Me.dblMontoCubrePoliza = 0

'            Me.strUsuarioManifiestoElectronico = String.Empty
'            Me.strClaveManifiestoElectronico = String.Empty
'            Me.lonNitManifiestoElectronico = 0

'            Me.intDiasInactivaConductorUltimoCargue = 0
'            Me.intCantidadViajesTrafico = 0

'            Me.dblValorTopeRetefuenteServicios = 0
'            Me.dblValorComercialMinimo = 0
'            Me.dblValorSeguroMinimo = 0
'            Me.dblRetencionContratistas = 0
'            Me.dblValorBaseRetencionContratistas = 0

'            Me.lonCodigoUIAF = 0
'            Me.dblRetencionCREE = 0
'            Me.intMaximoCuotasRecuperacionCartera = 0
'        End Sub

'        Public Sub New(ByVal Codigo As Long)
'            If Codigo = 0 Then
'                Me.lonCodigo = Codigo
'                Me.strNombre = ""
'                Me.objGeneral = New General()
'            Else
'                Me.lonCodigo = Codigo
'                Me.objGeneral = New General()
'                Me.strNombre = ""
'                Me.objCataTipoIdentificacion = New Catalogo(Me, "TIID")
'                Me.strRepresentanteLegal = ""

'                Me.objCataTipoIdentificacionRepresentante = New Catalogo(Me, "TIID")
'                Me.strIdentificacionRepresentante = ""
'                Me.objCiudad = New Ciudad(Me)
'                Me.strDireccion = ""
'                Me.strTelefono1 = ""

'                Me.strTelefono2 = ""
'                Me.strFax = ""
'                Me.strEmail = ""
'                Me.strPaginaWeb = ""
'                Me.objAseguradora = New Tercero(Me)

'                Me.strNombreAseguradora = ""
'                Me.strNitAseguradora = ""
'                Me.strNumeroPoliza = ""
'                Me.intAplicaReteIva = 0
'                Me.dblPorcentajeReteIva = 0

'                Me.dblSalarioMinimo = 0
'                Me.dblValorParafiscales = 0
'                Me.dblValorSeguroSocial = 0
'                Me.dblSeguroCamionero = 0
'                Me.dblPorcentajeAsistenciaCarretera = 0

'                Me.dblPorcentajeVariacionAnticipos = 0
'                Me.objTercero = New Tercero(Me)
'                Me.intFleteHorarios = 0

'                Me.strCodigoRegionalMinisterio = ""
'                Me.lonCodigoMinisterio = 0
'                Me.strRangoNumeracionManifiesto = ""
'                Me.objCataTipoNumeracion = New Catalogo(Me, "TINU")

'                Me.dblPorcentajeIntermediacion = 0
'                Me.dblRetencionManifiesto = 0
'                Me.dteFechaPoliza = Date.Today()

'                Me.dteFechaUltimoEnvio = Date.MinValue
'                Me.objCataDocumentoInicialTransporte = New Catalogo(Me, "DITR")
'                Me.dblTarifaRetencionIcaManifiesto = 0

'                Me.objCataTipoParametrizacionMargenRentabilidad = New Catalogo(Me, "TPMR")
'                Me.dblMargenRentabilidadDespachoNacionalVehiculoPropio = 0
'                Me.dblMargenRentabilidadDespachoUrbanoVehiculoPropio = 0
'                Me.dblMargenRentabilidadDespachoNacionalVehiculoTercero = 0
'                Me.dblMargenRentabilidadDespachoUrbanoVehiculoTercero = 0

'                Me.dblMultaDiaEntregaCumplido = 0
'                Me.intDiasMaximoCumplimiento = 0
'                Me.objCataTipoComisionConductor = New Catalogo(Me, "TCCO")
'                Me.dblPorcentajeGeneralComisionConductor = 0
'                Me.intMesesVigenciaEstudioSeguridadPropios = 0

'                Me.intMesesVigenciaEstudioSeguridadTerceros = 0
'                Me.strMensaje = ""
'                Me.strPrefijo = ""
'                Me.dblTasaRepresentativaMercado = 0

'                Me.objCataPeriodoMovimientoContableAnulacion = New Catalogo(Me, "PMCA")
'                Me.objCataPeriodoMovimientoContableAnulacion.Campo1 = Tesoreria.PRIMER_PERIODO_ABIERTO  ' Remplazar por un campo se capture desde la página
'                Me.strObservacionManifiesto = String.Empty
'                Me.dblPorcentajeSeguro = 0

'                Me.objCataPais = New Catalogo(Me, "PAIS")
'                Me.dblMontoCubrePoliza = 0

'                Me.strUsuarioManifiestoElectronico = String.Empty
'                Me.strClaveManifiestoElectronico = String.Empty
'                Me.lonNitManifiestoElectronico = 0

'                Me.intDiasInactivaConductorUltimoCargue = 0
'                Me.intCantidadViajesTrafico = 0
'                Me.dblValorTopeRetefuenteServicios = 0
'                Me.dblValorComercialMinimo = 0
'                Me.dblValorSeguroMinimo = 0

'                Me.dblRetencionContratistas = 0
'                Me.dblValorBaseRetencionContratistas = 0
'                Me.lonCodigoUIAF = 0
'                Me.dblRetencionCREE = 0
'                Me.intMaximoCuotasRecuperacionCartera = 0
'            End If
'        End Sub

'#End Region

'#Region "Propiedades"

'        Public Property RetencionContratistas() As Double
'            Get
'                Return Me.dblRetencionContratistas
'            End Get
'            Set(ByVal value As Double)
'                Me.dblRetencionContratistas = value
'            End Set
'        End Property

'        Public Property ValorBaseRetencionContratistas() As Double
'            Get
'                Return Me.dblValorBaseRetencionContratistas
'            End Get
'            Set(ByVal value As Double)
'                Me.dblValorBaseRetencionContratistas = value
'            End Set
'        End Property

'        Public Property CataPais() As Catalogo
'            Get
'                Return Me.objCataPais
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataPais = value
'            End Set
'        End Property

'        Public Property MontoCubrePoliza() As Double
'            Get
'                Return Me.dblMontoCubrePoliza
'            End Get
'            Set(ByVal value As Double)
'                Me.dblMontoCubrePoliza = value
'            End Set
'        End Property


'        Public Property PorcentajeSeguro() As Double
'            Get
'                Return Me.dblPorcentajeSeguro
'            End Get
'            Set(ByVal value As Double)
'                Me.dblPorcentajeSeguro = value
'            End Set
'        End Property

'        Public Property CataPeriodoMovimientoContableAnulacion() As Catalogo
'            Get
'                Return Me.objCataPeriodoMovimientoContableAnulacion
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataPeriodoMovimientoContableAnulacion = value
'            End Set
'        End Property

'        Public Property TasaRepresentativaMercado() As Double
'            Get
'                Return Me.dblTasaRepresentativaMercado
'            End Get
'            Set(ByVal value As Double)
'                Me.dblTasaRepresentativaMercado = value
'            End Set
'        End Property

'        Public Property PorcentajeAsistenciaCarretera() As Double
'            Get
'                Return Me.dblPorcentajeAsistenciaCarretera
'            End Get
'            Set(ByVal value As Double)
'                Me.dblPorcentajeAsistenciaCarretera = value
'            End Set
'        End Property

'        Public Property TarifaRetencionIcaManifiesto() As Double
'            Get
'                Return Me.dblTarifaRetencionIcaManifiesto
'            End Get
'            Set(ByVal value As Double)
'                Me.dblTarifaRetencionIcaManifiesto = value
'            End Set
'        End Property

'        Public Property PorcentajeReteIva() As Double
'            Get
'                Return Me.dblPorcentajeReteIva
'            End Get
'            Set(ByVal value As Double)
'                Me.dblPorcentajeReteIva = value
'            End Set
'        End Property

'        Public Property PorcentajeGeneralComisionConductor() As Double
'            Get
'                Return Me.dblPorcentajeGeneralComisionConductor
'            End Get
'            Set(ByVal value As Double)
'                Me.dblPorcentajeGeneralComisionConductor = value
'            End Set
'        End Property

'        Public Property PorcentajeIntermediacion() As Double
'            Get
'                Return Me.dblPorcentajeIntermediacion
'            End Get
'            Set(ByVal value As Double)
'                Me.dblPorcentajeIntermediacion = value
'            End Set
'        End Property

'        Public Property PorcentajeVariacionAnticipos() As Double
'            Get
'                Return Me.dblPorcentajeVariacionAnticipos
'            End Get
'            Set(ByVal value As Double)
'                Me.dblPorcentajeVariacionAnticipos = value
'            End Set
'        End Property

'        Public Property RetencionManifiesto() As Double
'            Get
'                Return Me.dblRetencionManifiesto
'            End Get
'            Set(ByVal value As Double)
'                Me.dblRetencionManifiesto = value
'            End Set
'        End Property

'        Public Property SalarioMinimo() As Double
'            Get
'                Return Me.dblSalarioMinimo
'            End Get
'            Set(ByVal value As Double)
'                Me.dblSalarioMinimo = value
'            End Set
'        End Property

'        Public Property SeguroCamionero() As Double
'            Get
'                Return Me.dblSeguroCamionero
'            End Get
'            Set(ByVal value As Double)
'                Me.dblSeguroCamionero = value
'            End Set
'        End Property

'        Public Property ValorParafiscales() As Double
'            Get
'                Return Me.dblValorParafiscales
'            End Get
'            Set(ByVal value As Double)
'                Me.dblValorParafiscales = value
'            End Set
'        End Property

'        Public Property ValorSeguroSocial() As Double
'            Get
'                Return Me.dblValorSeguroSocial
'            End Get
'            Set(ByVal value As Double)
'                Me.dblValorSeguroSocial = value
'            End Set
'        End Property

'        Public Property FechaPoliza() As Date
'            Get
'                Return Me.dteFechaPoliza
'            End Get
'            Set(ByVal value As Date)
'                Me.dteFechaPoliza = value
'            End Set
'        End Property

'        Public Property FechaUltimoEnvio() As Date
'            Get
'                Return Me.dteFechaUltimoEnvio
'            End Get
'            Set(ByVal value As Date)
'                Me.dteFechaUltimoEnvio = value
'            End Set
'        End Property

'        Public Property AplicaReteIva() As Integer
'            Get
'                Return Me.intAplicaReteIva
'            End Get
'            Set(ByVal value As Integer)
'                If value < 0 Then value = value * (-1)
'                Me.intAplicaReteIva = value
'            End Set
'        End Property

'        Public Property DiasMaximoCumplimiento() As Integer
'            Get
'                Return Me.intDiasMaximoCumplimiento
'            End Get
'            Set(ByVal value As Integer)
'                Me.intDiasMaximoCumplimiento = value
'            End Set
'        End Property

'        Public Property FleteHorarios() As Integer
'            Get
'                Return Me.intFleteHorarios
'            End Get
'            Set(ByVal value As Integer)
'                Me.intFleteHorarios = value
'            End Set
'        End Property

'        Public Property MesesVigenciaEstudioSeguridadPropios() As Integer
'            Get
'                Return Me.intMesesVigenciaEstudioSeguridadPropios
'            End Get
'            Set(ByVal value As Integer)
'                Me.intMesesVigenciaEstudioSeguridadPropios = value
'            End Set
'        End Property

'        Public Property MesesVigenciaEstudioSeguridadTerceros() As Integer
'            Get
'                Return Me.intMesesVigenciaEstudioSeguridadTerceros
'            End Get
'            Set(ByVal value As Integer)
'                Me.intMesesVigenciaEstudioSeguridadTerceros = value
'            End Set
'        End Property

'        Public Property Aseguradora() As Tercero
'            Get
'                Return Me.objAseguradora
'            End Get
'            Set(ByVal value As Tercero)
'                Me.objAseguradora = value
'            End Set
'        End Property

'        Public Property Codigo() As Long
'            Get
'                Return Me.lonCodigo
'            End Get
'            Set(ByVal value As Long)
'                Me.lonCodigo = value
'            End Set
'        End Property

'        Public Property CodigoMinisterio() As Long
'            Get
'                Return Me.lonCodigoMinisterio
'            End Get
'            Set(ByVal value As Long)
'                Me.lonCodigoMinisterio = value
'            End Set
'        End Property

'        Public Property CataTipoComisionConductor() As Catalogo
'            Get
'                Return Me.objCataTipoComisionConductor
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataTipoComisionConductor = value
'            End Set
'        End Property

'        Public Property CataTipoIdentificacion() As Catalogo
'            Get
'                Return Me.objCataTipoIdentificacion
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataTipoIdentificacion = value
'            End Set
'        End Property

'        Public Property CataTipoIdentificacionRepresentante() As Catalogo
'            Get
'                Return Me.objCataTipoIdentificacionRepresentante
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataTipoIdentificacionRepresentante = value
'            End Set
'        End Property

'        Public Property CataTipoNumeracion() As Catalogo
'            Get
'                Return Me.objCataTipoNumeracion
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataTipoNumeracion = value
'            End Set
'        End Property

'        Public Property CataDocumentoInicialTransporte() As Catalogo
'            Get
'                Return Me.objCataDocumentoInicialTransporte
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataDocumentoInicialTransporte = value
'            End Set
'        End Property

'        Public Property Ciudad() As Ciudad
'            Get
'                Return Me.objCiudad
'            End Get
'            Set(ByVal value As Ciudad)
'                Me.objCiudad = value
'            End Set
'        End Property

'        Public Property Tercero() As Tercero
'            Get
'                Return Me.objTercero
'            End Get
'            Set(ByVal value As Tercero)
'                Me.objTercero = value
'            End Set
'        End Property

'        Public Property CodigoRegionalMinisterio() As String
'            Get
'                Return Me.strCodigoRegionalMinisterio
'            End Get
'            Set(ByVal value As String)
'                Me.strCodigoRegionalMinisterio = value
'            End Set
'        End Property

'        Public Property Direccion() As String
'            Get
'                Return Me.strDireccion
'            End Get
'            Set(ByVal value As String)
'                Me.strDireccion = value
'            End Set
'        End Property

'        Public Property Email() As String
'            Get
'                Return Me.strEmail
'            End Get
'            Set(ByVal value As String)
'                Me.strEmail = value
'            End Set
'        End Property

'        Public Property Fax() As String
'            Get
'                Return Me.strFax
'            End Get
'            Set(ByVal value As String)
'                Me.strFax = value
'            End Set
'        End Property

'        Public Property IdentificacionRepresentante() As String
'            Get
'                Return Me.strIdentificacionRepresentante
'            End Get
'            Set(ByVal value As String)
'                Me.strIdentificacionRepresentante = value
'            End Set
'        End Property

'        Public Property NitAseguradora() As String
'            Get
'                Return Me.strNitAseguradora
'            End Get
'            Set(ByVal value As String)
'                Me.strNitAseguradora = value
'            End Set
'        End Property

'        Public Property Nombre() As String
'            Get
'                Return Me.strNombre
'            End Get
'            Set(ByVal value As String)
'                Me.strNombre = value
'            End Set
'        End Property

'        Public Property NombreAseguradora() As String
'            Get
'                Return Me.strNombreAseguradora
'            End Get
'            Set(ByVal value As String)
'                Me.strNombreAseguradora = value
'            End Set
'        End Property

'        Public Property NumeroIdentificacion() As String
'            Get
'                Return Me.strNumeroIdentificacion
'            End Get
'            Set(ByVal value As String)
'                Me.strNumeroIdentificacion = value
'            End Set
'        End Property

'        Public Property NumeroPoliza() As String
'            Get
'                Return Me.strNumeroPoliza
'            End Get
'            Set(ByVal value As String)
'                Me.strNumeroPoliza = value
'            End Set
'        End Property

'        Public Property PaginaWeb() As String
'            Get
'                Return Me.strPaginaWeb
'            End Get
'            Set(ByVal value As String)
'                Me.strPaginaWeb = value
'            End Set
'        End Property

'        Public Property RangoNumeracionManifiesto() As String
'            Get
'                Return Me.strRangoNumeracionManifiesto
'            End Get
'            Set(ByVal value As String)
'                Me.strRangoNumeracionManifiesto = value
'            End Set
'        End Property

'        Public Property RepresentanteLegal() As String
'            Get
'                Return Me.strRepresentanteLegal
'            End Get
'            Set(ByVal value As String)
'                Me.strRepresentanteLegal = value
'            End Set
'        End Property

'        Public Property Telefono1() As String
'            Get
'                Return Me.strTelefono1
'            End Get
'            Set(ByVal value As String)
'                Me.strTelefono1 = value
'            End Set
'        End Property

'        Public Property Telefono2() As String
'            Get
'                Return Me.strTelefono2
'            End Get
'            Set(ByVal value As String)
'                Me.strTelefono2 = value
'            End Set
'        End Property

'        Public Property Mensaje() As String
'            Get
'                Return strMensaje
'            End Get
'            Set(ByVal value As String)
'                strMensaje = value
'            End Set
'        End Property

'        Public Property CataTipoParametrizacionMargenRentabilidad() As Catalogo
'            Get
'                Return Me.objCataTipoParametrizacionMargenRentabilidad
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataTipoParametrizacionMargenRentabilidad = value
'            End Set
'        End Property

'        Public Property MargenRentabilidadDespachoNacionalVehiculoPropio() As Double
'            Get
'                Return Me.dblMargenRentabilidadDespachoNacionalVehiculoPropio
'            End Get
'            Set(ByVal value As Double)
'                Me.dblMargenRentabilidadDespachoNacionalVehiculoPropio = value
'            End Set
'        End Property

'        Public Property MargenRentabilidadDespachoUrbanoVehiculoPropio() As Double
'            Get
'                Return Me.dblMargenRentabilidadDespachoUrbanoVehiculoPropio
'            End Get
'            Set(ByVal value As Double)
'                Me.dblMargenRentabilidadDespachoUrbanoVehiculoPropio = value
'            End Set
'        End Property

'        Public Property MargenRentabilidadDespachoNacionalVehiculoTercero() As Double
'            Get
'                Return Me.dblMargenRentabilidadDespachoNacionalVehiculoTercero
'            End Get
'            Set(ByVal value As Double)
'                Me.dblMargenRentabilidadDespachoNacionalVehiculoTercero = value
'            End Set
'        End Property

'        Public Property MargenRentabilidadDespachoUrbanoVehiculoTercero() As Double
'            Get
'                Return Me.dblMargenRentabilidadDespachoUrbanoVehiculoTercero
'            End Get
'            Set(ByVal value As Double)
'                Me.dblMargenRentabilidadDespachoUrbanoVehiculoTercero = value
'            End Set
'        End Property

'        Public Property MultaDiaEntregaCumplido() As Double
'            Get
'                Return Me.dblMultaDiaEntregaCumplido
'            End Get
'            Set(ByVal value As Double)
'                Me.dblMultaDiaEntregaCumplido = value
'            End Set
'        End Property

'        Public Property NumeroMaximoUsuarios() As Integer
'            Get
'                Return Me.intNumeroMaximoUsuarios
'            End Get
'            Set(ByVal value As Integer)
'                Me.intNumeroMaximoUsuarios = value
'            End Set
'        End Property

'        Public Property Prefijo() As String
'            Get
'                Return Me.strPrefijo
'            End Get
'            Set(ByVal value As String)
'                Me.strPrefijo = value
'            End Set
'        End Property

'        Public Property ObservacionManifiesto() As String
'            Get
'                Return Me.strObservacionManifiesto
'            End Get
'            Set(ByVal value As String)
'                Me.strObservacionManifiesto = value
'            End Set
'        End Property

'        Public Property UsuarioManifiestoElectronico() As String
'            Get
'                Return Me.strUsuarioManifiestoElectronico
'            End Get
'            Set(ByVal value As String)
'                Me.strUsuarioManifiestoElectronico = value
'            End Set
'        End Property

'        Public Property ClaveManifiestoElectronico() As String
'            Get
'                Return Me.strClaveManifiestoElectronico
'            End Get
'            Set(ByVal value As String)
'                Me.strClaveManifiestoElectronico = value
'            End Set
'        End Property

'        Public Property NitManifiestoElectronico() As Long
'            Get
'                Return Me.lonNitManifiestoElectronico
'            End Get
'            Set(ByVal value As Long)
'                Me.lonNitManifiestoElectronico = value
'            End Set
'        End Property

'        Public Property DiasInactivaConductorUltimoCargue() As Byte
'            Get
'                Return Me.intDiasInactivaConductorUltimoCargue
'            End Get
'            Set(ByVal value As Byte)
'                Me.intDiasInactivaConductorUltimoCargue = value
'            End Set
'        End Property

'        Public Property CantidadViajesTrafico() As Integer
'            Get
'                Return Me.intCantidadViajesTrafico
'            End Get
'            Set(ByVal value As Integer)
'                Me.intCantidadViajesTrafico = value
'            End Set
'        End Property

'        Public Property ValorTopeRetefuenteServicios() As Double
'            Get
'                Return Me.dblValorTopeRetefuenteServicios
'            End Get
'            Set(ByVal value As Double)
'                Me.dblValorTopeRetefuenteServicios = value
'            End Set
'        End Property

'        Public Property ValorComercialMinimo() As Double
'            Get
'                Return Me.dblValorComercialMinimo
'            End Get
'            Set(ByVal value As Double)
'                Me.dblValorComercialMinimo = value
'            End Set
'        End Property

'        Public Property ValorSeguroMinimo() As Double
'            Get
'                Return Me.dblValorSeguroMinimo
'            End Get
'            Set(ByVal value As Double)
'                Me.dblValorSeguroMinimo = value
'            End Set
'        End Property

'        Public Property CodigoUIAF() As Long
'            Get
'                Return Me.lonCodigoUIAF
'            End Get
'            Set(ByVal value As Long)
'                Me.lonCodigoUIAF = value
'            End Set
'        End Property

'        Public Property PorcentajeRetencionCREE As Double
'            Get
'                Return Me.dblRetencionCREE
'            End Get
'            Set(value As Double)
'                Me.dblRetencionCREE = value
'            End Set
'        End Property

'        Public Property MaximoCuotasRecuperacionCartera As Short
'            Get
'                Return Me.intMaximoCuotasRecuperacionCartera
'            End Get
'            Set(value As Short)
'                Me.intMaximoCuotasRecuperacionCartera = value
'            End Set
'        End Property

'#End Region

'#Region "Funciones Publicas"

'        Public Function Consultar() As Boolean
'            Try
'                Dim sdrEmpresa As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT *,"
'                strSQL += " CONVERT(VARCHAR(3), DECRYPTBYPASSPHRASE('" & General.CLAVE_ENCRIPTADO_BD _
'            & "', Numero_Maximo_Usuarios)) As NumeroMaximoUsuarios"
'                strSQL += " FROM V_Empresas"
'                strSQL += " WHERE Codigo = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()
'                sdrEmpresa = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrEmpresa.Read() Then

'                    Me.strNombre = sdrEmpresa("Nombre").ToString()
'                    Me.objCataTipoIdentificacion.Consultar(sdrEmpresa("TIID_Codigo").ToString())
'                    Me.strNumeroIdentificacion = sdrEmpresa("Numero_Identificacion").ToString()
'                    Me.strRepresentanteLegal = sdrEmpresa("Representante_Legal").ToString()
'                    Me.objCataTipoIdentificacionRepresentante.Consultar(sdrEmpresa("TIID_Representante").ToString())

'                    Me.strIdentificacionRepresentante = sdrEmpresa("Identificacion_Representante").ToString()
'                    Me.objCiudad.Datos_Existe_Registro(Me, Val(sdrEmpresa("CIUD_Codigo").ToString()))
'                    Date.TryParse(sdrEmpresa("Fecha_Poliza").ToString(), Me.dteFechaPoliza)
'                    Me.strDireccion = sdrEmpresa("Direccion").ToString()
'                    Me.strTelefono1 = sdrEmpresa("Telefono1").ToString()

'                    Me.strTelefono2 = sdrEmpresa("Telefono2").ToString()
'                    Me.strFax = sdrEmpresa("Fax").ToString()
'                    Me.strEmail = sdrEmpresa("Email").ToString()
'                    Me.strPaginaWeb = sdrEmpresa("Pagina_Web").ToString()

'                    Dim arrCampos(1), arrValoresCampos(1) As String
'                    arrCampos(0) = "NombreAseguradora"
'                    arrCampos(1) = "Numero_Identificacion"
'                    If Me.objGeneral.Retorna_Campos(General.UNO, "V_Aseguradoras", arrCampos, arrValoresCampos, 2, General.CAMPO_ALFANUMERICO, "Codigo", Val(sdrEmpresa("TERC_Aseguradora").ToString), "", Me.strError) Then
'                        Me.objAseguradora.Codigo = Val(sdrEmpresa("TERC_Aseguradora").ToString)
'                        Me.objAseguradora.NumeroIdentificacion = arrValoresCampos(1)
'                        Me.Aseguradora.Nombre = arrValoresCampos(0)
'                    End If

'                    Me.strNumeroPoliza = sdrEmpresa("Numero_Poliza").ToString()
'                    Integer.TryParse(sdrEmpresa("Aplica_Rete_Iva").ToString(), Me.intAplicaReteIva)

'                    Double.TryParse(sdrEmpresa("Porcentaje_Rete_Iva"), Me.dblPorcentajeReteIva)
'                    Double.TryParse(sdrEmpresa("Valor_Salario_Minimo"), Me.dblSalarioMinimo)
'                    Double.TryParse(sdrEmpresa("Valor_Parafiscales"), Me.dblValorParafiscales)
'                    Double.TryParse(sdrEmpresa("Valor_Seguro_Social"), Me.dblValorSeguroSocial)
'                    Double.TryParse(sdrEmpresa("Valor_Seguro_Camionero"), Me.dblSeguroCamionero)

'                    Double.TryParse(sdrEmpresa("Porc_Asistencia_Carretera").ToString(), Me.dblPorcentajeAsistenciaCarretera)
'                    Double.TryParse(sdrEmpresa("Porcentaje_Variacion_Anticipos").ToString(), Me.dblPorcentajeVariacionAnticipos)

'                    Me.objTercero.Existe_Registro(Me, Val(sdrEmpresa("TERC_Codigo").ToString()))
'                    Integer.TryParse(sdrEmpresa("Flete_Horarios").ToString(), Me.intFleteHorarios)
'                    Me.strCodigoRegionalMinisterio = sdrEmpresa("Codigo_Regional_Ministerio").ToString()
'                    Long.TryParse(sdrEmpresa("Codigo_Ministerio").ToString(), Me.lonCodigoMinisterio)

'                    Me.objCataTipoNumeracion.Consultar(sdrEmpresa("Tipo_Numeracion").ToString())
'                    Double.TryParse(sdrEmpresa("Porcentaje_Intermediacion").ToString(), Me.dblPorcentajeIntermediacion)
'                    Double.TryParse(sdrEmpresa("Porc_Retencion_Manifiesto").ToString(), Me.dblRetencionManifiesto)

'                    Double.TryParse(sdrEmpresa("Rete_ICA_Manifiesto").ToString(), Me.dblTarifaRetencionIcaManifiesto)
'                    Me.objCataDocumentoInicialTransporte.Consultar(sdrEmpresa("DITR_Codigo").ToString())
'                    Integer.TryParse(sdrEmpresa("Dias_Maximo_Cumplimiento").ToString(), Me.intDiasMaximoCumplimiento)
'                    Double.TryParse(sdrEmpresa("Multa_Dia_Entrega_Cumplido").ToString(), Me.dblMultaDiaEntregaCumplido)
'                    Me.objCataTipoComisionConductor.Consultar(sdrEmpresa("TCCO_Codigo").ToString())

'                    Double.TryParse(sdrEmpresa("Porcentaje_General_Comision_Conductor").ToString(), Me.dblPorcentajeGeneralComisionConductor)
'                    Me.strMensaje = sdrEmpresa("Mensaje").ToString()
'                    Integer.TryParse(sdrEmpresa("NumeroMaximoUsuarios").ToString(), Me.intNumeroMaximoUsuarios)

'                    Me.objCataTipoParametrizacionMargenRentabilidad.Consultar(sdrEmpresa("TPMR_Codigo").ToString())
'                    Double.TryParse(sdrEmpresa("Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Propio").ToString(), Me.dblMargenRentabilidadDespachoNacionalVehiculoPropio)
'                    Double.TryParse(sdrEmpresa("Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Propio").ToString(), Me.dblMargenRentabilidadDespachoUrbanoVehiculoPropio)
'                    Double.TryParse(sdrEmpresa("Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Tercero").ToString(), Me.dblMargenRentabilidadDespachoNacionalVehiculoTercero)
'                    Double.TryParse(sdrEmpresa("Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Tercero").ToString(), Me.dblMargenRentabilidadDespachoUrbanoVehiculoTercero)

'                    Integer.TryParse(sdrEmpresa("Meses_Vigencia_Estudio_Seguridad_Propios").ToString(), Me.intMesesVigenciaEstudioSeguridadPropios)
'                    Integer.TryParse(sdrEmpresa("Meses_Vigencia_Estudio_Seguridad_Terceros").ToString(), Me.intMesesVigenciaEstudioSeguridadTerceros)

'                    Me.strPrefijo = sdrEmpresa("Prefijo").ToString()

'                    Me.strObservacionManifiesto = sdrEmpresa("Observacion_Manifiesto").ToString()
'                    Double.TryParse(sdrEmpresa("Porcentaje_Seguro").ToString(), Me.dblPorcentajeSeguro)
'                    Me.objCataPais.Consultar(sdrEmpresa("PAIS_Codigo").ToString())
'                    Double.TryParse(sdrEmpresa("Monto_Cubre_Poliza").ToString, Me.dblMontoCubrePoliza)

'                    Me.strUsuarioManifiestoElectronico = sdrEmpresa("Usuario_Manifiesto_Electronico").ToString()
'                    Me.strClaveManifiestoElectronico = sdrEmpresa("Clave_Manifiesto_Electronico").ToString()
'                    Long.TryParse(sdrEmpresa("Nit_Manifiesto_Electronico").ToString(), Me.lonNitManifiestoElectronico)

'                    Byte.TryParse(sdrEmpresa("Dias_Inactiva_Conductor_Ultimo_Cargue").ToString(), Me.intDiasInactivaConductorUltimoCargue)
'                    Integer.TryParse(sdrEmpresa("Codigo_Formato_Fecha").ToString(), Me.intCantidadViajesTrafico)

'                    Double.TryParse(sdrEmpresa("Valor_Tope_Retefuente_Servicios").ToString(), Me.dblValorTopeRetefuenteServicios)
'                    Double.TryParse(sdrEmpresa("Valor_Comercial_Minimo").ToString(), Me.dblValorComercialMinimo)
'                    Double.TryParse(sdrEmpresa("Valor_Seguro_Minimo").ToString(), Me.dblValorSeguroMinimo)

'                    Double.TryParse(sdrEmpresa("Porc_Rete_Fuente_Transportista").ToString(), Me.dblRetencionContratistas)
'                    Double.TryParse(sdrEmpresa("Valor_Base_Rete_Fuente_Transportista").ToString(), Me.dblValorBaseRetencionContratistas)
'                    Double.TryParse(sdrEmpresa("Codigo_UIAF").ToString(), Me.lonCodigoUIAF)

'                    Double.TryParse(sdrEmpresa("Porc_Rete_CREE").ToString(), Me.dblRetencionCREE)
'                    Integer.TryParse(sdrEmpresa("Maximo_Cuotas_Recuperacion_Cartera").ToString(), Me.intMaximoCuotasRecuperacionCartera)

'                    Consultar = True
'                Else
'                    Me.lonCodigo = 0
'                    Consultar = False
'                End If

'                sdrEmpresa.Close()
'                sdrEmpresa.Dispose()

'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & ex.Message.ToString()
'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Consultar(ByVal Codigo As Long) As Boolean
'            Try
'                Dim sdrEmpresa As SqlDataReader
'                Dim ComandoSQL As SqlCommand
'                Me.lonCodigo = Codigo

'                strSQL = "SELECT *,"
'                strSQL += " CONVERT(VARCHAR(3), DECRYPTBYPASSPHRASE('" & General.CLAVE_ENCRIPTADO_BD _
'            & "', Numero_Maximo_Usuarios)) As NumeroMaximoUsuarios"
'                strSQL += " FROM V_Empresas"
'                strSQL += " WHERE Codigo = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()
'                sdrEmpresa = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrEmpresa.Read() Then

'                    Me.strNombre = sdrEmpresa("Nombre").ToString()
'                    Me.objCataTipoIdentificacion.Consultar(sdrEmpresa("TIID_Codigo").ToString())
'                    Me.strNumeroIdentificacion = sdrEmpresa("Numero_Identificacion").ToString()
'                    Me.strRepresentanteLegal = sdrEmpresa("Representante_Legal").ToString()
'                    Me.objCataTipoIdentificacionRepresentante.Consultar(sdrEmpresa("TIID_Representante").ToString())

'                    Me.strIdentificacionRepresentante = sdrEmpresa("Identificacion_Representante").ToString()
'                    Me.objCiudad.Datos_Existe_Registro(Me, Val(sdrEmpresa("CIUD_Codigo").ToString()))
'                    Date.TryParse(sdrEmpresa("Fecha_Poliza").ToString(), Me.dteFechaPoliza)
'                    Me.strDireccion = sdrEmpresa("Direccion").ToString()
'                    Me.strTelefono1 = sdrEmpresa("Telefono1").ToString()

'                    Me.strTelefono2 = sdrEmpresa("Telefono2").ToString()
'                    Me.strFax = sdrEmpresa("Fax").ToString()
'                    Me.strEmail = sdrEmpresa("Email").ToString()
'                    Me.strPaginaWeb = sdrEmpresa("Pagina_Web").ToString()

'                    Dim arrCampos(1), arrValoresCampos(1) As String
'                    arrCampos(0) = "NombreAseguradora"
'                    arrCampos(1) = "Numero_Identificacion"
'                    If Me.objGeneral.Retorna_Campos(General.UNO, "V_Aseguradoras", arrCampos, arrValoresCampos, 2, General.CAMPO_ALFANUMERICO, "Codigo", Val(sdrEmpresa("TERC_Aseguradora").ToString), "", Me.strError) Then
'                        Me.objAseguradora.Codigo = Val(sdrEmpresa("TERC_Aseguradora").ToString)
'                        Me.objAseguradora.NumeroIdentificacion = arrValoresCampos(1)
'                        Me.Aseguradora.Nombre = arrValoresCampos(0)
'                    End If

'                    Me.strNumeroPoliza = sdrEmpresa("Numero_Poliza").ToString()
'                    Integer.TryParse(sdrEmpresa("Aplica_Rete_Iva").ToString(), Me.intAplicaReteIva)

'                    Double.TryParse(sdrEmpresa("Porcentaje_Rete_Iva"), Me.dblPorcentajeReteIva)
'                    Double.TryParse(sdrEmpresa("Valor_Salario_Minimo"), Me.dblSalarioMinimo)
'                    Double.TryParse(sdrEmpresa("Valor_Parafiscales"), Me.dblValorParafiscales)
'                    Double.TryParse(sdrEmpresa("Valor_Seguro_Social"), Me.dblValorSeguroSocial)
'                    Double.TryParse(sdrEmpresa("Valor_Seguro_Camionero"), Me.dblSeguroCamionero)

'                    Double.TryParse(sdrEmpresa("Porc_Asistencia_Carretera").ToString(), Me.dblPorcentajeAsistenciaCarretera)
'                    Double.TryParse(sdrEmpresa("Porcentaje_Variacion_Anticipos").ToString(), Me.dblPorcentajeVariacionAnticipos)

'                    Me.objTercero.Existe_Registro(Me, Val(sdrEmpresa("TERC_Codigo").ToString()))
'                    Integer.TryParse(sdrEmpresa("Flete_Horarios").ToString(), Me.intFleteHorarios)
'                    Me.strCodigoRegionalMinisterio = sdrEmpresa("Codigo_Regional_Ministerio").ToString()
'                    Long.TryParse(sdrEmpresa("Codigo_Ministerio").ToString(), Me.lonCodigoMinisterio)

'                    Me.objCataTipoNumeracion.Consultar(sdrEmpresa("Tipo_Numeracion").ToString())
'                    Double.TryParse(sdrEmpresa("Porcentaje_Intermediacion").ToString(), Me.dblPorcentajeIntermediacion)
'                    Double.TryParse(sdrEmpresa("Porc_Retencion_Manifiesto").ToString(), Me.dblRetencionManifiesto)

'                    Double.TryParse(sdrEmpresa("Rete_ICA_Manifiesto").ToString(), Me.dblTarifaRetencionIcaManifiesto)
'                    Me.objCataDocumentoInicialTransporte.Consultar(sdrEmpresa("DITR_Codigo").ToString())
'                    Integer.TryParse(sdrEmpresa("Dias_Maximo_Cumplimiento").ToString(), Me.intDiasMaximoCumplimiento)
'                    Double.TryParse(sdrEmpresa("Multa_Dia_Entrega_Cumplido").ToString(), Me.dblMultaDiaEntregaCumplido)
'                    Me.objCataTipoComisionConductor.Consultar(sdrEmpresa("TCCO_Codigo").ToString())

'                    Double.TryParse(sdrEmpresa("Porcentaje_General_Comision_Conductor").ToString(), Me.dblPorcentajeGeneralComisionConductor)
'                    Me.strMensaje = sdrEmpresa("Mensaje").ToString()
'                    Integer.TryParse(sdrEmpresa("NumeroMaximoUsuarios").ToString(), Me.intNumeroMaximoUsuarios)

'                    Me.objCataTipoParametrizacionMargenRentabilidad.Consultar(sdrEmpresa("TPMR_Codigo").ToString())
'                    Double.TryParse(sdrEmpresa("Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Propio").ToString(), Me.dblMargenRentabilidadDespachoNacionalVehiculoPropio)
'                    Double.TryParse(sdrEmpresa("Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Propio").ToString(), Me.dblMargenRentabilidadDespachoUrbanoVehiculoPropio)
'                    Double.TryParse(sdrEmpresa("Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Tercero").ToString(), Me.dblMargenRentabilidadDespachoNacionalVehiculoTercero)
'                    Double.TryParse(sdrEmpresa("Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Tercero").ToString(), Me.dblMargenRentabilidadDespachoUrbanoVehiculoTercero)

'                    Integer.TryParse(sdrEmpresa("Meses_Vigencia_Estudio_Seguridad_Propios").ToString(), Me.intMesesVigenciaEstudioSeguridadPropios)
'                    Integer.TryParse(sdrEmpresa("Meses_Vigencia_Estudio_Seguridad_Terceros").ToString(), Me.intMesesVigenciaEstudioSeguridadTerceros)

'                    Me.strPrefijo = sdrEmpresa("Prefijo").ToString()

'                    Me.strObservacionManifiesto = sdrEmpresa("Observacion_Manifiesto").ToString()
'                    Double.TryParse(sdrEmpresa("Porcentaje_Seguro").ToString(), Me.dblPorcentajeSeguro)
'                    Me.objCataPais.Consultar(sdrEmpresa("PAIS_Codigo").ToString())
'                    Double.TryParse(sdrEmpresa("Monto_Cubre_Poliza").ToString, Me.dblMontoCubrePoliza)

'                    Me.strUsuarioManifiestoElectronico = sdrEmpresa("Usuario_Manifiesto_Electronico").ToString()
'                    Me.strClaveManifiestoElectronico = sdrEmpresa("Clave_Manifiesto_Electronico").ToString()
'                    Long.TryParse(sdrEmpresa("Nit_Manifiesto_Electronico").ToString(), Me.lonNitManifiestoElectronico)

'                    Byte.TryParse(sdrEmpresa("Dias_Inactiva_Conductor_Ultimo_Cargue").ToString(), Me.intDiasInactivaConductorUltimoCargue)
'                    Integer.TryParse(sdrEmpresa("Codigo_Formato_Fecha").ToString(), Me.intCantidadViajesTrafico)

'                    Double.TryParse(sdrEmpresa("Valor_Tope_Retefuente_Servicios").ToString(), Me.dblValorTopeRetefuenteServicios)
'                    Double.TryParse(sdrEmpresa("Valor_Comercial_Minimo").ToString(), Me.dblValorComercialMinimo)
'                    Double.TryParse(sdrEmpresa("Valor_Seguro_Minimo").ToString(), Me.dblValorSeguroMinimo)

'                    Double.TryParse(sdrEmpresa("Porc_Rete_Fuente_Transportista").ToString(), Me.dblRetencionContratistas)
'                    Double.TryParse(sdrEmpresa("Valor_Base_Rete_Fuente_Transportista").ToString(), Me.dblValorBaseRetencionContratistas)
'                    Double.TryParse(sdrEmpresa("Codigo_UIAF").ToString(), Me.lonCodigoUIAF)
'                    Double.TryParse(sdrEmpresa("Porc_Rete_CREE").ToString(), Me.dblRetencionCREE)
'                    Integer.TryParse(sdrEmpresa("Maximo_Cuotas_Recuperacion_Cartera").ToString(), Me.intMaximoCuotasRecuperacionCartera)

'                    Consultar = True
'                Else
'                    Me.lonCodigo = 0
'                    Consultar = False
'                End If

'                sdrEmpresa.Close()
'                sdrEmpresa.Dispose()

'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & ex.Message.ToString()
'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Cargar_Impreso_Manifiesto(ByVal Codigo As Long) As Boolean

'            Dim sdrEmpresa As SqlDataReader
'            Dim ComandoSQL As SqlCommand

'            Me.lonCodigo = Codigo

'            strSQL = "SELECT *,"
'            strSQL += " CONVERT(VARCHAR(50), DECRYPTBYPASSPHRASE('" & General.CLAVE_ENCRIPTADO_BD _
'        & "', Numero_Maximo_Usuarios)) As NumeroMaximoUsuarios"
'            strSQL += " FROM V_Empresas"
'            strSQL += " WHERE Codigo = " & Me.lonCodigo


'            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'            Try
'                Me.objGeneral.ConexionSQL.Open()
'                sdrEmpresa = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrEmpresa.Read() Then

'                    Me.strNombre = sdrEmpresa("Nombre").ToString()
'                    Me.objCataTipoIdentificacion.Campo1 = sdrEmpresa("TIID_Codigo").ToString()
'                    Me.strNumeroIdentificacion = sdrEmpresa("Numero_Identificacion").ToString()
'                    Me.strRepresentanteLegal = sdrEmpresa("Representante_Legal").ToString()

'                    Me.strIdentificacionRepresentante = sdrEmpresa("Identificacion_Representante").ToString()
'                    Me.objCiudad.Datos_Existe_Registro(Me, Val(sdrEmpresa("CIUD_Codigo").ToString()))
'                    Date.TryParse(sdrEmpresa("Fecha_Poliza").ToString(), Me.dteFechaPoliza)
'                    Me.strDireccion = sdrEmpresa("Direccion").ToString()
'                    Me.strTelefono1 = sdrEmpresa("Telefono1").ToString()

'                    Me.strTelefono2 = sdrEmpresa("Telefono2").ToString()

'                    Me.objAseguradora.Existe_Registro(Me, Val(sdrEmpresa("TERC_Aseguradora").ToString()))
'                    Me.strNumeroPoliza = sdrEmpresa("Numero_Poliza").ToString()
'                    Integer.TryParse(sdrEmpresa("Aplica_Rete_Iva").ToString(), Me.intAplicaReteIva)

'                    Integer.TryParse(sdrEmpresa("Flete_Horarios").ToString(), Me.intFleteHorarios)
'                    Me.strCodigoRegionalMinisterio = sdrEmpresa("Codigo_Regional_Ministerio").ToString()
'                    Long.TryParse(sdrEmpresa("Codigo_Ministerio").ToString(), Me.lonCodigoMinisterio)

'                    Me.strObservacionManifiesto = sdrEmpresa("Observacion_Manifiesto").ToString()
'                    Me.objCataPais.Consultar(sdrEmpresa("PAIS_Codigo").ToString())
'                    Double.TryParse(sdrEmpresa("Monto_Cubre_Poliza").ToString, Me.dblMontoCubrePoliza)

'                    Cargar_Impreso_Manifiesto = True
'                Else
'                    Me.lonCodigo = 0
'                    Cargar_Impreso_Manifiesto = False
'                End If

'                sdrEmpresa.Close()
'                sdrEmpresa.Dispose()

'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Cargar_Impreso_Manifiesto: " & ex.Message.ToString()
'                Cargar_Impreso_Manifiesto = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Consultar_Datos_Basicos(ByVal Codigo As Long) As Boolean
'            Try
'                Dim sdrEmpresa As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                Me.lonCodigo = Codigo

'                strSQL = "SELECT Nombre, PaisOEmpresa, DITR_Codigo, Mensaje, Prefijo, "
'                strSQL += " CONVERT(VARCHAR(50), DECRYPTBYPASSPHRASE('" & General.CLAVE_ENCRIPTADO_BD _
'            & "', Numero_Maximo_Usuarios)) As NumeroMaximoUsuarios"
'                strSQL += " FROM V_Empresas"
'                strSQL += " WHERE Codigo = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrEmpresa = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrEmpresa.Read() Then


'                    Me.strNombre = sdrEmpresa("Nombre").ToString()
'                    Me.objCiudad.Departamento.CataRegion.Campo3 = Trim(sdrEmpresa("PaisOEmpresa").ToString())
'                    Me.objCataDocumentoInicialTransporte.Consultar(sdrEmpresa("DITR_Codigo").ToString())
'                    Integer.TryParse(sdrEmpresa("NumeroMaximoUsuarios").ToString(), Me.intNumeroMaximoUsuarios)
'                    Me.strMensaje = Trim(sdrEmpresa("Mensaje").ToString())
'                    Me.strPrefijo = sdrEmpresa("Prefijo").ToString()

'                    Consultar_Datos_Basicos = True
'                Else
'                    Me.lonCodigo = 0
'                    Consultar_Datos_Basicos = False
'                End If

'                sdrEmpresa.Close()
'                sdrEmpresa.Dispose()

'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar_Datos_Basicos: " & ex.Message.ToString()
'                Consultar_Datos_Basicos = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Existe_Registro(ByVal Codigo As Long) As Boolean

'            Dim sdrEmpresa As SqlDataReader
'            Dim ComandoSQL As SqlCommand

'            Me.lonCodigo = Codigo

'            strSQL = "SELECT *"
'            strSQL += " FROM V_Empresas"
'            strSQL += " WHERE Codigo = " & Me.lonCodigo

'            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'            Try
'                Me.objGeneral.ConexionSQL.Open()
'                sdrEmpresa = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrEmpresa.Read() Then

'                    Me.strNombre = sdrEmpresa("Nombre").ToString()
'                    Me.objCataTipoIdentificacion.Consultar(sdrEmpresa("TIID_Codigo").ToString())
'                    Me.strNumeroIdentificacion = sdrEmpresa("Numero_Identificacion").ToString()
'                    Me.strRepresentanteLegal = sdrEmpresa("Representante_Legal").ToString()
'                    Me.objCataTipoIdentificacionRepresentante.Consultar(sdrEmpresa("TIID_Representante").ToString())

'                    Me.strIdentificacionRepresentante = sdrEmpresa("Identificacion_Representante").ToString()
'                    Me.objCiudad.Datos_Existe_Registro(Me, Val(sdrEmpresa("CIUD_Codigo").ToString()))
'                    Date.TryParse(sdrEmpresa("Fecha_Poliza").ToString(), Me.dteFechaPoliza)
'                    Me.strDireccion = sdrEmpresa("Direccion").ToString()
'                    Me.strTelefono1 = sdrEmpresa("Telefono1").ToString()

'                    Me.strTelefono2 = sdrEmpresa("Telefono2").ToString()
'                    Me.strFax = sdrEmpresa("Fax").ToString()
'                    Me.strEmail = sdrEmpresa("Email").ToString()
'                    Me.strPaginaWeb = sdrEmpresa("Pagina_Web").ToString()

'                    Me.objAseguradora.Existe_Registro(Me, Val(sdrEmpresa("TERC_Aseguradora").ToString()))
'                    Me.strNombreAseguradora = "" ' Ya no se utiliza sdrEmpresa("Aseguradora").ToString()
'                    Me.strNitAseguradora = "" ' Ya no se utiliza sdrEmpresa("Nit_Aseguradora").ToString()
'                    Me.strNumeroPoliza = sdrEmpresa("Numero_Poliza").ToString()
'                    Integer.TryParse(sdrEmpresa("Aplica_Rete_Iva").ToString(), Me.intAplicaReteIva)

'                    Double.TryParse(sdrEmpresa("Porcentaje_Rete_Iva").ToString(), Me.dblPorcentajeReteIva)
'                    Double.TryParse(sdrEmpresa("Valor_Salario_Minimo").ToString(), Me.dblSalarioMinimo)
'                    Double.TryParse(sdrEmpresa("Valor_Parafiscales").ToString(), Me.dblValorParafiscales)
'                    Double.TryParse(sdrEmpresa("Valor_Seguro_Social").ToString(), Me.dblValorSeguroSocial)
'                    Double.TryParse(sdrEmpresa("Valor_Seguro_Camionero").ToString(), Me.dblSeguroCamionero)

'                    Double.TryParse(sdrEmpresa("Porc_Asistencia_Carretera").ToString(), Me.dblPorcentajeAsistenciaCarretera)
'                    Double.TryParse(sdrEmpresa("Porcentaje_Variacion_Anticipos").ToString(), Me.dblPorcentajeVariacionAnticipos)

'                    Integer.TryParse(sdrEmpresa("Flete_Horarios").ToString(), Me.intFleteHorarios)
'                    Me.strCodigoRegionalMinisterio = sdrEmpresa("Codigo_Regional_Ministerio").ToString()
'                    Long.TryParse(sdrEmpresa("Codigo_Ministerio").ToString(), Me.lonCodigoMinisterio)

'                    Double.TryParse(sdrEmpresa("Porcentaje_Intermediacion").ToString(), Me.dblPorcentajeIntermediacion)
'                    Double.TryParse(sdrEmpresa("Porc_Retencion_Manifiesto").ToString(), Me.dblRetencionManifiesto)

'                    Double.TryParse(sdrEmpresa("Rete_ICA_Manifiesto").ToString(), Me.dblTarifaRetencionIcaManifiesto)
'                    Integer.TryParse(sdrEmpresa("Dias_Maximo_Cumplimiento").ToString(), Me.intDiasMaximoCumplimiento)
'                    Double.TryParse(sdrEmpresa("Multa_Dia_Entrega_Cumplido").ToString(), Me.dblMultaDiaEntregaCumplido)
'                    Double.TryParse(sdrEmpresa("Porcentaje_General_Comision_Conductor").ToString(), Me.dblPorcentajeGeneralComisionConductor)
'                    Me.strMensaje = sdrEmpresa("Mensaje").ToString()

'                    Double.TryParse(sdrEmpresa("Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Propio").ToString(), Me.dblMargenRentabilidadDespachoNacionalVehiculoPropio)
'                    Double.TryParse(sdrEmpresa("Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Propio").ToString(), Me.dblMargenRentabilidadDespachoUrbanoVehiculoPropio)
'                    Double.TryParse(sdrEmpresa("Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Tercero").ToString(), Me.dblMargenRentabilidadDespachoNacionalVehiculoTercero)
'                    Double.TryParse(sdrEmpresa("Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Tercero").ToString(), Me.dblMargenRentabilidadDespachoUrbanoVehiculoTercero)

'                    Integer.TryParse(sdrEmpresa("Meses_Vigencia_Estudio_Seguridad_Propios").ToString(), Me.intMesesVigenciaEstudioSeguridadPropios)
'                    Integer.TryParse(sdrEmpresa("Meses_Vigencia_Estudio_Seguridad_Terceros").ToString(), Me.intMesesVigenciaEstudioSeguridadTerceros)

'                    Me.strPrefijo = sdrEmpresa("Prefijo").ToString()

'                    Me.strObservacionManifiesto = sdrEmpresa("Observacion_Manifiesto").ToString()
'                    Double.TryParse(sdrEmpresa("Porcentaje_Seguro").ToString(), Me.dblPorcentajeSeguro)
'                    Double.TryParse(sdrEmpresa("Monto_Cubre_Poliza").ToString, Me.dblMontoCubrePoliza)

'                    Me.strUsuarioManifiestoElectronico = sdrEmpresa("Usuario_Manifiesto_Electronico").ToString()
'                    Me.strClaveManifiestoElectronico = sdrEmpresa("Clave_Manifiesto_Electronico").ToString()
'                    Long.TryParse(sdrEmpresa("Nit_Manifiesto_Electronico").ToString(), Me.lonNitManifiestoElectronico)

'                    Existe_Registro = True
'                Else
'                    Me.lonCodigo = 0
'                    Existe_Registro = False
'                End If

'                sdrEmpresa.Close()
'                sdrEmpresa.Dispose()

'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Existe_Registro: " & ex.Message.ToString()
'                Existe_Registro = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function



'        Public Function Eliminar(Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                Eliminar = Borrar_SQL()
'                Mensaje = Me.strError
'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " en la función Eliminar: " & ex.Message.ToString()
'                Eliminar = False
'            End Try
'        End Function

'        Public Function Obtener_Usuarios_Conectados(ByVal Perfil As Integer) As Integer
'            Try
'                Dim ComandoSQL As SqlCommand
'                Dim intNumeroUsuariosConectados As Integer

'                strSQL = "SELECT COUNT()* FROM Usuarios"
'                strSQL += " WHERE EMPR_Codigo = " & Codigo
'                strSQL += " AND Login = 1"

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                intNumeroUsuariosConectados = CInt(ComandoSQL.ExecuteScalar())
'                Return intNumeroUsuariosConectados
'            Catch ex As Exception
'                Obtener_Usuarios_Conectados = 0
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Obtener_Usuarios_Conectados: " & ex.Message.ToString()
'                intNumeroMaximoUsuarios = 0
'            Finally
'                Me.objGeneral.ConexionSQL.Close()
'            End Try
'        End Function

'        Public Function Retorna_Campo(ByVal intCodigoEmpresa As Integer, ByVal strCampo As String, ByRef strValoCamp As String, Optional ByRef strError As String = "") As Boolean
'            Try
'                Dim strSQL As String
'                Dim intCont As Integer = 0
'                Dim objGeneral As New General()
'                Dim daDataAdapter As SqlDataAdapter
'                Dim dsDataSet As DataSet = New DataSet

'                objGeneral.ConexionSQL = New SqlConnection(objGeneral.CadenaDeConexionSQL)

'                strSQL = "SELECT " & strCampo
'                strSQL = strSQL & " FROM Empresas"
'                strSQL = strSQL & " WHERE Codigo = " & intCodigoEmpresa

'                daDataAdapter = New SqlDataAdapter(strSQL, objGeneral.ConexionSQL)
'                daDataAdapter.Fill(dsDataSet)
'                objGeneral.ConexionSQL.Close()

'                If dsDataSet.Tables(Lista.PRIMERA_TABLA).Rows.Count > 0 Then
'                    strValoCamp = dsDataSet.Tables(Lista.PRIMERA_TABLA).Rows(Lista.PRIMER_REGISTRO).Item(strCampo)
'                    Retorna_Campo = True
'                Else
'                    Retorna_Campo = False
'                End If
'                daDataAdapter.Dispose()
'                dsDataSet.Dispose()

'            Catch ex As Exception
'                Retorna_Campo = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Retorna_Campo: " & ex.Message.ToString()
'            End Try

'        End Function

'        Public Function Retorna_Campos(ByVal intCodigoEmpresa As Integer, ByVal arrCampos As String(), ByRef arrValoresCampos As String(), ByVal intNumeroCampos As Integer, ByVal strCondicion As String, Optional ByRef strError As String = "") As Boolean
'            Try
'                Dim strSQL As String
'                Dim intCont As Integer = 0
'                Dim objGeneral As New General()
'                Dim daDataAdapter As SqlDataAdapter
'                Dim dsDataSet As DataSet = New DataSet
'                objGeneral = New General()
'                objGeneral.ConexionSQL = New SqlConnection(objGeneral.CadenaDeConexionSQL)

'                strSQL = "SELECT "
'                For intCont = 0 To intNumeroCampos - 1
'                    If intCont = 0 Then
'                        strSQL = strSQL & arrCampos(intCont)
'                    Else
'                        strSQL = strSQL & ", " & arrCampos(intCont)
'                    End If
'                Next
'                strSQL = strSQL & Chr(10) & " FROM Empresas"
'                strSQL = strSQL & Chr(10) & " WHERE Codigo = " & intCodigoEmpresa

'                If strCondicion <> "" Then
'                    strSQL = strSQL & Chr(10) & " AND " & strCondicion
'                End If

'                daDataAdapter = New SqlDataAdapter(strSQL, objGeneral.ConexionSQL)
'                daDataAdapter.Fill(dsDataSet)
'                objGeneral.ConexionSQL.Close()

'                If dsDataSet.Tables(Lista.PRIMERA_TABLA).Rows.Count > 0 Then
'                    ReDim arrValoresCampos(intNumeroCampos - 1)

'                    For intCont = 0 To intNumeroCampos - 1
'                        arrValoresCampos(intCont) = dsDataSet.Tables(Lista.PRIMERA_TABLA).Rows(Lista.PRIMER_REGISTRO).Item(arrCampos(intCont).ToString)
'                    Next
'                    Retorna_Campos = True
'                Else
'                    Retorna_Campos = False
'                End If
'                daDataAdapter.Dispose()
'                dsDataSet.Dispose()

'            Catch ex As Exception
'                Retorna_Campos = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Retorna_Campos: " & ex.Message.ToString()
'            Finally
'                If objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'#End Region

'#Region "Funciones Privadas"


'        Private Function Borrar_SQL() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()
'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_borrar_empresas", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_Codigo").Value = Me.lonCodigo

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Borrar_SQL = True
'                Else
'                    Borrar_SQL = False
'                End If

'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " en la función Borrar: " & ex.Message.ToString()
'                Borrar_SQL = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Datos_Requeridos(Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                Dim intCont As Short

'                Me.strError = ""
'                intCont = 0
'                Datos_Requeridos = True

'                If Me.objTercero.Codigo = 0 Then
'                    Me.objTercero.Codigo = 1
'                End If

'                If Len(Trim(Me.strNombre)) = 0 Then
'                    intCont += 1
'                    Me.strError = intCont & ". Debe digitar el nombre de la empresa"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Len(Trim(Me.strNumeroIdentificacion)) = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe digitar el número de identificación de la empresa"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Len(Trim(Me.strRepresentanteLegal)) <= 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe digitar el nombre del representante legal"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Len(Trim(Me.strDireccion)) = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe digitar la dirección de la empresa"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Len(Trim(Me.strTelefono1)) = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe digitar el teléfono principal de la empresa"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Len(Trim(Me.strEmail)) > 0 Then
'                    If Not Me.objGeneral.Email_Valido(Me.strEmail) Then
'                        intCont += 1
'                        Me.strError += intCont & ". El Email ingresado no es válido"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'                If Len(Trim(Me.objAseguradora.NumeroIdentificacion)) = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe digitar el NIT de la aseguradora"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.dteFechaPoliza = Date.MinValue Then
'                    Me.dteFechaPoliza = General.FECHA_NULA
'                End If

'                If Not Me.intAplicaReteIva = 0 Then
'                    If Me.dblPorcentajeReteIva <= 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar el valor del porcentaje de retención de iva"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'                If Me.dblPorcentajeReteIva > 100 Then
'                    intCont += 1
'                    Me.strError += intCont & ". El valor del porcentaje de retención de iva no puede ser superior al 100%"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Len(Trim(Me.strCodigoRegionalMinisterio)) = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar el código regional asignado por el Ministerio de Transporte"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.lonCodigoMinisterio <= 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar el código de la empresa asignado por el Ministerio de Transporte"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.dblPorcentajeVariacionAnticipos < 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar el porcentaje de variación de los anticipos"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.dblPorcentajeVariacionAnticipos > 100 Then
'                    intCont += 1
'                    Me.strError += intCont & ". El valor del porcentaje de variación de los anticipos no puede ser superior al 100%"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.dblPorcentajeAsistenciaCarretera < 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar el porcentaje de asistencia en carretera "
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.dblPorcentajeAsistenciaCarretera > 100 Then
'                    intCont += 1
'                    Me.strError += intCont & ". El valor del porcentaje de asistencia en carretera no puede ser superior al 100%"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.dblPorcentajeIntermediacion < 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar el porcentaje de intermediación"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.dblPorcentajeIntermediacion > 100 Then
'                    intCont += 1
'                    Me.strError += intCont & ". El valor del porcentaje de intermediación no puede ser superior al 100%"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.dblRetencionManifiesto < 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar el porcentaje de retención del manifiesto"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.dblRetencionManifiesto > 100 Then
'                    intCont += 1
'                    Me.strError += intCont & ". El valor del porcentaje de la retención del manifiesto no puede ser superior al 100%"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.dblPorcentajeGeneralComisionConductor > 100 Then
'                    intCont += 1
'                    Me.strError += intCont & ". El valor del porcentaje de la comisión del conductor no puede ser superior al 100%"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Val(Me.objCataTipoComisionConductor.Retorna_Codigo) = COMISION_CONDUCTOR_POR_RUTA Then
'                    If Me.dblPorcentajeGeneralComisionConductor > 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". El valor del porcentaje de la comisión del conductor debe ser 0 y parametrizars en cada ruta"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'                If Val(Me.objCataTipoParametrizacionMargenRentabilidad.Retorna_Codigo) = MARGEN_RENTABILIDAD_POR_CLIENTE Then
'                    If Me.dblMargenRentabilidadDespachoNacionalVehiculoPropio <> 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". El porcentaje de margen de rentabilidad nacional para vehículos propios, se debe parametrizar por cliente no por empresa"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                    If Me.dblMargenRentabilidadDespachoNacionalVehiculoTercero <> 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". El porcentaje de margen de rentabilidad nacional para vehículos terceros, se debe parametrizar por cliente no por empresa"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                    If Me.dblMargenRentabilidadDespachoUrbanoVehiculoPropio <> 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". El porcentaje de margen de rentabilidad urbano para vehículos propios, se debe parametrizar por cliente no por empresa"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                    If Me.dblMargenRentabilidadDespachoUrbanoVehiculoTercero <> 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". El porcentaje de margen de rentabilidad urbano para vehículos terceros, se debe parametrizar por cliente no por empresa"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'            Catch ex As Exception
'                Datos_Requeridos = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Datos_Requeridos: " & ex.Message.ToString()
'            End Try
'        End Function

'#End Region

'    End Class
'End Namespace