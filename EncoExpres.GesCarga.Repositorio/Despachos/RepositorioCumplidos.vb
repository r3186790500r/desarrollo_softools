﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Despachos
Imports System.Transactions

Namespace Despachos

    ''' <summary>
    ''' Clase <see cref="RepositorioCumplidos"/>
    ''' </summary>
    Public Class RepositorioCumplidos
        Inherits RepositorioBase(Of Cumplido)

        Public Overrides Function Consultar(filtro As Cumplido) As IEnumerable(Of Cumplido)
            Dim lista As New List(Of Cumplido)
            Dim item As Cumplido

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.NumeroDocumento > Cero Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.NumeroDocumento)
                End If
                If filtro.NumeroPlanilla > Cero Then
                    conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.NumeroPlanilla)
                End If

                If filtro.FechaInicial > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                End If

                If filtro.FechaFinal > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                End If

                If filtro.NumeroManifiesto > Cero Then
                    conexion.AgregarParametroSQL("@par_ENMC_Numero", filtro.NumeroManifiesto)
                End If

                If Not IsNothing(filtro.Vehiculo) Then
                    If filtro.Vehiculo.Placa <> String.Empty Then
                        conexion.AgregarParametroSQL("@par_VEHI_Placa", filtro.Vehiculo.Placa)
                    End If
                End If

                If Not IsNothing(filtro.Tenedor) Then
                    If filtro.Tenedor.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_TENE_Codigo", filtro.Tenedor.Codigo)
                    End If
                    If Not IsNothing(filtro.Tenedor.Nombre) Then
                        conexion.AgregarParametroSQL("@par_Tenedor", filtro.Tenedor.Nombre)
                    End If
                End If

                If Not IsNothing(filtro.Conductor) Then
                    If filtro.Conductor.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_COND_Codigo", filtro.Conductor.Codigo)
                    End If
                    If Not IsNothing(filtro.Conductor.Nombre) Then
                        conexion.AgregarParametroSQL("@par_Conductor", filtro.Conductor.Nombre)
                    End If
                End If
                If Not IsNothing(filtro.Oficina) Then
                    If filtro.Oficina.Codigo <> -1 Then 'TODAS
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficina.Codigo)
                    End If

                End If

                If (filtro.Estado >= 0 Or filtro.Estado = 1) And filtro.Estado <> 2 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                    conexion.AgregarParametroSQL("@par_Anulado", 0)

                ElseIf filtro.Estado = 2 Then
                    conexion.AgregarParametroSQL("@par_Anulado", 1)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > Cero Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > Cero Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If
                If Not IsNothing(filtro.UsuarioConsulta) Then
                    If filtro.UsuarioConsulta.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_Usuario_Consulta", filtro.UsuarioConsulta.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.TipoDocumento) Then
                    If filtro.TipoDocumento.Codigo = 157 Then
                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_encabezado_cumplido_planilla_paqueteria")

                        While resultado.Read
                            item = New Cumplido(resultado)
                            lista.Add(item)
                        End While
                    Else
                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_encabezado_cumplido_planilla_despachos")

                        While resultado.Read
                            item = New Cumplido(resultado)
                            lista.Add(item)
                        End While
                    End If
                Else
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_encabezado_cumplido_planilla_despachos")

                    While resultado.Read
                        item = New Cumplido(resultado)
                        lista.Add(item)
                    End While
                End If

            End Using
            Return lista
        End Function

        Public Overrides Function Insertar(entidad As Cumplido) As Long
            Dim inserto As Boolean = False
            Const TipoDocumentoCumplidoPlanillaPaqueteria As Integer = 157

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento.Codigo)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_Fecha_Entrega", entidad.FechaEntrega, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.NumeroPlanilla)
                    If entidad.NumeroManifiesto > Cero Then
                        conexion.AgregarParametroSQL("@par_ENMC_Numero", entidad.NumeroManifiesto)
                    End If
                    conexion.AgregarParametroSQL("@par_Fecha_Inicio_Cargue", entidad.FechaInicioCargue, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Fecha_Fin_Cargue", entidad.FechaFinCargue, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Fecha_Inicio_Descargue", entidad.FechaInicioDescargue, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Fecha_Fin_Descargue", entidad.FechaFinDescargue, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Fecha_Llegada_Cargue", entidad.FechaLlegadaCargue, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Fecha_Llegada_Descargue", entidad.FechaLlegadaDescargue, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Valor_Multa_Extemporaneo", entidad.ValorMultaExtemporaneo)
                    conexion.AgregarParametroSQL("@par_Nombre_Entrego_Cumplido", entidad.NombreEntregoCumplido)
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
                    conexion.AgregarParametroSQL("@par_CATA_TICM_Codigo", entidad.TipoCumplidoManifiesto.Codigo)
                    If entidad.TipoCumplidoManifiesto.Codigo = 20802 Then
                        If Not IsNothing(entidad.MotivoSuspensionManifiesto) Then
                            conexion.AgregarParametroSQL("@par_CATA_MOSM_Codigo", entidad.MotivoSuspensionManifiesto.Codigo)
                        End If

                        If Not IsNothing(entidad.ConsecuenciaSuspencionManifiesto) Then
                            conexion.AgregarParametroSQL("@par_CATA_COSM_Codigo", entidad.ConsecuenciaSuspencionManifiesto.Codigo)
                        End If

                        If Not IsNothing(entidad.VehiculoTransbordo) Then
                            conexion.AgregarParametroSQL("@par_VEHI_Codigo_Transbordo", entidad.VehiculoTransbordo.Codigo)
                            If Not IsNothing(entidad.VehiculoTransbordo.Propietario) Then
                                conexion.AgregarParametroSQL("@par_TERC_Codigo_Propietario_Transbordo", entidad.VehiculoTransbordo.Propietario.Codigo)
                            End If
                        End If

                        If Not IsNothing(entidad.SemirremolqueTransbordo) Then
                            conexion.AgregarParametroSQL("@par_SEMI_Codigo_Transbordo", entidad.SemirremolqueTransbordo.Codigo)
                        End If

                        If Not IsNothing(entidad.TenedorTransbordo) Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Tenedor_Transbordo", entidad.TenedorTransbordo.Codigo)
                        End If

                        If Not IsNothing(entidad.ConductorTransbordo) Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Condcutor_Transbordo", entidad.ConductorTransbordo.Codigo)
                        End If

                        If Not IsNothing(entidad.RutaTransbordo) Then
                            conexion.AgregarParametroSQL("@par_RUTA_Codigo_Transbordo", entidad.RutaTransbordo.Codigo)
                        End If

                        If Not IsNothing(entidad.TarifaTransbordo) Then
                            conexion.AgregarParametroSQL("@par_TATC_Codigo_Transbordo", entidad.TarifaTransbordo.Codigo)
                        End If

                        If Not IsNothing(entidad.TipoTarifaTransbordo) Then
                            conexion.AgregarParametroSQL("@par_TTTC_Codigo_Transbordo", entidad.TipoTarifaTransbordo.Codigo)
                        End If

                        If entidad.CantidadTransbordo > 0 Then
                            conexion.AgregarParametroSQL("@par_Cantidad_Transbordo", entidad.CantidadTransbordo)
                        End If

                        If entidad.PesoTransbordo > 0 Then
                            conexion.AgregarParametroSQL("@par_Peso_Transbordo", entidad.PesoTransbordo)
                        End If

                        If entidad.ValorFleteTransportadorTransbordo > 0 Then
                            conexion.AgregarParametroSQL("@par_Valor_Flete_Transportador_Transbordo", entidad.ValorFleteTransportadorTransbordo, SqlDbType.Money)
                        End If
                    End If
                    If entidad.TipoDocumento.Codigo = TipoDocumentoCumplidoPlanillaPaqueteria Then
                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_cumplido_planilla_paqueteria")
                        While resultado.Read
                            entidad.Numero = resultado.Item("Numero").ToString
                            entidad.NumeroDocumento = resultado.Item("NumeroDocumento").ToString
                        End While
                        resultado.Close()
                    Else
                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_cumplido_planilla_despachos")
                        While resultado.Read
                            entidad.Numero = resultado.Item("Numero").ToString
                            entidad.NumeroDocumento = resultado.Item("NumeroDocumento").ToString
                        End While
                        resultado.Close()
                    End If



                    If entidad.Numero.Equals(Cero) Then
                        Return Cero
                    Else
                        inserto = True
                    End If

                    If entidad.Numero > Cero Then
                        If entidad.TipoCumplidoManifiesto.Codigo = 20802 And entidad.ConsecuenciaSuspencionManifiesto.Codigo <> 21004 Then
                            If entidad.Estado = 1 Then
                                inserto = GenerarManifiestoTransbordo(entidad, conexion)
                            End If
                        Else
                            If Not IsNothing(entidad.ListaRemesasManifiesto) Then
                                inserto = CumplirRemesas(entidad, conexion)
                            Else
                                If Not IsNothing(entidad.ListaCumplidoRemesas) Then
                                    inserto = CumplirRemesas(entidad, conexion)
                                End If

                            End If
                        End If
                    End If

                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    Return entidad.Numero
                End If
            End Using

            If inserto Then
                'If entidad.ConteoDocumentos > 0 Then
                Dim Documento = New Repositorio.Basico.General.RepositorioDocumentos
                entidad.Documentos = New Documentos()
                entidad.Documentos.CodigoEmpresa = entidad.CodigoEmpresa
                entidad.Documentos.NumeroCumplido = entidad.Numero
                entidad.Documentos.UsuarioCrea = entidad.UsuarioCrea
                entidad.Documentos.HaciaTemporal = 0

                Call Documento.InsertarDocumentosCumplido(entidad.Documentos)
                'End If
            End If

            Return entidad.NumeroDocumento
        End Function

        Public Overrides Function Modificar(entidad As Cumplido) As Long
            Dim modifico As Boolean = True
            Const TipoDocumentoCumplidoPlanillaPaqueteria As Integer = 157
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_Fecha_Entrega", entidad.FechaEntrega, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_Fecha_Inicio_Cargue", entidad.FechaInicioCargue, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Fecha_Fin_Cargue", entidad.FechaFinCargue, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Fecha_Inicio_Descargue", entidad.FechaInicioDescargue, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Fecha_Fin_Descargue", entidad.FechaFinDescargue, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Fecha_Llegada_Cargue", entidad.FechaLlegadaCargue, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Fecha_Llegada_Descargue", entidad.FechaLlegadaDescargue, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Valor_Multa_Extemporaneo", entidad.ValorMultaExtemporaneo)
                    conexion.AgregarParametroSQL("@par_Nombre_Entrego_Cumplido", entidad.NombreEntregoCumplido)
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
                    conexion.AgregarParametroSQL("@par_CATA_TICM_Codigo", entidad.TipoCumplidoManifiesto.Codigo)
                    If entidad.TipoCumplidoManifiesto.Codigo = 20802 Then
                        If Not IsNothing(entidad.MotivoSuspensionManifiesto) Then
                            conexion.AgregarParametroSQL("@par_CATA_MOSM_Codigo", entidad.MotivoSuspensionManifiesto.Codigo)
                        End If

                        If Not IsNothing(entidad.ConsecuenciaSuspencionManifiesto) Then
                            conexion.AgregarParametroSQL("@par_CATA_COSM_Codigo", entidad.ConsecuenciaSuspencionManifiesto.Codigo)
                        End If

                        If Not IsNothing(entidad.VehiculoTransbordo) Then
                            conexion.AgregarParametroSQL("@par_VEHI_Codigo_Transbordo", entidad.VehiculoTransbordo.Codigo)
                            If Not IsNothing(entidad.VehiculoTransbordo.Propietario) Then
                                conexion.AgregarParametroSQL("@par_TERC_Codigo_Propietario_Transbordo", entidad.VehiculoTransbordo.Propietario.Codigo)
                            End If
                        End If

                        If Not IsNothing(entidad.SemirremolqueTransbordo) Then
                            conexion.AgregarParametroSQL("@par_SEMI_Codigo_Transbordo", entidad.SemirremolqueTransbordo.Codigo)
                        End If

                        If Not IsNothing(entidad.TenedorTransbordo) Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Tenedor_Transbordo", entidad.TenedorTransbordo.Codigo)
                        End If

                        If Not IsNothing(entidad.ConductorTransbordo) Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Condcutor_Transbordo", entidad.ConductorTransbordo.Codigo)
                        End If

                        If Not IsNothing(entidad.RutaTransbordo) Then
                            conexion.AgregarParametroSQL("@par_RUTA_Codigo_Transbordo", entidad.RutaTransbordo.Codigo)
                        End If

                        If Not IsNothing(entidad.TarifaTransbordo) Then
                            conexion.AgregarParametroSQL("@par_TATC_Codigo_Transbordo", entidad.TarifaTransbordo.Codigo)
                        End If

                        If Not IsNothing(entidad.TipoTarifaTransbordo) Then
                            conexion.AgregarParametroSQL("@par_TTTC_Codigo_Transbordo", entidad.TipoTarifaTransbordo.Codigo)
                        End If

                        If entidad.CantidadTransbordo > 0 Then
                            conexion.AgregarParametroSQL("@par_Cantidad_Transbordo", entidad.CantidadTransbordo)
                        End If

                        If entidad.PesoTransbordo > 0 Then
                            conexion.AgregarParametroSQL("@par_Peso_Transbordo", entidad.PesoTransbordo)
                        End If

                        If entidad.ValorFleteTransportadorTransbordo > 0 Then
                            conexion.AgregarParametroSQL("@par_Valor_Flete_Transportador_Transbordo", entidad.ValorFleteTransportadorTransbordo, SqlDbType.Money)
                        End If
                    End If
                    If entidad.TipoDocumento.Codigo = TipoDocumentoCumplidoPlanillaPaqueteria Then
                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_encabezado_cumplido_planilla_paqueteria")
                        While resultado.Read
                            entidad.Numero = resultado.Item("Numero").ToString
                            entidad.NumeroDocumento = resultado.Item("NumeroDocumento").ToString
                        End While
                        resultado.Close()
                    Else
                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_encabezado_cumplido_planilla_despachos")
                        While resultado.Read
                            entidad.Numero = resultado.Item("Numero").ToString
                            entidad.NumeroDocumento = resultado.Item("NumeroDocumento").ToString
                        End While
                        resultado.Close()
                    End If


                    If entidad.Numero.Equals(Cero) Then
                        Return Cero
                    End If

                    If entidad.Numero > Cero Then
                        If entidad.TipoCumplidoManifiesto.Codigo = 20802 And entidad.ConsecuenciaSuspencionManifiesto.Codigo <> 21004 Then
                            If entidad.Estado = 1 Then
                                modifico = GenerarManifiestoTransbordo(entidad, conexion)
                            End If
                        Else
                            If Not IsNothing(entidad.ListaCumplidoRemesas) Then
                                modifico = CumplirRemesas(entidad, conexion)
                            End If
                        End If
                    End If
                End Using

                If modifico Then
                    transaccion.Complete()
                Else
                    entidad.Numero = Cero
                End If

            End Using

            If modifico Then
                'If entidad.ConteoDocumentos > 0 Then
                Dim Documento = New Repositorio.Basico.General.RepositorioDocumentos
                entidad.Documentos = New Documentos()
                entidad.Documentos.CodigoEmpresa = entidad.CodigoEmpresa
                entidad.Documentos.NumeroCumplido = entidad.Numero
                entidad.Documentos.UsuarioCrea = entidad.UsuarioCrea
                entidad.Documentos.HaciaTemporal = 0

                Call Documento.InsertarDocumentosCumplido(entidad.Documentos)
                ' End If
            End If

            Return entidad.NumeroDocumento
        End Function

        Public Overrides Function Obtener(filtro As Cumplido) As Cumplido
            Dim item As New Cumplido

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_encabezado_cumplido_planilla_despacho")

                While resultado.Read
                    item = New Cumplido(resultado)
                End While

                If item.Numero > 0 Then
                    item.ListaCumplidoRemesas = Obtener_Detalle_Cumplido_Planilla(filtro)
                End If

                Dim Documento = New Repositorio.Basico.General.RepositorioDocumentos
                filtro.Documentos = New Documentos()
                filtro.Documentos.CodigoEmpresa = filtro.CodigoEmpresa
                filtro.Documentos.Cumplido = True
                filtro.Documentos.UsuarioCrea = filtro.UsuarioCrea
                filtro.Documentos.NumeroCumplido = filtro.Numero
                item.ListaDocumentos = Documento.Consultar(filtro.Documentos)

            End Using
            Return item
        End Function

        Public Function Anular(entidad As Cumplido) As Cumplido
            Dim item As New Cumplido
            Dim itemDocumento As EncabezadoDocumentos
            Dim lista As New List(Of EncabezadoDocumentos)
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()


                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Anula", entidad.UsuarioAnula.Codigo)
                conexion.AgregarParametroSQL("@par_Causa_Anula", entidad.CausaAnula)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_anular_encabezado_cumplido_planilla_Despacho")

                While resultado.Read
                    itemDocumento = New EncabezadoDocumentos With {
                     .Liquidacion = New Liquidacion With {.Numero = Read(resultado, "ELPD_Numero_Documento")}
                     }
                    lista.Add(itemDocumento)
                    item.Anulado = Read(resultado, "Anulo")

                End While
                item.DocumentosRelacionados = lista

            End Using
            Return item
        End Function

        Public Function CumplirRemesas(entidad As Cumplido, ByRef Conexion As Conexion.DataBaseFactory) As Boolean
            Dim Cumplio As Boolean = True

            Dim resultado As IDataReader
            If Not IsNothing(entidad.ListaCumplidoRemesas) Then
                For Each item In entidad.ListaCumplidoRemesas
                    Conexion.CleanParameters()
                    Conexion.AgregarParametroSQL("@par_EMPR_Codigo", item.CodigoEmpresa)
                    Conexion.AgregarParametroSQL("@par_NumeroPlanilla", entidad.NumeroPlanilla)
                    Conexion.AgregarParametroSQL("@par_NumeroCumplido", entidad.Numero)
                    Conexion.AgregarParametroSQL("@par_ENRE_Numero", item.NumeroRemesa)
                    Conexion.AgregarParametroSQL("@par_Fecha_Recibe", item.FechaRecibe, SqlDbType.Date)
                    Conexion.AgregarParametroSQL("@par_Nombre_Recibe", item.NombreRecibe)
                    Conexion.AgregarParametroSQL("@par_Numero_Identificacion_Recibe", item.IdentificacionRecibe)
                    Conexion.AgregarParametroSQL("@par_Telefonos_Recibe", item.TelefonoRecibe)
                    Conexion.AgregarParametroSQL("@par_Observaciones_Recibe", item.ObservacionesRecibe)
                    Conexion.AgregarParametroSQL("@par_Cantidad_Recibe", item.CantidadRecibida)
                    Conexion.AgregarParametroSQL("@par_Peso_Recibe", item.PesoRecibido, SqlDbType.Money)
                    Conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", item.UsuarioCrea.Codigo)
                    Conexion.AgregarParametroSQL("@par_OFIC_Codigo", item.Oficina.Codigo)

                    Conexion.AgregarParametroSQL("@par_Cantidad_Faltante", item.CantidadFaltante)
                    Conexion.AgregarParametroSQL("@par_Peso_Faltante", item.PesoFaltante)
                    Conexion.AgregarParametroSQL("@par_Valor_Faltante", item.ValorFlatante, SqlDbType.Money)
                    Conexion.AgregarParametroSQL("@par_Peso_Cargue", item.PesoCargue, SqlDbType.Money)
                    Conexion.AgregarParametroSQL("@par_Peso_Descargue", item.PesoDescargue, SqlDbType.Money)
                    Conexion.AgregarParametroSQL("@par_Soporte_Cliente", item.SoporteCliente)

                    resultado = Conexion.ExecuteReaderStoreProcedure("gsp_cumplir_remesas_planilla_despacho")
                    While resultado.Read
                        If Not (resultado.Item("ENRE_Numero") > 0) Then
                            Cumplio = False
                            Exit For
                        End If
                    End While
                    resultado.Close()
                Next
            ElseIf Not IsNothing(entidad.ListaRemesasManifiesto) Then
                For Each item In entidad.ListaRemesasManifiesto
                    Conexion.CleanParameters()
                    Conexion.AgregarParametroSQL("@par_EMPR_Codigo", item.CodigoEmpresa)
                    Conexion.AgregarParametroSQL("@par_NumeroPlanilla", entidad.NumeroPlanilla)
                    Conexion.AgregarParametroSQL("@par_NumeroCumplido", entidad.Numero)
                    Conexion.AgregarParametroSQL("@par_ENRE_Numero", item.NumeroRemesa)
                    Conexion.AgregarParametroSQL("@par_Fecha_Recibe", item.FechaRecibe, SqlDbType.Date)
                    Conexion.AgregarParametroSQL("@par_Nombre_Recibe", item.NombreRecibe)
                    Conexion.AgregarParametroSQL("@par_Numero_Identificacion_Recibe", item.IdentificacionRecibe)

                    Conexion.AgregarParametroSQL("@par_Cantidad_Recibe", item.CantidadRecibida)
                    Conexion.AgregarParametroSQL("@par_Peso_Recibe", item.PesoRecibido)
                    Conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", item.UsuarioCrea.Codigo)
                    Conexion.AgregarParametroSQL("@par_OFIC_Codigo", item.Oficina.Codigo)



                    resultado = Conexion.ExecuteReaderStoreProcedure("gsp_cumplir_remesas_planilla_despacho")
                    While resultado.Read
                        If Not (resultado.Item("ENRE_Numero") > 0) Then
                            Cumplio = False
                            Exit For
                        End If
                    End While
                    resultado.Close()
                Next

            End If

            Return Cumplio
        End Function

        Public Function GenerarManifiestoTransbordo(entidad As Cumplido, ByRef Conexion As Conexion.DataBaseFactory) As Boolean
            Dim Cumplio As Boolean = True

            Dim resultado As IDataReader
            Conexion.CleanParameters()
            Conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            Conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.NumeroPlanilla)
            Conexion.AgregarParametroSQL("@par_ENMC_Numero", entidad.NumeroManifiesto)
            Conexion.AgregarParametroSQL("@par_ECPD_Numero", entidad.Numero)
            If Not IsNothing(entidad.VehiculoTransbordo) Then
                Conexion.AgregarParametroSQL("@par_VEHI_Codigo_Transbordo", entidad.VehiculoTransbordo.Codigo)
                If Not IsNothing(entidad.VehiculoTransbordo.Propietario) Then
                    Conexion.AgregarParametroSQL("@par_TERC_Codigo_Propietario_Transbordo", entidad.VehiculoTransbordo.Propietario.Codigo)
                End If
            End If
            If Not IsNothing(entidad.SemirremolqueTransbordo) Then
                Conexion.AgregarParametroSQL("@par_SEMI_Codigo_Transbordo", entidad.SemirremolqueTransbordo.Codigo)
            End If
            If Not IsNothing(entidad.TenedorTransbordo) Then
                Conexion.AgregarParametroSQL("@par_TERC_Codigo_Tenedor_Transbordo", entidad.TenedorTransbordo.Codigo)
            End If
            If Not IsNothing(entidad.ConductorTransbordo) Then
                Conexion.AgregarParametroSQL("@par_TERC_Codigo_Condcutor_Transbordo", entidad.ConductorTransbordo.Codigo)
            End If
            If Not IsNothing(entidad.RutaTransbordo) Then
                Conexion.AgregarParametroSQL("@par_RUTA_Codigo_Transbordo", entidad.RutaTransbordo.Codigo)
            End If

            If Not IsNothing(entidad.TarifaTransbordo) Then
                Conexion.AgregarParametroSQL("@par_TATC_Codigo_Transbordo", entidad.TarifaTransbordo.Codigo)
            End If

            If Not IsNothing(entidad.TipoTarifaTransbordo) Then
                Conexion.AgregarParametroSQL("@par_TTTC_Codigo_Transbordo", entidad.TipoTarifaTransbordo.Codigo)
            End If

            If entidad.CantidadTransbordo > 0 Then
                Conexion.AgregarParametroSQL("@par_Cantidad_Transbordo", entidad.CantidadTransbordo)
            End If

            If entidad.PesoTransbordo > 0 Then
                Conexion.AgregarParametroSQL("@par_Peso_Transbordo", entidad.PesoTransbordo)
            End If

            If entidad.ValorFleteTransportadorTransbordo > 0 Then
                Conexion.AgregarParametroSQL("@par_Valor_Flete_Transportador_Transbordo", entidad.ValorFleteTransportadorTransbordo, SqlDbType.Money)
            End If

            Conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
            resultado = Conexion.ExecuteReaderStoreProcedure("gsp_generar_manifiesto_transbordo")
            While resultado.Read
                If Not (Read(resultado, "Numero") > 0) Then
                    Cumplio = False
                End If
            End While
            resultado.Close()

            Return Cumplio
        End Function

        Public Function Obtener_Detalle_Cumplido_Planilla(filtro As Cumplido) As IEnumerable(Of CumplidoRemesa)
            Dim lista As New List(Of CumplidoRemesa)
            Dim item As CumplidoRemesa

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ECPD_Numero", filtro.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_cumplido_remesas")

                While resultado.Read
                    item = New CumplidoRemesa(resultado)
                    lista.Add(item)
                End While
            End Using
            Return lista
        End Function

        Public Function ConsultarTiempos(filtro As Cumplido) As Cumplido
            Dim item As New Cumplido

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero_Documento", filtro.NumeroDocumento)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_tiempos_seguimiento")

                While resultado.Read
                    item = New Cumplido(resultado)
                End While

            End Using
            Return item
        End Function
    End Class

End Namespace