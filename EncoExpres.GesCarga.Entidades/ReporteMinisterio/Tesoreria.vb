﻿Namespace Entidades.ReporteMinisterio
    Public Class Tesoreria

#Region "Declaracion de Constantes"

        ' Constantes de Formas de Pago
        Public Const PAGO_TRANSFERENCIA As Byte = 1
        Public Const PAGO_CHEQUE As Byte = 2
        Public Const PAGO_EFECTIVO As Byte = 3
        Public Const PAGO_CONSIGNACION As Byte = 4

        ' Constantes de los tipos de documentos
        Public Const DOCUMENTO_TIPO_GENERAR_COMP_EGRESO As Byte = 1
        Public Const DOCUMENTO_TIPO_GENERAR_COMP_INGRESO As Byte = 2
        Public Const DOCUMENTO_TIPO_EGRESO As Byte = 3
        Public Const DOCUMENTO_TIPO_INGRESO As Byte = 4

        Public Const APROBADO As Byte = 1

        ' Constantes de documentos Origenes
        ' Egresos
        Public Const DOOR_ANTICIPO_MANIFIESTO As Byte = 1
        Public Const DOOR_LIQUIDACION_MANIFIESTO As Byte = 2
        Public Const DOOR_PAGO_FACTURA_PROVEEDOR As Byte = 3
        Public Const DOOR_OTROS_EGRESOS As Byte = 4
        Public Const DOOR_ORDEN_COMPRA As Byte = 5
        Public Const DOOR_REANTICIPO_MANIFIESTO As Byte = 6
        Public Const DOOR_PAGO_CAUSACION As Byte = 10
        Public Const DOOR_TRANSPORTE_URBANO As Byte = 12
        Public Const DOOR_SOBREFLETE_MANIFIESTO As Byte = 13
        Public Const DOOR_LIQUIDACION_MANIFIESTO_OTRA_EMPR As Byte = 15
        Public Const DOOR_LIQUIDACION_REMESA_URBANA As Byte = 16
        Public Const DOOR_ANTICIPO_REMESA_URBANA As Byte = 17
        Public Const DOOR_REANTICIPO_REMESA_URBANA As Byte = 18
        Public Const DOOR_LIQUIDACION_REMESA_TERCERO As Byte = 21
        Public Const DOOR_LIQUIDACION_MASIVA_REMESA_URBANA As Byte = 22
        Public Const DOOR_ANTICIPO_FACTURA As Byte = 23
        Public Const DOOR_LIQUIDACION_PLANILLA_URBANA As Byte = 29
        Public Const DOOR_NOTA_PROVEEDOR As Byte = 30
        Public Const DOOR_LIQUIDACION_CONDUCTOR As Byte = 32
        Public Const DOOR_ANTICIPO_ORDEN_SERVICIO As Byte = 33
        Public Const DOOR_ANTICIPO_PLANILLA_GUIA As Byte = 50
        Public Const DOOR_REANTICIPO_PLANILLA_GUIA As Byte = 103
        Public Const DOOR_ANTICIPO_MANIFIESTO_OTRAS_EMPRESAS As Byte = 100
        Public Const DOOR_REANTICIPO_MANIFIESTO_OTRAS_EMPRESAS As Byte = 101
        Public Const DOOR_CARTERA_VEHICULAR As Short = 102
        Public Const DOOR_ANTICIPO_SOLICITUD As Byte = 150
        Public Const DOOR_ANTICIPO_ORDEN_CARGUE As Byte = 104

        ' Ingresos
        Public Const DOOR_OTROS_INGRESOS As Byte = 7
        Public Const DOOR_PAGO_FACTURA_CLIENTE As Byte = 8
        Public Const DOOR_NOTA_FACTURA_CLIENTE As Byte = 9
        Public Const DOOR_RECAUDO_CAUSACION As Byte = 11
        Public Const DOOR_PAGO_FACTURA_CLIENTE_OTROS_CONCEPTOS As Byte = 14
        Public Const DOOR_PAGO_CUENTA_COBRO_CLIENTE As Byte = 91
        Public Const DOOR_PAGO_CUENTA_COBRO_CLIENTE_OTROS_CONCEPTOS As Byte = 20
        Public Const DOOR_ANTICIPO_CLIENTE As Byte = 28
        Public Const DOOR_CONSIGNACION_BANCO As Byte = 31
        Public Const DOOR_PLANILLA_GUIA As Byte = 34

        'constantes de cuentas PUC 
        Public Const CPUC_RETE_FUENTE As Byte = 5
        Public Const CPUC_RETE_FUENTE_CLIENTE As Byte = 12
        ' cuentas de ICA e IVA
        Public Const CPUC_RETE_ICA As Byte = 6
        Public Const CPUC_RETE_ICA_TRANSPORTADOR As Byte = 7
        Public Const CPUC_RETE_ICA_OFICINA_PRINCIPAL As Byte = 28
        Public Const CPUC_RETE_ICA_TRANSPORTADOR_OFICINA_PRINCIPAL As Byte = 29
        Public Const CPUC_RETE_IVA As Byte = 30

        Public Const CPUC_CONSIGNACIONES As Byte = 25

        'Constantes de Validacion de Fechas
        Public Const VALIDAR_PERIODO_CONTABLE As Boolean = True
        Public Const NO_VALIDAR_PERIODO_CONTABLE As Boolean = False

        ' Constantes del sistema para movimientos contables
        Public Const CODIGO_NO_APLICA As Byte = 0
        Public Const CODIGO_PAGO_URBANO_REMESA As Byte = 1
        Public Const CODIGO_FLETE_TRANSPORTADOR_MANIFIESTO As Byte = 2
        Public Const CODIGO_RETE_FUENTE_MANIFIESTO As Byte = 3
        Public Const CODIGO_OTROS_DESCUENTOS_MANIFIESTO As Byte = 4

        Public Const CODIGO_RETE_ICA_MANIFIESTO As Byte = 5
        Public Const CODIGO_ANTICIPO_MANIFIESTO As Byte = 6
        Public Const CODIGO_VALOR_PAGAR_MANIFIESTO As Byte = 7
        Public Const CODIGO_SOBREFLETE_MANIFIESTO As Byte = 8
        Public Const CODIGO_VALOR_VALE_GASOLINA_MANIFIESTO As Byte = 9

        Public Const CODIGO_FLETE_TRANSPORTADOR_LIQUI_MANI_PROP As Byte = 10
        Public Const CODIGO_DESCUENTOS_LIQUI_MANI_PROP As Byte = 11
        Public Const CODIGO_RETE_FUENTE_LIQUI_MANI_PROP As Byte = 12
        Public Const CODIGO_RETE_ICA_LIQUI_MANI_PROP As Byte = 13
        Public Const CODIGO_RETE_IVA_LIQUI_MANI_PROP As Byte = 14

        Public Const CODIGO_VALOR_PAGAR_LIQUI_MANI_PROP As Byte = 15
        Public Const CODIGO_TOTAL_REMESAS_FACTURA As Byte = 16
        Public Const CODIGO_FALTANTES_FACTURA As Byte = 17
        Public Const CODIGO_OTROS_CONCEPTOS_FACTURA As Byte = 18
        Public Const CODIGO_RETE_FUENTE_FACTURA As Byte = 19

        Public Const CODIGO_RETE_ICA_FACTURA As Byte = 20
        Public Const CODIGO_TOTAL_FACTURA As Byte = 21
        Public Const CODIGO_SUBTOTAL_NOTA_FACTURA As Byte = 22
        Public Const CODIGO_RETE_FUENTE_NOTA_FACTURA As Byte = 23
        Public Const CODIGO_TOTAL_NOTA_FACTURA As Byte = 24

        Public Const CODIGO_GASTO_CONDUCTOR_FLETE As Byte = 25
        Public Const CODIGO_GASTO_CONDUCTOR_ANTICIPO As Byte = 26
        Public Const CODIGO_GASTO_CONDUCTOR_REANTICIPO As Byte = 27
        Public Const CODIGO_GASTO_CONDUCTOR_GASTOS As Byte = 28
        Public Const CODIGO_GASTO_CONDUCTOR_SALDO As Byte = 29

        Public Const CODIGO_VALOR_DEBITO As Byte = 30
        Public Const CODIGO_VALOR_CREDITO As Byte = 31
        Public Const CODIGO_VALOR_REMESA_VEHI_PROPIOS As Byte = 32
        Public Const CODIGO_VALOR_REMESA_VEHI_TERCEROS As Byte = 33
        Public Const CODIGO_FACTURA_OC_SUBTOTAL As Byte = 34

        Public Const CODIGO_FACTURA_OC_RETEFUENTE As Byte = 35
        Public Const CODIGO_FACTURA_OC_RETEICA As Byte = 36
        Public Const CODIGO_FACTURA_OC_IVA As Byte = 37
        Public Const CODIGO_FACTURA_OC_VALOR_FACTURA As Byte = 38
        Public Const CODIGO_PAGO_ANTICIPO_URBANO_REMESA As Byte = 39

        Public Const CODIGO_ANTICIPO_MANIFIESTO_OTRAS_EMPRESAS_FACTURA As Byte = 40
        Public Const CODIGO_VALOR_FLETE_TRANSPORTADOR_VEHI_TERCERO_FACTURA As Byte = 41
        Public Const CODIGO_VALOR_FLETE_TRANSPORTADOR_VEHI_PROPIO_FACTURA As Byte = 42
        Public Const CODIGO_VALOR_UTILIDAD_FLETE_REMESA As Byte = 43
        Public Const CODIGO_VALOR_UTILIDAD_FLETE_FACTURA As Byte = 44

        ' Liquidación Remesas Urbanas
        Public Const CODIGO_FLETE_TRANSPORTADOR_LIQUI_REME_URBANA As Byte = 45
        Public Const CODIGO_DESCUENTOS_LIQUI_REME_URBANA As Byte = 46
        Public Const CODIGO_RETE_FUENTE_LIQUI_REME_URBANA As Byte = 47
        Public Const CODIGO_RETE_ICA_LIQUI_REME_URBANA As Byte = 48
        Public Const CODIGO_RETE_IVA_LIQUI_REME_URBANA As Byte = 49
        Public Const CODIGO_VALOR_PAGAR_LIQUI_REME_URBANA As Byte = 50

        'Causación
        Public Const CODIGO_CAUSACION_VALOR_DEBITO As Byte = 51
        Public Const CODIGO_CAUSACION_VALOR_CREDITO As Byte = 52
        'Liquidación Conductor
        Public Const CODIGO_LIQUI_CONDUCTOR_COMISIONES As Byte = 53
        Public Const CODIGO_LIQUI_CONDUCTOR_DESCUENTOS As Byte = 54
        Public Const CODIGO_LIQUI_CONDUCTOR_GASTOS As Byte = 55
        Public Const CODIGO_LIQUI_CONDUCTOR_ANTICIPOS As Byte = 56
        Public Const CODIGO_LIQUI_CONDUCTOR_VALOR_A_PAGAR As Byte = 57

        'Liquidación remeas terceros
        Public Const CODIGO_LIQUI_REME_TERCERO_FLETE_TRANSPORTADOR As Byte = 58
        Public Const CODIGO_LIQUI_REME_TERCERO_DESCUENTOS As Byte = 59
        Public Const CODIGO_LIQUI_REME_TERCERO_RETE_FUENTE As Byte = 60
        Public Const CODIGO_LIQUI_REME_TERCERO_RETE_ICA As Byte = 61
        Public Const CODIGO_LIQUI_REME_TERCERO_VALOR_PAGAR As Byte = 62

        ' Liquidación Masiva de  Remesas Urbanas y Terceros
        Public Const CODIGO_LIQUI_REME_MAS_FLETE_TRANPORTADOR As Byte = 63
        Public Const CODIGO_LIQUI_REME_MAS_VALOR_FALTANTES As Byte = 64
        Public Const CODIGO_LIQUI_REME_MAS_FLETE_CLIENTE As Byte = 65

        ' Vales
        Public Const CODIGO_VALE_COMBUSTIBLE As Byte = 66
        Public Const CODIGO_VALE_CONSUMO As Byte = 67

        'Nota Factura
        Public Const CODIGO_RETE_ICA_NOTA_FACTURA As Byte = 68

        'Manifiestos Otras Empresas
        Public Const CODIGO_VALOR_MANIFIESTO_OTRA_EMPRESA_VEHI_PROPIOS As Byte = 69
        Public Const CODIGO_VALOR_MANIFIESTO_OTRA_EMPRESA_VEHI_TERCEROS As Byte = 70
        Public Const CODIGO_VALOR_FLETE_TRANS_MANIFIESTO_OTRA_EMPRESA_VEHI_TERCERO_FACTURA As Byte = 71
        Public Const CODIGO_VALOR_FLETE_TRANS_MANIFIESTO_OTRA_EMPRESA_VEHI_PROPIO_FACTURA As Byte = 72
        Public Const CODIGO_VALOR_UTILIDAD_FLETE_MANIFIESTO As Byte = 73

        'Facturas de otros conceptos
        Public Const CODIGO_FACTURA_OC_ANTICIPO As Byte = 74
        Public Const CODIGO_UTILIDAD_GUIA As Byte = 97

        'Liquidación Planilla Urbana
        Public Const CODIGO_LIQU_PLAN_URBA_VALOR_FLETE_TRANSPORTADOR As Byte = 75
        Public Const CODIGO_LIQU_PLAN_URBA_RETEICA As Byte = 76
        Public Const CODIGO_LIQU_PLAN_URBA_RETEFUENTE As Byte = 77
        Public Const CODIGO_LIQU_PLAN_URBA_VALOR_PAGAR As Byte = 78

        ' Manifiesto
        Public Const CODIGO_VALOR_PAGAR_MAS_ANTICIPO_MANIFIESTO As Byte = 79
        Public Const CODIGO_VALOR_PAPELERIA As Byte = 93

        ' Facturas 
        Public Const CODIGO_FACTURA_REAJUSTE_AUMENTO_FLETE_TRANS As Byte = 80
        Public Const CODIGO_FACTURA_REAJUSTE_DESCUENTO_FLETE_TRANS As Byte = 81
        Public Const CODIGO_FACTURA_UTILIDAD_REAJUSTE As Byte = 82
        Public Const CODIGO_RETE_CREE_FACTURA As Byte = 110

        ' Planilla Urbana
        Public Const CODIGO_PLAN_URBA_FLETE_TRANSPORTADOR As Byte = 83
        Public Const CODIGO_PLAN_URBA_VALOR_AL_COBRO As Byte = 84
        Public Const CODIGO_PLAN_URBA_INGRESO_CAJA_GUIAS As Byte = 85
        Public Const CODIGO_PLAN_URBA_ANTICIPO As Byte = 86
        Public Const CODIGO_PLAN_URBA_EGRESO_CAJA_ANTICIPO As Byte = 87
        Public Const CODIGO_PLAN_URBA_FLETE_TRANSPORTADOR_CREDITO As Byte = 88
        Public Const CODIGO_PLAN_URBA_ADMIN_SEGURO_MENOS_SEGURO_GUIAS_CREDITO As Byte = 89
        Public Const CODIGO_PLAN_URBA_UTILIDAD_VEHICULO_TERCERO As Byte = 90
        Public Const CODIGO_PLAN_URBA_UTILIDAD_VEHICULO_PROPIO As Byte = 91

        ' Gastos Conductor Varios Despachos
        Public Const CODIGO_GASTO_CONDUCTOR_SALDO_EFECTIVO_FAVOR_EMPRESA As Byte = 92
        Public Const CODIGO_GASTO_CONDUCTOR_SALDO_EFECTIVO_FAVOR_CONDUCTOR As Byte = 93
        Public Const CODIGO_GASTO_CONDUCTOR_SALDO_ACPM_FAVOR_EMPRESA As Byte = 94
        Public Const CODIGO_GASTO_CONDUCTOR_SALDO_ACPM_FAVOR_CONDUCTOR As Byte = 95
        Public Const CODIGO_GASTO_CONDUCTOR_FLETE_TRANSPORTADOR_POR_REMESA As Byte = 96

        ' Nota Factura 
        Public Const CODIGO_VALOR_IVA_NOTA_FACTURA As Byte = 103

        ' Constantes de Documentos Genera
        Public Const GENERA_REMESA_URBANO_PROPIOS As Byte = 1
        Public Const GENERA_REMESA_URBANO_TERCEROS As Byte = 2
        Public Const GENERA_MANIFIESTO_ANTICIPO_PROPIO As Byte = 3
        Public Const GENERA_MANIFIESTO_ANTICIPO_TERCERO As Byte = 4
        Public Const GENERA_MANIFIESTO_SOBREFLETE_PROPIO As Byte = 5
        Public Const GENERA_MANIFIESTO_SOBREFLETE_TERCERO As Byte = 6
        Public Const GENERA_LIQUI_MANIFIESTO_TERCEROS As Byte = 7
        Public Const GENERA_LIQUI_MANIFIESTO_SOCIOS As Byte = 8
        Public Const GENERA_LIQUI_MANIFIESTO_OTRA_EMPRESA As Byte = 9
        Public Const GENERA_FACTURA As Byte = 10
        Public Const GENERA_NOTA_DEBITO_FACTURA As Byte = 11
        Public Const GENERA_NOTA_CREDITO_FACTURA As Byte = 12
        Public Const GENERA_GASTO_CONDUCTORES As Byte = 13
        Public Const GENERA_COMPROBANTE_EGRESO As Byte = 14
        Public Const GENERA_COMPROBANTE_INGRESO As Byte = 15
        Public Const GENERA_FACTURA_OTRO_CONCEPTO As Byte = 16
        Public Const GENERA_LIQUI_REMESA_URBANA_TERCEROS As Byte = 17
        Public Const GENERA_LIQUI_REMESA_URBANA_SOCIOS As Byte = 18
        Public Const GENERA_REMESA_URBANA_ANTICIPO_PROPIO As Byte = 19
        Public Const GENERA_REMESA_URBANA_ANTICIPO_TERCERO As Byte = 20
        Public Const GENERA_CUENTA_COBRO As Byte = 21
        Public Const GENERA_CUENTA_COBRO_OTRO_CONCEPTO As Byte = 22
        Public Const GENERA_COMPROBANTE_CAUSACION As Byte = 23
        Public Const GENERA_LIQUI_REMESA_TERCERO As Byte = 24
        Public Const GENERA_LIQUI_MASIV_REMESA_URBANA As Byte = 25
        Public Const GENERA_VALE_COMBUSTIBLE As Byte = 26
        Public Const GENERA_VALE_CONSUMO As Byte = 27
        Public Const GENERA_NOTA_DEBITO_PROVEEDOR As Byte = 28
        Public Const GENERA_NOTA_CREDITO_PROVEEDOR As Byte = 29
        Public Const GENERA_MANIFIESTO As Byte = 30
        Public Const GENERA_LIQUI_PLANILLA_URBANA As Byte = 31
        Public Const GENERA_PLANILLA_URBANA As Byte = 32

        ' Constantes de Documentos Cruce
        Public Const CRUCE_REMESA As Byte = 1
        Public Const CRUCE_MANIFIESTO As Byte = 2
        Public Const CRUCE_LIQUI_MANIFIESTO As Byte = 3
        Public Const CRUCE_LIQUI_MANIFIESTO_OTRA_EMPRESA As Byte = 4
        Public Const CRUCE_FACTURA As Byte = 5
        Public Const CRUCE_NOTA_FACTURA As Byte = 6
        Public Const CRUCE_GASTO_CONDUCTORES As Byte = 7
        Public Const CRUCE_COMPROBANTE_EGRESO As Byte = 8
        Public Const CRUCE_COMPROBANTE_INGRESO As Byte = 9
        Public Const CRUCE_DOCUMENTO_ORIGEN As Byte = 10
        Public Const CRUCE_LIQUI_REMESA_URBANA As Byte = 11
        Public Const CRUCE_COMPROBANTE_CAUSACION As Byte = 12
        Public Const CRUCE_LIQUIDACION_CONDUCTOR As Byte = 13
        Public Const CRUCE_LIQUI_REMESA_TERCERO As Byte = 14
        Public Const CRUCE_MANIFIESTO_OTRA_EMPRESA As Byte = 15
        Public Const CRUCE_FACTURA_OTRO_CONCEPTO As Byte = 16
        Public Const CRUCE_LIQUIDACION_PLANILLA_URBANA As Byte = 17
        Public Const CRUCE_PLANILLA_URBANA As Byte = 18
        Public Const CRUCE_ORDEN_SERVICIO As Byte = 19


        ' Constantes de tipo de movimiento contable
        Public Const MOVIMIENTO_TIPO_CONCEPTO_CATALOGO As Byte = 1
        Public Const MOVIMIENTO_TIPO_VARIBLE_SISTEMA As Byte = 2
        Public Const CODIGO_RETEFUENTE_LIQUI_CONCEPTO As Byte = 104
        Public Const CODIGO_RETEICA_LIQUI_CONCEPTOS As Byte = 105

        ' Constantes de tipo sufijo de movimiento contable
        Public Const SUFIJO_CODIGO_OFICINA As Byte = 1
        Public Const SUFIJO_ICA_CLIENTES As Byte = 2
        Public Const SUFIJO_ICA_TRANSPORTADORES As Byte = 3
        Public Const SUFIJO_ICA_INGRESO As Byte = 4
        Public Const SUFIJO_INGRESO_CLIENTE As Byte = 5
        Public Const SUFIJO_CUENTA_PUC_FLETE_TRANSPORTADOR_CATALOGO As Byte = 6
        Public Const SUFIJO_CUENTA_INGRESO_X_CIUDAD As Byte = 4

        ' Constantes de tipo tercero de movimiento contable
        Public Const TERCERO_NO_APLICA As Byte = 0
        Public Const TERCERO_TENEDOR As Byte = 1
        Public Const TERCERO_CONDUCTOR As Byte = 2
        Public Const TERCERO_CLIENTE As Byte = 3
        Public Const TERCERO_FACTURAR_A As Byte = 4
        Public Const TERCERO_BENEFICIARIO As Byte = 5
        Public Const TERCERO_BASE As Byte = 6
        Public Const TERCERO_EMPRESA_TRANSPORTADORA As Byte = 7
        Public Const TERCERO_PROVEEDOR As Byte = 8
        Public Const TERCERO_PROPIETARIO As Byte = 9
        Public Const TERCERO_OFICINA As Byte = 10

        ' Constantes de tipo centro de costo de movimiento contable
        Public Const CENTRO_COSTO_OFICINA As Byte = 1
        Public Const CENTRO_COSTO_PLACA As Byte = 2
        Public Const CENTRO_COSTO_CODIGO_VEHICULO As Byte = 3
        Public Const CENTRO_COSTO_OFICINA_REMESA As Byte = 4
        Public Const CENTRO_COSTO_OFICINA_MANIFIESTO_OTRA_EMPRESA As Byte = 5

        Public Const ANULACION As Boolean = True

        Public Const PRIMER_PERIODO_ABIERTO As Byte = 1
        Public Const PERIODO_ACTUAL As Byte = 2
        Public Const PERIODO_DEL_DOCUMENTO As Byte = 3

        ' Constantes de estado caja
        Public Const CAJA_ABIERTA As Byte = 0
        Public Const CAJA_CERRADA As Byte = 1

        ' Constantes de Valor Concepto para Movimientos Contables
        Public Const CODIGO_CONCEPTO_NOTA_ACTIVIDADES_CONEXAS As Integer = 1

#End Region
    End Class
End Namespace

