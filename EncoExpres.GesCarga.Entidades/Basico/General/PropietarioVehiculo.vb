﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos

Namespace Basico.General

    Public Class PropietarioVehiculo
        Inherits Base


        Sub New()
        End Sub


        Sub New(lector As IDataReader, Optional autocomplete As Boolean = False)

            Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo")}
            Propietario = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Tenedor")}
            PorcentajeParticipacion = Read(lector, "Porcentaje_Propiedad")
        End Sub


        <JsonProperty>
        Public Property Vehiculo As Vehiculos
        <JsonProperty>
        Public Property Propietario As Tercero
        <JsonProperty>
        Public Property PorcentajeParticipacion As Double

    End Class

End Namespace
