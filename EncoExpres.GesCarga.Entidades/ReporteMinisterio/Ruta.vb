﻿'Imports System.Data
'Imports System.Data.SqlClient

'Namespace Entidades.ReporteMinisterio
'    Public Class Ruta
'#Region "Declaracion Variables"

'        Dim strSQL As String
'        Dim objGeneral As General
'        Public strError As String
'        Public bolModificar As Boolean

'        Private objEmpresa As Empresas
'        Private lonCodigo As Long
'        Private strCodigoTercero As String
'        Private strNombre As String
'        Private objCiudadOrigen As Ciudad

'        Private objCiudadDestino As Ciudad
'        Private lonDistancia As Long
'        Private objCataFormaCalculoAnticipo As Catalogo
'        Private dblAnticipo As Double
'        Private dblPorcentajeAnticipo As Double

'        Private dblHorasEstimadas As Double
'        Private dblPorcentajeIcaTransportador As Double
'        Private dblPorcentajeSeguroTransportador As Double
'        'Private objPeajes As clsPeaje
'        'Private objPuestosControl As clsPuestosControl

'        Private intOrden As Integer
'        Private dblValorFleteMinisterio As Double
'        Private objUsuario As Usuario
'        Private dblPorcentajeComisionConductorPropio As Double

'        Private intCodigoRutaMinisterio As Integer
'        Private intCorredorVialHabilitado As Integer
'        Private intHorasEstimadasArribo As Integer
'        Public bolFoto As Boolean
'        Private bytFoto As Byte()
'        Private dblCantidadCombustible As Double
'        Private objCataTipoRuta As Catalogo
'        Private objValidacion As Validacion
'        Private intKmPesado As Integer
'        'Private objHoteles As clsHoteles
'        'Private ObjParqueaderos As clsParqueaderos
'        Private intIdTemporal As Integer
'        Private intCategoriaRutas As Integer
'        Private intEstado As Integer

'#End Region

'#Region "Declaracion de Constantes"

'        Public Const CODIGO_RUTA_UNO As Byte = 1
'        Public Const ANTICIPO_X_VALOR As Byte = 1
'        Public Const ANTICIPO_X_PORCENTAJE As Byte = 2
'        Public Const CORREDOR_VIAL_HABILITADO As Byte = 1
'        Public Const BYTES_IMAGEN_DEFECTO As Integer = 0

'        Public Const CODIGO_RUTA_URBANA As Long = 1
'        Public Const CODIGO_RUTA_NACIONAL As Long = 2
'#End Region

'#Region "Constructor"

'        Sub New()
'            Me.objEmpresa = New Empresas(0)
'            Me.lonCodigo = 0
'            Me.strCodigoTercero = ""
'            Me.strNombre = ""
'            Me.objCiudadOrigen = New Ciudad(Me.objEmpresa)
'            Me.objCiudadDestino = New Ciudad(Me.objEmpresa)
'            Me.lonDistancia = 0
'            Me.objCataFormaCalculoAnticipo = New Catalogo(Me.objEmpresa, "FPAR")
'            'Me.objPeajes = New clsPeaje(Me.objEmpresa)
'            Me.dblAnticipo = 0
'            Me.dblPorcentajeAnticipo = 0
'            Me.dblHorasEstimadas = 0
'            Me.dblPorcentajeIcaTransportador = 0
'            Me.dblPorcentajeSeguroTransportador = 0
'            Me.objGeneral = New General
'            Me.dblValorFleteMinisterio = 0
'            Me.objUsuario = New Usuario(Me.objEmpresa)
'            Me.dblPorcentajeComisionConductorPropio = 0
'            Me.intHorasEstimadasArribo = 0
'            Me.intCodigoRutaMinisterio = 0
'            Me.intCorredorVialHabilitado = 0
'            Me.dblCantidadCombustible = 0
'            Me.objCataTipoRuta = New Catalogo(Me.objEmpresa, "TIRU")
'            Me.objValidacion = New Validacion(Me.objEmpresa)
'            Me.intKmPesado = 0
'            'Me.objHoteles = New clsHoteles(Me.objEmpresa)
'            'Me.ObjParqueaderos = New clsParqueaderos
'            Me.intIdTemporal = 0
'            Me.intCategoriaRutas = 0
'        End Sub

'        Sub New(ByRef Empresa As Empresas)
'            Me.objEmpresa = Empresa
'            Me.lonCodigo = 0
'            Me.strCodigoTercero = ""
'            Me.strNombre = ""
'            Me.objCiudadOrigen = New Ciudad(Me.objEmpresa)
'            Me.objCiudadDestino = New Ciudad(Me.objEmpresa)
'            'Me.objPeajes = New clsPeaje(Me.objEmpresa)
'            Me.lonDistancia = 0
'            Me.objCataFormaCalculoAnticipo = New Catalogo(Me.objEmpresa, "FPAR")
'            Me.dblAnticipo = 0
'            Me.dblPorcentajeAnticipo = 0
'            Me.dblHorasEstimadas = 0
'            Me.dblPorcentajeIcaTransportador = 0
'            Me.dblPorcentajeSeguroTransportador = 0
'            Me.objGeneral = New General
'            Me.dblValorFleteMinisterio = 0
'            Me.objUsuario = New Usuario(Me.objEmpresa)
'            Me.dblPorcentajeComisionConductorPropio = 0
'            Me.intHorasEstimadasArribo = 0
'            Me.intCodigoRutaMinisterio = 0
'            Me.intCorredorVialHabilitado = 0
'            Me.dblCantidadCombustible = 0
'            Me.objCataTipoRuta = New Catalogo(Me.objEmpresa, "TIRU")
'            Me.objValidacion = New Validacion(Me.objEmpresa)
'            Me.intKmPesado = 0
'            'Me.objHoteles = New clsHoteles(Me.objEmpresa)
'            'Me.ObjParqueaderos = New clsParqueaderos
'            Me.intIdTemporal = 0
'            Me.intCategoriaRutas = 0
'        End Sub

'#End Region

'#Region "Propiedades"

'        Public Property PorcentajeComisionConductorPropio() As Double
'            Get
'                Return Me.dblPorcentajeComisionConductorPropio
'            End Get
'            Set(ByVal value As Double)
'                Me.dblPorcentajeComisionConductorPropio = value
'            End Set
'        End Property

'        Public Property Anticipo() As Double
'            Get
'                Return Me.dblAnticipo
'            End Get
'            Set(ByVal value As Double)
'                Me.dblAnticipo = value
'            End Set
'        End Property

'        Public Property HorasEstimadas() As Double
'            Get
'                Return Me.dblHorasEstimadas
'            End Get
'            Set(ByVal value As Double)
'                Me.dblHorasEstimadas = value
'            End Set
'        End Property

'        Public Property PorcentajeAnticipo() As Double
'            Get
'                Return Me.dblPorcentajeAnticipo
'            End Get
'            Set(ByVal value As Double)
'                Me.dblPorcentajeAnticipo = value
'            End Set
'        End Property

'        Public Property PorcentajeIcaTransportador() As Double
'            Get
'                Return Me.dblPorcentajeIcaTransportador
'            End Get
'            Set(ByVal value As Double)
'                Me.dblPorcentajeIcaTransportador = value
'            End Set
'        End Property

'        Public Property PorcentajeSeguroTransportador() As Double
'            Get
'                Return Me.dblPorcentajeSeguroTransportador
'            End Get
'            Set(ByVal value As Double)
'                Me.dblPorcentajeSeguroTransportador = value
'            End Set
'        End Property

'        Public Property Distancia() As Long
'            Get
'                Return Me.lonDistancia
'            End Get
'            Set(ByVal value As Long)
'                Me.lonDistancia = value
'            End Set
'        End Property

'        Public Property Codigo() As Long
'            Get
'                Return Me.lonCodigo
'            End Get
'            Set(ByVal value As Long)
'                Me.lonCodigo = value
'            End Set
'        End Property

'        Public Property CataFormaCalculoAnticipo() As Catalogo
'            Get
'                Return Me.objCataFormaCalculoAnticipo
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataFormaCalculoAnticipo = value
'            End Set
'        End Property

'        Public Property CiudadDestino() As Ciudad
'            Get
'                Return Me.objCiudadDestino
'            End Get
'            Set(ByVal value As Ciudad)
'                Me.objCiudadDestino = value
'            End Set
'        End Property

'        Public Property CiudadOrigen() As Ciudad
'            Get
'                Return Me.objCiudadOrigen
'            End Get
'            Set(ByVal value As Ciudad)
'                Me.objCiudadOrigen = value
'            End Set
'        End Property

'        Public Property Empresa() As Empresas
'            Get
'                Return Me.objEmpresa
'            End Get
'            Set(ByVal value As Empresas)
'                Me.objEmpresa = value
'            End Set
'        End Property

'        Public Property CodigoTercero() As String
'            Get
'                Return Me.strCodigoTercero
'            End Get
'            Set(ByVal value As String)
'                Me.strCodigoTercero = value
'            End Set
'        End Property

'        Public Property Nombre() As String
'            Get
'                Return Me.strNombre
'            End Get
'            Set(ByVal value As String)
'                Me.strNombre = value
'            End Set
'        End Property

'        'Public Property Peajes() As clsPeaje
'        '    Get
'        '        Return Me.objPeajes
'        '    End Get
'        '    Set(ByVal value As clsPeaje)
'        '        Me.objPeajes = value
'        '    End Set
'        'End Property

'        'Public Property Hotel() As clsHoteles
'        '    Get
'        '        Return Me.objHoteles
'        '    End Get
'        '    Set(ByVal value As clsHoteles)
'        '        Me.objHoteles = value
'        '    End Set
'        'End Property
'        'Public Property Parqueaderos() As clsParqueaderos
'        '    Get
'        '        Return Me.ObjParqueaderos
'        '    End Get
'        '    Set(ByVal value As clsParqueaderos)
'        '        Me.ObjParqueaderos = value
'        '    End Set
'        'End Property

'        Public Property Orden() As Integer
'            Get
'                Return Me.intOrden
'            End Get
'            Set(ByVal value As Integer)
'                Me.intOrden = value
'            End Set
'        End Property

'        Public Property ValorFleteMinisterio() As Double
'            Get
'                Return Me.dblValorFleteMinisterio
'            End Get
'            Set(ByVal value As Double)
'                Me.dblValorFleteMinisterio = value
'            End Set
'        End Property

'        Public Property Usuario() As Usuario
'            Get
'                Return Me.objUsuario
'            End Get
'            Set(ByVal value As Usuario)
'                Me.objUsuario = value
'            End Set
'        End Property

'        Public Property HorasEstimadasArribo() As Integer
'            Get
'                Return Me.intHorasEstimadasArribo
'            End Get
'            Set(ByVal value As Integer)
'                Me.intHorasEstimadasArribo = value
'            End Set
'        End Property

'        Public Property CodigoRutaMinisterio() As Integer
'            Get
'                Return Me.intCodigoRutaMinisterio
'            End Get
'            Set(ByVal value As Integer)
'                Me.intCodigoRutaMinisterio = value
'            End Set
'        End Property

'        Public Property CorredorVialHabilitado() As Integer
'            Get
'                Return intCorredorVialHabilitado
'            End Get
'            Set(ByVal value As Integer)
'                intCorredorVialHabilitado = Math.Abs(value)
'            End Set
'        End Property

'        Public Property Foto() As Byte()
'            Get
'                Return Me.bytFoto
'            End Get
'            Set(ByVal value As Byte())
'                Me.bytFoto = value
'            End Set
'        End Property

'        Public Property CantidadCombustible() As Double
'            Get
'                Return Me.dblCantidadCombustible
'            End Get
'            Set(ByVal value As Double)
'                Me.dblCantidadCombustible = value
'            End Set
'        End Property

'        Public Property CataTipoRuta() As Catalogo
'            Get
'                Return Me.objCataTipoRuta
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataTipoRuta = value
'            End Set
'        End Property

'        Public Property kmPesado() As Integer
'            Get
'                Return Me.intKmPesado
'            End Get
'            Set(ByVal value As Integer)
'                Me.intKmPesado = value
'            End Set
'        End Property

'        Public Property IdTemporal() As Integer
'            Get
'                Return Me.intIdTemporal
'            End Get
'            Set(ByVal value As Integer)
'                Me.intIdTemporal = value
'            End Set
'        End Property

'        Public Property CategoriaRutas() As Integer
'            Get
'                Return Me.intCategoriaRutas
'            End Get
'            Set(ByVal value As Integer)
'                Me.intCategoriaRutas = value
'            End Set
'        End Property

'        Public Property Estado() As Integer
'            Get
'                Return Me.intEstado
'            End Get
'            Set(ByVal value As Integer)
'                Me.intEstado = value
'            End Set
'        End Property


'#End Region

'#Region "Funciones Publicas"

'        Public Function Consultar() As Boolean
'            Try
'                Consultar = True

'                Dim sdrRuta As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT Codigo, Nombre, Codigo_Tercero, CIUD_Origen, CIUD_Destino, Codigo_Ruta_Ministerio, "
'                strSQL += "Distancia, Forma_Calculo_Anticipo, Anticipo_Ruta, Porcentaje_Anticipo, Horas_Estimadas, "
'                strSQL += "Porcentaje_ICA_Transportador, Porcentaje_Seguro_Transportador, Valor_Flete_Ministerio, Porcentaje_Anticipo, Porcentaje_Comision_Conductor, Cantidad_Combustible, "
'                strSQL += "Corredor_Vial_Habilitado, 'Foto' = CASE WHEN Foto IS NULL THEN NULL ELSE 1 END, TIRU_Codigo, Km_Pesado, CARU_Codigo, Estado"
'                strSQL += " FROM Rutas"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.lonCodigo


'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()
'                sdrRuta = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrRuta.Read() Then
'                    Me.strNombre = sdrRuta("Nombre").ToString()
'                    Long.TryParse(sdrRuta("Codigo").ToString(), Me.lonCodigo)
'                    Me.strCodigoTercero = sdrRuta("Codigo_Tercero").ToString()
'                    Me.objCiudadOrigen.Codigo = Val(sdrRuta("CIUD_Origen").ToString())
'                    Me.objCiudadDestino.Codigo = Val(sdrRuta("CIUD_Destino").ToString())
'                    Long.TryParse(sdrRuta("Distancia").ToString(), Me.lonDistancia)
'                    Me.objCataFormaCalculoAnticipo.Campo1 = sdrRuta("Forma_Calculo_Anticipo").ToString()

'                    Double.TryParse(sdrRuta("Anticipo_Ruta").ToString(), Me.dblAnticipo)
'                    Double.TryParse(sdrRuta("Porcentaje_Anticipo").ToString(), Me.dblPorcentajeAnticipo)
'                    Double.TryParse(sdrRuta("Horas_Estimadas").ToString(), Me.dblHorasEstimadas)
'                    Double.TryParse(sdrRuta("Porcentaje_ICA_Transportador").ToString(), Me.dblPorcentajeIcaTransportador)

'                    Double.TryParse(sdrRuta("Porcentaje_Seguro_Transportador").ToString(), Me.dblPorcentajeSeguroTransportador)
'                    Double.TryParse(sdrRuta("Valor_Flete_Ministerio").ToString, Me.dblValorFleteMinisterio)
'                    Double.TryParse(sdrRuta("Porcentaje_Comision_Conductor").ToString(), Me.dblPorcentajeComisionConductorPropio)

'                    Me.intCorredorVialHabilitado = Val(sdrRuta("Corredor_Vial_Habilitado").ToString())
'                    Double.TryParse(sdrRuta("Cantidad_Combustible").ToString, Me.dblCantidadCombustible)
'                    Me.objCataTipoRuta.Campo1 = sdrRuta("TIRU_Codigo").ToString()
'                    Me.intCodigoRutaMinisterio = Val(sdrRuta("Codigo_Ruta_Ministerio").ToString)
'                    Me.intKmPesado = Val(sdrRuta("Km_Pesado").ToString)
'                    Me.CategoriaRutas = sdrRuta("CARU_Codigo").ToString
'                    Me.Estado = sdrRuta("Estado").ToString

'                    If sdrRuta("Foto") IsNot DBNull.Value Then
'                        bolFoto = True
'                    Else
'                        bolFoto = False
'                    End If
'                    Consultar = True
'                Else
'                    Me.lonCodigo = 0
'                    Consultar = False
'                End If

'                sdrRuta.Close()
'                sdrRuta.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Consultar(ByRef Empresa As Empresas, ByVal Codigo As Integer) As Boolean

'            Try
'                Me.objEmpresa = Empresa
'                Me.lonCodigo = Codigo

'                Consultar = True

'                Dim sdrRuta As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo, Codigo, Nombre, Codigo_Tercero, CIUD_Origen, CIUD_Destino, Codigo_Ruta_Ministerio, "
'                strSQL += "Distancia, Forma_Calculo_Anticipo, Anticipo_Ruta, Porcentaje_Anticipo, Horas_Estimadas, "
'                strSQL += "Porcentaje_ICA_Transportador, Porcentaje_Seguro_Transportador, Valor_Flete_Ministerio, Porcentaje_Anticipo, Porcentaje_Comision_Conductor, Cantidad_Combustible, "
'                strSQL += "Corredor_Vial_Habilitado, 'Foto' = CASE WHEN Foto IS NULL THEN NULL ELSE 1 END, TIRU_Codigo, Km_Pesado, CARU_Codigo"
'                strSQL += "  FROM Rutas"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.lonCodigo


'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()
'                sdrRuta = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrRuta.Read() Then
'                    Me.strNombre = sdrRuta("Nombre").ToString()
'                    Long.TryParse(sdrRuta("Codigo").ToString(), Me.lonCodigo)
'                    Me.strCodigoTercero = sdrRuta("Codigo_Tercero").ToString()
'                    Me.objCiudadOrigen.Existe_Registro(Me.objEmpresa, Val(sdrRuta("CIUD_Origen").ToString()))
'                    Me.objCiudadDestino.Existe_Registro(Me.objEmpresa, Val(sdrRuta("CIUD_Destino").ToString()))
'                    Integer.TryParse(sdrRuta("Distancia").ToString(), Me.lonDistancia)
'                    Me.objCataFormaCalculoAnticipo.Campo1 = sdrRuta("Forma_Calculo_Anticipo").ToString()
'                    Double.TryParse(sdrRuta("Anticipo_Ruta").ToString(), Me.dblAnticipo)
'                    Double.TryParse(sdrRuta("Porcentaje_Anticipo").ToString(), Me.dblPorcentajeAnticipo)
'                    Double.TryParse(sdrRuta("Horas_Estimadas").ToString(), Me.dblHorasEstimadas)
'                    Double.TryParse(sdrRuta("Porcentaje_ICA_Transportador").ToString(), Me.dblPorcentajeIcaTransportador)

'                    Double.TryParse(sdrRuta("Porcentaje_Seguro_Transportador").ToString(), Me.dblPorcentajeSeguroTransportador)
'                    Double.TryParse(sdrRuta("Valor_Flete_Ministerio").ToString, Me.dblValorFleteMinisterio)
'                    Double.TryParse(sdrRuta("Porcentaje_Comision_Conductor").ToString(), Me.dblPorcentajeComisionConductorPropio)
'                    Integer.TryParse(sdrRuta("Codigo_Ruta_Ministerio").ToString(), Me.intCodigoRutaMinisterio)
'                    Me.intCorredorVialHabilitado = Val(sdrRuta("Corredor_Vial_Habilitado").ToString())
'                    Double.TryParse(sdrRuta("Cantidad_Combustible").ToString, Me.dblCantidadCombustible)
'                    Me.objCataTipoRuta.Campo1 = sdrRuta("TIRU_Codigo").ToString()
'                    Me.intKmPesado = Val(sdrRuta("Km_Pesado").ToString)
'                    Me.CategoriaRutas = sdrRuta("CARU_Codigo").ToString

'                    If sdrRuta("Foto") IsNot DBNull.Value Then
'                        bolFoto = True
'                    Else
'                        bolFoto = False
'                    End If

'                    Consultar = True
'                Else
'                    Me.lonCodigo = 0
'                    Consultar = False
'                End If

'                sdrRuta.Close()
'                sdrRuta.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function


'        Public Function Consultar_Hotel_Ruta() As Boolean
'            Try
'                Consultar_Hotel_Ruta = True

'                Dim sdrRuta As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo, HOTE_ID, Ruta, Hotel"
'                strSQL += " FROM V_Hotel_Rutas"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND HOTE_ID = " & Me.lonCodigo


'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()
'                sdrRuta = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrRuta.Read() Then
'                    'Me.objHoteles.ID = Val(sdrRuta("HOTE_ID").ToString())
'                    Me.objCiudadOrigen.Codigo = Val(sdrRuta("Ruta").ToString())
'                    ' Me.objHoteles.Nombre = sdrRuta("Hotel").ToString()

'                    Consultar_Hotel_Ruta = True
'                Else
'                    Me.lonCodigo = 0
'                    Consultar_Hotel_Ruta = False
'                End If

'                sdrRuta.Close()
'                sdrRuta.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Consultar_Hotel_Ruta = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Consultar_Hotel_Ruta(ByRef Empresa As Empresas, ByVal Codigo As Integer) As Boolean
'            Try
'                Consultar_Hotel_Ruta = True

'                Dim sdrRuta As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo, HOTE_ID, Ruta, Hotel"
'                strSQL += " FROM V_Hotel_Rutas"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND HOTE_ID = " & Me.lonCodigo


'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()
'                sdrRuta = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrRuta.Read() Then
'                    ' Me.objHoteles.ID = Val(sdrRuta("HOTE_ID").ToString())
'                    Me.objCiudadOrigen.Codigo = Val(sdrRuta("Ruta").ToString())
'                    ' Me.objHoteles.Nombre = sdrRuta("Hotel").ToString()

'                    Consultar_Hotel_Ruta = True
'                Else
'                    Me.lonCodigo = 0
'                    Consultar_Hotel_Ruta = False
'                End If

'                sdrRuta.Close()
'                sdrRuta.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Consultar_Hotel_Ruta = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function
'        Public Function Consultar_Parqueadero_Ruta(ByRef Empresa As Empresas, ByVal Codigo As Integer) As Boolean
'            Try
'                Consultar_Parqueadero_Ruta = True

'                Dim sdrRuta As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo, PARQ_ID, Ruta, Parqueadero"
'                strSQL += " FROM V_Parqueadero_Rutas"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND PARQ_ID = " & Me.lonCodigo


'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()
'                sdrRuta = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrRuta.Read() Then
'                    ' Me.ObjParqueaderos.ID = Val(sdrRuta("PARQ_ID").ToString())
'                    Me.objCiudadOrigen.Codigo = Val(sdrRuta("Ruta").ToString())
'                    ' Me.ObjParqueaderos.Nombre = sdrRuta("Parqueadero").ToString()

'                    Consultar_Parqueadero_Ruta = True
'                Else
'                    Me.lonCodigo = 0
'                    Consultar_Parqueadero_Ruta = False
'                End If

'                sdrRuta.Close()
'                sdrRuta.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Consultar_Parqueadero_Ruta = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function
'        Public Function Cargar_Objeto(ByRef Empresa As Empresas, ByVal Codigo As Integer) As Boolean
'            Try
'                Cargar_Objeto = True
'                Dim sdrRuta As SqlDataReader
'                Me.objEmpresa = Empresa
'                Me.lonCodigo = Codigo

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo, Codigo, Nombre, Codigo_Tercero, CIUD_Origen, CIUD_Destino, Codigo_Ruta_Ministerio, "
'                strSQL += " Anticipo_Ruta, Porcentaje_Anticipo, Forma_Calculo_Anticipo, Valor_Flete_Ministerio"
'                strSQL += " FROM Rutas"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()
'                sdrRuta = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrRuta.Read() Then

'                    Me.strNombre = sdrRuta("Nombre").ToString()
'                    Me.strCodigoTercero = sdrRuta("Codigo_Tercero").ToString()
'                    Long.TryParse(sdrRuta("CIUD_Origen").ToString(), Me.objCiudadOrigen.Codigo)
'                    Long.TryParse(sdrRuta("CIUD_Destino").ToString(), Me.objCiudadDestino.Codigo)

'                    Double.TryParse(sdrRuta("Anticipo_Ruta").ToString(), Me.dblAnticipo)
'                    Double.TryParse(sdrRuta("Porcentaje_Anticipo").ToString(), Me.dblPorcentajeAnticipo)
'                    Double.TryParse(sdrRuta("Valor_Flete_Ministerio").ToString(), Me.dblValorFleteMinisterio)
'                    Me.objCataFormaCalculoAnticipo.Campo1 = sdrRuta("Forma_Calculo_Anticipo").ToString()
'                    Cargar_Objeto = True

'                Else
'                    Me.lonCodigo = 0
'                    Cargar_Objeto = False
'                End If

'                sdrRuta.Close()
'                sdrRuta.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Cargar_Objeto: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Cargar_Objeto: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Cargar_Objeto = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Existe_Registro() As Boolean
'            Try

'                Existe_Registro = True
'                Dim sdrRuta As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo, Nombre FROM Rutas"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.lonCodigo


'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrRuta = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrRuta.Read() Then
'                    Me.strNombre = sdrRuta("Nombre").ToString()
'                    Existe_Registro = True
'                Else
'                    Me.lonCodigo = 0
'                    Existe_Registro = False
'                End If

'                sdrRuta.Close()
'                sdrRuta.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Existe_Registro: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Existe_Registro: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Existe_Registro = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Existe_Registro(ByRef Empresa As Empresas, ByVal Codigo As Long) As Boolean
'            Try

'                Me.objEmpresa = Empresa
'                Me.lonCodigo = Codigo

'                Existe_Registro = True

'                Dim sdrRuta As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo, Nombre FROM Rutas"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.lonCodigo


'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrRuta = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrRuta.Read() Then
'                    Me.strNombre = sdrRuta("Nombre").ToString()
'                    Existe_Registro = True
'                Else
'                    Me.lonCodigo = 0
'                    Existe_Registro = False
'                End If

'                sdrRuta.Close()
'                sdrRuta.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Existe_Registro: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Existe_Registro: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Existe_Registro = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        'Public Function Agregar_Peajes(ByVal Empresa As Empresas, ByVal Peajes As clsPeaje, Optional ByRef Mensaje As String = "") As Boolean
'        '    Try

'        '        Me.objEmpresa = Empresa
'        '        ' Me.objPeajes = Peajes
'        '        Dim lonNumeRegi As Long

'        '        If Datos_Requeridos_Peajes(Mensaje) Then

'        '            Me.objGeneral.ConexionSQL.Open()

'        '            Dim ComandoSQL As SqlCommand = New SqlCommand("sp_insertar_peaje_rutas", Me.objGeneral.ConexionSQL)

'        '            ComandoSQL.CommandType = CommandType.StoredProcedure
'        '            ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'        '            ComandoSQL.Parameters.Add("@par_PEAJ_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_PEAJ_Codigo").Value = Me.objPeajes.Id
'        '            ComandoSQL.Parameters.Add("@par_RUTA_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_RUTA_Codigo").Value = Me.lonCodigo
'        '            ComandoSQL.Parameters.Add("@par_Orden", SqlDbType.Int) : ComandoSQL.Parameters("@par_Orden").Value = Me.intOrden

'        '            lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'        '            If lonNumeRegi > 0 Then
'        '                Agregar_Peajes = True
'        '            Else
'        '                Agregar_Peajes = False
'        '            End If

'        '            ComandoSQL.Dispose()
'        '        Else
'        '            Agregar_Peajes = False
'        '        End If
'        '    Catch ex As Exception
'        '        Agregar_Peajes = False
'        '        Try
'        '            Me.objGeneral.ExcepcionSQL = ex
'        '            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Agregar_Peajes: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'        '        Catch Exc As Exception
'        '            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Agregar_Peajes: " & Me.objGeneral.Traducir_Error(ex.Message)
'        '        End Try
'        '    Finally
'        '        If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'        '            Me.objGeneral.ConexionSQL.Close()
'        '        End If
'        '    End Try

'        '    Mensaje = Me.strError
'        'End Function

'        'Public Function Desasignar_Peajes(ByVal Empresa As Empresas, ByVal Peaje As clsPeaje, Optional ByRef Mensaje As String = "") As Boolean

'        '    Try

'        '        Me.objEmpresa = Empresa
'        '        Me.objPeajes = Peaje
'        '        Dim lonNumeRegi As Long

'        '        Me.objGeneral.ConexionSQL.Open()

'        '        Dim ComandoSQL As SqlCommand = New SqlCommand("sp_borrar_peaje_rutas", Me.objGeneral.ConexionSQL)

'        '        ComandoSQL.CommandType = CommandType.StoredProcedure
'        '        ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'        '        ComandoSQL.Parameters.Add("@par_PEAJ_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_PEAJ_Codigo").Value = Me.objPeajes.Id
'        '        ComandoSQL.Parameters.Add("@par_RUTA_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_RUTA_Codigo").Value = Me.lonCodigo

'        '        lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'        '        If lonNumeRegi > 0 Then
'        '            Desasignar_Peajes = True
'        '        Else
'        '            Desasignar_Peajes = False
'        '        End If
'        '        ComandoSQL.Dispose()

'        '    Catch ex As Exception
'        '        Desasignar_Peajes = False
'        '        Try
'        '            Me.objGeneral.ExcepcionSQL = ex
'        '            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Desasignar_Peajes: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'        '        Catch Exc As Exception
'        '            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Desasignar_Peajes: " & Me.objGeneral.Traducir_Error(ex.Message)
'        '        End Try
'        '    Finally
'        '        If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'        '            Me.objGeneral.ConexionSQL.Close()
'        '        End If
'        '    End Try
'        '    Mensaje = Me.strError

'        'End Function





'        'Public Function Modificar_Peajes(ByVal Empresa As Empresas, ByVal Peajes As clsPeaje, Optional ByRef Mensaje As String = "") As Boolean

'        '    Try

'        '        Me.objEmpresa = Empresa
'        '        Me.objPeajes = Peajes
'        '        Dim lonNumeRegi As Long

'        '        If Datos_Requeridos_Peajes(Mensaje) Then

'        '            Me.objGeneral.ConexionSQL.Open()

'        '            Dim ComandoSQL As SqlCommand = New SqlCommand("sp_modificar_peaje_rutas", Me.objGeneral.ConexionSQL)

'        '            ComandoSQL.CommandType = CommandType.StoredProcedure
'        '            ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'        '            ComandoSQL.Parameters.Add("@par_PEAJ_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_PEAJ_Codigo").Value = Me.objPeajes.Id
'        '            ComandoSQL.Parameters.Add("@par_RUTA_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_RUTA_Codigo").Value = Me.lonCodigo
'        '            ComandoSQL.Parameters.Add("@par_Orden", SqlDbType.Int) : ComandoSQL.Parameters("@par_Orden").Value = Me.intOrden

'        '            lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'        '            If lonNumeRegi > 0 Then
'        '                Modificar_Peajes = True
'        '            Else
'        '                Modificar_Peajes = False
'        '            End If

'        '            ComandoSQL.Dispose()

'        '        Else
'        '            Modificar_Peajes = False
'        '        End If
'        '    Catch ex As Exception
'        '        Modificar_Peajes = False
'        '        Try
'        '            Me.objGeneral.ExcepcionSQL = ex
'        '            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Modificar_Peajes: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'        '        Catch Exc As Exception
'        '            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Modificar_Peajes: " & Me.objGeneral.Traducir_Error(ex.Message)
'        '        End Try
'        '    Finally
'        '        If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'        '            Me.objGeneral.ConexionSQL.Close()
'        '        End If
'        '    End Try

'        '    Mensaje = Me.strError
'        'End Function

'        'Public Function Cargar_Peajes(ByVal Empresa As Empresas, ByVal Peajes As clsPeaje) As Boolean

'        '    Try

'        '        Me.objEmpresa = Empresa
'        '        Me.objPeajes = Peajes

'        '        Cargar_Peajes = True

'        '        Dim sdrRuta As SqlDataReader

'        '        Dim ComandoSQL As SqlCommand

'        '        strSQL = "SELECT * FROM Peaje_Rutas"
'        '        strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'        '        strSQL += " AND PEAJ_Codigo = " & Me.objPeajes.Id
'        '        strSQL += " AND RUTA_Codigo = " & Me.lonCodigo

'        '        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'        '        Me.objGeneral.ConexionSQL.Open()
'        '        sdrRuta = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'        '        If sdrRuta.Read() Then

'        '            Long.TryParse(sdrRuta("Orden").ToString, Me.intOrden)

'        '        End If

'        '        sdrRuta.Close()
'        '        sdrRuta.Dispose()
'        '        ComandoSQL.Dispose()

'        '    Catch ex As Exception
'        '        Try
'        '            Me.objGeneral.ExcepcionSQL = ex
'        '            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Cargar_Peajes: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'        '        Catch Exc As Exception
'        '            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Cargar_Peajes: " & Me.objGeneral.Traducir_Error(ex.Message)
'        '        End Try
'        '        Cargar_Peajes = False
'        '    Finally
'        '        If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'        '            Me.objGeneral.ConexionSQL.Close()
'        '        End If
'        '    End Try

'        'End Function

'        'Public Function Cargar_Hotel(ByVal Empresa As Empresas, ByVal Hotel As clsHoteles) As Boolean

'        '    Try

'        '        Me.objEmpresa = Empresa
'        '        Me.objHoteles = Hotel

'        '        Cargar_Hotel = True

'        '        Dim sdrRuta As SqlDataReader

'        '        Dim ComandoSQL As SqlCommand

'        '        strSQL = "SELECT * FROM ruta_hoteles"
'        '        strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'        '        strSQL += " AND HOTE_ID = " & Me.objHoteles.ID
'        '        strSQL += " AND RUTA_Codigo = " & Me.lonCodigo

'        '        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'        '        Me.objGeneral.ConexionSQL.Open()
'        '        sdrRuta = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'        '        If sdrRuta.Read() Then



'        '        End If

'        '        sdrRuta.Close()
'        '        sdrRuta.Dispose()
'        '        ComandoSQL.Dispose()

'        '    Catch ex As Exception
'        '        Try
'        '            Me.objGeneral.ExcepcionSQL = ex
'        '            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Cargar_Peajes: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'        '        Catch Exc As Exception
'        '            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Cargar_Peajes: " & Me.objGeneral.Traducir_Error(ex.Message)
'        '        End Try
'        '        Cargar_Hotel = False
'        '    Finally
'        '        If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'        '            Me.objGeneral.ConexionSQL.Close()
'        '        End If
'        '    End Try

'        'End Function
'        'Public Function Cargar_Parqueadero(ByVal Empresa As Empresas, ByVal Parqueadero As clsParqueaderos) As Boolean

'        '    Try

'        '        Me.objEmpresa = Empresa
'        '        Me.ObjParqueaderos = Parqueadero

'        '        Cargar_Parqueadero = True

'        '        Dim sdrRuta As SqlDataReader

'        '        Dim ComandoSQL As SqlCommand

'        '        strSQL = "SELECT * FROM ruta_Parqueaderos"
'        '        strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'        '        strSQL += " AND PARQ_ID = " & Me.ObjParqueaderos.ID
'        '        strSQL += " AND RUTA_Codigo = " & Me.lonCodigo

'        '        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'        '        Me.objGeneral.ConexionSQL.Open()
'        '        sdrRuta = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'        '        If sdrRuta.Read() Then

'        '        End If

'        '        sdrRuta.Close()
'        '        sdrRuta.Dispose()
'        '        ComandoSQL.Dispose()

'        '    Catch ex As Exception
'        '        Try
'        '            Me.objGeneral.ExcepcionSQL = ex
'        '            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Cargar_Parqueaderos: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'        '        Catch Exc As Exception
'        '            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Cargar_Parqueaderos: " & Me.objGeneral.Traducir_Error(ex.Message)
'        '        End Try
'        '        Cargar_Parqueadero = False
'        '    Finally
'        '        If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'        '            Me.objGeneral.ConexionSQL.Close()
'        '        End If
'        '    End Try

'        'End Function
'        'Public Function Agregar_Hotel(ByVal Empresa As Empresas, ByVal Hoteles As clsHoteles, Optional ByRef Mensaje As String = "") As Boolean
'        '    Try
'        '        Me.objEmpresa = Empresa
'        '        Me.objHoteles = Hoteles
'        '        Dim lonNumeRegi As Long

'        '        If Datos_Requeridos_Hoteles(Mensaje) Then
'        '            Me.objGeneral.ConexionSQL.Open()

'        '            Dim ComandoSQL As SqlCommand = New SqlCommand("sp_insertar_hotel_rutas", Me.objGeneral.ConexionSQL)

'        '            ComandoSQL.CommandType = CommandType.StoredProcedure
'        '            ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'        '            ComandoSQL.Parameters.Add("@par_RUTA_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_RUTA_Codigo").Value = Me.lonCodigo
'        '            ComandoSQL.Parameters.Add("@par_HOTE_ID", SqlDbType.Int) : ComandoSQL.Parameters("@par_HOTE_ID").Value = Me.objHoteles.ID

'        '            lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'        '            If lonNumeRegi > 0 Then
'        '                Agregar_Hotel = True
'        '            Else
'        '                Agregar_Hotel = False
'        '            End If

'        '            ComandoSQL.Dispose()
'        '        Else
'        '            Agregar_Hotel = False
'        '        End If
'        '    Catch ex As Exception
'        '        Agregar_Hotel = False
'        '        Try
'        '            Me.objGeneral.ExcepcionSQL = ex
'        '            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Agregar_Hoteles: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'        '        Catch Exc As Exception
'        '            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Agregar_Hoteles: " & Me.objGeneral.Traducir_Error(ex.Message)
'        '        End Try
'        '    Finally
'        '        If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'        '            Me.objGeneral.ConexionSQL.Close()
'        '        End If
'        '    End Try

'        '    Mensaje = Me.strError
'        'End Function
'        'Public Function Agregar_Parqueadero(ByVal Empresa As Empresas, ByVal Parqueadero As clsParqueaderos, Optional ByRef Mensaje As String = "") As Boolean
'        '    Try
'        '        Me.objEmpresa = Empresa
'        '        Me.ObjParqueaderos = Parqueadero
'        '        Dim lonNumeRegi As Long

'        '        If Datos_Requeridos_Parqueaderos(Mensaje) Then
'        '            Me.objGeneral.ConexionSQL.Open()

'        '            Dim ComandoSQL As SqlCommand = New SqlCommand("sp_insertar_Parqueadero_rutas", Me.objGeneral.ConexionSQL)

'        '            ComandoSQL.CommandType = CommandType.StoredProcedure
'        '            ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'        '            ComandoSQL.Parameters.Add("@par_RUTA_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_RUTA_Codigo").Value = Me.lonCodigo
'        '            ComandoSQL.Parameters.Add("@par_PARQ_ID", SqlDbType.Int) : ComandoSQL.Parameters("@par_PARQ_ID").Value = Me.ObjParqueaderos.ID

'        '            lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'        '            If lonNumeRegi > 0 Then
'        '                Agregar_Parqueadero = True
'        '            Else
'        '                Agregar_Parqueadero = False
'        '            End If

'        '            ComandoSQL.Dispose()
'        '        Else
'        '            Agregar_Parqueadero = False
'        '        End If
'        '    Catch ex As Exception
'        '        Agregar_Parqueadero = False
'        '        Try
'        '            Me.objGeneral.ExcepcionSQL = ex
'        '            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Agregar_Parqueaderos: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'        '        Catch Exc As Exception
'        '            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Agregar_Parqueaderos: " & Me.objGeneral.Traducir_Error(ex.Message)
'        '        End Try
'        '    Finally
'        '        If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'        '            Me.objGeneral.ConexionSQL.Close()
'        '        End If
'        '    End Try

'        'End Function


'        'Public Function Modificar_Hotel(ByVal Empresa As Empresas, ByVal Hoteles As clsHoteles, Optional ByRef Mensaje As String = "") As Boolean
'        '    Try
'        '        Me.objEmpresa = Empresa
'        '        Me.objHoteles = Hoteles
'        '        Dim lonNumeRegi As Long

'        '        If Datos_Requeridos_Hoteles(Mensaje) Then

'        '            Me.objGeneral.ConexionSQL.Open()

'        '            Dim ComandoSQL As SqlCommand = New SqlCommand("sp_modificar_hotel_rutas", Me.objGeneral.ConexionSQL)

'        '            ComandoSQL.CommandType = CommandType.StoredProcedure

'        '            ComandoSQL.Parameters.Add("@par_T_HOTE_ID", SqlDbType.Int) : ComandoSQL.Parameters("@par_T_HOTE_ID").Value = Me.IdTemporal
'        '            ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'        '            ComandoSQL.Parameters.Add("@par_HOTE_ID", SqlDbType.Int) : ComandoSQL.Parameters("@par_HOTE_ID").Value = Me.objHoteles.ID
'        '            ComandoSQL.Parameters.Add("@par_RUTA_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_RUTA_Codigo").Value = Me.lonCodigo


'        '            lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'        '            If lonNumeRegi > 0 Then
'        '                Modificar_Hotel = True
'        '            Else
'        '                Modificar_Hotel = False
'        '            End If

'        '            ComandoSQL.Dispose()
'        '        Else
'        '            Modificar_Hotel = False
'        '        End If
'        '    Catch ex As Exception
'        '        Modificar_Hotel = False
'        '        Try
'        '            Me.objGeneral.ExcepcionSQL = ex
'        '            strError = "En la clase " & Me.ToString & " se presentó un error en la función Modificar_Hoteles: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'        '        Catch Exc As Exception
'        '            strError = "En la clase " & Me.ToString & " se presentó un error en la función Modificar_Hoteles: " & Me.objGeneral.Traducir_Error(ex.Message)
'        '        End Try
'        '    Finally
'        '        If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'        '            Me.objGeneral.ConexionSQL.Close()
'        '        End If
'        '    End Try
'        '    Mensaje = Me.strError

'        'End Function
'        'Public Function Modificar_Parqueaderos(ByVal Empresa As Empresas, ByVal Parqueaderos As clsParqueaderos, Optional ByRef Mensaje As String = "") As Boolean
'        '    Try
'        '        Me.objEmpresa = Empresa
'        '        Me.ObjParqueaderos = Parqueaderos
'        '        Dim lonNumeRegi As Long

'        '        If Datos_Requeridos_Parqueaderos(Mensaje) Then

'        '            Me.objGeneral.ConexionSQL.Open()

'        '            Dim ComandoSQL As SqlCommand = New SqlCommand("sp_modificar_parqueadero_rutas", Me.objGeneral.ConexionSQL)

'        '            ComandoSQL.CommandType = CommandType.StoredProcedure

'        '            ComandoSQL.Parameters.Add("@par_T_PARQ_ID", SqlDbType.Int) : ComandoSQL.Parameters("@par_T_PARQ_ID").Value = Me.IdTemporal
'        '            ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'        '            ComandoSQL.Parameters.Add("@par_PARQ_ID", SqlDbType.Int) : ComandoSQL.Parameters("@par_PARQ_ID").Value = Me.ObjParqueaderos.ID
'        '            ComandoSQL.Parameters.Add("@par_RUTA_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_RUTA_Codigo").Value = Me.lonCodigo


'        '            lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'        '            If lonNumeRegi > 0 Then
'        '                Modificar_Parqueaderos = True
'        '            Else
'        '                Modificar_Parqueaderos = False
'        '            End If

'        '            ComandoSQL.Dispose()
'        '        Else
'        '            Modificar_Parqueaderos = False
'        '        End If
'        '    Catch ex As Exception
'        '        Modificar_Parqueaderos = False
'        '        Try
'        '            Me.objGeneral.ExcepcionSQL = ex
'        '            strError = "En la clase " & Me.ToString & " se presentó un error en la función Modificar_Parqueaderos: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'        '        Catch Exc As Exception
'        '            strError = "En la clase " & Me.ToString & " se presentó un error en la función Modificar_Parqueaderos: " & Me.objGeneral.Traducir_Error(ex.Message)
'        '        End Try
'        '    Finally
'        '        If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'        '            Me.objGeneral.ConexionSQL.Close()
'        '        End If
'        '    End Try
'        '    Mensaje = Me.strError

'        'End Function


'        'Public Function Desasignar_Hoteles_Ruta(ByVal Empresa As Empresas, ByVal Hotel As clsHoteles, Optional ByRef Mensaje As String = "") As Boolean

'        '    Try

'        '        Me.objEmpresa = Empresa
'        '        Me.objHoteles = Hotel
'        '        Dim lonNumeRegi As Long

'        '        Me.objGeneral.ConexionSQL.Open()

'        '        Dim ComandoSQL As SqlCommand = New SqlCommand("sp_borrar_hotel_rutas", Me.objGeneral.ConexionSQL)

'        '        ComandoSQL.CommandType = CommandType.StoredProcedure
'        '        ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'        '        ComandoSQL.Parameters.Add("@par_HOTE_ID", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_HOTE_ID").Value = Me.objHoteles.ID
'        '        ComandoSQL.Parameters.Add("@par_RUTA_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_RUTA_Codigo").Value = Me.lonCodigo

'        '        lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'        '        If lonNumeRegi > 0 Then
'        '            Desasignar_Hoteles_Ruta = True
'        '        Else
'        '            Desasignar_Hoteles_Ruta = False
'        '        End If
'        '        ComandoSQL.Dispose()

'        '    Catch ex As Exception
'        '        Desasignar_Hoteles_Ruta = False
'        '        Try
'        '            Me.objGeneral.ExcepcionSQL = ex
'        '            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Desasignar_Hoteles: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'        '        Catch Exc As Exception
'        '            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Desasignar_Hoteles: " & Me.objGeneral.Traducir_Error(ex.Message)
'        '        End Try
'        '    Finally
'        '        If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'        '            Me.objGeneral.ConexionSQL.Close()
'        '        End If
'        '    End Try
'        '    Mensaje = Me.strError

'        'End Function
'        'Public Function Desasignar_Parqueaderos_Ruta(ByVal Empresa As Empresas, ByVal Parqueaderos As clsParqueaderos, Optional ByRef Mensaje As String = "") As Boolean

'        '    Try

'        '        Me.objEmpresa = Empresa
'        '        Me.ObjParqueaderos = Parqueaderos
'        '        Dim lonNumeRegi As Long

'        '        Me.objGeneral.ConexionSQL.Open()

'        '        Dim ComandoSQL As SqlCommand = New SqlCommand("sp_borrar_Parqueadero_Rutas", Me.objGeneral.ConexionSQL)

'        '        ComandoSQL.CommandType = CommandType.StoredProcedure
'        '        ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'        '        ComandoSQL.Parameters.Add("@par_PARQ_ID", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_PARQ_ID").Value = Me.ObjParqueaderos.ID
'        '        ComandoSQL.Parameters.Add("@par_RUTA_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_RUTA_Codigo").Value = Me.lonCodigo

'        '        lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'        '        If lonNumeRegi > 0 Then
'        '            Desasignar_Parqueaderos_Ruta = True
'        '        Else
'        '            Desasignar_Parqueaderos_Ruta = False
'        '        End If
'        '        ComandoSQL.Dispose()

'        '    Catch ex As Exception
'        '        Desasignar_Parqueaderos_Ruta = False
'        '        Try
'        '            Me.objGeneral.ExcepcionSQL = ex
'        '            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Desasignar_Parqueaderos: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'        '        Catch Exc As Exception
'        '            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Desasignar_Parqueaderos: " & Me.objGeneral.Traducir_Error(ex.Message)
'        '        End Try
'        '    Finally
'        '        If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'        '            Me.objGeneral.ConexionSQL.Close()
'        '        End If
'        '    End Try
'        '    Mensaje = Me.strError

'        'End Function


'        'Public Function Agregar_Puestos(ByVal Empresa As Empresas, ByVal PuestoControl As clsPuestosControl, Optional ByRef Mensaje As String = "") As Boolean

'        '    Try

'        '        Me.objEmpresa = Empresa
'        '        Me.objPuestosControl = PuestoControl

'        '        If Datos_Requeridos_Puestos(Mensaje) Then
'        '            Dim lonNumeRegi As Long

'        '            Me.objGeneral.ConexionSQL.Open()

'        '            Dim ComandoSQL As SqlCommand = New SqlCommand("sp_insertar_puesto_control_rutas", Me.objGeneral.ConexionSQL)

'        '            ComandoSQL.CommandType = CommandType.StoredProcedure
'        '            ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'        '            ComandoSQL.Parameters.Add("@par_PUCO_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_PUCO_Codigo").Value = Me.objPuestosControl.Codigo
'        '            ComandoSQL.Parameters.Add("@par_RUTA_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_RUTA_Codigo").Value = Me.lonCodigo
'        '            ComandoSQL.Parameters.Add("@par_Orden", SqlDbType.Int) : ComandoSQL.Parameters("@par_Orden").Value = Me.objPuestosControl.Orden
'        '            ComandoSQL.Parameters.Add("@par_Horas_Estimadas_Arribo", SqlDbType.Float) : ComandoSQL.Parameters("@par_Horas_Estimadas_Arribo").Value = Me.objPuestosControl.HorasEstimadasArribo

'        '            lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'        '            If lonNumeRegi > 0 Then
'        '                Agregar_Puestos = True
'        '            Else
'        '                Agregar_Puestos = False
'        '            End If

'        '            ComandoSQL.Dispose()
'        '        Else
'        '            Agregar_Puestos = False
'        '        End If
'        '    Catch ex As Exception
'        '        Agregar_Puestos = False
'        '        Try
'        '            Me.objGeneral.ExcepcionSQL = ex
'        '            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Agregar_Puestos: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'        '        Catch Exc As Exception
'        '            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Agregar_Puestos: " & Me.objGeneral.Traducir_Error(ex.Message)
'        '        End Try
'        '    Finally
'        '        If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'        '            Me.objGeneral.ConexionSQL.Close()
'        '        End If
'        '    End Try

'        '    Mensaje = Me.strError
'        'End Function

'        Public Function Guardar(Optional ByRef Mensaje As String = "") As Boolean

'            If Datos_Requeridos(Mensaje) Then
'                If Not bolModificar Then
'                    Guardar = Insertar_SQL()
'                Else
'                    Guardar = Modificar_SQL()
'                End If
'            Else
'                Guardar = False
'            End If
'            Mensaje = Me.strError
'        End Function

'        Public Function Eliminar(Optional ByRef Mensaje As String = "") As Boolean
'            Eliminar = Borrar_SQL()
'            Mensaje = Me.strError
'        End Function

'#End Region

'#Region "Funciones Privadas"

'        Private Function Insertar_SQL() As Boolean

'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_insertar_rutas", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure
'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Codigo").Direction = ParameterDirection.Output
'                ComandoSQL.Parameters.Add("@par_Codigo_Tercero", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_Codigo_Tercero").Value = Val(Me.strCodigoTercero)
'                ComandoSQL.Parameters.Add("@par_Nombre", SqlDbType.VarChar, 50) : ComandoSQL.Parameters("@par_Nombre").Value = Me.strNombre

'                ComandoSQL.Parameters.Add("@par_CIUD_Origen", SqlDbType.Int) : ComandoSQL.Parameters("@par_CIUD_Origen").Value = Val(Me.objCiudadOrigen.Codigo)
'                ComandoSQL.Parameters.Add("@par_CIUD_Destino", SqlDbType.Int) : ComandoSQL.Parameters("@par_CIUD_Destino").Value = Val(Me.objCiudadDestino.Codigo)
'                ComandoSQL.Parameters.Add("@par_Distancia", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Distancia").Value = Val(Me.lonDistancia)
'                ComandoSQL.Parameters.Add("@par_Forma_Calculo_Anticipo", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Forma_Calculo_Anticipo").Value = Val(Me.CataFormaCalculoAnticipo.Retorna_Codigo)
'                ComandoSQL.Parameters.Add("@par_Anticipo_Ruta", SqlDbType.Money) : ComandoSQL.Parameters("@par_Anticipo_Ruta").Value = Me.dblAnticipo

'                ComandoSQL.Parameters.Add("@par_Porcentaje_Anticipo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Porcentaje_Anticipo").Value = Me.dblPorcentajeAnticipo
'                ComandoSQL.Parameters.Add("@par_Horas_Estimadas", SqlDbType.Int) : ComandoSQL.Parameters("@par_Horas_Estimadas").Value = Me.dblHorasEstimadas
'                ComandoSQL.Parameters.Add("@par_Porcentaje_ICA_Transportador", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Porcentaje_ICA_Transportador").Value = Me.dblPorcentajeIcaTransportador
'                ComandoSQL.Parameters.Add("@par_Porcentaje_Seguro_Transportador", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Porcentaje_Seguro_Transportador").Value = Me.dblPorcentajeSeguroTransportador
'                ComandoSQL.Parameters.Add("@par_Porcentaje_Comision_Conductor", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Porcentaje_Comision_Conductor").Value = Me.dblPorcentajeComisionConductorPropio
'                ComandoSQL.Parameters.Add("@par_Valor_Flete_Ministerio", SqlDbType.Money) : ComandoSQL.Parameters("@par_Valor_Flete_Ministerio").Value = Me.dblValorFleteMinisterio

'                ComandoSQL.Parameters.Add("@par_USUA_Crea", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_USUA_Crea").Value = Me.objUsuario.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo_Ruta_Ministerio", SqlDbType.Int) : ComandoSQL.Parameters("@par_Codigo_Ruta_Ministerio").Value = Me.intCodigoRutaMinisterio
'                ComandoSQL.Parameters.Add("@par_Corredor_Vial_Habilitado", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Corredor_Vial_Habilitado").Value = Me.intCorredorVialHabilitado
'                ComandoSQL.Parameters.Add("@par_Cantidad_Combustible", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Cantidad_Combustible").Value = Me.dblCantidadCombustible
'                ComandoSQL.Parameters.Add("@par_TIRU_Codigo", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_TIRU_Codigo").Value = Val(Me.CataTipoRuta.Retorna_Codigo)
'                ComandoSQL.Parameters.Add("@par_Km_Pesado", SqlDbType.Int) : ComandoSQL.Parameters("@par_Km_Pesado").Value = Me.intKmPesado
'                ComandoSQL.Parameters.Add("@par_CARU_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_CARU_Codigo").Value = Me.intCategoriaRutas
'                ComandoSQL.Parameters.Add("@par_Estado", SqlDbType.Int) : ComandoSQL.Parameters("@par_Estado").Value = Me.intEstado


'                If Not IsNothing(Me.bytFoto) Then
'                    If Me.bytFoto(0) = BYTES_IMAGEN_DEFECTO Then
'                        ComandoSQL.Parameters.Add("@par_Foto", SqlDbType.Image) : ComandoSQL.Parameters("@par_Foto").Value = DBNull.Value
'                    Else
'                        ComandoSQL.Parameters.Add("@par_Foto", SqlDbType.Image) : ComandoSQL.Parameters("@par_Foto").Value = Me.bytFoto
'                    End If
'                Else
'                    ComandoSQL.Parameters.Add("@par_Foto", SqlDbType.Image) : ComandoSQL.Parameters("@par_Foto").Value = DBNull.Value
'                End If

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Me.lonCodigo = ComandoSQL.Parameters("@par_Codigo").Value
'                    Insertar_SQL = True
'                Else
'                    Insertar_SQL = False
'                End If

'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Insertar_SQL = False
'                Me.lonCodigo = 0
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Insertar_SQL: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Insertar_SQL: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Private Function Modificar_SQL() As Boolean

'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_modificar_rutas", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure
'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_Codigo").Value = Val(Me.lonCodigo)
'                ComandoSQL.Parameters.Add("@par_Codigo_Tercero", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_Codigo_Tercero").Value = Val(Me.strCodigoTercero)
'                ComandoSQL.Parameters.Add("@par_Nombre", SqlDbType.VarChar, 50) : ComandoSQL.Parameters("@par_Nombre").Value = Me.strNombre
'                ComandoSQL.Parameters.Add("@par_CIUD_Origen", SqlDbType.Int) : ComandoSQL.Parameters("@par_CIUD_Origen").Value = Val(Me.objCiudadOrigen.Codigo)

'                ComandoSQL.Parameters.Add("@par_CIUD_Destino", SqlDbType.Int) : ComandoSQL.Parameters("@par_CIUD_Destino").Value = Val(Me.objCiudadDestino.Codigo)
'                ComandoSQL.Parameters.Add("@par_Distancia", SqlDbType.Int) : ComandoSQL.Parameters("@par_Distancia").Value = Me.lonDistancia
'                ComandoSQL.Parameters.Add("@par_Forma_Calculo_Anticipo", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Forma_Calculo_Anticipo").Value = Val(Me.CataFormaCalculoAnticipo.Retorna_Codigo)
'                ComandoSQL.Parameters.Add("@par_Anticipo_Ruta", SqlDbType.Money) : ComandoSQL.Parameters("@par_Anticipo_Ruta").Value = Me.dblAnticipo
'                ComandoSQL.Parameters.Add("@par_Porcentaje_Anticipo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Porcentaje_Anticipo").Value = Me.dblPorcentajeAnticipo

'                ComandoSQL.Parameters.Add("@par_Horas_Estimadas", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Horas_Estimadas").Value = Me.dblHorasEstimadas
'                ComandoSQL.Parameters.Add("@par_Porcentaje_ICA_Transportador", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Porcentaje_ICA_Transportador").Value = Me.dblPorcentajeIcaTransportador
'                ComandoSQL.Parameters.Add("@par_Porcentaje_Seguro_Transportador", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Porcentaje_Seguro_Transportador").Value = Me.dblPorcentajeSeguroTransportador
'                ComandoSQL.Parameters.Add("@par_Porcentaje_Comision_Conductor", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Porcentaje_Comision_Conductor").Value = Me.dblPorcentajeComisionConductorPropio
'                ComandoSQL.Parameters.Add("@par_Valor_Flete_Ministerio", SqlDbType.Money) : ComandoSQL.Parameters("@par_Valor_Flete_Ministerio").Value = Me.dblValorFleteMinisterio

'                ComandoSQL.Parameters.Add("@par_USUA_Modifica", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_USUA_Modifica").Value = Me.objUsuario.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo_Ruta_Ministerio", SqlDbType.Int) : ComandoSQL.Parameters("@par_Codigo_Ruta_Ministerio").Value = Me.intCodigoRutaMinisterio
'                ComandoSQL.Parameters.Add("@par_Corredor_Vial_Habilitado", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Corredor_Vial_Habilitado").Value = Me.intCorredorVialHabilitado
'                ComandoSQL.Parameters.Add("@par_Cantidad_Combustible", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Cantidad_Combustible").Value = Me.dblCantidadCombustible
'                ComandoSQL.Parameters.Add("@par_TIRU_Codigo", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_TIRU_Codigo").Value = Val(Me.CataTipoRuta.Retorna_Codigo)
'                ComandoSQL.Parameters.Add("@par_Km_Pesado", SqlDbType.Int) : ComandoSQL.Parameters("@par_Km_Pesado").Value = Me.intKmPesado
'                ComandoSQL.Parameters.Add("@par_CARU_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_CARU_Codigo").Value = Me.intCategoriaRutas
'                ComandoSQL.Parameters.Add("@par_Estado", SqlDbType.Int) : ComandoSQL.Parameters("@par_Estado").Value = Me.intEstado

'                If Not IsNothing(Me.bytFoto) Then
'                    If Me.bytFoto(0) = BYTES_IMAGEN_DEFECTO Then
'                        ComandoSQL.Parameters.Add("@par_Foto", SqlDbType.Image) : ComandoSQL.Parameters("@par_Foto").Value = DBNull.Value
'                    Else
'                        ComandoSQL.Parameters.Add("@par_Foto", SqlDbType.Image) : ComandoSQL.Parameters("@par_Foto").Value = Me.bytFoto
'                    End If
'                Else
'                    ComandoSQL.Parameters.Add("@par_Foto", SqlDbType.Image) : ComandoSQL.Parameters("@par_Foto").Value = DBNull.Value
'                End If


'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Modificar_SQL = True
'                Else
'                    Modificar_SQL = False
'                End If

'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Modificar_SQL = False
'                Me.lonCodigo = 0
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Modificar_SQL: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Modificar_SQL: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Datos_Requeridos(Optional ByRef Mensaje As String = "") As Boolean

'            Try

'                Dim intCont As Integer
'                Dim bolIngresoSinAnticipo As Boolean = False

'                Datos_Requeridos = True

'                If Me.strNombre = "" Then
'                    intCont += 1
'                    Me.strError += intCont & ". Digite el nombre de la ruta."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.strNombre <> "" And Len(Me.strNombre) < 4 Then
'                    intCont += 1
'                    strError += intCont & ". El nombre de la ruta no puede tener menos de cuatro (4) carácteres"
'                    strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.objCiudadOrigen.Codigo = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Seleccione la ciudad de origen."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.objCiudadDestino.Codigo = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Seleccione la ciudad de destino."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                'JV: Si la ruta es urbana, no debe exigir ingresar la distancia y las horas estimadas
'                If Val(Me.objCataTipoRuta.Campo1) = CODIGO_RUTA_NACIONAL Then
'                    If Me.lonDistancia <= 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". La distancia debe ser un valor numérico mayor a cero."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If

'                    If Me.dblHorasEstimadas <= 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". Las horas estimadas deben ser un valor numérico mayor a cero."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'                'JV: Solamente se controla que el porcentaje del anticipo no supere el 100%
'                If Val(Me.CataFormaCalculoAnticipo.Retorna_Codigo) = Ruta.ANTICIPO_X_PORCENTAJE Then
'                    If Me.dblPorcentajeAnticipo > 100 Then
'                        intCont += 1
'                        Me.strError += intCont & ". El porcentaje del anticipo no puede ser mayor a 100."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If

'                    If Me.dblPorcentajeComisionConductorPropio > 100 Then
'                        intCont += 1
'                        Me.strError += intCont & ". El porcentaje de comisión de conductor propio no puede ser mayor a 100."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'                If Me.dblPorcentajeIcaTransportador > 100 Then
'                    intCont += 1
'                    Me.strError += intCont & ". El porcentaje del ICA transportador no puede ser mayor a 100."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.dblPorcentajeSeguroTransportador > 100 Then
'                    intCont += 1
'                    Me.strError += intCont & ". El porcentaje del seguro transportador no puede ser mayor a 100."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Not IsNumeric(Me.intKmPesado) Then
'                    intCont += 1
'                    Me.strError += intCont & ". El campo Km Pesado debe ser numérico."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'            Catch ex As Exception
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Datos_Requeridos: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Datos_Requeridos: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Datos_Requeridos = False
'            End Try

'        End Function

'        Private Function Datos_Requeridos_Peajes(Optional ByRef Mensaje As String = "") As Boolean
'            Try

'                Dim intCont As Integer
'                Datos_Requeridos_Peajes = True

'                'If Me.objPeajes.Id = 0 Then
'                '    intCont += 1
'                '    Me.strError += intCont & ". Seleccione el peaje."
'                '    Me.strError += " <Br />"
'                '    Datos_Requeridos_Peajes = False
'                'End If

'                If Me.intOrden = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Ingrese el orden del peaje."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Peajes = False
'                End If

'            Catch ex As Exception
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Datos_Requeridos_Peajes: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)

'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Datos_Requeridos_Peajes: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Datos_Requeridos_Peajes = False
'            End Try

'        End Function

'        Private Function Datos_Requeridos_Puestos(Optional ByRef Mensaje As String = "") As Boolean
'            Try

'                Dim intCont As Integer
'                Datos_Requeridos_Puestos = True

'                'If Me.objPuestosControl.Codigo = 0 Then
'                '    intCont += 1
'                '    Me.strError += intCont & ". Seleccione el puesto de control."
'                '    Me.strError += " <Br />"
'                '    Datos_Requeridos_Puestos = False
'                'End If

'                'If Me.objPuestosControl.Orden = 0 Then
'                '    intCont += 1
'                '    Me.strError += intCont & ". Ingrese el orden del puesto control."
'                '    Me.strError += " <Br />"
'                '    Datos_Requeridos_Puestos = False
'                'End If
'                Mensaje = strError

'            Catch ex As Exception
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Datos_Requeridos_Puestos: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)

'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Datos_Requeridos_Puestos: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Datos_Requeridos_Puestos = False
'            End Try
'        End Function

'        Private Function Datos_Requeridos_Hoteles(Optional ByRef Mensaje As String = "") As Boolean
'            Try

'                Dim intCont As Integer
'                Datos_Requeridos_Hoteles = True

'                'If Me.objHoteles.ID = General.CERO Then
'                '    intCont += 1
'                '    Me.strError += intCont & ". Seleccione el hotel"
'                '    Me.strError += " <Br />"
'                '    Datos_Requeridos_Hoteles = False
'                'Else
'                '    Datos_Requeridos_Hoteles = True
'                'End If

'            Catch ex As Exception
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Datos_Requeridos_Peajes: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)

'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Datos_Requeridos_Peajes: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Datos_Requeridos_Hoteles = False
'            End Try

'        End Function
'        Private Function Datos_Requeridos_Parqueaderos(Optional ByRef Mensaje As String = "") As Boolean
'            Try

'                Datos_Requeridos_Parqueaderos = True


'                'If Me.ObjParqueaderos.ID = General.CERO Then
'                '    Me.strError = "Seleccione el Parqueadero "
'                '    Datos_Requeridos_Parqueaderos = False
'                'Else
'                '    Datos_Requeridos_Parqueaderos = True
'                'End If

'            Catch ex As Exception
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Datos_Requeridos_Parqueaderos: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)

'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Datos_Requeridos_Parqueaderos: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Datos_Requeridos_Parqueaderos = False
'            End Try

'        End Function

'        Private Function Borrar_SQL() As Boolean
'            Try

'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_borrar_rutas", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Codigo").Value = Me.lonCodigo

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Borrar_SQL = True
'                Else
'                    Borrar_SQL = False
'                End If

'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Borrar_SQL = False
'                Me.lonCodigo = 0
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Borrar_SQL: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Borrar_SQL: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Eliminar_Foto(ByVal Codigo As Double) As Boolean
'            Try

'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_eliminar_foto_ruta", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Codigo").Value = Codigo

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Eliminar_Foto = True
'                Else
'                    Eliminar_Foto = False
'                End If

'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Eliminar_Foto = False
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Eliminar_Foto: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Eliminar_Foto: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try

'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'#End Region
'    End Class

'End Namespace
