﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.Tesoreria

Namespace Basico.Almacen

    ''' <summary>
    ''' Clase <see cref=" GrupoReferencias"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class GrupoReferencias
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="GrupoReferencias"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="GrupoReferencias"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            Nombre = Read(lector, "Nombre")
            Estado = Read(lector, "Estado")
            CuentaPUC = New PlanUnicoCuentas With {.Codigo = Read(lector, "PLUC_Codigo"), .Nombre = Read(lector, "CuentaPUC"), .CodigoCuenta = Read(lector, "Codigo_Cuenta")}
            Obtener = Read(lector, "Obtener")
            If Obtener = 0 Then
                TotalRegistros = Read(lector, "TotalRegistros")
            End If

        End Sub

        <JsonProperty>
        Public Property CuentaPUC As PlanUnicoCuentas
        <JsonProperty>
        Public Property Nombre As String
    End Class
End Namespace
