﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Tesoreria
Imports EncoExpres.GesCarga.Repositorio.Tesoreria

Namespace Tesoreria
    Public NotInheritable Class PersistenciaCuentasPorCobrar
        Implements IPersistenciaBase(Of CuentasPorCobrar)

        Public Function Consultar(filtro As CuentasPorCobrar) As IEnumerable(Of CuentasPorCobrar) Implements IPersistenciaBase(Of CuentasPorCobrar).Consultar
            Return New RepositorioCuentasPorCobrar().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As CuentasPorCobrar) As Long Implements IPersistenciaBase(Of CuentasPorCobrar).Insertar
            Return New RepositorioCuentasPorCobrar().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As CuentasPorCobrar) As Long Implements IPersistenciaBase(Of CuentasPorCobrar).Modificar
            Return New RepositorioCuentasPorCobrar().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As CuentasPorCobrar) As CuentasPorCobrar Implements IPersistenciaBase(Of CuentasPorCobrar).Obtener
            Return New RepositorioCuentasPorCobrar().Obtener(filtro)
        End Function

        Public Function Anular(entidad As CuentasPorCobrar) As CuentasPorCobrar
            Return New RepositorioCuentasPorCobrar().Anular(entidad)
        End Function

        Public Function ConsultarDetalleObservaciones(entidad As CuentasPorCobrar) As CuentasPorCobrar
            Return New RepositorioCuentasPorCobrar().ConsultarDetalleObservaciones(entidad)
        End Function

        Public Function GuardarDetalleObservaciones(entidad As CuentasPorCobrar) As Long
            Return New RepositorioCuentasPorCobrar().GuardarDetalleObservaciones(entidad)
        End Function
    End Class
End Namespace