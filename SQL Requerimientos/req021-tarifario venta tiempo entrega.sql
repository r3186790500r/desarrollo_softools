USE [ENCOEXPRES]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER TABLE [dbo].[Detalle_Tarifario_Carga_Ventas]
ADD Tiempo_Entrega smallint not null default 1;
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_detalle_tarifario_carga_venta]    
 (                      
 @par_EMPR_Codigo numeric,                           
 @par_ETCV_Numero numeric,
 @par_CIUD_Origen varchar(35) = NULL,
 @par_CIUD_Destino varchar(35) = NULL
 )    
as begin    
 SELECT                             
 DTCV.EMPR_Codigo,                                                   
 DTCV.ETCV_Numero,   
 ENTV.Nombre,  
 TTCA.Nombre AS Tarifa,            
 ISNULL(TTTC.Nombre, '') TTTC_Nombre,       
 ISNULL(CIOR.Nombre,'') AS CiudadOrigen,                        
 ISNULL(CIDE.Nombre,'') AS CiudadDestino,                      
 ISNULL(FPVE.Campo1,'') AS FormaPago,                    
 RUTA.Nombre AS Ruta,    
 isnull(DTCV.Valor_Flete,0) as Valor_Flete,          
 isnull(DTCV.Porcentaje_Seguro,0) as Porcentaje_Seguro,          
 isnull(DTCV.Valor_Escolta,0) as Valor_Escolta,                                  
 isnull(DTCV.Valor_Otros1,0) as Valor_Otros1,                            
 isnull(DTCV.Valor_Otros2,0) as Valor_Otros2,                            
 isnull(DTCV.Valor_Otros3,0) as Valor_Otros3,                            
 isnull(DTCV.Valor_Otros4,0) as Valor_Otros4,                
 isnull(DTCV.Valor_Cargue,0) as Valor_Cargue,                   
 isnull(DTCV.Valor_Descargue,0) as Valor_Descargue,                   
 isnull(DTCV.Valor_Flete_Transportador,0) as Valor_Flete_Transportador,                               
 isnull(DTCV.Fecha_Vigencia_Inicio,'') as Fecha_Vigencia_Inicio,                            
 isnull(DTCV.Fecha_Vigencia_Fin,'') as Fecha_Vigencia_Fin,                               
 Porcentaje_Afiliado ,
 TAPV.Porcentaje_Manejo,
 DTCV.Reexpedicion,     
 DTCV.Estado,
 DTCV.Tiempo_Entrega
                            
 FROM    
 Detalle_Tarifario_Carga_Ventas DTCV LEFT JOIN  Ciudades CIOR ON                         
 DTCV.EMPR_Codigo = CIOR.EMPR_Codigo                        
 AND DTCV.CIUD_Codigo_Origen = CIOR.Codigo 
 
 LEFT JOIN Tarifas_Paqueteria_Ventas TAPV ON 
 DTCV.EMPR_Codigo=TAPV.EMPR_Codigo
 AND DTCV.ETCV_Numero=TAPV.ETCV_Numero
   
 LEFT JOIN Encabezado_Tarifario_Carga_Ventas ENTV ON   
 ENTV.Numero=DTCV.ETCV_Numero   
 AND ENTV.EMPR_Codigo=DTCV.EMPR_Codigo  
  
                        
 LEFT JOIN  Ciudades CIDE ON                        
 DTCV.EMPR_Codigo = CIDE.EMPR_Codigo                        
 AND DTCV.CIUD_Codigo_Destino = CIDE.Codigo                        
                        
 LEFT JOIN  Valor_Catalogos FPVE ON                        
 DTCV.EMPR_Codigo = FPVE.EMPR_Codigo                        
 AND DTCV.CATA_FPVE_Codigo = FPVE.Codigo                    
                    
                        
 LEFT JOIN  Rutas RUTA ON                        
 DTCV.EMPR_Codigo = RUTA.EMPR_Codigo                        
 AND DTCV.RUTA_Codigo = RUTA.Codigo                          
                  
 LEFT JOIN  Tarifa_Transporte_Carga TTCA ON                        
 DTCV.EMPR_Codigo = TTCA.EMPR_Codigo                        
 AND DTCV.TATC_Codigo = TTCA.Codigo             
            
 LEFT JOIN  V_Tipo_Tarifa_Transporte_Carga TTTC ON                          
 DTCV.EMPR_Codigo = TTTC.EMPR_Codigo                              
 AND DTCV.TTTC_Codigo = TTTC.Codigo                               
 AND ISNULL(DTCV.TATC_Codigo,0) = ISNULL(TTTC.TATC_Codigo,0) 
 
                            
 WHERE DTCV.EMPR_Codigo = @par_EMPR_Codigo                            
 AND DTCV.ETCV_Numero = ISNULL(@par_ETCV_Numero,DTCV.ETCV_Numero)
 AND ((ISNUMERIC(@par_CIUD_Origen) > 0 and DTCV.CIUD_Codigo_Origen in (SELECT value FROM STRING_SPLIT(@par_CIUD_Origen,','))) or (ISNUMERIC(@par_CIUD_Origen) = 0 and DTCV.CIUD_Codigo_Origen = DTCV.CIUD_Codigo_Origen))
 AND ((ISNUMERIC(@par_CIUD_Destino) > 0 and DTCV.CIUD_Codigo_Destino in (SELECT value FROM STRING_SPLIT(@par_CIUD_Destino,','))) or (ISNUMERIC(@par_CIUD_Destino) = 0 and DTCV.CIUD_Codigo_Destino = DTCV.CIUD_Codigo_Destino))
         
 ORDER BY RUTA.Nombre,CIOR.Nombre,CIDE.Nombre,TTCA.Nombre, TTTC.Nombre, FPVE.Campo1                      
    END
GO

 CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_detalle_tarifario_ventas]      
(                      
 @par_EMPR_Codigo SMALLINT,                      
 @par_ETCV_Numero NUMERIC,
 @par_LNTC_Codigo VARCHAR(6) = NULL
)                      
AS                       
BEGIN                      
 SELECT                       
 DTCV.EMPR_Codigo,                      
 DTCV.Codigo,                      
 DTCV.ETCV_Numero,                      
 DTCV.LNTC_Codigo,                      
 DTCV.TLNC_Codigo,   
 DTCV.Reexpedicion,
 TTCA.Nombre AS Tarifa,                
 DTCV.TATC_Codigo,                      
 DTCV.TTTC_Codigo,      
 ISNULL(TTTC.Nombre, '') TTTC_Nombre,      
 DTCV.RUTA_Codigo,                   
 ISNULL(DTCV.CATA_FPVE_Codigo,4900) AS CATA_FPVE_Codigo,                
 ISNULL(DTCV.CIUD_Codigo_Origen,0) AS CIUD_Codigo_Origen,                  
 ISNULL(DTCV.CIUD_Codigo_Destino,0) AS CIUD_Codigo_Destino,                  
 isnull(DTCV.Valor_Flete,0) as Valor_Flete,    
 isnull(DTCV.Porcentaje_Seguro,0) as Porcentaje_Seguro,    
 isnull(DTCV.Valor_Escolta,0) as Valor_Escolta,                      
 --isnull(Valor_Kilo_Adicional,0) as Valor_Kilo_Adicional,                      
 isnull(DTCV.Valor_Otros1,0) as Valor_Otros1,                      
 isnull(DTCV.Valor_Otros2,0) as Valor_Otros2,                      
 isnull(DTCV.Valor_Otros3,0) as Valor_Otros3,                      
 isnull(DTCV.Valor_Otros4,0) as Valor_Otros4,          
 isnull(DTCV.Valor_Cargue,0) as Valor_Cargue,             
 isnull(DTCV.Valor_Descargue,0) as Valor_Descargue,             
 isnull(DTCV.Valor_Flete_Transportador,0) as Valor_Flete_Transportador,                         
 isnull(DTCV.Fecha_Vigencia_Inicio,'') as Fecha_Vigencia_Inicio,                      
 isnull(DTCV.Fecha_Vigencia_Fin,'') as Fecha_Vigencia_Fin,                      
 DTCV.Estado,                  
 ISNULL(CIOR.Nombre,'') AS CiudadOrigen,                  
 ISNULL(CIDE.Nombre,'') AS CiudadDestino,                
 ISNULL(FPVE.Campo1,'') AS FormaPago,              
 RUTA.Nombre AS Ruta,        
 Porcentaje_Afiliado,
 DTCV.Tiempo_Entrega
                      
 FROM                       
 Detalle_Tarifario_Carga_Ventas DTCV LEFT JOIN  Ciudades CIOR ON                   
 DTCV.EMPR_Codigo = CIOR.EMPR_Codigo                  
 AND DTCV.CIUD_Codigo_Origen = CIOR.Codigo                  
                  
 LEFT JOIN  Ciudades CIDE ON                  
 DTCV.EMPR_Codigo = CIDE.EMPR_Codigo                  
 AND DTCV.CIUD_Codigo_Destino = CIDE.Codigo                  
                  
 LEFT JOIN  Valor_Catalogos FPVE ON                  
 DTCV.EMPR_Codigo = FPVE.EMPR_Codigo                  
 AND DTCV.CATA_FPVE_Codigo = FPVE.Codigo              
              
                  
 LEFT JOIN  Rutas RUTA ON                  
 DTCV.EMPR_Codigo = RUTA.EMPR_Codigo                  
 AND DTCV.RUTA_Codigo = RUTA.Codigo                    
            
 LEFT JOIN  Tarifa_Transporte_Carga TTCA ON                  
 DTCV.EMPR_Codigo = TTCA.EMPR_Codigo                  
 AND DTCV.TATC_Codigo = TTCA.Codigo       
      
 LEFT JOIN  V_Tipo_Tarifa_Transporte_Carga TTTC ON                    
 DTCV.EMPR_Codigo = TTTC.EMPR_Codigo                        
 AND DTCV.TTTC_Codigo = TTTC.Codigo                         
 AND ISNULL(DTCV.TATC_Codigo,0) = ISNULL(TTTC.TATC_Codigo,0)                 
                      
 WHERE DTCV.EMPR_Codigo = @par_EMPR_Codigo                      
 AND DTCV.ETCV_Numero = ISNULL(@par_ETCV_Numero,DTCV.ETCV_Numero)     
 AND ((ISNUMERIC(@par_LNTC_Codigo) > 0 and DTCV.LNTC_Codigo in (SELECT value FROM STRING_SPLIT(@par_LNTC_Codigo,','))) or (ISNUMERIC(@par_LNTC_Codigo) = 0 and DTCV.LNTC_Codigo = DTCV.LNTC_Codigo))
   
 ORDER BY RUTA.Nombre,CIOR.Nombre,CIDE.Nombre,TTCA.Nombre, TTTC.Nombre, FPVE.Campo1                
                  
END    
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_detalle_tarifario_ventas_cliente]  
(              
 @par_EMPR_Codigo SMALLINT,              
 @par_ETCV_Numero NUMERIC,
 @par_LNTC_Codigo VARCHAR(6) = NULL
)              
AS               
BEGIN              
 SELECT               
 DTCV.EMPR_Codigo,              
 DTCV.Codigo,              
 DTCV.ETCV_Numero,              
 DTCV.LNTC_Codigo,              
 DTCV.TLNC_Codigo,              
 DTCV.TATC_Codigo,              
 DTCV.TTTC_Codigo,              
 DTCV.RUTA_Codigo, 
 DTCV.Reexpedicion,
 ISNULL(DTCV.CATA_FPVE_Codigo,4900) AS CATA_FPVE_Codigo,        
 ISNULL(DTCV.CIUD_Codigo_Origen,0) AS CIUD_Codigo_Origen,          
 ISNULL(DTCV.CIUD_Codigo_Destino,0) AS CIUD_Codigo_Destino,          
 isnull(DTCV.Valor_Flete,0) as Valor_Flete,  
 ISNULL(DTCV.Porcentaje_Seguro,0) as Porcentaje_Seguro,  
 isnull(DTCV.Valor_Escolta,0) as Valor_Escolta,              
 --isnull(Valor_Kilo_Adicional,0) as Valor_Kilo_Adicional,              
 isnull(DTCV.Valor_Otros1,0) as Valor_Otros1,              
 isnull(DTCV.Valor_Otros2,0) as Valor_Otros2,              
 isnull(DTCV.Valor_Otros3,0) as Valor_Otros3,              
 isnull(DTCV.Valor_Otros4,0) as Valor_Otros4,      
 isnull(DTCV.Valor_Cargue,0) as Valor_Cargue,     
 isnull(DTCV.Valor_Descargue,0) as Valor_Descargue,     
 isnull(DTCV.Valor_Flete_Transportador,0) as Valor_Flete_Transportador,             
 isnull(DTCV.Fecha_Vigencia_Inicio,'') as Fecha_Vigencia_Inicio,              
 isnull(DTCV.Fecha_Vigencia_Fin,'') as Fecha_Vigencia_Fin,              
 DTCV.Estado,          
 ISNULL(CIOR.Nombre,'') AS CiudadOrigen,          
 ISNULL(CIDE.Nombre,'') AS CiudadDestino,        
 ISNULL(FPVE.Campo1,'') AS FormaPago,
 DTCV.Tiempo_Entrega
              
 FROM               
 Detalle_Tarifario_Carga_Ventas DTCV LEFT JOIN  Ciudades CIOR ON           
 DTCV.EMPR_Codigo = CIOR.EMPR_Codigo          
 AND DTCV.CIUD_Codigo_Origen = CIOR.Codigo          
          
 LEFT JOIN  Ciudades CIDE ON          
 DTCV.EMPR_Codigo = CIDE.EMPR_Codigo          
 AND DTCV.CIUD_Codigo_Destino = CIDE.Codigo          
          
 LEFT JOIN  Valor_Catalogos FPVE ON          
 DTCV.EMPR_Codigo = FPVE.EMPR_Codigo          
 AND DTCV.CATA_FPVE_Codigo = FPVE.Codigo          
              
 WHERE DTCV.EMPR_Codigo = @par_EMPR_Codigo              
 AND DTCV.ETCV_Numero = ISNULL(@par_ETCV_Numero,DTCV.ETCV_Numero)
 AND ((ISNUMERIC(@par_LNTC_Codigo) > 0 and DTCV.LNTC_Codigo in (SELECT value FROM STRING_SPLIT(@par_LNTC_Codigo,','))) or (ISNUMERIC(@par_LNTC_Codigo) = 0 and DTCV.LNTC_Codigo = DTCV.LNTC_Codigo))
 AND DTCV.Fecha_Vigencia_Fin >= GETDATE()          
END  


GO

CREATE OR ALTER   PROCEDURE [dbo].[gsp_consultar_tarifario_clientes]
	@par_EMPR_Codigo [smallint],
	@par_ETCV_Codigo [numeric](18, 0) = NULL,
	@par_TERC_Codigo [numeric](18, 0) = NULL,
	@par_LNTC_Codigo [numeric](18, 0) = NULL,
	@par_TLNC_Codigo [numeric](18, 0) = NULL,
	@par_CIUD_Codigo_Origen [numeric](18, 0) = NULL,
	@par_CIUD_Codigo_Destino [numeric](18, 0) = NULL,
	@par_CATA_FPVE_Codigo [numeric](18, 0) = NULL
WITH EXECUTE AS CALLER
AS
BEGIN      
 IF @par_LNTC_Codigo = 3 AND @par_TLNC_Codigo = 302      
 BEGIN      
  SET @par_CIUD_Codigo_Destino = 0      
 END      
                  
 IF ISNULL(@par_TERC_Codigo, 0) > 0               
 BEGIN              
  SELECT DTCV.EMPR_Codigo,                         
  DTCV.ETCV_Numero,                        
  DTCV.Codigo As DTCV_Codigo,                        
  DTCV.LNTC_Codigo,                         
  DTCV.TLNC_Codigo,                         
  DTCV.TATC_Codigo,                         
  DTCV.TTTC_Codigo,                        
  ISNULL(TECL.CATA_FPCL_Codigo,0) AS   CATA_FPCL_Codigo,                      
  DTCV.Valor_Flete,                        
  DTCV.Valor_Escolta,                        
  DTCV.Valor_Otros1,                         
  DTCV.Valor_Otros2,                        
  DTCV.Valor_Otros3,                        
  DTCV.Valor_Otros4,                        
  TTTC.Nombre As NombreTipoTarifaCarga,              
  TTTC.NombreTarifa As NombreTarifaCarga,          
  DTCV.CATA_FPVE_Codigo AS CodigoFormaPagoTarifa,          
  TTTC.CATA_Codigo AS Catalogo,              
  TTTC.VACA_Codigo,              
  TTTC.VACA_Campo2,              
  TTTC.VACA_Campo3,              
  ISNULL(TAPV.Valor_Manejo,0) AS Valor_Manejo,                      
  ISNULL(TAPV.Valor_Seguro,0) AS Valor_Seguro,              
  ISNULL(DTCV.Porcentaje_Seguro,0) AS Porcentaje_Seguro,              
  DTCV.Porcentaje_Afiliado,        
  DTCV.Reexpedicion,        
  DTCV.Porcentaje_Afiliado AS Porcentaje_Reexpedicion,        
  DTCV.Valor_Cargue,        
  DTCV.Valor_Descargue,  
  DTCV.CIUD_Codigo_Origen,  
  DTCV.CIUD_Codigo_Destino,
  DTCV.Tiempo_Entrega
                        
  FROM Detalle_Tarifario_Carga_Ventas DTCV                        
                        
  LEFT JOIN                        
  V_Tipo_Tarifa_Transporte_Carga TTTC ON                        
  DTCV.EMPR_Codigo = TTTC.EMPR_Codigo                        
  AND DTCV.TATC_Codigo = TTTC.TATC_Codigo                        
  AND DTCV.TTTC_Codigo = TTTC.Codigo                        
                        
  AND TTTC.EstadoTarifa = 1 --> Tarifa Activa                        
  --AND TTTC.EstadoTipoTarifa = 1 --> Tipo Tarifa Activa                        
                        
  INNER JOIN                        
  Tercero_Clientes TECL ON                        
  DTCV.EMPR_Codigo = TECL.EMPR_Codigo                        
  AND DTCV.ETCV_Numero = ISNULL(@par_ETCV_Codigo,TECL.ETCV_Numero)                        
  AND TECL.TERC_Codigo = @par_TERC_Codigo                        
                        
  LEFT JOIN                       
  Tarifas_Paqueteria_Ventas AS TAPV                      
  ON TAPV.EMPR_Codigo = DTCV.EMPR_Codigo                        
  AND TAPV.ETCV_Numero = DTCV.ETCV_Numero      
                      
  WHERE                         
  DTCV.EMPR_Codigo = @par_EMPR_Codigo
  AND DTCV.ETCV_Numero =  ISNULL(@par_ETCV_Codigo,DTCV.ETCV_Numero)
  AND DTCV.Estado = 1--> Activo                        
  AND DTCV.LNTC_Codigo = ISNULL(@par_LNTC_Codigo,DTCV.LNTC_Codigo )                        
  AND DTCV.CIUD_Codigo_Origen = ISNULL(@par_CIUD_Codigo_Origen ,DTCV.CIUD_Codigo_Origen )                    
  AND DTCV.CIUD_Codigo_Destino = ISNULL(@par_CIUD_Codigo_Destino, DTCV.CIUD_Codigo_Destino)                
  AND (DTCV.CATA_FPVE_Codigo = @par_CATA_FPVE_Codigo  OR  @par_CATA_FPVE_Codigo IS NULL)                    
 END                        
 ELSE                            
 BEGIN               
  SELECT DTCV.EMPR_Codigo,                        
                        
  DTCV.ETCV_Numero,                        
  DTCV.Codigo As DTCV_Codigo,      
  DTCV.LNTC_Codigo,                         
  DTCV.TLNC_Codigo,                         
  DTCV.TATC_Codigo,                         
  DTCV.TTTC_Codigo,                        
  6102 AS CATA_FPCL_Codigo, --> Forma pago contado                        
  DTCV.Valor_Flete,                        
  DTCV.Valor_Escolta,                        
  DTCV.Valor_Otros1,                         
  DTCV.Valor_Otros2,                        
  DTCV.Valor_Otros3,                        
  DTCV.Valor_Otros4,                        
 TTTC.Nombre As NombreTipoTarifaCarga,                        
  TTTC.NombreTarifa As NombreTarifaCarga,          
  DTCV.CATA_FPVE_Codigo AS CodigoFormaPagoTarifa,              
  TTTC.CATA_Codigo AS Catalogo,              
  TTTC.VACA_Codigo,              
  TTTC.VACA_Campo2,              
  TTTC.VACA_Campo3,              
  ISNULL(TAPV.Valor_Manejo,0) AS Valor_Manejo,                      
  ISNULL(TAPV.Valor_Seguro,0) AS Valor_Seguro,              
  ISNULL(DTCV.Porcentaje_Seguro,0) AS Porcentaje_Seguro,              
  DTCV.Porcentaje_Afiliado,        
  DTCV.Reexpedicion,        
  DTCV.Porcentaje_Afiliado AS Porcentaje_Reexpedicion,        
  DTCV.Valor_Cargue,        
  DTCV.Valor_Descargue        ,    
  DTCV.CIUD_Codigo_Origen,  
  DTCV.CIUD_Codigo_Destino,
  DTCV.Tiempo_Entrega
                        
  FROM Detalle_Tarifario_Carga_Ventas DTCV                        
                        
  INNER JOIN                         
  Encabezado_Tarifario_Carga_Ventas ETCV ON                        
  DTCV.EMPR_Codigo = ETCV.EMPR_Codigo                        
  AND DTCV.ETCV_Numero = ETCV.Numero                        
                        
  INNER JOIN                        
  V_Tipo_Tarifa_Transporte_Carga TTTC ON                      
  DTCV.EMPR_Codigo = TTTC.EMPR_Codigo                        
  AND DTCV.TATC_Codigo = TTTC.TATC_Codigo                        
  AND DTCV.TTTC_Codigo = TTTC.Codigo                        
                        
  AND TTTC.EstadoTarifa = 1 --> Tarifa Activa                        
  --AND TTTC.EstadoTipoTarifa = 1 --> Tipo Tarifa Activa                        
                      
  LEFT JOIN                       
  Tarifas_Paqueteria_Ventas AS TAPV                      
  ON TAPV.EMPR_Codigo = DTCV.EMPR_Codigo                        
  AND TAPV.ETCV_Numero = DTCV.ETCV_Numero        
    
                        
  WHERE                         
  DTCV.EMPR_Codigo = @par_EMPR_Codigo                        
  AND DTCV.Estado = 1--> Activo                        
  AND DTCV.LNTC_Codigo = @par_LNTC_Codigo                        
  AND DTCV.CIUD_Codigo_Origen = @par_CIUD_Codigo_Origen                        
  AND DTCV.CIUD_Codigo_Destino = @par_CIUD_Codigo_Destino                        
  AND (DTCV.CATA_FPVE_Codigo = @par_CATA_FPVE_Codigo  OR  @par_CATA_FPVE_Codigo IS NULL)                    
  AND ETCV.Tarifario_Base = 1                           
 END                            
END
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_tarifario_ventas]        
(                        
 @par_EMPR_Codigo SMALLINT,                        
 @par_ETCV_Numero NUMERIC ,  
 @par_LNTC_Codigo NUMERIC = NULL,  
 @par_TLNC_Codigo NUMERIC = NULL,  
 @par_Ruta_Codigo NUMERIC = NULL,  
 @par_CIOR_Codigo VARCHAR(35) = NULL,  
 @par_CIDE_Codigo VARCHAR(35) = NULL,  
 @par_TATC_Codigo NUMERIC = NULL,  
 @par_TTTC_Codigo NUMERIC = NULL,  
 @par_CATA_FPVE_Codigo NUMERIC = NULL   
  
  
)                        
AS                         
BEGIN                        
 SELECT                         
 DTCV.EMPR_Codigo,                        
 DTCV.Codigo,                        
 DTCV.ETCV_Numero,                        
 DTCV.LNTC_Codigo,     
 LNTC.Nombre AS Linea_Negocio_Transporte_Carga,  
 TLNC.Nombre As Tipo_Linea_Negocio_Carga,  
 DTCV.TLNC_Codigo,                    
 TTCA.Nombre AS Tarifa,                  
 DTCV.TATC_Codigo,                        
 DTCV.TTTC_Codigo,        
 ISNULL(TTTC.Nombre, '') TTTC_Nombre,        
 DTCV.RUTA_Codigo,                     
 ISNULL(DTCV.CATA_FPVE_Codigo,4900) AS CATA_FPVE_Codigo,                  
 ISNULL(DTCV.CIUD_Codigo_Origen,0) AS CIUD_Codigo_Origen,       
 CIOR.Nombre As CiudadOrigen,  
 CIDE.Nombre As CiudadDestino,  
 ISNULL(DTCV.CIUD_Codigo_Destino,0) AS CIUD_Codigo_Destino,                    
 isnull(DTCV.Valor_Flete,0) as Valor_Flete,      
 isnull(DTCV.Porcentaje_Seguro,0) as Porcentaje_Seguro,      
 isnull(DTCV.Valor_Escolta,0) as Valor_Escolta,                        
 --isnull(Valor_Kilo_Adicional,0) as Valor_Kilo_Adicional,                        
 isnull(DTCV.Valor_Otros1,0) as Valor_Otros1,                        
 isnull(DTCV.Valor_Otros2,0) as Valor_Otros2,                        
 isnull(DTCV.Valor_Otros3,0) as Valor_Otros3,                        
 isnull(DTCV.Valor_Otros4,0) as Valor_Otros4,            
 isnull(DTCV.Valor_Cargue,0) as Valor_Cargue,               
 isnull(DTCV.Valor_Descargue,0) as Valor_Descargue,               
 isnull(DTCV.Valor_Flete_Transportador,0) as Valor_Flete_Transportador,                           
 isnull(DTCV.Fecha_Vigencia_Inicio,'') as Fecha_Vigencia_Inicio,                        
 isnull(DTCV.Fecha_Vigencia_Fin,'') as Fecha_Vigencia_Fin,                        
 DTCV.Estado,                    
 ISNULL(CIOR.Nombre,'') AS CiudadOrigen,                    
 ISNULL(CIDE.Nombre,'') AS CiudadDestino,                  
 ISNULL(FPVE.Campo1,'') AS FormaPago,                
 RUTA.Nombre AS Ruta,          
 Porcentaje_Afiliado    ,
 DTCV.Reexpedicion,
 DTCV.Tiempo_Entrega
                        
 FROM                         
 Detalle_Tarifario_Carga_Ventas DTCV LEFT JOIN  Ciudades CIOR ON                     
 DTCV.EMPR_Codigo = CIOR.EMPR_Codigo                    
 AND DTCV.CIUD_Codigo_Origen = CIOR.Codigo          
   
 LEFT JOIN Linea_Negocio_Transporte_Carga LNTC ON  
 DTCV.EMPR_Codigo = LNTC.EMPR_Codigo AND  
 DTCV.LNTC_Codigo = LNTC.Codigo  
  
  LEFT JOIN Tipo_Linea_Negocio_Carga TLNC ON  
 DTCV.EMPR_Codigo = TLNC.EMPR_Codigo AND  
 DTCV.TLNC_Codigo = TLNC.Codigo  
                    
 LEFT JOIN  Ciudades CIDE ON                    
 DTCV.EMPR_Codigo = CIDE.EMPR_Codigo                    
 AND DTCV.CIUD_Codigo_Destino = CIDE.Codigo                    
                    
 LEFT JOIN  Valor_Catalogos FPVE ON                    
 DTCV.EMPR_Codigo = FPVE.EMPR_Codigo                    
 AND DTCV.CATA_FPVE_Codigo = FPVE.Codigo                
                
                    
 LEFT JOIN  Rutas RUTA ON                    
 DTCV.EMPR_Codigo = RUTA.EMPR_Codigo                    
 AND DTCV.RUTA_Codigo = RUTA.Codigo                      
              
 LEFT JOIN  Tarifa_Transporte_Carga TTCA ON                    
 DTCV.EMPR_Codigo = TTCA.EMPR_Codigo                    
 AND DTCV.TATC_Codigo = TTCA.Codigo         
        
 LEFT JOIN  V_Tipo_Tarifa_Transporte_Carga TTTC ON                      
 DTCV.EMPR_Codigo = TTTC.EMPR_Codigo                          
 AND DTCV.TTTC_Codigo = TTTC.Codigo                           
 AND ISNULL(DTCV.TATC_Codigo,0) = ISNULL(TTTC.TATC_Codigo,0)                   
                        
 WHERE DTCV.EMPR_Codigo = @par_EMPR_Codigo                        
 AND DTCV.ETCV_Numero = ISNULL(@par_ETCV_Numero,DTCV.ETCV_Numero)   
 AND DTCV.LNTC_Codigo = ISNULL(@par_LNTC_Codigo,DTCV.LNTC_Codigo)  
 AND DTCV.TLNC_Codigo = ISNULL(@par_TLNC_Codigo,DTCV.TLNC_Codigo)  
 AND DTCV.RUTA_Codigo = ISNULL(@par_Ruta_Codigo, DTCV.RUTA_Codigo)  
 AND ((ISNUMERIC(@par_CIOR_Codigo) > 0 and DTCV.CIUD_Codigo_Origen in (SELECT value FROM STRING_SPLIT(@par_CIOR_Codigo,','))) or (ISNUMERIC(@par_CIOR_Codigo) = 0 and DTCV.CIUD_Codigo_Origen = DTCV.CIUD_Codigo_Origen))
 AND ((ISNUMERIC(@par_CIDE_Codigo) > 0 and DTCV.CIUD_Codigo_Destino in (SELECT value FROM STRING_SPLIT(@par_CIDE_Codigo,','))) or (ISNUMERIC(@par_CIDE_Codigo) = 0 and DTCV.CIUD_Codigo_Destino = DTCV.CIUD_Codigo_Destino))
 AND DTCV.TATC_Codigo = ISNULL(@par_TATC_Codigo,DTCV.TATC_Codigo)  
 AND DTCV.TTTC_Codigo = ISNULL(@par_TTTC_Codigo,DTCV.TTTC_Codigo)  
 AND DTCV.CATA_FPVE_Codigo = ISNULL(@par_CATA_FPVE_Codigo,DTCV.CATA_FPVE_Codigo)  
     
 ORDER BY RUTA.Nombre,CIOR.Nombre,CIDE.Nombre,TTCA.Nombre, TTTC.Nombre, FPVE.Campo1                  
                    
END      
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_insertar_detalle_tarifario_ventas]  
(                                  
 @par_EMPR_Codigo smallint                                  
 ,@par_Codigo numeric(18,0) = NULL                              
 ,@par_ETCV_Numero numeric(18,0)                                  
 ,@par_LNTC_Codigo smallint                                  
 ,@par_TLNC_Codigo smallint                                  
 ,@par_RUTA_Codigo numeric(18,0) = NULL                              
 ,@par_CIUD_Codigo_Origen numeric(18,0) = NULL                              
 ,@par_CIUD_Codigo_Destino numeric(18,0) = NULL                                 
 ,@par_TATC_Codigo smallint    = NULL                                 
 ,@par_TTTC_Codigo smallint                                  
 ,@par_CATA_FPVE_Codigo NUMERIC  = NULL                        
 ,@par_Valor_Flete money                                  
 ,@par_Valor_Escolta money = null                                     
 ,@par_Valor_Otros1 money = null                                  
 ,@par_Valor_Otros2 money = null                                  
 ,@par_Valor_Otros3 money = null                                  
 ,@par_Valor_Otros4 money = null                  
 ,@par_Valor_Cargue money = null               
 ,@par_Valor_Descargue money = null               
 ,@par_Valor_Flete_Transportador money = null                               
 ,@par_Fecha_Vigencia_Inicio date = null                                  
 ,@par_Fecha_Vigencia_Fin date = null                                  
 ,@par_Estado smallint                                  
 ,@par_USUA_Codigo_Crea smallint                                  
 ,@par_Porcentaje_Afiliado numeric(18,2) = null  
 ,@par_Porcentaje_Seguro money = null  
 ,@par_Reexpedicion smallint = null
 ,@par_Tiempo_Entrega smallint = 1
)                                  
AS   
BEGIN                                  
                                  
 IF @par_Codigo > 0  AND EXISTS(SELECT * FROM Detalle_Tarifario_Carga_Ventas WHERE Codigo = @par_Codigo)              
 BEGIN  
  UPDATE Detalle_Tarifario_Carga_Ventas                              
  SET                                 
  Valor_Flete = ISNULL(@par_Valor_Flete,0),  
  Porcentaje_Seguro = ISNULL(@par_Porcentaje_Seguro,0),  
  Valor_Escolta = ISNULL(@par_Valor_Escolta ,0),                              
  Valor_Otros1 =  ISNULL(@par_Valor_Otros1,0),                              
  Valor_Otros2 = ISNULL(@par_Valor_Otros2, 0),                                 
  Valor_Otros3 = ISNULL(@par_Valor_Otros3,0),                                  
  Valor_Otros4 = ISNULL(@par_Valor_Otros4,0),                 
  Valor_Cargue = ISNULL(@par_Valor_Cargue,0),                 
  Valor_Descargue = ISNULL(@par_Valor_Descargue,0),                 
  Valor_Flete_Transportador = ISNULL(@par_Valor_Flete_Transportador,0),                          
  CATA_FPVE_Codigo = ISNULL(@par_CATA_FPVE_Codigo,4900), --Si no se envia la forma de pago por defecto se deja no aplica                         
  Fecha_Vigencia_Inicio = ISNULL(@par_Fecha_Vigencia_Inicio,''),                                   
  Fecha_Vigencia_Fin =  ISNULL(@par_Fecha_Vigencia_Fin,''),                                 
  Estado =  @par_Estado,                                
  USUA_Codigo_Modifica = @par_USUA_Codigo_Crea,                              
  Porcentaje_Afiliado = @par_Porcentaje_Afiliado,  
  Reexpedicion = @par_Reexpedicion,
  Tiempo_Entrega = @par_Tiempo_Entrega,
  Fecha_Modifica = GETDATE()                              
      
  where EMPR_Codigo =  @par_EMPR_Codigo                  
  and  ETCV_Numero = @par_ETCV_Numero                  
  and Codigo = @par_Codigo                   
  
  SELECT @par_Codigo AS Codigo                                             
 END                              
 ELSE                               
 BEGIN  
  IF exists                  
  (                  
  select * from Detalle_Tarifario_Carga_Ventas                  
  where EMPR_Codigo =  @par_EMPR_Codigo                  
  and  ETCV_Numero = @par_ETCV_Numero                  
  and LNTC_Codigo = @par_LNTC_Codigo                   
  and TLNC_Codigo = @par_TLNC_Codigo                   
  and TATC_Codigo = ISNULL(@par_TATC_Codigo,0)                
  and TTTC_Codigo = @par_TTTC_Codigo                   
  and RUTA_Codigo = @par_RUTA_Codigo                  
  and CIUD_Codigo_Origen = ISNULL(@par_CIUD_Codigo_Origen,0)                  
  and CIUD_Codigo_Destino = ISNULL(@par_CIUD_Codigo_Destino,0)                  
  and CATA_FPVE_Codigo = ISNULL(@par_CATA_FPVE_Codigo,4900)                      
  )                  
  BEGIN                              
   UPDATE Detalle_Tarifario_Carga_Ventas  
   SET                                 
   Valor_Flete = ISNULL(@par_Valor_Flete,0),  
   Porcentaje_Seguro = ISNULL(@par_Porcentaje_Seguro,0),  
   Valor_Escolta = ISNULL(@par_Valor_Escolta ,0),                              
   Valor_Otros1 =  ISNULL(@par_Valor_Otros1,0),                              
   Valor_Otros2 = ISNULL(@par_Valor_Otros2, 0),                                 
   Valor_Otros3 = ISNULL(@par_Valor_Otros3,0),                                  
   Valor_Otros4 = ISNULL(@par_Valor_Otros4,0),                 
   Valor_Cargue = ISNULL(@par_Valor_Cargue,0),                 
   Valor_Descargue = ISNULL(@par_Valor_Descargue,0),                 
   Valor_Flete_Transportador = ISNULL(@par_Valor_Flete_Transportador,0),                          
   CATA_FPVE_Codigo = ISNULL(@par_CATA_FPVE_Codigo,4900), --Si no se envia la forma de pago por defecto se deja no aplica                         
   Fecha_Vigencia_Inicio = ISNULL(@par_Fecha_Vigencia_Inicio,''),                                   
   Fecha_Vigencia_Fin =  ISNULL(@par_Fecha_Vigencia_Fin,''),                                 
   Estado =  @par_Estado,                                
   USUA_Codigo_Modifica = @par_USUA_Codigo_Crea,                              
   Porcentaje_Afiliado = @par_Porcentaje_Afiliado, 
   Reexpedicion = @par_Reexpedicion,
   Tiempo_Entrega = @par_Tiempo_Entrega,
   Fecha_Modifica = GETDATE()                              
  
   where EMPR_Codigo =  @par_EMPR_Codigo                  
   and  ETCV_Numero = @par_ETCV_Numero                  
   and LNTC_Codigo = @par_LNTC_Codigo                   
   and TLNC_Codigo = @par_TLNC_Codigo                   
   and TATC_Codigo =ISNULL(@par_TATC_Codigo,0)                   
   and TTTC_Codigo = @par_TTTC_Codigo                   
   and RUTA_Codigo = @par_RUTA_Codigo                  
   and CIUD_Codigo_Origen = ISNULL(@par_CIUD_Codigo_Origen,0)                  
   and CIUD_Codigo_Destino = ISNULL(@par_CIUD_Codigo_Destino,0)                  
   and CATA_FPVE_Codigo = ISNULL(@par_CATA_FPVE_Codigo,4900)                   
   SELECT @@ROWCOUNT AS Codigo                                 
  END                              
  ELSE            
   BEGIN            
   INSERT INTO Detalle_Tarifario_Carga_Ventas  
   (                                  
   EMPR_Codigo                                  
   ,ETCV_Numero                                  
   --,Codigo                                  
   ,LNTC_Codigo                                  
   ,TLNC_Codigo                                  
   ,RUTA_Codigo                                 
   ,CIUD_Codigo_Origen                              
   ,CIUD_Codigo_Destino                              
   ,TATC_Codigo                                  
   ,TTTC_Codigo                                  
   ,Valor_Flete                                  
   ,Valor_Escolta                         
   ,CATA_FPVE_Codigo                                 
   --,Valor_Kilo_Adicional                                  
   ,Valor_Otros1                                  
   ,Valor_Otros2                                  
   ,Valor_Otros3                                  
   ,Valor_Otros4               
   ,Valor_Cargue              
   ,Valor_Descargue              
   ,Valor_Flete_Transportador                                 
   ,Fecha_Vigencia_Inicio                                  
   ,Fecha_Vigencia_Fin                                  
   ,Estado                                  
   ,USUA_Codigo_Crea                                  
   ,Fecha_Crea                  
   ,Porcentaje_Afiliado  
   ,Porcentaje_Seguro  
   ,Reexpedicion
   ,Tiempo_Entrega
   )                                  
   VALUES                              
   (@par_EMPR_Codigo                                  
   ,@par_ETCV_Numero                                  
   --,@Codigo                                  
   ,@par_LNTC_Codigo                                  
   ,@par_TLNC_Codigo                                  
   ,@par_RUTA_Codigo                               
   ,ISNULL(@par_CIUD_Codigo_Origen,0)                            
   ,ISNULL(@par_CIUD_Codigo_Destino,0)                            
   ,ISNULL(@par_TATC_Codigo,0)                                  
   ,@par_TTTC_Codigo                                  
   ,@par_Valor_Flete                                  
   ,isnull(@par_Valor_Escolta,0)                           
   ,ISNULL(@par_CATA_FPVE_Codigo,4900)                               
   --,isnull(@par_Valor_Kilo_Adicional,0)                                  
   ,isnull(@par_Valor_Otros1,0)                                  
   ,isnull(@par_Valor_Otros2,0)                                  
   ,isnull(@par_Valor_Otros3,0)                                  
   ,isnull(@par_Valor_Otros4,0)               
   ,isnull(@par_Valor_Cargue,0)              
   ,isnull(@par_Valor_Descargue,0)              
   ,isnull(@par_Valor_Flete_Transportador,0)                                 
   ,isnull(@par_Fecha_Vigencia_Inicio,'')                                  
   ,isnull(@par_Fecha_Vigencia_Fin,'')                                  
   ,@par_Estado                                  
   ,@par_USUA_Codigo_Crea                                  
   ,GETDATE()                                  
   ,@par_Porcentaje_Afiliado   
   ,isnull(@par_Porcentaje_Seguro, 0)  
   ,@par_Reexpedicion
   ,@par_Tiempo_Entrega
   )                                  
   SELECT @@IDENTITY AS Codigo                                  
  END                              
 END                              
END  
GO


