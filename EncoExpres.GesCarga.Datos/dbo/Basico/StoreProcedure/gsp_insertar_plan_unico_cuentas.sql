﻿PRINT 'gsp_insertar_plan_unico_cuentas'
GO
DROP PROCEDURE gsp_insertar_plan_unico_cuentas
GO
CREATE PROCEDURE gsp_insertar_plan_unico_cuentas
(
@par_EMPR_Codigo SMALLINT,
@par_CATA_CCPU_Codigo NUMERIC,
@par_Codigo_Cuenta VARCHAR(50),
@par_Codigo_Alterno VARCHAR(20),
@par_Nombre VARCHAR(100),
@par_Exige_Centro_Costo SMALLINT,
@par_Exige_Tercero SMALLINT,
@par_Exige_Valor_Base SMALLINT,
@par_Valor_Base MONEY,
@par_Exige_Documento_Cruce SMALLINT,
@par_Estado SMALLINT,
@par_USUA_Codigo_Crea NUMERIC
)
AS
BEGIN

INSERT INTO  Plan_Unico_Cuentas
(
EMPR_Codigo,
CATA_CCPU_Codigo,
Codigo_Cuenta,
Codigo_Alterno,
Nombre,
Exige_Centro_Costo,
Exige_Tercero,
Exige_Valor_Base,
Valor_Base,
Exige_Documento_Cruce,
Estado,
USUA_Codigo_Crea,
Fecha_Crea
)
VALUES
(
@par_EMPR_Codigo,
@par_CATA_CCPU_Codigo,
@par_Codigo_Cuenta,
@par_Codigo_Alterno,
@par_Nombre,
@par_Exige_Centro_Costo,
@par_Exige_Tercero,
@par_Exige_Valor_Base,
@par_Valor_Base,
@par_Exige_Documento_Cruce,
@par_Estado,
@par_USUA_Codigo_Crea,
GETDATE()
)
SELECT Codigo = @@identity

END
GO