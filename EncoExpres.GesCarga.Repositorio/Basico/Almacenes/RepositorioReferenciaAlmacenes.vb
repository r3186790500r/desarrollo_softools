﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.Almacen
Namespace Basico.Almacen

    ''' <summary>
    ''' Clase <see cref="RepositorioReferenciaAlmacenes"/>
    ''' </summary>
    Public NotInheritable Class RepositorioReferenciaAlmacenes
        Inherits RepositorioBase(Of ReferenciaAlmacenes)

        Public Overrides Function Consultar(filtro As ReferenciaAlmacenes) As IEnumerable(Of ReferenciaAlmacenes)
            Dim lista As New List(Of ReferenciaAlmacenes)
            Dim item As ReferenciaAlmacenes

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                If Not String.IsNullOrWhiteSpace(filtro.CodigoAlterno) Then
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", filtro.CodigoAlterno)
                End If
                If Not String.IsNullOrWhiteSpace(filtro.CodigoReferencia) Then
                    conexion.AgregarParametroSQL("@par_Referencia", filtro.CodigoReferencia)
                End If
                If Not String.IsNullOrWhiteSpace(filtro.NombreAlmacen) Then
                    conexion.AgregarParametroSQL("@par_Almacen", filtro.NombreAlmacen)
                End If
                If Not String.IsNullOrWhiteSpace(filtro.NombreReferencia) Then
                    conexion.AgregarParametroSQL("@par_Nombre_Referencia", filtro.NombreReferencia)
                End If

                If filtro.Estado >= 0 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_almacen_referencia_almacenes]")

                While resultado.Read
                    item = New ReferenciaAlmacenes(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As ReferenciaAlmacenes) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                conexion.AgregarParametroSQL("@par_ALMA_Codigo", entidad.Almacen.Codigo)
                conexion.AgregarParametroSQL("@par_REAL_Codigo", entidad.Referencia.Codigo)
                conexion.AgregarParametroSQL("@par_Existencia_Minima", entidad.ExistenciaMinima)
                conexion.AgregarParametroSQL("@par_Existencia_Maxima", entidad.ExistenciaMaxima)
                conexion.AgregarParametroSQL("@par_Existencia_Actual", entidad.ExistenciaActual)
                conexion.AgregarParametroSQL("@par_Ultimo_Precio", entidad.UltimoPrecio)
                conexion.AgregarParametroSQL("@par_Valor_Promedio", entidad.ValorPromedio)
                conexion.AgregarParametroSQL("@par_Ubicacion_Almacen", entidad.UbicacionAlmacen)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_almacen_referencia_almacenes")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As ReferenciaAlmacenes) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                conexion.AgregarParametroSQL("@par_ALMA_Codigo", entidad.Almacen.Codigo)
                conexion.AgregarParametroSQL("@par_REAL_Codigo", entidad.Referencia.Codigo)
                conexion.AgregarParametroSQL("@par_Existencia_Minima", entidad.ExistenciaMinima)
                conexion.AgregarParametroSQL("@par_Existencia_Maxima", entidad.ExistenciaMaxima)
                conexion.AgregarParametroSQL("@par_Existencia_Actual", entidad.ExistenciaActual)
                conexion.AgregarParametroSQL("@par_Ultimo_Precio", entidad.UltimoPrecio)
                conexion.AgregarParametroSQL("@par_Valor_Promedio", entidad.ValorPromedio)
                conexion.AgregarParametroSQL("@par_Ubicacion_Almacen", entidad.UbicacionAlmacen)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_almacen_referencia_almacenes")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As ReferenciaAlmacenes) As ReferenciaAlmacenes
            Dim item As New ReferenciaAlmacenes

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                If Not IsNothing(filtro.Almacen) Then
                    If filtro.Almacen.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Almacen", filtro.Almacen.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Referencia) Then
                    If filtro.Referencia.Codigo > 0 Then
                        conexion.AgregarParametroSQL("par_Referencia", filtro.Referencia.Codigo)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_almacen_referencia_almacenes]")

                While resultado.Read
                    item = New ReferenciaAlmacenes(resultado)
                End While

            End Using

            Return item
        End Function

        Public Function Anular(entidad As ReferenciaAlmacenes) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_almacen_referencia_almacenes]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Codigo")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function
    End Class

End Namespace