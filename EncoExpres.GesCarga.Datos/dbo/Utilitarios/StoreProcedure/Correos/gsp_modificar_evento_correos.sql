﻿PRINT 'gsp_modificar_evento_correos'
GO
DROP PROCEDURE gsp_modificar_evento_correos 
GO
CREATE PROCEDURE gsp_modificar_evento_correos 
(
	@par_EMPR_Codigo smallint,
	@par_Codigo numeric,
	@par_Estado numeric,
	@par_USUA_Codigo_Modifica numeric
)
AS
BEGIN

	Update Evento_Correos SET
	Estado = @par_Estado,
	USUA_Codigo_Modifica = @par_USUA_Codigo_Modifica,
	Fecha_Modifica = GETDATE()

	WHERE EMPR_Codigo = @par_EMPR_Codigo
	AND Codigo = @par_Codigo
	
	SELECT Codigo FROM Evento_Correos
	WHERE EMPR_Codigo = @par_EMPR_Codigo      
	AND Codigo = @par_Codigo
END 
GO