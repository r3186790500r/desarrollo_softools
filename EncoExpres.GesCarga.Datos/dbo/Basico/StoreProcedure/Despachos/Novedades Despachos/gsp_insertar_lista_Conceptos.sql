﻿PRINT 'gsp_insertar_lista_Conceptos'
GO
DROP PROCEDURE gsp_insertar_lista_Conceptos
GO
CREATE PROCEDURE gsp_insertar_lista_Conceptos 
(@par_EMPR_Codigo SMALLINT,        
@par_NODE_Codigo NUMERIC,        
@par_COVE_Codigo NUMERIC ,        
@par_CLPD_Codigo NUMERIC      
)        
AS       
BEGIN        
        
INSERT INTO [dbo].[Novedades_Conceptos]        
           ([EMPR_Codigo]        
           ,[NODE_Codigo]    
           ,[COVE_Codigo]    
           ,[CLPD_Codigo]        
         )        
     VALUES        
           (@par_EMPR_Codigo        
           ,@par_NODE_Codigo        
           ,@par_COVE_Codigo      
           ,@par_CLPD_Codigo)        
END         
GO