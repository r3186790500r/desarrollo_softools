﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 30/09/2018
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	
PRINT 'gsp_insertar_novedades_planilla_despachos'
GO
DROP PROCEDURE gsp_insertar_novedades_planilla_despachos
GO 
CREATE PROCEDURE gsp_insertar_novedades_planilla_despachos 
(  
@par_EMPR_Codigo SMALLINT   
,@par_ENPD_Numero NUMERIC = NULL  
,@par_ENRE_Numero NUMERIC = NULL 
,@par_NODE_Codigo NUMERIC = NULL  
,@par_Observaciones VARCHAR(500) = NULL  
,@par_Valor_Compra MONEY = NULL  
,@par_Valor_Venta MONEY = NULL   
,@par_TERC_Codigo_Proveedor NUMERIC = NULL
,@par_USUA_Codigo_Crea SMALLINT   
)  
AS  
BEGIN  
  
INSERT INTO Detalle_Novedades_Despacho  
           (EMPR_Codigo,
            ENPD_Numero,
            ENRE_Numero,
            NODE_Codigo,
            Observaciones,
            Valor_Compra,
            TERC_Codigo_Proveedor,
            Valor_Venta,
            Fecha_Crea,
            USUA_Codigo_Crea,
            Anulado,
			Aprobado
           )  
     VALUES  
           (@par_EMPR_Codigo   
           ,@par_ENPD_Numero   
           ,ISNULL(@par_ENRE_Numero , 0)  
           ,@par_NODE_Codigo
           ,@par_Observaciones 
           ,ISNULL(@par_Valor_Compra, 0)  
		   ,ISNULL(@par_TERC_Codigo_Proveedor, 0)  		   
           ,ISNULL(@par_Valor_Venta ,0)             
           ,GETDATE()   
           ,@par_USUA_Codigo_Crea   
		   ,0
		   ,0
   )  
  
SELECT @@IDENTITY AS Codigo  

END  
GO