﻿Imports System.Transactions
Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Fidelizacion.Documentos

Namespace Fidelizacion.Documentos
    Public NotInheritable Class RepositorioDetalleCausalesPuntosVehiculos
        Inherits RepositorioBase(Of DetalleCausalesPuntosVehiculos)

        Public Overrides Function Consultar(filtro As DetalleCausalesPuntosVehiculos) As IEnumerable(Of DetalleCausalesPuntosVehiculos)
            Dim lista As New List(Of DetalleCausalesPuntosVehiculos)
            Dim item As DetalleCausalesPuntosVehiculos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > Cero Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                If Not IsNothing(filtro.Vehiculo) Then
                    If filtro.Vehiculo.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_Vehi_Codigo", filtro.Vehiculo.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Tenedor) Then
                    If filtro.Tenedor.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_TENE_Codigo", filtro.Tenedor.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Conductor) Then
                    If filtro.Conductor.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_COND_Codigo", filtro.Conductor.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.CATA_CPPV) Then
                    If filtro.CATA_CPPV.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_Cata_CPPV", filtro.CATA_CPPV.Codigo)
                    End If
                End If

                If filtro.FechaInicioVigenciaDesde <> Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicio_Vigencia_Desde", filtro.FechaInicioVigenciaDesde, SqlDbType.Date)
                End If

                If filtro.FechaInicioVigenciaHasta <> Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicio_Vigencia_Hasta", filtro.FechaInicioVigenciaHasta, SqlDbType.Date)
                End If

                If filtro.FechaFinVigenciaDesde <> Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Fin_Vigencia_Desde", filtro.FechaFinVigenciaDesde, SqlDbType.Date)
                End If

                If filtro.FechaFinVigenciaHasta <> Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Fin_Vigencia_Hasta", filtro.FechaFinVigenciaHasta, SqlDbType.Date)
                End If

                If (filtro.Estado = Cero Or filtro.Estado = 1) And filtro.Estado <> 2 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                    conexion.AgregarParametroSQL("@par_Anulado", NO_APLICA)

                ElseIf filtro.Estado = 2 Then
                    conexion.AgregarParametroSQL("@par_Anulado", SI_APLICA)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > Cero Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If

                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > Cero Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_detalle_causales_fidelizacion_puntos_vehiculos")

                While resultado.Read
                    item = New DetalleCausalesPuntosVehiculos(resultado)
                    lista.Add(item)
                End While

            End Using
            Return lista
        End Function

        Public Overrides Function Insertar(entidad As DetalleCausalesPuntosVehiculos) As Long
            Dim inserto As Boolean = False
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.CleanParameters()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Vehi_Codigo", entidad.Vehiculo.Codigo)
                    conexion.AgregarParametroSQL("@par_Cata_CPPV", entidad.CATA_CPPV.Codigo)
                    conexion.AgregarParametroSQL("@par_Puntos", entidad.Puntos)
                    conexion.AgregarParametroSQL("@par_Fecha_Inicio_Vigencia", entidad.FechaInicioVigencia, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_Fecha_Fin_Vigencia", entidad.FechaFinVigencia, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_Anulado", entidad.Anulado)

                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_causal_fidelizacion_plan_puntos_vehiculos")

                    While resultado.Read
                        entidad.Codigo = resultado.Item("Codigo").ToString()
                        inserto = True
                    End While
                    resultado.Close()

                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As DetalleCausalesPuntosVehiculos) As Long
            Dim Modifico As Boolean = False
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                    conexion.AgregarParametroSQL("@par_Vehi_Codigo", entidad.Vehiculo.Codigo)
                    conexion.AgregarParametroSQL("@par_Cata_CPPV", entidad.CATA_CPPV.Codigo)
                    conexion.AgregarParametroSQL("@par_Puntos", entidad.Puntos)
                    conexion.AgregarParametroSQL("@par_Fecha_Inicio_Vigencia", entidad.FechaInicioVigencia, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_Fecha_Fin_Vigencia", entidad.FechaFinVigencia, SqlDbType.Date)

                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modifica_Detalle_Causales_Fidelizacion_Plan_Puntos_Vehiculos")

                    While resultado.Read
                        entidad.Codigo = resultado.Item("Codigo").ToString()
                        Modifico = True
                    End While
                    resultado.Close()
                End Using

                If Modifico Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As DetalleCausalesPuntosVehiculos) As DetalleCausalesPuntosVehiculos
            Dim item As New DetalleCausalesPuntosVehiculos
            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_obtener_detalle_causales_fidelizacion_puntos_vehiculos")

                While resultado.Read
                    item = New DetalleCausalesPuntosVehiculos(resultado)
                End While

            End Using
            Return item
        End Function

        Public Function Anular(entidad As DetalleCausalesPuntosVehiculos) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Vehiculo", entidad.Vehiculo.Codigo)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Anula", entidad.UsuarioAnula.Codigo)
                conexion.AgregarParametroSQL("@par_Causa_Anulacion", entidad.CausaAnulacion)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_anular_detalle_causales_fidelizacion_puntos_vehiculos]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Codigo")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function
    End Class
End Namespace

