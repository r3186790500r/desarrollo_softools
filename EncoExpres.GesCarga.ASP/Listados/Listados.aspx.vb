﻿Imports System.Data.SqlClient
Imports Microsoft.Reporting.WebForms

Public Class Listados
    Inherits System.Web.UI.Page

#Region "Atributos"
    Dim objGeneral As clsGeneral
    Dim repDocumento As ReportViewer
#End Region

#Region "Constantes"

    '---Basico:

    Const NOMBRE_LISTADO_EXCEL_NOVEDADES_TERCERO As String = "lstNovedadesTercero"
    Const NOMBRE_LISTADO_EXCEL_NOVEDADES_VEHICULO As String = "lstNovedadesVehiculo"
    Const NOMBRE_LISTADO_EXCEL_NOVEDADES_SEMIRREMOLQUE As String = "lstNovedadesSemirremolque"



    Const NOMBRE_LISTADO_SEGUIMIENTO_DESPACHOS As String = "lstSeguimientoDespachos"
    Const NOMBRE_LISTADO_SEGUIMIENTO_ORDENES_CARGUE As String = "lstSeguimientoOrdenesCargue"
    Const NOMBRE_LISTADO_SEGUIMIENTO_FLOTA_PROPIA As String = "lstSeguimientoFlotaPropia"
    Const NOMBRE_LISTADO_CONTROL_CONTENEDORES As String = "lstControlContenedores"

    '---Despachos:

    Const NOMBRE_LISTADO_INDICADORES_DESPACHOS As String = "lstIndicadoresDespachos"
    Const NOMBRE_LISTADO_EXCEL_PLANILLAS_DESPACHOS_OTRAS_EMPRESAS = "lstExcelPlanillasDespachosOtrasEmpresas"
    Const NOMBRE_LISTADO_SABANA_DATOS = "lstSabanaDatos"

    '---Paqueteria
    Const NOMBRE_LISTADO_GUIA_CLIENTE As String = "lstGuiasCliente"
    Const NOMBRE_LISTADO_GUIA_OFICINA As String = "lstGuiasOficina"
    Const NOMBRE_LISTADO_INVENTARIO_REMESAS_OFICINA = "lstInventarioRemesasOficina"
    Const NOMBRE_LISTADO_INVENTARIO_REMESAS_CLIENTE = "lstInventarioRemesasCliente"
    Const NOMBRE_LISTADO_INVENTARIO_REMESAS_OFICINA_POR_DESTINO = "lstInventarioRemesasOficinaporDestino"


    'Paquteria V2
    Const NOMBRE_LISTADO_RECOLECCIONES_POR_OFICINA_CREA = "lstRecoleccionesxOficinaCrea"
    Const NOMBRE_LISTADO_RECOLECCIONES_POR_OFICINA_GESTIONA = "lstRecoleccionesxOficinaGestiona"
    Const NOMBRE_LISTADO_ZONIFICACION_GUIA = "lstZonificacionGuia"

    'Comercial-Listados
    Const NOMBRE_LISTADO_COMERCIAL_TARIFARIO_VENTA = "lstTarifarioVentas"


    '-Planillas
    Const NOMBRE_LISTADO_PLANILLAS_RECOGIDA As String = "lstPlanillasRecogidas"
    Const NOMBRE_LITADO_PLANILLAS_DESPACHOS_PAQUETERIA As String = "lstPlanillasDespachosPaqueteria"
    Const NOMBRE_LITADO_PLANILLAS_DESPACHOS_PAQUETERIA_COOTR As String = "lstPlanillasDespachosPaqueteria_COOTR"
    Const NOMBRE_LITADO_PLANILLAS_DESPACHOS_PAQUETERIA_TENEDOR = "lstPlanillasDespachosPaqueteriaTenedor"
    Const NOMBRE_LITADO_PLANILLAS_ENTREGAS As String = "lstPlanillasEntregas"
    '-Remesas
    Const NOMBRE_LISTADO_REMESAS_PAQUETERIA As String = "lstRemesas"
    Const NOMBRE_LISTADO_COMISIONES_POR_OFICINA As String = "lstComisionesOficina"
    Const NOMBRE_LISTADO_REMESAS_PENDIENTE_CUMPLIR_PAQUETERIA As String = "lstRemesasPorCumplir"
    Const NOMBRE_LISTADO_REMESAS_PENDIENTE_FACTURAR_PAQUETERIA As String = "lstRemesasPorFacturar"
    Const NOMBRE_LISTADO_NOVEDADES_REMESAS_PAQUETERIA As String = "lstNovedadesRemesasPaqueteria"
    Const NOMBRE_LISTADO_VENTAS_OFICINA_PAQUETERIA As String = "lstVentasPorOficinaPaqueteria"
    Const NOMBRE_LISTADO_PUNTOS_SIN_ENTREGAR As String = "lstPuntosSinEntregar"
    Const NOMBRE_LISTADO_INFORME_REEXPEDICION As String = "lstInformeReexpedicion"
    Const NOMBRE_LISTADO_INFORME_REEXPEDICION_OFICINA = "lstInformeReexpedicionOficina"

    '--Paqueteria

    Const NOMBRE_LISTADO_TERCERO As String = "lstTercero"
    Const NOMBRE_LISTADO_VEHICULO As String = "lstVehiculo"
    Const NOMBRE_LISTADO_SEMIRREMOLQUE As String = "lstSemirremolque"
    Const NOMBRE_LISTADO_RUTA As String = "lstRuta"
    Const NOMBRE_LISTADO_SITIOS_CARGUE_DESCARGUE As String = "lstSitioCargueDescargue"

    Const NOMBRE_LITADO_PRODUCTOS_TRANSPORTADOS As String = "lstProductosTransportados"
    Const NOMBRE_LITADO_COLORES_VEHICULOS As String = "lstColores"
    Const NOMBRE_LITADO_MARCAS_VEHICULOS As String = "lstMarcas"
    Const NOMBRE_LITADO_LINEAS_VEHICULOS As String = "lstLineasVehiculos"
    Const NOMBRE_LITADO_MARCAS_SEMIRREMOLQUES As String = "lstMarcasSemi"

    Const NOMBRE_LITADO_PUESTOS_CONTROL As String = "lstPuestosControl"
    Const NOMBRE_LISTADO_ORDEN_CARGUE_CLIENTE As String = "lstOrdenCargueCliente"
    Const NOMBRE_LISTADO_ORDEN_CARGUE_OFICINA As String = "lstOrdenCargueOficina"
    Const NOMBRE_LISTADO_REMESA_CLIENTE As String = "lstRemesaCliente"
    Const NOMBRE_LISTADO_REMESA_OFICINA As String = "lstRemesaOficina"

    Const NOMBRE_LISTADO_PLANILLA_DESPACHO_TRANSPORTADOR As String = "lstPlanillaDespachoTransportador"
    Const NOMBRE_LISTADO_PLANILLA_DESPACHO_OFICINA As String = "lstPlanillaDespachoOficina"
    Const NOMBRE_LISTADO_MANIFIESTO_TRANSPORTADOR As String = "lstManifiestoTransportador"
    Const NOMBRE_LISTADO_MANIFIESTO_OFICINA As String = "lstManifiestoOficina"
    Const NOMBRE_LISTADO_CUMPLIDOS_TRANSPORTADOR As String = "lstCumplidosTransportador"

    Const NOMBRE_LISTADO_CUMPLIDOS_OFICINA As String = "lstCumplidosOficina"
    Const NOMBRE_LISTADO_LIQUIDACION_TRANSPORTADOR As String = "lstLiquidacionTransportador"
    Const NOMBRE_LISTADO_LIQUIDACION_OFICINA As String = "lstLiquidacionOficina"
    Const NOMBRE_LISTADO_INFORME_LIQUIDACIONES As String = "lstInformeLiquidaciones"
    Const NOMBRE_LISTADO_NOVEDADES_REMESAS As String = "lstNovedadesRemesas"
    Const NOMBRE_LISTADO_DISTRIBUCION_REMESAS As String = "lstDistribucionRemesas"

    Const NOMBRE_LITADO_FACTURAS_CLIENTE As String = "lstFacturasCliente"
    Const NOMBRE_LISTADO_FACTURAS_OFICINA As String = "lstFacturasOficina"
    Const NOMBRE_LITADO_CARTERA_DETALLADO As String = "lstCarteraDetallado"
    Const NOMBRE_LISTADO_CARTERA_RESUMIDO As String = "lstCarteraResumido"
    Const NOMBRE_LITADO_CARTERA_SALDO_DETALLADO As String = "lstCarteraSaldoDetallado"
    Const NOMBRE_LISTADO_NOTAS_FACTURAS_POR_CLIENTE As String = "lstNotasFacturasCliente"
    Const NOMBRE_LISTADO_NOTAS_FACTURAS_POR_OFICINA As String = "lstNotasFacturasOficina"

    Const NOMBRE_LISTADO_CARTERA_SALDO_RESUMIDO As String = "lstCarteraSaldoResumido"
    Const NOMBRE_LITADO_COMPROBANTE_EGRESO_TERCERO As String = "lstEgresoTercero"
    Const NOMBRE_LISTADO_ANTICIPOS As String = "lstAnticipos"
    Const NOMBRE_LISTADO_COMPROBANTE_EGRESO_OFICINA As String = "lstEgresoOficina"
    Const NOMBRE_LITADO_COMPROBANTE_INGRESO_TERCERO As String = "lstIngresoTercero"
    Const NOMBRE_LISTADO_COMPROBANTE_CAUSACION_OFICINA = "lstCausacionOficina"
    Const NOMBRE_LISTADO_COMPROBANTE_CAUSACION_TERCERO = "lstCausacionTercero"

    Const NOMBRE_LISTADO_COMPROBANTE_INGRESO_OFICINA As String = "lstIngresoOficina"
    Const NOMBRE_LISTADO_REMESA_PENDIENTES_CUMPLIR_CLIENTE As String = "lstRemesaPendienteCumplirCliente"
    Const NOMBRE_LISTADO_REMESA_PENDIENTES_CUMPLIR_OFICINA As String = "lstRemesaPendienteCumplirOficina"
    Const NOMBRE_LISTADO_REMESA_PENDIENTES_FACTURAR_CLIENTE As String = "lstRemesaPendienteFacturarCliente"
    Const NOMBRE_LISTADO_REMESA_PENDIENTES_FACTURAR_OFICINA As String = "lstRemesaPendienteFacturarOficina"
    Const NOMBRE_LISTADO_REMESAS_GENERALES_TIPO As String = "lstRemesasGeneralesTipo"

    Const NOMBRE_LISTADO_LG_RECAUDO_CONTRA_ENTREGA As String = "lstInformeRecaudoContraEntrega"
    Const NOMBRE_LISTADO_LG_DOCUMENTOS_CARTERA As String = "lstInformeDocuCartera"
    Const NOMBRE_LISTADO_LG_DOCUMENTOS_ARCHIVOS As String = "lstInformeDocuArchivos"




    Const NOMBRE_LISTADO_PLANILLA_DESPACHO_PENDIENTES_CUMPLIR_TRANSPORTADOR As String = "lstPlanillaDespachoPendientesCumplirTransportador"
    Const NOMBRE_LISTADO_PLANILLA_DESPACHO_PENDIENTES_CUMPLIR_OFICINA As String = "lstPlanillaDespachoPendientesCimplirOficina"
    Const NOMBRE_LISTADO_PLANILLA_DESPACHO_PENDIENTES_LIQUIDAR_TRANSPORTADOR As String = "lstPlanillaDespachoPendientesLiquidarTransportador"
    Const NOMBRE_LISTADO_PLANILLA_DESPACHO_PENDIENTES_LIQUIDAR_OFICINA As String = "lstPlanillaDespachoPendientesLiquidarOficina"
    Const NOMBRE_LISTADO_PLANILLA_DESPACHO_PENDIENTES_LIQUIDAR As String = "lstPlanillaDespachoPendientesLiquidar"
    Const NOMBRE_LISTADO_LEGALIZACION_GASTOS_RESUMIDO As String = "lstLegalizacionGastosResumido"
    Const NOMBRE_LISTADO_PLANILLAS_DESPACHOS_PROVEEDORES_OFICINA = "lstPlanillasDespachosProveedoresOficina"
    Const NOMBRE_LISTADO_PLANILLAS_DESPACHOS_PROVEEDORES_TERCERO = "lstPlanillasDespachosProveedoresTercero"

    Const NOMBRE_LISTADO_LEGALIZACION_GASTOS_DETALLE As String = "lstDetalladoGastosConductores"
    Const NOMBRE_LITADO_COMPROBANTE_CONTABLE_DETALLADO As String = "lstComprobanteContableDetallado"
    Const NOMBRE_LITADO_COMPROBANTE_CONTABLE_RESUMIDO As String = "lstComprobanteContableResumido"
    Const NOMBRE_LISTADO_SIESA As String = "lstUIAF"
    Const NOMBRE_LISTADO_SOLICITUD_SERVICIOS_CLIENTE = "lstSolicitudServicioCliente"

    Const NOMBRE_LISTADO_SOLICITUD_SERVICIOS_OFICINA = "lstSolicitudServicioOficina"
    Const NOMBRE_LISTADO_ORDEN_SERVICIOS_CLIENTE = "lstOrdenServicioCliente"
    Const NOMBRE_LISTADO_ORDEN_SERVICIOS_OFICINA = "lstOrdenServicioOficina"
    Const NOMBRE_LISTADO_INFORME_PEDIDOS = "lstOrdenInformePedidos"
    Const NOMBRE_LISTADO_INFORME_DETALLE_PROGRAMACIONES = "lstInformeDetalleProgramaciones"
    Const NOMBRE_LISTADO_INFORME_FIDELIZACION = "lstInformeFidelizacion"

    Const NOMBRE_LISTADO_FIDELIZACION_CAUSAL_ANULADAS = "lstFidelizacionCausalAnuladas"
    Const NOMBRE_LISTADO_PLANEACION_CUMPLIMINETO = "lstPlaneacionCumplimiento"
    Const NOMBRE_LISTADO_PRECINTO_DESPACHO_OFICINA = "lstPrecintosDespachoOfic"
    Const NOMBRE_LISTADO_REMESAS_PENDIENTES_TIEMPOS = "lstRemesasPendientesTiempos"
    Const NOMBRE_LISTADO_RECHAZOS_ENTURNAMIENTOS = "lstRechazosEnturnamientos"
    Const NOMBRE_LISTADO_INFORME_SEGUROS = "lstInformeSeguros"

    Const NOMBRE_LISTADO_OPERACION_REAL = "lstOperacionReal"
    Const NOMBRE_LISTADO_TIEMPOS_REMESAS = "lstTiemposRemesas"
    Const NOMBRE_LISTADO_INFORME_GENERAL_OPERACIONES = "lstInformeGeneralOperaciones"


    Const NOMBRE_LITADO_IMPUESTOS = "lstImpuestos"
    Const NOMBRE_LITADO_NOVEDADES_DESPACHO = "lstNovedades"
    Const NOMBRE_LITADO_CONCEPTOS_LIQUIDACION = "lstConceptosLiquidacion"
    Const NOMBRE_LITADO_CONCEPTOS_FACTURACION = "lstConceptosFacturacion"
    Const NOMBRE_LISTADO_SAC_REMESAS_PAQUETERIA = "lstSacRemesasPaqueteria"

    ''SEGURIDAD USUARIOS
    Const NOMBRE_LISTADO_USUARIOS = "lstSeguridadUsuarios"
    Const NOMBRE_LISTADO_GRUPOS = "lstSeguridadGrupos"
    Const NOMBRE_LISTADO_USUARIOS_GRUPO = "lstSeguridadUsuariosGrupo"
    Const NOMBRE_LISTADO_PERMISOS_GRUPO = "lstSeguridadPermisosGrupo"

    'FLOTA PROPIA
    Const NOMBRE_LISTADO_GASTOS_CONDUCTOR_VARIOS_DESPACHOS_CHIP As String = "lstGastosConductoresVariosDespachosPorChip"
    Const NOMBRE_LISTADO_DETALLADO_GASTOS_CONDUCTORES As String = "lstDetalladoGastosConductores"
    Const NOMBRE_LISTADO_SALDO_LEGALIZACION_GASTOS_CONDUCTORES As String = "lstSaldoLegalizacionGastosConductores"
    Const NOMBRE_LISTADO_ANTICIPO_LEGALIZACION_GASTOS_CONDUCTORES As String = "lstAnticipoLegalizacionGastos"
    Const NOMBRE_LISTADO_PLANILLAS_DESPACHOS_OTRAS_EMPRESAS As String = "lstPlanillasDespachosOtrasEmpresas"
    Const NOMBRE_LISTADO_PLANILLAS_DESPACHOS_OTRAS_EMPRESAS_CLIENTE As String = "lstPlanillasDespachosOtrasEmpresasporCliente"
    Const NOMBRE_LISTADO_LEGALIZACION_GASTOS_VARIOS_CONDUCTORES As String = "lstLegalizacionGastosVariosConductores"
    Const NOMBRE_LISTADO_PLANILLAS_PENDIENTES_LEGALIZAR As String = "lstPlanillasDespachosPendientesLegalizar"
    Const NOMBRE_LISTADO_COMBUSTIBLE_CON_CHIP As String = "lstCombustibleConChip"
    Const NOMBRE_LISTADO_GASTOS_FLETE_RUTA = "lstGastosFleteRuta"
    'UTILITARIOS->AUDITORIA
    Const NOMBRE_LISTADO_AUDITORIA_DOCUMENTOS As String = "lstAuditoriaDocumentos"

    'FACTURA CARTERA
    Const NOMBRE_LISTADO_CARTERA_RESUMIDA As String = "lstCarteraResumida"
    Const NOMBRE_LISTADO_CARTERA_DETALLADA As String = "lstCarteraDetallada"

    'Control TRAFICO:
    Const NOMBRE_LISTADO_SEGUIMIENTO_DESPACHOS_HISTORICO = "lstSeguimientoDespachosHistorico"
    Const NOMBRE_LISTADO_SEGUIMIENTO_REMESAS_HISTORICO = "lstSeguimientoRemesasHistorico"

    'liquidacion proveedores'
    Const NOMBRE_LISTADO_LIQUIDACIONES_PLANILLAS_PROVEEDORES_X_OFICINA = "lstliquidacionproveedoresxoficina"
    Const NOMBRE_LISTADO_LIQUIDACION_PLANILLAS_PROVEEDORES = "lstliquidacionproveedores"
    Const NOMBRE_LISTADO_LIQUIDACIONES_PLANILLAS_PROVEEDORES_X_PROVEEDOR = "lstliquidacionproveedoresxproveedor"

    'Facturación:
    Const NOMBRE_LISTADO_REMESAS_X_FACTURAR = "lstRemesasXFacturar"

    'Tarifarios exportar a Excel 
    Const NOMBRE_LISTADO_EXPORTAR_TARIFARIO_VENTA = "lstExportarTarifarioVenta"
    Const NOMBRE_LISTADO_EXPORTAR_TARIFARIO_COMPRA = "lstExportarTarifarioCompra"

#End Region

#Region "Constructores"

    Public Sub New()
        Me.objGeneral = New clsGeneral
        Me.repDocumento = New ReportViewer
    End Sub
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_GASTOS_FLETE_RUTA Then
                Call Configurar_Listado_Gastos_Flete_Ruta()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_SABANA_DATOS Then
                Call Configurar_Listado_Sabana_Datos()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_REMESAS_X_FACTURAR Then
                Call Configurar_Listado_remesas_x_facturar()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_EXPORTAR_TARIFARIO_VENTA Then
                Call Configurar_Excel_Tarifario_Carga_Venta()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_EXPORTAR_TARIFARIO_COMPRA Then
                Call Configurar_Excel_Tarifario_Carga_Compra()
            End If


            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_EXCEL_NOVEDADES_TERCERO Then
                Call Configurar_Listado_Novedades_Tercero()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_EXCEL_NOVEDADES_VEHICULO Then
                Call Configurar_Listado_Novedades_Vehiculo()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_EXCEL_NOVEDADES_SEMIRREMOLQUE Then
                Call Configurar_Listado_Novedades_Semirremolque()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_SEGUIMIENTO_DESPACHOS_HISTORICO Then
                Call Configurar_Seguimiento_Despachos_Historico()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_SEGUIMIENTO_REMESAS_HISTORICO Then
                Call Configurar_Seguimiento_Remesas_Historico()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_EXCEL_PLANILLAS_DESPACHOS_OTRAS_EMPRESAS Then
                Call Configurar_Excel_Planillas_Despachos_Otras_Empresas()
            End If
            'CONTROL CONTENEDORES

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_CONTROL_CONTENEDORES Then
                Call Configurar_Control_Contenedores()
            End If
            'INDICADORES DESPACHOS 

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_INDICADORES_DESPACHOS Then
                Call Configurar_Indicadores_Despachos()
            End If
            'LISTADOS DE PAQUETERIA 
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_GUIA_CLIENTE Then
                Call Configurar_Guias()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_ZONIFICACION_GUIA Then
                Call Configurar_Zonificacion_Guia()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_GUIA_OFICINA Then
                Call Configurar_Guias()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PLANILLAS_RECOGIDA Or
                Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_PLANILLAS_DESPACHOS_PAQUETERIA Or
                Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_PLANILLAS_DESPACHOS_PAQUETERIA_COOTR Or
                Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_PLANILLAS_DESPACHOS_PAQUETERIA_TENEDOR Or
                Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_PLANILLAS_ENTREGAS Then
                Call Configurar_ListadoPlanillasPaqueteria()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_REMESAS_PAQUETERIA Or
                Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_COMISIONES_POR_OFICINA Or
                Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_REMESAS_PENDIENTE_CUMPLIR_PAQUETERIA Or
                Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_REMESAS_PENDIENTE_FACTURAR_PAQUETERIA Or
                Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_VENTAS_OFICINA_PAQUETERIA Then
                Call Configurar_ListadoRemesasPaqueteria()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_SAC_REMESAS_PAQUETERIA Then
                Call Configurar_ListadoSacRemesasPaqueteria()
            End If

            'Diego'


            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_LIQUIDACIONES_PLANILLAS_PROVEEDORES_X_OFICINA Or
                Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_LIQUIDACION_PLANILLAS_PROVEEDORES Or
                Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_LIQUIDACIONES_PLANILLAS_PROVEEDORES_X_PROVEEDOR Then
                Call Configurar_liquidacion_planillas_proveedores()
            End If



            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_INVENTARIO_REMESAS_CLIENTE Or
                Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_INVENTARIO_REMESAS_OFICINA Or
                Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_INVENTARIO_REMESAS_OFICINA_POR_DESTINO Then
                Call Configurar_ListadoRemesasPaqueteriaInventarios()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PLANILLAS_DESPACHOS_PROVEEDORES_OFICINA Or
               Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PLANILLAS_DESPACHOS_PROVEEDORES_TERCERO Then
                Call Configurar_ListadosPlanillasDespachosProveedores()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PUNTOS_SIN_ENTREGAR Then
                Call Configurar_ListadoPuntosSinEntregar()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_INFORME_REEXPEDICION Or
                Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_INFORME_REEXPEDICION_OFICINA Then
                Call Configurar_Informe_reexpediciones()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_NOVEDADES_REMESAS_PAQUETERIA Then
                Call Configurar_ListadoNovedadesRemesasPaqueteria()
            End If

            'LISTADOS SERVICIO AL CLIENTE 
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_SOLICITUD_SERVICIOS_CLIENTE Then
                Call Configurar_Listado_Orden_Servicio()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_SOLICITUD_SERVICIOS_OFICINA Then
                Call Configurar_Listado_Orden_Servicio()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_ORDEN_SERVICIOS_CLIENTE Then
                Call Configurar_Listado_Orden_Servicio()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_ORDEN_SERVICIOS_OFICINA Then
                Call Configurar_Listado_Orden_Servicio()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PRECINTO_DESPACHO_OFICINA Then
                Call Configurar_Listado_Precinto_Despacho_Oficina()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_RECHAZOS_ENTURNAMIENTOS Then
                Call Configurar_Listado_Rechazos_Enturnamiento()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_INFORME_SEGUROS Then
                Call Configurar_Listado_Informe_Seguros()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_REMESAS_PENDIENTES_TIEMPOS Then
                Call Configurar_Listado_Remesas_Pendientes_tiempos()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_INFORME_PEDIDOS Then
                Call Configurar_Listado_Informe_Pedidos()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_INFORME_DETALLE_PROGRAMACIONES Then
                Call Configurar_Listado_Informe_Detalle_Programaciones()
            End If


            'Listados DE BASICO 
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_TERCERO Then
                Call Configurar_Listado_Terceros()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_VEHICULO Then
                Call Configurar_Listado_Vehiculos()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_SEMIRREMOLQUE Then
                Call Configurar_Listado_Semirremolques()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_RUTA Then
                Call Configurar_Listado_Rutas()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_SITIOS_CARGUE_DESCARGUE Then
                Call Configurar_Listado_Sitios_Cargue_Descargue()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_PRODUCTOS_TRANSPORTADOS Then
                Call Configurar_Listado_Productos_Transportados()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_COLORES_VEHICULOS Then
                Call Configurar_Colores_Vehiculos()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_MARCAS_VEHICULOS Then
                Call Configurar_Listado_Marcas_Vehiculos()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_LINEAS_VEHICULOS Then
                Call Configurar_Listado_Lineas_Vehiculos()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_MARCAS_SEMIRREMOLQUES Then
                Call Configurar_Listado_Marcas_Semirremolques()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_PUESTOS_CONTROL Then
                Call Configurar_Listado_Puesto_Control()
            End If

            'LISTADOS 
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_NOVEDADES_REMESAS Then
                Call Configurar_Novedades_Distribucion_Remesas()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_DISTRIBUCION_REMESAS Then
                Call Configurar_Novedades_Distribucion_Remesas()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_ORDEN_CARGUE_CLIENTE Then
                Call Configurar_Orden_Cargue()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_ORDEN_CARGUE_OFICINA Then
                Call Configurar_Orden_Cargue()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_REMESA_CLIENTE Then
                Call Configurar_Remesa()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_REMESA_OFICINA Then
                Call Configurar_Remesa()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PLANILLA_DESPACHO_TRANSPORTADOR Then
                Call Configurar_Planilla_Despacho()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PLANILLA_DESPACHO_OFICINA Then
                Call Configurar_Planilla_Despacho()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_MANIFIESTO_TRANSPORTADOR Then
                Call Configurar_Manifiesto()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_MANIFIESTO_OFICINA Then
                Call Configurar_Manifiesto()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_CUMPLIDOS_TRANSPORTADOR Then
                Call Configurar_Cumplido_Planilla_Despacho()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_CUMPLIDOS_OFICINA Then
                Call Configurar_Cumplido_Planilla_Despacho()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_LIQUIDACION_TRANSPORTADOR Then
                Call Configurar_Liquidacion_Planilla_Despacho()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_LIQUIDACION_OFICINA Then
                Call Configurar_Liquidacion_Planilla_Despacho()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_INFORME_LIQUIDACIONES Then
                Call Configurar_Informe_Liquidacion_Planilla_Despachos()
            End If
            'Fidelizacion
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_FIDELIZACION_CAUSAL_ANULADAS Then
                Call Configurar_Causal_Fidelizacion_Anulacion()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_INFORME_FIDELIZACION Then
                Call Configurar_Informe_Fidelizacion()
            End If

            'Reportes Contabilidad

            If Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_FACTURAS_CLIENTE Then
                Call Configurar_Factura()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_FACTURAS_OFICINA Then
                Call Configurar_Factura()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_CARTERA_DETALLADO Then
                Call Configurar_Cartera()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_CARTERA_RESUMIDO Then
                Call Configurar_Cartera()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_CARTERA_SALDO_DETALLADO Then
                Call Configurar_Cartera()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_CARTERA_SALDO_RESUMIDO Then
                Call Configurar_Cartera()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_NOTAS_FACTURAS_POR_OFICINA Then
                Call Configurar_Listado_Notas_Facturas()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_NOTAS_FACTURAS_POR_CLIENTE Then
                Call Configurar_Listado_Notas_Facturas()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_CARTERA_SALDO_RESUMIDO Then
                Call Configurar_Cartera()
            End If

            'LISTADOS SEGUIMIENTO
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_SEGUIMIENTO_DESPACHOS Then
                Call Configurar_Seguimiento_Despachos()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_SEGUIMIENTO_ORDENES_CARGUE Then
                Call Configurar_Seguimiento_Ordenes_Cargue()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_SEGUIMIENTO_FLOTA_PROPIA Then
                Call Configurar_Seguimiento_Flota_Propia()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_COMPROBANTE_EGRESO_TERCERO Then
                Call Configurar_Comprobante_Egreso()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_COMPROBANTE_EGRESO_OFICINA Then
                Call Configurar_Comprobante_Egreso()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_ANTICIPOS Then
                Call Configurar_Anticipos()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_COMPROBANTE_INGRESO_TERCERO Then
                Call Configurar_Comprobante_Ingreso()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_COMPROBANTE_INGRESO_OFICINA Then
                Call Configurar_Comprobante_Ingreso()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_COMPROBANTE_CAUSACION_OFICINA Then
                Call Configurar_Comprobante_Causacion()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_COMPROBANTE_CAUSACION_TERCERO Then
                Call Configurar_Comprobante_Causacion()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_REMESA_PENDIENTES_CUMPLIR_CLIENTE Then
                Call Configurar_Remesas_Pendientes_Cumplir()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_REMESA_PENDIENTES_CUMPLIR_OFICINA Then
                Call Configurar_Remesas_Pendientes_Cumplir()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_REMESA_PENDIENTES_FACTURAR_CLIENTE Then
                Call Configurar_Remesas_Pendientes_Facturar()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_REMESA_PENDIENTES_FACTURAR_OFICINA Then
                Call Configurar_Remesas_Pendientes_Facturar()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_REMESAS_GENERALES_TIPO Then
                Call Configurar_Remesas_Generales_Por_Tipo()
            End If


            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_LG_DOCUMENTOS_CARTERA Then
                Call Configurar_GL_Documentos_Cartera()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_LG_RECAUDO_CONTRA_ENTREGA Then
                Call Configurar_GL_Recaudo_Contra_Entrega()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_LG_DOCUMENTOS_ARCHIVOS Then
                Call Configurar_GL_Documentos_archivos()
            End If


            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PLANILLA_DESPACHO_PENDIENTES_CUMPLIR_TRANSPORTADOR Then
                Call Configurar_Planilla_Despacho_Pendientes_Cumplir()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PLANILLA_DESPACHO_PENDIENTES_CUMPLIR_OFICINA Then
                Call Configurar_Planilla_Despacho_Pendientes_Cumplir()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PLANILLA_DESPACHO_PENDIENTES_LIQUIDAR_TRANSPORTADOR Then
                Call Configurar_Planilla_Despacho_Pendientes_Liquidar_OFICINA()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PLANILLA_DESPACHO_PENDIENTES_LIQUIDAR_OFICINA Then
                Call Configurar_Planilla_Despacho_Pendientes_Liquidar_OFICINA()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PLANILLA_DESPACHO_PENDIENTES_LIQUIDAR Then
                Call Configurar_Planilla_Despacho_Pendientes_Liquidar()
            End If



            If Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_COMPROBANTE_CONTABLE_DETALLADO Then
                Call Configurar_Comprobante_Contable()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_COMPROBANTE_CONTABLE_RESUMIDO Then
                Call Configurar_Comprobante_Contable()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PLANEACION_CUMPLIMINETO Then
                Call Configurar_Planeacion_Cumplimiento()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_OPERACION_REAL Then
                Call Configurar_Operacion_Real()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_TIEMPOS_REMESAS Then
                Call Configurar_Tiempos_Remesas()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_INFORME_GENERAL_OPERACIONES Then
                Call Configurar_Informe_General_Operaciones()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_IMPUESTOS Then
                Call Configurar_Listado_Impuestos()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_NOVEDADES_DESPACHO Then
                Call Configurar_Listado_Novedades_Despachos()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_CONCEPTOS_LIQUIDACION Then
                Call Configurar_Listado_Conceptos_Liquidacion()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_CONCEPTOS_FACTURACION Then
                Call Configurar_Listado_Conceptos_Facturacion()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_GRUPOS Then
                Call Configurar_Listado_Grupos()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_USUARIOS Then
                Call Configurar_Listado_Usuarios()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_USUARIOS_GRUPO Then
                Call Configurar_Listado_Usuarios_por_Grupo()
            End If


            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PERMISOS_GRUPO Then
                Call Configurar_Listado_Permisos_por_Grupo()
            End If

            'FLOTA PROPIA

            'If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_LEGALIZACION_GASTOS_RESUMIDO Then
            '    Call Configurar_Legalizacion_Gastos()
            'End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_GASTOS_CONDUCTOR_VARIOS_DESPACHOS_CHIP Then
                Call Configurar_listado_gastos_conductor_varios_despachos_chip()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_DETALLADO_GASTOS_CONDUCTORES Then
                Call configurar_listado_detallado_gastos_conductores()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_SALDO_LEGALIZACION_GASTOS_CONDUCTORES Then
                Call configurar_listado_saldo_legalizacion_gastos_conductores()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_ANTICIPO_LEGALIZACION_GASTOS_CONDUCTORES Then
                Call configurar_listado_anticipo_legalizacion_gastos_conductores()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PLANILLAS_DESPACHOS_OTRAS_EMPRESAS Then
                Call configurar_listado_planillas_otras_empresas()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PLANILLAS_DESPACHOS_OTRAS_EMPRESAS_CLIENTE Then
                Call configurar_listado_planillas_otras_empresas_cliente()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_LEGALIZACION_GASTOS_VARIOS_CONDUCTORES Then
                Call configurar_listado_legalizacion_gastos_varios_conductores()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PLANILLAS_PENDIENTES_LEGALIZAR Then
                Call configurar_listado_planillas_pendientes_legalizar()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_COMBUSTIBLE_CON_CHIP Then
                Call configurar_listado_combustible_con_chip()
            End If

            'UTILITARIOS->AUDITORIA
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_AUDITORIA_DOCUMENTOS Then
                Call Configurar_listado_auditoria_documentos()
            End If

            'FACTURA CARTERA
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_CARTERA_RESUMIDA Then
                Call configurar_listado_cartera_resumida()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_CARTERA_DETALLADA Then
                Call configurar_listado_cartera_detallada()
            End If

            'LISTADOS PAQUTERIA V2
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_RECOLECCIONES_POR_OFICINA_CREA Or
               Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_RECOLECCIONES_POR_OFICINA_GESTIONA Then
                Call Configurar_Listado_Recoleccion_Por_Oficina()
            End If

            'Comercial-listados
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_COMERCIAL_TARIFARIO_VENTA Then
                Call Configurar_Listado_Comercial()
            End If



        End If

    End Sub

    Private Sub Configurar_Listado_Sitios_Cargue_Descargue()
        Dim strConsulta As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Basico/lstSitioCargueDescargue.rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_consultar_listado_Sitios_Cargue_Descargue "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Pais")) Then
                strConsulta += "'" & Request.QueryString("Pais") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Ciudad")) Then
                strConsulta += "'" & Request.QueryString("Ciudad") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim Estado As String = Request.QueryString("Estado")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("TipoSitio")) Then
                strConsulta += Request.QueryString("TipoSitio")
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsSitioCargue As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsSitioCargue.Fill(dsReportes, "dtsSitioCargue")

            'Pasar el dataset a un datatable
            Dim dtbSitioCargue As New DataTable
            dtbSitioCargue = dsReportes.Tables("dtsSitioCargue")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsSitioCargue", CType(dtbSitioCargue, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Semirremolques :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Public Sub Configurar_Listado_Terceros()
        Dim strConsulta As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Basico/lstTerceros.rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_consultar_listado_terceros "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Terceros")) And Request.QueryString("Terceros") <> String.Empty Then
                strConsulta += "'" & Request.QueryString("Terceros") & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) And Request.QueryString("Estado") <> String.Empty Then
                strConsulta += Request.QueryString("Estado")
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsTerceros As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsTerceros.Fill(dsReportes, "dtsTerceros")

            'Pasar el dataset a un datatable
            Dim dtbTerceros As New DataTable
            dtbTerceros = dsReportes.Tables("dtsTerceros")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsTerceros", CType(dtbTerceros, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Terceros :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub
    Public Sub Configurar_Listado_Productos_Transportados()
        Dim strConsulta As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Basico/lstProductosTransportados.rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_listado_productos_Transportados "

            strConsulta += Val(Request.QueryString("Empresa")) & ","

            If Not IsNothing(Request.QueryString("Estado")) Then
                strConsulta += Request.QueryString("Estado")
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsProductosTransportados As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsProductosTransportados.Fill(dsReportes, "dtsProductosTransportados")

            'Pasar el dataset a un datatable
            Dim dtbProductosTransportados As New DataTable
            dtbProductosTransportados = dsReportes.Tables("dtsProductosTransportados")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsProductosTransportados", CType(dtbProductosTransportados, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Terceros :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Public Sub Configurar_Colores_Vehiculos()
        Dim strConsulta As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Basico/lstColorVehiculos.rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_listado_Colores_Vehiculos "

            strConsulta += Val(Request.QueryString("Empresa")) & ","


            If Not IsNothing(Request.QueryString("Estado")) Then
                strConsulta += "'" & Request.QueryString("Estado")
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsColores As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsColores.Fill(dsReportes, "dtsColores")

            'Pasar el dataset a un datatable
            Dim dtbColores As New DataTable
            dtbColores = dsReportes.Tables("dtsColores")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsColores", CType(dtbColores, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Terceros :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Public Sub Configurar_Listado_Marcas_Vehiculos()
        Dim strConsulta As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Basico/lstMarcasVehiculos.rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC  gsp_listado_Marcas_Vehiculos "

            strConsulta += Val(Request.QueryString("Empresa")) & ","

            If Not IsNothing(Request.QueryString("Estado")) Then
                strConsulta += "'" & Request.QueryString("Estado")
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsMarcasVehiculos As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsMarcasVehiculos.Fill(dsReportes, "dtsMarcasVehiculos")

            'Pasar el dataset a un datatable
            Dim dtbMarcasVehiculos As New DataTable
            dtbMarcasVehiculos = dsReportes.Tables("dtsMarcasVehiculos")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsMarcasVehiculos", CType(dtbMarcasVehiculos, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Terceros :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Public Sub Configurar_Listado_Lineas_Vehiculos()

        Dim strConsulta As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Basico/lstLineasVehiculos.rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta

            strConsulta = "EXEC  gsp_listado_Lineas_Vehiculos "

            strConsulta += Val(Request.QueryString("Empresa")) & ","

            If Not IsNothing(Request.QueryString("Estado")) Then
                strConsulta += "'" & Request.QueryString("Estado")
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsLineasVehiculos As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsLineasVehiculos.Fill(dsReportes, "dtsLineasVehiculos")

            'Pasar el dataset a un datatable
            Dim dtbLineasVehiculos As New DataTable
            dtbLineasVehiculos = dsReportes.Tables("dtsLineasVehiculos")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsLineasVehiculos", CType(dtbLineasVehiculos, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Terceros :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try
    End Sub

    Public Sub Configurar_Listado_Marcas_Semirremolques()
        Dim strConsulta As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Basico/lstMarcasSemirremolques.rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_listado_Marcas_Semirremolques "

            strConsulta += Val(Request.QueryString("Empresa")) & ","

            If Not IsNothing(Request.QueryString("Estado")) Then
                strConsulta += "'" & Request.QueryString("Estado")
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsMarcasSemirremolques As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsMarcasSemirremolques.Fill(dsReportes, "dtsMarcasSemirremolques")

            'Pasar el dataset a un datatable
            Dim dtbMarcasSemirremolques As New DataTable
            dtbMarcasSemirremolques = dsReportes.Tables("dtsMarcasSemirremolques")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsMarcasSemirremolques", CType(dtbMarcasSemirremolques, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Terceros :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Public Sub Configurar_Listado_Puesto_Control()
        Dim strConsulta As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Basico/lstPuestosControl.rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_listado_Puesto_Control "

            strConsulta += Val(Request.QueryString("Empresa")) & ","

            If Not IsNothing(Request.QueryString("Estado")) Then
                strConsulta += "'" & Request.QueryString("Estado")
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsPuestosControl As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsPuestosControl.Fill(dsReportes, "dtsPuestosControl")

            'Pasar el dataset a un datatable
            Dim dtbPuestosControl As New DataTable
            dtbPuestosControl = dsReportes.Tables("dtsPuestosControl")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsPuestosControl", CType(dtbPuestosControl, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Terceros :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub


    Public Sub Configurar_Listado_Vehiculos()
        Dim strConsulta As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Basico/lstVehiculos.rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_consultar_listado_vehiculos "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Placa")) And Request.QueryString("Placa") <> String.Empty Then
                strConsulta += "'" & Request.QueryString("Placa") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Semirromolque")) And Request.QueryString("Semirromolque") <> String.Empty Then
                strConsulta += "'" & Request.QueryString("Semirromolque") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Conductor")) And Request.QueryString("Conductor") <> String.Empty Then
                strConsulta += "'" & Request.QueryString("Conductor") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Estado")) And Request.QueryString("Estado") <> String.Empty Then
                strConsulta += Request.QueryString("Estado")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsVehiculos As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsVehiculos.Fill(dsReportes, "dtsVehiculos")

            'Pasar el dataset a un datatable
            Dim dtbVehiculos As New DataTable
            dtbVehiculos = dsReportes.Tables("dtsVehiculos")

            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsVehiculos", CType(dtbVehiculos, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Vehiculos :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try


    End Sub


    Public Sub Configurar_Listado_Semirremolques()
        Dim strConsulta As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Basico/lstSemirremolques.rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_consultar_listado_semirremolques "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Semirromolque")) Then
                strConsulta += "'" & Request.QueryString("Semirromolque") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Terceros")) Then
                strConsulta += "'" & Request.QueryString("Terceros") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Estado")) Then
                strConsulta += Request.QueryString("Estado")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsSemirremolques As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsSemirremolques.Fill(dsReportes, "dtsSemirremolques")

            'Pasar el dataset a un datatable
            Dim dtbSemirremolques As New DataTable
            dtbSemirremolques = dsReportes.Tables("dtsSemirremolques")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsSemirremolques", CType(dtbSemirremolques, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Semirremolques :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try


    End Sub

    Public Sub Configurar_Listado_Rutas()
        Dim strConsulta As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Basico/lstRutas.rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_consultar_listado_rutas "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Ruta")) Then
                strConsulta += "'" & Request.QueryString("Ruta") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Estado")) Then
                strConsulta += Request.QueryString("Estado")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsRutas As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsRutas.Fill(dsReportes, "dtsRutas")

            'Pasar el dataset a un datatable
            Dim dtbRutas As New DataTable
            dtbRutas = dsReportes.Tables("dtsRutas")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsRutas", CType(dtbRutas, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Rutas :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try


    End Sub


    Private Sub Configurar_Planilla_Despacho_Pendientes_Liquidar_OFICINA()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PLANILLA_DESPACHO_PENDIENTES_LIQUIDAR_TRANSPORTADOR Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/PlanillasDespachos/lstPlanillaDespachosPendientesLiquidarTransportador.rdlc")
                End If

                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PLANILLA_DESPACHO_PENDIENTES_LIQUIDAR_OFICINA Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/PlanillasDespachos/lstPlanillaDespachosPendientesLiquidarOficina.rdlc")
                End If
            Else
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PLANILLA_DESPACHO_PENDIENTES_LIQUIDAR_TRANSPORTADOR Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/PlanillasDespachos/lstPlanillaDespachosPendientesLiquidarTransportadorxls.rdlc")
                End If

                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PLANILLA_DESPACHO_PENDIENTES_LIQUIDAR_OFICINA Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/PlanillasDespachos/lstPlanillaDespachosPendientesLiquidarOficinaxls.rdlc")
                End If
            End If


            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Listado_Planilla_Despachos_pendientes_Liquidar  "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Identificacion_Cliente")) Then
                Dim Identificacion_Cliente As String = Request.QueryString("Identificacion_Cliente")
                strConsulta += "'" & Identificacion_Cliente & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "'" & Codigo_Oficina & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim Estado As String = Request.QueryString("Estado")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Ruta")) Then
                Dim Ruta As String = Request.QueryString("Ruta")
                strConsulta += "'" & Ruta & "'"
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtslistadoplanillasdespachospendientesliquidar As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtslistadoplanillasdespachospendientesliquidar.Fill(dsReportes, "dtslistadoplanillasdespachospendientesliquidar")

            'Pasar el dataset a un datatable
            Dim dtblistadoplanillasdespachospendientesliquidar As New DataTable
            dtblistadoplanillasdespachospendientesliquidar = dsReportes.Tables("dtslistadoplanillasdespachospendientesliquidar")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtslistadoplanillasdespachospendientesliquidar", CType(dtblistadoplanillasdespachospendientesliquidar, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Private Sub Configurar_Planilla_Despacho_Pendientes_Liquidar()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PLANILLA_DESPACHO_PENDIENTES_LIQUIDAR Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/PlanillasDespachos/lstPlanillaDespachosPendientesLiquidar.rdlc")
                End If
            Else
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PLANILLA_DESPACHO_PENDIENTES_LIQUIDAR Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/PlanillasDespachos/lstPlanillaDespachosPendientesLiquidarrxls.rdlc")
                End If
            End If


            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Listado_Planilla_pendientes_Liquidar  "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Inicial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Inicial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Identificacion_Cliente")) Then
                Dim Identificacion_Cliente As String = Request.QueryString("Identificacion_Cliente")
                strConsulta += "'" & Identificacion_Cliente & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "'" & Codigo_Oficina & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim Estado As String = Request.QueryString("Estado")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Ruta")) Then
                Dim Ruta As String = Request.QueryString("Ruta")
                strConsulta += "'" & Ruta & "'"
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtslistadoplanillasdespachospendientesliquidar As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtslistadoplanillasdespachospendientesliquidar.Fill(dsReportes, "dtslistadoplanillasdespachospendientesliquidar")

            'Pasar el dataset a un datatable
            Dim dtblistadoplanillasdespachospendientesliquidar As New DataTable
            dtblistadoplanillasdespachospendientesliquidar = dsReportes.Tables("dtslistadoplanillasdespachospendientesliquidar")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtslistadoplanillasdespachospendientesliquidar", CType(dtblistadoplanillasdespachospendientesliquidar, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub
    Private Sub Configurar_Planilla_Despacho_Pendientes_Cumplir()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then

                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PLANILLA_DESPACHO_PENDIENTES_CUMPLIR_TRANSPORTADOR Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/PlanillasDespachos/lstPlanillaDespachosPendientesCumplirTransportador.rdlc")
                End If

                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PLANILLA_DESPACHO_PENDIENTES_CUMPLIR_OFICINA Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/PlanillasDespachos/lstPlanillaDespachosPendientesCumplirOficina.rdlc")
                End If
            Else
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PLANILLA_DESPACHO_PENDIENTES_CUMPLIR_TRANSPORTADOR Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/PlanillasDespachos/lstPlanillaDespachosPendientesCumplirTransportadorxls.rdlc")
                End If

                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PLANILLA_DESPACHO_PENDIENTES_CUMPLIR_OFICINA Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/PlanillasDespachos/lstPlanillaDespachosPendientesCumplirOficinaxls.rdlc")
                End If
            End If

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Listado_Planilla_Despachos_pendientes_Cumplir  "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Identificacion_Cliente")) Then
                Dim Identificacion_Cliente As String = Request.QueryString("Identificacion_Cliente")
                strConsulta += "'" & Identificacion_Cliente & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "'" & Codigo_Oficina & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim Estado As String = Request.QueryString("Estado")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Ruta")) Then
                Dim Ruta As String = Request.QueryString("Ruta")
                strConsulta += "'" & Ruta & "'"
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtslistadoplanilladespachospendientescumplir As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtslistadoplanilladespachospendientescumplir.Fill(dsReportes, "dtslistadoplanilladespachospendientescumplir")

            'Pasar el dataset a un datatable
            Dim dtblistadoplanilladespachospendientescumplir As New DataTable
            dtblistadoplanilladespachospendientescumplir = dsReportes.Tables("dtslistadoplanilladespachospendientescumplir")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtslistadoplanilladespachospendientescumplir", CType(dtblistadoplanilladespachospendientescumplir, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Private Sub Configurar_Operacion_Real()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet

            strPathRepo = Server.MapPath("Formatos/Despachos/Progrmacion/lstOperacionReal.rdlc")

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Listado_Operacion_Real  "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Fecha_Inicial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Inicial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Identificacion_Cliente")) Then
                Dim Identificacion_Cliente As String = Request.QueryString("Identificacion_Cliente")
                strConsulta += "'" & Identificacion_Cliente & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "'" & Codigo_Oficina & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim Estado As String = Request.QueryString("Estado")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Ruta")) Then
                Dim Ruta As String = Request.QueryString("Ruta")
                strConsulta += "'" & Ruta & "'"
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtslistadoplanilladespachospendientescumplir As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtslistadoplanilladespachospendientescumplir.SelectCommand.CommandTimeout = 180
            dtslistadoplanilladespachospendientescumplir.Fill(dsReportes, "dtsOperacionReal")

            'Pasar el dataset a un datatable
            Dim dtblistadoplanilladespachospendientescumplir As New DataTable
            dtblistadoplanilladespachospendientescumplir = dsReportes.Tables("dtsOperacionReal")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsOperacionReal", CType(dtblistadoplanilladespachospendientescumplir, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "Operacion_Real", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Private Sub Configurar_Tiempos_Remesas()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet

            strPathRepo = Server.MapPath("Formatos/Despachos/Remesa/lstTiemposRemesa.rdlc")

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Listado_Tiempos_Remesa  "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Identificacion_Cliente")) Then
                Dim Identificacion_Cliente As String = Request.QueryString("Identificacion_Cliente")
                strConsulta += "'" & Identificacion_Cliente & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "'" & Codigo_Oficina & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim Estado As String = Request.QueryString("Estado")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Ruta")) Then
                Dim Ruta As String = Request.QueryString("Ruta")
                strConsulta += "'" & Ruta & "'"
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsTiemposRemesa As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsTiemposRemesa.Fill(dsReportes, "dtsTiemposRemesa")

            'Pasar el dataset a un datatable
            Dim dtbTiemposRemesa As New DataTable
            dtbTiemposRemesa = dsReportes.Tables("dtsTiemposRemesa")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsTiemposRemesa", CType(dtbTiemposRemesa, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "Tiempos_Remesas", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Private Sub Configurar_Informe_General_Operaciones()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet

            strPathRepo = Server.MapPath("Formatos/Despachos/InformeGnerealOperaciones/lstInformeGeneralOperaciones.rdlc")
            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Listado_Informe_General_Operaciones  "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "'" & Codigo_Oficina & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim Estado As String = Request.QueryString("Estado")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Ruta")) Then
                Dim Ruta As String = Request.QueryString("Ruta")
                strConsulta += "'" & Ruta & "'"
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsInformeGeneralOperacion As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsInformeGeneralOperacion.Fill(dsReportes, "dtsInformeGeneralOperacion")

            'Pasar el dataset a un datatable
            Dim dtbInformeGeneralOperacion As New DataTable
            dtbInformeGeneralOperacion = dsReportes.Tables("dtsInformeGeneralOperacion")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtbInformeGeneralOperacion", CType(dtbInformeGeneralOperacion, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "Informe_General_Operaciones", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Private Sub Configurar_Comprobante_Ingreso()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                If Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_COMPROBANTE_INGRESO_TERCERO Then
                    strPathRepo = Server.MapPath("Formatos/Tesoreria/Comprobantes/lstComprobanteIngresoCliente.rdlc")
                End If

                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_COMPROBANTE_INGRESO_OFICINA Then
                    strPathRepo = Server.MapPath("Formatos/Tesoreria/Comprobantes/lstComprobanteIngresoOficina.rdlc")
                End If
            Else
                If Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_COMPROBANTE_INGRESO_TERCERO Then
                    strPathRepo = Server.MapPath("Formatos/Tesoreria/Comprobantes/lstComprobanteIngresoClientexls.rdlc")
                End If

                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_COMPROBANTE_INGRESO_OFICINA Then
                    strPathRepo = Server.MapPath("Formatos/Tesoreria/Comprobantes/lstComprobanteIngresoOficinaxls.rdlc")
                End If
            End If


            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC  gsp_listado_comprobante_Ingreso  "


            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                strConsulta += "" & Request.QueryString("Numero_Documento_Inicial") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                strConsulta += "" & Request.QueryString("Numero_Documento_Final") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Incial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("TERC_Codigo")) Then
                strConsulta += "" & Request.QueryString("TERC_Codigo") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("DocumentoOrigen")) Then
                strConsulta += "" & Request.QueryString("DocumentoOrigen") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("NumeroDocumentoOrigen")) Then
                strConsulta += "" & Request.QueryString("NumeroDocumentoOrigen") & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "'" & Codigo_Oficina & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim Estado As String = Request.QueryString("Estado")
                strConsulta += "'" & Estado & "'"
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsComprobanteIngreso As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsComprobanteIngreso.Fill(dsReportes, "dtsComprobanteIngreso")

            'Pasar el dataset a un datatable
            Dim dtbComprobanteIngreso As New DataTable
            dtbComprobanteIngreso = dsReportes.Tables("dtsComprobanteIngreso")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsComprobanteIngreso", CType(dtbComprobanteIngreso, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Private Sub Configurar_Comprobante_Causacion()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_COMPROBANTE_CAUSACION_TERCERO Then
                strPathRepo = Server.MapPath("Formatos/Tesoreria/Comprobantes/lstComprobanteCausacionTercero.rdlc")
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_COMPROBANTE_CAUSACION_OFICINA Then
                strPathRepo = Server.MapPath("Formatos/Tesoreria/Comprobantes/lstComprobanteCausacionOficina.rdlc")
            End If


            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC  gsp_listado_comprobante_Causacion "


            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                strConsulta += "" & Request.QueryString("Numero_Documento_Inicial") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                strConsulta += "" & Request.QueryString("Numero_Documento_Final") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("TICO_Codigo")) Then
                strConsulta += "'" & Request.QueryString("TICO_Codigo") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Incial") & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("TERC_Codigo")) Then
                strConsulta += "" & Request.QueryString("TERC_Codigo") & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "'" & Codigo_Oficina & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Codigo_Oficina_Destino")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina_Destino")
                strConsulta += "'" & Codigo_Oficina & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim Estado As String = Request.QueryString("Estado")
                strConsulta += "'" & Estado & "'"
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsComprobanteIngreso As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsComprobanteIngreso.Fill(dsReportes, "dtsComprobanteCausacion")

            'Pasar el dataset a un datatable
            Dim dtbComprobanteIngreso As New DataTable
            dtbComprobanteIngreso = dsReportes.Tables("dtsComprobanteCausacion")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsComprobanteCausacion", CType(dtbComprobanteIngreso, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub


    Private Sub Configurar_ListadosPlanillasDespachosProveedores()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PLANILLAS_DESPACHOS_PROVEEDORES_OFICINA Then
                strPathRepo = Server.MapPath("Formatos/Proveedores/lstPlanillasDespachosProveedoresOficina.rdlc")
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PLANILLAS_DESPACHOS_PROVEEDORES_TERCERO Then
                strPathRepo = Server.MapPath("Formatos/Proveedores/lstPlanillasDespachosProveedoresTercero.rdlc")
            End If


            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC  gsp_listado_planilla_despachos_proveedores "


            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                If Request.QueryString("Numero_Documento_Inicial") > 0 And Request.QueryString("Numero_Documento_Inicial") <> "0" Then
                    strConsulta += "" & Request.QueryString("Numero_Documento_Inicial") & ","
                Else
                    strConsulta += "NULL,"
                End If
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                If Request.QueryString("Numero_Documento_Final") > 0 And Request.QueryString("Numero_Documento_Final") <> "0" Then
                    strConsulta += "" & Request.QueryString("Numero_Documento_Final") & ","
                Else
                    strConsulta += "NULL,"
                End If
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Incial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("TERC_Codigo")) Then
                strConsulta += "" & Request.QueryString("TERC_Codigo") & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Transportador")) Then
                strConsulta += "" & Request.QueryString("Transportador") & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                strConsulta += "" & Request.QueryString("Codigo_Oficina") & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim Estado As String = Request.QueryString("Estado")
                strConsulta += "'" & Estado & "'"
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsComprobanteIngreso As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsComprobanteIngreso.Fill(dsReportes, "dtsPlanillasDespachosProveedores")

            'Pasar el dataset a un datatable
            Dim dtbComprobanteIngreso As New DataTable
            dtbComprobanteIngreso = dsReportes.Tables("dtsPlanillasDespachosProveedores")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsPlanillasDespachosProveedores", CType(dtbComprobanteIngreso, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub
    Private Sub Configurar_Comprobante_Egreso()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet


            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                If Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_COMPROBANTE_EGRESO_TERCERO Then
                    strPathRepo = Server.MapPath("Formatos/Tesoreria/Comprobantes/lstComprobanteEgresoCliente.rdlc")
                End If

                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_COMPROBANTE_EGRESO_OFICINA Then
                    strPathRepo = Server.MapPath("Formatos/Tesoreria/Comprobantes/lstComprobanteEgresoOficina.rdlc")
                End If
            Else
                If Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_COMPROBANTE_EGRESO_TERCERO Then
                    strPathRepo = Server.MapPath("Formatos/Tesoreria/Comprobantes/lstComprobanteEgresoClientexls.rdlc")
                End If

                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_COMPROBANTE_EGRESO_OFICINA Then
                    strPathRepo = Server.MapPath("Formatos/Tesoreria/Comprobantes/lstComprobanteEgresoOficinaxls.rdlc")
                End If
            End If
            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC  gsp_listado_comprobante_egreso  "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                strConsulta += "" & Request.QueryString("Numero_Documento_Inicial") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                strConsulta += "" & Request.QueryString("Numero_Documento_Final") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Incial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("TERC_Codigo")) Then
                strConsulta += "" & Request.QueryString("TERC_Codigo") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("DocumentoOrigen")) Then
                strConsulta += "" & Request.QueryString("DocumentoOrigen") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("NumeroDocumentoOrigen")) Then
                strConsulta += "" & Request.QueryString("NumeroDocumentoOrigen") & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "" & Codigo_Oficina & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim Estado As String = Request.QueryString("Estado")
                strConsulta += "" & Estado & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("CentroOperaciones")) Then
                Dim CentroOperaciones As String = Request.QueryString("CentroOperaciones")
                strConsulta += "" & CentroOperaciones & ""
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsComprobanteEgreso As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsComprobanteEgreso.Fill(dsReportes, "dtsComprobanteEgreso")

            'Pasar el dataset a un datatable
            Dim dtbComprobanteEgreso As New DataTable
            dtbComprobanteEgreso = dsReportes.Tables("dtsComprobanteEgreso")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsComprobanteEgreso", CType(dtbComprobanteEgreso, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Private Sub Configurar_Anticipos()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet


            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_ANTICIPOS Then
                    strPathRepo = Server.MapPath("Formatos/Tesoreria/Comprobantes/lstAnticipos.rdlc")
                End If
            Else
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_ANTICIPOS Then
                    strPathRepo = Server.MapPath("Formatos/Tesoreria/Comprobantes/lstAnticiposxls.rdlc")
                End If
            End If
            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC  gsp_listado_Anticipos  "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                strConsulta += "" & Request.QueryString("Numero_Documento_Inicial") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                strConsulta += "" & Request.QueryString("Numero_Documento_Final") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Incial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("TERC_Codigo")) Then
                strConsulta += "" & Request.QueryString("TERC_Codigo") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("NumeroDocumentoOrigen")) Then
                strConsulta += "" & Request.QueryString("NumeroDocumentoOrigen") & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "" & Codigo_Oficina & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim Estado As String = Request.QueryString("Estado")
                strConsulta += "" & Estado & ","
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsComprobanteEgreso As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsComprobanteEgreso.Fill(dsReportes, "dtsAnticipos")

            'Pasar el dataset a un datatable
            Dim dtbComprobanteEgreso As New DataTable
            dtbComprobanteEgreso = dsReportes.Tables("dtsAnticipos")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsAnticipos", CType(dtbComprobanteEgreso, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub
    Private Sub Configurar_Seguimiento_Despachos()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_SEGUIMIENTO_DESPACHOS Then
                    strPathRepo = Server.MapPath("Formatos/Seguimiento/lstSeguimientoDespachos.rdlc")
                End If
            Else
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_SEGUIMIENTO_DESPACHOS Then
                    strPathRepo = Server.MapPath("Formatos/Seguimiento/lstSeguimientoDespachosxls.rdlc")
                End If
            End If


            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_listado_seguimiento_vehicular_planilla_despachos  "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Vehiculo")) Then
                strConsulta += "'" & Request.QueryString("Vehiculo") & "',"
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("Conductor")) Then
                strConsulta += "'" & Request.QueryString("Conductor") & "',"
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("Cliente")) Then
                strConsulta += "'" & Request.QueryString("Cliente") & "',"
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("TipoDueno")) Then
                strConsulta += "'" & Request.QueryString("TipoDueno") & "',"
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("Producto")) Then
                strConsulta += "'" & Request.QueryString("Producto") & "',"
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("Origen")) Then
                strConsulta += "'" & Request.QueryString("Origen") & "',"
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("Destino")) Then
                strConsulta += "'" & Request.QueryString("Destino") & "',"
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("Ruta")) Then
                strConsulta += "" & Request.QueryString("Ruta") & ","
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("PuestoControl")) Then
                strConsulta += "" & Request.QueryString("PuestoControl") & ","
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("NumeroPlanilla")) Then
                strConsulta += "'" & Request.QueryString("NumeroPlanilla") & "',"
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("NumeroManifiesto")) Then
                strConsulta += "'" & Request.QueryString("NumeroManifiesto") & "',"
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("OrigenReporte")) Then
                strConsulta += "" & Request.QueryString("OrigenReporte") & ","
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("SitioReporte")) Then
                strConsulta += "" & Request.QueryString("SitioReporte") & ","
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("Mostrar")) Then
                strConsulta += "" & Request.QueryString("Mostrar") & ","
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("CodigosOficinas")) Then
                strConsulta += "'" & Request.QueryString("CodigosOficinas") & "',"
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("Anulado")) Then
                strConsulta += "" & Request.QueryString("Anulado") & ""
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsSeguimiento As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsSeguimiento.Fill(dsReportes, "dtsSeguimientoVehicularPlanillasDespachos")

            'Pasar el dataset a un datatable
            Dim dtbSeguimiento As New DataTable
            dtbSeguimiento = dsReportes.Tables("dtsSeguimientoVehicularPlanillasDespachos")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsSeguimientoVehicularPlanillasDespachos", CType(dtbSeguimiento, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Seguimiento_Despachos :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Private Sub Configurar_Seguimiento_Ordenes_Cargue()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_SEGUIMIENTO_ORDENES_CARGUE Then
                    strPathRepo = Server.MapPath("Formatos/Seguimiento/lstSeguimientoOrdenesCargue.rdlc")
                End If
            Else
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_SEGUIMIENTO_ORDENES_CARGUE Then
                    strPathRepo = Server.MapPath("Formatos/Seguimiento/lstSeguimientoOrdenesCarguexls.rdlc")
                End If
            End If


            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_listado_seguimiento_vehicular_ordenes_cargue  "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Vehiculo")) Then
                strConsulta += "'" & Request.QueryString("Vehiculo") & "',"
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("Conductor")) Then
                strConsulta += "'" & Request.QueryString("Conductor") & "',"
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("Cliente")) Then
                strConsulta += "'" & Request.QueryString("Cliente") & "',"
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("TipoDueno")) Then
                strConsulta += "'" & Request.QueryString("TipoDueno") & "',"
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("Producto")) Then
                strConsulta += "'" & Request.QueryString("Producto") & "',"
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("Origen")) Then
                strConsulta += "'" & Request.QueryString("Origen") & "',"
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("Destino")) Then
                strConsulta += "'" & Request.QueryString("Destino") & "',"
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("Ruta")) Then
                strConsulta += "" & Request.QueryString("Ruta") & ","
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("PuestoControl")) Then
                strConsulta += "" & Request.QueryString("PuestoControl") & ","
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("NumeroPlanilla")) Then
                strConsulta += "'" & Request.QueryString("NumeroPlanilla") & "',"
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("NumeroManifiesto")) Then
                strConsulta += "'" & Request.QueryString("NumeroManifiesto") & "',"
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("OrigenReporte")) Then
                strConsulta += "" & Request.QueryString("OrigenReporte") & ","
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("SitioReporte")) Then
                strConsulta += "" & Request.QueryString("SitioReporte") & ","
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("Mostrar")) Then
                strConsulta += "" & Request.QueryString("Mostrar") & ","
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("CodigosOficinas")) Then
                strConsulta += "'" & Request.QueryString("CodigosOficinas") & "',"
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("Anulado")) Then
                strConsulta += "" & Request.QueryString("Anulado") & ""
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsSeguimiento As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsSeguimiento.Fill(dsReportes, "dtsSeguimientoVehicularOrdenesCargue")

            'Pasar el dataset a un datatable
            Dim dtbSeguimiento As New DataTable
            dtbSeguimiento = dsReportes.Tables("dtsSeguimientoVehicularOrdenesCargue")

            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsSeguimientoVehicularOrdenesCargue", CType(dtbSeguimiento, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Seguimiento_Ordenes_Cargue :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub
    Private Sub Configurar_Seguimiento_Flota_Propia()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_SEGUIMIENTO_FLOTA_PROPIA Then
                    strPathRepo = Server.MapPath("Formatos/Seguimiento/lstSeguimientoFlotaPropia.rdlc")
                End If
            Else
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_SEGUIMIENTO_FLOTA_PROPIA Then
                    strPathRepo = Server.MapPath("Formatos/Seguimiento/lstSeguimientoFlotaPropiaxls.rdlc")
                End If
            End If


            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_listado_seguimiento_flota_propia  "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            If Not IsNothing(Request.QueryString("Mostrar")) Then
                strConsulta += "" & Request.QueryString("Mostrar") & ","
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("NumeroManifiesto")) Then
                strConsulta += "'" & Request.QueryString("NumeroManifiesto") & "',"
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("PuestoControl")) Then
                strConsulta += "" & Request.QueryString("PuestoControl") & ","
            Else
                strConsulta += " NULL,"
            End If

            If Not IsNothing(Request.QueryString("Vehiculo")) Then
                strConsulta += "'" & Request.QueryString("Vehiculo") & "',"
            Else
                strConsulta += " NULL,"
            End If

            If Not IsNothing(Request.QueryString("Conductor")) Then
                strConsulta += "'" & Request.QueryString("Conductor") & "',"
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("OrigenReporte")) Then
                strConsulta += "" & Request.QueryString("OrigenReporte") & ","
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("SitioReporte")) Then
                strConsulta += "" & Request.QueryString("SitioReporte") & ","
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("CodigosOficinas")) Then
                strConsulta += "'" & Request.QueryString("CodigosOficinas") & "',"
            Else
                strConsulta += " NULL,"
            End If
            If Not IsNothing(Request.QueryString("Anulado")) Then
                strConsulta += "" & Request.QueryString("Anulado") & ""
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsSeguimiento As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsSeguimiento.Fill(dsReportes, "dtsSeguimientoFlotaPropia")

            'Pasar el dataset a un datatable
            Dim dtbSeguimiento As New DataTable
            dtbSeguimiento = dsReportes.Tables("dtsSeguimientoFlotaPropia")

            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsSeguimientoFlotaPropia", CType(dtbSeguimiento, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Seguimiento_Ordenes_Cargue :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Private Sub Configurar_Factura()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                If Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_FACTURAS_CLIENTE Then
                    strPathRepo = Server.MapPath("Formatos/Facturacion/Facturas/lstFacturaCliente.rdlc")
                End If

                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_FACTURAS_OFICINA Then
                    strPathRepo = Server.MapPath("Formatos/Facturacion/Facturas/lstFacturaOficina.rdlc")
                End If
            Else
                If Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_FACTURAS_CLIENTE Then
                    strPathRepo = Server.MapPath("Formatos/Facturacion/Facturas/lstFacturaClientexls.rdlc")
                End If

                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_FACTURAS_OFICINA Then
                    strPathRepo = Server.MapPath("Formatos/Facturacion/Facturas/lstFacturaOficinaxls.rdlc")
                End If
            End If


            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_listado_facturas   "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Identificacion_Cliente")) Then
                Dim Identificacion_Cliente As String = Request.QueryString("Identificacion_Cliente")
                strConsulta += "'" & Identificacion_Cliente & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "'" & Codigo_Oficina & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                If Request.QueryString("Estado") = 2 Then
                    strConsulta += "NULL, 1"
                Else
                    strConsulta += Request.QueryString("Estado")
                End If
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsFacturas As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsFacturas.Fill(dsReportes, "dtsFacturas")

            'Pasar el dataset a un datatable
            Dim dtbFacturas As New DataTable
            dtbFacturas = dsReportes.Tables("dtsFacturas")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsFacturas", CType(dtbFacturas, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Public Sub Configurar_Guias()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_GUIA_CLIENTE Then
                strPathRepo = Server.MapPath("Formatos/Despachos/Guias/lstGuiasCliente.rdlc")
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_GUIA_OFICINA Then
                strPathRepo = Server.MapPath("Formatos/Despachos/Guias/lstGuiasOficina.rdlc")
            End If

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_listado_guias "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Identificacion_Cliente")) Then
                Dim Identificacion_Cliente As String = Request.QueryString("Identificacion_Cliente")
                strConsulta += "'" & Identificacion_Cliente & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Identificacion_Remitente")) Then
                Dim Identificacion_Remitente As String = Request.QueryString("Identificacion_Remitente")
                strConsulta += "'" & Identificacion_Remitente & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Identificacion_Destinario")) Then
                Dim Identificacion_Destinario As String = Request.QueryString("Identificacion_Destinario")
                strConsulta += "'" & Identificacion_Destinario & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "'" & Codigo_Oficina & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim Estado As String = Request.QueryString("Estado")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Ruta")) Then
                Dim Ruta As String = Request.QueryString("Ruta")
                strConsulta += "'" & Ruta & "'"
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsGuias As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsGuias.Fill(dsReportes, "dtsGuias")

            'Pasar el dataset a un datatable
            Dim dtbGuias As New DataTable
            dtbGuias = dsReportes.Tables("dtsGuias")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsGuias", CType(dtbGuias, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Guias_por_Cliente :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub



    Public Sub Configurar_ListadoPlanillasPaqueteria()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PLANILLAS_RECOGIDA Then
                strPathRepo = Server.MapPath("Formatos/Paqueteria/lstPlanillasRecogidas.rdlc")
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_PLANILLAS_DESPACHOS_PAQUETERIA Then
                strPathRepo = Server.MapPath("Formatos/Paqueteria/lstPlanillasDespachosPaqueteria.rdlc")
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_PLANILLAS_DESPACHOS_PAQUETERIA_COOTR Then
                strPathRepo = Server.MapPath("Formatos/Paqueteria/lstPlanillasDespachosPaqueteria_COOTR.rdlc")
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_PLANILLAS_DESPACHOS_PAQUETERIA_TENEDOR Then
                strPathRepo = Server.MapPath("Formatos/Paqueteria/lstPlanillasDespachosPaqueteriaTenedor.rdlc")
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_PLANILLAS_ENTREGAS Then
                strPathRepo = Server.MapPath("Formatos/Paqueteria/lstPlanillasEntregas.rdlc")
            End If

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Listado_Planilla_Paqueteria "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            strConsulta += Val(Request.QueryString("TipoDocumento")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Oficina")) Then
                Dim Oficina As String = Request.QueryString("Oficina")
                strConsulta += "'" & Oficina & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim Estado As String = Request.QueryString("Estado")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Ruta")) Then
                Dim Ruta As String = Request.QueryString("Ruta")
                strConsulta += "'" & Ruta & "'"
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsPlanilla As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsPlanilla.Fill(dsReportes, "dtsPlanillaPaqueteria")

            'Pasar el dataset a un datatable
            Dim dtbPlanilla As New DataTable
            dtbPlanilla = dsReportes.Tables("dtsPlanillaPaqueteria")

            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsPlanillaPaqueteria", CType(dtbPlanilla, DataTable)))
            rpwDocumento.LocalReport.Refresh()

            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If


        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_ListadoPlanillasPaqueteria :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Public Sub Configurar_ListadoRemesasPaqueteria()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_REMESAS_PAQUETERIA Then
                strPathRepo = Server.MapPath("Formatos/Paqueteria/lstRemesas.rdlc")
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_COMISIONES_POR_OFICINA Then
                strPathRepo = Server.MapPath("Formatos/Paqueteria/lstComisionesOficina.rdlc")
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_REMESAS_PENDIENTE_CUMPLIR_PAQUETERIA Then
                strPathRepo = Server.MapPath("Formatos/Paqueteria/lstRemesasPorCumplir.rdlc")
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_REMESAS_PENDIENTE_FACTURAR_PAQUETERIA Then
                strPathRepo = Server.MapPath("Formatos/Paqueteria/lstRemesasPorFacturar.rdlc")
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_VENTAS_OFICINA_PAQUETERIA Then
                strPathRepo = Server.MapPath("Formatos/Paqueteria/lstVentasPorOficinaPaqueteria.rdlc")
            End If


            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Listado_Remesas_Paqueteria "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            strConsulta += Val(Request.QueryString("TipoDocumento")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("FormaPago")) Then
                Dim FormaPago As String = Request.QueryString("FormaPago")
                strConsulta += "'" & FormaPago & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Reexpedicion")) Then
                Dim Reexpedicion As String = Request.QueryString("Reexpedicion")
                strConsulta += "'" & Reexpedicion & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("PendienteCumplir")) Then
                Dim PendienteCumplir As String = Request.QueryString("PendienteCumplir")
                strConsulta += "'" & PendienteCumplir & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("PendienteFacturar")) Then
                Dim PendienteFacturar As String = Request.QueryString("PendienteFacturar")
                strConsulta += "'" & PendienteFacturar & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Oficina")) Then
                Dim Oficina As String = Request.QueryString("Oficina")
                strConsulta += "'" & Oficina & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Cliente")) Then
                Dim Fecha_Final As String = Request.QueryString("Cliente")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim Estado As String = Request.QueryString("Estado")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("OficinaActual")) Then
                Dim OficinaActual As String = Request.QueryString("OficinaActual")
                strConsulta += "'" & OficinaActual & "'"
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsRemesa As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsRemesa.Fill(dsReportes, "dtsListadoRemesasPaqueteria")

            'Pasar el dataset a un datatable
            Dim dtbRemesa As New DataTable
            dtbRemesa = dsReportes.Tables("dtsListadoRemesasPaqueteria")

            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsListadoRemesasPaqueteria", CType(dtbRemesa, DataTable)))
            rpwDocumento.LocalReport.Refresh()

            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If


        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_ListadoRemesasPaqueteria :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Public Sub Configurar_ListadoSacRemesasPaqueteria()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_SAC_REMESAS_PAQUETERIA Then
                strPathRepo = Server.MapPath("Formatos/Paqueteria/lstSacRemesasPaqueteria.rdlc")
            End If

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Listado_sac_remesas_paqueteria "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            'strConsulta += Val(Request.QueryString("TipoDocumento")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Cliente")) Then
                Dim cliente As String = Request.QueryString("Cliente")
                strConsulta += "'" & cliente & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Oficina")) Then
                Dim Oficina As String = Request.QueryString("Oficina")
                strConsulta += "'" & Oficina & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim Estado As String = Request.QueryString("Estado")
                strConsulta += "'" & Estado & "'"
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsSacRemesas As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsSacRemesas.Fill(dsReportes, "dtsListadoSacRemesasPaqueteria")

            'Pasar el dataset a un datatable
            Dim dtbSacRemesa As New DataTable
            dtbSacRemesa = dsReportes.Tables("dtsListadoSacRemesasPaqueteria")

            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc) dtslistadoSacRemesaPaqueteria
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtslistadoSacRemesaPaqueteria", CType(dtbSacRemesa, DataTable)))
            rpwDocumento.LocalReport.Refresh()

            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If


        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_ListadoRemesasPaqueteria :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Public Sub Configurar_ListadoRemesasPaqueteriaInventarios()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet


            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_INVENTARIO_REMESAS_OFICINA Then
                strPathRepo = Server.MapPath("Formatos/Paqueteria/lstInventarioRemesasOficina.rdlc")
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_INVENTARIO_REMESAS_CLIENTE Then
                strPathRepo = Server.MapPath("Formatos/Paqueteria/lstInventarioRemesasOficinaporCliente.rdlc")
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_INVENTARIO_REMESAS_OFICINA_POR_DESTINO Then
                strPathRepo = Server.MapPath("Formatos/Paqueteria/lstInventarioRemesasOficinaporDestino.rdlc")
            End If
            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Listado_Remesas_Paqueteria_Inventarios "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            strConsulta += Val(Request.QueryString("TipoDocumento")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("FormaPago")) Then
                Dim FormaPago As String = Request.QueryString("FormaPago")
                strConsulta += "'" & FormaPago & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Reexpedicion")) Then
                Dim Reexpedicion As String = Request.QueryString("Reexpedicion")
                strConsulta += "'" & Reexpedicion & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("PendienteCumplir")) Then
                Dim PendienteCumplir As String = Request.QueryString("PendienteCumplir")
                strConsulta += "'" & PendienteCumplir & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("PendienteFacturar")) Then
                Dim PendienteFacturar As String = Request.QueryString("PendienteFacturar")
                strConsulta += "'" & PendienteFacturar & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Oficina")) Then
                Dim Oficina As String = Request.QueryString("Oficina")
                strConsulta += "'" & Oficina & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Cliente")) Then
                Dim Fecha_Final As String = Request.QueryString("Cliente")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim Estado As String = Request.QueryString("Estado")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("OficinaActual")) Then
                Dim OficinaActual As String = Request.QueryString("OficinaActual")
                strConsulta += "'" & OficinaActual & "'"
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsRemesa As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsRemesa.Fill(dsReportes, "dtsListadoRemesasPaqueteria")

            'Pasar el dataset a un datatable
            Dim dtbRemesa As New DataTable
            dtbRemesa = dsReportes.Tables("dtsListadoRemesasPaqueteria")

            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsListadoRemesasPaqueteria", CType(dtbRemesa, DataTable)))
            rpwDocumento.LocalReport.Refresh()

            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If


        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_ListadoRemesasPaqueteria :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Public Sub Configurar_ListadoPuntosSinEntregar()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet

            strPathRepo = Server.MapPath("Formatos/Paqueteria/lstPuntosSinEntregar.rdlc")
            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_obtener_encabezados_listados "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Incial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "'"
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsObtenerEncabezadoListados")
            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dtsObtenerEncabezadoListados")

            'Armar la consulta
            strConsulta = "EXEC gsp_Listado_Remesas_Paqueteria_Pendientes_Entregas "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            strConsulta += Val(Request.QueryString("TipoDocumento")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("FormaPago")) Then
                Dim FormaPago As String = Request.QueryString("FormaPago")
                strConsulta += "'" & FormaPago & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Oficina")) Then
                Dim Oficina As String = Request.QueryString("Oficina")
                strConsulta += "'" & Oficina & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim Estado As String = Request.QueryString("Estado")
                strConsulta += "'" & Estado & "'"
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsRemesa As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsRemesa.Fill(dsReportes, "dtListadoRemesesaPaqueteriaPendientesEntregas")

            'Pasar el dataset a un datatable
            Dim dtbRemesa As New DataTable
            dtbRemesa = dsReportes.Tables("dtListadoRemesesaPaqueteriaPendientesEntregas")

            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsObtenerEncabezadoListados", CType(dtb, DataTable)))
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtListadoRemesesaPaqueteriaPendientesEntregas", CType(dtbRemesa, DataTable)))
            rpwDocumento.LocalReport.Refresh()

            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If


        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_ListadoPuntosSinEntregar :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub
    Public Sub Configurar_Informe_reexpediciones()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_INFORME_REEXPEDICION Then
                strPathRepo = Server.MapPath("Formatos/Paqueteria/lstInformeReexpediciones.rdlc")
            ElseIf Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_INFORME_REEXPEDICION_OFICINA Then
                strPathRepo = Server.MapPath("Formatos/Paqueteria/lstInformeReexpedicionesPorOficina.rdlc")
            End If

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_obtener_encabezados_listados "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Incial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "'"
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsObtenerEncabezadoListados")
            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dtsObtenerEncabezadoListados")

            'Armar la consulta
            strConsulta = "EXEC gsp_informe_reexpediciones "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            strConsulta += Val(Request.QueryString("TipoDocumento")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("FormaPago")) Then
                Dim FormaPago As String = Request.QueryString("FormaPago")
                strConsulta += "'" & FormaPago & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Oficina")) Then
                Dim Oficina As String = Request.QueryString("Oficina")
                strConsulta += "'" & Oficina & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Planilla")) Then
                strConsulta += Request.QueryString("Planilla") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Placa")) Then
                strConsulta += "'" & Request.QueryString("Placa") & "'"
            Else
                strConsulta += "NULL"
            End If


            'Guardar el resultado del sp en un dataset
            Dim dtsRemesa As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsRemesa.Fill(dsReportes, "dtsReexpediciones")

            'Pasar el dataset a un datatable
            Dim dtbRemesa As New DataTable
            dtbRemesa = dsReportes.Tables("dtsReexpediciones")

            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsObtenerEncabezadoListados", CType(dtb, DataTable)))
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsReexpediciones", CType(dtbRemesa, DataTable)))
            rpwDocumento.LocalReport.Refresh()

            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If


        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_ListadoPuntosSinEntregar :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Public Sub Configurar_ListadoNovedadesRemesasPaqueteria()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            strPathRepo = Server.MapPath("Formatos/Paqueteria/" & NOMBRE_LISTADO_NOVEDADES_REMESAS_PAQUETERIA & ".rdlc")

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Listado_Novedades_Remesas_Paqueteria "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            strConsulta += Val(Request.QueryString("TipoDocumento")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Cliente")) Then
                Dim Fecha_Final As String = Request.QueryString("Cliente")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Oficina")) Then
                Dim Oficina As String = Request.QueryString("Oficina")
                strConsulta += "'" & Oficina & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim Estado As String = Request.QueryString("Estado")
                strConsulta += "'" & Estado & "'"
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsNovedadRemesa As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsNovedadRemesa.Fill(dsReportes, "dtsListadoNovedadesRemesasPaqueteria")

            'Pasar el dataset a un datatable
            Dim dtbNovedadRemesa As New DataTable
            dtbNovedadRemesa = dsReportes.Tables("dtsListadoNovedadesRemesasPaqueteria")

            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsListadoNovedadesRemesasPaqueteria", CType(dtbNovedadRemesa, DataTable)))
            rpwDocumento.LocalReport.Refresh()

            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If


        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_ListadoRemesasPaqueteria :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Private Sub Configurar_Manifiesto()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_MANIFIESTO_TRANSPORTADOR Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/Manifiesto/lstManifiestoConductor.rdlc")
                End If

                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_MANIFIESTO_OFICINA Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/Manifiesto/lstManifiestoOficina.rdlc")
                End If
            Else
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_MANIFIESTO_TRANSPORTADOR Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/Manifiesto/lstManifiestoConductorxls.rdlc")
                End If

                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_MANIFIESTO_OFICINA Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/Manifiesto/lstManifiestoOficinaxls.rdlc")
                End If
            End If


            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Listado_Manifiesto "


            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Identificacion_Cliente")) Then
                Dim Identificacion_Cliente As String = Request.QueryString("Identificacion_Cliente")
                strConsulta += "'" & Identificacion_Cliente & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "'" & Codigo_Oficina & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim Estado As String = Request.QueryString("Estado")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Ruta")) Then
                Dim Ruta As String = Request.QueryString("Ruta")
                strConsulta += "'" & Ruta & "'"
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsManifiesto As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsManifiesto.Fill(dsReportes, "dtsManifiesto")

            'Pasar el dataset a un datatable
            Dim dtbManifiesto As New DataTable
            dtbManifiesto = dsReportes.Tables("dtsManifiesto")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsManifiesto", CType(dtbManifiesto, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Private Sub Configurar_Orden_Cargue()

        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_ORDEN_CARGUE_CLIENTE Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/OrdenCargue/lstOrdenCargueCliente.rdlc")
                End If

                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_ORDEN_CARGUE_OFICINA Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/OrdenCargue/lstOrdenCargueOficina.rdlc")

                End If
            Else
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_ORDEN_CARGUE_CLIENTE Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/OrdenCargue/lstOrdenCargueClientexls.rdlc")
                End If

                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_ORDEN_CARGUE_OFICINA Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/OrdenCargue/lstOrdenCargueOficinaxls.rdlc")

                End If
            End If



            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Listado_OrdenCargue "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Identificacion_Cliente")) Then
                Dim Identificacion_Cliente As String = Request.QueryString("Identificacion_Cliente")
                strConsulta += "'" & Identificacion_Cliente & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "'" & Codigo_Oficina & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim Estado As String = Request.QueryString("Estado")
                If Request.QueryString("Estado") = 2 Then
                    strConsulta += "NULL,"
                    Dim Anulado = "1"
                    strConsulta += "'" & Anulado & "',"
                Else
                    strConsulta += "'" & Estado & "',"
                End If
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Ruta")) Then
                Dim Ruta As String = Request.QueryString("Ruta")
                strConsulta += "'" & Ruta & "'"
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsOrdenCargue As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsOrdenCargue.Fill(dsReportes, "dtsOrdenCargue")

            'Pasar el dataset a un datatable
            Dim dtbOrdenCargue As New DataTable
            dtbOrdenCargue = dsReportes.Tables("dtsOrdenCargue")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsOrdenCargue", CType(dtbOrdenCargue, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Guias_por_Cliente :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Private Sub Configurar_Planilla_Despacho()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then

                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PLANILLA_DESPACHO_TRANSPORTADOR Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/PlanillasDespachos/lstPlanillaDespachosTransportador.rdlc")
                End If

                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PLANILLA_DESPACHO_OFICINA Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/PlanillasDespachos/lstPlanillaDespachosOficina.rdlc")
                End If
            Else

                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PLANILLA_DESPACHO_TRANSPORTADOR Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/PlanillasDespachos/lstPlanillaDespachosTransportadorxls.rdlc")
                End If

                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PLANILLA_DESPACHO_OFICINA Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/PlanillasDespachos/lstPlanillaDespachosOficinaxls.rdlc")
                End If
            End If

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Listado_Planilla_Despachos  "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Identificacion_Cliente")) Then
                Dim Identificacion_Cliente As String = Request.QueryString("Identificacion_Cliente")
                strConsulta += "'" & Identificacion_Cliente & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "'" & Codigo_Oficina & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim Estado As String = Request.QueryString("Estado")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Ruta")) Then
                Dim Ruta As String = Request.QueryString("Ruta")
                strConsulta += "'" & Ruta & "'"
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsGuias As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsGuias.Fill(dsReportes, "dtsPlanillaDespachos")

            'Pasar el dataset a un datatable
            Dim dtbGuias As New DataTable
            dtbGuias = dsReportes.Tables("dtsPlanillaDespachos")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsPlanillaDespachos", CType(dtbGuias, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub
    Private Sub Configurar_Planeacion_Cumplimiento()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet

            strPathRepo = Server.MapPath("Formatos/Despachos/Progrmacion/lstPlaneacionCumplimiento.rdlc")

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_listado_planeacion_cumplimiento  "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Cliente")) Then
                strConsulta += "'" & Request.QueryString("Cliente") & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Destinatario")) Then
                strConsulta += "'" & Request.QueryString("Destinatario") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Destino")) Then
                strConsulta += "'" & Request.QueryString("Destino") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("EstadoPlaneacion")) Then
                strConsulta += Request.QueryString("EstadoPlaneacion")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsGuias As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsGuias.Fill(dsReportes, "dtsPlaneacionCumplimiento")

            'Pasar el dataset a un datatable
            Dim dtbGuias As New DataTable
            dtbGuias = dsReportes.Tables("dtsPlaneacionCumplimiento")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsPlaneacionCumplimiento", CType(dtbGuias, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "Listado Planeación", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.Close()
                'Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Private Sub Configurar_Liquidacion_Planilla_Despacho()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet


            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_LIQUIDACION_TRANSPORTADOR Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/Liquidacion/lstLiquidacionPlanillasDeschadosPorTransportador.rdlc")
                End If

                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_LIQUIDACION_OFICINA Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/Liquidacion/lstLiquidacionPlanillasDeschadosPorOficina.rdlc")
                End If
            Else
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_LIQUIDACION_TRANSPORTADOR Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/Liquidacion/lstLiquidacionPlanillasDeschadosPorTransportadorxls.rdlc")
                End If

                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_LIQUIDACION_OFICINA Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/Liquidacion/lstLiquidacionPlanillasDeschadosPorOficinaxls.rdlc")
                End If
            End If
            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC  gsp_Listado_Liquidacion  "


            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Identificacion_Cliente")) Then
                Dim Identificacion_Cliente As String = Request.QueryString("Identificacion_Cliente")
                strConsulta += "'" & Identificacion_Cliente & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "'" & Codigo_Oficina & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim Estado As String = Request.QueryString("Estado")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Ruta")) Then
                Dim Ruta As String = Request.QueryString("Ruta")
                strConsulta += "'" & Ruta & "'"
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsLiquidacion As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsLiquidacion.Fill(dsReportes, "dtsLiquidacion")

            'Pasar el dataset a un datatable
            Dim dtbLiquidacion As New DataTable
            dtbLiquidacion = dsReportes.Tables("dtsLiquidacion")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsLiquidacion", CType(dtbLiquidacion, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub
    Private Sub Configurar_Informe_Liquidacion_Planilla_Despachos()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            strPathRepo = Server.MapPath("Formatos/Despachos/Liquidacion/lstInformeLiquidacione.rdlc")

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC  gsp_Listado_Informe_Liquidacion "


            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Identificacion_Cliente")) Then
                Dim Identificacion_Cliente As String = Request.QueryString("Identificacion_Cliente")
                strConsulta += "'" & Identificacion_Cliente & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "'" & Codigo_Oficina & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("EstadoLiquidaciones")) Then
                Dim Estado As String = Request.QueryString("EstadoLiquidaciones")
                If Estado > 0 Then
                    strConsulta += "'" & Estado & "',"
                End If
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Ruta")) Then
                Dim Ruta As String = Request.QueryString("Ruta")
                strConsulta += "'" & Ruta & "'"
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsLiquida As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)

            dtsLiquida.Fill(dsReportes, "dtsLiquida")



            'Pasar el dataset a un datatable
            Dim dtbLiquida As New DataTable

            dtbLiquida = dsReportes.Tables("dtsLiquida")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)
            rpwDocumento.LocalReport.DataSources.Clear()
            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsLiquida", CType(dtbLiquida, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Private Sub Configurar_Cumplido_Planilla_Despacho()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_CUMPLIDOS_TRANSPORTADOR Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/Cumplidos/lstCumplidosPlanillasDeschadosPorTransportador.rdlc")
                End If

                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_CUMPLIDOS_OFICINA Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/Cumplidos/lstCumplidosPlanillasDeschadosPorOficina.rdlc")
                End If
            Else
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_CUMPLIDOS_TRANSPORTADOR Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/Cumplidos/lstCumplidosPlanillasDeschadosPorTransportadorxls.rdlc")
                End If

                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_CUMPLIDOS_OFICINA Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/Cumplidos/lstCumplidosPlanillasDeschadosPorOficinaxls.rdlc")
                End If
            End If

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Listado_Cumplido_Planilla  "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Identificacion_Cliente")) Then
                Dim Identificacion_Cliente As String = Request.QueryString("Identificacion_Cliente")
                strConsulta += "'" & Identificacion_Cliente & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "'" & Codigo_Oficina & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim Estado As String = Request.QueryString("Estado")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Ruta")) Then
                Dim Ruta As String = Request.QueryString("Ruta")
                strConsulta += "'" & Ruta & "'"
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsListado As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsListado.Fill(dsReportes, "dtsListadoCumplidoPlanillaDespacho")

            'Pasar el dataset a un datatable
            Dim dtbListado As New DataTable
            dtbListado = dsReportes.Tables("dtsListadoCumplidoPlanillaDespacho")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsListadoCumplidoPlanillaDespacho", CType(dtbListado, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Private Sub Configurar_Remesa()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet

            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_REMESA_CLIENTE Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/Remesa/lstRemesaCliente.rdlc")
                End If

                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_REMESA_OFICINA Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/Remesa/lstRemesaOficina.rdlc")

                End If
            Else
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_REMESA_CLIENTE Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/Remesa/lstRemesaClientexls.rdlc")
                End If

                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_REMESA_OFICINA Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/Remesa/lstRemesaOficinaxls.rdlc")

                End If
            End If

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Listado_Remesa "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Identificacion_Cliente")) Then
                Dim Identificacion_Cliente As String = Request.QueryString("Identificacion_Cliente")
                strConsulta += "'" & Identificacion_Cliente & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "'" & Codigo_Oficina & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim Estado As String = Request.QueryString("Estado")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Ruta")) Then
                Dim Ruta As String = Request.QueryString("Ruta")
                strConsulta += "'" & Ruta & "'"
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsRemesas As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsRemesas.Fill(dsReportes, "dtsRemesas")

            'Pasar el dataset a un datatable
            Dim dtbRemesas As New DataTable
            dtbRemesas = dsReportes.Tables("dtsRemesas")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsRemesas", CType(dtbRemesas, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Guias_por_Cliente :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Private Sub Configurar_Novedades_Distribucion_Remesas()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_NOVEDADES_REMESAS Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/Remesa/lstNovedadesRemesas.rdlc")
                End If
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_DISTRIBUCION_REMESAS Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/Remesa/lstDistribucionRemesas.rdlc")
                End If
            Else
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_NOVEDADES_REMESAS Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/Remesa/lstNovedadesRemesasxls.rdlc")
                End If
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_DISTRIBUCION_REMESAS Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/Remesa/lstDistribucionRemesasxls.rdlc")
                End If
            End If


            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_NOVEDADES_REMESAS Then
                'Armar la consulta
                strConsulta = "EXEC gsp_Listado_Novedades_Remesas "
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_DISTRIBUCION_REMESAS Then
                'Armar la consulta
                strConsulta = "EXEC gsp_Listado_Distriucion_Remesas "
            End If

            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Identificacion_Cliente")) Then
                Dim Identificacion_Cliente As String = Request.QueryString("Identificacion_Cliente")
                strConsulta += "'" & Identificacion_Cliente & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "'" & Codigo_Oficina & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim Estado As String = Request.QueryString("Estado")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Ruta")) Then
                Dim Ruta As String = Request.QueryString("Ruta")
                strConsulta += "'" & Ruta & "'"
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsRemesas As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsRemesas.Fill(dsReportes, "dtsNovedadesDistribucionRemesas")

            'Pasar el dataset a un datatable
            Dim dtbRemesas As New DataTable
            dtbRemesas = dsReportes.Tables("dtsNovedadesDistribucionRemesas")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsNovedadesDistribucionRemesas", CType(dtbRemesas, DataTable)))
            rpwDocumento.LocalReport.Refresh()

            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing

                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Novedades_Distribucion_Remesas :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Private Sub Configurar_Remesas_Pendientes_Facturar()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_REMESA_PENDIENTES_FACTURAR_CLIENTE Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/Remesa/lstRemesaPendientesFacturarCliente.rdlc")
                End If

                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_REMESA_PENDIENTES_FACTURAR_OFICINA Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/Remesa/lstRemesaPendientesFacturarOficina.rdlc")
                End If
            Else
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_REMESA_PENDIENTES_FACTURAR_CLIENTE Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/Remesa/lstRemesaPendientesFacturarClientexls.rdlc")
                End If

                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_REMESA_PENDIENTES_FACTURAR_OFICINA Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/Remesa/lstRemesaPendientesFacturarOficinaxls.rdlc")
                End If
            End If


            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC  gsp_Listado_Remesa_pendientes_facturar  "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Identificacion_Cliente")) Then
                Dim Identificacion_Cliente As String = Request.QueryString("Identificacion_Cliente")
                strConsulta += "'" & Identificacion_Cliente & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "'" & Codigo_Oficina & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim Estado As String = Request.QueryString("Estado")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Ruta")) Then
                Dim Ruta As String = Request.QueryString("Ruta")
                strConsulta += "'" & Ruta & "'"
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtslistadoremesaspendientesfacturar As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtslistadoremesaspendientesfacturar.Fill(dsReportes, "dtslistadoremesaspendientesfacturar")

            'Pasar el dataset a un datatable
            Dim dtblistadoremesaspendientesfacturar As New DataTable
            dtblistadoremesaspendientesfacturar = dsReportes.Tables("dtslistadoremesaspendientesfacturar")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtslistadoremesaspendientesfacturar", CType(dtblistadoremesaspendientesfacturar, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Private Sub Configurar_Remesas_Generales_Por_Tipo()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            strPathRepo = Server.MapPath("Formatos/Despachos/Remesa/lstRemesasGeneralesTipo.rdlc")
            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_obtener_encabezados_listados "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Incial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "'"
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsObtenerEncabezadoListados")
            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dtsObtenerEncabezadoListados")


            'Armar la consulta
            strConsulta = "EXEC  gsp_Listado_Remesas_Generales_Por_Tipo  "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Identificacion_Cliente")) Then
                Dim Identificacion_Cliente As String = Request.QueryString("Identificacion_Cliente")
                strConsulta += "'" & Identificacion_Cliente & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "'" & Codigo_Oficina & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Vehiculo")) Then
                Dim Vehiculo As String = Request.QueryString("Vehiculo")
                strConsulta += "'" & Vehiculo & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim Estado As String = Request.QueryString("Estado")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Ruta")) Then
                Dim Ruta As String = Request.QueryString("Ruta")
                strConsulta += "'" & Ruta & "'"
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsRmesas As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsRmesas.Fill(dsReportes, "dtsRemesasGeneralesPorTipo")

            'Pasar el dataset a un datatable
            Dim dtbRemesas As New DataTable
            dtbRemesas = dsReportes.Tables("dtsRemesasGeneralesPorTipo")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsRemesasGeneralesPorTipo", CType(dtbRemesas, DataTable)))
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsObtenerEncabezadoListados", CType(dtb, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Private Sub Configurar_Remesas_Pendientes_Cumplir()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_REMESA_PENDIENTES_CUMPLIR_CLIENTE Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/Remesa/lstRemesaPendientesCumplirCliente.rdlc")
                End If

                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_REMESA_PENDIENTES_CUMPLIR_OFICINA Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/Remesa/lstRemesaPendientesCumplirOficina.rdlc")
                End If
            Else
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_REMESA_PENDIENTES_CUMPLIR_CLIENTE Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/Remesa/lstRemesaPendientesCumplirClientexls.rdlc")
                End If

                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_REMESA_PENDIENTES_CUMPLIR_OFICINA Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/Remesa/lstRemesaPendientesCumplirOficinaxls.rdlc")
                End If
            End If


            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Listado_Remesa_pendientes_Cumplir   "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Identificacion_Cliente")) Then
                Dim Identificacion_Cliente As String = Request.QueryString("Identificacion_Cliente")
                strConsulta += "'" & Identificacion_Cliente & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "'" & Codigo_Oficina & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim Estado As String = Request.QueryString("Estado")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Ruta")) Then
                Dim Ruta As String = Request.QueryString("Ruta")
                strConsulta += "'" & Ruta & "'"
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtslistadoremesaspendientescumplir As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtslistadoremesaspendientescumplir.Fill(dsReportes, "dtslistadoremesaspendientescumplir")

            'Pasar el dataset a un datatable
            Dim dtblistadoremesaspendientescumplir As New DataTable
            dtblistadoremesaspendientescumplir = dsReportes.Tables("dtslistadoremesaspendientescumplir")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtslistadoremesaspendientescumplir", CType(dtblistadoremesaspendientescumplir, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Private Sub Configurar_Comprobante_Contable()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet


            If Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_COMPROBANTE_CONTABLE_DETALLADO Then
                strPathRepo = Server.MapPath("Formatos//Contabilidad/lstComprobantesContableDetallado.rdlc")
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_COMPROBANTE_CONTABLE_RESUMIDO Then
                strPathRepo = Server.MapPath("Formatos/Contabilidad/lstComprobantesContableResumido.rdlc")

            End If
            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Litado_Comprobantes_Contable "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Identificacion_Cliente")) Then
                Dim Identificacion_Cliente As String = Request.QueryString("Identificacion_Cliente")
                strConsulta += "'" & Identificacion_Cliente & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "'" & Codigo_Oficina & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim Estado As String = Request.QueryString("Estado")
                strConsulta += "'" & Estado & "'"
            Else
                strConsulta += "NULL"
            End If


            'Guardar el resultado del sp en un dataset
            Dim dtsComprobanteContable As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsComprobanteContable.Fill(dsReportes, "dtsComprobanteContable")

            'Pasar el dataset a un datatable
            Dim dtbComprobanteContable As New DataTable
            dtbComprobanteContable = dsReportes.Tables("dtsComprobanteContable")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsComprobanteContable", CType(dtbComprobanteContable, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Guias_por_Cliente :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Public Sub Configurar_Listado_Orden_Servicio()
        Dim strConsulta As String = ""
        Dim strPathRepo As String
        Try
            'Definir variables
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_SOLICITUD_SERVICIOS_CLIENTE Then
                    strPathRepo = Server.MapPath("Formatos/ServicioCliente/lstSolicitudCliente.rdlc")
                End If
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_SOLICITUD_SERVICIOS_OFICINA Then
                    strPathRepo = Server.MapPath("Formatos/ServicioCliente/lstSolicitudOficina.rdlc")
                End If
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_ORDEN_SERVICIOS_CLIENTE Then
                    strPathRepo = Server.MapPath("Formatos/ServicioCliente/lstOrdenCliente.rdlc")
                End If
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_ORDEN_SERVICIOS_OFICINA Then
                    strPathRepo = Server.MapPath("Formatos/ServicioCliente/lstOrdenOficina.rdlc")
                End If
            Else
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_SOLICITUD_SERVICIOS_CLIENTE Then
                    strPathRepo = Server.MapPath("Formatos/ServicioCliente/lstSolicitudClientexls.rdlc")
                End If
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_SOLICITUD_SERVICIOS_OFICINA Then
                    strPathRepo = Server.MapPath("Formatos/ServicioCliente/lstSolicitudOficinaxls.rdlc")
                End If
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_ORDEN_SERVICIOS_CLIENTE Then
                    strPathRepo = Server.MapPath("Formatos/ServicioCliente/lstOrdenClientexls.rdlc")
                End If
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_ORDEN_SERVICIOS_OFICINA Then
                    strPathRepo = Server.MapPath("Formatos/ServicioCliente/lstOrdenOficinaxls.rdlc")
                End If
            End If



            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Listado_Ordenes_y_Solicitudes_servicio "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Linea_Negocio")) Then
                Dim Estado As String = Request.QueryString("Linea_Negocio")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Tipo_Documento")) Then
                Dim Estado As String = Request.QueryString("Tipo_Documento")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Estado_Documento")) Then
                Dim Estado As String = Request.QueryString("Estado_Documento")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado_Proceso")) Then
                Dim Estado As String = Request.QueryString("Estado_Proceso")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Identificacion_Cliente")) Then
                Dim Identificacion_Cliente As String = Request.QueryString("Identificacion_Cliente")
                strConsulta += "'" & Identificacion_Cliente & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "'" & Codigo_Oficina & "'"
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsSolicitudOrdenServicio As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsSolicitudOrdenServicio.Fill(dsReportes, "dtsSolicitudOrdenServicio")

            'Pasar el dataset a un datatable
            Dim dtbSolicitudOrdenServicio As New DataTable
            dtbSolicitudOrdenServicio = dsReportes.Tables("dtsSolicitudOrdenServicio")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = strPathRepo

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsSolicitudOrdenServicio", CType(dtbSolicitudOrdenServicio, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Rutas :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try


    End Sub
    Public Sub Configurar_Listado_Informe_Pedidos()
        Dim strConsulta As String = ""
        Dim strPathRepo As String
        Try
            'Definir variables
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                strPathRepo = Server.MapPath("Formatos/ServicioCliente/lstInformePedidos.rdlc")

            Else
                strPathRepo = Server.MapPath("Formatos/ServicioCliente/lstInformePedidosxls.rdlc")

            End If

            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Listado_Informe_Pedidos "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Linea_Negocio")) Then
                Dim Estado As String = Request.QueryString("Linea_Negocio")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Tipo_Documento")) Then
                Dim Estado As String = Request.QueryString("Tipo_Documento")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Estado_Documento")) Then
                Dim Estado As String = Request.QueryString("Estado_Documento")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado_Proceso")) Then
                Dim Estado As String = Request.QueryString("Estado_Proceso")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Identificacion_Cliente")) Then
                Dim Identificacion_Cliente As String = Request.QueryString("Identificacion_Cliente")
                strConsulta += "'" & Identificacion_Cliente & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "'" & Codigo_Oficina & "'"
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsSolicitudOrdenServicio As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsSolicitudOrdenServicio.Fill(dsReportes, "dtsInformePedidos")

            'Pasar el dataset a un datatable
            Dim dtbSolicitudOrdenServicio As New DataTable
            dtbSolicitudOrdenServicio = dsReportes.Tables("dtsInformePedidos")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = strPathRepo

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsInformePedidos", CType(dtbSolicitudOrdenServicio, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "Informe_Pedidos", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Rutas :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try


    End Sub
    '//////////////legalizacion de guias ////////
    Public Sub Configurar_GL_Documentos_Cartera()
        Dim strConsulta As String = ""
        Dim strPathRepo As String
        Try
            'Definir variables
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                strPathRepo = Server.MapPath("Formatos/ServicioCliente/lstInformeDocuCartera.rdlc")

            Else
                strPathRepo = Server.MapPath("Formatos/ServicioCliente/lstInformeDocuCarterasxls.rdlc")

            End If

            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Listado_Informe_Pedidos "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Linea_Negocio")) Then
                Dim Estado As String = Request.QueryString("Linea_Negocio")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Tipo_Documento")) Then
                Dim Estado As String = Request.QueryString("Tipo_Documento")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Estado_Documento")) Then
                Dim Estado As String = Request.QueryString("Estado_Documento")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado_Proceso")) Then
                Dim Estado As String = Request.QueryString("Estado_Proceso")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Identificacion_Cliente")) Then
                Dim Identificacion_Cliente As String = Request.QueryString("Identificacion_Cliente")
                strConsulta += "'" & Identificacion_Cliente & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "'" & Codigo_Oficina & "'"
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsSolicitudOrdenServicio As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsSolicitudOrdenServicio.Fill(dsReportes, "dtsInformeDocuCartera")

            'Pasar el dataset a un datatable
            Dim dtbSolicitudOrdenServicio As New DataTable
            dtbSolicitudOrdenServicio = dsReportes.Tables("dtsInformeDocuCartera")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = strPathRepo

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsInformeDocuCartera", CType(dtbSolicitudOrdenServicio, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "Informe_Documentos_Cartera", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Rutas :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try


    End Sub
    Public Sub Configurar_GL_Recaudo_Contra_Entrega()
        Dim strConsulta As String = ""
        Dim strPathRepo As String
        Try
            'Definir variables
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                strPathRepo = Server.MapPath("Formatos/ServicioCliente/lstInformeRecaudoContraEntrega.rdlc")

            Else
                strPathRepo = Server.MapPath("Formatos/ServicioCliente/lstInformeRecaudoContraEntregasxls.rdlc")

            End If

            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Listado_Informe_Pedidos "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Linea_Negocio")) Then
                Dim Estado As String = Request.QueryString("Linea_Negocio")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Tipo_Documento")) Then
                Dim Estado As String = Request.QueryString("Tipo_Documento")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Estado_Documento")) Then
                Dim Estado As String = Request.QueryString("Estado_Documento")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado_Proceso")) Then
                Dim Estado As String = Request.QueryString("Estado_Proceso")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Identificacion_Cliente")) Then
                Dim Identificacion_Cliente As String = Request.QueryString("Identificacion_Cliente")
                strConsulta += "'" & Identificacion_Cliente & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "'" & Codigo_Oficina & "'"
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsSolicitudOrdenServicio As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsSolicitudOrdenServicio.Fill(dsReportes, "dtsInformeRecaudoContraEntrega")

            'Pasar el dataset a un datatable
            Dim dtbSolicitudOrdenServicio As New DataTable
            dtbSolicitudOrdenServicio = dsReportes.Tables("dtsInformeRecaudoContraEntrega")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = strPathRepo

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsInformeRecaudoContraEntrega", CType(dtbSolicitudOrdenServicio, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "Informe_Recaudo_Contra_Entrega_Contado", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Rutas :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try


    End Sub
    Public Sub Configurar_GL_Documentos_archivos()
        Dim strConsulta As String = ""
        Dim strPathRepo As String
        Try
            'Definir variables
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                strPathRepo = Server.MapPath("Formatos/ServicioCliente/lstInformeDocuArchivos.rdlc")

            Else
                strPathRepo = Server.MapPath("Formatos/ServicioCliente/lstInformeDocuArchivossxls.rdlc")

            End If

            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Listado_Informe_Pedidos "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Linea_Negocio")) Then
                Dim Estado As String = Request.QueryString("Linea_Negocio")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Tipo_Documento")) Then
                Dim Estado As String = Request.QueryString("Tipo_Documento")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Estado_Documento")) Then
                Dim Estado As String = Request.QueryString("Estado_Documento")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado_Proceso")) Then
                Dim Estado As String = Request.QueryString("Estado_Proceso")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Identificacion_Cliente")) Then
                Dim Identificacion_Cliente As String = Request.QueryString("Identificacion_Cliente")
                strConsulta += "'" & Identificacion_Cliente & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "'" & Codigo_Oficina & "'"
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsSolicitudOrdenServicio As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsSolicitudOrdenServicio.Fill(dsReportes, "dtsInformeDocuArchivos")

            'Pasar el dataset a un datatable
            Dim dtbSolicitudOrdenServicio As New DataTable
            dtbSolicitudOrdenServicio = dsReportes.Tables("dtsInformeDocuArchivos")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = strPathRepo

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsInformeDocuArchivos", CType(dtbSolicitudOrdenServicio, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "Reporte_Entrega_Documentos_Archivo", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Rutas :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try


    End Sub

    Public Sub Configurar_Listado_Informe_Detalle_Programaciones()
        Dim strConsulta As String = ""
        Dim strPathRepo As String
        Try
            'Definir variables

            strPathRepo = Server.MapPath("Formatos/ServicioCliente/lstInformeDetalleProgramaciones.rdlc")

            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_listado_detalle_programacion_orden_servicios "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Identificacion_Cliente")) Then
                Dim Identificacion_Cliente As String = Request.QueryString("Identificacion_Cliente")
                If Identificacion_Cliente <> "" Then
                    strConsulta += "'" & Identificacion_Cliente & "',"
                Else
                    strConsulta += "NULL,"
                End If
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Linea_Negocio")) Then
                Dim Estado As String = Request.QueryString("Linea_Negocio")
                If Estado <> 0 And Estado <> "0" Then
                    strConsulta += "'" & Estado & "',"
                Else
                    strConsulta += "NULL,"
                End If
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado_Proceso")) Then
                Dim Estado As String = Request.QueryString("Estado_Proceso")
                If Estado <> 0 And Estado <> "0" Then
                    strConsulta += "'" & Estado & "',"
                Else
                    strConsulta += "NULL,"
                End If
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado_Documento")) Then
                Dim Estado As String = Request.QueryString("Estado_Documento")
                If Estado <> -1 And Estado <> "-1" Then
                    strConsulta += "'" & Estado & "',"
                Else
                    strConsulta += "NULL,"
                End If
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("USUA_Imprime")) Then
                Dim Usua As String = Request.QueryString("USUA_Imprime")
                strConsulta += "'" & Usua & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                If Codigo_Oficina <> 0 And Codigo_Oficina <> "0" Then
                    strConsulta += "'" & Codigo_Oficina & "'"
                Else
                    strConsulta += "NULL"
                End If
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsSolicitudOrdenServicio As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsSolicitudOrdenServicio.Fill(dsReportes, "dtsInformeDetalleProgramaciones")

            'Pasar el dataset a un datatable
            Dim dtbSolicitudOrdenServicio As New DataTable
            dtbSolicitudOrdenServicio = dsReportes.Tables("dtsInformeDetalleProgramaciones")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = strPathRepo

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsInformeDetalleProgramaciones", CType(dtbSolicitudOrdenServicio, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "Informe_Pedidos", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Informe_Detalle_Programaciones :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try


    End Sub

    Public Sub Configurar_Listado_Precinto_Despacho_Oficina()
        Dim strConsulta As String = ""
        Dim strPathRepo As String = ""
        Try
            'Definir variables
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PRECINTO_DESPACHO_OFICINA Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/lstPrecintosDespachoOfic.rdlc")
                End If
            Else
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_PRECINTO_DESPACHO_OFICINA Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/lstPrecintosDespachoOficxls.rdlc")
                End If
            End If
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_listado_precinto_despacho_oficinas "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim EstadoReg As String = Request.QueryString("Estado")
                strConsulta &= EstadoReg & ","
            Else
                strConsulta &= "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "" & Codigo_Oficina & ""
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsPrecintos As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsPrecintos.Fill(dsReportes, "dtsPrecintos")

            'Pasar el dataset a un datatable
            Dim dtbPrecintos As New DataTable
            dtbPrecintos = dsReportes.Tables("dtsPrecintos")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = strPathRepo

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsPrecintos", CType(dtbPrecintos, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Precinto_Despacho_Oficina :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try


    End Sub

    Public Sub Configurar_Listado_Remesas_Pendientes_tiempos()
        Dim strConsulta As String = ""
        Dim strPathRepo As String = ""
        Try
            'Definir variables
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_REMESAS_PENDIENTES_TIEMPOS Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/Remesa/lstRemesasPendientesTiempos.rdlc")
                End If
            Else
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_REMESAS_PENDIENTES_TIEMPOS Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/Remesa/lstRemesasPendientesTiemposxls.rdlc")
                End If
            End If


            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta # 1
            strConsulta = "EXEC gsp_listado_remesas_pendientes_asignar_tiempos "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "'" & Codigo_Oficina & "'"
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsRemesasPendientesAsignarTiempos As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsRemesasPendientesAsignarTiempos.Fill(dsReportes, "dtsRemesasPendientesTiemposRemesas")

            'Pasar el dataset a un datatable
            Dim dtbRemesasPendientesAsignarTiempos As New DataTable
            dtbRemesasPendientesAsignarTiempos = dsReportes.Tables("dtsRemesasPendientesTiemposRemesas")

            'Armar la consulta #2
            strConsulta = "EXEC gsp_listado_planillas_pendientes_asignar_tiempos "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "'" & Codigo_Oficina & "'"
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsPlanillasPendientesAsignarTiempos As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsPlanillasPendientesAsignarTiempos.Fill(dsReportes, "dtsRemesasPendientesTiemposPlanilla")

            'Pasar el dataset a un datatable
            Dim dtbPlanillasPendientesAsignarTiempos As New DataTable
            dtbPlanillasPendientesAsignarTiempos = dsReportes.Tables("dtsRemesasPendientesTiemposPlanilla")

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim RRAT As New ReportDataSource("dtsRemesasPendientesTiemposRemesas", dsReportes.Tables(0))
            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim RPAT As New ReportDataSource("dtsRemesasPendientesTiemposPlanilla", dsReportes.Tables(1))

            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = strPathRepo

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            'rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsRemesasPendientesAsignarTiempo", CType(dtbRemesasPlanillasPendientesAsignarTiempos, DataTable)))
            'rpwDocumento.LocalReport.Refresh()
            rpwDocumento.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            rpwDocumento.LocalReport.DataSources.Add(RRAT)
            rpwDocumento.LocalReport.DataSources.Add(RPAT)
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Remesas_Pendientes_tiempos :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try
    End Sub
    Public Sub Configurar_Listado_Rechazos_Enturnamiento()
        Dim strConsulta As String = ""
        Dim strPathRepo As String = ""
        Try
            'Definir variables
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_RECHAZOS_ENTURNAMIENTOS Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/lstRechazosEnturnamientos.rdlc")
                End If
            Else
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_RECHAZOS_ENTURNAMIENTOS Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/lstRechazosEnturnamientosxls.rdlc")
                End If
            End If


            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath


            'Armar la consulta
            strConsulta = "EXEC  gsp_listado_Rechazos_Enturnamientos "


            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Fecha_Inicial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Inicial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Vehiculo")) Then
                Dim Vehiculo As String = Request.QueryString("Vehiculo")
                strConsulta += Vehiculo
            Else
                strConsulta += "NULL"
            End If


            'Guardar el resultado del sp en un dataset
            Dim dtsEnturnamiento As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsEnturnamiento.Fill(dsReportes, "dtsListadoEnturnamiento")

            'Pasar el dataset a un datatable
            Dim dtbEnturnamiento As New DataTable
            dtbEnturnamiento = dsReportes.Tables("dtsListadoEnturnamiento")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsListadoEnturnamiento", CType(dtbEnturnamiento, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try
    End Sub

    Public Sub Configurar_Listado_Informe_Seguros()
        Dim strConsulta As String = ""
        Dim strPathRepo As String = ""
        Try
            'Definir variables
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_INFORME_SEGUROS Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/PlanillasDespachos/lstInformeSeguros.rdlc")
                End If
            Else
                If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_INFORME_SEGUROS Then
                    strPathRepo = Server.MapPath("Formatos/Despachos/PlanillasDespachos/lstInformeSegurossxls.rdlc")
                End If
            End If


            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath


            'Armar la consulta
            strConsulta = "EXEC  gsp_Listados_Seguros "


            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Fecha_Inicial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Inicial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "'"
            Else
                strConsulta += "NULL"
            End If


            'Guardar el resultado del sp en un dataset
            Dim dtsInformeSeguro As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsInformeSeguro.Fill(dsReportes, "dtsInformeSeguros")

            'Pasar el dataset a un datatable
            Dim dtbInformeSeguro As New DataTable
            dtbInformeSeguro = dsReportes.Tables("dtsInformeSeguros")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsInformeSeguros", CType(dtbInformeSeguro, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try
    End Sub
    Public Sub Configurar_Cartera()
        Dim strConsulta As String = ""
        Dim strPathRepo As String
        Try

            If Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_CARTERA_DETALLADO Then
                strPathRepo = Server.MapPath("Formatos/Facturacion/lstCarteraDetallado.rdlc")
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_CARTERA_RESUMIDO Then
                strPathRepo = Server.MapPath("Formatos/Facturacion/lstCarteraResumido.rdlc")
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LITADO_CARTERA_SALDO_DETALLADO Then
                strPathRepo = Server.MapPath("Formatos/Facturacion/lstSaldoDetallado.rdlc")
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_CARTERA_SALDO_RESUMIDO Then
                strPathRepo = Server.MapPath("Formatos/Facturacion/lstSaldoResumido.rdlc")
            End If


            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC  "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                Dim Numero_Documento_Inicial As String = Request.QueryString("Numero_Documento_Inicial")
                strConsulta += "'" & Numero_Documento_Inicial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                Dim Numero_Documento_Final As String = Request.QueryString("Numero_Documento_Final")
                strConsulta += "'" & Numero_Documento_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Linea_Negocio")) Then
                Dim Estado As String = Request.QueryString("Linea_Negocio")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Tipo_Documento")) Then
                Dim Estado As String = Request.QueryString("Tipo_Documento")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Estado_Documento")) Then
                Dim Estado As String = Request.QueryString("Estado_Documento")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado_Proceso")) Then
                Dim Estado As String = Request.QueryString("Estado_Proceso")
                strConsulta += "'" & Estado & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Identificacion_Cliente")) Then
                Dim Identificacion_Cliente As String = Request.QueryString("Identificacion_Cliente")
                strConsulta += "'" & Identificacion_Cliente & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "'" & Codigo_Oficina & "'"
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsSolicitudOrdenServicio As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsSolicitudOrdenServicio.Fill(dsReportes, "dtsSolicitudOrdenServicio")

            'Pasar el dataset a un datatable
            Dim dtbSolicitudOrdenServicio As New DataTable
            dtbSolicitudOrdenServicio = dsReportes.Tables("dtsSolicitudOrdenServicio")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = strPathRepo

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsSolicitudOrdenServicio", CType(dtbSolicitudOrdenServicio, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Rutas :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try


    End Sub

    Private Sub Configurar_Informe_Fidelizacion()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_INFORME_FIDELIZACION Then
                strPathRepo = Server.MapPath("Formatos/Fidelizacion/lstInformeFidelizacion.rdlc")
            End If

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC  gsp_listado_informe_fidelizaciones "


            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Vehiculo")) Then
                Dim Vehiculo As String = Request.QueryString("Vehiculo")
                strConsulta += "'" & Vehiculo & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Inicial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Inicial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim Estado As String = Request.QueryString("Estado")
                strConsulta += Estado
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsFidelizacion As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsFidelizacion.Fill(dsReportes, "dtsFidelizacion")

            'Pasar el dataset a un datatable
            Dim dtbFidelizacion As New DataTable
            dtbFidelizacion = dsReportes.Tables("dtsFidelizacion")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsFidelizacion", CType(dtbFidelizacion, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Private Sub Configurar_Causal_Fidelizacion_Anulacion()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_FIDELIZACION_CAUSAL_ANULADAS Then
                strPathRepo = Server.MapPath("Formatos/Fidelizacion/lstFidelizacionCausalAnuladas.rdlc")
            End If

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC  gsp_listado_causales_fidelizaciones_anuladas "


            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Vehiculo")) Then
                Dim Vehiculo As String = Request.QueryString("Vehiculo")
                strConsulta += "'" & Vehiculo & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Inicial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Inicial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "'"
            Else
                strConsulta += "NULL"
            End If


            'Guardar el resultado del sp en un dataset
            Dim dtsFidelizacion As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsFidelizacion.Fill(dsReportes, "dtsFidelizacion")

            'Pasar el dataset a un datatable
            Dim dtbFidelizacion As New DataTable
            dtbFidelizacion = dsReportes.Tables("dtsFidelizacion")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsFidelizacion", CType(dtbFidelizacion, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub
    Public Sub Configurar_Listado_Impuestos()
        Dim strConsulta As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Basico/lstImpuestos.rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_listado_impuestos "

            strConsulta += Val(Request.QueryString("Empresa")) & ","

            If Not IsNothing(Request.QueryString("Nombre")) And Request.QueryString("Nombre") <> String.Empty Then
                strConsulta += "'" & Request.QueryString("Nombre") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Ciudad")) And Request.QueryString("Ciudad") <> String.Empty Then
                strConsulta += "'" & Request.QueryString("Ciudad") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Estado")) And Request.QueryString("Estado") <> String.Empty Then
                strConsulta += "" & Request.QueryString("Estado")
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsImpuestos As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsImpuestos.Fill(dsReportes, "dtsImpuestos")

            'Pasar el dataset a un datatable
            Dim dtbImpuestos As New DataTable
            dtbImpuestos = dsReportes.Tables("dtsImpuestos")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsImpuestos", CType(dtbImpuestos, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "Listado_Impuestos", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Terceros :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub
    Public Sub Configurar_Listado_Novedades_Despachos()
        Dim strConsulta As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Basico/lstNovedades.rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Listado_Novedades_Despachos "

            strConsulta += Val(Request.QueryString("Empresa")) & ","

            If Not IsNothing(Request.QueryString("Nombre")) And Request.QueryString("Nombre") <> String.Empty Then
                strConsulta += "'" & Request.QueryString("Nombre") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Estado")) And Request.QueryString("Estado") <> String.Empty Then
                strConsulta += "" & Request.QueryString("Estado")
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsNovedades")

            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dtsNovedades")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsNovedades", CType(dtb, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "Listado_Impuestos", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Terceros :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub
    Public Sub Configurar_Listado_Conceptos_Liquidacion()
        Dim strConsulta As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Basico/lstConceptosLiquidacion.rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Listado_Conceptos_Liquidacion "

            strConsulta += Val(Request.QueryString("Empresa")) & ","

            If Not IsNothing(Request.QueryString("Nombre")) And Request.QueryString("Nombre") <> String.Empty Then
                strConsulta += "'" & Request.QueryString("Nombre") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Estado")) And Request.QueryString("Estado") <> String.Empty Then
                strConsulta += "" & Request.QueryString("Estado")
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsConceptosLiquidacion")

            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dtsConceptosLiquidacion")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsConceptosLiquidacion", CType(dtb, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "Listado_Impuestos", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Terceros :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub
    Public Sub Configurar_Listado_Conceptos_Facturacion()
        Dim strConsulta As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Basico/lstConceptosFacturacion.rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Listado_Conceptos_Facturacion "

            strConsulta += Val(Request.QueryString("Empresa")) & ","

            If Not IsNothing(Request.QueryString("Nombre")) And Request.QueryString("Nombre") <> String.Empty Then
                strConsulta += "'" & Request.QueryString("Nombre") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Estado")) And Request.QueryString("Estado") <> String.Empty Then
                strConsulta += "" & Request.QueryString("Estado")
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsConceptosFacturacion")

            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dtsConceptosFacturacion")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsConceptosFacturacion", CType(dtb, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "Listado_Impuestos", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Terceros :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Public Sub Configurar_Listado_Usuarios()
        Dim strConsulta As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/SeguridadUsuarios/lstSeguridadUsuarios.rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Listado_SUsuarios "

            strConsulta += Val(Request.QueryString("Empresa")) & ","

            If Not IsNothing(Request.QueryString("Identificador")) Then
                strConsulta += "'" & Request.QueryString("Identificador") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Nombre")) Then
                strConsulta += "" & Request.QueryString("Nombre") & ","
            Else
                strConsulta += "NULL, "
            End If

            If Not IsNothing(Request.QueryString("Aplicacion")) Then
                strConsulta += "" & Request.QueryString("Aplicacion") & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("CuentaActiva")) Then
                If (Request.QueryString("CuentaActiva") <> -1 Or Request.QueryString("CuentaActiva") <> "-1") Then
                    strConsulta += "" & Request.QueryString("CuentaActiva") & ","
                Else
                    strConsulta += "NULL,"
                End If
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Grupo")) Then
                If (Request.QueryString("Grupo") <> -1 Or Request.QueryString("Grupo") <> "-1") Then
                    strConsulta += "" & Request.QueryString("Grupo")
                Else
                    strConsulta += "NULL"
                End If
            End If

            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsListadoSUsuarios")

            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dtsListadoSUsuarios")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsListadoSUsuarios", CType(dtb, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "Listado_Seguridad_Usuarios", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Usuarios :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Public Sub Configurar_Listado_Usuarios_por_Grupo()
        Dim strConsulta As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/SeguridadUsuarios/lstSeguridadUsuariosGrupo.rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Listado_SUsuarios "

            strConsulta += Val(Request.QueryString("Empresa")) & ","

            If Not IsNothing(Request.QueryString("Identificador")) Then
                strConsulta += "'" & Request.QueryString("Identificador") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Nombre")) Then
                strConsulta += "" & Request.QueryString("Nombre") & ","
            Else
                strConsulta += "NULL, "
            End If

            If Not IsNothing(Request.QueryString("Aplicacion")) Then
                strConsulta += "" & Request.QueryString("Aplicacion") & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("CuentaActiva")) Then
                If (Request.QueryString("CuentaActiva") <> -1 Or Request.QueryString("CuentaActiva") <> "-1") Then
                    strConsulta += "" & Request.QueryString("CuentaActiva") & ","
                Else
                    strConsulta += "NULL,"
                End If
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Grupo")) Then
                If (Request.QueryString("Grupo") <> -1 Or Request.QueryString("Grupo") <> "-1") Then
                    strConsulta += "" & Request.QueryString("Grupo")
                Else
                    strConsulta += "NULL"
                End If
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsListadoSUsuarios")

            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dtsListadoSUsuarios")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsListadoSUsuarios", CType(dtb, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "Listado_Seguridad_Usuarios", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Usuarios :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Public Sub Configurar_Listado_Grupos()
        Dim strConsulta As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/SeguridadUsuarios/lstSeguridadGrupos.rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Listado_Grupos "

            strConsulta += Val(Request.QueryString("Empresa")) & ","

            If Not IsNothing(Request.QueryString("Identificador")) Then
                strConsulta += "'" & Request.QueryString("Identificador") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Nombre")) Then
                strConsulta += "" & Request.QueryString("Nombre")
            Else
                strConsulta += "NULL"
            End If



            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsGrupos")

            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dtsGrupos")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsGrupos", CType(dtb, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "Listado_Grupos", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Grupos :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Private Sub Configurar_listado_gastos_conductor_varios_despachos_chip()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            strPathRepo = Server.MapPath("Formatos/FlotaPropia/LegalizacionGastos/lstGastosConductoresVariosDespachosPorChip.rdlc")
            'Armar la consulta
            strConsulta = "EXEC gsp_obtener_encabezados_listados "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Incial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "'"
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dstEncabezado")
            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dstEncabezado")

            strConsulta = "EXEC gsp_listado_gastos_conductor_varios_despachos_con_chip "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Incial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Numero_Documento")) Then
                strConsulta += Request.QueryString("Numero_Documento") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Conductor")) Then
                strConsulta += Request.QueryString("Conductor") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Vehiculo")) Then
                strConsulta += Request.QueryString("Vehiculo") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                strConsulta += Request.QueryString("Codigo_Oficina") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Estado")) Then
                strConsulta += Request.QueryString("Estado")
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dts2 As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts2.Fill(dsReportes, "dtsLegalizacionGastos")
            'Pasar el dataset a un datatable
            Dim dtb2 As New DataTable
            dtb2 = dsReportes.Tables("dtsLegalizacionGastos")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Clear()
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dstEncabezado", dsReportes.Tables(0)))
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsLegalizacionGastos", dsReportes.Tables(1)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub
    Private Sub configurar_listado_detallado_gastos_conductores()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            strPathRepo = Server.MapPath("Formatos/FlotaPropia/LegalizacionGastos/lstDetalladoGastosConductores.rdlc")
            'Armar la consulta
            strConsulta = "EXEC gsp_obtener_encabezados_listados "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Incial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "'"
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dstEncabezado")
            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dstEncabezado")

            strConsulta = "EXEC gsp_Listado_Detallado_Gastos_Conductor "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Incial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Numero_Documento")) Then
                strConsulta += Request.QueryString("Numero_Documento") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Conductor")) Then
                strConsulta += Request.QueryString("Conductor") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Vehiculo")) Then
                strConsulta += Request.QueryString("Vehiculo") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                strConsulta += Request.QueryString("Codigo_Oficina") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Estado")) Then
                strConsulta += Request.QueryString("Estado")
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dts2 As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts2.Fill(dsReportes, "dtsLegalizacionGastos")
            'Pasar el dataset a un datatable
            Dim dtb2 As New DataTable
            dtb2 = dsReportes.Tables("dtsLegalizacionGastos")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Clear()
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dstEncabezado", dsReportes.Tables(0)))
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsLegalizacionGastos", dsReportes.Tables(1)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub
    Private Sub configurar_listado_saldo_legalizacion_gastos_conductores()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            strPathRepo = Server.MapPath("Formatos/FlotaPropia/LegalizacionGastos/lstSaldoLegalizacionGastosConductores.rdlc")
            'Armar la consulta
            strConsulta = "EXEC gsp_obtener_encabezados_listados "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Incial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "'"
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dstEncabezado")
            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dstEncabezado")

            strConsulta = "EXEC gsp_listado_Saldo_Legalizacion_Gastos_Conductores "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Incial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Numero_Documento")) Then
                strConsulta += Request.QueryString("Numero_Documento") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Conductor")) Then
                strConsulta += Request.QueryString("Conductor") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Vehiculo")) Then
                strConsulta += Request.QueryString("Vehiculo") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                strConsulta += Request.QueryString("Codigo_Oficina") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Estado")) Then
                strConsulta += Request.QueryString("Estado")
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dts2 As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts2.Fill(dsReportes, "dtsLegalizacionGastos")
            'Pasar el dataset a un datatable
            Dim dtb2 As New DataTable
            dtb2 = dsReportes.Tables("dtsLegalizacionGastos")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Clear()
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dstEncabezado", dsReportes.Tables(0)))
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsLegalizacionGastos", dsReportes.Tables(1)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Private Sub configurar_listado_anticipo_legalizacion_gastos_conductores()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            strPathRepo = Server.MapPath("Formatos/FlotaPropia/LegalizacionGastos/lstAnticipoLegalizacionGastos.rdlc")
            'Armar la consulta
            strConsulta = "EXEC gsp_obtener_encabezados_listados "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Incial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "'"
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsObtenerEncabezadoListados")
            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dtsObtenerEncabezadoListados")

            strConsulta = "EXEC gsp_Listado_Anticipos_Legalizacion_Gastos "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Incial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Numero_Documento")) Then
                strConsulta += Request.QueryString("Numero_Documento") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Conductor")) Then
                strConsulta += Request.QueryString("Conductor") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Vehiculo")) Then
                strConsulta += Request.QueryString("Vehiculo") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                strConsulta += Request.QueryString("Codigo_Oficina") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Estado")) Then
                strConsulta += Request.QueryString("Estado")
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dts2 As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts2.Fill(dsReportes, "dtsListadoAnticiposLegalizacionGastos")
            'Pasar el dataset a un datatable
            Dim dtb2 As New DataTable
            dtb2 = dsReportes.Tables("dtsListadoAnticiposLegalizacionGastos")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Clear()
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsObtenerEncabezadoListados", dsReportes.Tables(0)))
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsListadoAnticiposLegalizacionGastos", dsReportes.Tables(1)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Private Sub configurar_listado_legalizacion_gastos_varios_conductores()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            strPathRepo = Server.MapPath("Formatos/FlotaPropia/LegalizacionGastos/lstLegalizacionGastosVariosConductores.rdlc")
            'Armar la consulta
            strConsulta = "EXEC gsp_obtener_encabezados_listados "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Incial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "'"
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsObtenerEncabezadoListados")
            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dtsObtenerEncabezadoListados")

            strConsulta = "EXEC gsp_Listado_Legalizacion_Gastos_Varios_Conductores "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Incial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Numero_Documento")) Then
                strConsulta += Request.QueryString("Numero_Documento") & ","
            Else
                strConsulta += "NULL,"
            End If
            'If Not IsNothing(Request.QueryString("Conductor")) Then
            '    strConsulta += Request.QueryString("Conductor") & ","
            'Else
            '    strConsulta += "NULL,"
            'End If
            If Not IsNothing(Request.QueryString("Vehiculo")) Then
                strConsulta += Request.QueryString("Vehiculo") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                strConsulta += Request.QueryString("Codigo_Oficina") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Estado")) Then
                strConsulta += Request.QueryString("Estado")
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dts2 As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts2.Fill(dsReportes, "dtsListadoLegalizacionGastosVariosConductores")
            'Pasar el dataset a un datatable
            Dim dtb2 As New DataTable
            dtb2 = dsReportes.Tables("dtsListadoLegalizacionGastosVariosConductores")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Clear()
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsObtenerEncabezadoListados", dsReportes.Tables(0)))
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsListadoLegalizacionGastosVariosConductores", dsReportes.Tables(1)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub

    Private Sub configurar_listado_planillas_pendientes_legalizar()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            strPathRepo = Server.MapPath("Formatos/FlotaPropia/LegalizacionGastos/lstPlanillasDespachosPendientesLegalizar.rdlc")
            'Armar la consulta
            strConsulta = "EXEC gsp_obtener_encabezados_listados "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Incial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "'"
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsObtenerEncabezadoListados")
            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dtsObtenerEncabezadoListados")

            strConsulta = "EXEC gsp_listado_Planillas_Pendientes_Legalizar "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Incial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Numero_Documento")) Then
                strConsulta += Request.QueryString("Numero_Documento") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Conductor")) Then
                strConsulta += Request.QueryString("Conductor") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Vehiculo")) Then
                strConsulta += Request.QueryString("Vehiculo") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                strConsulta += Request.QueryString("Codigo_Oficina") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Estado")) Then
                If Request.QueryString("Estado") = 2 Then
                    strConsulta += "NULL, 1"
                Else
                    strConsulta += Request.QueryString("Estado")
                End If
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dts2 As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts2.Fill(dsReportes, "dtsPlanillasPendientesLegalizar")
            'Pasar el dataset a un datatable
            Dim dtb2 As New DataTable
            dtb2 = dsReportes.Tables("dtsPlanillasPendientesLegalizar")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Clear()
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsObtenerEncabezadoListados", dsReportes.Tables(0)))
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsPlanillasPendientesLegalizar", dsReportes.Tables(1)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing

                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try
    End Sub

    Private Sub configurar_listado_combustible_con_chip()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            strPathRepo = Server.MapPath("Formatos/FlotaPropia/LegalizacionGastos/lstCombustibleConChip.rdlc")
            'Armar la consulta
            strConsulta = "EXEC gsp_obtener_encabezados_listados "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Incial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "'"
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsObtenerEncabezadoListados")
            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dtsObtenerEncabezadoListados")

            strConsulta = "EXEC gsp_Listado_combustible_Chip "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Incial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Numero_Documento")) Then
                strConsulta += Request.QueryString("Numero_Documento") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Conductor")) Then
                strConsulta += Request.QueryString("Conductor") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Vehiculo")) Then
                strConsulta += Request.QueryString("Vehiculo") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                strConsulta += Request.QueryString("Codigo_Oficina") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Estado")) Then
                If Request.QueryString("Estado") = 2 Then
                    strConsulta += "NULL, 1"
                Else
                    strConsulta += Request.QueryString("Estado")
                End If
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dts2 As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts2.Fill(dsReportes, "dtsPlanillasPendientesLegalizar")
            'Pasar el dataset a un datatable
            Dim dtb2 As New DataTable
            dtb2 = dsReportes.Tables("dtsListadocombustibleChip")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Clear()
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsObtenerEncabezadoListados", dsReportes.Tables(0)))
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsListadocombustibleChip", dsReportes.Tables(1)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing

                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try
    End Sub

    Private Sub configurar_listado_planillas_otras_empresas()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            strPathRepo = Server.MapPath("Formatos/FlotaPropia/PlanillasDespachosOtrasEmpresas/lstPlanillasDespachosOtrasEmpresas.rdlc")


            'Armar la consulta
            strConsulta = "EXEC gsp_listado_planillas_despachos_otras_empresas "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            If Not IsNothing(Request.QueryString("Numero_Documento")) Then
                strConsulta += Request.QueryString("Numero_Documento") & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Incial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                strConsulta += Request.QueryString("Estado") & ","
            Else
                strConsulta += "NULL,"
            End If




            If Not IsNothing(Request.QueryString("Vehiculo")) Then
                strConsulta += Request.QueryString("Vehiculo") & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Conductor")) Then
                strConsulta += Request.QueryString("Conductor") & ""
            Else
                strConsulta += "NULL"
            End If




            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsPlanillasDespachosOtrasEmpresas")
            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dtsPlanillasDespachosOtrasEmpresas")



            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Clear()
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsPlanillasDespachosOtrasEmpresas", dsReportes.Tables(0)))

            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try
    End Sub

    Private Sub configurar_listado_planillas_otras_empresas_cliente()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            strPathRepo = Server.MapPath("Formatos/FlotaPropia/PlanillasDespachosOtrasEmpresas/lstPlanillasDespachosOtrasEmpresasporCliente.rdlc")


            'Armar la consulta
            strConsulta = "EXEC gsp_listado_planillas_despachos_otras_empresas "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            If Not IsNothing(Request.QueryString("Numero_Documento")) Then
                strConsulta += Request.QueryString("Numero_Documento") & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Incial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                strConsulta += Request.QueryString("Estado") & ","
            Else
                strConsulta += "NULL,"
            End If




            If Not IsNothing(Request.QueryString("Vehiculo")) Then
                strConsulta += Request.QueryString("Vehiculo") & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Conductor")) Then
                strConsulta += Request.QueryString("Conductor") & ""
            Else
                strConsulta += "NULL"
            End If




            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsPlanillasDespachosOtrasEmpresas")
            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dtsPlanillasDespachosOtrasEmpresas")



            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Clear()
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsPlanillasDespachosOtrasEmpresas", dsReportes.Tables(0)))

            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try
    End Sub

    Private Sub Configurar_listado_auditoria_documentos()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            strPathRepo = Server.MapPath("Formatos/Utilitarios/lstAuditoriaDocumentos.rdlc")


            'Armar la consulta
            strConsulta = "EXEC gsp_consultar_auditoria_documentos "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            If Not IsNothing(Request.QueryString("TipoConsulta")) Then
                strConsulta += Request.QueryString("TipoConsulta") & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("NumeroInicio")) Then
                strConsulta += "'" & Request.QueryString("NumeroInicio") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("NumeroFin")) Then
                strConsulta += "'" & Request.QueryString("NumeroFin") & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("FechaInicio")) Then

                strConsulta += "'" & Request.QueryString("FechaInicio") & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("FechaFin")) Then
                strConsulta += "'" & Request.QueryString("FechaFin") & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("NombreUsuario")) Then
                strConsulta += Request.QueryString("NombreUsuario") & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                If Request.QueryString("Estado") <> -1 Or Request.QueryString("Estado") <> "-1" Then
                    strConsulta += Request.QueryString("Estado") & ","
                Else
                    strConsulta += "NULL,"
                End If
            Else
                strConsulta += "NULL,"
            End If



            If Not IsNothing(Request.QueryString("Anulado")) Then
                If Request.QueryString("Anulado") <> -1 Or Request.QueryString("Anulado") <> "-1" Then
                    strConsulta += Request.QueryString("Anulado") & ""
                Else
                    strConsulta += "NULL"
                End If
            Else
                strConsulta += "NULL"
            End If






            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsAuditoriaDocumentos")
            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dtsAuditoriaDocumentos")



            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Clear()
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsAuditoriaDocumentos", dsReportes.Tables(0)))

            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try
    End Sub

    Private Sub Configurar_Indicadores_Despachos()
        Dim strConsulta As String = ""
        Dim strPathRepo As String

        Try
            strPathRepo = Server.MapPath("Formatos/Despachos/lstIndicadoresDespachos.rdlc")

            Dim dsReportes As New DataSet
            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_consultar_indicadores_Planilla_Despachos "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Oficina")) Then
                strConsulta += "'" & Request.QueryString("Oficina") & "', "
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Ruta")) Then
                strConsulta += "'" & Request.QueryString("Ruta") & "', "
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Vehiculo")) Then
                strConsulta += "'" & Request.QueryString("Vehiculo") & "', "
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Semirremolque")) Then
                strConsulta += "'" & Request.QueryString("Semirremolque") & "', "
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Producto")) Then
                strConsulta += "'" & Request.QueryString("Producto") & "', "
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Conductor")) Then
                strConsulta += "'" & Request.QueryString("Conductor") & "', "
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Tenedor")) Then
                strConsulta += "'" & Request.QueryString("Tenedor") & "', "
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Cliente")) Then
                strConsulta += "'" & Request.QueryString("Cliente") & "', "
            Else
                strConsulta += "NULL,"
            End If

            strConsulta += "NULL,"
            strConsulta += "NULL,"

            If Not IsNothing(Request.QueryString("FechaInicial")) Then
                strConsulta += "'" & Request.QueryString("FechaInicial") & "', "
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("FechaFinal")) Then
                strConsulta += "'" & Request.QueryString("FechaFinal") & "'"
            Else
                strConsulta += "NULL"
            End If


            Dim RepIndicadores As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            RepIndicadores.Fill(dsReportes, "dtsIndicadoresDespachos")

            'Pasar el dataset a un datatable
            Dim dtsIndicadoresPasajeros As New DataTable
            dtsIndicadoresPasajeros = dsReportes.Tables("dtsIndicadoresDespachos")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)


            Dim rds As New ReportDataSource("dtsIndicadoresDespachos", dsReportes.Tables(0))
            rpwDocumento.LocalReport.DataSources.Clear()              'limpio la fuente de datos
            rpwDocumento.LocalReport.DataSources.Add(rds)
            rpwDocumento.LocalReport.Refresh()

            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar reporte :" & ex.Message.ToString & vbNewLine & " Consulta SQL Generada: " & strConsulta
        End Try
    End Sub

    Private Sub Configurar_Listado_Notas_Facturas()
        Dim strConsulta As String = ""
        Dim strPathRepo As String
        Try

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_NOTAS_FACTURAS_POR_CLIENTE Then
                strPathRepo = Server.MapPath("Formatos/Facturacion/lstNotasFacturasCliente.rdlc")
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_NOTAS_FACTURAS_POR_OFICINA Then
                strPathRepo = Server.MapPath("Formatos/Facturacion/lstNotasFacturasOficina.rdlc")
            End If



            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_consultar_encabezado_Nota_Facturas "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            strConsulta += "NULL,NULL,NULL,"


            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Identificacion_Cliente")) Then
                Dim Identificacion_Cliente As String = Request.QueryString("Identificacion_Cliente")
                strConsulta += "'" & Identificacion_Cliente & "',"
            Else
                strConsulta += "NULL,"
            End If

            strConsulta += "NULL,"

            If Not IsNothing(Request.QueryString("Estado")) Then
                Dim Estado As String = Request.QueryString("Estado")
                If Estado = 2 Or Estado = "2" Then
                    strConsulta += "NULL,1,"
                Else
                    strConsulta += Estado & ",NULL,"
                End If

            Else
                strConsulta += "NULL,NULL,"
            End If

            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                Dim Codigo_Oficina As String = Request.QueryString("Codigo_Oficina")
                strConsulta += "'" & Codigo_Oficina & "'"
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsSolicitudOrdenServicio As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsSolicitudOrdenServicio.Fill(dsReportes, "dtsListadoNotasFacturas")

            'Pasar el dataset a un datatable
            Dim dtbSolicitudOrdenServicio As New DataTable
            dtbSolicitudOrdenServicio = dsReportes.Tables("dtsListadoNotasFacturas")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = strPathRepo

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsListadoNotasFacturas", CType(dtbSolicitudOrdenServicio, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Notas_Facturas :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try
    End Sub

    Private Sub configurar_listado_cartera_resumida()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            strPathRepo = Server.MapPath("Formatos/Facturacion/Cartera/lstCarteraResumida.rdlc")
            'Armar la consulta
            strConsulta = "EXEC gsp_obtener_encabezados_listados "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            If Not IsNothing(Request.QueryString("Fecha_Inicial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Inicial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "'"
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsObtenerEncabezadoListados")
            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dtsObtenerEncabezadoListados")

            strConsulta = "EXEC gsp_Edad_Cartera_Factura "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            If Not IsNothing(Request.QueryString("Fecha_Inicial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Inicial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Corte")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Corte") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Cliente")) Then
                strConsulta += Request.QueryString("Cliente")
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dts2 As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts2.Fill(dsReportes, "dtsCarteraResumida")
            'Pasar el dataset a un datatable
            Dim dtb2 As New DataTable
            dtb2 = dsReportes.Tables("dtsCarteraResumida")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Clear()
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsObtenerEncabezadoListados", dsReportes.Tables(0)))
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsCarteraResumida", dsReportes.Tables(1)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing

                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try
    End Sub

    Private Sub configurar_listado_cartera_detallada()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            strPathRepo = Server.MapPath("Formatos/Facturacion/Cartera/lstCarteraDetallada.rdlc")
            'Armar la consulta
            strConsulta = "EXEC gsp_obtener_encabezados_listados "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            If Not IsNothing(Request.QueryString("Fecha_Inicial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Inicial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "'"
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsObtenerEncabezadoListados")
            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dtsObtenerEncabezadoListados")

            strConsulta = "EXEC gsp_Edad_Cartera_Factura "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            If Not IsNothing(Request.QueryString("Fecha_Inicial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Inicial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Corte")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Corte") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Cliente")) Then
                strConsulta += Request.QueryString("Cliente")
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dts2 As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts2.Fill(dsReportes, "dtsCarteraDetallada")
            'Pasar el dataset a un datatable
            Dim dtb2 As New DataTable
            dtb2 = dsReportes.Tables("dtsCarteraDetallada")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Clear()
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsObtenerEncabezadoListados", dsReportes.Tables(0)))
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsCarteraDetallada", dsReportes.Tables(1)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing

                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try
    End Sub


    Private Sub Configurar_Zonificacion_Guia()
        Dim strConsulta As String = ""
        Dim strPathRepo As String
        Try

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_ZONIFICACION_GUIA Then
                strPathRepo = Server.MapPath("Formatos/PaqueteriaV2/ltsZonificacionGuias.rdlc")
            End If


            Dim dsReportes As New DataSet
            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            strConsulta = "EXEC gsp_reporte_zonificacion_guias "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "


            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += "'" & Request.QueryString("Numero") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Planilla")) Then
                strConsulta += "'" & Request.QueryString("Planilla") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Incial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("ListaRemesas")) Then
                strConsulta += "'" & Request.QueryString("ListaRemesas") & "'"
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsZonificacionGuias")
            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dtsZonificacionGuias")

            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Clear()
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsZonificacionGuias", dsReportes.Tables(0)))

            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try
    End Sub
    Private Sub Configurar_Listado_Recoleccion_Por_Oficina()
        Dim strConsulta As String = ""
        Dim strPathRepo As String
        Try

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_RECOLECCIONES_POR_OFICINA_CREA Then
                strPathRepo = Server.MapPath("Formatos/PaqueteriaV2/lstRecoleccionesxOficinaCrea.rdlc")
            End If


            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_RECOLECCIONES_POR_OFICINA_GESTIONA Then
                strPathRepo = Server.MapPath("Formatos/PaqueteriaV2/lstRecoleccionesxOficinaGestiona.rdlc")
            End If


            Dim dsReportes As New DataSet
            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            strConsulta = "EXEC gsp_obtener_encabezados_listados "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Incial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Numero_Incial")) Then
                strConsulta += "'" & Request.QueryString("Numero_Incial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Numero_Final")) Then
                strConsulta += "'" & Request.QueryString("Numero_Final") & "'"
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsEncabezadopepito As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsEncabezadopepito.Fill(dsReportes, "dtsEncabezadoListados")

            'Pasar el dataset a un datatable
            Dim dtbEncabezadopepito As New DataTable
            dtbEncabezadopepito = dsReportes.Tables("dtsEncabezadoListados")

            'Armar la consulta
            strConsulta = "EXEC gsp_listado_recolecciones_paqueteria "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            'If Not IsNothing(Request.QueryString("Estado")) Then
            '    Dim Estado As String = Request.QueryString("Estado")
            '    If Estado = 2 Or Estado = "2" Then
            '        strConsulta += "NULL,1,"
            '    Else
            '        strConsulta += Estado & ",NULL,"
            '    End If

            'Else
            '    strConsulta += "NULL,NULL,"
            'End If

            If Not IsNothing(Request.QueryString("Ciudad")) Then
                Dim Ciudad As String = Request.QueryString("Ciudad")
                strConsulta += Ciudad

            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsSolicitudOrdenServicio As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsSolicitudOrdenServicio.Fill(dsReportes, "dtsListadoRecoleccionesPaqueteria")

            'Pasar el dataset a un datatable
            Dim dtbSolicitudOrdenServicio As New DataTable
            dtbSolicitudOrdenServicio = dsReportes.Tables("dtsListadoRecoleccionesPaqueteria")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = strPathRepo

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rds As New ReportDataSource("dtsEncabezadoListados", dsReportes.Tables(0))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rd As New ReportDataSource("dtsListadoRecoleccionesPaqueteria", dsReportes.Tables(1))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)


            rpwDocumento.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            rpwDocumento.LocalReport.DataSources.Add(rds)
            rpwDocumento.LocalReport.DataSources.Add(rd)
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Recolecciones :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try
    End Sub

    Private Sub Configurar_Listado_Comercial()
        Dim strConsulta As String = ""
        Dim strPathRepo As String
        Try

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_RECOLECCIONES_POR_OFICINA_CREA Then
                strPathRepo = Server.MapPath("Formatos/PaqueteriaV2/lstRecoleccionesxOficinaCrea.rdlc")
            End If


            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_RECOLECCIONES_POR_OFICINA_GESTIONA Then
                strPathRepo = Server.MapPath("Formatos/PaqueteriaV2/lstRecoleccionesxOficinaGestiona.rdlc")
            End If


            Dim dsReportes As New DataSet
            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            strConsulta = "EXEC gsp_obtener_encabezados_listados "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Incial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Numero_Incial")) Then
                strConsulta += "'" & Request.QueryString("Numero_Incial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Numero_Final")) Then
                strConsulta += "'" & Request.QueryString("Numero_Final") & "'"
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsEncabezadopepito As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsEncabezadopepito.Fill(dsReportes, "dtsEncabezadoListados")

            'Pasar el dataset a un datatable
            Dim dtbEncabezadopepito As New DataTable
            dtbEncabezadopepito = dsReportes.Tables("dtsEncabezadoListados")

            'Armar la consulta
            strConsulta = "EXEC gsp_listado_recolecciones_paqueteria "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                Dim Fecha_Incial As String = Request.QueryString("Fecha_Incial")
                strConsulta += "'" & Fecha_Incial & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                Dim Fecha_Final As String = Request.QueryString("Fecha_Final")
                strConsulta += "'" & Fecha_Final & "',"
            Else
                strConsulta += "NULL,"
            End If

            'If Not IsNothing(Request.QueryString("Estado")) Then
            '    Dim Estado As String = Request.QueryString("Estado")
            '    If Estado = 2 Or Estado = "2" Then
            '        strConsulta += "NULL,1,"
            '    Else
            '        strConsulta += Estado & ",NULL,"
            '    End If

            'Else
            '    strConsulta += "NULL,NULL,"
            'End If

            If Not IsNothing(Request.QueryString("Ciudad")) Then
                Dim Ciudad As String = Request.QueryString("Ciudad")
                strConsulta += Ciudad

            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsSolicitudOrdenServicio As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsSolicitudOrdenServicio.Fill(dsReportes, "dtsListadoRecoleccionesPaqueteria")

            'Pasar el dataset a un datatable
            Dim dtbSolicitudOrdenServicio As New DataTable
            dtbSolicitudOrdenServicio = dsReportes.Tables("dtsListadoRecoleccionesPaqueteria")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = strPathRepo

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rds As New ReportDataSource("dtsEncabezadoListados", dsReportes.Tables(0))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rd As New ReportDataSource("dtsListadoRecoleccionesPaqueteria", dsReportes.Tables(1))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)


            rpwDocumento.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            rpwDocumento.LocalReport.DataSources.Add(rds)
            rpwDocumento.LocalReport.DataSources.Add(rd)
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Recolecciones :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try
    End Sub

    Private Sub Configurar_Control_Contenedores()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            strPathRepo = Server.MapPath("Formatos/Despachos/Consultas/lstControlContenedores.rdlc")

            'Armar la consulta
            strConsulta = "EXEC gsp_consultar_control_contenedores_remesas "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            If Not IsNothing(Request.QueryString("Remesa")) Then
                strConsulta += "'" & Request.QueryString("Remesa") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Incial") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "',"
            Else
                strConsulta += "NULL,"
            End If
            strConsulta += "NULL,"
            If Not IsNothing(Request.QueryString("Cliente")) Then
                strConsulta += "'" & Request.QueryString("Cliente") & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Contenedor")) Then
                strConsulta += "'" & Request.QueryString("Contenedor") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Placa")) Then
                strConsulta += "'" & Request.QueryString("Placa") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Oficina")) Then
                strConsulta += "'" & Request.QueryString("Oficina") & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then

                strConsulta += "'" & Request.QueryString("Estado") & "',"

            Else
                strConsulta += "NULL,"

            End If

            If Not IsNothing(Request.QueryString("Orden_Servicio")) Then
                strConsulta += "'" & Request.QueryString("Orden_Servicio") & "',"
            Else
                strConsulta += "NULL,"
            End If

            strConsulta += "NULL,NULL,NULL,NULL,"
            If Not IsNothing(Request.QueryString("Tarifa")) Then
                strConsulta += "'" & Request.QueryString("Tarifa") & "'"
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsControlContenedores")
            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dtsControlContenedores")



            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Clear()
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsControlContenedores", dsReportes.Tables(0)))

            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing

                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try
    End Sub

    Private Sub Configurar_Listado_Permisos_por_Grupo()
        Dim strConsulta As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/SeguridadUsuarios/lstPermisoGrupoUsuarios.rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Listado_Permisos "

            strConsulta += Val(Request.QueryString("Empresa")) & ","


            If Not IsNothing(Request.QueryString("Grupo")) Then
                If (Request.QueryString("Grupo") <> -1 Or Request.QueryString("Grupo") <> "-1") Then
                    strConsulta += "" & Request.QueryString("Grupo")
                Else
                    strConsulta += "NULL"
                End If
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsPermisoGrupoUsuarios")

            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dtsPermisoGrupoUsuarios")


            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsPermisoGrupoUsuarios", CType(dtb, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "Listado_Seguridad_Usuarios", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Listado_Usuarios :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try
    End Sub

    Private Sub Configurar_Excel_Planillas_Despachos_Otras_Empresas()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            strPathRepo = Server.MapPath("Formatos/FlotaPropia/PlanillasDespachosOtrasEmpresas/lstExcelPlanillasDespachosOtrasEmpresas.rdlc")


            'Armar la consulta
            strConsulta = "EXEC gsp_consultar_planilla_despachos_otras_empresas "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero_Documento")) Then
                strConsulta += Request.QueryString("Numero_Documento") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("NumeroDocumentoTransporte")) Then
                strConsulta += Request.QueryString("NumeroDocumentoTransporte") & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("FechaInicio")) Then
                strConsulta += "'" & Request.QueryString("FechaInicio") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("FechaFin")) Then
                strConsulta += "'" & Request.QueryString("FechaFin") & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                If Request.QueryString("Estado") = "1" Then
                    strConsulta += "1,0,"
                ElseIf Request.QueryString("Estado") = "0" Then
                    strConsulta += "0,0,"
                ElseIf Request.QueryString("Estado") = "2" Then
                    strConsulta += "NULL,1,"
                Else
                    strConsulta += "NULL,NULL,"
                End If
            Else
                strConsulta += "NULL,NULL,"
            End If
            If Not IsNothing(Request.QueryString("PlacaVehiculo")) Then
                strConsulta += Request.QueryString("PlacaVehiculo") & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("NombreCliente")) Then
                strConsulta += Request.QueryString("NombreCliente") & ","
            Else
                strConsulta += "NULL,"
            End If

            strConsulta += "NULL,NULL"


            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsExcelPlanillasDespachosOtrasEmpresas")
            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dtsExcelPlanillasDespachosOtrasEmpresas")



            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Clear()
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsExcelPlanillasDespachosOtrasEmpresas", dsReportes.Tables(0)))

            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try
    End Sub

    Private Sub Configurar_Seguimiento_Despachos_Historico()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            strPathRepo = Server.MapPath("Formatos/Seguimiento/lstSegumientoDespachosHistorico.rdlc")


            'Armar la consulta
            strConsulta = "EXEC gsp_consultar_detalle_seguimiento_vehiculos_planilla_despachos_historico "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("FechaInicio")) Then
                strConsulta += "'" & Request.QueryString("FechaInicio") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("FechaFin")) Then
                strConsulta += "'" & Request.QueryString("FechaFin") & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("NumeroPlanilla")) Then
                strConsulta += Request.QueryString("NumeroPlanilla") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("NumeroManifiesto")) Then
                If Request.QueryString("NumeroManifiesto") <> "0" Or Request.QueryString("NumeroManifiesto") > 0 Then
                    strConsulta += Request.QueryString("NumeroManifiesto") & ","
                Else
                    strConsulta += "NULL,"
                End If
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Vehiculo")) Then
                strConsulta += Request.QueryString("Vehiculo") & ","
            Else
                strConsulta += "NULL,"
            End If
            strConsulta += "NULL,"
            If Not IsNothing(Request.QueryString("Ruta")) Then
                strConsulta += Request.QueryString("Ruta") & ","
            Else
                strConsulta += "NULL,"
            End If
            strConsulta += "NULL,NULL,NULL,NULL,"
            If Not IsNothing(Request.QueryString("Conductor")) Then
                strConsulta += Request.QueryString("Conductor") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Cliente")) Then
                strConsulta += Request.QueryString("Cliente") & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Origen")) Then
                strConsulta += Request.QueryString("Origen") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Destino")) Then
                strConsulta += Request.QueryString("Destino") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Producto")) Then
                strConsulta += Request.QueryString("Producto") & ","
            Else
                strConsulta += "NULL,"
            End If

            strConsulta += "NULL,NULL,NULL"


            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsSeguimientoDespachosHistorico")
            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dtsSeguimientoDespachosHistorico")



            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Clear()
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsSeguimientoDespachosHistorico", dsReportes.Tables(0)))

            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try
    End Sub

    Private Sub Configurar_Seguimiento_Remesas_Historico()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            strPathRepo = Server.MapPath("Formatos/Seguimiento/lstSeguimientoRemesasHistorico.rdlc")


            'Armar la consulta
            strConsulta = "EXEC gsp_consultar_detalle_seguimiento_remesas_historico "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("FechaInicio")) Then
                strConsulta += "'" & Request.QueryString("FechaInicio") & "',"
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("FechaFin")) Then
                strConsulta += "'" & Request.QueryString("FechaFin") & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Vehiculo")) Then
                strConsulta += Request.QueryString("Vehiculo") & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Ruta")) Then
                strConsulta += Request.QueryString("Ruta") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("NumeroDocumento")) Then
                If Request.QueryString("NumeroDocumento") <> "0" Or Request.QueryString("NumeroDocumento") > 0 Then
                    strConsulta += Request.QueryString("NumeroDocumento") & ","
                Else
                    strConsulta += "NULL,"

                End If
            Else
                strConsulta += "NULL,"
            End If
            strConsulta += "NULL,"

            If Not IsNothing(Request.QueryString("Cliente")) Then
                strConsulta += Request.QueryString("Cliente") & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("DocumentoCliente")) Then
                strConsulta += Request.QueryString("DocumentoCliente") & ","
            Else
                strConsulta += "NULL,"
            End If

            strConsulta += "NULL,NULL,NULL"


            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsSeguimientoRemesasHistorico")
            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dtsSeguimientoRemesasHistorico")



            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Clear()
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsSeguimientoRemesasHistorico", dsReportes.Tables(0)))

            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try
    End Sub

    '----  Liquidacion Planilla Proveedores ---- Diego Marin 2020/10/07  -------'
    Private Sub Configurar_liquidacion_planillas_proveedores()
        Dim strConsulta As String = ""
        Try




            'Definir variables

            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_LIQUIDACION_PLANILLAS_PROVEEDORES Then
                strPathRepo = Server.MapPath("Formatos/Proveedores/lstLiquidacionPlanillaProveedores.rdlc")
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_LIQUIDACIONES_PLANILLAS_PROVEEDORES_X_OFICINA Then
                strPathRepo = Server.MapPath("Formatos/Proveedores/lstLiquidacionPlanillaProveedoresxOficina.rdlc")
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_LISTADO_LIQUIDACIONES_PLANILLAS_PROVEEDORES_X_PROVEEDOR Then
                strPathRepo = Server.MapPath("Formatos/Proveedores/lstLiquidacionPlanillaProveedoresxProveedor.rdlc")
            End If









            'Armar la consulta
            strConsulta = "EXEC gsp_consultar_liquidacion_planilla_despachos_Proveedores "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            strConsulta += "NULL,"

            strConsulta += "NULL,"

            If Not IsNothing(Request.QueryString("Fecha_Incial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Incial") & "',"
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Estado")) Then
                strConsulta += "'" & Request.QueryString("Estado") & "',"
            Else
                strConsulta += "NULL,"
            End If

            strConsulta += "NULL,"

            strConsulta += "NULL,"

            strConsulta += "NULL,"


            strConsulta += "NULL,"

            If Not IsNothing(Request.QueryString("Numero_Documento_Inicial")) Then
                strConsulta += Request.QueryString("Numero_Documento_Inicial") & ","
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Numero_Documento_Final")) Then
                strConsulta += Request.QueryString("Numero_Documento_Final") & ","
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Transportador")) Then
                strConsulta += Request.QueryString("Transportador") & ","
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Codigo_Oficina")) Then
                strConsulta += Request.QueryString("Codigo_Oficina") & ""
            Else
                strConsulta += "NULL"
            End If




            '   strConsulta += "NULL,NULL,NULL" '



            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsLiquidacionPlanillasProveedores")
            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dtsLiquidacionPlanillasProveedores")



            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Clear()
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsLiquidacionPlanillasProveedores", dsReportes.Tables(0)))

            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try
    End Sub


    Private Sub Configurar_Listado_Novedades_Tercero()
        Dim strConsulta As String = ""
        Try




            'Definir variables

            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet


            strPathRepo = Server.MapPath("Formatos/Basico/lstNovedadesTercero.rdlc")



            'Armar la consulta
            strConsulta = "EXEC gsp_consultar_novedades_tercero "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            strConsulta += Request.QueryString("TERC_Codigo")

            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsNovedadesTercero")
            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dtsNovedadesTercero")



            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Clear()
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsNovedadesTercero", dsReportes.Tables(0)))

            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try
    End Sub


    Private Sub Configurar_Listado_Novedades_Vehiculo()
        Dim strConsulta As String = ""
        Try




            'Definir variables

            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet


            strPathRepo = Server.MapPath("Formatos/Basico/lstNovedadesVehiculo.rdlc")



            'Armar la consulta
            strConsulta = "EXEC gsp_consultar_novedades_vehiculo "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            strConsulta += Request.QueryString("VEHI_Codigo")

            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsNovedadesVehiculo")
            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dtsNovedadesVehiculo")



            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Clear()
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsNovedadesVehiculo", dsReportes.Tables(0)))

            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try
    End Sub


    Private Sub Configurar_Listado_Novedades_Semirremolque()
        Dim strConsulta As String = ""
        Try




            'Definir variables

            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet


            strPathRepo = Server.MapPath("Formatos/Basico/lstNovedadesSemirremolque.rdlc")



            'Armar la consulta
            strConsulta = "EXEC gsp_consultar_novedades_semirremolque "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "
            strConsulta += Request.QueryString("SEMI_Codigo")

            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsNovedadesSemirremolque")
            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dtsNovedadesSemirremolque")



            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Clear()
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsNovedadesSemirremolque", dsReportes.Tables(0)))

            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try
    End Sub

    Private Sub Configurar_Listado_remesas_x_facturar()
        Dim strConsulta As String = ""
        Try

            Dim strNombreCliente As String = Request.QueryString("NombreCliente")
            Dim strFechaGeneracionDocumento As String = Request.QueryString("FechaGeneracionDocumento")

            'Definir variables

            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet


            strPathRepo = Server.MapPath("Formatos/Facturacion/lstRemesasXFacturar.rdlc")



            'Armar la consulta
            strConsulta = "EXEC gsp_consultar_remesas_facturar_exportar_excel "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("NumeroDocumento")) Then
                strConsulta += "" & Request.QueryString("NumeroDocumento") & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("TipoDocumento")) Then
                strConsulta += "" & Request.QueryString("TipoDocumento") & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("FechaInicial")) Then
                strConsulta += "'" & Request.QueryString("FechaInicial") & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("FechaFinal")) Then
                strConsulta += "'" & Request.QueryString("FechaFinal") & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Cliente")) Then
                strConsulta += "" & Request.QueryString("Cliente") & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("NumeroDocumentoCliente")) Then
                strConsulta += "'" & Request.QueryString("NumeroDocumentoCliente") & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("PlanillaDespacho")) Then
                strConsulta += "" & Request.QueryString("PlanillaDespacho") & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("OrdenServicio")) Then
                strConsulta += "" & Request.QueryString("OrdenServicio") & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Sede")) Then
                strConsulta += "" & Request.QueryString("Sede") & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("FacturarA")) Then
                strConsulta += "" & Request.QueryString("FacturarA") & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("CiudadOrigen")) Then
                strConsulta += "" & Request.QueryString("CiudadOrigen") & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("CiudadDestino")) Then
                strConsulta += "" & Request.QueryString("CiudadDestino") & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Producto")) Then
                strConsulta += "" & Request.QueryString("Producto") & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("NumeroFactura")) Then
                strConsulta += "" & Request.QueryString("NumeroFactura") & ""
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsRemesasXFacturar")
            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dtsRemesasXFacturar")



            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Clear()
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsRemesasXFacturar", dsReportes.Tables(0)))

            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", String.Concat("Inicial Remesas x Facturar ", strNombreCliente, " ", strFechaGeneracionDocumento.ToString()), "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try
    End Sub

    Private Sub Configurar_Excel_Tarifario_Carga_Venta()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            strPathRepo = Server.MapPath("Formatos/Basico/lstTarifarioCargaVentaExportarExcel.rdlc")


            'Armar la consulta
            strConsulta = "EXEC gsp_consultar_detalle_tarifario_carga_venta"
            strConsulta += " "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero") & ", "
            Else
                strConsulta += "NULL"
            End If

            If Not IsNothing(Request.QueryString("CiudadesOrigen")) AndAlso Not Request.QueryString("CiudadesOrigen").Equals("") Then
                strConsulta += "'" & Request.QueryString("CiudadesOrigen") & "'" & ", "
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("CiudadesDestino")) AndAlso Not Request.QueryString("CiudadesDestino").Equals("") Then
                strConsulta += "'" & Request.QueryString("CiudadesDestino") & "'"
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsConsultarDetalleTarifarioCargaVenta")
            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dtsConsultarDetalleTarifarioCargaVenta")



            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Clear()
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsConsultarDetalleTarifarioCargaVenta", dsReportes.Tables(0)))

            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try
    End Sub

    Private Sub Configurar_Excel_Tarifario_Carga_Compra()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            strPathRepo = Server.MapPath("Formatos/Basico/lstTarifarioCargaCompraExportarExcel.rdlc")


            'Armar la consulta
            strConsulta = "EXEC gsp_consultar_detalle_tarifario_carga_Compra"
            strConsulta += " "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsConsultarDetalleTarifarioCargaCompra")
            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dtsConsultarDetalleTarifarioCargaCompra")



            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Clear()
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsConsultarDetalleTarifarioCargaCompra", dsReportes.Tables(0)))

            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try
    End Sub


    Private Sub Configurar_Listado_Sabana_Datos()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            strPathRepo = Server.MapPath("Formatos/Despachos/lstSabanaDatos.rdlc")


            'Armar la consulta
            strConsulta = "EXEC gsp_consultar_listado_sabana_datos"
            strConsulta += " "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Fecha_Inicial")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Inicial") & "',"
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Fecha_Final")) Then
                strConsulta += "'" & Request.QueryString("Fecha_Final") & "'"
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsSabanaDatos")


            'Pasar el dataset a un datatable
            Dim dtbSabana As New DataTable
            dtbSabana = dsReportes.Tables("dtsSabanaDatos")

            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsSabanaDatos", CType(dtbSabana, DataTable)))
            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  Sabana_Datos :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try

    End Sub


    Private Sub Configurar_Listado_Gastos_Flete_Ruta()
        Dim strConsulta As String = ""
        Try

            'Definir variables
            Dim strPathRepo As String = ""
            Dim dsReportes As New DataSet
            strPathRepo = Server.MapPath("Formatos/FlotaPropia/lstGastosFleteRuta.rdlc")


            'Armar la consulta
            strConsulta = "EXEC gsp_consultar_listado_gastos_flete_ruta"
            strConsulta += " "
            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Ruta")) Then
                strConsulta += "" & Request.QueryString("Ruta") & ","
            Else
                strConsulta += "NULL,"
            End If

            If Not IsNothing(Request.QueryString("Tipo_Vehiculo")) Then
                strConsulta += "" & Request.QueryString("Tipo_Vehiculo") & ""
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dts As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dts.Fill(dsReportes, "dtsGastosFleteRuta")
            'Pasar el dataset a un datatable
            Dim dtb As New DataTable
            dtb = dsReportes.Tables("dtsGastosFleteRuta")



            ' Configurar el objeto ReportViewer
            rpwDocumento.LocalReport.EnableExternalImages = True
            rpwDocumento.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            rpwDocumento.LocalReport.DataSources.Clear()
            rpwDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsGastosFleteRuta", dsReportes.Tables(0)))

            rpwDocumento.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(rpwDocumento)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = rpwDocumento.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en  :" & ex.Message.ToString & " | Consulta SQL: " & strConsulta
        End Try
    End Sub
End Class