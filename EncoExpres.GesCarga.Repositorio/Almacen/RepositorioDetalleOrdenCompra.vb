﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Almacen


Namespace Almacen
    Public NotInheritable Class RepositorioDetalleOrdenCompra
        Inherits RepositorioBase(Of DetalleOrdenCompra)
        Public Overrides Function Consultar(filtro As DetalleOrdenCompra) As IEnumerable(Of DetalleOrdenCompra)
            Dim lista As New List(Of DetalleOrdenCompra)
            Dim item As DetalleOrdenCompra

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If Not IsNothing(filtro.Numero) Then
                    If filtro.Numero > 0 Then
                        conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("sp_consultar_detalle_orden_compra_almacenes")

                While resultado.Read
                    item = New DetalleOrdenCompra(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As DetalleOrdenCompra) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As DetalleOrdenCompra) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As DetalleOrdenCompra) As DetalleOrdenCompra
            Throw New NotImplementedException()
        End Function
    End Class
End Namespace
