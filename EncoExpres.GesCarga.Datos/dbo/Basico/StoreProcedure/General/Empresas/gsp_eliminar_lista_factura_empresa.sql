﻿PRINT 'gsp_eliminar_lista_factura_empresa'
GO
DROP PROCEDURE gsp_eliminar_lista_factura_empresa
GO   
CREATE PROCEDURE gsp_eliminar_lista_factura_empresa 
(@par_EMPR_Codigo SMALLINT)    
AS  
BEGIN    
DELETE Empresa_Factura_Electronica    
WHERE     
EMPR_Codigo = @par_EMPR_Codigo       
END    
GO    