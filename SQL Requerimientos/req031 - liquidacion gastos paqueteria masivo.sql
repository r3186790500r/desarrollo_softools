USE [ENCOEXPRES]
GO

/****** Object:  StoredProcedure [dbo].[gsp_insertar_conceptos_detalle_liquidacion]    Script Date: 21/02/2022 10:18:13 p.�m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_insertar_conceptos_detalle_liquidacion]          
(                                                        
 @par_EMPR_Codigo NUMERIC,                                      
 @par_ENPD_Numero NUMERIC = NULL,                              
 @par_ELPD_Numero NUMERIC,                              
 @par_TIDO_Codigo NUMERIC = NULL,                                  
 @par_TERC_Codigo NUMERIC = NULL                                  
)                                      
AS                                      
BEGIN                                      
 DECLARE @TIDO_Planilla_Despacho_Masivo NUMERIC = 150                                      
 DECLARE @TIDO_Planilla_Despacho_Paqueteria NUMERIC = 130                                    
 DECLARE @TIDO_Planilla_Despacho_Paqueteria_V2 NUMERIC = 135                                    
 DECLARE @CATA_DOOR_Anticipo NUMERIC = 2604                                      
 DECLARE @CATA_DOOR_Anticipo_Paqueteria NUMERIC = 2613       
 DECLARE @CATA_DOOR_Sobreanticipo NUMERIC = 2605                                  
 DECLARE @CATA_DOOR_Sobreanticipo_Paqueteria NUMERIC = 2617 
                                      
 DECLARE @ValorAnticipo MONEY                                                        
 DECLARE @ValorReanticipo MONEY                                                        
 DECLARE @ValorValorRemesasContraEntrega MONEY                                                        
 DECLARE @ValorMultaExtemporanea MONEY                                                        
 DECLARE @ValorPolizaTrayectos MONEY                                       
 DECLARE @ValorCxCSaldoLiquidacion MONEY        
 DECLARE @ValorFondoResponsabilidad MONEY
                                                        
 CREATE TABLE #Conceptos_Liquidacion_Planilla_Despachos                                                        
 (                                                        
 EMPR_Codigo SMALLINT,                                                        
 ELPD_Numero NUMERIC,                                                        
 CLPD_Codigo NUMERIC,              
 CLPD_Nombre VARCHAR(MAX),              
 Observaciones VARCHAR(MAX),              
 Valor MONEY,                            
 Documento_Origen NUMERIC(18,0),          
 ENDC_Codigo NUMERIC(18,0)       ,
 Aplica_Valor_Base_Impuestos smallint
 )     
 
 --Consultar Valor Fondo Responsabilidad
 SET @ValorFondoResponsabilidad = (
 (SELECT Valor_Flete_Transportador FROM Encabezado_Planilla_Despachos WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero = @par_ENPD_Numero) * 
 (SELECT Valor_Porcentaje FROM Conceptos_Liquidacion_Planilla_Despachos WHERE EMPR_Codigo = @par_EMPR_Codigo AND Codigo = 7)--FONDO RESPONSABILIDAD
 )/100
                 
 -- Consultar los Valores Multa Extemporanea y Poliza Trayectos del Cumplido de la Planilla                
 SELECT                                                        
 @ValorMultaExtemporanea = ISNULL(SUM(Valor_Multa_Extemporaneo),0),                                                        
 @ValorPolizaTrayectos = ISNULL(SUM(Valor_Seguro_Poliza),0)                                                        
 FROM                                                        
 Encabezado_Cumplido_Planilla_Despachos AS ECPD                                              
 INNER JOIN Encabezado_Planilla_Despachos AS ENPD                                              
 ON ENPD.EMPR_Codigo = ECPD.EMPR_Codigo                                              
 AND ENPD.Numero = ECPD.ENPD_Numero                                              
 AND ECPD.Anulado = 0                                              
 WHERE                 
 ENPD.EMPR_Codigo = @par_EMPR_Codigo                                                        
 AND ENPD.TIDO_Codigo = @par_TIDO_Codigo                                    
 AND ENPD.Numero_Documento = @par_ELPD_Numero                                              
                 
 -- Pendiente                                     
 SELECT                                      
 @ValorValorRemesasContraEntrega = ISNULL(SUM(Valor_Remesas_Contra_Entrega),0)                                        
 FROM                                      
 Encabezado_Planilla_Despachos AS ENPD                                      
 WHERE                                      
 ENPD.EMPR_Codigo = @par_EMPR_Codigo                                                        
 AND ENPD.TIDO_Codigo = @par_TIDO_Codigo                
 AND ENPD.Numero_Documento = @par_ELPD_Numero                                              
                                       
                              
 -- Consultar Comprobante Egreso Anticipo                
 SELECT                       
 @ValorAnticipo = ISNULL(SUM(Valor_Pago_Recaudo_Total),0)                                                        
FROM                                                        
 Encabezado_Documento_Comprobantes               
 WHERE                                                        
 EMPR_Codigo = @par_EMPR_Codigo                                                        
 AND TIDO_Codigo = 30 --Comprobante Egreso                                                        
 AND CATA_DOOR_Codigo = (CASE @par_TIDO_Codigo                    
 WHEN @TIDO_Planilla_Despacho_Masivo THEN @CATA_DOOR_Anticipo                                     
 WHEN @TIDO_Planilla_Despacho_Paqueteria THEN @CATA_DOOR_Anticipo_Paqueteria                                    
 WHEN @TIDO_Planilla_Despacho_Paqueteria_V2 THEN @CATA_DOOR_Anticipo_Paqueteria END)                                    
 AND Documento_Origen = @par_ELPD_Numero                                      
 AND Anulado = 0 
 AND Estado = 1                                          
                 
 --PSL (IMPOCOMA)                
 IF @ValorAnticipo = 0                                      
 BEGIN                                                
                                                
 SELECT                                                        
 @ValorAnticipo = ISNULL(SUM(EDCU.Valor_Total),0)                           
 FROM                                                        
 Documentos_Interfaz_Contable_PSL AS DICP                                                
 LEFT JOIN Encabezado_Documento_Cuentas AS EDCU                                                
 ON DICP.EMPR_Codigo = EDCU.EMPR_Codigo                                                
 AND DICP.TIDO_Codigo_Origen = EDCU.TIDO_Codigo                              
 AND DICP.Numero_Origen = EDCU.Numero                                                
 AND DICP.Numero_Documento_Origen = EDCU.Documento_Origen                                                
                                                         
 WHERE                                                   
 EDCU.EMPR_Codigo = @par_EMPR_Codigo                                                        
 AND EDCU.TIDO_Codigo = 85 --Comprobante Egreso                                                        
 AND EDCU.CATA_DOOR_Codigo = 2604 --Anticipo                                                        
 AND EDCU.Documento_Origen = @par_ELPD_Numero                                                  
 AND Estado_Proceso_PSL LIKE '%PG%'                                                
 AND Anulado = 0                                                  
 END                                                
                                                         
 -- Consultar Comprobante Egreso SobreAnticipo                                                        
 SELECT                                                        
 @ValorReanticipo = ISNULL(SUM(Valor_Pago_Recaudo_Total),0)                                                        
 FROM                                                        
 Encabezado_Documento_Comprobantes                                                        
 WHERE                                                        
 EMPR_Codigo = @par_EMPR_Codigo                                                        
 AND TIDO_Codigo = 30 --Comprobante Egreso                                                        
 AND CATA_DOOR_Codigo = (CASE @par_TIDO_Codigo                    
 WHEN @TIDO_Planilla_Despacho_Masivo THEN @CATA_DOOR_Sobreanticipo                                     
 WHEN @TIDO_Planilla_Despacho_Paqueteria THEN @CATA_DOOR_Sobreanticipo_Paqueteria                                    
 WHEN @TIDO_Planilla_Despacho_Paqueteria_V2 THEN @CATA_DOOR_Sobreanticipo_Paqueteria END)                                                        
 AND Documento_Origen = @par_ELPD_Numero                                                        
 AND Anulado = 0                                                  
 AND Estado = 1                                          
                                                        
 -- Insertar todos los conceptos de liquidaci�n a la tabla temporal menos los conceptos Sistema                
 -- 10000=CxC SALDO LIQUIDACION, 10009=CxC EGRESO ANTICIPO PLANILLA ANULADA,10002=CxC MANUAL                
 INSERT INTO                                                        
 #Conceptos_Liquidacion_Planilla_Despachos                                                        
 SELECT              
 @par_EMPR_Codigo,                                                        
 0,                                                        
 Codigo,              
 '',              
 '',                                                        
 0,           
 0,          
 0,
 Aplica_Valor_Base_Impuestos
 FROM                                                        
 Conceptos_Liquidacion_Planilla_Despachos                                                        
 WHERE                      
 EMPR_Codigo = @par_EMPR_Codigo                                                        
 AND Estado = 1                                       
 AND Codigo NOT IN(10000,10002,10009)                 
                 
          
 -- Insertar Conceptos Liquidaci�n:                                      
 DECLARE @DocumentoOrigen NUMERIC(18,0) = (SELECT   TOP(1)                          
 Documento_Origen                            
 FROM                                                        
 Encabezado_Documento_Cuentas                                                        
 WHERE                            
 EMPR_Codigo = @par_EMPR_Codigo                                                        
 AND Estado = 1                             
 AND Anulado = 0                     
 AND TERC_Codigo = @par_TERC_Codigo                            
 AND Saldo > 0                            
 AND CATA_DOOR_Codigo = 2602 -- Liquidaci�n Planilla Masivo                            
 AND TIDO_Codigo = 80 --CxC                  
 AND ISNULL(Cuenta_Manual, 0) = 0 -- Solo para las generadas automaticamente                  
 )                            
                 
              
 -- Insertar Cuentas CxC SALDO LIQUIDACION = 10000                
-- INSERT INTO                                                        
-- #Conceptos_Liquidacion_Planilla_Despachos                                        
-- SELECT                                                        
-- @par_EMPR_Codigo,                                                        
-- 0,                                                        
-- 10000,              
-- '',              
-- -- se valida si existe un abono en la cuenta para agregar el n�mero de la liquidaci�n en las observaciones dependiendo si hay un abono > 0     
--IIF((SELECT COUNT(1) FROM Detalle_Cruce_Documento_Cuentas WHERE EMPR_Codigo = @par_EMPR_Codigo AND CATA_DOOR_Codigo = 2602 AND Anulado = 0 AND     
--Documento_Origen IN (SELECT Documento_Origen FROM Encabezado_Documento_Cuentas  WHERE EMPR_Codigo = @par_EMPR_Codigo AND TERC_Codigo = @par_TERC_Codigo AND CATA_DOOR_Codigo = 2602  AND TIDO_Codigo = 80 )) > 0,    
----Cuando hay abono:    
--'LIQUIDACI�N No.'+CONVERT(VARCHAR(MAX),Documento_Origen)+'. VALOR: '+CONVERT(VARCHAR(MAX),Valor_Total)+', ABONO: '+CONVERT(VARCHAR(MAX),Abono)+ '. �LTIMO ABONO: LIQUIDACI�N No.'+CONVERT(VARCHAR(MAX),ISNULL(    
-- (SELECT MAX(Documento_Origen) FROM Detalle_Cruce_Documento_Cuentas WHERE EMPR_Codigo = @par_EMPR_Codigo AND CATA_DOOR_Codigo = 2602 AND Anulado = 0 AND     
--  Documento_Origen IN (SELECT Documento_Origen FROM  Encabezado_Documento_Cuentas WHERE EMPR_Codigo = @par_EMPR_Codigo AND TERC_Codigo = @par_TERC_Codigo AND CATA_DOOR_Codigo = 2602  AND TIDO_Codigo = 80)),'0')) ,    
----Cuando no hay abono:    
--'LIQUIDACI�N No.'+ CONVERT(VARCHAR(MAX),Documento_Origen) + '. VALOR: ' + CONVERT(VARCHAR(MAX),Valor_Total) + ', ABONO: ' + CONVERT(VARCHAR(MAX),Abono)),      
-- Saldo,                            
-- Documento_Origen,          
-- Codigo        ,
-- 0
-- FROM                                                        
-- Encabezado_Documento_Cuentas            
-- WHERE                                                        
-- EMPR_Codigo = @par_EMPR_Codigo                                                      
-- AND TERC_Codigo = @par_TERC_Codigo                            
-- AND Saldo > 0                            
-- AND CATA_DOOR_Codigo = 2602 -- Liquidaci�n Planilla Masivo                            
-- AND TIDO_Codigo = 80 --CxC                  
-- AND Estado = 1                             
-- AND Anulado = 0                            
--  AND ISNULL(Cuenta_Manual, 0) = 0 -- Solo para las generadas automaticamente                  
  
    
 -- Insertar Concepto 10009=CxC EGRESO ANTICIPO PLANILLA ANULADA     
 -- Verifica si la Validaci�n esta prendida para la empresa 64=Manejo CxC Anulaci�n Planilla                
 DECLARE @ManejoCxCAnulacionPlanillaMasivo SMALLINT = 0                    
 SET @ManejoCxCAnulacionPlanillaMasivo = (SELECT Estado FROM Validacion_Procesos WHERE EMPR_Codigo = @par_EMPR_Codigo AND PROC_Codigo = 64) --64=Manejo CxC Anulaci�n Planilla                    
                
 IF ISNULL(@ManejoCxCAnulacionPlanillaMasivo,0) > 0                    
 BEGIN                    
                          
 DECLARE @CantidadCxCPlanillasAnuladasTercero NUMERIC(18,0) = 0                 
                 
        
 SET @CantidadCxCPlanillasAnuladasTercero = 
 (SELECT COUNT(1) FROM Encabezado_Documento_Cuentas WHERE EMPR_Codigo = @par_EMPR_Codigo AND CATA_DOOR_Codigo = 2604 AND TERC_Codigo = @par_TERC_Codigo AND Estado = 1 AND Anulado = 0 AND Saldo > 0 AND TIDO_Codigo = 80 AND Documento_Origen = 
	(SELECT TOP(1) Numero_Documento FROM Encabezado_Planilla_Despachos WHERE EMPR_Codigo = @par_EMPR_Codigo AND TIDO_Codigo = @par_TIDO_Codigo AND Anulado = 1 
	AND Numero_Documento IN(SELECT TOP(1) Documento_Origen FROM Encabezado_Documento_Cuentas WHERE EMPR_Codigo = @par_EMPR_Codigo AND CATA_DOOR_Codigo = 2604 AND TERC_Codigo = @par_TERC_Codigo AND Estado = 1 AND Anulado = 0 AND Saldo > 0 AND TIDO_Codigo = 80)))                  
                    
 IF ISNULL(@CantidadCxCPlanillasAnuladasTercero,0) > 0                    
 BEGIN                    
 INSERT INTO                                                  
 #Conceptos_Liquidacion_Planilla_Despachos                                        
 SELECT                                                        
@par_EMPR_Codigo,                                                        
 0,                                                        
 10009,              
 '',              
-- se valida si existe un abono en la cuenta para agregar el n�mero de la liquidaci�n en las observaciones dependiendo si hay un abono > 0                   
IIF((SELECT COUNT(1) FROM Detalle_Cruce_Documento_Cuentas WHERE EMPR_Codigo = @par_EMPR_Codigo AND CATA_DOOR_Codigo = 2602 AND Anulado = 0 AND     
Documento_Origen IN (SELECT Documento_Origen FROM Encabezado_Documento_Cuentas  WHERE EMPR_Codigo = @par_EMPR_Codigo AND TERC_Codigo = @par_TERC_Codigo AND CATA_DOOR_Codigo = 2602  AND TIDO_Codigo = 80 )) > 0,    
--Cuando hay abono :     
'PLANILLA ANULADA No.'+CONVERT(VARCHAR(MAX),Documento_Origen)+'. VALOR: '+CONVERT(VARCHAR(MAX),Valor_Total)+', ABONO: '+CONVERT(VARCHAR(MAX),Abono)+ '. �LTIMO ABONO: LIQUIDACI�N No.'+CONVERT(VARCHAR(MAX),ISNULL(    
 (SELECT MAX(Documento_Origen) FROM Detalle_Cruce_Documento_Cuentas WHERE EMPR_Codigo = @par_EMPR_Codigo AND CATA_DOOR_Codigo = 2602 AND Anulado = 0 AND     
 Documento_Origen IN (SELECT Documento_Origen FROM  Encabezado_Documento_Cuentas WHERE EMPR_Codigo = @par_EMPR_Codigo AND TERC_Codigo = @par_TERC_Codigo AND CATA_DOOR_Codigo = 2602  AND TIDO_Codigo = 80)),'0')) ,     
--Cuando no hay abono:     
'PLANILLA ANULADA No.'+ CONVERT(VARCHAR(MAX),Documento_Origen) + '. VALOR: ' + CONVERT(VARCHAR(MAX),Valor_Total) + ', ABONO: ' + CONVERT(VARCHAR(MAX),Abono)),      
Saldo,                        
 Documento_Origen   ,        
 Codigo    ,
 0
 FROM Encabezado_Documento_Cuentas                 
 WHERE EMPR_Codigo = @par_EMPR_Codigo                 
 AND CATA_DOOR_Codigo = 2604                 
 AND TERC_Codigo = @par_TERC_Codigo                 
 AND Estado = 1                
 AND Anulado = 0                 
 AND Saldo > 0                 
 AND Documento_Origen IN (SELECT Documento_Origen FROM Encabezado_Documento_Cuentas WHERE EMPR_Codigo = @par_EMPR_Codigo AND CATA_DOOR_Codigo = 2604 AND TERC_Codigo = @par_TERC_Codigo AND Estado = 1 AND Anulado = 0 AND Saldo > 0 AND TIDO_Codigo = 80)     
  
    
      
       
 AND Documento_Origen IN (SELECT Numero_Documento FROM Encabezado_Planilla_Despachos WHERE EMPR_Codigo = @par_EMPR_Codigo AND TIDO_Codigo = @par_TIDO_Codigo AND Anulado = 1 AND Numero_Documento IN(SELECT Documento_Origen FROM Encabezado_Documento_Cuentas 
  
    
      
WHERE EMPR_Codigo = @par_EMPR_Codigo AND CATA_DOOR_Codigo = 2604 AND TERC_Codigo = @par_TERC_Codigo AND Estado = 1 AND Anulado = 0 AND Saldo > 0 AND TIDO_Codigo = 80)) -- 2604: Anticipo Planilla Masivo              
 AND TIDO_Codigo = 80 --CxC                   
 END                    
 END                    
              
        
              
 -- Insertar Cuentas CONCEPTO CxC MANUAL = 10002              
 INSERT INTO                                                        
 #Conceptos_Liquidacion_Planilla_Despachos                                        
 SELECT                                                        
 @par_EMPR_Codigo,                                                        
 0,                                                        
 10002,              
 ' (' + CLPD.Nombre + ')',              
 'CXC No. ' + CONVERT(VARCHAR(MAX),ENDC.Numero) + ' , CONCEPTO: ' + CLPD.Nombre,              
 ENDC.Saldo,                            
 ENDC.Documento_Origen,          
 ENDC.Codigo  ,
 0
 FROM                                                        
 Encabezado_Documento_Cuentas ENDC              
              
 LEFT JOIN Conceptos_Liquidacion_Planilla_Despachos CLPD ON              
 CLPD.EMPR_Codigo = ENDC.EMPR_Codigo              
 AND CLPD.Codigo = ENDC.Codigo_Concepto              
              
 WHERE                                                        
 ENDC.EMPR_Codigo = @par_EMPR_Codigo                                                        
 AND ENDC.TERC_Codigo = @par_TERC_Codigo                            
 AND ENDC.Saldo > 0                            
 AND ENDC.CATA_DOOR_Codigo = 2602 -- Liquidaci�n Planilla Masivo                            
 AND ENDC.TIDO_Codigo = 80 --CxC                  
 AND ENDC.Estado = 1                             
 AND ENDC.Anulado = 0                            
 AND ISNULL(ENDC.Cuenta_Manual, 0) = 1 -- Generadas Manualmente              
          
 --Fin Inserci�n Conceptos Liquidaci�n                            
                         
 --Se actualizan los valores de la tabla temporal de acuerdo a los comprobantes de egreso                                                        
 UPDATE                                                        
 #Conceptos_Liquidacion_Planilla_Despachos                                                        
 SET                                                        
 Valor = @ValorAnticipo                                                        
 WHERE                                                        
 EMPR_Codigo = @par_EMPR_Codigo                      
 AND (ELPD_Numero = 0 OR ELPD_Numero = @par_ELPD_Numero)                                                        
 AND CLPD_Codigo = 1 --Anticipo                                                        
                                         
 UPDATE                                                        
 #Conceptos_Liquidacion_Planilla_Despachos                                                        
 SET                                                        
 Valor = @ValorValorRemesasContraEntrega                    
 WHERE                                                        
 EMPR_Codigo = @par_EMPR_Codigo                                                        
 AND (ELPD_Numero = 0 OR ELPD_Numero = @par_ELPD_Numero)                          
 AND CLPD_Codigo = 5 --Anticipo                                          
                                                 
                                               
 UPDATE                                                        
 #Conceptos_Liquidacion_Planilla_Despachos                                                        
 SET                                                        
 Valor = @ValorReanticipo                                                        
 WHERE                                                        
 EMPR_Codigo = @par_EMPR_Codigo                                                        
 AND (ELPD_Numero = 0 OR ELPD_Numero = @par_ELPD_Numero)                                                        
 AND CLPD_Codigo = 2 --Reanticipo                                                        
                                                
 UPDATE                                                        
 #Conceptos_Liquidacion_Planilla_Despachos                                           
 SET                                                        
 Valor = @ValorMultaExtemporanea                                                        
 WHERE                                                        
 EMPR_Codigo = @par_EMPR_Codigo                                                        
 AND (ELPD_Numero = 0 OR ELPD_Numero = @par_ELPD_Numero)                                                        
 AND CLPD_Codigo = 3 --Multa Extemporanea                                              
                                            
 UPDATE                                                        
 #Conceptos_Liquidacion_Planilla_Despachos                                                       
 SET                            
 Valor = @ValorPolizaTrayectos                                                        
 WHERE                                                        
 EMPR_Codigo = @par_EMPR_Codigo                                                        
 AND (ELPD_Numero = 0 OR ELPD_Numero = @par_ELPD_Numero)                                                        
 AND CLPD_Codigo = 4 --Poliza Trayectos                  
 
 UPDATE
 #Conceptos_Liquidacion_Planilla_Despachos
 SET
 Valor = @ValorFondoResponsabilidad
 WHERE
 EMPR_Codigo = @par_EMPR_Codigo AND
 (ELPD_Numero = 0 OR ELPD_Numero = @par_ELPD_Numero)  AND
 CLPD_Codigo = 7 --Fondo Responsabilidad         
                
 --Elimina Concepto 6=Contrato Vinculacion Temporal (APLICA en la empresa CARGA), se ajusta como valor de el encabezado          
 DELETE #Conceptos_Liquidacion_Planilla_Despachos WHERE EMPR_Codigo = @par_EMPR_Codigo AND CLPD_Codigo = 6               
                                                 
 --Se retorna la lista                                                        
 SELECT                                                        
 TCLPD.EMPR_Codigo,                                                        
 TCLPD.ELPD_Numero,                                                        
 ISNULL(ELPD.Numero_Documento,0) AS Numero_Documento,             
 TCLPD.CLPD_Codigo,                                                        
 CLPD.Nombre + TCLPD.CLPD_Nombre AS Nombre,                                                        
 TCLPD.Observaciones,                                                 
 TCLPD.Valor,                                                        
 CLPD.Operacion,                                                        
 CLPD.Concepto_Sistema,                               
 TCLPD.Documento_Origen,          
 TCLPD.ENDC_Codigo,          
 isnull(CLPD.Aplica_Valor_Base_Impuestos  ,0) as Aplica_Valor_Base_Impuestos          
 FROM                                                        
 #Conceptos_Liquidacion_Planilla_Despachos TCLPD                                                        
                                                        
 INNER JOIN Conceptos_Liquidacion_Planilla_Despachos CLPD                                                        
 ON TCLPD.EMPR_Codigo = CLPD.EMPR_Codigo AND TCLPD.CLPD_Codigo = CLPD.Codigo                                                        
 AND TCLPD.EMPR_Codigo = @par_EMPR_Codigo              
 AND TCLPD.ELPD_Numero = 0 OR TCLPD.ELPD_Numero = @par_ELPD_Numero                                                        
                                                        
 LEFT JOIN Encabezado_Liquidacion_Planilla_Despachos ELPD                                                     
 ON TCLPD.EMPR_Codigo = ELPD.EMPR_Codigo AND TCLPD.ELPD_Numero = ELPD.Numero          
END          
GO


