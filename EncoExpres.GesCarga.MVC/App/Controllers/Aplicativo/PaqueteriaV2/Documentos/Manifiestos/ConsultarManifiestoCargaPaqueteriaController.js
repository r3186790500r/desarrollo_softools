﻿EncoExpresApp.controller("ConsultarManifiestoCargaPaqueteriaCtrl", ['$scope', '$timeout', 'ManifiestoFactory', '$linq', 'blockUI', '$routeParams', 'ValorCatalogosFactory', 'OficinasFactory', 'EmpresasFactory',
    function ($scope, $timeout, ManifiestoFactory, $linq, blockUI, $routeParams, ValorCatalogosFactory, OficinasFactory, EmpresasFactory) {

        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Documentos' }, { Nombre: 'Manifiesto Carga' }];

        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        $scope.pref = '';
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.ListadoOficinas = [];
        $scope.Numero = 0;
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PAQUETERIA.MANIFIESTO_CARGA);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.ModeloFechaFinal = new Date();
        }
        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };



        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                if (DatosRequeridos()) {
                    $scope.Codigo = 0
                    Find()
                }
            }
        };

        if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== 0) {
            if ($routeParams.Numero > 0) {
                $scope.ModeloNumero = $routeParams.Numero;
                Find();
            }
        }

        $scope.ListadoEstados = [
            { Nombre: '(TODAS)', Codigo: -1 },
            { Nombre: 'DEFINITIVO', Codigo: 1 },
            { Nombre: 'BORRADOR', Codigo: 0 },
            { Nombre: 'ANULADO', Codigo: 2 }
        ];
        $scope.ModeloEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');

        EmpresasFactory.Consultar(
            { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, }
        ).then(function (response) {
            if (response.data.ProcesoExitoso === true) {

                $scope.pref = response.data.Datos[0].Prefijo;

            }
        });


        OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, Usuario: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo } }).
            then(function (response) {
                $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1 })
                if (response.data.ProcesoExitoso === true) {
                    response.data.Datos.forEach(function (item) {
                        $scope.ListadoOficinas.push(item)
                    });
                    if ($scope.Sesion.UsuarioAutenticado.Prefijo == PREFIJO_INTEGRA) {
                        $scope.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == 1');
                    } else {
                        $scope.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == -1');
                    }

                }
            }, function (response) {
                ShowError(response.statusText);
            });
        $scope.ModeloFechaInicial = new Date();
        $scope.ModeloFechaFinal = new Date();
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.ModeloFechaInicial = new Date();
            $scope.ModeloFechaFinal = new Date();
        }
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.ModeloFechaInicial === null || $scope.ModeloFechaInicial === undefined || $scope.ModeloFechaInicial === '')
                && ($scope.ModeloFechaFinal === null || $scope.ModeloFechaFinal === undefined || $scope.ModeloFechaFinal === '')
                && ($scope.ModeloNumero === null || $scope.ModeloNumero === undefined || $scope.ModeloNumero === '' || $scope.ModeloNumero === 0 || isNaN($scope.ModeloNumero) === true)

            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false

            } else if (($scope.ModeloNumero !== null && $scope.ModeloNumero !== undefined && $scope.ModeloNumero !== '' && $scope.ModeloNumero !== 0)
                || ($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')
                || ($scope.ModeloFechaFinal !== null && $scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '')

            ) {
                if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')
                    && ($scope.ModeloFechaFinal !== null && $scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '')) {
                    if ($scope.ModeloFechaFinal < $scope.ModeloFechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.ModeloFechaFinal - $scope.ModeloFechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }

                else {
                    if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')) {
                        $scope.ModeloFechaFinal = $scope.ModeloFechaInicial
                    } else {
                        $scope.ModeloFechaInicial = $scope.ModeloFechaFinal
                    }
                }
            }
            return continuar
        }

        function Find() {
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                UsuarioCrea: $scope.Sesion.UsuarioAutenticado.Codigo,
                UsuarioModifica: $scope.Sesion.UsuarioAutenticado.Codigo,
                NumeroDocumento: $scope.ModeloNumero,
                FechaInicial: $scope.ModeloNumero > 0 ? undefined : $scope.ModeloFechaInicial,
                FechaFinal: $scope.ModeloNumero > 0 ? undefined : $scope.ModeloFechaFinal,
                Vehiculo: { Placa: $scope.ModeloPlaca },
                Oficina: $scope.ModeloOficina,
                Estado: $scope.ModeloEstado,
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                Tipo_Manifiesto: { Codigo: 8806 },
                UsuarioConsulta: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                Planilla: { TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_PAQUETERIA }
            };

            blockUI.start('Buscando registros ...');
            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoManifiesto = [];
            if ($scope.Buscando) {
                if ($scope.MensajesError.length == 0) {
                    blockUI.delay = 1000;
                    $scope.Pagina = $scope.paginaActual
                    $scope.RegistrosPagina = 10
                    ManifiestoFactory.Consultar(filtros).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                $scope.Numero = 0
                                if (response.data.Datos.length > 0) {
                                    $scope.ListadoManifiesto = response.data.Datos
                                    $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                    $scope.Buscando = false;
                                    $scope.ResultadoSinRegistros = '';
                                }
                                else {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }
                                var listado = [];
                                response.data.Datos.forEach(function (item) {
                                    listado.push(item);
                                });
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            blockUI.stop();
        }
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.MostrarMensajeErrorModal = function () {
            $scope.MostrarMensajeErrorCompleto = true
        }
        $scope.CerrarModal = function () {
            closeModal('modalEliminarTarifarioVentas');
            closeModal('modalMensajeEliminarTarifarioVentas');
        }

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };

        $scope.DesplegarInforme = function (Numero, OpcionPDf, OpcionEXCEL, NumeroDocumento) {

            $scope.urlASP = '';
            $scope.Numero = Numero;
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NumeroDocumento = NumeroDocumento;

            $scope.NombreReporte = NOMBRE_REPORTE_MANI;

            //Arma el filtro
            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&Usuario=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&USUA_Imprime=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&Prefijo=' + $scope.pref);
            }
            if (OpcionEXCEL == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&Usuario=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf + '&USUA_Imprime=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&Prefijo=' + $scope.pref);
            }

            //Se limpia el filtro para poder hacer mas consultas
            //$scope.FiltroArmado = '';
            //filtros = {
            //    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            //    UsuarioCrea: $scope.Sesion.UsuarioAutenticado.Codigo,
            //    UsuarioModifica: $scope.Sesion.UsuarioAutenticado.Codigo,
            //    NumeroDocumento: $scope.NumeroDocumento,

            //    Pagina: $scope.paginaActual,
            //    RegistrosPagina: $scope.cantidadRegistrosPorPaginas
            //};
            //var imgQRCode;
            //ManifiestoFactory.Consultar(filtros).
            //    then(function (response) {
            //        if (response.data.ProcesoExitoso === true) {

            //            if (response.data.Datos.length > 0) {

            //                var bolAplicaResolucionOrigen = false;
            //                var bolAplicaResolucionDestino = false;

            //                var bolAplicaResolucionXOrigen = false;
            //                var bolAplicaResolucionXDestino = false;

            //                //1. Origen en departamentos
            //                lstDEPARTAMENTOSSINREDUCCION.forEach(function (itmDepa) {
            //                    if (itmDepa.Codigo == response.data.Datos[0].CodigoDepartOrigen) {
            //                        bolAplicaResolucionOrigen = true;
            //                    }
            //                });

            //                //destino hacia cualquier departamento distinto a uno de los anteriores
            //                lstDEPARTAMENTOSSINREDUCCION.forEach(function (itmDepa) {
            //                    if (itmDepa.Codigo !== response.data.Datos[0].CodigoDepartDestino) {
            //                        bolAplicaResolucionDestino = true;
            //                    } else {
            //                        bolAplicaResolucionDestino = false;
            //                    }
            //                });

            //                if (bolAplicaResolucionOrigen == true && bolAplicaResolucionDestino == true) {
            //                    bolAplicaResolucionXOrigen = true;
            //                }

            //                bolAplicaResolucionOrigen = false;
            //                bolAplicaResolucionDestino = false;                           

            //                if (bolAplicaResolucionXOrigen == false) {
            //                    //2.  Destino a los departamentos 
            //                    lstDEPARTAMENTOSSINREDUCCION.forEach(function (itmDepa) {
            //                        if (itmDepa.Codigo == response.data.Datos[0].CodigoDepartDestino) {
            //                            bolAplicaResolucionDestino = true;
            //                        }
            //                    });

            //                    // Con origen de otros departamentos
            //                    lstDEPARTAMENTOSSINREDUCCION.forEach(function (itmDepa) {
            //                        if (itmDepa.Codigo !== response.data.Datos[0].CodigoDepartOrigen) {
            //                            bolAplicaResolucionOrigen = true;
            //                        } else {
            //                            bolAplicaResolucionOrigen = false;
            //                        }
            //                    });
            //                    if (bolAplicaResolucionOrigen == true && bolAplicaResolucionDestino == true) {
            //                        bolAplicaResolucionXDestino = true;
            //                    }

            //                }

            //                //Asigna obervaciones de reduccion de peajes si lo aplica
            //                if (bolAplicaResolucionXOrigen == true || bolAplicaResolucionXDestino == true) {

            //                    if (response.data.Datos[0].Observaciones.indexOf(OBSERVACIONES_RESOLUCION) == -1) {
            //                        response.data.Datos[0].Observaciones = OBSERVACIONES_RESOLUCION + response.data.Datos[0].Observaciones;
            //                    }

            //                }


            //                imgQRCode = qrcodeGenerator(JsObjToString({
            //                    NumeroDocumento: response.data.Datos[0].NumeroDocumento,
            //                    Placa: response.data.Datos[0].Vehiculo.Placa,
            //                    Remolque: response.data.Datos[0].Semirremolque.Placa,
            //                    Orig: response.data.Datos[0].NombreCiudadOrigen,
            //                    Dest: response.data.Datos[0].NombreCiudadDestino,
            //                    Mercancia: response.data.Datos[0].NombreProductotrans,
            //                    Conductor: response.data.Datos[0].Conductor.NombreCompleto,
            //                    Empresa: response.data.Datos[0].NombreEmpresa,
            //                    Observaciones: response.data.Datos[0].Observaciones

            //                }));


            //                var objEnviar = {
            //                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            //                    Numero: $scope.NumeroDocumento,
            //                    QR_Manifiesto: imgQRCode.bytes,
            //                    Observaciones: response.data.Datos[0].Observaciones
            //                }
            //                ManifiestoFactory.Guardar(objEnviar).
            //                    then(function (response) {
            //                        if (response.data.ProcesoExitoso == true) {
            //                            if (response.data.Datos > CERO) {

            //                                $scope.NombreReporte = NOMBRE_REPORTE_MANI;

            //                                //Arma el filtro
            //                                $scope.ArmarFiltro();

            //                                //Llama a la pagina ASP con el filtro por GET
            //                                if (OpcionPDf == 1) {
            //                                    window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&Usuario=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            //                                }
            //                                if (OpcionEXCEL == 1) {
            //                                    window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&Usuario=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            //                                }

            //                                //Se limpia el filtro para poder hacer mas consultas
            //                                $scope.FiltroArmado = '';

            //                            }
            //                            else {
            //                                ShowError(response.statusText);
            //                            }
            //                        }
            //                        else {
            //                            ShowError(response.statusText);
            //                        }
            //                    }, function (response) {
            //                        ShowError(response.statusText);
            //                    });
            //            }
            //            else {

            //                $scope.Buscando = false;
            //            }

            //        }
            //    }, function (response) {
            //        ShowError(response.statusText);
            //    });

        };


        $scope.DesplegarPlan = function (Numero, OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.Numero = Numero;
            $scope.NombreReporte = NOMBRE_REPORTE_PLAN_RUTA;

            //Arma el filtro
            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf === 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&USUA_Imprime=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&Prefijo=' + $scope.pref);
            }
            if (OpcionEXCEL === 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionEXCEL + '&USUA_Imprime=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&Prefijo=' + $scope.pref);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';

        };
        $scope.AnularDocumento = function (Numero, NumeroDocumento) {
            $scope.Numero = Numero

            $scope.ModalErrorCompleto = ''
            $scope.NumeroDocumento = NumeroDocumento
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false
            showModal('modalAnular');
        };
        function DatosRequeridosAnular() {
            var continuar = true
            $scope.MensajesErrorAnular = []

            if ($scope.ModeloCausaAnula == undefined || $scope.ModeloCausaAnula == null || $scope.ModeloCausaAnula == "") {
                continuar = false
                $scope.MensajesErrorAnular.push('debe ingresar la causa de anulación')
            }
            return continuar
        }
        $scope.Anular = function () {
            var aplicaLiquidaciones = false
            var aplicaCumplidos = false

            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                CausaAnulacion: $scope.ModeloCausaAnula,
                Numero: $scope.Numero,

            };
            if (DatosRequeridosAnular()) {
                ManifiestoFactory.Anular(entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Anulado > 0) {
                                ShowSuccess('Se anuló el manifiesto ' + $scope.NumeroDocumento);
                                closeModal('modalAnular');
                                Find();
                            } else {
                                var mensaje = 'No se pudo anular el manifiesto ya que se encuentra relacionado con los siguientes documentos: '

                                var Liquidaciones = '\nLiquidaciones: '
                                var Cumplidos = '\nCumplidos: '
                                for (var i = 0; i < response.data.Datos.DocumentosRelacionados.length; i++) {
                                    if (response.data.Datos.DocumentosRelacionados[i].Cumplido.Numero > 0) {
                                        Cumplidos += response.data.Datos.DocumentosRelacionados[i].Cumplido.Numero + ','
                                        aplicaCumplidos = true
                                    }
                                    if (response.data.Datos.DocumentosRelacionados[i].Liquidacion.Numero > 0) {
                                        Liquidaciones += response.data.Datos.DocumentosRelacionados[i].Liquidacion.Numero + ','
                                        aplicaLiquidaciones = true
                                    }

                                }

                                if (aplicaLiquidaciones) {
                                    mensaje += Liquidaciones
                                }
                                if (aplicaCumplidos) {
                                    mensaje += Cumplidos
                                }

                                ShowError(mensaje)
                            }

                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        var result = ''
                        showModal('modalMensajeAnularOrdenCargue');
                        result = InternalServerError(response.statusText)
                        $scope.ModalError = 'No se puede anular la orden de servicio ' + $scope.NumeroDocumento;
                        $scope.ModalErrorCompleto = response.statusText
                    });
            }


        };

        $scope.ArmarFiltro = function () {
            if ($scope.Numero != undefined && $scope.Numero != '') {
                $scope.FiltroArmado = '&Numero=' + $scope.Numero;
            }

        }

    }]);