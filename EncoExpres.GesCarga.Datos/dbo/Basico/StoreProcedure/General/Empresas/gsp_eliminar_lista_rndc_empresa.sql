﻿PRINT 'gsp_eliminar_lista_rndc_empresa'
GO
DROP PROCEDURE gsp_eliminar_lista_rndc_empresa
GO   
CREATE PROCEDURE gsp_eliminar_lista_rndc_empresa 
(@par_EMPR_Codigo SMALLINT)    
AS  
BEGIN    
DELETE Empresa_Manifiesto_Electronico    
WHERE     
EMPR_Codigo = @par_EMPR_Codigo       
END    
GO    