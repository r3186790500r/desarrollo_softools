﻿Imports System.Transactions
Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Seguridad
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios

Namespace SeguridadUsuarios
    Public NotInheritable Class RepositorioEstudioSeguridad
        Inherits RepositorioBase(Of EstudioSeguridad)
        Public Overrides Function Consultar(filtro As EstudioSeguridad) As IEnumerable(Of EstudioSeguridad)
            Dim lista As New List(Of EstudioSeguridad)
            Dim item As EstudioSeguridad

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.NumeroDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero_Documento", filtro.NumeroDocumento)
                End If

                If filtro.FechaInicio > DateTime.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicio, SqlDbType.DateTime)
                End If

                If filtro.FechaFin > DateTime.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFin, SqlDbType.DateTime)
                End If

                If Not IsNothing(filtro.Oficinas) Then
                    If filtro.Oficinas.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficinas.Codigo)
                    End If
                End If

                If Not String.IsNullOrWhiteSpace(filtro.Placa) Then
                    conexion.AgregarParametroSQL("@par_Placa", filtro.Placa)
                End If

                If Not String.IsNullOrWhiteSpace(filtro.NombrePropietario) Then
                    conexion.AgregarParametroSQL("@par_Nombre_Propietario", filtro.NombrePropietario)
                End If

                If Not String.IsNullOrWhiteSpace(filtro.NombreConductor) Then
                    conexion.AgregarParametroSQL("@par_Nombre_Conductor", filtro.NombreConductor)
                End If

                If Not String.IsNullOrWhiteSpace(filtro.NombreTenedor) Then
                    conexion.AgregarParametroSQL("@par_Nombre_Tenedor", filtro.NombreTenedor)
                End If

                If Not IsNothing(filtro.UsuarioSolicito) Then
                    conexion.AgregarParametroSQL("@par_Usuario_Solicito", filtro.UsuarioSolicito)
                End If

                If Not IsNothing(filtro.UsuarioEstudio) Then
                    conexion.AgregarParametroSQL("@par_Usuario_Estudio", filtro.UsuarioEstudio)
                End If

                If filtro.Numeracion > 0 Then
                    conexion.AgregarParametroSQL("@par_Numeracion", filtro.Numeracion)
                End If

                If filtro.EstadoAutorizacion > -1 Then
                    conexion.AgregarParametroSQL("@par_Estado_Autorizacion", filtro.EstadoAutorizacion)
                End If

                If filtro.Estado > -1 Then
                    If filtro.Estado = 0 Then
                        conexion.AgregarParametroSQL("@par_Estado", 0)
                        conexion.AgregarParametroSQL("@par_Anulado", 0)
                    End If
                    If filtro.Estado = 1 Then
                        conexion.AgregarParametroSQL("@par_Estado", 1)
                        conexion.AgregarParametroSQL("@par_Anulado", 0)
                    End If
                    If filtro.Estado = 2 Then
                        conexion.AgregarParametroSQL("@par_Anulado", 1)
                    End If
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If

                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_encabezado_estudio_seguridad")
                While resultado.Read
                    item = New EstudioSeguridad(resultado)
                    lista.Add(item)
                End While
            End Using

            Return lista
        End Function


        Public Overrides Function Insertar(entidad As EstudioSeguridad) As Long
            Dim transladartemporal As Boolean = True
            Dim limpiartemporal As Boolean = True
            Dim DetalleDocumentos As New EstudioSeguridadDocumentos
            Using conexionDocumentos = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                conexionDocumentos.CreateConnection()
                Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                    Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                        conexion.CreateConnection()

                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
                        conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                        conexion.AgregarParametroSQL("@par_Placa", entidad.Placa)
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficinas.Codigo)
                        conexion.AgregarParametroSQL("@par_Estado_Autorizacion", entidad.EstadoAutorizacion)
                        conexion.AgregarParametroSQL("@par_Numero_Autorizacion", entidad.NumeroAutorizacion)
                        conexion.AgregarParametroSQL("@par_CATA_CRES_Codigo", entidad.ClaseRiesgo)
                        conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                        conexion.AgregarParametroSQL("@par_Observaciones_Estudio_Seguridad", entidad.Observaciones)
                        conexion.AgregarParametroSQL("@par_MAVE_Codigo", entidad.MarcaVehiculo)
                        conexion.AgregarParametroSQL("@par_CATA_TIVE_Codigo", entidad.TipoVehiculo)
                        conexion.AgregarParametroSQL("@par_Modelo_Vehiculo", entidad.Modelo)
                        conexion.AgregarParametroSQL("@par_Modelo_Repotenciado", entidad.ModeloRepotenciado)
                        conexion.AgregarParametroSQL("@par_COVE_Codigo", entidad.ColorVehiculo)

                        If Not IsNothing(entidad.Aseguradora) Then
                            conexion.AgregarParametroSQL("@par_TERC_Aseguradora_Seguro_Obligatorio", entidad.Aseguradora.Codigo)
                        End If

                        conexion.AgregarParametroSQL("@par_Numero_Seguro_Obligatorio", entidad.NumeroSeguro)
                        conexion.AgregarParametroSQL("@par_Fecha_Emision_Seguro_Obligatorio", entidad.FechaEmisionSeguro, SqlDbType.Date)
                        conexion.AgregarParametroSQL("@par_Fecha_Vence_Seguro_Obligatorio", entidad.FechaVenceSeguro, SqlDbType.Date)
                        conexion.AgregarParametroSQL("@par_Numero_Revision_Tecnomecanica", entidad.NumeroTecnomecanica)
                        conexion.AgregarParametroSQL("@par_Fecha_Emision_Revision_Tecnomecanica", entidad.FechaEmisionTecnomecanica, SqlDbType.Date)
                        conexion.AgregarParametroSQL("@par_Fecha_Vence_Revision_Tecnomecanica", entidad.FechaVenceTecnomecanica, SqlDbType.Date)

                        conexion.AgregarParametroSQL("@par_Nombre_Empresa_GPS", entidad.NombreEmpresaGPS)
                        conexion.AgregarParametroSQL("@par_Telefono_Empresa_GPS", entidad.TelefonoEmpresaGPS)
                        conexion.AgregarParametroSQL("@par_Usuario_GPS", entidad.UsuarioGPS)
                        conexion.AgregarParametroSQL("@par_Clave_GPS", entidad.ClaveGPS)

                        conexion.AgregarParametroSQL("@par_Empresa_Afiliadora", entidad.EmpresaAfiliadora)
                        conexion.AgregarParametroSQL("@par_Telefono_Afiliadora", entidad.TelefonoAfiliadora)
                        conexion.AgregarParametroSQL("@par_Numero_Carnet_Afiliadora", entidad.NumeroCarnetAfiliadora)
                        conexion.AgregarParametroSQL("@par_Fecha_Vence_Afiliadora", entidad.FechaVenceAfiliadora, SqlDbType.Date)

                        conexion.AgregarParametroSQL("@par_Placa_Semirremolque", entidad.PlacaSemirremolque)
                        If Not IsNothing(entidad.MarcaSemirremolque) Then
                            conexion.AgregarParametroSQL("@par_MASE_Codigo", entidad.MarcaSemirremolque.Codigo)
                        End If
                        If Not IsNothing(entidad.TipoSemirremolque) Then
                            conexion.AgregarParametroSQL("@par_CATA_TISE_Codigo", entidad.TipoSemirremolque.Codigo)
                        End If
                        If Not IsNothing(entidad.TipoCarroceriaSemirremolque) Then
                            conexion.AgregarParametroSQL("@par_CATA_TICA_Codigo", entidad.TipoCarroceriaSemirremolque.Codigo)
                        End If
                        conexion.AgregarParametroSQL("@par_Modelo_Semirremolque", entidad.ModeloSemirremolque)
                        conexion.AgregarParametroSQL("@par_Numero_Ejes", entidad.NumeroEjes)

                        If Not IsNothing(entidad.Propietario) Then
                            conexion.AgregarParametroSQL("@par_CATA_TINT_Propietario", entidad.Propietario.TipoNaturaleza.Codigo)
                            conexion.AgregarParametroSQL("@par_CATA_TIID_Propietario", entidad.Propietario.TipoIdentificacion.Codigo)
                            conexion.AgregarParametroSQL("@par_Numero_Identificacion_Propietario", entidad.Propietario.NumeroIdentificacion)
                            conexion.AgregarParametroSQL("@par_Nombre_Propietario", entidad.Propietario.Nombre)
                            conexion.AgregarParametroSQL("@par_Primero_Apellido_Propietario", entidad.Propietario.PrimeroApellido)
                            conexion.AgregarParametroSQL("@par_Segundo_Apellido_Propietario", entidad.Propietario.SegundoApellido)
                            conexion.AgregarParametroSQL("@par_Razon_Social_Propietario", entidad.Propietario.RazonSocial)
                            conexion.AgregarParametroSQL("@par_Digito_Chequeo_Propietario", entidad.Propietario.DigitoChequeo)
                            If Not IsNothing(entidad.Propietario.CiudadExpedicionIdent) Then
                                conexion.AgregarParametroSQL("@par_CIUD_Identificacion_Propietario", entidad.Propietario.CiudadExpedicionIdent.Codigo)
                            End If
                            If Not IsNothing(entidad.Propietario.Ciudad) Then
                                conexion.AgregarParametroSQL("@par_CIUD_Residencia_Propietario", entidad.Propietario.Ciudad.Codigo)
                            End If
                            conexion.AgregarParametroSQL("@par_Direccion_Propietario", entidad.Propietario.Direccion)
                            conexion.AgregarParametroSQL("@par_Telefono_Propietario", entidad.Propietario.Telefonos)
                            conexion.AgregarParametroSQL("@par_Celular_Propietario", entidad.Propietario.Celular)
                        End If

                        If Not IsNothing(entidad.PropietarioRemolque) Then
                            conexion.AgregarParametroSQL("@par_CATA_TINT_Propietario_Remolque", entidad.PropietarioRemolque.TipoNaturaleza.Codigo)
                            conexion.AgregarParametroSQL("@par_CATA_TIID_Propietario_Remolque", entidad.PropietarioRemolque.TipoIdentificacion.Codigo)
                            conexion.AgregarParametroSQL("@par_Numero_Identificacion_Propietario_Remolque", entidad.PropietarioRemolque.NumeroIdentificacion)
                            conexion.AgregarParametroSQL("@par_Nombre_Propietario_Remolque", entidad.PropietarioRemolque.Nombre)
                            conexion.AgregarParametroSQL("@par_Primero_Apellido_Propietario_Remolque", entidad.PropietarioRemolque.PrimeroApellido)
                            conexion.AgregarParametroSQL("@par_Segundo_Apellido_Propietario_Remolque", entidad.PropietarioRemolque.SegundoApellido)
                            conexion.AgregarParametroSQL("@par_Razon_Social_Propietario_Remolque", entidad.PropietarioRemolque.RazonSocial)
                            conexion.AgregarParametroSQL("@par_Digito_Chequeo_Propietario_Remolque", entidad.PropietarioRemolque.DigitoChequeo)
                            If Not IsNothing(entidad.PropietarioRemolque.CiudadExpedicionIdent) Then
                                conexion.AgregarParametroSQL("@par_CIUD_Identificacion_Propietario_Remolque", entidad.PropietarioRemolque.CiudadExpedicionIdent.Codigo)
                            End If
                            If Not IsNothing(entidad.PropietarioRemolque.Ciudad) Then
                                conexion.AgregarParametroSQL("@par_CIUD_Residencia_Propietario_Remolque", entidad.PropietarioRemolque.Ciudad.Codigo)
                            End If
                            conexion.AgregarParametroSQL("@par_Direccion_Propietario_Remolque", entidad.PropietarioRemolque.Direccion)
                            conexion.AgregarParametroSQL("@par_Telefono_Propietario_Remolque", entidad.PropietarioRemolque.Telefonos)
                            conexion.AgregarParametroSQL("@par_Celular_Propietario_Remolque", entidad.PropietarioRemolque.Celular)
                        End If

                        If Not IsNothing(entidad.Tenedor) Then
                            conexion.AgregarParametroSQL("@par_CATA_TINT_Tenedor", entidad.Tenedor.TipoNaturaleza.Codigo)
                            conexion.AgregarParametroSQL("@par_CATA_TIID_Tenedor", entidad.Tenedor.TipoIdentificacion.Codigo)
                            conexion.AgregarParametroSQL("@par_Numero_Identificacion_Tenedor", entidad.Tenedor.NumeroIdentificacion)
                            conexion.AgregarParametroSQL("@par_Nombre_Tenedor", entidad.Tenedor.Nombre)
                            conexion.AgregarParametroSQL("@par_Primero_Apellido_Tenedor", entidad.Tenedor.PrimeroApellido)
                            conexion.AgregarParametroSQL("@par_Segundo_Apellido_Tenedor", entidad.Tenedor.SegundoApellido)
                            conexion.AgregarParametroSQL("@par_Razon_Social_Tenedor", entidad.Tenedor.RazonSocial)
                            conexion.AgregarParametroSQL("@par_Digito_Chequeo_Tenedor", entidad.Tenedor.DigitoChequeo)
                            If Not IsNothing(entidad.Tenedor.CiudadExpedicionIdent) Then
                                conexion.AgregarParametroSQL("@par_CIUD_Identificacion_Tenedor", entidad.Tenedor.CiudadExpedicionIdent.Codigo)
                            End If
                            If Not IsNothing(entidad.Tenedor.Ciudad) Then
                                conexion.AgregarParametroSQL("@par_CIUD_Residencia_Tenedor", entidad.Tenedor.Ciudad.Codigo)
                            End If
                            conexion.AgregarParametroSQL("@par_Direccion_Tenedor", entidad.Tenedor.Direccion)
                            conexion.AgregarParametroSQL("@par_Telefono_Tenedor", entidad.Tenedor.Telefonos)
                            conexion.AgregarParametroSQL("@par_Celular_Tenedor", entidad.Tenedor.Celular)
                        End If

                        If Not IsNothing(entidad.Conductor) Then
                            conexion.AgregarParametroSQL("@par_CATA_TINT_Conductor", entidad.Conductor.TipoNaturaleza.Codigo)
                            conexion.AgregarParametroSQL("@par_CATA_TIID_Conductor", entidad.Conductor.TipoIdentificacion.Codigo)
                            conexion.AgregarParametroSQL("@par_Numero_Identificacion_Conductor", entidad.Conductor.NumeroIdentificacion)
                            conexion.AgregarParametroSQL("@par_Nombre_Conductor", entidad.Conductor.Nombre)
                            conexion.AgregarParametroSQL("@par_Primero_Apellido_Conductor", entidad.Conductor.PrimeroApellido)
                            conexion.AgregarParametroSQL("@par_Segundo_Apellido_Conductor", entidad.Conductor.SegundoApellido)
                            conexion.AgregarParametroSQL("@par_Razon_Social_Conductor", entidad.Conductor.RazonSocial)
                            conexion.AgregarParametroSQL("@par_Digito_Chequeo_Conductor", entidad.Conductor.DigitoChequeo)
                            If Not IsNothing(entidad.Conductor.CiudadExpedicionIdent) Then
                                conexion.AgregarParametroSQL("@par_CIUD_Identificacion_Conductor", entidad.Conductor.CiudadExpedicionIdent.Codigo)
                            End If
                            If Not IsNothing(entidad.Conductor.Ciudad) Then
                                conexion.AgregarParametroSQL("@par_CIUD_Residencia_Conductor", entidad.Conductor.Ciudad.Codigo)
                            End If
                            conexion.AgregarParametroSQL("@par_Direccion_Conductor", entidad.Conductor.Direccion)
                            conexion.AgregarParametroSQL("@par_Telefono_Conductor", entidad.Conductor.Telefonos)
                            conexion.AgregarParametroSQL("@par_Celular_Conductor", entidad.Conductor.Celular)
                        End If

                        If Not IsNothing(entidad.Cliente) Then
                            conexion.AgregarParametroSQL("@par_Nombre_Cliente", entidad.Cliente)
                        End If
                        conexion.AgregarParametroSQL("@par_Nombre_Mercancia_Cliente", entidad.MercanciaCliente)
                        If Not IsNothing(entidad.Ruta) Then
                            conexion.AgregarParametroSQL("@par_RUTA_Codigo_Cliente", entidad.Ruta.Codigo)
                        End If
                        conexion.AgregarParametroSQL("@par_Valor_Mercancia_Cliente", entidad.ValorMercancia, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Observaciones_Despacho_Cliente", entidad.ObservacionesCliente)

                        conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_estudio_seguridad")
                        While resultado.Read
                            entidad.NumeroDocumento = resultado.Item("Numero_Documento").ToString()
                            entidad.Numero = resultado.Item("Numero").ToString()
                        End While

                        resultado.Close()

                        If entidad.NumeroDocumento.Equals(Cero) Then
                            Return Cero
                        Else

                            Call BorrarListasReferencias(entidad.CodigoEmpresa, entidad.Numero, conexion)
                            If Not IsNothing(entidad.ListadoReferencias) Then
                                For Each ListadoReferencias In entidad.ListadoReferencias

                                    ListadoReferencias.CodigoEmpresa = entidad.CodigoEmpresa
                                    ListadoReferencias.Numero = entidad.Numero

                                    If Not InsertarlistasReferencias(ListadoReferencias, conexion) Then
                                        Exit For
                                    End If
                                Next
                            End If

                            Call BorrarListasEntidades(entidad.CodigoEmpresa, entidad.Numero, conexion)
                            If Not IsNothing(entidad.ListadoEntidades) Then
                                For Each ListadoEntidades In entidad.ListadoEntidades

                                    ListadoEntidades.CodigoEmpresa = entidad.CodigoEmpresa
                                    ListadoEntidades.Numero = entidad.Numero

                                    If Not InsertarlistasEntidades(ListadoEntidades, conexion) Then
                                        Exit For
                                    End If
                                Next
                            End If

                            DetalleDocumentos.CodigoEmpresa = entidad.CodigoEmpresa
                            DetalleDocumentos.Numero = entidad.Numero
                            DetalleDocumentos.UsuarioCrea = entidad.UsuarioCrea

                            transladartemporal = New RepositorioEstudioSeguridadDocumentos().TrasladarDocumentos(DetalleDocumentos, conexionDocumentos)

                            If transladartemporal Then
                                limpiartemporal = New RepositorioEstudioSeguridadDocumentos().LimpiarTemporalUsuario(DetalleDocumentos, conexionDocumentos)
                            End If

                            transaccion.Complete()

                        End If

                    End Using

                End Using

                Dim Documento = New RepositorioEstudioSeguridadDocumentos
                If Not IsNothing(entidad.Documentos) Then
                    For Each detalle In entidad.Documentos
                        detalle.Numero = entidad.Numero
                        If Not Documento.InsertarDetalleDocumento(detalle) Then
                            Exit For
                        End If
                    Next
                End If
            End Using
            'If entidad.Numero > 0 Then
            '    Dim Correos As New Repositorio.Utilitarios.RepositorioCorreos
            '    If entidad.Estado = 1 Then
            '        Correos.GenerarCorreo(entidad)
            '    End If
            'End If
            Return entidad.NumeroDocumento
        End Function

        Public Overrides Function Modificar(entidad As EstudioSeguridad) As Long
            Dim transladartemporal As Boolean = True
            Dim limpiartemporal As Boolean = True
            Dim DetalleDocumentos As New EstudioSeguridadDocumentos
            Using conexionDocumentos = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                conexionDocumentos.CreateConnection()
                Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                    Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                        conexion.CreateConnection()

                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                        conexion.AgregarParametroSQL("@par_Numero_Documento", entidad.NumeroDocumento)
                        conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
                        conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                        conexion.AgregarParametroSQL("@par_Placa", entidad.Placa)
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficinas.Codigo)
                        conexion.AgregarParametroSQL("@par_Estado_Autorizacion", entidad.EstadoAutorizacion)
                        conexion.AgregarParametroSQL("@par_Numero_Autorizacion", entidad.NumeroAutorizacion)
                        conexion.AgregarParametroSQL("@par_CATA_CRES_Codigo", entidad.ClaseRiesgo)
                        conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                        conexion.AgregarParametroSQL("@par_Observaciones_Estudio_Seguridad", entidad.Observaciones)
                        conexion.AgregarParametroSQL("@par_MAVE_Codigo", entidad.MarcaVehiculo)
                        conexion.AgregarParametroSQL("@par_CATA_TIVE_Codigo", entidad.TipoVehiculo)
                        conexion.AgregarParametroSQL("@par_Modelo_Vehiculo", entidad.Modelo)
                        conexion.AgregarParametroSQL("@par_Modelo_Repotenciado", entidad.ModeloRepotenciado)
                        conexion.AgregarParametroSQL("@par_COVE_Codigo", entidad.ColorVehiculo)

                        If Not IsNothing(entidad.Aseguradora) Then
                            conexion.AgregarParametroSQL("@par_TERC_Aseguradora_Seguro_Obligatorio", entidad.Aseguradora.Codigo)
                        End If

                        conexion.AgregarParametroSQL("@par_Numero_Seguro_Obligatorio", entidad.NumeroSeguro)
                        conexion.AgregarParametroSQL("@par_Fecha_Emision_Seguro_Obligatorio", entidad.FechaEmisionSeguro, SqlDbType.Date)
                        conexion.AgregarParametroSQL("@par_Fecha_Vence_Seguro_Obligatorio", entidad.FechaVenceSeguro, SqlDbType.Date)
                        conexion.AgregarParametroSQL("@par_Numero_Revision_Tecnomecanica", entidad.NumeroTecnomecanica)
                        conexion.AgregarParametroSQL("@par_Fecha_Emision_Revision_Tecnomecanica", entidad.FechaEmisionTecnomecanica, SqlDbType.Date)
                        conexion.AgregarParametroSQL("@par_Fecha_Vence_Revision_Tecnomecanica", entidad.FechaVenceTecnomecanica, SqlDbType.Date)

                        conexion.AgregarParametroSQL("@par_Nombre_Empresa_GPS", entidad.NombreEmpresaGPS)
                        conexion.AgregarParametroSQL("@par_Telefono_Empresa_GPS", entidad.TelefonoEmpresaGPS)
                        conexion.AgregarParametroSQL("@par_Usuario_GPS", entidad.UsuarioGPS)
                        conexion.AgregarParametroSQL("@par_Clave_GPS", entidad.ClaveGPS)

                        conexion.AgregarParametroSQL("@par_Empresa_Afiliadora", entidad.EmpresaAfiliadora)
                        conexion.AgregarParametroSQL("@par_Telefono_Afiliadora", entidad.TelefonoAfiliadora)
                        conexion.AgregarParametroSQL("@par_Numero_Carnet_Afiliadora", entidad.NumeroCarnetAfiliadora)
                        conexion.AgregarParametroSQL("@par_Fecha_Vence_Afiliadora", entidad.FechaVenceAfiliadora, SqlDbType.Date)

                        conexion.AgregarParametroSQL("@par_Placa_Semirremolque", entidad.PlacaSemirremolque)
                        If Not IsNothing(entidad.MarcaSemirremolque) Then
                            conexion.AgregarParametroSQL("@par_MASE_Codigo", entidad.MarcaSemirremolque.Codigo)
                        End If
                        If Not IsNothing(entidad.TipoSemirremolque) Then
                            conexion.AgregarParametroSQL("@par_CATA_TISE_Codigo", entidad.TipoSemirremolque.Codigo)
                        End If
                        If Not IsNothing(entidad.TipoCarroceriaSemirremolque) Then
                            conexion.AgregarParametroSQL("@par_CATA_TICA_Codigo", entidad.TipoCarroceriaSemirremolque.Codigo)
                        End If
                        conexion.AgregarParametroSQL("@par_Modelo_Semirremolque", entidad.ModeloSemirremolque)
                        conexion.AgregarParametroSQL("@par_Numero_Ejes", entidad.NumeroEjes)

                        If Not IsNothing(entidad.Propietario) Then
                            conexion.AgregarParametroSQL("@par_CATA_TINT_Propietario", entidad.Propietario.TipoNaturaleza.Codigo)
                            conexion.AgregarParametroSQL("@par_CATA_TIID_Propietario", entidad.Propietario.TipoIdentificacion.Codigo)
                            conexion.AgregarParametroSQL("@par_Numero_Identificacion_Propietario", entidad.Propietario.NumeroIdentificacion)
                            conexion.AgregarParametroSQL("@par_Nombre_Propietario", entidad.Propietario.Nombre)
                            conexion.AgregarParametroSQL("@par_Primero_Apellido_Propietario", entidad.Propietario.PrimeroApellido)
                            conexion.AgregarParametroSQL("@par_Segundo_Apellido_Propietario", entidad.Propietario.SegundoApellido)
                            conexion.AgregarParametroSQL("@par_Razon_Social_Propietario", entidad.Propietario.RazonSocial)
                            conexion.AgregarParametroSQL("@par_Digito_Chequeo_Propietario", entidad.Propietario.DigitoChequeo)
                            If Not IsNothing(entidad.Propietario.CiudadExpedicionIdent) Then
                                conexion.AgregarParametroSQL("@par_CIUD_Identificacion_Propietario", entidad.Propietario.CiudadExpedicionIdent.Codigo)
                            End If
                            If Not IsNothing(entidad.Propietario.Ciudad) Then
                                conexion.AgregarParametroSQL("@par_CIUD_Residencia_Propietario", entidad.Propietario.Ciudad.Codigo)
                            End If
                            conexion.AgregarParametroSQL("@par_Direccion_Propietario", entidad.Propietario.Direccion)
                            conexion.AgregarParametroSQL("@par_Telefono_Propietario", entidad.Propietario.Telefonos)
                            conexion.AgregarParametroSQL("@par_Celular_Propietario", entidad.Propietario.Celular)
                        End If

                        If Not IsNothing(entidad.PropietarioRemolque) Then
                            conexion.AgregarParametroSQL("@par_CATA_TINT_Propietario_Remolque", entidad.PropietarioRemolque.TipoNaturaleza.Codigo)
                            conexion.AgregarParametroSQL("@par_CATA_TIID_Propietario_Remolque", entidad.PropietarioRemolque.TipoIdentificacion.Codigo)
                            conexion.AgregarParametroSQL("@par_Numero_Identificacion_Propietario_Remolque", entidad.PropietarioRemolque.NumeroIdentificacion)
                            conexion.AgregarParametroSQL("@par_Nombre_Propietario_Remolque", entidad.PropietarioRemolque.Nombre)
                            conexion.AgregarParametroSQL("@par_Primero_Apellido_Propietario_Remolque", entidad.PropietarioRemolque.PrimeroApellido)
                            conexion.AgregarParametroSQL("@par_Segundo_Apellido_Propietario_Remolque", entidad.PropietarioRemolque.SegundoApellido)
                            conexion.AgregarParametroSQL("@par_Razon_Social_Propietario_Remolque", entidad.PropietarioRemolque.RazonSocial)
                            conexion.AgregarParametroSQL("@par_Digito_Chequeo_Propietario_Remolque", entidad.PropietarioRemolque.DigitoChequeo)
                            If Not IsNothing(entidad.PropietarioRemolque.CiudadExpedicionIdent) Then
                                conexion.AgregarParametroSQL("@par_CIUD_Identificacion_Propietario_Remolque", entidad.PropietarioRemolque.CiudadExpedicionIdent.Codigo)
                            End If
                            If Not IsNothing(entidad.PropietarioRemolque.Ciudad) Then
                                conexion.AgregarParametroSQL("@par_CIUD_Residencia_Propietario_Remolque", entidad.PropietarioRemolque.Ciudad.Codigo)
                            End If
                            conexion.AgregarParametroSQL("@par_Direccion_Propietario_Remolque", entidad.PropietarioRemolque.Direccion)
                            conexion.AgregarParametroSQL("@par_Telefono_Propietario_Remolque", entidad.PropietarioRemolque.Telefonos)
                            conexion.AgregarParametroSQL("@par_Celular_Propietario_Remolque", entidad.PropietarioRemolque.Celular)
                        End If

                        If Not IsNothing(entidad.Tenedor) Then
                            conexion.AgregarParametroSQL("@par_CATA_TINT_Tenedor", entidad.Tenedor.TipoNaturaleza.Codigo)
                            conexion.AgregarParametroSQL("@par_CATA_TIID_Tenedor", entidad.Tenedor.TipoIdentificacion.Codigo)
                            conexion.AgregarParametroSQL("@par_Numero_Identificacion_Tenedor", entidad.Tenedor.NumeroIdentificacion)
                            conexion.AgregarParametroSQL("@par_Nombre_Tenedor", entidad.Tenedor.Nombre)
                            conexion.AgregarParametroSQL("@par_Primero_Apellido_Tenedor", entidad.Tenedor.PrimeroApellido)
                            conexion.AgregarParametroSQL("@par_Segundo_Apellido_Tenedor", entidad.Tenedor.SegundoApellido)
                            conexion.AgregarParametroSQL("@par_Razon_Social_Tenedor", entidad.Tenedor.RazonSocial)
                            conexion.AgregarParametroSQL("@par_Digito_Chequeo_Tenedor", entidad.Tenedor.DigitoChequeo)
                            If Not IsNothing(entidad.Tenedor.CiudadExpedicionIdent) Then
                                conexion.AgregarParametroSQL("@par_CIUD_Identificacion_Tenedor", entidad.Tenedor.CiudadExpedicionIdent.Codigo)
                            End If
                            If Not IsNothing(entidad.Tenedor.Ciudad) Then
                                conexion.AgregarParametroSQL("@par_CIUD_Residencia_Tenedor", entidad.Tenedor.Ciudad.Codigo)
                            End If
                            conexion.AgregarParametroSQL("@par_Direccion_Tenedor", entidad.Tenedor.Direccion)
                            conexion.AgregarParametroSQL("@par_Telefono_Tenedor", entidad.Tenedor.Telefonos)
                            conexion.AgregarParametroSQL("@par_Celular_Tenedor", entidad.Tenedor.Celular)
                        End If

                        If Not IsNothing(entidad.Conductor) Then
                            conexion.AgregarParametroSQL("@par_CATA_TINT_Conductor", entidad.Conductor.TipoNaturaleza.Codigo)
                            conexion.AgregarParametroSQL("@par_CATA_TIID_Conductor", entidad.Conductor.TipoIdentificacion.Codigo)
                            conexion.AgregarParametroSQL("@par_Numero_Identificacion_Conductor", entidad.Conductor.NumeroIdentificacion)
                            conexion.AgregarParametroSQL("@par_Nombre_Conductor", entidad.Conductor.Nombre)
                            conexion.AgregarParametroSQL("@par_Primero_Apellido_Conductor", entidad.Conductor.PrimeroApellido)
                            conexion.AgregarParametroSQL("@par_Segundo_Apellido_Conductor", entidad.Conductor.SegundoApellido)
                            conexion.AgregarParametroSQL("@par_Razon_Social_Conductor", entidad.Conductor.RazonSocial)
                            conexion.AgregarParametroSQL("@par_Digito_Chequeo_Conductor", entidad.Conductor.DigitoChequeo)
                            If Not IsNothing(entidad.Conductor.CiudadExpedicionIdent) Then
                                conexion.AgregarParametroSQL("@par_CIUD_Identificacion_Conductor", entidad.Conductor.CiudadExpedicionIdent.Codigo)
                            End If
                            If Not IsNothing(entidad.Conductor.Ciudad) Then
                                conexion.AgregarParametroSQL("@par_CIUD_Residencia_Conductor", entidad.Conductor.Ciudad.Codigo)
                            End If
                            conexion.AgregarParametroSQL("@par_Direccion_Conductor", entidad.Conductor.Direccion)
                            conexion.AgregarParametroSQL("@par_Telefono_Conductor", entidad.Conductor.Telefonos)
                            conexion.AgregarParametroSQL("@par_Celular_Conductor", entidad.Conductor.Celular)
                        End If

                        If Not IsNothing(entidad.Cliente) Then
                            conexion.AgregarParametroSQL("@par_Nombre_Cliente", entidad.Cliente)
                        End If
                        conexion.AgregarParametroSQL("@par_Nombre_Mercancia_Cliente", entidad.MercanciaCliente)
                        If Not IsNothing(entidad.Ruta) Then
                            conexion.AgregarParametroSQL("@par_RUTA_Codigo_Cliente", entidad.Ruta.Codigo)
                        End If
                        conexion.AgregarParametroSQL("@par_Valor_Mercancia_Cliente", entidad.ValorMercancia)
                        conexion.AgregarParametroSQL("@par_Observaciones_Despacho_Cliente", entidad.ObservacionesCliente)

                        conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)

                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_modificar_encabezado_estudio_seguridad]")

                        While resultado.Read
                            entidad.NumeroDocumento = resultado.Item("Numero_Documento").ToString()
                            entidad.Numero = resultado.Item("Numero").ToString()
                        End While

                        resultado.Close()

                        If entidad.Numero.Equals(Cero) Then
                            Return Cero
                        Else

                            Call BorrarListasReferencias(entidad.CodigoEmpresa, entidad.Numero, conexion)
                            If Not IsNothing(entidad.ListadoReferencias) Then
                                For Each ListadoReferencias In entidad.ListadoReferencias

                                    ListadoReferencias.CodigoEmpresa = entidad.CodigoEmpresa
                                    ListadoReferencias.Numero = entidad.Numero

                                    If Not InsertarlistasReferencias(ListadoReferencias, conexion) Then
                                        Exit For
                                    End If
                                Next
                            End If

                            Call BorrarListasEntidades(entidad.CodigoEmpresa, entidad.Numero, conexion)
                            If Not IsNothing(entidad.ListadoEntidades) Then
                                For Each ListadoEntidades In entidad.ListadoEntidades

                                    ListadoEntidades.CodigoEmpresa = entidad.CodigoEmpresa
                                    ListadoEntidades.Numero = entidad.Numero

                                    If Not InsertarlistasEntidades(ListadoEntidades, conexion) Then
                                        Exit For
                                    End If
                                Next
                            End If

                            DetalleDocumentos.CodigoEmpresa = entidad.CodigoEmpresa
                            DetalleDocumentos.Numero = entidad.Numero
                            DetalleDocumentos.UsuarioCrea = entidad.UsuarioModifica

                            transladartemporal = New RepositorioEstudioSeguridadDocumentos().TrasladarDocumentos(DetalleDocumentos, conexionDocumentos)

                            If transladartemporal Then
                                limpiartemporal = New RepositorioEstudioSeguridadDocumentos().LimpiarTemporalUsuario(DetalleDocumentos, conexionDocumentos)
                            End If

                            transaccion.Complete()

                        End If

                    End Using

                End Using

            End Using

            Dim Documento = New RepositorioEstudioSeguridadDocumentos
            If Not IsNothing(entidad.Documentos) Then
                For Each detalle In entidad.Documentos
                    detalle.Numero = entidad.Numero
                    If Not Documento.InsertarDetalleDocumento(detalle) Then
                        Exit For
                    End If
                Next
            End If
            'If entidad.Numero > 0 Then
            '    Dim Correos As New Repositorio.Utilitarios.RepositorioCorreos
            '    If entidad.Estado = 1 Then
            '        Correos.GenerarCorreo(entidad)
            '    End If
            'End If
            Return entidad.NumeroDocumento
        End Function

        Public Overrides Function Obtener(filtro As EstudioSeguridad) As EstudioSeguridad
            Dim item As New EstudioSeguridad
            Using conexionDocumentos = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                conexionDocumentos.CreateConnection()
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                    If filtro.Numero > 0 Then
                        conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                    End If

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_encabezado_estudio_seguridad")

                    While resultado.Read
                        item = New EstudioSeguridad(resultado)
                    End While

                    If filtro.ConsularDocumentos > 0 Then
                        Dim lista As New List(Of EstudioSeguridadDocumentos)
                        Dim Seccion As EstudioSeguridadDocumentos
                        resultado.Close()
                        conexionDocumentos.CleanParameters()
                        conexionDocumentos.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                        If filtro.Numero > 0 Then
                            conexionDocumentos.AgregarParametroSQL("@par_Numero", filtro.Numero)
                        End If

                        conexionDocumentos.AgregarParametroSQL("@par_USUA_Codigo_Modifica", filtro.UsuarioModifica.Codigo)

                        resultado = conexionDocumentos.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_estudio_seguridad_documentos]")
                        While resultado.Read
                            Seccion = New EstudioSeguridadDocumentos(resultado)
                            lista.Add(Seccion)
                        End While
                        item.ListadoDocumentos = lista

                    End If

                    If item.Numero > 0 Then
                        resultado.Close()
                        item.ListadoReferencias = ConsultarListasReferencias(item.CodigoEmpresa, item.Numero, conexion, resultado)
                    End If

                    If item.Numero > 0 Then
                        resultado.Close()
                        item.ListadoEntidades = ConsultarListasEntidades(item.CodigoEmpresa, item.Numero, conexion, resultado)
                    End If

                End Using

            End Using

            Return item
        End Function
        Public Function Anular(entidad As EstudioSeguridad) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_encabezado_estudio_seguridad]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function

        Public Function BorrarListasReferencias(CodiEmpresa As Short, numEstudio As Long, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_Numero_Estudio", numEstudio)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_listas_referencias]")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function InsertarlistasReferencias(entidad As DetalleReferenciasEstudioSeguridad, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_Numero_Estudio", entidad.Numero)
            conexion.AgregarParametroSQL("@par_Tipo_Referencia", entidad.Referencia.Codigo)
            conexion.AgregarParametroSQL("@par_Observaciones", entidad.ObservacionReferencia)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_listas_referencias]")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function ConsultarListasReferencias(CodiEmpresa As Short, numEstudio As Long, ByRef conexion As Conexion.DataBaseFactory, resultado As IDataReader) As IEnumerable(Of DetalleReferenciasEstudioSeguridad)
            Dim lista As New List(Of DetalleReferenciasEstudioSeguridad)
            Dim item As DetalleReferenciasEstudioSeguridad
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_Numero_Estudio", numEstudio)

            resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_listas_referencias]")

            While resultado.Read
                item = New DetalleReferenciasEstudioSeguridad(resultado)
                lista.Add(item)
            End While
            resultado.Close()
            Return lista
        End Function

        Public Function BorrarListasEntidades(CodiEmpresa As Short, numEstudio As Long, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_Numero_Estudio", numEstudio)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_listas_entidades_consultadas]")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function InsertarlistasEntidades(entidad As DetalleEntidadesConsultadasEstudioSeguridad, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_Numero_Estudio", entidad.Numero)
            conexion.AgregarParametroSQL("@par_Entidad_Consultada", entidad.Codigo)
            conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
            conexion.AgregarParametroSQL("@par_Observaciones", entidad.ObservacionEntidades)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_listas_entidades_consultadas]")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function ConsultarListasEntidades(CodiEmpresa As Short, numEstudio As Long, ByRef conexion As Conexion.DataBaseFactory, resultado As IDataReader) As IEnumerable(Of DetalleEntidadesConsultadasEstudioSeguridad)
            Dim lista As New List(Of DetalleEntidadesConsultadasEstudioSeguridad)
            Dim item As DetalleEntidadesConsultadasEstudioSeguridad
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_Numero_Estudio", numEstudio)

            resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_listas_entidades_consultadas]")

            While resultado.Read
                item = New DetalleEntidadesConsultadasEstudioSeguridad(resultado)
                lista.Add(item)
            End While
            resultado.Close()
            Return lista
        End Function

        Public Function Autorizar(entidad As EstudioSeguridad) As Boolean
            Dim Autorizo As Boolean = False
            Dim usuario = entidad.UsuarioCrea
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", 300)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Autoriza", entidad.UsuarioCrea.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_autorizar_Estudio_Seguridad]")

                While resultado.Read
                    Autorizo = IIf((resultado.Item("Numero")) > Cero, True, False)
                End While
                'resultado.Close()
                'Dim Autorizacion = New Autorizaciones
                'Autorizacion.CodigoEmpresa = entidad.CodigoEmpresa
                'Autorizacion.EstudioSeguridad = entidad
                'Autorizacion.TipoAutorizacion = New ValorCatalogos With {.Codigo = 18401}
                'Autorizacion.NumeroAutorizacion = entidad.NumeroAutorizacion
                'Autorizacion.UsuarioSolicita = entidad.UsuarioCrea
                'Dim repAutorizacion = New RepositorioAutorizaciones()

                'Autorizo = If(repAutorizacion.InsertarAutorizacion(Autorizacion, conexion) > 0, True, False)

                resultado.Close()
            End Using
            'Dim Estidio As New RepositorioEstudioSeguridad
            'entidad = Estidio.Obtener(entidad)
            'If Autorizo Then
            '    Dim Correos As New Repositorio.Utilitarios.RepositorioCorreos
            '    If entidad.Estado = 1 Then
            '        entidad.TipoDocumento = 22001
            '        entidad.UsuarioCrea = usuario
            '        Correos.GenerarCorreo(entidad)
            '    End If
            'End If
            Return Autorizo
        End Function

        Public Function Rechazar(entidad As EstudioSeguridad) As Boolean
            Dim anulo As Boolean = False
            Dim usuario = entidad.UsuarioCrea

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Autoriza", entidad.UsuarioCrea.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_rechazar_estudio_seguridad]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > Cero, True, False)
                End While

            End Using
            'entidad = Obtener(entidad)
            'If anulo Then
            '    Dim Correos As New Repositorio.Utilitarios.RepositorioCorreos
            '    entidad.TipoDocumento = 22002
            '    entidad.UsuarioCrea = usuario
            '    Correos.GenerarCorreo(entidad)
            'End If
            Return anulo
        End Function

    End Class
End Namespace
