﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Repositorio.Basico.Despachos

Namespace Basico.Despachos
    Public NotInheritable Class PersistenciaPeajes
        Implements IPersistenciaBase(Of Peajes)

        Public Function Consultar(filtro As Peajes) As IEnumerable(Of Peajes) Implements IPersistenciaBase(Of Peajes).Consultar
            Return New RepositorioPeajes().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As Peajes) As Long Implements IPersistenciaBase(Of Peajes).Insertar
            Return New RepositorioPeajes().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As Peajes) As Long Implements IPersistenciaBase(Of Peajes).Modificar
            Return New RepositorioPeajes().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As Peajes) As Peajes Implements IPersistenciaBase(Of Peajes).Obtener
            Return New RepositorioPeajes().Obtener(filtro)
        End Function
        Public Function Anular(entidad As Peajes) As Boolean
            Return New RepositorioPeajes().Anular(entidad)
        End Function
        Public Function InsertarTarifasPeajes(entidad As Peajes) As Boolean
            Return New RepositorioPeajes().InsertarTarifasPeajes(entidad)
        End Function

        Public Function GenerarPlanitilla(filtro As Peajes) As Peajes
            Return New RepositorioPeajes().GenerarPlanitilla(filtro)
        End Function

    End Class

End Namespace

