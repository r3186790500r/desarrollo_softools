﻿Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Transactions
Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Facturacion
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Comercial.Documentos
Imports EncoExpres.GesCarga.Entidades.Despachos.Remesa
Imports EncoExpres.GesCarga.Entidades.Paqueteria
Imports EncoExpres.GesCarga.Repositorio.Basico.Despachos
Imports EncoExpres.GesCarga.Repositorio.Basico.General
Imports EncoExpres.GesCarga.Repositorio.Facturacion
Imports System.Reflection

Namespace Paqueteria

    ''' <summary>
    ''' Clase <see cref="RepositorioRemesaPaqueteria"/>
    ''' </summary>

    Public NotInheritable Class RepositorioCargueMasivoGuias
        Inherits RepositorioBase(Of RemesaPaqueteria)

        Public Overrides Function Consultar(filtro As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria)
            Dim lista As New List(Of RemesaPaqueteria)
            Dim item As RemesaPaqueteria
            Dim resultado As IDataReader
            'Using transaccion = New TransactionScope(TransactionScopeOption.Required)

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.NumeroInicial > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.NumeroInicial)
                End If
                If filtro.FechaInicial > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_FechaInicial", filtro.FechaInicial, SqlDbType.Date)
                End If

                If filtro.FechaFinal > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_FechaFinal", filtro.FechaFinal, SqlDbType.Date)
                End If

                If filtro.NumeroPlanillaInicial > 0 Then
                    conexion.AgregarParametroSQL("@par_NumeroPlanillaInicial", filtro.NumeroPlanillaInicial)
                End If

                If filtro.NumeroPlanillaFinal > 0 Then
                    conexion.AgregarParametroSQL("@par_NumeroPlanillaFinal", filtro.NumeroPlanillaFinal)
                End If

                If filtro.FechaPlanillaInicial > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_FechaPlanillaInicial", filtro.FechaPlanillaInicial, SqlDbType.Date)
                End If

                If filtro.FechaPlanillaFinal > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_FechaPlanillaFinal", filtro.FechaPlanillaFinal, SqlDbType.Date)
                End If

                If Not IsNothing(filtro.Remesa) Then
                    If filtro.Remesa.Fecha > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha", filtro.Remesa.Fecha, SqlDbType.Date)
                    End If

                    If filtro.Remesa.NumeroDocumentoCliente > 0 Then
                        conexion.AgregarParametroSQL("@par_Documento_Cliente", filtro.Remesa.NumeroDocumentoCliente)
                    End If

                    If Not IsNothing(filtro.Remesa.Cliente) Then
                        If Not IsNothing(filtro.Remesa.Cliente.NombreCompleto) And filtro.Remesa.Cliente.NombreCompleto <> String.Empty Then
                            conexion.AgregarParametroSQL("@par_NombreCliente", filtro.Remesa.Cliente.NombreCompleto)
                        End If
                        If filtro.Remesa.Cliente.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", filtro.Remesa.Cliente.Codigo)
                        End If
                    End If

                    If Not IsNothing(filtro.Remesa.ProductoTransportado) Then
                        If filtro.Remesa.ProductoTransportado.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_PRTR_Codigo", filtro.Remesa.ProductoTransportado.Codigo)
                        End If
                    End If

                    If Not IsNothing(filtro.TipoDocumento) Then
                        If filtro.TipoDocumento > 0 Then

                        End If
                    End If


                    If Not IsNothing(filtro.Remesa.Vehiculo) Then
                        If filtro.Remesa.Vehiculo.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_VEHI_Codigo", filtro.Remesa.Vehiculo.Codigo)
                        End If
                    End If

                    If Not IsNothing(filtro.Remesa.Ruta) Then

                        If Not IsNothing(filtro.Remesa.Ruta.Nombre) Then
                            conexion.AgregarParametroSQL("@par_NombreRuta", filtro.Remesa.Ruta.Nombre)
                        End If

                        If filtro.Remesa.Ruta.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_RUTA_Codigo", filtro.Remesa.Ruta.Codigo)
                        End If
                    End If

                    If Not IsNothing(filtro.Remesa.CiudadRemitente) Then

                        If Not IsNothing(filtro.Remesa.CiudadRemitente.Nombre) Then
                            conexion.AgregarParametroSQL("@par_Ciudad_Origen", filtro.Remesa.CiudadRemitente.Nombre)
                        End If

                    End If

                    If Not IsNothing(filtro.Remesa.CiudadDestinatario) Then
                        If Not IsNothing(filtro.Remesa.CiudadDestinatario.Nombre) Then
                            conexion.AgregarParametroSQL("@par_Ciudad_Destino", filtro.Remesa.CiudadDestinatario.Nombre)
                        End If
                    End If

                    If Not IsNothing(filtro.Remesa.Numeroplanillaentrega) Then
                        If filtro.Remesa.Numeroplanillaentrega > 0 Then
                            If filtro.Remesa.Numeroplanillaentrega = 1 Then
                                conexion.AgregarParametroSQL("@par_planilla_entrega", 0)
                            Else
                                conexion.AgregarParametroSQL("@par_planilla_entrega", filtro.Remesa.Numeroplanillaentrega)
                            End If
                        End If
                    End If

                    If Not IsNothing(filtro.Remesa.NumeroplanillaDespacho) Then
                        If filtro.Remesa.NumeroplanillaDespacho > 0 Then
                            If filtro.Remesa.NumeroplanillaDespacho = 1 Then
                                conexion.AgregarParametroSQL("@par_planilla_despachos", 0)
                            Else
                                conexion.AgregarParametroSQL("@par_planilla_despachos", filtro.Remesa.NumeroplanillaDespacho)
                            End If
                        End If
                    End If

                    If Not IsNothing(filtro.Remesa.Numeroplanillarecoleccion) Then
                        If filtro.Remesa.Numeroplanillarecoleccion > 0 Then
                            If filtro.Remesa.Numeroplanillarecoleccion = 1 Then
                                conexion.AgregarParametroSQL("@par_planilla_recoleccion", 0)
                            Else
                                conexion.AgregarParametroSQL("@par_planilla_recoleccion", filtro.Remesa.Numeroplanillarecoleccion)
                            End If

                        End If
                    End If

                    If Not IsNothing(filtro.Remesa.Destinatario) Then
                        conexion.AgregarParametroSQL("@par_NombreDestinatario", filtro.Remesa.Destinatario.Nombre)
                    End If
                    If Not IsNothing(filtro.Remesa.FormaPago) Then
                        If filtro.Remesa.FormaPago.Codigo <> -1 Then
                            conexion.AgregarParametroSQL("@par_Forma_Pago", filtro.Remesa.FormaPago.Codigo)
                        End If

                    End If
                    If Not IsNothing(filtro.Remesa.Remitente) Then
                        If Not IsNothing(filtro.Remesa.Remitente.Nombre) Then
                            conexion.AgregarParametroSQL("@par_NombreRemitente", filtro.Remesa.Remitente.Nombre)
                        Else
                            If Not IsNothing(filtro.Remesa.Remitente.NombreCompleto) Then
                                conexion.AgregarParametroSQL("@par_NombreRemitente", filtro.Remesa.Remitente.NombreCompleto)
                            End If
                        End If
                    End If
                End If

                If Not IsNothing(filtro.TipoEntregaRemesaPaqueteria) Then
                    If filtro.TipoEntregaRemesaPaqueteria.Codigo <> 0 And filtro.TipoEntregaRemesaPaqueteria.Codigo <> 6600 Then
                        conexion.AgregarParametroSQL("@par_CATA_TERP_Codigo", filtro.TipoEntregaRemesaPaqueteria.Codigo)
                    End If
                End If
                'If Not IsNothing(filtro.TipoTransporteRemsaPaqueteria) Then
                '    If filtro.TipoTransporteRemsaPaqueteria.Codigo <> 0 And filtro.TipoTransporteRemsaPaqueteria.Codigo <> 6200 Then
                '        conexion.AgregarParametroSQL("@par_CATA_TTRP_Codigo", filtro.TipoTransporteRemsaPaqueteria.Codigo)
                '    End If
                'End If
                If Not IsNothing(filtro.TipoDespachoRemesaPaqueteria) Then
                    If filtro.TipoDespachoRemesaPaqueteria.Codigo <> 0 And filtro.TipoDespachoRemesaPaqueteria.Codigo <> 6300 Then
                        conexion.AgregarParametroSQL("@par_CATA_TDRP_Codigo", filtro.TipoDespachoRemesaPaqueteria.Codigo)
                    End If
                End If
                'If Not IsNothing(filtro.TipoServicioRemesaPaqueteria) Then
                '    If filtro.TipoServicioRemesaPaqueteria.Codigo <> 0 And filtro.TipoServicioRemesaPaqueteria.Codigo <> 6500 Then
                '        conexion.AgregarParametroSQL("@par_CATA_TSRP_Codigo", filtro.TipoServicioRemesaPaqueteria.Codigo)
                '    End If
                'End If



                If Not IsNothing(filtro.EstadoRemesaPaqueteria) Then

                    If filtro.EstadoRemesaPaqueteria.Codigo > 0 And filtro.EstadoRemesaPaqueteria.Codigo > 6000 Then
                        conexion.AgregarParametroSQL("@par_CATA_ESRP_Codigo", filtro.EstadoRemesaPaqueteria.Codigo)
                    End If
                End If

                If filtro.NumeroPlanilla > -1 Then
                    conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.NumeroPlanilla)
                End If

                If filtro.Estado > -1 And filtro.Estado < 2 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If

                If filtro.Estado = 2 Then
                    conexion.AgregarParametroSQL("@par_Anulado", SI_APLICA)
                End If


                If filtro.CodigoOficina > 0 Then
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.CodigoOficina)

                ElseIf Not IsNothing(filtro.Oficina) Then
                    If filtro.Oficina.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficina.Codigo)
                    End If

                End If

                If Not IsNothing(filtro.CodigoCiudad) Then
                    If filtro.CodigoCiudad > 0 Then
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo", filtro.CodigoCiudad)
                    End If
                End If

                If Not IsNothing(filtro.CodigoCiudadActual) Then
                    If filtro.CodigoCiudadActual > 0 Then
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo_Actual", filtro.CodigoCiudadActual)
                    End If
                End If

                If Not IsNothing(filtro.CodigoOficinaActualUsuario) Then
                    If filtro.CodigoOficinaActualUsuario > 0 Then
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo_Actual_Usuario", filtro.CodigoOficinaActualUsuario)
                    End If
                End If

                If Not IsNothing(filtro.CodigoCiudadOrigen) Then
                    If filtro.CodigoCiudadOrigen > 0 Then
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo_Origen", filtro.CodigoCiudadOrigen)
                    End If
                End If

                If Not IsNothing(filtro.OficinaOrigen) Then
                    If Not IsNothing(filtro.OficinaOrigen.Codigo) Then
                        If filtro.OficinaOrigen.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_OFIC_Codigo_Origen", filtro.OficinaOrigen.Codigo)
                        End If
                    End If
                End If
                If Not IsNothing(filtro.OficinaActual) Then

                    If Not IsNothing(filtro.OficinaActual.Codigo) Then
                        If filtro.OficinaActual.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_OFIC_Codigo_Actual", filtro.OficinaActual.Codigo)
                        End If
                    End If
                End If

                If Not IsNothing(filtro.CodigoZona) Then
                    If filtro.CodigoZona > 0 Then
                        conexion.AgregarParametroSQL("@par_ZOCI_Codigo", filtro.CodigoZona)
                    End If
                End If

                If Not IsNothing(filtro.Cumplido) Then
                    conexion.AgregarParametroSQL("@par_Cumplido", filtro.Cumplido)
                    conexion.AgregarParametroSQL("@par_Planillado", 0)
                End If

                If Not IsNothing(filtro.Numeracion) Then
                    If filtro.Numeracion <> "" Then
                        conexion.AgregarParametroSQL("@par_Numeracion", filtro.Numeracion)
                    End If
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If filtro.CodigoCiudadPlanillaRecoleccion > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo_Ciudad_Planilla_Recolecciones", filtro.CodigoCiudadPlanillaRecoleccion)
                End If

                If filtro.CodigoOficinaPlanillaRecoleccion > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo_Oficina_Planilla_Recolecciones", filtro.CodigoOficinaPlanillaRecoleccion)
                End If

                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                If Not IsNothing(filtro.UsuarioConsulta) Then
                    If filtro.UsuarioConsulta.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_USUA_Codigo_Consultar", filtro.UsuarioConsulta.Codigo)
                    End If
                End If

                '----------------Nuevos Filtros -----------
                If Not IsNothing(filtro.OficinaRecibe) Then
                    If filtro.OficinaRecibe.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo_Recibe", filtro.OficinaRecibe.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.RecogerOficinaDestino) Then
                    If filtro.RecogerOficinaDestino > 0 Then
                        conexion.AgregarParametroSQL("@par_RecogerOfficina", filtro.RecogerOficinaDestino)
                    End If
                End If

                If Not IsNothing(filtro.BarrioDestinatario) Then
                    If filtro.BarrioDestinatario <> "" Then
                        conexion.AgregarParametroSQL("@par_Barrio", filtro.BarrioDestinatario)
                    End If
                End If

                If filtro.AsociarRemesasPlanilla > 0 Then
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_remesa_paqueteria_asociar_planilla")
                ElseIf filtro.AsociarRemesasRecolecciones > 0 Then
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_remesa_paqueteria_asociar_recolecciones")
                ElseIf filtro.AsociarRemesasEntrega > 0 Then
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_remesa_paqueteria_asociar_entrega")
                ElseIf filtro.PlanillaEntrega Then
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_guias_Planilla_Entrega")
                    'ElseIf filtro.ConsultarControlEntregas > 0 Then
                    '    resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_remesa_paqueteria_control_entregas")
                ElseIf filtro.actualizaestado Then
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_guias_norecibidas")
                ElseIf filtro.TipoDocumentoOrigen = 135 Then
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_guias_norecibidas_paqueteria")
                    'DM'
                ElseIf filtro.IndicadorPaqueteria = 1 Then
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_indicadores_remesas_X_Oficina")
                ElseIf filtro.IndicadorPaqueteria = 2 Then
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_indicadores_remesas_X_Cliente")
                ElseIf filtro.IndicadorPaqueteria = 3 Then
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_indicadores_remesas_X_CiudadDestino")
                ElseIf filtro.IndicadorPaqueteria = 4 Then
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_indicadores_Ventas_X_Oficina")
                ElseIf filtro.IndicadorPaqueteria = 5 Then
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_indicadores_Ventas_X_Cliente")
                ElseIf filtro.IndicadorPaqueteria = 6 Then
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_indicadores_ventas_X_CiudadDestino")
                ElseIf Not IsNothing(filtro.Planilla) Then
                    If filtro.Planilla.TipoDocumento = 135 Then 'PlanillaPaqueteria
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_remesa_paqueteria_control_entregas_planilla_paqueteria")
                    Else
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_remesa_paqueteria")
                    End If
                Else
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_remesa_paqueteria")
                End If

                While resultado.Read
                    item = New RemesaPaqueteria(resultado)
                    item.ListaReexpedicionOficinas = ConsultarDetalleReexpedicionConsulta(filtro.CodigoEmpresa, item.Remesa.Numero)
                    If filtro.ConsultarControlEntregas = 1 Then
                        item.GestionDocumentosRemesa = ConsultarDetalleGestionDocumentos(filtro.CodigoEmpresa, item.Remesa.Numero)
                    End If
                    lista.Add(item)
                End While

                'conexion.CloseConnection()
            End Using
            'End Using
            Return lista
        End Function


        Public Function ConsultarGuiasEtiquetasPreimpresas(filtro As RemesaPaqueteria) As IEnumerable(Of DetalleGuiaPreimpresa)
            Dim lista As New List(Of DetalleGuiaPreimpresa)
            Dim item As DetalleGuiaPreimpresa
            Dim resultado As IDataReader

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero_Etiqueta ", filtro.NumeroInicial)

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_detalle_guias_etiquetas_preimpresas")

                While resultado.Read
                    item = New DetalleGuiaPreimpresa(resultado)
                    lista.Add(item)
                End While
            End Using
            Return lista
        End Function

        Public Function ConsultarGuiasPlanillaEntrega(filtro As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria)
            Dim lista As New List(Of RemesaPaqueteria)
            Dim item As RemesaPaqueteria
            Dim resultado As IDataReader
            'Using transaccion = New TransactionScope(TransactionScopeOption.Required)

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.NumeroInicial > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.NumeroInicial)
                End If
                If filtro.FechaInicial > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_FechaInicial", filtro.FechaInicial, SqlDbType.Date)
                End If

                If filtro.FechaFinal > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_FechaFinal", filtro.FechaFinal, SqlDbType.Date)
                End If

                If filtro.NumeroPlanillaInicial > 0 Then
                    conexion.AgregarParametroSQL("@par_NumeroPlanillaInicial", filtro.NumeroPlanillaInicial)
                End If

                If filtro.NumeroPlanillaFinal > 0 Then
                    conexion.AgregarParametroSQL("@par_NumeroPlanillaFinal", filtro.NumeroPlanillaFinal)
                End If

                If filtro.FechaPlanillaInicial > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_FechaPlanillaInicial", filtro.FechaPlanillaInicial, SqlDbType.Date)
                End If

                If filtro.FechaPlanillaFinal > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_FechaPlanillaFinal", filtro.FechaPlanillaFinal, SqlDbType.Date)
                End If

                If Not IsNothing(filtro.Remesa) Then
                    If filtro.Remesa.Fecha > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha", filtro.Remesa.Fecha, SqlDbType.Date)
                    End If

                    If filtro.Remesa.NumeroDocumentoCliente > 0 Then
                        conexion.AgregarParametroSQL("@par_Documento_Cliente", filtro.Remesa.NumeroDocumentoCliente)
                    End If

                    If Not IsNothing(filtro.Remesa.Cliente) Then
                        If Not IsNothing(filtro.Remesa.Cliente.NombreCompleto) And filtro.Remesa.Cliente.NombreCompleto <> String.Empty Then
                            conexion.AgregarParametroSQL("@par_NombreCliente", filtro.Remesa.Cliente.NombreCompleto)
                        End If
                        If filtro.Remesa.Cliente.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", filtro.Remesa.Cliente.Codigo)
                        End If
                    End If

                    If Not IsNothing(filtro.Remesa.ProductoTransportado) Then
                        If filtro.Remesa.ProductoTransportado.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_PRTR_Codigo", filtro.Remesa.ProductoTransportado.Codigo)
                        End If
                    End If

                    If Not IsNothing(filtro.TipoDocumento) Then
                        If filtro.TipoDocumento > 0 Then
                            conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento)
                        End If
                    End If


                    If Not IsNothing(filtro.Remesa.Vehiculo) Then
                        If filtro.Remesa.Vehiculo.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_VEHI_Codigo", filtro.Remesa.Vehiculo.Codigo)
                        End If
                    End If

                    If Not IsNothing(filtro.Remesa.Ruta) Then

                        If Not IsNothing(filtro.Remesa.Ruta.Nombre) Then
                            conexion.AgregarParametroSQL("@par_NombreRuta", filtro.Remesa.Ruta.Nombre)
                        End If

                        If filtro.Remesa.Ruta.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_RUTA_Codigo", filtro.Remesa.Ruta.Codigo)
                        End If
                    End If

                    If Not IsNothing(filtro.Remesa.CiudadRemitente) Then

                        If Not IsNothing(filtro.Remesa.CiudadRemitente.Nombre) Then
                            conexion.AgregarParametroSQL("@par_Ciudad_Origen", filtro.Remesa.CiudadRemitente.Nombre)
                        End If

                    End If

                    If Not IsNothing(filtro.Remesa.CiudadDestinatario) Then
                        If Not IsNothing(filtro.Remesa.CiudadDestinatario.Nombre) Then
                            conexion.AgregarParametroSQL("@par_Ciudad_Destino", filtro.Remesa.CiudadDestinatario.Nombre)
                        End If
                    End If

                    If Not IsNothing(filtro.Remesa.Numeroplanillaentrega) Then
                        If filtro.Remesa.Numeroplanillaentrega > 0 Then
                            If filtro.Remesa.Numeroplanillaentrega = 1 Then
                                conexion.AgregarParametroSQL("@par_planilla_entrega", 0)
                            Else
                                conexion.AgregarParametroSQL("@par_planilla_entrega", filtro.Remesa.Numeroplanillaentrega)
                            End If
                        End If
                    End If

                    If Not IsNothing(filtro.Remesa.NumeroplanillaDespacho) Then
                        If filtro.Remesa.NumeroplanillaDespacho > 0 Then
                            If filtro.Remesa.NumeroplanillaDespacho = 1 Then
                                conexion.AgregarParametroSQL("@par_planilla_despachos", 0)
                            Else
                                conexion.AgregarParametroSQL("@par_planilla_despachos", filtro.Remesa.NumeroplanillaDespacho)
                            End If
                        End If
                    End If

                    If Not IsNothing(filtro.Remesa.Numeroplanillarecoleccion) Then
                        If filtro.Remesa.Numeroplanillarecoleccion > 0 Then
                            If filtro.Remesa.Numeroplanillarecoleccion = 1 Then
                                conexion.AgregarParametroSQL("@par_planilla_recoleccion", 0)
                            Else
                                conexion.AgregarParametroSQL("@par_planilla_recoleccion", filtro.Remesa.Numeroplanillarecoleccion)
                            End If

                        End If
                    End If

                    If Not IsNothing(filtro.Remesa.Destinatario) Then
                        conexion.AgregarParametroSQL("@par_NombreDestinatario", filtro.Remesa.Destinatario.Nombre)
                    End If
                    If Not IsNothing(filtro.Remesa.FormaPago) Then
                        If filtro.Remesa.FormaPago.Codigo <> -1 Then
                            conexion.AgregarParametroSQL("@par_Forma_Pago", filtro.Remesa.FormaPago.Codigo)
                        End If

                    End If
                    If Not IsNothing(filtro.Remesa.Remitente) Then
                        If Not IsNothing(filtro.Remesa.Remitente.Nombre) Then
                            conexion.AgregarParametroSQL("@par_NombreRemitente", filtro.Remesa.Remitente.Nombre)
                        Else
                            If Not IsNothing(filtro.Remesa.Remitente.NombreCompleto) Then
                                conexion.AgregarParametroSQL("@par_NombreRemitente", filtro.Remesa.Remitente.NombreCompleto)
                            End If
                        End If
                    End If
                End If

                If Not IsNothing(filtro.TipoEntregaRemesaPaqueteria) Then
                    If filtro.TipoEntregaRemesaPaqueteria.Codigo <> 0 And filtro.TipoEntregaRemesaPaqueteria.Codigo <> 6600 Then
                        conexion.AgregarParametroSQL("@par_CATA_TERP_Codigo", filtro.TipoEntregaRemesaPaqueteria.Codigo)
                    End If
                End If
                'If Not IsNothing(filtro.TipoTransporteRemsaPaqueteria) Then
                '    If filtro.TipoTransporteRemsaPaqueteria.Codigo <> 0 And filtro.TipoTransporteRemsaPaqueteria.Codigo <> 6200 Then
                '        conexion.AgregarParametroSQL("@par_CATA_TTRP_Codigo", filtro.TipoTransporteRemsaPaqueteria.Codigo)
                '    End If
                'End If
                If Not IsNothing(filtro.TipoDespachoRemesaPaqueteria) Then
                    If filtro.TipoDespachoRemesaPaqueteria.Codigo <> 0 And filtro.TipoDespachoRemesaPaqueteria.Codigo <> 6300 Then
                        conexion.AgregarParametroSQL("@par_CATA_TDRP_Codigo", filtro.TipoDespachoRemesaPaqueteria.Codigo)
                    End If
                End If
                'If Not IsNothing(filtro.TipoServicioRemesaPaqueteria) Then
                '    If filtro.TipoServicioRemesaPaqueteria.Codigo <> 0 And filtro.TipoServicioRemesaPaqueteria.Codigo <> 6500 Then
                '        conexion.AgregarParametroSQL("@par_CATA_TSRP_Codigo", filtro.TipoServicioRemesaPaqueteria.Codigo)
                '    End If
                'End If



                If Not IsNothing(filtro.EstadoRemesaPaqueteria) Then

                    If filtro.EstadoRemesaPaqueteria.Codigo > 0 And filtro.EstadoRemesaPaqueteria.Codigo > 6000 Then
                        conexion.AgregarParametroSQL("@par_CATA_ESRP_Codigo", filtro.EstadoRemesaPaqueteria.Codigo)
                    End If
                End If

                If filtro.NumeroPlanilla > -1 Then
                    conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.NumeroPlanilla)
                End If

                If filtro.Estado > -1 And filtro.Estado < 2 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If

                If filtro.Estado = 2 Then
                    conexion.AgregarParametroSQL("@par_Anulado", SI_APLICA)
                End If


                If filtro.CodigoOficina > 0 Then
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.CodigoOficina)

                ElseIf Not IsNothing(filtro.Oficina) Then
                    If filtro.Oficina.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficina.Codigo)
                    End If

                End If

                If Not IsNothing(filtro.CodigoCiudad) Then
                    If filtro.CodigoCiudad > 0 Then
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo", filtro.CodigoCiudad)
                    End If
                End If

                If Not IsNothing(filtro.CodigoCiudadActual) Then
                    If filtro.CodigoCiudadActual > 0 Then
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo_Actual", filtro.CodigoCiudadActual)
                    End If
                End If

                If Not IsNothing(filtro.CodigoOficinaActualUsuario) Then
                    If filtro.CodigoOficinaActualUsuario > 0 Then
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo_Actual_Usuario", filtro.CodigoOficinaActualUsuario)
                    End If
                End If

                If Not IsNothing(filtro.CodigoCiudadOrigen) Then
                    If filtro.CodigoCiudadOrigen > 0 Then
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo_Origen", filtro.CodigoCiudadOrigen)
                    End If
                End If

                If Not IsNothing(filtro.OficinaOrigen) Then
                    If Not IsNothing(filtro.OficinaOrigen.Codigo) Then
                        If filtro.OficinaOrigen.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_OFIC_Codigo_Origen", filtro.OficinaOrigen.Codigo)
                        End If
                    End If
                End If
                If Not IsNothing(filtro.OficinaActual) Then

                    If Not IsNothing(filtro.OficinaActual.Codigo) Then
                        If filtro.OficinaActual.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_OFIC_Codigo_Actual", filtro.OficinaActual.Codigo)
                        End If
                    End If
                End If

                If Not IsNothing(filtro.CodigoZona) Then
                    If filtro.CodigoZona > 0 Then
                        conexion.AgregarParametroSQL("@par_ZOCI_Codigo", filtro.CodigoZona)
                    End If
                End If

                If Not IsNothing(filtro.Zona) Then
                    If filtro.Zona.Nombre <> "" Then
                        conexion.AgregarParametroSQL("@par_ZOCI_Nombre", filtro.Zona.Nombre)
                    End If
                End If

                If Not IsNothing(filtro.Cumplido) Then
                    conexion.AgregarParametroSQL("@par_Cumplido", filtro.Cumplido)
                    conexion.AgregarParametroSQL("@par_Planillado", 0)
                End If

                If Not IsNothing(filtro.Numeracion) Then
                    If filtro.Numeracion <> "" Then
                        conexion.AgregarParametroSQL("@par_Numeracion", filtro.Numeracion)
                    End If
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If filtro.CodigoCiudadPlanillaRecoleccion > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo_Ciudad_Planilla_Recolecciones", filtro.CodigoCiudadPlanillaRecoleccion)
                End If

                If filtro.CodigoOficinaPlanillaRecoleccion > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo_Oficina_Planilla_Recolecciones", filtro.CodigoOficinaPlanillaRecoleccion)
                End If

                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                If Not IsNothing(filtro.UsuarioConsulta) Then
                    If filtro.UsuarioConsulta.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_USUA_Codigo_Consultar", filtro.UsuarioConsulta.Codigo)
                    End If
                End If

                '----------------Nuevos Filtros -----------
                If Not IsNothing(filtro.OficinaRecibe) Then
                    If filtro.OficinaRecibe.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo_Recibe", filtro.OficinaRecibe.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.RecogerOficinaDestino) Then
                    If filtro.RecogerOficinaDestino > 0 Then
                        conexion.AgregarParametroSQL("@par_RecogerOfficina", filtro.RecogerOficinaDestino)
                    End If
                End If

                If Not IsNothing(filtro.BarrioDestinatario) Then
                    If filtro.BarrioDestinatario <> "" Then
                        conexion.AgregarParametroSQL("@par_Barrio", filtro.BarrioDestinatario)
                    End If
                End If


                resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_guia_paqueteria_control_entregas_planilla_entrega")


                While resultado.Read
                    item = New RemesaPaqueteria(resultado)
                    'item.ListaReexpedicionOficinas = ConsultarDetalleReexpedicionConsulta(filtro.CodigoEmpresa, item.Remesa.Numero)
                    'If filtro.ConsultarControlEntregas = 1 Then
                    '    item.GestionDocumentosRemesa = ConsultarDetalleGestionDocumentos(filtro.CodigoEmpresa, item.Remesa.Numero)
                    'End If
                    lista.Add(item)
                End While

                'conexion.CloseConnection()
            End Using
            'End Using
            Return lista
        End Function

        Public Overrides Function Insertar(entidad As RemesaPaqueteria) As Long
            Dim inserto As Boolean = True

            'Insertar Producto
            If Not IsNothing(entidad.Remesa.ProductoTransportado) Then
                If entidad.Remesa.ProductoTransportado.Codigo = 0 Then
                    Dim Producto = New Repositorio.Basico.Despachos.RepositorioProductoTransportados
                    entidad.Remesa.ProductoTransportado.CodigoEmpresa = entidad.CodigoEmpresa
                    entidad.Remesa.ProductoTransportado.UsuarioCrea = entidad.UsuarioCrea
                    entidad.Remesa.ProductoTransportado.Codigo = Producto.Insertar(entidad.Remesa.ProductoTransportado)
                End If
            End If

            Dim tercero = New Repositorio.Basico.General.RepositorioTerceros

            'Insertar Remitente/Cliente
            If Not IsNothing(entidad.Remesa.Remitente) Then
                If entidad.Remesa.Remitente.Codigo = 0 Then
                    entidad.Remesa.Remitente.CodigoEmpresa = entidad.CodigoEmpresa
                    entidad.Remesa.Remitente.UsuaCodigoCrea = entidad.UsuarioCrea.Codigo
                    entidad.Remesa.Remitente.CadenaPerfiles = "1404,1410,"
                    entidad.Remesa.Remitente.Estado = New Estado With {.Codigo = 1}
                    If entidad.Remesa.Remitente.TipoIdentificacion.Codigo = 102 Then
                        'NIT
                        entidad.Remesa.Remitente.RazonSocial = entidad.Remesa.Remitente.Nombre
                        entidad.Remesa.Remitente.TipoNaturaleza = New ValorCatalogos With {.Codigo = 502}
                    Else
                        entidad.Remesa.Remitente.RazonSocial = ""
                        entidad.Remesa.Remitente.TipoNaturaleza = New ValorCatalogos With {.Codigo = 501}
                    End If
                    entidad.Remesa.Remitente.Correo = entidad.Remesa.Remitente.CorreoFacturacion
                    entidad.Remesa.Remitente.Codigo = tercero.Insertar(entidad.Remesa.Remitente)
                    entidad.Remesa.Cliente = entidad.Remesa.Remitente
                Else
                    If entidad.Remesa.Remitente.CorreoFacturacion <> String.Empty Then
                        Dim Terc As Terceros = New Terceros With {.CodigoEmpresa = entidad.CodigoEmpresa, .Codigo = entidad.Remesa.Remitente.Codigo}
                        Terc = tercero.Obtener(Terc)
                        If Terc.CorreoFacturacion <> entidad.Remesa.Remitente.CorreoFacturacion Then
                            entidad.Remesa.Remitente.CodigoEmpresa = entidad.CodigoEmpresa
                            entidad.Remesa.Remitente.UsuaCodigoCrea = entidad.UsuarioCrea.Codigo
                            Terc.Codigo = tercero.ActualizarEmailFacturaElectronica(entidad.Remesa.Remitente)
                        End If
                    End If
                End If
            End If
            'Insertar Destinatario
            If Not IsNothing(entidad.Remesa.Destinatario) Then
                If entidad.Remesa.Destinatario.Codigo = 0 Then 'para destinatarios nuevos
                    If entidad.Remesa.Destinatario.NumeroIdentificacion <> entidad.Remesa.Remitente.NumeroIdentificacion Then 'si identificacion es igual entre remitente y destinatario, no crea, actualiza codigo
                        entidad.Remesa.Destinatario.CodigoEmpresa = entidad.CodigoEmpresa
                        entidad.Remesa.Destinatario.UsuaCodigoCrea = entidad.UsuarioCrea.Codigo
                        entidad.Remesa.Destinatario.CadenaPerfiles = "1404,1410," ' Destinatario y Remitente
                        entidad.Remesa.Destinatario.Estado = New Estado With {.Codigo = 1}
                        If entidad.Remesa.Destinatario.TipoIdentificacion.Codigo = 102 Then
                            'NIT
                            entidad.Remesa.Destinatario.RazonSocial = entidad.Remesa.Destinatario.Nombre
                            entidad.Remesa.Destinatario.TipoNaturaleza = New ValorCatalogos With {.Codigo = 502}
                        Else
                            entidad.Remesa.Destinatario.RazonSocial = ""
                            entidad.Remesa.Destinatario.TipoNaturaleza = New ValorCatalogos With {.Codigo = 501}
                        End If
                        entidad.Remesa.Destinatario.Correo = entidad.Remesa.Destinatario.CorreoFacturacion
                        entidad.Remesa.Destinatario.Codigo = tercero.Insertar(entidad.Remesa.Destinatario)
                    Else
                        entidad.Remesa.Destinatario.Codigo = entidad.Remesa.Remitente.Codigo
                    End If
                Else
                    If entidad.Remesa.Destinatario.CorreoFacturacion <> String.Empty Then
                        Dim Terc As Terceros = New Terceros With {.CodigoEmpresa = entidad.CodigoEmpresa, .Codigo = entidad.Remesa.Destinatario.Codigo}
                        Terc = tercero.Obtener(Terc)
                        If Terc.CorreoFacturacion <> entidad.Remesa.Destinatario.CorreoFacturacion Then
                            entidad.Remesa.Destinatario.CodigoEmpresa = entidad.CodigoEmpresa
                            entidad.Remesa.Destinatario.UsuaCodigoCrea = entidad.UsuarioCrea.Codigo
                            Terc.Codigo = tercero.ActualizarEmailFacturaElectronica(entidad.Remesa.Destinatario)
                        End If
                    End If
                End If
            End If

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()


                    conexion.CreateConnection()
                    conexion.CleanParameters()
                    'Inserta la remesa'
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)

                    If Not IsNothing(entidad.Remesa) Then

                        conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.Remesa.TipoDocumento.Codigo)

                        If Not IsNothing(entidad.Remesa.TipoRemesa) Then
                            If entidad.Remesa.TipoRemesa.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_CATA_TIRE_Codigo", entidad.Remesa.TipoRemesa.Codigo)
                            End If
                        End If

                        If Not IsNothing(entidad.Remesa.NumeroDocumentoCliente) Then
                            conexion.AgregarParametroSQL("@par_Documento_Cliente", entidad.Remesa.NumeroDocumentoCliente, SqlDbType.VarChar)
                        Else
                            conexion.AgregarParametroSQL("@par_Documento_Cliente", VACIO)
                        End If

                        conexion.AgregarParametroSQL("@par_Fecha_Documento_Cliente", entidad.Remesa.FechaDocumentoCliente, SqlDbType.Date)
                        conexion.AgregarParametroSQL("@par_Remesa_Cortesia", entidad.Remesa.RemesaCortesia)
                        '5

                        conexion.AgregarParametroSQL("@par_Fecha", entidad.Remesa.Fecha, SqlDbType.Date)
                        conexion.AgregarParametroSQL("@par_RUTA_Codigo", entidad.Remesa.Ruta.Codigo)
                        conexion.AgregarParametroSQL("@par_PRTR_Codigo", entidad.Remesa.ProductoTransportado.Codigo)
                        conexion.AgregarParametroSQL("@par_CATA_FOPR_Codigo", entidad.Remesa.FormaPago.Codigo)

                        If Not IsNothing(entidad.Remesa.Cliente) Then
                            If entidad.Remesa.Cliente.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", entidad.Remesa.Cliente.Codigo)
                            End If
                        End If

                        '10
                        If Not IsNothing(entidad.Remesa.Remitente) Then
                            If entidad.Remesa.Remitente.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_TERC_Codigo_Remitente", entidad.Remesa.Remitente.Codigo)
                            End If
                        End If
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo_Remitente", entidad.Remesa.CiudadRemitente.Codigo)
                        conexion.AgregarParametroSQL("@par_Direccion_Remitente", entidad.Remesa.DireccionRemitente)
                        conexion.AgregarParametroSQL("@par_Telefonos_Remitente", entidad.Remesa.TelefonoRemitente, SqlDbType.VarChar)
                        If Not IsNothing(entidad.Remesa.Observaciones) Then
                            conexion.AgregarParametroSQL("@par_Observaciones", entidad.Remesa.Observaciones)
                        Else
                            conexion.AgregarParametroSQL("@par_Observaciones", "")
                        End If
                        '15

                        conexion.AgregarParametroSQL("@par_Cantidad_Cliente", entidad.Remesa.CantidadCliente)
                        conexion.AgregarParametroSQL("@par_Peso_Cliente", entidad.Remesa.PesoCliente, SqlDbType.Float)
                        conexion.AgregarParametroSQL("@par_Peso_Volumetrico_Cliente", entidad.Remesa.PesoVolumetricoCliente, SqlDbType.Float)
                        conexion.AgregarParametroSQL("@par_DTCV_Codigo", entidad.Remesa.DetalleTarifaVenta.Codigo)
                        conexion.AgregarParametroSQL("@par_Valor_Flete_Cliente", entidad.Remesa.ValorFleteCliente, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Valor_Manejo_Cliente", entidad.Remesa.ValorManejoCliente, SqlDbType.Money)
                        If entidad.Remesa.ValorComisionOficina > 0 Then
                            conexion.AgregarParametroSQL("@par_Comision_Oficina", entidad.Remesa.ValorComisionOficina, SqlDbType.Money)
                        End If
                        If entidad.Remesa.ValorComisionAgencista > 0 Then
                            conexion.AgregarParametroSQL("@par_Comision_Agencista", entidad.Remesa.ValorComisionAgencista, SqlDbType.Money)
                        End If
                        '20

                        conexion.AgregarParametroSQL("@par_Valor_Seguro_Cliente", entidad.Remesa.ValorSeguroCliente, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Valor_Descuento_Cliente", entidad.Remesa.ValorDescuentoCliente, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Total_Flete_Cliente", entidad.Remesa.TotalFleteCliente, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Valor_Comercial_Cliente", entidad.Remesa.ValorComercialCliente, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Cantidad_Transportador", entidad.Remesa.CantidadTransportador)
                        '25

                        conexion.AgregarParametroSQL("@par_Peso_Transportador", entidad.Remesa.PesoTransportador, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Valor_Flete_Transportador", entidad.Remesa.ValorFleteTransportador, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Valor_Reexpedicion", entidad.ValorReexpedicion, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Total_Flete_Transportador", entidad.Remesa.TotalFleteTransportador, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Destinatario", entidad.Remesa.Destinatario.Codigo)
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo_Destinatario", entidad.Remesa.CiudadDestinatario.Codigo)
                        '30

                        conexion.AgregarParametroSQL("@par_Direccion_Destinatario", entidad.Remesa.DireccionDestinatario)
                        conexion.AgregarParametroSQL("@par_Telefonos_Destinatario", entidad.Remesa.TelefonoDestinatario, SqlDbType.VarChar)
                        conexion.AgregarParametroSQL("@par_Estado", entidad.Remesa.Estado)
                        conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
                        '35

                        If Not IsNothing(entidad.Remesa.TarifarioCompra) Then
                            If entidad.Remesa.TarifarioCompra.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_ETCC_Numero", entidad.Remesa.TarifarioCompra.Codigo)
                            End If
                        End If
                        '40

                        If Not IsNothing(entidad.Remesa.TarifarioVenta) Then
                            If entidad.Remesa.TarifarioVenta.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_ETCV_Numero", entidad.Remesa.TarifarioVenta.Codigo)
                            End If
                        End If
                        conexion.AgregarParametroSQL("@par_ESOS_Numero", entidad.Remesa.NumeroOrdenServicio)

                        If Not IsNothing(entidad.Remesa.Conductor) Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", entidad.Remesa.Conductor.Codigo)
                        End If


                        If Not IsNothing(entidad.Remesa.Vehiculo) Then
                            If entidad.Remesa.Vehiculo.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Remesa.Vehiculo.Codigo)
                            End If
                        End If


                        If Not IsNothing(entidad.Remesa.Planilla) Then
                            If entidad.Remesa.Planilla.Numero > 0 Then
                                conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Remesa.Planilla.Numero)
                            End If
                        End If

                    End If

                    If Not IsNothing(entidad.Remesa.BarrioRemitente) Then
                        conexion.AgregarParametroSQL("@par_Barrio_Remitente", entidad.Remesa.BarrioRemitente)
                    End If
                    If Not IsNothing(entidad.Remesa.CodigoPostalRemitente) Then
                        conexion.AgregarParametroSQL("@par_Codigo_Postal_Remitente", entidad.Remesa.CodigoPostalRemitente)
                    End If
                    If Not IsNothing(entidad.Remesa.BarrioDestinatario) Then
                        conexion.AgregarParametroSQL("@par_Barrio_Destinatario", entidad.Remesa.BarrioDestinatario)
                    End If
                    If Not IsNothing(entidad.Remesa.CodigoPostalDestinatario) Then
                        conexion.AgregarParametroSQL("@par_Codigo_Postal_Destinatario", entidad.Remesa.CodigoPostalDestinatario)
                    End If
                    conexion.AgregarParametroSQL("@par_Valor_Otros", entidad.Remesa.ValorOtros)
                    conexion.AgregarParametroSQL("@par_Valor_Cargue", entidad.Remesa.ValorCargue)
                    conexion.AgregarParametroSQL("@par_Valor_Descargue", entidad.Remesa.ValorDescargue)


                    If Not IsNothing(entidad.Numeracion) Then
                        If entidad.Numeracion <> "" Then
                            conexion.AgregarParametroSQL("@par_Numeracion", entidad.Numeracion)
                        End If
                    End If
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_remesas")

                    While resultado.Read
                        entidad.Remesa.Numero = Convert.ToInt64(resultado.Item("Numero").ToString())
                        entidad.Remesa.NumeroDocumento = Convert.ToInt64(resultado.Item("Numero_Documento").ToString())
                    End While

                    resultado.Close()

                    If entidad.Remesa.Numero > 0 Then
                        conexion.CleanParameters()

                        'INSERTA REMESA PAQUETERIA'
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                        conexion.AgregarParametroSQL("@par_ENRE_Numero", entidad.Remesa.Numero)

                        If Not IsNothing(entidad.TransportadorExterno) Then
                            If entidad.TransportadorExterno.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_TERC_Codigo_Transportador_Externo", entidad.TransportadorExterno.Codigo)
                            End If
                        End If


                        If Not IsNothing(entidad.NumeroGuiaExterna) Then
                            conexion.AgregarParametroSQL("@par_Numero_Guia_Externa", entidad.NumeroGuiaExterna)
                        Else
                            conexion.AgregarParametroSQL("@par_Numero_Guia_Externa", "")
                        End If

                        If Not IsNothing(entidad.TipoTransporteRemsaPaqueteria) Then
                            If entidad.TipoTransporteRemsaPaqueteria.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_CATA_TTRP_Codigo", entidad.TipoTransporteRemsaPaqueteria.Codigo)
                            End If
                        End If

                        If Not IsNothing(entidad.TipoDespachoRemesaPaqueteria) Then
                            If entidad.TipoDespachoRemesaPaqueteria.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_CATA_TDRP_Codigo", entidad.TipoDespachoRemesaPaqueteria.Codigo)
                            End If
                        End If

                        If Not IsNothing(entidad.TemperaturaProductoRemesaPaqueteria) Then
                            If entidad.TemperaturaProductoRemesaPaqueteria.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_CATA_TPRP_Codigo", entidad.TemperaturaProductoRemesaPaqueteria.Codigo)
                            End If
                        End If

                        If Not IsNothing(entidad.TipoServicioRemesaPaqueteria) Then
                            If entidad.TipoServicioRemesaPaqueteria.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_CATA_TSRP_Codigo", entidad.TipoServicioRemesaPaqueteria.Codigo)
                            End If
                        End If

                        If Not IsNothing(entidad.TipoEntregaRemesaPaqueteria) Then
                            If entidad.TipoEntregaRemesaPaqueteria.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_CATA_TERP_Codigo", entidad.TipoEntregaRemesaPaqueteria.Codigo)
                            End If
                        End If

                        If Not IsNothing(entidad.TipoInterfazRemesaPaqueteria) Then
                            If entidad.TipoInterfazRemesaPaqueteria.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_CATA_TIRP_Codigo", entidad.TipoInterfazRemesaPaqueteria.Codigo)
                            End If
                        End If

                        If entidad.FechaInterfazTracking > Date.MinValue Then
                            conexion.AgregarParametroSQL("@par_Fecha_Interfaz_Tracking", entidad.FechaInterfazTracking, SqlDbType.DateTime)
                        End If

                        If entidad.FechaInterfazWMS > Date.MinValue Then
                            conexion.AgregarParametroSQL("@par_Fecha_Interfaz_WMS", entidad.FechaInterfazWMS, SqlDbType.DateTime)
                        End If

                        If Not IsNothing(entidad.EstadoRemesaPaqueteria) Then
                            If entidad.EstadoRemesaPaqueteria.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_CATA_ESRP_Codigo", entidad.EstadoRemesaPaqueteria.Codigo)
                            End If
                        End If



                        If Not IsNothing(entidad.ObservacionesRemitente) Then
                            conexion.AgregarParametroSQL("@par_Observaciones_Remitente", entidad.ObservacionesRemitente)
                        Else
                            conexion.AgregarParametroSQL("@par_Observaciones_Remitente", VACIO)
                        End If
                        If Not IsNothing(entidad.ObservacionesDestinatario) Then
                            conexion.AgregarParametroSQL("@par_Observaciones_Destinatario", entidad.ObservacionesDestinatario)
                        Else
                            conexion.AgregarParametroSQL("@par_Observaciones_Destinatario", VACIO)
                        End If

                        conexion.AgregarParametroSQL("@par_CIUD_Codigo_Origen", entidad.Remesa.CiudadRemitente.Codigo)
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo_Destino", entidad.Remesa.CiudadDestinatario.Codigo)

                        conexion.AgregarParametroSQL("@par_OFIC_Codigo_Origen", entidad.OficinaOrigen.Codigo)
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo_Destino", entidad.OficinaDestino.Codigo)
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo_Actual", entidad.OficinaActual.Codigo)
                        conexion.AgregarParametroSQL("@par_Descripcion_Mercancia", entidad.DesripcionMercancia)
                        conexion.AgregarParametroSQL("@par_Reexpedicion", entidad.Reexpedicion)

                        conexion.AgregarParametroSQL("@par_Centro_Costo", entidad.CentroCosto)
                        If Not IsNothing(entidad.LineaSerivicioCliente) Then
                            conexion.AgregarParametroSQL("@par_CATA_LISC_Codigo", entidad.LineaSerivicioCliente.Codigo)
                        End If
                        If Not IsNothing(entidad.SitioCargue) Then
                            conexion.AgregarParametroSQL("@par_SICD_Codigo_Cargue", entidad.SitioCargue.Codigo)
                        End If
                        If Not IsNothing(entidad.SitioDescargue) Then
                            conexion.AgregarParametroSQL("@par_SICD_Codigo_Descargue", entidad.SitioDescargue.Codigo)
                        End If
                        If entidad.FechaEntrega > Date.MinValue Then
                            conexion.AgregarParametroSQL("@par_Fecha_Entrega", entidad.FechaEntrega, SqlDbType.DateTime)
                        End If

                        conexion.AgregarParametroSQL("@par_Largo", entidad.Largo, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Alto", entidad.Alto, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Ancho", entidad.Ancho, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Peso_Volumetrico", entidad.PesoVolumetrico, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Peso_A_Cobrar", entidad.PesoCobrar, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Flete_Pactado", entidad.FletePactado, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Ajuste_Flete", entidad.AjusteFlete, SqlDbType.Money)

                        If Not IsNothing(entidad.LineaNegocioPaqueteria) Then
                            If entidad.LineaNegocioPaqueteria.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_Linea_Negocio_Paqueteria", entidad.LineaNegocioPaqueteria.Codigo)
                            End If
                        End If

                        If entidad.ManejaDetalleUnidades >= 0 Then
                            conexion.AgregarParametroSQL("@par_Maneja_Detalle_Unidades", entidad.ManejaDetalleUnidades)
                        End If

                        If entidad.RecogerOficinaDestino >= 0 Then
                            conexion.AgregarParametroSQL("@par_Recoger_Oficina_Destino", entidad.RecogerOficinaDestino)
                        End If

                        If Not IsNothing(entidad.OficinaRecibe) Then
                            If entidad.OficinaRecibe.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_OFIC_Codigo_Recibe", entidad.OficinaRecibe.Codigo)
                            End If
                        End If

                        If entidad.CodigoZona > 0 Then
                            conexion.AgregarParametroSQL("@par_ZONA_Codigo", entidad.CodigoZona)
                        End If

                        conexion.AgregarParametroSQL("@par_Latitud", entidad.Latitud, SqlDbType.Decimal)
                        conexion.AgregarParametroSQL("@par_Longitud", entidad.Longitud, SqlDbType.Decimal)

                        If Not IsNothing(entidad.HorarioEntregaRemesa) Then
                            If entidad.HorarioEntregaRemesa.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_HERP_Codigo", entidad.HorarioEntregaRemesa.Codigo)
                            End If
                        End If

                        conexion.AgregarParametroSQL("@par_Liquidar_Unidad", entidad.LiquidarUnidad)

                        If Not IsNothing(entidad.Aforador) Then
                            If entidad.Aforador.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_TERC_Codigo_Aforador", entidad.Aforador.Codigo)
                            End If
                        End If

                        If Not IsNothing(entidad.OficinaRegistroManual) Then
                            If entidad.OficinaRegistroManual.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_OFIC_Codigo_Registro_Manual", entidad.OficinaRegistroManual.Codigo)
                            End If
                        End If
                        conexion.AgregarParametroSQL("@par_Guia_Creada_Ruta_Conductor", entidad.GuiaCreadaRutaConductor)
                        conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_remesa_paqueteria")

                        While resultado.Read
                            entidad.Remesa.Numero = Convert.ToInt64(resultado.Item("Numero").ToString())
                        End While

                        resultado.Close()

                        'Call BorrarDirecciones(entidad.CodigoEmpresa, entidad.Codigo, conexion)
                        If Not IsNothing(entidad.ListaReexpedicionOficinas) Then
                            For Each item In entidad.ListaReexpedicionOficinas
                                item.CodigoEmpresa = entidad.CodigoEmpresa
                                item.CodigoRemesaPaqueteria = entidad.Remesa.Numero
                                If Not InsertarReexpedicion(item, conexion) Then
                                    Exit For
                                End If
                            Next
                        End If

                        'Agrega Detalle Unidades Remesa Paqueteria
                        If entidad.ManejaDetalleUnidades = 1 Then
                            BorrarDetalleUnidades(entidad.CodigoEmpresa, entidad.Remesa.Numero, conexion)
                            For Each item In entidad.DetalleUnidades
                                item.CodigoEmpresa = entidad.CodigoEmpresa
                                item.NumeroRemesa = entidad.Remesa.Numero
                                If Not InsertarDetalleUnidades(item, conexion) Then
                                    Exit For
                                End If
                            Next
                        End If
                        'Agregar Documentos Gestion Remesas
                        If Not IsNothing(entidad.GestionDocumentosRemesa) Then
                            If entidad.GestionDocumentosRemesa.Count > 0 Then
                                For Each item In entidad.GestionDocumentosRemesa
                                    item.CodigoEmpresa = entidad.CodigoEmpresa
                                    item.NumeroRemesa = entidad.Remesa.Numero
                                    If Not InsertarDetalleGestionDocumentos(item, conexion) Then
                                        Exit For
                                    End If
                                Next
                            End If
                        End If
                    Else
                        transaccion.Dispose()
                        Return entidad.Remesa.Numero
                    End If
                    conexion.CloseConnection()
                    conexion.Dispose()
                End Using
                'Factura
                If entidad.Remesa.Estado = 1 Then
                    If Not IsNothing(entidad.Factura) Then
                        If Not IsNothing(entidad.Factura.FormaPago) Then
                            If entidad.Factura.Cliente.Codigo = 0 Then
                                Select Case entidad.Factura.FormaPago.Codigo
                                    Case 4902 'Contado
                                        If Not IsNothing(entidad.Remesa.Cliente) Then
                                            entidad.Factura.Cliente = entidad.Remesa.Cliente
                                            entidad.Factura.FacturarA = entidad.Remesa.Cliente
                                        Else
                                            entidad.Factura.Cliente = entidad.Remesa.Remitente
                                            entidad.Factura.FacturarA = entidad.Remesa.Remitente
                                        End If
                                    Case 4903 'Contra Entregas
                                        entidad.Factura.Cliente = entidad.Remesa.Destinatario
                                        entidad.Factura.FacturarA = entidad.Remesa.Destinatario
                                End Select
                            End If
                            If entidad.Remesa.PesoCliente <= 0 Then
                                entidad.Remesa.PesoCliente = entidad.PesoCobrar
                            End If
                            Dim RemeFactura As List(Of DetalleFacturas) = New List(Of DetalleFacturas)
                            RemeFactura.Add(New DetalleFacturas With {.Remesas = entidad.Remesa})

                            Dim Factura As Facturas = entidad.Factura
                            Factura.ValorRemesas = entidad.Remesa.TotalFleteCliente
                            Factura.ValorFactura = entidad.Remesa.TotalFleteCliente
                            Factura.Subtotal = entidad.Remesa.TotalFleteCliente
                            Factura.Remesas = RemeFactura
                            Dim FactRepo = New RepositorioFacturas()
                            Dim ResultFactRepo = FactRepo.InsertarFacturaRemesasPaqueteria(Factura)

                            If ResultFactRepo <= 0 Then
                                entidad.MensajeSQL = "No se pudo Generar la factura"
                                entidad.Remesa.Numero = 0
                                entidad.Remesa.NumeroDocumento = 0
                            End If
                        End If

                    End If
                End If
                If entidad.Remesa.Numero > 0 Then
                    transaccion.Complete()
                End If
            End Using

            'Documental
            If Not IsNothing(entidad.ListadoDocumentos) Then
                If entidad.ListadoDocumentos.Count > 0 Then
                    BorrarDocumentosRemesa(entidad.CodigoEmpresa, entidad.Remesa.Numero, entidad.Remesa.TipoDocumento.Codigo)
                    Dim DoucRepo = New RepositorioDocumentos()
                    For Each item In entidad.ListadoDocumentos
                        item.CodigoEmpresa = entidad.CodigoEmpresa
                        item.Numero = entidad.Remesa.Numero
                        item.Temporal = False
                        Dim ResultDoucRepo = DoucRepo.InsertarDocumentoRemesa(item)
                    Next
                End If
            End If

            Return entidad.Remesa.NumeroDocumento
        End Function

        Public Sub BorrarDocumentosRemesa(empresa As Integer, NumeroRemesa As Integer, TipoDocumento As Integer)
            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", empresa)
                conexion.AgregarParametroSQL("@par_ENRE_Numero", NumeroRemesa)
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", TipoDocumento)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_documentos_remesa")
                If resultado.RecordsAffected > 0 Then
                    resultado.Close()
                End If
                resultado.Close()
            End Using
        End Sub

        Private Function RemesaOficinaDestino(entidad As RemesaPaqueteria) As Boolean
            Dim inserto As Boolean = True

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Guias_Seleccionadas", entidad.CadenaGuias)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gps_guias_oficina_destino")

                While resultado.Read

                    entidad.Numero = resultado.Item("Numero").ToString()
                    entidad.MensajeSQL = resultado.Item("Mensaje").ToString()
                End While
                resultado.Close()
                If entidad.Numero.Equals(Cero) Then
                    inserto = False
                End If

            End Using



            Return inserto
        End Function

        Private Function RecepcionRemesaPaqueteriaOficina(entidad As RemesaPaqueteria) As Long
            Dim inserto As Boolean = True
            Dim insertoRecorrido As Boolean = True
            Dim insertoConcepto As Boolean = True



            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ESRP_Numero", entidad.EstadoRemesaPaqueteria.Codigo)
                conexion.AgregarParametroSQL("@par_OFIC_Codigo_Actual", entidad.CodigoOficinaActual)
                conexion.AgregarParametroSQL("@par_Ciudad_Actual", entidad.CodigoCiudad)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_recepcion_guias_oficina")

                While resultado.Read

                    entidad.NumeroDocumento = resultado.Item("Numero").ToString()

                End While
                If entidad.NumeroDocumento <> Cero Then
                    CONTADOR += 1
                End If
                resultado.Close()
            End Using

            entidad.NumeroDocumento = CONTADOR


            Return entidad.NumeroDocumento
        End Function

        'Cambiar Estado Remesas Paqueteria
        Public Function CambiarEstadoRemesasPaqueteria(entidad As RemesaPaqueteria) As Long
            Dim inserto As Boolean = False
            Dim insertoFactura As Boolean = True
            Dim CONTADOR As Long = 0

            For Each listadetalleAuxi In entidad.RemesaPaqueteria
                Dim Remesa = New RemesaPaqueteria With {.CodigoEmpresa = entidad.CodigoEmpresa, .Numero = listadetalleAuxi.Numero}
                Dim RepoRemesas = New RepositorioRemesaPaqueteria()
                Remesa = RepoRemesas.Obtener(Remesa)

                Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                    Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                        conexion.CreateConnection()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_Numero", listadetalleAuxi.Numero)
                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_estado_remesas_paqueteria")

                        While resultado.Read
                            entidad.NumeroDocumento = resultado.Item("Numero").ToString()
                            inserto = True
                        End While
                        resultado.Close()
                    End Using
                    If inserto Then
                        If Not IsNothing(Remesa.Remesa.FormaPago) Then
                            If Remesa.Remesa.FormaPago.Codigo = 4902 Or Remesa.Remesa.FormaPago.Codigo = 4903 Then
                                'Objeto Factura
                                Dim Factura As Facturas = New Facturas With {
                                    .CodigoEmpresa = Remesa.CodigoEmpresa,
                                    .TipoDocumento = 170,
                                    .NumeroPreImpreso = 0,
                                    .CataTipoFactura = 5201,
                                    .Fecha = Remesa.Remesa.Fecha,
                                    .OficinaFactura = Remesa.Oficina,
                                    .Cliente = Remesa.Remesa.Cliente,
                                    .FacturarA = Remesa.Remesa.Cliente,
                                    .FormaPago = Remesa.Remesa.FormaPago,
                                    .DiasPlazo = 0,
                                    .Observaciones = "",
                                    .ValorRemesas = Remesa.Remesa.TotalFleteCliente,
                                    .ValorOtrosConceptos = 0,
                                    .ValorDescuentos = 0,
                                    .Subtotal = Remesa.Remesa.TotalFleteCliente,
                                    .ValorImpuestos = 0,
                                    .ValorFactura = Remesa.Remesa.TotalFleteCliente,
                                    .ValorTRM = 0,
                                    .Moneda = New Monedas With {.Codigo = 1},
                                    .ValorMonedaAlterna = 0,
                                    .ValorAnticipo = 0,
                                    .ResolucionFacturacion = "",
                                    .ControlImpresion = 1,
                                    .Estado = 1,
                                    .UsuarioCrea = entidad.UsuarioCrea,
                                    .Anulado = 0
                                }

                                Select Case Remesa.Remesa.FormaPago.Codigo
                                    Case 4902 'Contado
                                        If Not IsNothing(Remesa.Remesa.Cliente) Then
                                            If Remesa.Remesa.Cliente.Codigo > 0 Then
                                                Factura.Cliente = Remesa.Remesa.Cliente
                                                Factura.FacturarA = Remesa.Remesa.Cliente
                                            Else
                                                Factura.Cliente = Remesa.Remesa.Remitente
                                                Factura.FacturarA = Remesa.Remesa.Remitente
                                            End If
                                        Else
                                            Factura.Cliente = Remesa.Remesa.Remitente
                                            Factura.FacturarA = Remesa.Remesa.Remitente
                                        End If
                                    Case 4903 'Contra Entregas
                                        Factura.Cliente = Remesa.Remesa.Destinatario
                                        Factura.FacturarA = Remesa.Remesa.Destinatario
                                End Select
                                Dim RemeFactura As List(Of DetalleFacturas) = New List(Of DetalleFacturas)
                                RemeFactura.Add(New DetalleFacturas With {.Remesas = Remesa.Remesa})

                                Factura.Remesas = RemeFactura
                                Dim FactRepo = New RepositorioFacturas()
                                Dim ResultFactRepo = FactRepo.InsertarFacturaRemesasPaqueteria(Factura)

                                If ResultFactRepo <= 0 Then
                                    entidad.MensajeSQL = "No se pudo Generar la factura"
                                    insertoFactura = False
                                Else
                                    CONTADOR += 1
                                    insertoFactura = True
                                End If
                            End If
                        End If
                    End If
                    If inserto And insertoFactura Then
                        transaccion.Complete()
                    Else
                        transaccion.Dispose()
                    End If
                End Using
                inserto = False
                insertoFactura = True
            Next
            entidad.NumeroDocumento = CONTADOR
            Return entidad.NumeroDocumento
        End Function

        Public Overrides Function Modificar(entidad As RemesaPaqueteria) As Long
            Dim inserto As Double
            Try

                If entidad.actualizaestado = 1 Then
                    Using transaccion = New TransactionScope(TransactionScopeOption.Required)

                        If RemesaOficinaDestino(entidad) Then
                            If Not IsNothing(entidad.RemesaPaqueteria) Then
                                Dim EstadoRemesaPaqueteria = entidad.RemesaPaqueteria
                                For Each listadetalleAuxi In entidad.RemesaPaqueteria
                                    inserto = RecepcionRemesaPaqueteriaOficina(listadetalleAuxi)
                                Next
                            End If
                        End If
                        If entidad.Numero.Equals(Cero) Then
                            Return Cero
                        End If
                        If inserto > 0 Then
                            entidad.Remesa.NumeroDocumento = inserto
                            transaccion.Complete()
                        Else
                            transaccion.Dispose()
                            entidad.Numero = Cero
                        End If
                    End Using
                Else

                    'Insertar Producto
                    If Not IsNothing(entidad.Remesa.ProductoTransportado) Then
                        If entidad.Remesa.ProductoTransportado.Codigo = 0 Then
                            Dim Producto = New Repositorio.Basico.Despachos.RepositorioProductoTransportados
                            entidad.Remesa.ProductoTransportado.CodigoEmpresa = entidad.CodigoEmpresa
                            entidad.Remesa.ProductoTransportado.UsuarioCrea = entidad.UsuarioCrea
                            entidad.Remesa.ProductoTransportado.Codigo = Producto.Insertar(entidad.Remesa.ProductoTransportado)
                        End If
                    End If


                    Dim tercero = New Repositorio.Basico.General.RepositorioTerceros

                    'Insertar Remitente/Cliente
                    If Not IsNothing(entidad.Remesa.Remitente) Then
                        If entidad.Remesa.Remitente.Codigo = 0 Then
                            entidad.Remesa.Remitente.CodigoEmpresa = entidad.CodigoEmpresa
                            entidad.Remesa.Remitente.UsuaCodigoCrea = entidad.UsuarioCrea.Codigo
                            entidad.Remesa.Remitente.CadenaPerfiles = "1404,1410,"
                            entidad.Remesa.Remitente.RazonSocial = ""
                            entidad.Remesa.Remitente.TipoNaturaleza = New ValorCatalogos With {.Codigo = 501}
                            entidad.Remesa.Remitente.Correo = entidad.Remesa.Remitente.CorreoFacturacion
                            entidad.Remesa.Remitente.Codigo = tercero.Insertar(entidad.Remesa.Remitente)
                            entidad.Remesa.Cliente = entidad.Remesa.Remitente
                        Else
                            If entidad.Remesa.Remitente.CorreoFacturacion <> String.Empty Then
                                Dim Terc As Terceros = New Terceros With {.CodigoEmpresa = entidad.CodigoEmpresa, .Codigo = entidad.Remesa.Remitente.Codigo}
                                Terc = tercero.Obtener(Terc)
                                If Terc.CorreoFacturacion <> entidad.Remesa.Remitente.CorreoFacturacion Then
                                    entidad.Remesa.Remitente.CodigoEmpresa = entidad.CodigoEmpresa
                                    entidad.Remesa.Remitente.UsuaCodigoCrea = entidad.UsuarioCrea.Codigo
                                    Terc.Codigo = tercero.ActualizarEmailFacturaElectronica(entidad.Remesa.Remitente)
                                End If
                            End If
                        End If
                    End If
                    'Insertar Destinatario
                    If Not IsNothing(entidad.Remesa.Destinatario) Then
                        If entidad.Remesa.Destinatario.Codigo = 0 And entidad.Remesa.Destinatario.NumeroIdentificacion <> entidad.Remesa.Remitente.NumeroIdentificacion Then
                            entidad.Remesa.Destinatario.CodigoEmpresa = entidad.CodigoEmpresa
                            entidad.Remesa.Destinatario.UsuaCodigoCrea = entidad.UsuarioCrea.Codigo
                            entidad.Remesa.Destinatario.CadenaPerfiles = "1404,1410," ' Destinatario y Remitente
                            entidad.Remesa.Destinatario.TipoNaturaleza = New ValorCatalogos With {.Codigo = 501}
                            entidad.Remesa.Remitente.Correo = entidad.Remesa.Remitente.CorreoFacturacion
                            entidad.Remesa.Destinatario.Codigo = tercero.Insertar(entidad.Remesa.Destinatario)
                        Else
                            If entidad.Remesa.Destinatario.CorreoFacturacion <> String.Empty Then
                                Dim Terc As Terceros = New Terceros With {.CodigoEmpresa = entidad.CodigoEmpresa, .Codigo = entidad.Remesa.Destinatario.Codigo}
                                Terc = tercero.Obtener(Terc)
                                If Terc.CorreoFacturacion <> entidad.Remesa.Destinatario.CorreoFacturacion Then
                                    entidad.Remesa.Destinatario.CodigoEmpresa = entidad.CodigoEmpresa
                                    entidad.Remesa.Destinatario.UsuaCodigoCrea = entidad.UsuarioCrea.Codigo
                                    Terc.Codigo = tercero.ActualizarEmailFacturaElectronica(entidad.Remesa.Destinatario)
                                End If
                            End If
                        End If
                    End If
                    Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                        Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                            conexion.CreateConnection()
                            conexion.CleanParameters()
                            'Modifica la remesa'
                            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,


                            If Not IsNothing(entidad.Remesa) Then
                                conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.Remesa.TipoDocumento.Codigo)
                                conexion.AgregarParametroSQL("@par_Numero", entidad.Remesa.Numero)
                                If Not IsNothing(entidad.Remesa.TipoRemesa) Then
                                    If entidad.Remesa.TipoRemesa.Codigo > 0 Then
                                        conexion.AgregarParametroSQL("@par_CATA_TIRE_Codigo", entidad.Remesa.TipoRemesa.Codigo)
                                    End If
                                End If

                                If Not IsNothing(entidad.Remesa.NumeroDocumentoCliente) Then
                                    conexion.AgregarParametroSQL("@par_Documento_Cliente", entidad.Remesa.NumeroDocumentoCliente, SqlDbType.VarChar)
                                Else
                                    conexion.AgregarParametroSQL("@par_Documento_Cliente", VACIO)
                                End If

                                conexion.AgregarParametroSQL("@par_Fecha_Documento_Cliente", entidad.Remesa.FechaDocumentoCliente, SqlDbType.Date)
                                conexion.AgregarParametroSQL("@par_Remesa_Cortesia", entidad.Remesa.RemesaCortesia)
                                '5

                                conexion.AgregarParametroSQL("@par_Fecha", entidad.Remesa.Fecha, SqlDbType.Date)
                                conexion.AgregarParametroSQL("@par_RUTA_Codigo", entidad.Remesa.Ruta.Codigo)
                                conexion.AgregarParametroSQL("@par_PRTR_Codigo", entidad.Remesa.ProductoTransportado.Codigo)
                                conexion.AgregarParametroSQL("@par_CATA_FOPR_Codigo", entidad.Remesa.FormaPago.Codigo)
                                If Not IsNothing(entidad.Remesa.Cliente) Then
                                    If entidad.Remesa.Cliente.Codigo > 0 Then
                                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", entidad.Remesa.Cliente.Codigo)
                                    End If
                                End If

                                '10
                                If Not IsNothing(entidad.Remesa.Remitente) Then
                                    If entidad.Remesa.Remitente.Codigo > 0 Then
                                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Remitente", entidad.Remesa.Remitente.Codigo)
                                    End If
                                End If
                                conexion.AgregarParametroSQL("@par_CIUD_Codigo_Remitente", entidad.Remesa.CiudadRemitente.Codigo)
                                conexion.AgregarParametroSQL("@par_Direccion_Remitente", entidad.Remesa.DireccionRemitente)
                                conexion.AgregarParametroSQL("@par_Telefonos_Remitente", entidad.Remesa.TelefonoRemitente, SqlDbType.VarChar)
                                If Not IsNothing(entidad.Remesa.Observaciones) Then
                                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Remesa.Observaciones)
                                Else
                                    conexion.AgregarParametroSQL("@par_Observaciones", "")
                                End If

                                '15
                                conexion.AgregarParametroSQL("@par_Cantidad_Cliente", entidad.Remesa.CantidadCliente)
                                conexion.AgregarParametroSQL("@par_Peso_Cliente", entidad.Remesa.PesoCliente, SqlDbType.Money)
                                conexion.AgregarParametroSQL("@par_Peso_Volumetrico_Cliente", entidad.Remesa.PesoVolumetricoCliente, SqlDbType.Float)
                                conexion.AgregarParametroSQL("@par_DTCV_Codigo", entidad.Remesa.DetalleTarifaVenta.Codigo)
                                conexion.AgregarParametroSQL("@par_Valor_Flete_Cliente", entidad.Remesa.ValorFleteCliente, SqlDbType.Money)
                                conexion.AgregarParametroSQL("@par_Valor_Manejo_Cliente", entidad.Remesa.ValorManejoCliente, SqlDbType.Money)
                                If entidad.Remesa.ValorComisionOficina > 0 Then
                                    conexion.AgregarParametroSQL("@par_Comision_Oficina", entidad.Remesa.ValorComisionOficina, SqlDbType.Money)
                                End If
                                If entidad.Remesa.ValorComisionAgencista > 0 Then
                                    conexion.AgregarParametroSQL("@par_Comision_Agencista", entidad.Remesa.ValorComisionAgencista, SqlDbType.Money)
                                End If

                                '20
                                conexion.AgregarParametroSQL("@par_Valor_Seguro_Cliente", entidad.Remesa.ValorSeguroCliente, SqlDbType.Money)
                                conexion.AgregarParametroSQL("@par_Valor_Descuento_Cliente", entidad.Remesa.ValorDescuentoCliente, SqlDbType.Money)
                                conexion.AgregarParametroSQL("@par_Total_Flete_Cliente", entidad.Remesa.TotalFleteCliente, SqlDbType.Money)
                                conexion.AgregarParametroSQL("@par_Valor_Comercial_Cliente", entidad.Remesa.ValorComercialCliente, SqlDbType.Money)
                                conexion.AgregarParametroSQL("@par_Cantidad_Transportador", entidad.Remesa.CantidadTransportador)

                                '25
                                conexion.AgregarParametroSQL("@par_Peso_Transportador", entidad.Remesa.PesoTransportador, SqlDbType.Money)
                                conexion.AgregarParametroSQL("@par_Valor_Flete_Transportador", entidad.Remesa.ValorFleteTransportador, SqlDbType.Money)
                                conexion.AgregarParametroSQL("@par_Valor_Reexpedicion", entidad.ValorReexpedicion, SqlDbType.Money)
                                conexion.AgregarParametroSQL("@par_Total_Flete_Transportador", entidad.Remesa.TotalFleteTransportador, SqlDbType.Money)
                                conexion.AgregarParametroSQL("@par_TERC_Codigo_Destinatario", entidad.Remesa.Destinatario.Codigo)
                                conexion.AgregarParametroSQL("@par_CIUD_Codigo_Destinatario", entidad.Remesa.CiudadDestinatario.Codigo)

                                '30
                                conexion.AgregarParametroSQL("@par_Direccion_Destinatario", entidad.Remesa.DireccionDestinatario)
                                conexion.AgregarParametroSQL("@par_Telefonos_Destinatario", entidad.Remesa.TelefonoDestinatario, SqlDbType.VarChar)
                                conexion.AgregarParametroSQL("@par_Estado", entidad.Remesa.Estado)
                                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.Remesa.UsuarioCrea.Codigo)
                                conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)

                                '35
                                If Not IsNothing(entidad.Remesa.TarifarioCompra) Then
                                    If entidad.Remesa.TarifarioCompra.Codigo > 0 Then
                                        conexion.AgregarParametroSQL("@par_ETCC_Numero", entidad.Remesa.TarifarioCompra.Codigo)
                                    End If
                                End If
                                '40

                                If Not IsNothing(entidad.Remesa.TarifarioVenta) Then
                                    If entidad.Remesa.TarifarioVenta.Codigo > 0 Then
                                        conexion.AgregarParametroSQL("@par_ETCV_Numero", entidad.Remesa.TarifarioVenta.Codigo)
                                    End If
                                End If
                                conexion.AgregarParametroSQL("@par_ESOS_Numero", entidad.Remesa.NumeroOrdenServicio)

                                If Not IsNothing(entidad.Remesa.Conductor) Then
                                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", entidad.Remesa.Conductor.Codigo)
                                End If


                                If Not IsNothing(entidad.Remesa.Vehiculo) Then
                                    If entidad.Remesa.Vehiculo.Codigo > 0 Then
                                        conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Remesa.Vehiculo.Codigo)
                                    End If
                                End If


                                If Not IsNothing(entidad.Remesa.Planilla) Then
                                    If entidad.Remesa.Planilla.Numero > 0 Then
                                        conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Remesa.Planilla.Numero)
                                    End If
                                End If

                            End If

                            If Not IsNothing(entidad.Remesa.BarrioRemitente) Then
                                conexion.AgregarParametroSQL("@par_Barrio_Remitente", entidad.Remesa.BarrioRemitente)
                            End If
                            If Not IsNothing(entidad.Remesa.CodigoPostalRemitente) Then
                                conexion.AgregarParametroSQL("@par_Codigo_Postal_Remitente", entidad.Remesa.CodigoPostalRemitente)
                            End If
                            If Not IsNothing(entidad.Remesa.BarrioDestinatario) Then
                                conexion.AgregarParametroSQL("@par_Barrio_Destinatario", entidad.Remesa.BarrioDestinatario)
                            End If
                            If Not IsNothing(entidad.Remesa.CodigoPostalDestinatario) Then
                                conexion.AgregarParametroSQL("@par_Codigo_Postal_Destinatario", entidad.Remesa.CodigoPostalDestinatario)
                            End If

                            If Not IsNothing(entidad.Numeracion) Then
                                If entidad.Numeracion <> "" Then
                                    conexion.AgregarParametroSQL("@par_Numeracion", entidad.Numeracion)
                                End If
                            End If

                            Dim resultado = conexion.ExecuteReaderStoreProcedure("gsp_modificar_encabezado_remesa")

                            While resultado.Read
                                entidad.Remesa.Numero = resultado.Item("Numero").ToString()
                                entidad.Remesa.NumeroDocumento = resultado.Item("Numero_Documento").ToString()
                            End While

                            resultado.Close()

                            'MODIFICAR REMESA PAQUETERIA'
                            If entidad.Remesa.Numero > 0 Then
                                conexion.CleanParameters()

                                'MODIFICAR REMESA PAQUETERIA'
                                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                                conexion.AgregarParametroSQL("@par_ENRE_Numero", entidad.Remesa.Numero)

                                If Not IsNothing(entidad.TransportadorExterno) Then
                                    If entidad.TransportadorExterno.Codigo > 0 Then
                                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Transportador_Externo", entidad.TransportadorExterno.Codigo)
                                    End If
                                End If


                                If Not IsNothing(entidad.NumeroGuiaExterna) Then
                                    conexion.AgregarParametroSQL("@par_Numero_Guia_Externa", entidad.NumeroGuiaExterna)
                                Else
                                    conexion.AgregarParametroSQL("@par_Numero_Guia_Externa", "")
                                End If

                                If Not IsNothing(entidad.TipoTransporteRemsaPaqueteria) Then
                                    If entidad.TipoTransporteRemsaPaqueteria.Codigo > 0 Then
                                        conexion.AgregarParametroSQL("@par_CATA_TTRP_Codigo", entidad.TipoTransporteRemsaPaqueteria.Codigo)
                                    End If
                                End If

                                If Not IsNothing(entidad.TipoDespachoRemesaPaqueteria) Then
                                    If entidad.TipoDespachoRemesaPaqueteria.Codigo > 0 Then
                                        conexion.AgregarParametroSQL("@par_CATA_TDRP_Codigo", entidad.TipoDespachoRemesaPaqueteria.Codigo)
                                    End If
                                End If

                                If Not IsNothing(entidad.TemperaturaProductoRemesaPaqueteria) Then
                                    If entidad.TemperaturaProductoRemesaPaqueteria.Codigo > 0 Then
                                        conexion.AgregarParametroSQL("@par_CATA_TPRP_Codigo", entidad.TemperaturaProductoRemesaPaqueteria.Codigo)
                                    End If
                                End If

                                If Not IsNothing(entidad.TipoServicioRemesaPaqueteria) Then
                                    If entidad.TipoServicioRemesaPaqueteria.Codigo > 0 Then
                                        conexion.AgregarParametroSQL("@par_CATA_TSRP_Codigo", entidad.TipoServicioRemesaPaqueteria.Codigo)
                                    End If
                                End If

                                If Not IsNothing(entidad.TipoEntregaRemesaPaqueteria) Then
                                    If entidad.TipoEntregaRemesaPaqueteria.Codigo > 0 Then
                                        conexion.AgregarParametroSQL("@par_CATA_TERP_Codigo", entidad.TipoEntregaRemesaPaqueteria.Codigo)
                                    End If
                                End If

                                If Not IsNothing(entidad.TipoInterfazRemesaPaqueteria.Codigo) Then
                                    If entidad.TipoInterfazRemesaPaqueteria.Codigo > 0 Then
                                        conexion.AgregarParametroSQL("@par_CATA_TIRP_Codigo", entidad.TipoInterfazRemesaPaqueteria.Codigo)
                                    End If
                                End If

                                If entidad.FechaInterfazTracking > Date.MinValue Then
                                    conexion.AgregarParametroSQL("@par_Fecha_Interfaz_Tracking", entidad.FechaInterfazTracking, SqlDbType.DateTime)
                                End If

                                If entidad.FechaInterfazWMS > Date.MinValue Then
                                    conexion.AgregarParametroSQL("@par_Fecha_Interfaz_WMS", entidad.FechaInterfazWMS, SqlDbType.DateTime)
                                End If

                                If Not IsNothing(entidad.EstadoRemesaPaqueteria) Then
                                    If entidad.EstadoRemesaPaqueteria.Codigo > 0 Then
                                        conexion.AgregarParametroSQL("@par_CATA_ESRP_Codigo", entidad.EstadoRemesaPaqueteria.Codigo)
                                    End If
                                End If



                                If Not IsNothing(entidad.ObservacionesRemitente) Then
                                    conexion.AgregarParametroSQL("@par_Observaciones_Remitente", entidad.ObservacionesRemitente)
                                Else
                                    conexion.AgregarParametroSQL("@par_Observaciones_Remitente", VACIO)
                                End If
                                If Not IsNothing(entidad.ObservacionesDestinatario) Then
                                    conexion.AgregarParametroSQL("@par_Observaciones_Destinatario", entidad.ObservacionesDestinatario)
                                Else
                                    conexion.AgregarParametroSQL("@par_Observaciones_Destinatario", VACIO)
                                End If

                                conexion.AgregarParametroSQL("@par_CIUD_Codigo_Origen", entidad.Remesa.CiudadRemitente.Codigo)
                                conexion.AgregarParametroSQL("@par_CIUD_Codigo_Destino", entidad.Remesa.CiudadDestinatario.Codigo)

                                conexion.AgregarParametroSQL("@par_OFIC_Codigo_Origen", entidad.OficinaOrigen.Codigo)
                                conexion.AgregarParametroSQL("@par_OFIC_Codigo_Destino", entidad.OficinaDestino.Codigo)
                                conexion.AgregarParametroSQL("@par_OFIC_Codigo_Actual", entidad.OficinaActual.Codigo)
                                conexion.AgregarParametroSQL("@par_Descripcion_Mercancia", entidad.DesripcionMercancia)
                                conexion.AgregarParametroSQL("@par_Reexpedicion", entidad.Reexpedicion)

                                conexion.AgregarParametroSQL("@par_Centro_Costo", entidad.CentroCosto)
                                If Not IsNothing(entidad.LineaSerivicioCliente) Then
                                    conexion.AgregarParametroSQL("@par_CATA_LISC_Codigo", entidad.LineaSerivicioCliente.Codigo)
                                End If
                                If Not IsNothing(entidad.SitioCargue) Then
                                    conexion.AgregarParametroSQL("@par_SICD_Codigo_Cargue", entidad.SitioCargue.Codigo)
                                End If
                                If Not IsNothing(entidad.SitioDescargue) Then
                                    conexion.AgregarParametroSQL("@par_SICD_Codigo_Descargue", entidad.SitioDescargue.Codigo)
                                End If
                                If entidad.FechaEntrega > Date.MinValue Then
                                    conexion.AgregarParametroSQL("@par_Fecha_Entrega", entidad.FechaEntrega, SqlDbType.DateTime)
                                End If

                                conexion.AgregarParametroSQL("@par_Largo", entidad.Largo, SqlDbType.Money)
                                conexion.AgregarParametroSQL("@par_Alto", entidad.Alto, SqlDbType.Money)
                                conexion.AgregarParametroSQL("@par_Ancho", entidad.Ancho, SqlDbType.Money)
                                conexion.AgregarParametroSQL("@par_Peso_Volumetrico", entidad.PesoVolumetrico, SqlDbType.Money)
                                conexion.AgregarParametroSQL("@par_Peso_A_Cobrar", entidad.PesoCobrar, SqlDbType.Money)
                                conexion.AgregarParametroSQL("@par_Flete_Pactado", entidad.FletePactado, SqlDbType.Money)
                                conexion.AgregarParametroSQL("@par_Ajuste_Flete", entidad.AjusteFlete, SqlDbType.Money)

                                If Not IsNothing(entidad.LineaNegocioPaqueteria) Then
                                    If entidad.LineaNegocioPaqueteria.Codigo > 0 Then
                                        conexion.AgregarParametroSQL("@par_Linea_Negocio_Paqueteria", entidad.LineaNegocioPaqueteria.Codigo)
                                    End If
                                End If

                                If entidad.ManejaDetalleUnidades >= 0 Then
                                    conexion.AgregarParametroSQL("@par_Maneja_Detalle_Unidades", entidad.ManejaDetalleUnidades)
                                End If

                                If entidad.RecogerOficinaDestino >= 0 Then
                                    conexion.AgregarParametroSQL("@par_Recoger_Oficina_Destino", entidad.RecogerOficinaDestino)
                                End If

                                If Not IsNothing(entidad.OficinaRecibe) Then
                                    If entidad.OficinaRecibe.Codigo > 0 Then
                                        conexion.AgregarParametroSQL("@par_OFIC_Codigo_Recibe", entidad.OficinaRecibe.Codigo)
                                    End If
                                End If

                                If entidad.CodigoZona > 0 Then
                                    conexion.AgregarParametroSQL("@par_ZONA_Codigo", entidad.CodigoZona)
                                End If

                                conexion.AgregarParametroSQL("@par_Latitud", entidad.Latitud, SqlDbType.Decimal)
                                conexion.AgregarParametroSQL("@par_Longitud", entidad.Longitud, SqlDbType.Decimal)

                                If Not IsNothing(entidad.HorarioEntregaRemesa) Then
                                    If entidad.HorarioEntregaRemesa.Codigo > 0 Then
                                        conexion.AgregarParametroSQL("@par_HERP_Codigo", entidad.HorarioEntregaRemesa.Codigo)
                                    End If
                                End If
                                conexion.AgregarParametroSQL("@par_Liquidar_Unidad", entidad.LiquidarUnidad)

                                If Not IsNothing(entidad.Aforador) Then
                                    If entidad.Aforador.Codigo > 0 Then
                                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Aforador", entidad.Aforador.Codigo)
                                    End If
                                End If

                                If Not IsNothing(entidad.OficinaRegistroManual) Then
                                    If entidad.OficinaRegistroManual.Codigo > 0 Then
                                        conexion.AgregarParametroSQL("@par_OFIC_Codigo_Registro_Manual", entidad.OficinaRegistroManual.Codigo)
                                    End If
                                End If

                                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                                resultado = conexion.ExecuteReaderStoreProcedure("gsp_modificar_encabezado_remesa_paqueteria")

                                While resultado.Read
                                    entidad.Remesa.Numero = Convert.ToInt64(resultado.Item("Numero").ToString())
                                End While

                                resultado.Close()

                                If Not IsNothing(entidad.ListaReexpedicionOficinas) Then
                                    For Each item In entidad.ListaReexpedicionOficinas
                                        item.CodigoEmpresa = entidad.CodigoEmpresa
                                        item.CodigoRemesaPaqueteria = entidad.Remesa.Numero
                                        If Not InsertarReexpedicion(item, conexion) Then
                                            Exit For
                                        End If
                                    Next
                                End If
                                'Agrega Detalle Unidades Remesa Paqueteria
                                If entidad.ManejaDetalleUnidades = 1 Then
                                    BorrarDetalleUnidades(entidad.CodigoEmpresa, entidad.Remesa.Numero, conexion)
                                    For Each item In entidad.DetalleUnidades
                                        item.CodigoEmpresa = entidad.CodigoEmpresa
                                        item.NumeroRemesa = entidad.Remesa.Numero
                                        If Not InsertarDetalleUnidades(item, conexion) Then
                                            Exit For
                                        End If
                                    Next
                                End If
                                'Agregar Documentos Gestion Remesas
                                If Not IsNothing(entidad.GestionDocumentosRemesa) Then
                                    If entidad.GestionDocumentosRemesa.Count > 0 Then
                                        For Each item In entidad.GestionDocumentosRemesa
                                            item.CodigoEmpresa = entidad.CodigoEmpresa
                                            item.NumeroRemesa = entidad.Remesa.Numero
                                            If Not InsertarDetalleGestionDocumentos(item, conexion) Then
                                                Exit For
                                            End If
                                        Next
                                    End If
                                End If
                            Else
                                transaccion.Dispose()
                                Return entidad.Remesa.Numero
                            End If
                        End Using
                        'Generar Factura para estado definitivo y forma de pago contado y contra entrega
                        If entidad.Remesa.Estado = 1 Then
                            If Not IsNothing(entidad.Factura) Then
                                If Not IsNothing(entidad.Factura.FormaPago) Then
                                    If entidad.Factura.Cliente.Codigo = 0 Then
                                        Select Case entidad.Factura.FormaPago.Codigo
                                            Case 4902 'Contado
                                                If Not IsNothing(entidad.Remesa.Cliente) Then
                                                    entidad.Factura.Cliente = entidad.Remesa.Cliente
                                                    entidad.Factura.FacturarA = entidad.Remesa.Cliente
                                                Else
                                                    entidad.Factura.Cliente = entidad.Remesa.Remitente
                                                    entidad.Factura.FacturarA = entidad.Remesa.Remitente
                                                End If
                                            Case 4903 'Contra Entregas
                                                entidad.Factura.Cliente = entidad.Remesa.Destinatario
                                                entidad.Factura.FacturarA = entidad.Remesa.Destinatario
                                        End Select
                                    End If
                                    If entidad.Remesa.PesoCliente <= 0 Then
                                        entidad.Remesa.PesoCliente = entidad.PesoCobrar
                                    End If
                                    Dim RemeFactura As List(Of DetalleFacturas) = New List(Of DetalleFacturas)
                                    RemeFactura.Add(New DetalleFacturas With {.Remesas = entidad.Remesa})

                                    Dim Factura As Facturas = entidad.Factura
                                    Factura.ValorRemesas = entidad.Remesa.TotalFleteCliente
                                    Factura.ValorFactura = entidad.Remesa.TotalFleteCliente
                                    Factura.Subtotal = entidad.Remesa.TotalFleteCliente
                                    Factura.Remesas = RemeFactura
                                    Dim FactRepo = New RepositorioFacturas()
                                    Dim ResultFactRepo = FactRepo.InsertarFacturaRemesasPaqueteria(Factura)

                                    If ResultFactRepo <= 0 Then
                                        entidad.MensajeSQL = "No se pudo Generar la factura"
                                        entidad.Remesa.Numero = 0
                                        entidad.Remesa.NumeroDocumento = 0
                                    End If
                                End If

                            End If
                        End If
                        If entidad.Remesa.Numero > 0 Then
                            transaccion.Complete()
                        End If
                    End Using
                End If

            Catch ex As Exception
                Return Cero
            End Try
            Return entidad.Remesa.NumeroDocumento
        End Function

        Public Overrides Function Obtener(filtro As RemesaPaqueteria) As RemesaPaqueteria
            Dim item As New RemesaPaqueteria
            Dim Doc As New Documentos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_remesa_paqueteria]")

                While resultado.Read
                    item = New RemesaPaqueteria(resultado)
                End While
                resultado.Close()

                item.ListaReexpedicionOficinas = ConsultarDetalleReexpedicion(filtro.CodigoEmpresa, filtro.Numero, conexion, resultado)

                If item.Remesa.Numero > 0 And item.ManejaDetalleUnidades > 0 Then
                    item.DetalleUnidades = ConsultarDetalleUnidades(filtro.CodigoEmpresa, item.Remesa.Numero, conexion, resultado)
                End If

                item.Remesa.GestionDocumentosRemesa = ConsultarDetalleGestionDocumentos(filtro.CodigoEmpresa, filtro.Numero)

                conexion.CloseConnection()
            End Using

            Dim Documento = New RepositorioDocumentos()
            Dim Documentos = New Documentos()
            Documentos.CodigoEmpresa = filtro.CodigoEmpresa
            Documentos.Remesa = True
            Documentos.Numero = item.Remesa.Numero
            item.ListadoDocumentos = Documento.Consultar(Documentos)

            Return item
        End Function

        Public Function Anular(entidad As RemesaPaqueteria) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                conexion.AgregarParametroSQL("@par_USUA_Codigo", entidad.UsuarioAnula.Codigo)
                conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.OficinaAnula.Codigo)
                conexion.AgregarParametroSQL("@par_Causa_Anula", entidad.CausaAnulacion)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_anular_encabezado_remesas]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function

        Public Function CumplirGuias(entidad As RemesaPaqueteria) As Boolean
            Dim Cumplio As Boolean = True
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)

                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    Dim resultado As IDataReader
                    If Not IsNothing(entidad.CumplidoGuiasPaqueteria) Then
                        conexion.CreateConnection()
                        For Each item In entidad.CumplidoGuiasPaqueteria
                            conexion.CleanParameters()
                            conexion.AgregarParametroSQL("@par_EMPR_Codigo", item.CodigoEmpresa)
                            conexion.AgregarParametroSQL("@par_ENRE_Numero", item.NumeroRemesa)
                            conexion.AgregarParametroSQL("@par_Fecha_Recibe", item.FechaRecibe, SqlDbType.Date)
                            conexion.AgregarParametroSQL("@par_Nombre_Recibe", item.NombreRecibe)
                            conexion.AgregarParametroSQL("@par_Numero_Identificacion_Recibe", item.IdentificacionRecibe)
                            conexion.AgregarParametroSQL("@par_Telefonos_Recibe", item.TelefonoRecibe)
                            conexion.AgregarParametroSQL("@par_Observaciones_Recibe", item.ObservacionesRecibe)
                            conexion.AgregarParametroSQL("@par_Cantidad_Recibe", item.CantidadRecibida)
                            conexion.AgregarParametroSQL("@par_Peso_Recibe", item.PesoRecibido)
                            conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", item.UsuarioCrea.Codigo)
                            conexion.AgregarParametroSQL("@par_OFIC_Codigo", item.Oficina.Codigo)

                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_cumplir_guias_paqueteria")
                            While resultado.Read
                                If Not (resultado.Item("ENRE_Numero") > 0) Then
                                    Cumplio = False
                                    Exit For
                                End If
                            End While
                            resultado.Close()
                        Next
                    End If
                End Using
                If Cumplio Then
                    transaccion.Complete()
                End If
            End Using

            Return Cumplio
        End Function

        Public Function InsertarReexpedicion(entidad As ReexpedicionRemesasPaqueteria, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_ENRE_Numero", entidad.CodigoRemesaPaqueteria)
            conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
            conexion.AgregarParametroSQL("@par_CATA_TRRP_Codigo", entidad.TipoReexpedicion.Codigo)
            If Not IsNothing(entidad.ProveedorExterno) Then
                conexion.AgregarParametroSQL("@par_TERC_Codigo_Proveedor_Externo", entidad.ProveedorExterno.Codigo)
            End If
            conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
            conexion.AgregarParametroSQL("@par_Valor_Flete", entidad.Valor)
            conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_detalle_reexpedicion_remesas_paqueteria]")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Sub BorrarDetalleUnidades(empresa As Integer, NumeroRemesa As Integer, ByRef conexion As Conexion.DataBaseFactory)
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", empresa)
            conexion.AgregarParametroSQL("@par_ENRE_Numero", NumeroRemesa)
            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_borrar_detalle_unidades_remesa_paqueteria")
            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
        End Sub

        Public Function ConsultarDetalleUnidades(CodiEmpresa As Short, CodRemesa As Integer, ByRef conexion As Conexion.DataBaseFactory, resultado As IDataReader) As IEnumerable(Of DetalleUnidades)
            Dim lista As New List(Of DetalleUnidades)
            Dim item As DetalleUnidades

            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_ENRE_Numero", CodRemesa)

            resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_detalle_unidades_remesa_paqueteria")

            While resultado.Read
                item = New DetalleUnidades(resultado)
                lista.Add(item)
            End While
            resultado.Close()
            Return lista
        End Function

        Public Function ConsultarDetalleGestionDocumentos(CodiEmpresa As Short, CodRemesa As Integer) As IEnumerable(Of GestionDocumentosRemesa)
            Dim lista As New List(Of GestionDocumentosRemesa)
            Dim item As GestionDocumentosRemesa
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
                conexion.AgregarParametroSQL("@par_ENRE_Numero", CodRemesa)
                Dim resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_detalle_gestion_documentos_remesa_paqueteria")
                While resultado.Read
                    item = New GestionDocumentosRemesa(resultado)
                    lista.Add(item)
                End While
            End Using

            Return lista
        End Function

        Public Function InsertarDetalleUnidades(entidad As DetalleUnidades, ByRef conexion As Conexion.DataBaseFactory) As Boolean
            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_ENRE_Numero", entidad.NumeroRemesa)
            conexion.AgregarParametroSQL("@par_PROD_Codigo", entidad.Producto.Codigo)
            If Len(entidad.Referencia) > 0 Then
                conexion.AgregarParametroSQL("@par_Referencia", entidad.Referencia)
            End If
            conexion.AgregarParametroSQL("@par_Peso", entidad.Peso, SqlDbType.Float)
            conexion.AgregarParametroSQL("@par_Largo", entidad.Largo, SqlDbType.Float)
            conexion.AgregarParametroSQL("@par_Alto", entidad.Alto, SqlDbType.Float)
            conexion.AgregarParametroSQL("@par_Ancho", entidad.Ancho, SqlDbType.Float)
            conexion.AgregarParametroSQL("@par_PesoVolumetrico", entidad.PesoVolumetrico, SqlDbType.Float)
            conexion.AgregarParametroSQL("@par_ValorComercial", entidad.ValorComercial)
            If Len(entidad.DescripcionProducto) > 0 Then
                conexion.AgregarParametroSQL("@par_DescripcionProducto", entidad.DescripcionProducto)
            End If

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_unidades_remesa_paqueteria")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function InsertarDetalleGestionDocumentos(entidad As GestionDocumentosRemesa, ByRef conexion As Conexion.DataBaseFactory) As Boolean
            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_ENRE_Numero", entidad.NumeroRemesa)
            conexion.AgregarParametroSQL("@par_NumeroDocumentoGestion", entidad.NumeroDocumentoGestion)
            conexion.AgregarParametroSQL("@par_TDGC_Codigo", entidad.TipoDocumento.Codigo)
            conexion.AgregarParametroSQL("@par_TGCD_Codigo", entidad.TipoGestion.Codigo)
            conexion.AgregarParametroSQL("@par_Numero_Hojas", entidad.NumeroHojas)
            conexion.AgregarParametroSQL("@par_Valor_Recaudar", entidad.ValorRecaudar, SqlDbType.Money)
            conexion.AgregarParametroSQL("@par_Recibido", entidad.Recibido)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_gestion_documentos_remesa_paqueteria")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function ConsultarDetalleReexpedicion(CodiEmpresa As Short, CodRemesa As Integer, ByRef conexion As Conexion.DataBaseFactory, resultado As IDataReader) As IEnumerable(Of ReexpedicionRemesasPaqueteria)
            Dim lista As New List(Of ReexpedicionRemesasPaqueteria)
            Dim item As ReexpedicionRemesasPaqueteria
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_ENRE_Numero", CodRemesa)

            resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_reexpedicion_remesas_paqueteria]")

            While resultado.Read
                item = New ReexpedicionRemesasPaqueteria(resultado)
                lista.Add(item)
            End While
            resultado.Close()
            Return lista
        End Function

        Public Function ConsultarDetalleReexpedicionConsulta(CodiEmpresa As Short, CodRemesa As Integer) As IEnumerable(Of ReexpedicionRemesasPaqueteria)

            Dim lista As New List(Of ReexpedicionRemesasPaqueteria)
            Dim item As ReexpedicionRemesasPaqueteria
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
                conexion.AgregarParametroSQL("@par_ENRE_Numero", CodRemesa)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_reexpedicion_remesas_paqueteria]")

                While resultado.Read
                    item = New ReexpedicionRemesasPaqueteria(resultado)
                    lista.Add(item)
                End While
            End Using
            Return lista
        End Function

        Public Function GenerarPlantilla(filtro As RemesaPaqueteria) As RemesaPaqueteria
            Dim fullPath As String
            fullPath = System.AppDomain.CurrentDomain.BaseDirectory()
            System.IO.File.Copy(fullPath + "Controllers\Paqueteria\Plantillas\Plantilla_Cargue_Masivo_Guias_Paqueteria_Cliente.xlsx", fullPath + "Controllers\Paqueteria\Plantillas\Plantilla_Cargue_Masivo_Guias_Paqueteria_Cliente_Generado.xlsx", True)
            Dim cn As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fullPath + "Controllers\Paqueteria\Plantillas\Plantilla_Cargue_Masivo_Guias_Paqueteria_Cliente_Generado.xlsx;Extended Properties='Excel 12.0 Xml;HDR=YES'"
            'Dim Clientes As IEnumerable(Of Terceros)
            'Dim Cliente As New Terceros
            'Dim ObCliente As New RepositorioTerceros
            'Cliente.CodigoEmpresa = filtro.CodigoEmpresa
            'Cliente.Estado = New Estado With {.Codigo = 1}
            'Cliente.CadenaPerfiles = "1401"
            'Clientes = ObCliente.Consultar(Cliente)


            Dim Productos As IEnumerable(Of ProductoTransportados)
            Dim Producto As New ProductoTransportados
            Dim ObjProducto As New Basico.Despachos.RepositorioProductoTransportados
            Producto.CodigoEmpresa = filtro.CodigoEmpresa
            Producto.Estado = 1
            Productos = ObjProducto.Consultar(Producto)



            Dim Ciudades As IEnumerable(Of Ciudades)
            Dim Ciudad As New Ciudades
            Dim ObjCiudad As New RepositorioCiudades
            Ciudad.CodigoEmpresa = filtro.CodigoEmpresa
            Ciudad.Estado = 1
            Ciudades = ObjCiudad.Consultar(Ciudad)


            Dim Catalogos As IEnumerable(Of ValorCatalogos)
            Dim Catalogo As New ValorCatalogos
            Dim ObjValorCatalogos As New RepositorioValorCatalogos
            Catalogo.CodigoEmpresa = filtro.CodigoEmpresa
            Catalogo.Catalogo = New Catalogos With {.Codigo = 1}
            Catalogos = ObjValorCatalogos.Consultar(Catalogo)

            Dim Sitios As IEnumerable(Of SitiosCargueDescargue)
            Dim Sitio As New SitiosCargueDescargue
            Dim ObjSitio As New RepositorioSitiosCargueDescargue
            Sitio.CodigoEmpresa = filtro.CodigoEmpresa
            Sitio.Estado = 1
            Sitios = ObjSitio.Consultar(Sitio)


            Dim Zonas As IEnumerable(Of Zonas)
            Dim Zona As New Zonas
            Dim ObjZona As New RepositorioZonas
            Zona.CodigoEmpresa = filtro.CodigoEmpresa
            Zona.Estado = 1
            Zonas = ObjZona.Consultar(Zona)


            Using cnn As New OleDbConnection(cn)
                cnn.Open()
                Using cmd As OleDbCommand = cnn.CreateCommand()
                    'Clientes = Clientes.OrderBy(Function(Clie) Clie.NombreCompleto)
                    'For i = 1 To Clientes.Count
                    '    Dim row = i + 2
                    '    cmd.Parameters.Clear()
                    '    cmd.CommandText = "INSERT INTO [Clientes$] (Codigo,Nombre) values(@Cod,@nom)"
                    '    cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Clientes(i - 1).Codigo
                    '    cmd.Parameters.AddWithValue("@nom", Clientes(i - 1).NombreCompleto)
                    '    cmd.ExecuteNonQuery()
                    'Next
                    Dim CiudadesSitios As New List(Of Ciudades)
                    Dim CiudadesZonas As New List(Of Ciudades)
                    Dim ID As Integer = 0
                    Dim IDZonas As Integer = 0
                    Dim Ultimo_ID As Integer = 1
                    Dim STROLD As String = ""
                    Dim STRZOCI As String = ""
                    Ciudades = Ciudades.OrderBy(Function(a) a.Nombre)
                    STROLD = "CREATE TABLE [Sitios_Cargue_Ciudades$] ( ID INT, "
                    STRZOCI = "CREATE TABLE [Zonas_Ciudades$] ( ID INT, "
                    For i = 1 To Ciudades.Count
                        Dim row = i + 2
                        cmd.Parameters.Clear()
                        cmd.CommandText = "INSERT INTO [Ciudad$] (Codigo,Nombre) values(@Cod,@nom)"
                        cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Ciudades(i - 1).Codigo
                        cmd.Parameters.AddWithValue("@nom", Ciudades(i - 1).Nombre + "(" + Ciudades(i - 1).Departamento.Nombre + ")")
                        cmd.ExecuteNonQuery()

                        If Ciudades(i - 1).Cantidad_Sitios > 0 Then
                            STROLD += "[" + Ciudades(i - 1).Nombre + "(" + Ciudades(i - 1).Departamento.Nombre + ")] char(250),"
                            CiudadesSitios.Add(Ciudades(i - 1))
                        End If
                        If Ciudades(i - 1).CantidadZonas > 0 Then
                            STRZOCI += "[" + Ciudades(i - 1).Nombre + "(" + Ciudades(i - 1).Departamento.Nombre + ")] char(250),"
                            CiudadesZonas.Add(Ciudades(i - 1))
                        End If
                    Next
                    STROLD = STROLD.Remove(STROLD.Length - 1)
                    STROLD += ")"
                    STRZOCI = STRZOCI.Remove(STRZOCI.Length - 1)
                    STRZOCI += ")"
                    cmd.Parameters.Clear()
                    cmd.CommandText = STROLD
                    cmd.ExecuteNonQuery()
                    cmd.Parameters.Clear()
                    cmd.CommandText = STRZOCI
                    cmd.ExecuteNonQuery()
                    Dim ListaSitios As New List(Of SitiosCargueDescargue)
                    Sitios = Sitios.OrderBy(Function(a) a.Nombre)
                    Dim UltimoID = 0
                    For i = 1 To Sitios.Count
                        cmd.Parameters.Clear()
                        cmd.CommandText = "INSERT INTO [Sitios_cargue$] (Codigo,Nombre) values(@Cod,@nom)"
                        cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Sitios(i - 1).Codigo
                        cmd.Parameters.AddWithValue("@nom", Sitios(i - 1).Nombre)
                        cmd.ExecuteNonQuery()
                        ListaSitios.Add(Sitios(i - 1))
                    Next

                    Dim ListaZonas As New List(Of Zonas)
                    Zonas = Zonas.OrderBy(Function(a) a.Ciudad.Nombre)
                    Dim UltimoIDZonas = 1
                    For i = 1 To Zonas.Count
                        cmd.Parameters.Clear()
                        cmd.CommandText = "INSERT INTO [Zonas$] (Codigo,Nombre) values(@Cod,@nom)"
                        cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Zonas(i - 1).Codigo
                        cmd.Parameters.AddWithValue("@nom", Zonas(i - 1).Nombre)
                        cmd.ExecuteNonQuery()
                        ListaZonas.Add(Zonas(i - 1))
                    Next
                    For i = 1 To CiudadesSitios.Count
                        Dim Recorrido = 1
                        Dim pos = 0
                        While (pos < ListaSitios.Count())
                            Dim item = ListaSitios(pos)
                            If item.Ciudad.Codigo = CiudadesSitios(i - 1).Codigo Then
                                cmd.Parameters.Clear()
                                cmd.CommandText = "Select MIN(ID) From [Sitios_Cargue_Ciudades$] Where [" + item.Ciudad.Nombre + "(" + item.Ciudad.Departamento.Nombre + ")] Is NULL Or [" + item.Ciudad.Nombre + "(" + item.Ciudad.Departamento.Nombre + ")] = '' "
                                Dim reader As OleDbDataReader = cmd.ExecuteReader()
                                ID = 0
                                While reader.Read
                                    If Not IsDBNull(reader.GetValue(0)) Then
                                        ID = reader.GetValue(0)
                                    End If
                                End While
                                reader.Close()
                                If ID = 0 Then
                                    cmd.CommandText = "INSERT INTO [Sitios_Cargue_Ciudades$] (ID, [" + item.Ciudad.Nombre + "(" + item.Ciudad.Departamento.Nombre + ")]) values(@Cod,@nom)"
                                    cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Ultimo_ID
                                    cmd.Parameters.AddWithValue("@nom", item.Nombre)
                                    cmd.ExecuteNonQuery()
                                    Ultimo_ID = Ultimo_ID + 1
                                Else
                                    cmd.CommandText = "UPDATE [Sitios_Cargue_Ciudades$] SET [" + item.Ciudad.Nombre + "(" + item.Ciudad.Departamento.Nombre + ")] = @nom WHERE ID =" + ID.ToString()
                                    cmd.Parameters.AddWithValue("@nom", item.Nombre)
                                    cmd.ExecuteNonQuery()
                                End If
                                ListaSitios.Remove(item)
                            Else
                                pos += 1
                            End If
                        End While
                    Next


                    For i = 1 To CiudadesZonas.Count
                        Dim Recorrido = 1
                        Dim pos = 0
                        While (pos < ListaZonas.Count())
                            Dim item = ListaZonas(pos)
                            If item.Ciudad.Codigo = CiudadesZonas(i - 1).Codigo Then
                                cmd.Parameters.Clear()
                                cmd.CommandText = "Select MIN(ID) From [Zonas_Ciudades$] Where [" + item.Ciudad.Nombre + "(" + item.Ciudad.Departamento.Nombre + ")] Is NULL Or [" + item.Ciudad.Nombre + "(" + item.Ciudad.Departamento.Nombre + ")] = '' "
                                Dim reader As OleDbDataReader = cmd.ExecuteReader()
                                IDZonas = 0
                                While reader.Read
                                    If Not IsDBNull(reader.GetValue(0)) Then
                                        IDZonas = reader.GetValue(0)
                                    End If
                                End While
                                reader.Close()
                                If IDZonas = 0 Then
                                    cmd.CommandText = "INSERT INTO [Zonas_Ciudades$] (ID, [" + item.Ciudad.Nombre + "(" + item.Ciudad.Departamento.Nombre + ")]) values(@Cod,@nom)"
                                    cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = UltimoIDZonas
                                    cmd.Parameters.AddWithValue("@nom", item.Nombre)
                                    cmd.ExecuteNonQuery()
                                    UltimoIDZonas = UltimoIDZonas + 1
                                Else
                                    cmd.CommandText = "UPDATE [Zonas_Ciudades$] SET [" + item.Ciudad.Nombre + "(" + item.Ciudad.Departamento.Nombre + ")] = @nom WHERE ID =" + IDZonas.ToString()
                                    cmd.Parameters.AddWithValue("@nom", item.Nombre)
                                    cmd.ExecuteNonQuery()
                                End If
                                ListaZonas.Remove(item)
                            Else
                                pos += 1
                            End If
                        End While

                    Next
                    Productos = Productos.OrderBy(Function(a) a.Nombre)
                    For i = 1 To Productos.Count
                        Dim row = i + 2
                        cmd.Parameters.Clear()
                        cmd.CommandText = "INSERT INTO [Producto$] (Codigo,Nombre) values(@Cod,@nom)"
                        cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Productos(i - 1).Codigo
                        cmd.Parameters.AddWithValue("@nom", Productos(i - 1).Nombre)
                        cmd.ExecuteNonQuery()
                    Next


                    Catalogos = Catalogos.OrderBy(Function(a) a.Nombre)
                    For i = 1 To Catalogos.Count
                        Dim row = i + 2
                        cmd.Parameters.Clear()
                        cmd.CommandText = "INSERT INTO [Tipo_Identificacion$] (Codigo,Nombre) values(@Cod,@nom)"
                        cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Catalogos(i - 1).Codigo
                        cmd.Parameters.AddWithValue("@nom", Catalogos(i - 1).Nombre)
                        cmd.ExecuteNonQuery()
                    Next
                    Catalogo.Catalogo = New Catalogos With {.Codigo = 66}
                    Catalogos = ObjValorCatalogos.Consultar(Catalogo)
                    Catalogos = Catalogos.OrderBy(Function(a) a.Nombre)
                    For i = 1 To Catalogos.Count
                        Dim row = i + 2
                        cmd.Parameters.Clear()
                        cmd.CommandText = "INSERT INTO [Tipo_Entrega$] (Codigo,Nombre) values(@Cod,@nom)"
                        cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Catalogos(i - 1).Codigo
                        cmd.Parameters.AddWithValue("@nom", Catalogos(i - 1).Nombre)
                        cmd.ExecuteNonQuery()
                    Next
                    Catalogo.Catalogo = New Catalogos With {.Codigo = 49}
                    Catalogos = ObjValorCatalogos.Consultar(Catalogo)
                    Catalogos = Catalogos.OrderBy(Function(a) a.Nombre)
                    For i = 1 To Catalogos.Count
                        Dim row = i + 2
                        cmd.Parameters.Clear()
                        cmd.CommandText = "INSERT INTO [Forma_Pago$] (Codigo,Nombre) values(@Cod,@nom)"
                        cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Catalogos(i - 1).Codigo
                        cmd.Parameters.AddWithValue("@nom", Catalogos(i - 1).Nombre)
                        cmd.ExecuteNonQuery()
                    Next
                    Catalogo.Catalogo = New Catalogos With {.Codigo = 194}
                    Catalogos = ObjValorCatalogos.Consultar(Catalogo)
                    Catalogos = Catalogos.OrderBy(Function(a) a.Nombre)
                    For i = 1 To Catalogos.Count
                        Dim row = i + 2
                        cmd.Parameters.Clear()
                        cmd.CommandText = "INSERT INTO [Linea_Servicios$] (Codigo,Nombre) values(@Cod,@nom)"
                        cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Catalogos(i - 1).Codigo
                        cmd.Parameters.AddWithValue("@nom", Catalogos(i - 1).Nombre)
                        cmd.ExecuteNonQuery()
                    Next
                    Catalogo.Catalogo = New Catalogos With {.Codigo = 216}
                    Catalogos = ObjValorCatalogos.Consultar(Catalogo)
                    Catalogos = Catalogos.OrderBy(Function(a) a.Nombre)
                    For i = 1 To Catalogos.Count
                        Dim row = i + 2
                        cmd.Parameters.Clear()
                        cmd.CommandText = "INSERT INTO [Linea_Negocio_Paqueteria$] (Codigo,Nombre) values(@Cod,@nom)"
                        cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Catalogos(i - 1).Codigo
                        cmd.Parameters.AddWithValue("@nom", Catalogos(i - 1).Nombre)
                        cmd.ExecuteNonQuery()
                    Next

                    Catalogo.Catalogo = New Catalogos With {.Codigo = 217}
                    Catalogos = ObjValorCatalogos.Consultar(Catalogo)
                    Catalogos = Catalogos.OrderBy(Function(a) a.Nombre)
                    For i = 1 To Catalogos.Count
                        Dim row = i + 2
                        cmd.Parameters.Clear()
                        cmd.CommandText = "INSERT INTO [Horario_Entrega$] (Codigo,Nombre) values(@Cod,@nom)"
                        cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Catalogos(i - 1).Codigo
                        cmd.Parameters.AddWithValue("@nom", Catalogos(i - 1).Nombre)
                        cmd.ExecuteNonQuery()
                    Next
                    cnn.Close()
                End Using
            End Using
            Dim RTA As New RemesaPaqueteria
            Dim Archivo() As Byte = IO.File.ReadAllBytes(fullPath + "Controllers\Paqueteria\Plantillas\Plantilla_Cargue_Masivo_Guias_Paqueteria_Cliente_Generado.xlsx")
            RTA.Planitlla = Archivo
            Return RTA
        End Function

        Public Function GuardarEntrega(entidad As Remesas) As Long

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    Dim resultado As IDataReader
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_ENRE_Numero", entidad.Numero)
                    If entidad.DevolucionRemesa > 0 Then
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_registrar_devolucion_remesa_paqueteria")
                    Else
                        conexion.AgregarParametroSQL("@par_Cantidad_Recibe", entidad.CantidadRecibe, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Peso_Recibe", entidad.PesoRecibe, SqlDbType.Money)
                        'conexion.AgregarParametroSQL("@par_Fecha_Recibe", entidad.FechaRecibe, SqlDbType.Date)
                        conexion.AgregarParametroSQL("@par_CATA_TIID_Codigo_Recibe", entidad.CodigoTipoIdentificacionRecibe)
                        conexion.AgregarParametroSQL("@par_Numero_Identificacion_Recibe", entidad.NumeroIdentificacionRecibe)
                        conexion.AgregarParametroSQL("@par_Nombre_Recibe", entidad.NombreRecibe)
                        conexion.AgregarParametroSQL("@par_Telefonos_Recibe", entidad.TelefonoRecibe)
                        If Not IsNothing(entidad.NovedadEntrega) Then
                            conexion.AgregarParametroSQL("@par_CATA_NERP_Codigo", entidad.NovedadEntrega.Codigo)
                        End If
                        If Not IsNothing(entidad.Oficina) Then
                            If entidad.Oficina.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
                            End If
                        End If
                        conexion.AgregarParametroSQL("@par_Observaciones_Recibe", entidad.ObservacionesRecibe)
                        conexion.AgregarParametroSQL("@par_Firma", entidad.Firma, SqlDbType.Image)
                        conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)

                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_registrar_entrega_remesa_paqueteria")
                    End If
                    While resultado.Read
                        entidad.Numero = resultado.Item("Codigo").ToString()
                    End While

                    resultado.Close()

                    'Actualizar Estados Documentos Gestion Remesas
                    If Not IsNothing(entidad.GestionDocumentosRemesa) Then
                        If entidad.GestionDocumentosRemesa.Count > 0 Then
                            For Each item In entidad.GestionDocumentosRemesa
                                item.CodigoEmpresa = entidad.CodigoEmpresa
                                item.NumeroRemesa = entidad.Numero
                                If Not EstadoEntregaDetalleGestionDocumentos(item, conexion) Then
                                    Exit For
                                End If
                            Next
                        End If
                    End If

                    resultado.Close()
                    conexion.CloseConnection()

                    If entidad.Numero.Equals(Cero) Then
                        transaccion.Dispose()
                        Return Cero
                    Else
                        transaccion.Complete()
                    End If

                End Using

            End Using

            If entidad.Numero > 0 Then
                If Not IsNothing(entidad.Fotografia) Then

                    entidad.Fotografia.CodigoEmpresa = entidad.CodigoEmpresa
                    entidad.Fotografia.NumeroRemesa = entidad.Numero
                    entidad.Fotografia.Codigo = entidad.Numero
                    entidad.Fotografia.CodigoUsuarioCrea = entidad.UsuarioModifica.Codigo
                    If entidad.DevolucionRemesa > 0 Then
                        entidad.Fotografia.EntregaDevolucion = 1
                    Else
                        entidad.Fotografia.EntregaDevolucion = 0
                    End If

                    InsertarFoto(entidad.Fotografia)

                End If

                If Not IsNothing(entidad.Fotografias) Then
                    For Each Foto In entidad.Fotografias
                        Foto.CodigoEmpresa = entidad.CodigoEmpresa
                        Foto.NumeroRemesa = entidad.Numero
                        Foto.Codigo = entidad.Numero
                        Foto.CodigoUsuarioCrea = entidad.UsuarioModifica.Codigo
                        Foto.ListaFotos = True
                        If entidad.DevolucionRemesa > 0 Then
                            Foto.EntregaDevolucion = 1
                        Else
                            Foto.EntregaDevolucion = 0
                        End If
                        InsertarFoto(Foto)
                    Next
                End If
            End If

            Return entidad.Numero

        End Function

        Public Function EstadoEntregaDetalleGestionDocumentos(entidad As GestionDocumentosRemesa, ByRef conexion As Conexion.DataBaseFactory) As Boolean
            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_ENRE_Numero", entidad.NumeroRemesa)
            conexion.AgregarParametroSQL("@par_TDGC_Codigo", entidad.TipoDocumento.Codigo)
            conexion.AgregarParametroSQL("@par_TGCD_Codigo", entidad.TipoGestion.Codigo)
            conexion.AgregarParametroSQL("@par_Entregado", entidad.Entregado)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_guardar_estado_detalle_gestion_documentos_remesa_paqueteria")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function InsertarFoto(entidad As FotoDetalleDistribucionRemesas) As Boolean
            Dim Inserto As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENRE_Numero", entidad.NumeroRemesa)
                ' conexion.AgregarParametroSQL("@par_DEDR_ID", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Foto", entidad.FotoBits, SqlDbType.VarBinary)
                conexion.AgregarParametroSQL("@par_Nombre_Foto", entidad.NombreFoto)
                conexion.AgregarParametroSQL("@par_Extension_Foto", entidad.ExtensionFoto)
                conexion.AgregarParametroSQL("@par_Tipo_Foto", entidad.TipoFoto)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.CodigoUsuarioCrea)
                conexion.AgregarParametroSQL("@par_Entrega_Devolucion", entidad.EntregaDevolucion)

                If entidad.ListaFotos Then
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_fotos_detalle_distribucion_remesas]")
                    While resultado.Read
                        Inserto = IIf((resultado.Item("Codigo")) > 0, True, False)
                    End While

                    resultado.Close()
                Else
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_foto_detalle_distribucion_remesas]")
                    While resultado.Read
                        Inserto = IIf((resultado.Item("Codigo")) > 0, True, False)
                    End While

                    resultado.Close()
                End If




                conexion.CloseConnection()

            End Using

            Return Inserto
        End Function

        Public Function ConsultarIndicadores(filtro As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria)
            Dim lista As New List(Of RemesaPaqueteria)
            Dim item As RemesaPaqueteria
            Dim resultado As IDataReader
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)

                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                    If filtro.FechaInicial > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_FechaInicial", filtro.FechaInicial, SqlDbType.Date)
                    End If

                    If filtro.FechaFinal > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_FechaFinal", filtro.FechaFinal, SqlDbType.Date)
                    End If

                    If Not IsNothing(filtro.Remesa) Then
                        If Not IsNothing(filtro.Remesa.Cliente) Then
                            If filtro.Remesa.Cliente.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", filtro.Remesa.Cliente.Codigo)
                            End If
                        End If

                        If Not IsNothing(filtro.Remesa.Remitente) Then
                            If filtro.Remesa.Remitente.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_TERC_Codigo_Remitente", filtro.Remesa.Remitente.Codigo)
                            End If
                        End If

                        If Not IsNothing(filtro.Remesa.Destinatario) Then
                            If filtro.Remesa.Destinatario.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_TERC_Codigo_Destinatario", filtro.Remesa.Destinatario.Codigo)
                            End If
                        End If
                    End If
                    If Not IsNothing(filtro.SitioDescargue) Then
                        If filtro.SitioDescargue.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_SICD_Codigo_Descargue", filtro.SitioDescargue.Codigo)
                        End If
                    End If
                    If Not IsNothing(filtro.Pagina) Then
                        If filtro.Pagina > 0 Then
                            conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                        End If
                    End If

                    If Not IsNothing(filtro.RegistrosPagina) Then
                        If filtro.RegistrosPagina > 0 Then
                            conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                        End If
                    End If


                    If filtro.InidicadorPedidoNovedades > 0 Then
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_indicador_Novedades_Pedidos")
                    ElseIf filtro.InidicadorEntregas > 0 Then
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_indicador_Entregas")
                    ElseIf filtro.InidicadorErroresAtribuibles > 0 Then
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_indicador_Errores_Atribuibles")
                    ElseIf filtro.InidicadorPuntosEntregados > 0 Then
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_indicador_Puntos_Entregados")
                    ElseIf filtro.InidicadorCajasEntregadas > 0 Then
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_indicador_Cajas_Entregados")
                    Else
                        If filtro.ConsultaNovedades > 0 Then
                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_remesa_paqueteria_indicadores_novedades")
                        Else
                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_remesa_paqueteria_indicadores")
                        End If
                    End If

                    While resultado.Read
                        item = New RemesaPaqueteria(resultado)
                        lista.Add(item)
                    End While
                    conexion.CloseConnection()
                End Using
            End Using
            Return lista
        End Function

        Public Function AsociarGuiaPlanilla(filtro As RemesaPaqueteria) As Boolean
            Dim Inserto As Boolean = True
            Dim resultado As IDataReader
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)

                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    For Each item In filtro.RemesaPaqueteria
                        conexion.CleanParameters()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.NumeroPlanilla)
                        conexion.AgregarParametroSQL("@par_ENRE_Numero", item.Numero)
                        conexion.AgregarParametroSQL("@par_ENRE_Numero_Documento", item.NumeroDocumento)
                        If Not IsNothing(filtro.TipoDocumento) Then
                            If filtro.TipoDocumento = 205 Then
                                resultado = conexion.ExecuteReaderStoreProcedure("gsp_asociar_remesa_paqueteria_recoleccion")
                            ElseIf filtro.TipoDocumento = 210 Then
                                resultado = conexion.ExecuteReaderStoreProcedure("gsp_asociar_remesa_paqueteria_entrega")
                            Else
                                resultado = conexion.ExecuteReaderStoreProcedure("gsp_asociar_remesa_paqueteria_planilla")
                            End If
                        Else
                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_asociar_remesa_paqueteria_planilla")
                        End If
                        While resultado.Read
                            If Not (resultado.Item("Codigo") > 0) Then
                                Inserto = False
                                Exit For
                            End If
                        End While
                        resultado.Close()
                    Next
                    conexion.CloseConnection()
                End Using
                If Inserto Then
                    transaccion.Complete()
                Else
                    transaccion.Dispose()
                End If
            End Using
            Return Inserto
        End Function

        Public Function ConsultarEstadosRemesa(filtro As RemesaPaqueteria) As IEnumerable(Of DetalleEstadosRemesa)
            Dim lista As New List(Of DetalleEstadosRemesa)
            Dim item As DetalleEstadosRemesa
            Dim resultado As IDataReader
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)

                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                    If filtro.Numero > 0 Then
                        conexion.AgregarParametroSQL("@par_ENRE_Numero", filtro.Numero)
                    End If
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_detalle_estados_remesas_paqueteria")

                    While resultado.Read
                        item = New DetalleEstadosRemesa(resultado)
                        lista.Add(item)
                    End While
                    conexion.CloseConnection()
                End Using
            End Using
            Return lista
        End Function

        Public Function ObtenerControlEntregas(filtro As RemesaPaqueteria) As RemesaPaqueteria
            Dim item As New RemesaPaqueteria

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_remesa_paqueteria_control_entregas]")

                While resultado.Read
                    item = New RemesaPaqueteria(resultado)
                End While
                resultado.Close()
                conexion.CloseConnection()
            End Using

            If item.Remesa.Numero > 0 Then
                'item.Remesa.Fotografia = ConsultarFoto(filtro.CodigoEmpresa, filtro.Numero)

                Dim Documento = New Repositorio.Basico.General.RepositorioDocumentos
                Dim filtroDocu As Documentos = New Documentos With {
                    .CodigoEmpresa = filtro.CodigoEmpresa,
                    .CumplidoRemesa = True,
                    .Numero = filtro.Numero
                }
                item.DocumentosCumplidoRemsa = Documento.Consultar(filtroDocu)
                item.Remesa.Fotografias = ConsultarFoto(filtro.CodigoEmpresa, filtro.Numero)
            End If

            Return item
        End Function

        Public Function ConsultarFoto(CodiEmpresa As Short, NumeRemesa As Long) As IEnumerable(Of FotoDetalleDistribucionRemesas)
            Dim item As New FotoDetalleDistribucionRemesas
            Dim lista As New List(Of FotoDetalleDistribucionRemesas)
            Using conexionDocumentos = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                conexionDocumentos.CreateConnection()
                conexionDocumentos.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
                conexionDocumentos.AgregarParametroSQL("@par_ENRE_Numero", NumeRemesa)

                Dim resultado As IDataReader = conexionDocumentos.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_foto_detalle_distribucion_remesas]")

                While resultado.Read
                    item = New FotoDetalleDistribucionRemesas(resultado)
                    lista.Add(item)
                End While
                resultado.Close()
                conexionDocumentos.CloseConnection()
            End Using
            Return lista
        End Function

        Public Function ConsultarRemesasPendientesControlEntregas(filtro As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria)
            Dim lista As New List(Of RemesaPaqueteria)
            Dim item As RemesaPaqueteria
            Dim resultado As IDataReader

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.NumeroInicial > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.NumeroInicial)
                End If
                If filtro.FechaInicial > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_FechaInicial", filtro.FechaInicial, SqlDbType.Date)
                End If
                If filtro.FechaFinal > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_FechaFinal", filtro.FechaFinal, SqlDbType.Date)
                End If
                If filtro.NumeroPlanillaInicial > 0 Then
                    conexion.AgregarParametroSQL("@par_NumeroPlanillaDespacho", filtro.NumeroPlanillaInicial)
                End If
                If filtro.PlanillaEntrega > 0 Then
                    conexion.AgregarParametroSQL("@par_NumeroPlanillaEntrega", filtro.PlanillaEntrega)
                End If
                If Not IsNothing(filtro.Remesa) Then
                    If Not IsNothing(filtro.Remesa.Vehiculo) Then
                        If filtro.Remesa.Vehiculo.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_VEHI_Codigo", filtro.Remesa.Vehiculo.Codigo)
                        End If
                    End If
                    If Not IsNothing(filtro.Remesa.CiudadRemitente) Then
                        If Not IsNothing(filtro.Remesa.CiudadRemitente.Nombre) Then
                            conexion.AgregarParametroSQL("@par_Ciudad_Origen", filtro.Remesa.CiudadRemitente.Nombre)
                        End If
                    End If
                    If Not IsNothing(filtro.Remesa.CiudadDestinatario) Then
                        If Not IsNothing(filtro.Remesa.CiudadDestinatario.Nombre) Then
                            conexion.AgregarParametroSQL("@par_Ciudad_Destino", filtro.Remesa.CiudadDestinatario.Nombre)
                        End If
                    End If
                    If Not IsNothing(filtro.Remesa.Cliente) Then
                        If filtro.Remesa.Cliente.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", filtro.Remesa.Cliente.Codigo)
                        End If
                    End If
                    If filtro.Remesa.NumeroDocumentoCliente > 0 Then
                        conexion.AgregarParametroSQL("@par_Documento_Cliente", filtro.Remesa.NumeroDocumentoCliente)
                    End If
                End If
                If Not IsNothing(filtro.TipoEntregaRemesaPaqueteria) Then
                    If filtro.TipoEntregaRemesaPaqueteria.Codigo <> 0 And filtro.TipoEntregaRemesaPaqueteria.Codigo <> 6600 Then
                        conexion.AgregarParametroSQL("@par_CATA_TERP_Codigo", filtro.TipoEntregaRemesaPaqueteria.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.CodigoOficina) Then
                    If filtro.CodigoOficina > 0 Then
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.CodigoOficina)
                    End If
                End If
                If Not IsNothing(filtro.UsuarioConsulta) Then
                    If filtro.UsuarioConsulta.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_Usuario_Consulta", filtro.UsuarioConsulta.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                If filtro.TipoDocumento = 135 Then

                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_remesa_paqueteria_pendientes_control_entregas")

                    While resultado.Read
                        item = New RemesaPaqueteria(resultado)
                        lista.Add(item)
                    End While
                Else

                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_remesa_paqueteria_pendientes_control_entrega")

                    While resultado.Read
                        item = New RemesaPaqueteria(resultado)
                        lista.Add(item)
                    End While
                End If

                conexion.CloseConnection()

            End Using
            Return lista
        End Function

        Public Function ConsultarRemesasPorPlanillar(filtro As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria)
            Dim lista As New List(Of RemesaPaqueteria)
            Dim item As RemesaPaqueteria
            Dim resultado As IDataReader

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.NumeroInicial > 0 Then
                    conexion.AgregarParametroSQL("@par_NumeroInicial", filtro.NumeroInicial)
                End If
                If filtro.FechaInicial > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_FechaInicial", filtro.FechaInicial, SqlDbType.Date)
                End If
                If filtro.FechaFinal > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_FechaFinal", filtro.FechaFinal, SqlDbType.Date)
                End If

                If (filtro.NumeroDocumentoPlanillaCargue > 0) Then
                    conexion.AgregarParametroSQL("@par_NumeroDocumentoPlanillaCargue", filtro.NumeroDocumentoPlanillaCargue)
                End If

                If Not IsNothing(filtro.Remesa) Then
                    If Not IsNothing(filtro.Remesa.Cliente) Then
                        If Not IsNothing(filtro.Remesa.Cliente.NombreCompleto) And filtro.Remesa.Cliente.NombreCompleto <> String.Empty Then
                            conexion.AgregarParametroSQL("@par_NombreCliente", filtro.Remesa.Cliente.NombreCompleto)
                        End If
                        If filtro.Remesa.Cliente.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", filtro.Remesa.Cliente.Codigo)
                        End If
                    End If
                    If Not IsNothing(filtro.TipoDocumento) Then
                        If filtro.TipoDocumento > 0 Then
                            conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento)
                        End If
                    End If

                    If Not IsNothing(filtro.Remesa.CiudadRemitente) Then
                        If Not IsNothing(filtro.Remesa.CiudadRemitente.Nombre) Then
                            conexion.AgregarParametroSQL("@par_Ciudad_Origen", filtro.Remesa.CiudadRemitente.Nombre)
                        End If

                        If filtro.Remesa.CiudadRemitente.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_Ciudad_Origen_Codigo", filtro.Remesa.CiudadRemitente.Codigo)
                        End If
                    End If

                    If Not IsNothing(filtro.Remesa.CiudadDestinatario) Then
                        If Not IsNothing(filtro.Remesa.CiudadDestinatario.Nombre) Then
                            conexion.AgregarParametroSQL("@par_Ciudad_Destino", filtro.Remesa.CiudadDestinatario.Nombre)
                        End If

                        If filtro.Remesa.CiudadDestinatario.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_Ciudad_Destino_Codigo", filtro.Remesa.CiudadDestinatario.Codigo)
                        End If
                    End If

                    If Not IsNothing(filtro.Remesa.Destinatario) Then
                        conexion.AgregarParametroSQL("@par_NombreDestinatario", filtro.Remesa.Destinatario.Nombre)
                    End If

                    If Not IsNothing(filtro.Remesa.Remitente) Then
                        If Not IsNothing(filtro.Remesa.Remitente.Nombre) Then
                            conexion.AgregarParametroSQL("@par_NombreRemitente", filtro.Remesa.Remitente.Nombre)
                        Else
                            If Not IsNothing(filtro.Remesa.Remitente.NombreCompleto) Then
                                conexion.AgregarParametroSQL("@par_NombreRemitente", filtro.Remesa.Remitente.NombreCompleto)
                            End If
                        End If
                    End If
                End If

                If filtro.NumeroPlanilla > -1 Then
                    conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.NumeroPlanilla)
                End If

                If filtro.Estado > -1 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If

                If filtro.Anulado > -1 Then
                    conexion.AgregarParametroSQL("@par_Anulado", filtro.Anulado)
                End If

                If filtro.Reexpedicion > -1 Then
                    conexion.AgregarParametroSQL("@par_Reexpedicion", filtro.Reexpedicion)
                End If

                If Not IsNothing(filtro.CodigoOficinaActualUsuario) Then
                    If filtro.CodigoOficinaActualUsuario > 0 Then
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo_Actual_Usuario", filtro.CodigoOficinaActualUsuario)
                    End If
                End If

                If Not IsNothing(filtro.CodigoCiudadOrigen) Then
                    If filtro.CodigoCiudadOrigen > 0 Then
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo_Origen", filtro.CodigoCiudadOrigen)
                    End If
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If

                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_remesas_pendientes_planillar_paqueteria")

                While resultado.Read
                    item = New RemesaPaqueteria(resultado)
                    If item.Reexpedicion = 1 Then
                        item.ListaReexpedicionOficinas = ConsultarDetalleReexpedicionConsulta(filtro.CodigoEmpresa, item.Remesa.Numero)
                    End If
                    lista.Add(item)
                End While

            End Using
            Return lista
        End Function
        Public Function ConsultarRemesasRecogerOficina(filtro As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria)
            Dim lista As New List(Of RemesaPaqueteria)
            Dim item As RemesaPaqueteria
            Dim resultado As IDataReader

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.NumeroInicial)

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_remesa_paqueteria_control_entregas_Ofcinas")

                While resultado.Read
                    item = New RemesaPaqueteria(resultado)
                    If item.Reexpedicion = 1 Then
                        item.ListaReexpedicionOficinas = ConsultarDetalleReexpedicionConsulta(filtro.CodigoEmpresa, item.Remesa.Numero)
                    End If
                    lista.Add(item)
                End While

            End Using
            Return lista
        End Function

        Public Function ConsultarRemesasRetornoContado(filtro As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria)
            Dim lista As New List(Of RemesaPaqueteria)
            Dim item As RemesaPaqueteria
            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.NumeroInicial > 0 Then
                    conexion.AgregarParametroSQL("@par_NumeroInicial", filtro.NumeroInicial)
                End If
                If filtro.FechaInicial > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_FechaInicial", filtro.FechaInicial, SqlDbType.Date)
                End If
                If filtro.FechaFinal > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_FechaFinal", filtro.FechaFinal, SqlDbType.Date)
                End If

                If Not IsNothing(filtro.Remesa) Then
                    If Not IsNothing(filtro.Remesa.Cliente) Then
                        If filtro.Remesa.Cliente.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", filtro.Remesa.Cliente.Codigo)
                        End If
                    End If

                    If Len(filtro.Remesa.NumeroDocumentoCliente) > 0 Then
                        conexion.AgregarParametroSQL("@par_Documento_Cliente", filtro.Remesa.NumeroDocumentoCliente)
                    End If

                    If Not IsNothing(filtro.TipoDocumento) Then
                        If filtro.TipoDocumento > 0 Then
                            conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento)
                        End If
                    End If

                    If Not IsNothing(filtro.Remesa.CiudadRemitente) Then
                        If filtro.Remesa.CiudadRemitente.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_Ciudad_Origen_Codigo", filtro.Remesa.CiudadRemitente.Codigo)
                        End If
                    End If

                    If Not IsNothing(filtro.Remesa.CiudadDestinatario) Then
                        If filtro.Remesa.CiudadDestinatario.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_Ciudad_Destino_Codigo", filtro.Remesa.CiudadDestinatario.Codigo)
                        End If
                    End If

                    If Not IsNothing(filtro.Remesa.Remitente) Then
                        If filtro.Remesa.Remitente.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Remitente", filtro.Remesa.Remitente.Codigo)
                        End If
                    End If
                End If

                If Not IsNothing(filtro.CodigoOficinaActualUsuario) Then
                    If filtro.CodigoOficinaActualUsuario > 0 Then
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo_Actual_Usuario", filtro.CodigoOficinaActualUsuario)
                    End If
                End If

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_remesas_retorno_contado_planilla_paqueteria")

                While resultado.Read
                    item = New RemesaPaqueteria(resultado)
                    lista.Add(item)
                End While

            End Using
            Return lista
        End Function

        Public Function ValidarPreimpreso(entidad As RemesaPaqueteria) As Long
            Dim Numero As Long = 0
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
                conexion.AgregarParametroSQL("@par_Numeracion", entidad.Numeracion)
                If entidad.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_Validar_Preimpreso_Remesa_Paqueteria")

                While resultado.Read
                    Numero = resultado.Item("Numero_Documento")
                End While

            End Using

            Return Numero
        End Function

        Public Function ConsultarRemesasPendientesRecibirOficina(filtro As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria)
            Dim lista As New List(Of RemesaPaqueteria)
            Dim item As RemesaPaqueteria
            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento)
                If filtro.NumeroInicial > 0 Then
                    conexion.AgregarParametroSQL("@par_NumeroInicial", filtro.NumeroInicial)
                End If
                If filtro.NumeroFinal > 0 Then
                    conexion.AgregarParametroSQL("@par_NumeroFinal", filtro.NumeroFinal)
                End If
                If filtro.FechaInicial > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_FechaInicial", filtro.FechaInicial, SqlDbType.Date)
                End If

                If filtro.FechaFinal > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_FechaFinal", filtro.FechaFinal, SqlDbType.Date)
                End If

                If Not IsNothing(filtro.Planilla) Then
                    If filtro.Planilla.Numero > 0 Then
                        conexion.AgregarParametroSQL("@par_ENDP_Numero", filtro.Planilla.Numero)
                    End If
                    If filtro.Planilla.NumeroDocumento > 0 Then
                        conexion.AgregarParametroSQL("@par_ENPD_Numero_Documento", filtro.Planilla.NumeroDocumento)
                    End If
                    If filtro.Planilla.TipoDocumento > 0 Then
                        conexion.AgregarParametroSQL("@par_ENPD_TIDO_Codigo", filtro.Planilla.TipoDocumento)
                    End If

                End If

                If Not IsNothing(filtro.OficinaRecibe) Then
                    If filtro.OficinaRecibe.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Oficina_Recibe_Codigo", filtro.OficinaRecibe.Codigo)
                    End If
                End If

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_remesas_pendientes_recibir_oficina_destino")
                While resultado.Read
                    item = New RemesaPaqueteria(resultado)
                    lista.Add(item)
                End While
            End Using
            Return lista
        End Function

        Public Function RecibirRemesaPaqueteriaOficinaDestino(entidad As RemesaPaqueteria) As Long
            Dim Cont As Integer = 0

            If Not IsNothing(entidad.RemesaPaqueteria) Then
                For Each reme In entidad.RemesaPaqueteria
                    Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                        conexion.CreateConnection()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_Numero", reme.Numero)
                        conexion.AgregarParametroSQL("@par_ENPD_Codigo", entidad.NumeroPlanilla)
                        conexion.AgregarParametroSQL("@par_ESRP_Numero", entidad.EstadoRemesaPaqueteria.Codigo)
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo_Actual", entidad.CodigoOficinaActual)
                        conexion.AgregarParametroSQL("@par_Ciudad_Actual", entidad.CodigoCiudad)
                        conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_recibir_remesas_oficina_destino")
                        While resultado.Read
                            If resultado.Item("Numero") > 0 Then
                                Cont += 1
                            End If
                        End While
                        resultado.Close()
                    End Using
                Next
            End If

            Return Cont
        End Function

        Public Function ConsultarRemesasPorLegalizar(filtro As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria)
            Dim lista As New List(Of RemesaPaqueteria)
            Dim item As RemesaPaqueteria
            Dim resultado As IDataReader

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.NumeroInicial > 0 Then
                    conexion.AgregarParametroSQL("@par_NumeroInicial", filtro.NumeroInicial)
                End If
                If filtro.FechaInicial > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_FechaInicial", filtro.FechaInicial, SqlDbType.Date)
                End If
                If filtro.FechaFinal > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_FechaFinal", filtro.FechaFinal, SqlDbType.Date)
                End If

                If Not IsNothing(filtro.Remesa) Then
                    If Not IsNothing(filtro.Remesa.Cliente) Then
                        If filtro.Remesa.Conductor.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", filtro.Remesa.Conductor.Codigo)
                        End If
                    End If
                    If Not IsNothing(filtro.TipoDocumento) Then
                        If filtro.TipoDocumento > 0 Then
                            conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento)
                        End If
                    End If
                End If

                If filtro.NumeroPlanilla > 0 Then
                    conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.NumeroPlanilla)
                End If

                If Not IsNothing(filtro.OficinaActual) Then
                    If filtro.OficinaActual.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_OFIC_Actual", filtro.OficinaActual.Codigo)
                    End If
                End If

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_remesas_pendientes_legalizar_paqueteria")

                While resultado.Read
                    item = New RemesaPaqueteria(resultado)
                    item.GestionDocumentosRemesa = ConsultarDetalleGestionDocumentos(item.CodigoEmpresa, item.Remesa.Numero)
                    lista.Add(item)
                End While

            End Using
            Return lista
        End Function
        Public Function ConsultarRemesasPorLegalizarRecaudo(filtro As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria)
            Dim lista As New List(Of RemesaPaqueteria)
            Dim item As RemesaPaqueteria
            Dim resultado As IDataReader

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.NumeroInicial > 0 Then
                    conexion.AgregarParametroSQL("@par_NumeroInicial", filtro.NumeroInicial)
                End If
                If Not IsNothing(filtro.TipoDocumento) Then
                    If filtro.TipoDocumento > 0 Then
                        conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento)
                    End If
                End If
                If filtro.FechaInicial > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_FechaInicial", filtro.FechaInicial, SqlDbType.Date)
                End If
                If filtro.FechaFinal > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_FechaFinal", filtro.FechaFinal, SqlDbType.Date)
                End If
                If filtro.NumeroPlanilla > 0 Then
                    conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.NumeroPlanilla)
                End If

                If filtro.NumeroDocumentoLegalizarGuias > 0 Then
                    conexion.AgregarParametroSQL("@par_ELGU_Numero", filtro.NumeroDocumentoLegalizarGuias)
                End If

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_remesas_pendientes_legalizar_recaudo")

                While resultado.Read
                    item = New RemesaPaqueteria(resultado)
                    lista.Add(item)
                End While

            End Using
            Return lista
        End Function

        Public Function InsertarMasivo(entidad As RemesaPaqueteria) As Long
            'Using transaccion = New TransactionScope(TransactionScopeOption.Required)
            Dim UltimoConsecutivo As Long


            Dim dts As New DataSet()
            Dim tbl As DataTable
            ' el nombre del dataTable puede variar respecto a la tabla de destino, sin embargo, el orden de las columnas debe ser igual a como está en la tabla destino
            tbl = dts.Tables.Add("V_Encabezado_Remesas_Cargue_Masivo")
            tbl.Columns.Add("EMPR_Codigo")
            tbl.Columns.Add("Numero")
            tbl.Columns.Add("TIDO_Codigo")
            tbl.Columns.Add("Numero_Documento")
            tbl.Columns.Add("Documento_Cliente")
            tbl.Columns.Add("VEHI_Codigo")
            tbl.Columns.Add("Cumplido")
            tbl.Columns.Add("Numeracion")
            tbl.Columns.Add("CATA_TIRE_Codigo")
            tbl.Columns.Add("Fecha")
            tbl.Columns.Add("RUTA_Codigo")
            tbl.Columns.Add("PRTR_Codigo")
            tbl.Columns.Add("CATA_FOPR_Codigo")
            tbl.Columns.Add("TERC_Codigo_Cliente")
            tbl.Columns.Add("TERC_Codigo_Remitente")
            tbl.Columns.Add("CIUD_Codigo_Remitente")
            tbl.Columns.Add("Direccion_Remitente")
            tbl.Columns.Add("Telefonos_Remitente")
            tbl.Columns.Add("Cantidad_Cliente")
            tbl.Columns.Add("Peso_Cliente")
            tbl.Columns.Add("Peso_Volumetrico_Cliente")
            tbl.Columns.Add("DTCV_Codigo")
            tbl.Columns.Add("Valor_Flete_Cliente")
            tbl.Columns.Add("Valor_Manejo_Cliente")
            tbl.Columns.Add("Valor_Seguro_Cliente")
            tbl.Columns.Add("Valor_Descuento_Cliente")
            tbl.Columns.Add("Total_Flete_Cliente")
            tbl.Columns.Add("Valor_Comercial_Cliente")
            tbl.Columns.Add("Cantidad_Transportador")
            tbl.Columns.Add("Peso_Transportador")
            tbl.Columns.Add("Valor_Flete_Transportador")
            tbl.Columns.Add("Total_Flete_Transportador")
            tbl.Columns.Add("TERC_Codigo_Destinatario")
            tbl.Columns.Add("CIUD_Codigo_Destinatario")
            tbl.Columns.Add("Direccion_Destinatario")
            tbl.Columns.Add("Telefonos_Destinatario")
            tbl.Columns.Add("Fecha_Crea", GetType(DateTime))
            tbl.Columns.Add("USUA_Codigo_Crea")
            tbl.Columns.Add("OFIC_Codigo")
            tbl.Columns.Add("ETCC_Numero")
            tbl.Columns.Add("ETCV_Numero")
            tbl.Columns.Add("ESOS_Numero")
            tbl.Columns.Add("ENPD_Numero")
            tbl.Columns.Add("ENPR_Numero")
            tbl.Columns.Add("ENPE_Numero")
            tbl.Columns.Add("ENMC_Numero")
            tbl.Columns.Add("ECPD_Numero")
            tbl.Columns.Add("ENFA_Numero")
            tbl.Columns.Add("Anulado")
            tbl.Columns.Add("Estado")
            tbl.Columns.Add("Valor_Reexpedicion")
            tbl.Columns.Add("Valor_Cargue")
            tbl.Columns.Add("Valor_Descargue")
            tbl.Columns.Add("Observaciones")
            tbl.Columns.Add(" Codigo_Postal_Remitente")
            tbl.Columns.Add(" Codigo_Postal_Destinatario")
            'Remesas_Paqueteria
            Dim dtsrp As New DataSet()
            Dim tblrp As DataTable
            ' el nombre del dataTable puede variar respecto a la tabla de destino, sin embargo, el orden de las columnas debe ser igual a como está en la tabla destino
            tblrp = dts.Tables.Add("V_Remesas_Paqueteria_Cargue_Masivo")
            tblrp.Columns.Add("EMPR_Codigo")
            tblrp.Columns.Add("ENRE_Numero")
            tblrp.Columns.Add("CATA_ESRP_Codigo")
            tblrp.Columns.Add("CATA_LNPA_Codigo")
            tblrp.Columns.Add("CATA_HERP_Codigo")
            tblrp.Columns.Add("CATA_TTRP_Codigo")
            tblrp.Columns.Add("CATA_TDRP_Codigo")
            tblrp.Columns.Add("CATA_TPRP_Codigo")
            tblrp.Columns.Add("CATA_TSRP_Codigo")
            tblrp.Columns.Add("CATA_TERP_Codigo ")
            tblrp.Columns.Add("CATA_TIRP_Codigo")
            tblrp.Columns.Add("CIUD_Codigo_Origen")
            tblrp.Columns.Add("CIUD_Codigo_Destino")
            tblrp.Columns.Add("OFIC_Codigo_Origen")
            tblrp.Columns.Add("OFIC_Codigo_Destino")
            tblrp.Columns.Add("OFIC_Codigo_Actual")
            tblrp.Columns.Add("Descripcion_Mercancia")
            tblrp.Columns.Add("Largo")
            tblrp.Columns.Add("Alto")
            tblrp.Columns.Add("Ancho")
            tblrp.Columns.Add("Peso_Volumetrico")
            tblrp.Columns.Add("Peso_A_Cobrar")
            tblrp.Columns.Add("Recoger_Oficina_Destino")
            tblrp.Columns.Add("Reexpedicion")
            tblrp.Columns.Add("ZOCI_Codigo_Entrega")
            tblrp.Columns.Add("Latitud")
            tblrp.Columns.Add("Longitud")
            tblrp.Columns.Add("Centro_Costo")
            tblrp.Columns.Add("Observaciones_Remitente")
            tblrp.Columns.Add("Observaciones_Destinatario")
            tblrp.Columns.Add("TERC_Codigo_Transportador_Externo")
            tblrp.Columns.Add("Numero_Guia_Externa")
            tblrp.Columns.Add("OFIC_Codigo_Recibe")
            tblrp.Columns.Add("SICD_Codigo_Descargue")

            For Each guia As RemesaPaqueteria In entidad.RemesaPaqueteria
                UltimoConsecutivo = guia.Remesa.NumeroIdentificacionRecibe
                tbl.Rows.Add(guia.CodigoEmpresa, guia.Remesa.Numero, 110, guia.Remesa.NumeroDocumento, 0, 0, 0, 0, guia.Remesa.TipoRemesa.Codigo, Date.Now(), guia.Remesa.Ruta.Codigo, guia.Remesa.ProductoTransportado.Codigo, guia.Remesa.FormaPago.Codigo, guia.Remesa.Remitente.Codigo, guia.Remesa.Remitente.Codigo, guia.Remesa.CiudadRemitente.Codigo, guia.Remesa.DireccionRemitente, guia.Remesa.TelefonoRemitente, guia.Remesa.CantidadCliente, guia.Remesa.PesoCliente, guia.Remesa.PesoVolumetricoCliente, guia.Remesa.DetalleTarifaVenta.Codigo, guia.Remesa.ValorFleteCliente, guia.Remesa.ValorManejoCliente, guia.Remesa.ValorSeguroCliente, 0, guia.Remesa.TotalFleteCliente, guia.Remesa.ValorComercialCliente, guia.Remesa.CantidadTransportador, guia.Remesa.PesoTransportador, guia.Remesa.ValorFleteTransportador, guia.Remesa.TotalFleteTransportador, guia.Remesa.Destinatario.Codigo, guia.Remesa.CiudadDestinatario.Codigo, guia.Remesa.DireccionDestinatario, guia.Remesa.TelefonoDestinatario, Convert.ToDateTime(Date.Now()).ToString, 0, guia.Remesa.Oficina.Codigo, 0, guia.Remesa.TarifarioVenta.Codigo, guia.Remesa.NumeroOrdenServicio, 0, 0, 0, 0, 0, 0, 0, 1, guia.ValorReexpedicion, guia.Remesa.ValorCargue, guia.Remesa.ValorDescargue, guia.Remesa.Observaciones, guia.Remesa.CodigoPostalRemitente, guia.Remesa.CodigoPostalDestinatario)
                tblrp.Rows.Add(guia.CodigoEmpresa, guia.Remesa.Numero, guia.EstadoRemesaPaqueteria.Codigo, guia.LineaNegocioPaqueteria.Codigo, guia.HorarioEntregaRemesa.Codigo, 6200, 6300, 6400, 6500, guia.TipoEntregaRemesaPaqueteria.Codigo, 6700, guia.Remesa.CiudadRemitente.Codigo, guia.Remesa.CiudadDestinatario.Codigo, guia.OficinaOrigen.Codigo, guia.OficinaDestino.Codigo, guia.OficinaActual.Codigo, guia.DesripcionMercancia, guia.Largo, guia.Alto, guia.Ancho, guia.PesoVolumetrico, guia.PesoCobrar, guia.RecogerOficinaDestino, IIf(guia.ValorReexpedicion > 0, 1, 0), guia.CodigoZona, guia.Latitud, guia.Longitud, guia.CentroCosto, guia.Remesa.Observaciones, guia.Remesa.Observaciones, guia.TransportadorExterno.Codigo, guia.NumeroGuiaExterna, guia.OficinaDestino.Codigo, guia.SitioDescargue.Codigo)
            Next

            Using destinationConnection As SqlConnection =
                New SqlConnection(ConnectionString)


                Using bulkCopy As SqlClient.SqlBulkCopy =
                  New SqlClient.SqlBulkCopy(ConnectionString, SqlBulkCopyOptions.KeepIdentity)
                    destinationConnection.Open()

                    bulkCopy.DestinationTableName = "V_Encabezado_Remesas_Cargue_Masivo"
                    bulkCopy.BulkCopyTimeout = 0 ' se ajusta en cero ya que es mejor para grandes cantidades de registros

                    Try

                        bulkCopy.WriteToServer(tbl)

                    Catch ex As Exception

                        Return 0
                        ' transaccion.Dispose()
                    Finally

                    End Try

                    bulkCopy.DestinationTableName = "V_Remesas_Paqueteria_Cargue_Masivo"

                    Try

                        bulkCopy.WriteToServer(tblrp)

                    Catch ex As Exception
                        'transaccion.Dispose()
                        Return 0

                    Finally

                    End Try
                End Using


            End Using

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                Dim inserto As Integer
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Ultimo_Consecutivo", UltimoConsecutivo)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_consecutivo_tipo_documento_remesa_paqueteria")

                While resultado.Read
                    inserto = IIf((resultado.Item("Codigo")) > 0, 1, 0)
                End While

                resultado.Close()

                conexion.CloseConnection()

                If inserto = 0 Then
                    Return inserto
                    'transaccion.Dispose()
                End If
            End Using

            'transaccion.Complete()
            'End Using

            Return entidad.RemesaPaqueteria.Count
        End Function


        Public Function InsertarNuevosTerceros(entidad As Terceros) As Long

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Dim dts As New DataSet()
                Dim tbl As DataTable
                ' el nombre del dataTable puede variar respecto a la tabla de destino, sin embargo, el orden de las columnas debe ser igual a como está en la tabla destino
                tbl = dts.Tables.Add("Terceros")
                tbl.Columns.Add("EMPR_Codigo")
                tbl.Columns.Add("Codigo")
                tbl.Columns.Add("CATA_TINT_Codigo")
                tbl.Columns.Add("CATA_TIID_Codigo")
                tbl.Columns.Add("Numero_Identificacion")
                tbl.Columns.Add("Digito_Chequeo")
                tbl.Columns.Add("CIUD_Codigo_Identificacion")
                tbl.Columns.Add("CIUD_Codigo_Nacimiento")
                tbl.Columns.Add("Nombre")
                tbl.Columns.Add("Apellido1")
                tbl.Columns.Add("Apellido2")
                tbl.Columns.Add("CATA_SETE_Codigo")
                tbl.Columns.Add("PAIS_Codigo")
                tbl.Columns.Add("CIUD_Codigo")
                tbl.Columns.Add("Direccion")
                tbl.Columns.Add("Telefonos")
                tbl.Columns.Add("Observaciones")
                tbl.Columns.Add("Reporto_Contabilidad")
                tbl.Columns.Add("CATA_TICB_Codigo")
                tbl.Columns.Add("Estado")
                tbl.Columns.Add("USUA_Codigo_Crea")
                tbl.Columns.Add("Fecha_Crea", GetType(DateTime))



                'Tabla Tercero_Perfiles

                Dim dtstp As New DataSet()
                Dim tbltp As DataTable
                ' el nombre del dataTable puede variar respecto a la tabla de destino, sin embargo, el orden de las columnas debe ser igual a como está en la tabla destino
                tbltp = dts.Tables.Add("Perfil_Terceros")
                tbltp.Columns.Add("EMPR_Codigo")
                tbltp.Columns.Add("TERC_Codigo")
                tbltp.Columns.Add("Codigo")

                For Each tercero As Terceros In entidad.ListaNuevosTerceros
                    tbl.Rows.Add(entidad.CodigoEmpresa, tercero.Codigo, 501, tercero.TipoIdentificacion.Codigo, tercero.NumeroIdentificacion, 0, tercero.Ciudad.Codigo, tercero.Ciudad.Codigo, tercero.Nombre, "", "", 600, 0, tercero.Ciudad.Codigo, tercero.Direccion, tercero.Telefonos, "creado desde cargue masivo guías", 0, 400, 1, 0, Convert.ToDateTime(Date.Now()).ToString)
                    tbltp.Rows.Add(entidad.CodigoEmpresa, tercero.Codigo, 1404)
                Next



                Using destinationConnection As SqlConnection =
                New SqlConnection(ConnectionString)


                    Using bulkCopy As SqlClient.SqlBulkCopy =
                  New SqlClient.SqlBulkCopy(destinationConnection)
                        destinationConnection.Open()

                        bulkCopy.DestinationTableName = "V_Terceros_Cargue_Masivo"
                        bulkCopy.BulkCopyTimeout = 0 ' se ajusta en cero ya que es mejor para grandes cantidades de registros

                        Try

                            bulkCopy.WriteToServer(tbl)

                        Catch ex As Exception
                            transaccion.Dispose()
                            Return 0
                        Finally

                        End Try

                        bulkCopy.DestinationTableName = "Perfil_Terceros"

                        Try

                            bulkCopy.WriteToServer(tbltp)

                        Catch ex As Exception
                            transaccion.Dispose()
                            Return 0

                        Finally

                        End Try

                    End Using


                End Using

                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    Dim inserto As Integer
                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Consecutivo", entidad.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_consecutivo_tipo_documento_tercero")

                    While resultado.Read
                        inserto = IIf((resultado.Item("Codigo")) > 0, 1, 0)
                    End While

                    resultado.Close()

                    conexion.CloseConnection()

                    If inserto = 0 Then
                        Return inserto
                        transaccion.Dispose()
                    End If
                End Using

                transaccion.Complete()
            End Using


            Return entidad.ListaNuevosTerceros.Count
        End Function

        Public Function ConvertToDataTable(Of T)(ByVal list As IList(Of T)) As DataTable
            Dim table As New DataTable()
            Dim fields() As FieldInfo = GetType(T).GetFields()
            For Each field As FieldInfo In fields
                table.Columns.Add(field.Name, field.FieldType)
            Next
            For Each item As T In list
                Dim row As DataRow = table.NewRow()
                For Each field As FieldInfo In fields
                    row(field.Name) = field.GetValue(item)
                Next
                table.Rows.Add(row)
            Next
            Return table
        End Function



        Public Function ConsultarTercerosIdentificacion(filtro As Terceros) As IEnumerable(Of Terceros)
            Dim lista As New List(Of Terceros)
            Dim item As Terceros
            Dim resultado As IDataReader
            'Using transaccion = New TransactionScope(TransactionScopeOption.Required)

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Lista_Identificaciones", filtro.strIdentificaciones)

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_terceros_por_identificacion")


                While resultado.Read
                    item = New Terceros(resultado)

                    lista.Add(item)
                End While

                'conexion.CloseConnection()
            End Using
            'End Using
            Return lista
        End Function



        Public Function ObtenerUltimoConsecutivoTercero(filtro As Terceros) As Terceros
            Dim item As New Terceros


            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_ultimo_consecutivo_tercero")

                While resultado.Read
                    item = New Terceros(resultado)
                End While

                conexion.CloseConnection()
            End Using


            Return item
        End Function

        Public Function ObtenerUltimaGuia(filtro As RemesaPaqueteria) As RemesaPaqueteria
            Dim item As New RemesaPaqueteria


            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_ultima_guia_paqueteria")

                While resultado.Read
                    item = New RemesaPaqueteria(resultado)
                End While

                conexion.CloseConnection()
            End Using


            Return item
        End Function

    End Class


    '<JsonObject>
    'Public Class Contexto
    '    Inherits DbContext
    '    Dim conexion As IDbConnection
    '    Sub New(options As DbContextOptions(Of EncabezadoRemesa), conexion As IDbConnection)
    '        Me.conexion = conexion

    '    End Sub



    '    <JsonProperty>
    '    Public Property Guias As DbSet(Of EncabezadoRemesa)

    'End Class


    Public NotInheritable Class SqlBulkCopy
        Implements IDisposable

        Private disposedValue As Boolean
        Dim PPmt As SqlConnection
        Public Sub New(connectionString As String)
            PPmt = New SqlConnection(connectionString)

        End Sub

        Private Sub Dispose(disposing As Boolean)
            If Not disposedValue Then
                If disposing Then
                    ' TODO: eliminar el estado administrado (objetos administrados)
                End If

                ' TODO: liberar los recursos no administrados (objetos no administrados) y reemplazar el finalizador
                ' TODO: establecer los campos grandes como NULL
                disposedValue = True
            End If
        End Sub

        ' ' TODO: reemplazar el finalizador solo si "Dispose(disposing As Boolean)" tiene código para liberar los recursos no administrados
        ' Protected Overrides Sub Finalize()
        '     ' No cambie este código. Coloque el código de limpieza en el método "Dispose(disposing As Boolean)".
        '     Dispose(disposing:=False)
        '     MyBase.Finalize()
        ' End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            ' No cambie este código. Coloque el código de limpieza en el método "Dispose(disposing As Boolean)".
            Dispose(disposing:=True)
            GC.SuppressFinalize(Me)
        End Sub
    End Class
End Namespace
