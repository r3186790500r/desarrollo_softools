﻿EncoExpresApp.controller("GestionarEntradaAlmacenCtrl", ['$scope', '$timeout', '$routeParams', 'blockUI', '$linq', 'DocumentoAlmacenesFactory', 'AlmacenesFactory', 'OrdenCompraFactory', function ($scope, $timeout, $routeParams, blockUI, $linq, DocumentoAlmacenesFactory, AlmacenesFactory, OrdenCompraFactory) {

    $scope.MapaSitio = [{ Nombre: 'Almacén' }, { Nombre: 'Documentos' }, { Nombre: 'Entradas Almacén' }];

    //----------------------------- Decalaracion Variables ------------------------------------------------------//


    $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_ENTRADA_ALMACENES);
    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

    //Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

    $scope.Modelo = {
        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        Numero: 0,
        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        TipoDocumento: CODIGO_TIPO_DOCUMENTO_ENTRADA_ALMACEN,
        Oficina: {
            Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
            Nombre: $scope.Sesion.UsuarioAutenticado.Oficinas.Nombre,
        },
        OrdenCompra: { Numero: '' },
        Detalles: []
    };


    $scope.MensajesError = [];
    $scope.ListadoDetalle = [];
    $scope.ListadoDetalleOrdenCompra = [];
    $scope.ListadoOrdenCompra = [];
    $scope.AuxiListadoDetalleOrdenCompra = [];
    $scope.ListadoAlmacenes = [];
    $scope.Titulo = 'NUEVA ENTRADA ALMACÉN';
    $scope.Fecha = new Date();
    $scope.ModeloFecha = new Date();
    $scope.ListadoEstados = [
        { Nombre: 'DEFINITIVO', Codigo: ESTADO_DEFINITIVO },
        { Nombre: 'BORRADOR', Codigo: ESTADO_BORRADOR }
    ];
    $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + ESTADO_BORRADOR);

    /* Obtener parametros del enrutador*/
    if ($routeParams.Numero !== undefined) {
        $scope.Modelo.Numero = $routeParams.Numero;
        $scope.Modelo.ConsultaKardex = $routeParams.ConsultaKardex;
    }
    else {
        $scope.Modelo.Numero = 0;
    }

    // Metodo para volver a la página de consulta
    $scope.VolverMaster = function () {
        if ($scope.Modelo.ConsultaKardex == 0 || $scope.Modelo.ConsultaKardex == undefined) {
            if ($scope.Modelo.Numero > 0) {
                document.location.href = '#!ConsultarEntradaAlmacenes/' + $scope.Modelo.Numero;
            }
            else {
                document.location.href = '#!ConsultarEntradaAlmacenes';
            }
        }
        else {
            document.location.href = '#!ConsultarKardex';
        }
    };

    //-------------------------------------------------------------------------------------------Cargar Combos/Autocomplete----------------------------------------------------------------
    //Cargar el combo de almacenes
    $scope.ListadoAlmacenes = AlmacenesFactory.Consultar({
        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        Estado: ESTADO_DEFINITIVO,
        Sync: true
    }).Datos;
    $scope.Modelo.Almacen = $scope.ListadoAlmacenes[0];

    // validar Orden compra
    $scope.ValidarOrdenCompra = function (Numero) {
        if ($scope.Modelo.Almacen !== undefined && $scope.Modelo.Almacen !== '' && $scope.Modelo.Almacen.Codigo > 0) {

            if (Numero !== '' && Numero !== undefined && Numero > 0) {
                var responseOrdenCompra = OrdenCompraFactory.Obtener({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: Numero,
                    Sync: true
                });
                if (responseOrdenCompra.ProcesoExitoso === true) {
                    if (responseOrdenCompra.Datos.Estado.Codigo == ESTADO_BORRADOR || responseOrdenCompra.Datos.Anulado == ESTADO_ANULADO) {
                        $scope.Modelo.OrdenCompra.Numero = '';
                        ShowError('La orden de compra se encuentra en borrador o anulada');
                    } else if (responseOrdenCompra.Datos.Almacen.Codigo !== $scope.Modelo.Almacen.Codigo) {
                        $scope.Modelo.OrdenCompra.Numero = '';
                        ShowError('La orden de compra no pertenece al almacen ' + $scope.Modelo.Almacen.Nombre);
                    } else {
                        // Cargar información de la Orden de Compra
                        if ($scope.Modelo.Numero == 0) {
                            $scope.Modelo.NombreRecibe = responseOrdenCompra.Datos.ContactoRecibe
                            $scope.Modelo.Proveedor = responseOrdenCompra.Datos.Proveedor
                            $scope.ListadoDetalleOrdenCompra = [];
                            $scope.AuxiListadoDetalleOrdenCompra = [];
                            $scope.AuxiListadoDetalleOrdenCompra = responseOrdenCompra.Datos.Detalles;

                            $scope.AuxiListadoDetalleOrdenCompra.forEach(item => {
                                item.ValorUnitario = parseInt(item.ValorNeto) / parseInt(item.Cantidad)
                                if (item.Cantidad > item.CantidadRecibida) {
                                    item.Cantidad = Math.ceil(item.Cantidad) - Math.ceil(item.CantidadRecibida);
                                    item.AuxiCantidad = item.Cantidad;
                                    item.ValorAntesDescuento = Math.ceil(item.Cantidad) * Math.ceil(item.ValorUnitario);
                                    item.UnidadMedida = item.UnidadMedida.NombreCorto;
                                    item.UnidadEmpaque = item.UnidadEmpaque.NombreCorto;
                                }
                                $scope.ListadoDetalleOrdenCompra.push(item)

                            });
                            if ($scope.ListadoDetalleOrdenCompra.length == 0) {
                                ShowError('La orden de compra No. ' + NumeroOrden + ' no tiene cantidades pendientes');
                                $scope.AuxiListadoDetalleOrdenCompra = response.data.Datos.Detalles;
                                $scope.ValorTotal = ''
                                $scope.NombreRecibe = ''
                                $scope.Proveedor = ''
                            }
                        }
                        else {
                            $scope.ListadoDetalleOrdenCompra.forEach(function (item) {
                                response.data.Datos.Detalles.forEach(function (itemOrden) {
                                    item.ValorUnitario = parseInt(itemOrden.ValorNeto) / parseInt(itemOrden.Cantidad)
                                    if (itemOrden.Cantidad > itemOrden.CantidadRecibida) {
                                        itemOrden.Cantidad = Math.ceil(itemOrden.Cantidad - itemOrden.CantidadRecibida)
                                        itemOrden.AuxiCantidad = itemOrden.Cantidad
                                        itemOrden.ValorAntesDescuento = Math.ceil(itemOrden.Cantidad * itemOrden.ValorUnitario)
                                        if (itemOrden.ReferenciaAlmacen.Codigo == item.ReferenciaAlmacen.Codigo && itemOrden.ReferenciaProveedor.Codigo == item.ReferenciaProveedor.Codigo) {
                                            item.AuxiCantidad = itemOrden.AuxiCantidad;
                                        }
                                    }
                                })
                            })
                        }
                    }
                } else {
                    $scope.Modelo.OrdenCompra.Numero = '';
                    ShowError('No se encontro la orden de compra con el número ingresado');
                }
            }
        }
        else {
            $scope.Modelo.OrdenCompra.Numero = '';
            ShowError('Debe seleccionar un almacen');
        }
    }




    // Metodo para guardar /modificar
    $scope.Guardar = function () {
        //$scope.MaskNumero();
        closeModal('modalConfirmacionGuardarEntrada');
        //Información encabezado
     
        $scope.Modelo.Fecha = $scope.Modelo.Fecha;
        $scope.Modelo.Almacen = { Codigo: $scope.Modelo.Almacen.Codigo };
        $scope.Modelo.Almacentraslado = $scope.Modelo.Proveedor.Codigo;
        $scope.Modelo.ENDATraslado = $scope.Modelo.Proveedor.Codigo;
        $scope.Modelo.NumeroOrdenCompra = $scope.Modelo.OrdenCompra.Numero;
        $scope.Modelo.NumeroRemision = $scope.Modelo.NumeroRemision;
        $scope.Modelo.NombreRecibe = $scope.Modelo.NombreRecibe;
        $scope.Modelo.Observaciones = $scope.Modelo.Observaciones;
        $scope.Modelo.Oficina = { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo, Nombre: $scope.Sesion.UsuarioAutenticado.Oficinas.Nombre };
        $scope.Modelo.Estado = { Codigo: $scope.Modelo.Estado.Codigo }
        //Información detalle
        $scope.ListadoDetalleOrdenCompra.forEach(Detalles => {
            $scope.Modelo.Detalles.push({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                ReferenciaAlmacen: { Codigo: Detalles.ReferenciaAlmacen.Codigo },
                ValorUnitario: Detalles.ValorUnitario,
                Cantidad: Detalles.Cantidad,
                ValorTotal: Detalles.ValorNeto
            });

        });


        if (DatosRequeridos()) {
            DocumentoAlmacenesFactory.Guardar($scope.Modelo).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Modelo.Numero == 0) {
                                ShowSuccess('Se guardó la entrada almacén No.' + response.data.Datos);
                                location.href = '#!ConsultarEntradaAlmacenes/' + response.data.Datos;
                            }
                            else {
                                ShowSuccess('Se modificó la entrada almacén No.' + response.data.Datos);
                                location.href = '#!ConsultarEntradaAlmacenes/' + $scope.Modelo.NumeroDocumento;
                            }
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }
    };


    function DatosRequeridos() {
        $scope.MensajesError = [];
        var continuar = true;
        $scope.ValidarCantidad1 = true
        $scope.ValidarCantidad2 = true
        if ($scope.Modelo.Fecha == '' || $scope.Modelo.Fecha == undefined || $scope.Modelo.Fecha == null) {
            $scope.MensajesError.push('Debe ingresar una fecha');
            continuar = false;
        }
        if ($scope.Modelo.Almacen == '' || $scope.Modelo.Almacen == undefined || $scope.Modelo.Almacen == null) {
            $scope.MensajesError.push('Debe seleccionar un almacén');
            continuar = false;
        }
        if ($scope.Modelo.OrdenCompra.Numero == '' || $scope.Modelo.OrdenCompra.Numero == undefined || $scope.Modelo.OrdenCompra.Numero == null) {
            $scope.MensajesError.push('Debe ingresar la orden compra');
            continuar = false;
        }
        if ($scope.Modelo.NombreRecibe == '' || $scope.Modelo.NombreRecibe == undefined || $scope.Modelo.NombreRecibe == null) {
            $scope.MensajesError.push('Debe ingresar el contacto recibe');
            continuar = false;
        }
        if ($scope.Modelo.NumeroRemision == '' || $scope.Modelo.NumeroRemision == undefined || $scope.Modelo.NumeroRemision == null) {
            $scope.Modelo.NumeroRemision = 0
        }
        if ($scope.Modelo.Observaciones == '' || $scope.Modelo.Observaciones == undefined || $scope.Modelo.Observaciones == null) {
            $scope.Modelo.Observaciones = ''
        }
        if ($scope.ListadoDetalleOrdenCompra.length == 0 || $scope.ListadoDetalleOrdenCompra.length == undefined) {
            $scope.MensajesError.push('Debe ingresar mínimo un detalle');
            continuar = false;
        }
        $scope.ListadoDetalleOrdenCompra.forEach(function (item) {
            var con1 = 0
            var con2 = 0

            if (item.Cantidad < 0) {
                con1++
            }
            else if (item.Cantidad > item.AuxiCantidad) {
                con2++
            }
            if (con1 > 0) {
                $scope.ValidarCantidad1 = false
            }
            if (con2 > 0) {
                $scope.ValidarCantidad2 = false
            }
        })
        if ($scope.ValidarCantidad1 == false) {
            $scope.MensajesError.push('La cantidad ingresada debe ser igual o mayor a 0');
            continuar = false;

        }
        if ($scope.ValidarCantidad2 == false) {
            $scope.MensajesError.push('La cantidad ingresada debe ser menor o igual a la cantidad pendiente de recibir de la Orden de Compra');
            continuar = false;
        }
        return continuar
    }

    $scope.ConfirmacionGuardarEntrada = function () {
        showModal('modalConfirmacionGuardarEntrada');
    };

    /*-------------------------------------------------------------------------------------------------------*/
    if ($scope.Modelo.Numero > 0) {
        // Consultar los datos de la factura correspondientes al número que llego como parametro del enrutador
        $scope.Titulo = 'EDITAR ENTRADA ALMACÉN';
        $scope.Deshabilitar = true;
        $scope.Bloquear = true;
        Obtener();
    }

    // Metodo Obtener Contratos 
    function Obtener() {
        blockUI.start('cargando entrada número ' + $scope.Modelo.Numero);
        $timeout(function () {
            blockUI.message('cargando entrada número ' + $scope.Modelo.Numero);
        }, 200);

        filtros = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Numero: $scope.Modelo.Numero,
            TipoDocumento: CODIGO_TIPO_DOCUMENTO_ENTRADA_ALMACEN,
        };

        blockUI.delay = 1000;
        DocumentoAlmacenesFactory.Obtener(filtros).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.Modelo.NumeroDocumento = response.data.Datos.NumeroDocumento; // proximo erro
                    var fecha = new Date(response.data.Datos.Fecha);
                    fecha.setHours(0);
                    fecha.setMinutes(0);
                    fecha.setMilliseconds(0);
                    $scope.Modelo.Fecha = fecha;

                    if ($scope.ListadoAlmacenes.length > 0) {
                        $scope.Modelo.Almacen = $linq.Enumerable().From($scope.ListadoAlmacenes).First('$.Codigo == ' + response.data.Datos.Almacen.Codigo);
                    }

                    $scope.Modelo.OrdenCompra.Numero = response.data.Datos.NumeroOrdenCompra;
                    $scope.Modelo.Proveedor = response.data.Datos.Proveedor;
                    $scope.Modelo.NumeroRemision = response.data.Datos.NumeroRemision;
                    $scope.Modelo.NombreRecibe = response.data.Datos.NombreRecibe;
                    $scope.Modelo.Observaciones = response.data.Datos.Observaciones;

                    $scope.DetalleOrdenCompra = response.data.Datos.Detalles.forEach(item => {
                        item.UnidadMedida = item.UnidadMedida.NombreCorto
                        item.UnidadEmpaque = item.UnidadEmpaque.NombreCorto
                        item.AuxiCantidad = item.Cantidad;
                        $scope.ListadoDetalleOrdenCompra.push(item)
                    });

                    if ($scope.ListadoEstados.length > 0) {
                        if (response.data.Datos.Estado.Codigo !== ESTADO_ANULADO) {
                            $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado.Codigo);
                        } else {
                            $scope.ListadoEstados.push({ Nombre: 'Anulado', Codigo: ESTADO_ANULADO });
                            $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado.Codigo);
                            $scope.Deshabilitar = true;
                            $scope.Bloquear = true;
                        }
                        if (response.data.Datos.Estado.Codigo == ESTADO_BORRADOR) {
                            $scope.Deshabilitar = false;
                            $scope.Bloquear = true;
                        }
                        else if (response.data.Datos.Estado.Codigo == ESTADO_DEFINITIVO) {
                            $scope.Deshabilitar = true;
                            $scope.Bloquear = true;
                        }
                    }
                }
                else {
                    ShowError('no se logro consultar la entrada no.' + $scope.modelo.numero + '. ' + response.data.mensajeoperacion);
                    Document.location.href = '#!consultarentradaalmacenes';
                }
            }, function (response) {
                ShowError(response.statustext);
                Document.location.href = '#!consultarentradaalmacenes';
            });

        blockUI.stop();
    };
    $scope.Calcular = function () {
        $scope.MensajesError = [];
        var valido = true
        $scope.ListadoDetalleOrdenCompra.forEach(function (item) {
            if (item.AuxiCantidad >= item.Cantidad) {
                item.ValorAntesDescuento = Math.ceil(item.Cantidad * item.ValorUnitario)
            }
            if (item.Cantidad < 0) {
                $scope.MensajesError.push('La cantidad Ingresada debe ser igual o mayor a 0');
            }
            else if (item.Cantidad > item.AuxiCantidad) {
                valido = false
                item.Cantidad = item.AuxiCantidad
            }
        })

        if (valido == false) {
            $scope.MensajesError.push('La cantidad ingresada debe ser igual o menor a la cantidad inicial');
        }

    }

}]);

