﻿PRINT 'gsp_consultar_plan_unico_cuentas'
GO
DROP PROCEDURE gsp_consultar_plan_unico_cuentas
GO
CREATE PROCEDURE gsp_consultar_plan_unico_cuentas 
(    
 @par_EMPR_Codigo SMALLINT,    
 @par_Codigo NUMERIC = NULL,    
 @par_Codigo_Cuenta VARCHAR(50) = NULL,    
 @par_Codigo_Alterno VARCHAR(20) = NULL,    
 @par_Nombre VARCHAR(50) = NULL,    
 @par_Estado SMALLINT = NULL,    
 @par_NumeroPagina INT = NULL,    
 @par_RegistrosPagina INT = NULL    
)    
AS    
BEGIN    
 SET NOCOUNT ON;    
  DECLARE    
   @CantidadRegistros INT    
  SELECT @CantidadRegistros = (    
    SELECT DISTINCT     
     COUNT(1)     
     FROM     
     Plan_Unico_Cuentas PLUC,    
     Valor_Catalogos CCPU    
    
     WHERE    
    
     PLUC.EMPR_Codigo = @par_EMPR_Codigo    
     AND PLUC.EMPR_Codigo = CCPU.EMPR_Codigo    
     AND PLUC.CATA_CCPU_Codigo = CCPU.Codigo    
          AND PLUC.Codigo > 0    
     AND PLUC.Codigo = ISNULL(@par_Codigo, PLUC.Codigo)    
     AND PLUC.Estado = ISNULL(@par_Estado, PLUC.Estado)    
     AND ((PLUC.Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))    
     AND ((PLUC.Codigo_Alterno LIKE '%' + @par_Codigo_Alterno + '%') OR (@par_Codigo_Alterno IS NULL))    
     AND ((PLUC.Codigo_Cuenta LIKE '%' + @par_Codigo_Cuenta + '%') OR (@par_Codigo_Cuenta IS NULL))    
    
     );    
                
    WITH Pagina AS    
    (    
    
     SELECT     
     PLUC.EMPR_Codigo,    
     PLUC.Codigo,    
     PLUC.CATA_CCPU_Codigo,    
     PLUC.Codigo_Cuenta,    
     PLUC.Codigo_Alterno,    
     PLUC.Nombre,    
     PLUC.Exige_Centro_Costo,    
     PLUC.Exige_Tercero,    
     PLUC.Exige_Valor_Base,    
     PLUC.Valor_Base,    
     PLUC.Exige_Documento_Cruce,    
     PLUC.Estado,    
     CCPU.Campo1 AS ClasePlanUnicoCuenta,    
     CONCAT(PLUC.Codigo_Cuenta,' - ',PLUC.Nombre) AS CodigoNombreCuenta,  
     ROW_NUMBER() OVER(ORDER BY PLUC.Nombre) AS RowNumber    
     FROM     
     Plan_Unico_Cuentas PLUC,    
     Valor_Catalogos CCPU    
    
     WHERE    
    
     PLUC.EMPR_Codigo = @par_EMPR_Codigo    
     AND PLUC.EMPR_Codigo = CCPU.EMPR_Codigo    
     AND PLUC.CATA_CCPU_Codigo = CCPU.Codigo    
     AND PLUC.Codigo > 0    
     AND PLUC.Codigo = ISNULL(@par_Codigo, PLUC.Codigo)    
     AND PLUC.Estado = ISNULL(@par_Estado, PLUC.Estado)    
     AND ((PLUC.Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))    
     AND ((PLUC.Codigo_Alterno LIKE '%' + @par_Codigo_Alterno + '%') OR (@par_Codigo_Alterno IS NULL))    
     AND ((PLUC.Codigo_Cuenta LIKE '%' + @par_Codigo_Cuenta + '%') OR (@par_Codigo_Cuenta IS NULL))    
  )    
  SELECT DISTINCT    
  0 AS Obtener,    
  EMPR_Codigo,    
  Codigo,    
  CATA_CCPU_Codigo,    
  Codigo_Cuenta,    
  Codigo_Alterno,    
  Nombre,    
  Exige_Centro_Costo,    
  Exige_Tercero,    
  Exige_Valor_Base,    
  Valor_Base,    
  Exige_Documento_Cruce,    
  Estado,    
  ClasePlanUnicoCuenta,    
  CodigoNombreCuenta,  
  @CantidadRegistros AS TotalRegistros,    
  @par_NumeroPagina AS PaginaObtener,    
  @par_RegistrosPagina AS RegistrosPagina    
  FROM    
   Pagina    
  WHERE    
   RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)    
   AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)    
  ORDER BY Nombre    
END      
GO