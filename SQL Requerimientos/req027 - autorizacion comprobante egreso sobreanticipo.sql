USE [ENCOEXPRES]
GO

/****** Object:  Table [dbo].[Encabezado_Autorizaciones]    Script Date: 14/02/2022 5:24:53 p.�m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER TABLE [dbo].[Encabezado_Autorizaciones]
ALTER COLUMN [Observaciones] VARCHAR(300) NULL
GO

ALTER TABLE [dbo].[Encabezado_Autorizaciones]
ALTER COLUMN [Observaciones_Cuenta] VARCHAR(300) NULL
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_insertar_encabezado_autorizaciones]        
(                    
 @par_EMPR_Codigo smallint,                    
 @par_ENES_Numero numeric = null,                    
 @par_ENPD_Numero numeric = null,                    
 @par_Numero_Autorizaci�n numeric = null,                    
 @par_CATA_TSAU_Codigo numeric,                    
 @par_CATA_ESAU_Codigo numeric = null,                    
 @par_VEHI_Codigo numeric  = null,                    
 @par_TIDO_Codigo_Cuenta numeric = null,                    
 @par_TERC_Codigo numeric = null,                    
 @par_TIDO_Codigo_Documento_Origen numeric = null,                    
 @par_Valor money = null,                    
 @par_Fecha_Cancelacion_Pago datetime = null,                    
 @par_OFIC_Codigo numeric = null,                    
 @par_Codigo_Documento_Origen numeric = null,                    
 @par_Numero_Documento_Origen numeric = null,                    
 @par_Observaciones_Cuenta varchar(300) = null,                    
 @par_Observaciones varchar(300) = null,                    
 @par_ENDE_ID NUMERIC = null,                    
 @par_ENDC_Numero NUMERIC = null,                    
 @par_USUA_Codigo_Solicita numeric,          
 @par_Valor_Flete MONEY = NULL,          
 @par_valor_Flete_Autorizacion MONEY = NULL,      
 @par_Valor_Declarado_Autorizacion MONEY = NULL      
)                    
AS                    
BEGIN                    
 DECLARE @Numero int                    
 INSERT INTO Encabezado_Autorizaciones        
 (                    
 EMPR_Codigo,                    
 ENES_Numero,                    
 ENPD_Numero,                    
 Numero_Autorizaci�n,                    
 CATA_TSAU_Codigo,                    
 CATA_ESAU_Codigo,                    
 VEHI_Codigo,                    
 TIDO_Codigo_Cuenta,                    
 TERC_Codigo,                    
 TIDO_Codigo_Documento_Origen,                    
 Valor,                    
 Fecha_Cancelacion_Pago,                    
 OFIC_Codigo,                    
 Codigo_Documento_Origen,                    
 Numero_Documento_Origen,                    
 Observaciones_Cuenta,                    
 Observaciones,                    
 USUA_Codigo_Solicita,                    
 Fecha_Solicita,                    
 USUA_Codigo_Gestiona,                    
 Fecha_Gestiona,              
 ENDE_ID,            
 ENDC_Numero,          
 Valor_Flete,          
 valor_Flete_Autorizacion    ,      
 Valor_Declarado_Autorizacion      
 )                    
 VALUES                     
 (                    
 @par_EMPR_Codigo,                    
 @par_ENES_Numero,                    
 @par_ENPD_Numero,                    
 @par_Numero_Autorizaci�n,                    
 @par_CATA_TSAU_Codigo,                    
 ISNULL(@par_CATA_ESAU_Codigo,18301),                    
 @par_VEHI_Codigo,                    
 @par_TIDO_Codigo_Cuenta,                    
 @par_TERC_Codigo,                    
 @par_TIDO_Codigo_Documento_Origen,                    
 @par_Valor,                    
 @par_Fecha_Cancelacion_Pago,                    
 @par_OFIC_Codigo,                    
 @par_Codigo_Documento_Origen,                    
 @par_Numero_Documento_Origen,                    
 @par_Observaciones_Cuenta,                    
 @par_Observaciones,                    
 @par_USUA_Codigo_Solicita,                    
 GETDATE(),                    
 null,                    
 null,              
 @par_ENDE_ID ,            
 @par_ENDC_NumerO ,          
 @par_Valor_Flete,          
 @par_valor_Flete_Autorizacion ,         
 @par_Valor_Declarado_Autorizacion      
 )                    
                    
 SELECT @Numero = @@IDENTITY       
     
 DECLARE @TipoNotificacion NUMERIC(18,0) = NULL    
SET @TipoNotificacion = CASE     
 WHEN @par_CATA_TSAU_Codigo = 18402 --Anticipo Superior Autorizado     
 THEN 18501    
WHEN @par_CATA_TSAU_Codigo = 18404 -- Rechazo Veh�culo Enturnado    
THEN 18503    
WHEN @par_CATA_TSAU_Codigo = 18407 -- Autorizar Mayor Valor Declarado    
THEN 18502    
WHEN @par_CATA_TSAU_Codigo = 18408 -- No cumplimiento Inspecci�n   
THEN 18504   
END     
                   
 INSERT INTO Bandeja_Notificaciones        
 (                    
 EMPR_Codigo,                    
 USUA_Codigo_Notificacion,                    
 CATA_PRNO_Codigo,                    
 Mensaje,                    
 Numero_Origen,                    
 Fecha_Crea_Notificacion,                    
 Fecha_Ultima_Notificacion,                    
 Numero_Notificaciones                    
 )                    
 VALUES(    
@par_EMPR_Codigo,    
@par_USUA_Codigo_Solicita,    
ISNULL(@TipoNotificacion,18501),    
'Se ingreso una nueva solicitud: '+(SELECT Campo1 FROM Valor_Catalogos WHERE EMPR_Codigo = @par_EMPR_Codigo AND Codigo = @par_CATA_TSAU_Codigo),    
@Numero,                    
 GETDATE(),                    
 GETDATE(),                    
 0       
 )    
    
-- JCB: SOLO SE DEBE GUARDAR UNA VEZ LA NOTIFICACION    
-- 12-MAR-2021    
    
--SELECT DISTINCT                     
 --USGU.EMPR_Codigo,                    
 --USGU.USUA_Codigo,                    
 --ISNULL(@TipoNotificacion,18501),                    
 --'Se ingreso una nueva solicitud: '+(SELECT Campo1 FROM Valor_Catalogos WHERE Codigo = @par_CATA_TSAU_Codigo AND EMPR_Codigo = @par_EMPR_Codigo) ,                    
 --@Numero,                    
 --GETDATE(),                    
 --GETDATE(),                    
 --0                    
 --from                     
        
 --Usuario_Grupo_Usuarios AS USGU                     
                    
 --INNER JOIN Grupo_Usuarios AS GRUS                    
 --ON USGU.EMPR_Codigo = GRUS.EMPR_Codigo                    
 --AND USGU.GRUS_Codigo = GRUS.Codigo                    
                    
 --INNER JOIN  Permiso_Grupo_Usuarios AS PEGU                    
 --ON GRUS.EMPR_Codigo = PEGU.EMPR_Codigo                    
 --AND GRUS.Codigo = PEGU.GRUS_Codigo                    
                    
 --where PEGU.EMPR_Codigo = @par_EMPR_Codigo AND PEGU.MEAP_Codigo = 500102 AND PEGU.Consultar = 1                    
              
 IF @par_ENDE_ID > 0        
       BEGIN      
                      
       UPDATE Enturnamiento_Despachos SET CATA_ESED_Codigo = 19001 WHERE ID = @par_ENDE_ID AND EMPR_Codigo = @par_EMPR_Codigo        
       DECLARE @OficinaEnturne NUMERIC = 0        
       SELECT @OficinaEnturne = OFIC_Codigo_Enturna FROM Enturnamiento_Despachos WHERE ID = @par_ENDE_ID AND EMPR_Codigo = @par_EMPR_Codigo        
        
       EXEC gsp_asignar_turno_enturnamiento_despachos @par_EMPR_Codigo, NULL, @OficinaEnturne        
                   
       END        
SELECT @Numero as Codigo                    
    
END        
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_modificar_encabezado_solicitud_autorizaciones_operacion]
(                                                    
	@par_EMPR_Codigo smallint,                                                    
	@par_Numero NUMERIC,                                                    
	@par_CATA_ESAO_Codigo NUMERIC,                                                    
	@par_Observacion_Autoriza varchar (300) = null,                                                    
	@par_USUA_Codigo_Autoriza NUMERIC,        
	@par_Valor MONEY = NULL                                             
)                                                    
AS                                                    
BEGIN                            
	UPDATE Encabezado_Autorizaciones                                                    
	SET CATA_ESAU_Codigo = @par_CATA_ESAO_Codigo,                                                    
	Observaciones = @par_Observacion_Autoriza,                                                    
	USUA_Codigo_Gestiona = @par_USUA_Codigo_Autoriza,                                                  
	Fecha_Gestiona = GETDATE()          
	WHERE                                                    
	EMPR_Codigo = @par_EMPR_Codigo                                                    
	and Codigo = @par_Numero              

	DECLARE @Id_Enturnamiento Numeric  = 0
	DECLARE @OficinaEnturne NUMERIC = 0
	DECLARE @ResulAsginaTurno NUMERIC = 0

	DECLARE @tipoSolicitud NUMERIC                                       
	DECLARE @ENPD_Numero numeric,        
	@Valor_Flete numeric,        
	@Valor_Impuestos numeric,        
	@Valor_Pagar numeric        
	SELECT               
	@tipoSolicitud = CATA_TSAU_Codigo          
	FROM Encabezado_Autorizaciones               
	WHERE               
	EMPR_Codigo = @par_EMPR_Codigo             
	and Codigo = @par_Numero             
        
	IF  @par_CATA_ESAO_Codigo = 18302
	BEGIN              
		if @tipoSolicitud = 18401 --Estudio Seguridad
		BEGIN
			UPDATE ENES set ENES.CATA_ESES_Codigo = 9702           
			from Encabezado_Autorizaciones AS ENAU           
			INNER JOIN Encabezado_Estudio_Seguridad AS ENES           
			ON ENAU.EMPR_Codigo = ENES.EMPR_Codigo          
			AND ENAU.ENES_Numero = ENES.Numero          
			WHERE               
			ENAU.EMPR_Codigo = @par_EMPR_Codigo             
			and ENAU.Codigo = @par_Numero             
		END          
		if @tipoSolicitud = 18402 --Anticipo
		BEGIN
			DECLARE @NumeroGenerado NUMERIC            
			DECLARE @CodigoOficina NUMERIC            
			DECLARE @FechaDocumento date            
          
			SELECT @CodigoOficina = ENPD.OFIC_Codigo, @FechaDocumento = ENPD.Fecha from           
			Encabezado_Autorizaciones AS ENAU           
			INNER JOIN Encabezado_Planilla_Despachos AS ENPD           
			ON ENAU.EMPR_Codigo = ENPD.EMPR_Codigo          
			AND ENAU.ENPD_Numero = ENPD.Numero          
			WHERE               
			ENAU.EMPR_Codigo = @par_EMPR_Codigo             
			and ENAU.Codigo = @par_Numero             
          
			EXEC gsp_generar_consecutivo @par_EMPR_Codigo,30,@CodigoOficina,@NumeroGenerado OUTPUT            
            
			INSERT INTO            
			Encabezado_Documento_Cuentas            
			(            
			EMPR_Codigo,            
			Numero,            
			TIDO_Codigo,            
			Codigo_Alterno,            
			Fecha,            
			TERC_Codigo,            
			CATA_DOOR_Codigo,            
			Codigo_Documento_Origen,            
			Documento_Origen,            
			Fecha_Documento_Origen,            
			Fecha_Vence_Documento_Origen,            
			Numeracion,            
			Observaciones,            
			PLUC_Codigo,            
			Valor_Total,            
			Abono,            
			Saldo,            
			Fecha_Cancelacion_Pago,            
			Aprobado,            
			USUA_Codigo_Aprobo,            
			Fecha_Aprobo,            
			OFIC_Codigo,            
			VEHI_Codigo,            
			Anulado,            
			Estado,     
			USUA_Codigo_Crea,            
			Fecha_Crea            
			)            
			select           
			@par_EMPR_Codigo,            
			@NumeroGenerado,            
			TIDO_Codigo_Cuenta,            
			0,            
			GETDATE(),            
			TERC_Codigo,            
			TIDO_Codigo_Documento_Origen,            
			Codigo_Documento_Origen,            
			Numero_Documento_Origen,            
			@FechaDocumento,            
			@FechaDocumento,            
			0,            
			Observaciones_Cuenta,            
			NULL,            
			ISNULL(@par_Valor,Valor),            
			0,            
			ISNULL(@par_Valor,Valor),            
			Fecha_Cancelacion_Pago,            
			0,            
			NULL,            
			NULL,            
			@CodigoOficina,            
			VEHI_Codigo,            
			0,--Anulado            
			1,--Estado            
			USUA_Codigo_Solicita,            
			GETDATE()            
                
			FROM Encabezado_Autorizaciones          
			WHERE               
			EMPR_Codigo = @par_EMPR_Codigo             
			and Codigo = @par_Numero             

			SELECT         
			@ENPD_Numero = ENAU.ENPD_Numero,        
			@Valor_Flete = ENPD.Valor_Flete_Transportador,        
			@Valor_Impuestos = (ENPD.Valor_Flete_Transportador-ENPD.Valor_Anticipo-ENPD.Valor_Pagar_Transportador)        
			FROM         
			Encabezado_Autorizaciones as ENAU         
			INNER JOIN Encabezado_Planilla_Despachos AS ENPD ON          
			ENAU.EMPR_Codigo = ENPD.EMPR_Codigo         
			AND ENAU.ENPD_Numero = ENPD.Numero         
			WHERE ENAU.EMPR_Codigo = @par_EMPR_Codigo and Codigo = @par_Numero        
        
			SET @Valor_Pagar = @Valor_Flete-@Valor_Impuestos-@par_Valor        
			UPDATE Encabezado_Planilla_Despachos SET Valor_Anticipo = @par_Valor, Valor_Pagar_Transportador = @Valor_Pagar WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero = @ENPD_Numero        
			UPDATE Encabezado_Manifiesto_Carga SET Estado = 1 ,Valor_Anticipo = @par_Valor, Valor_Pagar = @Valor_Pagar WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENPD_Numero = @ENPD_Numero        
		END          
		if @tipoSolicitud = 18404 --Reestablece Enturnamiento
		BEGIN
			UPDATE ENDE set ENDE.CATA_ESED_Codigo = 19002           
			from Encabezado_Autorizaciones AS ENAU           
			INNER JOIN Enturnamiento_Despachos AS ENDE           
			ON ENAU.EMPR_Codigo = ENDE.EMPR_Codigo          
			AND ENAU.ENDE_ID = ENDE.ID          
			WHERE               
			ENAU.EMPR_Codigo = @par_EMPR_Codigo             
			and ENAU.Codigo = @par_Numero

			--- Genera Nuevos Turnos
			SELECT @Id_Enturnamiento = ENDE_ID from Encabezado_Autorizaciones 
			where EMPR_Codigo = @par_EMPR_Codigo
			AND Codigo = @par_Numero

			SELECT @OficinaEnturne = OFIC_Codigo_Enturna FROM Enturnamiento_Despachos WHERE EMPR_Codigo = @par_EMPR_Codigo AND ID = @Id_Enturnamiento
	
			EXEC gsp_asignar_turno_enturnamiento_despachos @par_EMPR_Codigo, NULL, @OficinaEnturne
			--- Genera Nuevos Turnos
		END
		if @tipoSolicitud = 18405 --Sobreanticipo
		BEGIN
			UPDATE ENDC set ENDC.Estado = 1         
			from Encabezado_Autorizaciones AS ENAU           
			INNER JOIN Encabezado_Documento_Comprobantes AS ENDC           
			ON ENAU.EMPR_Codigo = ENDC.EMPR_Codigo          
			AND ENAU.ENDC_Numero = ENDC.Codigo          
			WHERE               
			ENAU.EMPR_Codigo = @par_EMPR_Codigo             
			and ENAU.Codigo = @par_Numero  
			------------sumar valor de sobreanticipo planilla-----------------------
			UPDATE ENPD SET Valor_Anticipo = Valor_Anticipo + ENDC.Valor_Pago_Recaudo_Total
			from Encabezado_Autorizaciones AS ENAU           
			INNER JOIN Encabezado_Documento_Comprobantes AS ENDC   
			ON ENAU.EMPR_Codigo = ENDC.EMPR_Codigo          
			AND ENAU.ENDC_Numero = ENDC.Codigo      
			INNER JOIN Encabezado_Planilla_Despachos AS ENPD
			ON ENPD.EMPR_Codigo = ENAU.EMPR_Codigo
			AND ENPD.Numero_Documento = ENDC.Documento_Origen
			AND ENPD.TIDO_Codigo = CASE WHEN ENDC.CATA_DOOR_Codigo = 2605 THEN 150 ELSE 135 END --Planilla Masivo(150) o planilla paqueteria(135)
			WHERE               
			ENAU.EMPR_Codigo = @par_EMPR_Codigo             
			AND ENAU.Codigo = @par_Numero  

		END
		if @tipoSolicitud = 18406 --Cambio Flete          
		BEGIN
			UPDATE ENPD set ENPD.Valor_Flete_Transportador = ENAU.valor_Flete_Autorizacion        
			from Encabezado_Autorizaciones AS ENAU           
			INNER JOIN Encabezado_Planilla_Despachos AS ENPD           
			ON ENAU.EMPR_Codigo = ENPD.EMPR_Codigo          
			AND ENAU.ENPD_Numero = ENPD.Numero          
			WHERE               
			ENAU.EMPR_Codigo = @par_EMPR_Codigo             
			and ENAU.Codigo = @par_Numero     
      
			UPDATE ENMC set ENMC.Valor_Flete = ENAU.valor_Flete_Autorizacion        
			from Encabezado_Autorizaciones AS ENAU           
			INNER JOIN Encabezado_Manifiesto_Carga AS ENMC           
			ON ENAU.EMPR_Codigo = ENMC.EMPR_Codigo          
			AND ENAU.ENPD_Numero = ENMC.ENPD_Numero          
			WHERE               
			ENAU.EMPR_Codigo = @par_EMPR_Codigo             
			and ENAU.Codigo = @par_Numero     
			select @par_Numero as Numero  
		END      
	END
	ELSE         
	BEGIN        
		if @tipoSolicitud = 18402 --Anticipo
		BEGIN
			SELECT         
			@ENPD_Numero = ENAU.ENPD_Numero,        
			@Valor_Flete = ENPD.Valor_Flete_Transportador,        
			@Valor_Impuestos = (ENPD.Valor_Flete_Transportador-ENPD.Valor_Anticipo-ENPD.Valor_Pagar_Transportador)        
			FROM         
			Encabezado_Autorizaciones as ENAU         
			INNER JOIN Encabezado_Planilla_Despachos AS ENPD ON          
			ENAU.EMPR_Codigo = ENPD.EMPR_Codigo         
			AND ENAU.ENPD_Numero = ENPD.Numero         
			WHERE ENAU.EMPR_Codigo = @par_EMPR_Codigo and Codigo = @par_Numero        
        
			SET @Valor_Pagar = @Valor_Flete-@Valor_Impuestos        
			UPDATE Encabezado_Planilla_Despachos SET Valor_Anticipo = 0, Valor_Pagar_Transportador = @Valor_Pagar WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero = @ENPD_Numero        
			UPDATE Encabezado_Manifiesto_Carga SET Estado = 1 ,Valor_Anticipo = 0, Valor_Pagar = @Valor_Pagar WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENPD_Numero = @ENPD_Numero        
		END          
		if @tipoSolicitud = 18404 --Anula Enturnamiento
		BEGIN
			UPDATE ENDE set ENDE.Anulado = 1, ENDE.Causa_Anula = ENAU.Observaciones           
			from Encabezado_Autorizaciones AS ENAU           
			INNER JOIN Enturnamiento_Despachos AS ENDE           
			ON ENAU.EMPR_Codigo = ENDE.EMPR_Codigo          
			AND ENAU.ENDE_ID = ENDE.ID          
			WHERE               
			ENAU.EMPR_Codigo = @par_EMPR_Codigo             
			and ENAU.Codigo = @par_Numero
			
			--- Genera Nuevos Turnos
			SELECT @Id_Enturnamiento = ENDE_ID from Encabezado_Autorizaciones 
			where EMPR_Codigo = @par_EMPR_Codigo
			AND Codigo = @par_Numero

			SELECT @OficinaEnturne = OFIC_Codigo_Enturna FROM Enturnamiento_Despachos WHERE EMPR_Codigo = @par_EMPR_Codigo AND ID = @Id_Enturnamiento
	
			EXEC gsp_asignar_turno_enturnamiento_despachos @par_EMPR_Codigo, NULL, @OficinaEnturne
			--- Genera Nuevos Turnos  
		END 
		if @tipoSolicitud = 18405 --Sobreanticipo          
		BEGIN
			UPDATE ENDC set ENDC.Estado = 1 , ENDC.Anulado = 1        
			from Encabezado_Autorizaciones AS ENAU           
			INNER JOIN Encabezado_Documento_Comprobantes AS ENDC           
			ON ENAU.EMPR_Codigo = ENDC.EMPR_Codigo          
			AND ENAU.ENDC_Numero = ENDC.Codigo          
			WHERE               
			ENAU.EMPR_Codigo = @par_EMPR_Codigo             
			and ENAU.Codigo = @par_Numero      
			DECLARE @ENDc_Numero numeric    
			select @ENDc_Numero =  ENDC_Numero from  Encabezado_Autorizaciones    
			WHERE               
			EMPR_Codigo = @par_EMPR_Codigo             
			and Codigo = @par_Numero    
			EXEC gsp_anular_movimiento_contable @par_EMPR_Codigo,30,@ENDc_Numero
		END       
	END                                     
SELECT @par_Numero as Numero                                                    
END
GO


