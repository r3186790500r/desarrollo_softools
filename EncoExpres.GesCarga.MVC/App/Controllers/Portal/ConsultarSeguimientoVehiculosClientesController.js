﻿EncoExpresApp.controller("ConsultarSeguimientoVehiculosClientesCtrl", ['$scope', 'RutasFactory', '$linq', 'DetalleSeguimientoVehiculosFactory', 'blockUIConfig', 'ValorCatalogosFactory', 'TercerosFactory', 'EventoCorreosFactory', 'DetalleDistribucionRemesasFactory', 'ProductoTransportadosFactory', 'RemesasFactory', 'VehiculosFactory', 'ReporteMinisterioFactory', 'OficinasFactory', 'CiudadesFactory', 'PuestoControlesFactory', 'PlanillaDespachosFactory',
    function ($scope, RutasFactory, $linq, DetalleSeguimientoVehiculosFactory, blockUIConfig, ValorCatalogosFactory, TercerosFactory, EventoCorreosFactory, DetalleDistribucionRemesasFactory, ProductoTransportadosFactory, RemesasFactory, VehiculosFactory, ReporteMinisterioFactory, OficinasFactory, CiudadesFactory, PuestoControlesFactory, PlanillaDespachosFactory) {

        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        // Se crea la propiedad modelo en el ambito
        $scope.NombreReporte = '';
        $scope.FiltroArmado = '';
        $scope.VerOpcionPDF = false;
        $('#TabPlanillas').show()
        $('#TabPlanillas2').show()

        //Obtiene la URL para llamar el proyecto ASP
        $scope.urlASP = '';
        $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
        $scope.DeshabilitarDetalle = false;
        $scope.DeshabilitarPuestoControl = true;
        $scope.DeshabilitarFlotaPropia = false;
        var filtros = {};
        $scope.ListadoOficinas = [];
        $scope.cantidadRegistrosPorPaginas = 20;
        $scope.paginaActual = 1;
        $scope.paginaActualOrden = 1;
        $scope.paginaActualRemesa = 1;
        $scope.paginaActualFlotaPropia = 1;
        $scope.paginaActualRNDC = 1;
        $scope.ModalErrorCompleto = '';
        $scope.ModalError = '';
        $scope.MostrarMensajeError = false;
        $scope.ListaPuntosControl = [];
        $scope.ListaSitioReporteModal = [];
        $scope.CodigoSitioReporteSeguimiento = 0;
        $scope.ListaNovedadSeguimiento = [];
        $scope.CodigoNovedadSeguimiento = 0;
        $scope.ListadoSeguimientosPlanilla = [];
        $scope.ListadoSeguimientosOrden = [];
        $scope.MensajesErrorSeguimiento = [];
        $scope.ListaCantidadSeguimientos = [];
        $scope.ListaTipoOrigenSeguimiento = [];
        $scope.ConsultaProgramacion = {};
        $scope.MensajesErrorAnular = [];
        $scope.CorreosTransportador = [];
        $scope.CorreosClientes = [];
        $scope.TipoConsulta = 1;
        $scope.ValorBanderaAnularRemesa = 0;
        $scope.ListaPuntosGestionModal = [];
        $scope.ListaProductoTransportados = [];
        $scope.ListadoSeguimientosRemesasPlanillaDespachos = [];
        $scope.Correo = {};
        $scope.Modelo = {
            Orden: 0
        };
        $scope.Remesas = {
            RegistrosPagina: 20,
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
        };
        $scope.Distribucion = {};
        $scope.Despachos = {
            Estado: false
        };
        $scope.Ordenes = {
            Estado: false
        };
        $scope.FlotaPropia = {
            Estado: false
        };
        $scope.RNDC = {};

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CONSULTAS_SEGUIMIENTO_VEHICULAR_TRANSIO_PORTAL);
            $scope.MapaSitio = [{ Nombre: 'Portal' }, { Nombre: 'Control Trafico' }, { Nombre: 'Procesos' }, { Nombre: 'Seguimiento Vehicular en Tránsito' }];
            $('#ConsultaOrdenesCargue').hide(); $('#ConsultaPlanillasDespachos').show(); $('#ConsultaRemesasPlanillasDespachos').hide(); $('#ConsultaFlotaPropia').hide(); $('#ConsultaRNDC').hide();
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR PLANILLA--------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };


        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR ORDEN--------------------------------------------------------------------------------- */
        $scope.PrimerPaginaOrden = function () {
            $scope.paginaActualOrden = 1;
            Find();
        };

        $scope.SiguienteOrden = function () {
            if ($scope.paginaActualOrden < $scope.totalPaginasOrden) {
                $scope.paginaActualOrden = $scope.paginaActualOrden + 1;
                Find();
            }
        };

        $scope.AnteriorOrden = function () {
            if ($scope.paginaActualOrden > 1) {
                $scope.paginaActualOrden = $scope.paginaActualOrden - 1;
                Find();
            }
        };

        $scope.UltimaPaginaOrden = function () {
            $scope.paginaActualOrden = $scope.totalPaginasOrden;
            Find();
        };

        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN REMESAS--------------------------------------------------------------------------------- */
        $scope.PrimerPaginaRemesa = function () {
            $scope.paginaActualRemesa = 1;
            FindRemesa();
        };

        $scope.SiguienteRemesa = function () {
            if ($scope.paginaActualRemesa < $scope.totalPaginasRemesa) {
                $scope.paginaActualRemesa = $scope.paginaActualRemesa + 1;
                FindRemesa();
            }
        };

        $scope.AnteriorRemesa = function () {
            if ($scope.paginaActualRemesa > 1) {
                $scope.paginaActualRemesa = $scope.paginaActualRemesa - 1;
                FindRemesa();
            }
        };

        $scope.UltimaPaginaRemesa = function () {
            $scope.paginaActualRemesa = $scope.totalPaginasRemesa;
            FindRemesa();
        };

        /*-----------------------------------------------------------------COMBOS Y AUTOCOMPLETE--------------------------------------------------------------------------------- */
        ///*Verifica oficinas */
        $scope.AsignarOficina = function (ofic) {
            if (ofic.Codigo === -1) {
                $scope.ListadoOficinas.forEach(function (item) {
                    if (item.Codigo === -1) {
                        item.Estado = true;
                    } else {
                        item.Estado = false;
                    }
                });
            } else {
                $scope.ListadoOficinas.forEach(function (item) {
                    if (item.Codigo === -1) {
                        item.Estado = false;
                    }
                });
            }
        };

        /*Verifica rAutocomplete Puesto Control */
        $scope.VerificarAutocompletePuestoControl = function (puestocontrol) {
            if (puestocontrol.Codigo === undefined) {
                $scope.Modelo.PuestoControl = '';
            }
        };

        OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1, Estado: true });
                if (response.data.ProcesoExitoso === true) {
                    response.data.Datos.forEach(function (item) {
                        if (item.Codigo === -1) {
                            item.Estado = true;
                        } else {
                            item.Estado = false;
                        }
                        $scope.ListadoOficinas.push(item);
                    });
                    /*Cargar el combo  cantidad seguimientos */
                    ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_CANTIDAD_LISTA_SEGUIMIENTOS } }).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                $scope.ListaCantidadSeguimientos = response.data.Datos;
                                $scope.Despachos.Mostrar = $scope.ListaCantidadSeguimientos[1];
                                $scope.Ordenes.Mostrar = $scope.ListaCantidadSeguimientos[1];
                                $scope.Remesas.Mostrar = $scope.ListaCantidadSeguimientos[1];
                                $scope.FlotaPropia.Mostrar = $scope.ListaCantidadSeguimientos[1];
                                if ($scope.Sesion.UsuarioAutenticado.Cliente.Codigo > 0 && $scope.Sesion.UsuarioAutenticado.Cliente.Codigo != undefined && $scope.Sesion.UsuarioAutenticado.Cliente.Codigo != '') {
                                    $scope.Despachos.Cliente = $scope.CargarTercero($scope.Sesion.UsuarioAutenticado.Cliente.Codigo)
                                    $scope.Remesas.Cliente = $scope.Despachos.Cliente
                                    $scope.DeshabilitarCliente = true;
                                    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

                                    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
                                    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
                                    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
                                    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
                                    Find();

                                } else {
                                    if ($scope.Sesion.UsuarioAutenticado.Codigo > 0) {
                                        ShowError('El usuario ingresado no tiene clientes asociados')
                                        $scope.DeshabilitarConsulta = true
                                        $scope.DeshabilitarConsulta = true
                                        $scope.DeshabilitarEliminarAnular = true
                                        $scope.DeshabilitarImprimir = true
                                        $scope.DeshabilitarActualizar = true

                                    }
                                }
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_DUEÑO_VEHICULO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoDueno = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoDueno = response.data.Datos;
                        $scope.Despachos.TipoDueno = $scope.ListadoTipoDueno[0]
                        $scope.Ordenes.TipoDueno = $scope.ListadoTipoDueno[0]
                    }
                    else {
                        $scope.ListadoTipoDueno = []
                    }
                }
            }, function (response) {
            });
        $scope.ListadoConductores = [];
        $scope.AutocompleteConductor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de Conductor*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CONDUCTOR, ValorAutocomplete: value, Sync: true })
                    $scope.ListadoConductores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoConductores)
                }
            }
            return $scope.ListadoConductores
        }
        // Vehiculo
        $scope.ListaPlaca = [];
        $scope.AutocompleteVehiculos = function (value) {
            if (value.length > 0) {
                if ((value.length % 2) == 0 || value.length == 1) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListaPlaca = ValidarListadoAutocomplete(Response.Datos, $scope.ListaPlaca);
                }
            }
            return $scope.ListaPlaca;
        };
        $scope.AutocompleteProductos = function (value) {
            $scope.ListaProductoTransportado = [];
            if (value.length > 0) {
                /*Cargar Autocomplete de propietario*/
                blockUIConfig.autoBlock = false;
                var Response = ProductoTransportadosFactory.Consultar({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    ValorAutocomplete: value,
                    AplicaTarifario: -1,
                    Sync: true
                })
                $scope.ListaProductoTransportado = ValidarListadoAutocomplete(Response.Datos, $scope.ListaProductoTransportado)
            }
            return $scope.ListaProductoTransportado
        }
        //-- Productos Transportados --//
        ProductoTransportadosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, AplicaTarifario: -1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    ProductoTranportadoCarga = true;
                    if (response.data.Datos.length > 0) {
                        $scope.ListaProductoTransportado = response.data.Datos;
                    }
                    else {
                        $scope.ListaProductoTransportado = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        //Cargar datos vehiculo
        $scope.CargarDatosVehiculo = function (vehiculo) {
            if (vehiculo !== '' && vehiculo !== null && vehiculo !== undefined) {
                $scope.CodigoVehiculo = vehiculo.Codigo;
                VehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.CodigoVehiculo }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $scope.ModalFlotaPropia = {};
                            $scope.ModalFlotaPropia = response.data.Datos;
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };
        $scope.ListadoCliente = []
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CLIENTE,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCliente)
                }
            }
            return $scope.ListadoCliente
        }
        $scope.ListadoCiudades = []
        $scope.AutocompleteCiudades = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    })
                    $scope.ListadoCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCiudades)
                }
            }
            return $scope.ListadoCiudades
        }
        /*Cargar el combo Tipo Origen */
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_ORIGEN_SEGUIMIENTO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaTipoOrigenSeguimiento = [];
                    $scope.ListaTipoOrigenSeguimiento = response.data.Datos;
                    $scope.Despachos.OrigenReporte = $scope.ListaTipoOrigenSeguimiento[0];
                    $scope.Ordenes.OrigenReporte = $scope.ListaTipoOrigenSeguimiento[0];
                    $scope.Remesas.TipoOrigenSeguimiento = $scope.ListaTipoOrigenSeguimiento[0];
                    $scope.FlotaPropia.OrigenReporte = $scope.ListaTipoOrigenSeguimiento[0];
                    //Find();
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar el combo sitio reporte estado*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_SITIO_REPORTE_SEGUIMIENTO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaSitioReporte = [];
                    $scope.ListaSitioReporteModal = [];
                    $scope.ListaSitioReporte = response.data.Datos;
                    response.data.Datos.forEach(function (item) {
                        if (item.Codigo !== 8201) {
                            $scope.ListaSitioReporteModal.push(item);
                        }
                    });
                    $scope.Despachos.SitioReporte = $scope.ListaSitioReporte[0];
                    $scope.Ordenes.SitioReporte = $scope.ListaSitioReporte[0];
                    $scope.Remesas.SitioReporteSeguimiento = $scope.ListaSitioReporte[0];
                    $scope.FlotaPropia.SitioReporte = $scope.ListaSitioReporte[0];
                    //Find();
                    if ($scope.CodigoSitioReporteSeguimiento !== undefined && $scope.CodigoSitioReporteSeguimiento !== '' && $scope.CodigoSitioReporteSeguimiento !== null && $scope.CodigoSitioReporteSeguimiento !== 0) {
                        $scope.Modelo.SitioReporteSeguimiento = $linq.Enumerable().From($scope.ListaSitioReporteModal).First('$.Codigo ==' + $scope.CodigoSitioReporteSeguimiento);
                    } else {
                        $scope.Modelo.SitioReporteSeguimiento = $linq.Enumerable().From($scope.ListaSitioReporteModal).First('$.Codigo == 8200');
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });



        //Carga autocomplete de rutas
        RutasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaRutas = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListaRutas = response.data.Datos;
                    }
                    else {
                        $scope.ListaRutas = [];
                    }
                }
            }, function (response) {
            });
        //-- Puestos Control --//
        PuestoControlesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, AplicaTarifario: -1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListaPuestosControl = response.data.Datos;
                    }
                    else {
                        $scope.ListaPuestosControl = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIEMPOS_LOGISTICOS_REMESA } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTiemposLogisticos = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTiemposLogisticos = response.data.Datos;
                    }
                    else {
                        $scope.ListadoTiemposLogisticos = []
                    }
                }
            }, function (response) {
            });


        //-------------------------------------------------FUNCIONES BUSCAR REMESAS--------------------------------------------------------
        $scope.BuscarRemesa = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                FindRemesa();
            }
        };

        function FindRemesa() {
            $scope.MensajesError = [];
            $scope.ListadoSeguimientosRemesa = [];

            if ($scope.RemesasEstado === true) {
                $scope.Remesas.Anulado = 1;
            } else {
                $scope.Remesas.Anulado = 0;
            }

            var CodigosOficinas = '';
            $scope.ListadoOficinas.forEach(function (item) {
                if (item.Estado === true) {
                    CodigosOficinas = CodigosOficinas + item.Codigo + ',';
                }
            });

            if (CodigosOficinas === '-1,') {
                $scope.ListadoOficinas.forEach(function (item) {
                    if (item.Codigo !== -1) {
                        CodigosOficinas = CodigosOficinas + item.Codigo + ',';
                    }
                });
            }

            $scope.Remesas.CodigosOficinas = CodigosOficinas;
            $scope.Remesas.Pagina = $scope.paginaActualRemesa;
            $scope.Remesas.Consulta = 1;

            DetalleSeguimientoVehiculosFactory.ConsultarMasterRemesa($scope.Remesas).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoSeguimientosRemesa = response.data.Datos;
                            $scope.totalRegistrosRemesa = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginasRemesa = Math.ceil($scope.totalRegistrosRemesa / $scope.cantidadRegistrosPorPaginas);
                            $scope.BuscandoRemesa = false;
                            $scope.ResultadoSinRegistrosRemesa = '';
                        }
                        else {
                            $scope.totalRegistrosRemesa = 0;
                            $scope.totalPaginasRemesa = 0;
                            $scope.paginaActualRemesa = 1;
                            $scope.ResultadoSinRegistrosRemesa = 'No hay datos para mostrar';
                            $scope.BuscandoRemesa = false;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }

        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.CargarNovedadesSitioReporte = function (sitioReporte) {
            $scope.ListaNovedadSeguimiento = [];
            $scope.Modelo.NovedadSeguimiento = '';
            if (sitioReporte.Codigo !== undefined && sitioReporte.Codigo !== 8200) {
                var filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    SitioReporteSeguimiento: sitioReporte
                };
                DetalleSeguimientoVehiculosFactory.ConsultarNovedadesSitiosReporte(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListaNovedadSeguimiento = response.data.Datos;
                                $scope.DeshabilitarNovedadesSeguimiento = false;
                            } else {
                                $scope.DeshabilitarNovedadesSeguimiento = true;
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            } else {
                $scope.DeshabilitarNovedadesSeguimiento = true;
            }
        };

        $scope.CargarPuestoControlRutas = function (ruta) {
            $scope.ListaPuntosControl = [];
            $scope.Modelo.PuestoControl = '';
            if (ruta !== undefined && ruta !== null && ruta !== '') {
                var filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Ruta: ruta
                };
                DetalleSeguimientoVehiculosFactory.ConsultarPuestoControlRutas(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListaPuntosControl = response.data.Datos;
                                if ($scope.CodigoPuestoControl !== undefined && $scope.CodigoPuestoControl !== '' && $scope.CodigoPuestoControl !== null && $scope.CodigoPuestoControl !== 0) {
                                    if (!$scope.DeshabilitarDetalle) {
                                        $scope.Modelo.PuestoControl = $linq.Enumerable().From($scope.ListaPuntosControl).First('$.PuestoControl.Codigo ==' + $scope.CodigoPuestoControl);
                                    } else {
                                        $scope.Modelo.PuestoControl = '';
                                    }
                                } else {
                                    $scope.Modelo.PuestoControl = '';
                                }
                                $scope.DeshabilitarPuestoControl = false;
                            } else {
                                if (!$scope.DeshabilitarDetalle) {
                                    $scope.DeshabilitarPuestoControl = true;
                                    ShowError('La ruta no tiene puntos de control asociados');
                                }
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            } else {
                $scope.DeshabilitarPuestoControl = true;
            }
        };

        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                Find();
            }
        };

        function Find() {
            $scope.ListadoSeguimientosPlanilla = [];
            $scope.ListadoSeguimientosOrden = [];
            $scope.MensajesError = [];
            if ($scope.MensajesError.length == 0) {
                filtros = {};

                var anuladodespachos = 0;
                if ($scope.Despachos.Estado == true) {
                    anuladodespachos = 1;
                } else {
                    anuladodespachos = 0;
                }
                var anuladoorden = 0;
                if ($scope.Ordenes.Estado == true) {
                    anuladoorden = 1;
                } else {
                    anuladoorden = 0;
                }

                var CodigosOficinas = '';
                $scope.ListadoOficinas.forEach(function (item) {
                    if (item.Estado === true) {
                        CodigosOficinas = CodigosOficinas + item.Codigo + ',';
                    }
                });

                if (CodigosOficinas === '-1,') {
                    $scope.ListadoOficinas.forEach(function (item) {
                        if (item.Codigo !== -1) {
                            CodigosOficinas = CodigosOficinas + item.Codigo + ',';
                        }
                    });
                }


                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Pagina: $scope.paginaActual,
                    RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                    TipoConsulta: $scope.TipoConsulta,
                    Vehiculo: { Placa: $scope.Despachos.Vehiculo },
                    Conductor: $scope.Despachos.Conductor,
                    Cliente: $scope.Despachos.Cliente,
                    TipoDueno: $scope.Despachos.TipoDueno,
                    Producto: $scope.Despachos.Producto,
                    Origen: $scope.Despachos.Origen,
                    Destino: $scope.Despachos.Destino,
                    Ruta: $scope.Despachos.Ruta,
                    PuestoControl: $scope.Despachos.PuestoControl,
                    NumeroDocumentoPlanilla: $scope.Despachos.NumeroPlanilla,
                    NumeroManifiesto: $scope.Despachos.NumeroManifiesto,
                    SitioReporteSeguimiento: $scope.Despachos.SitioReporte,
                    TipoOrigenSeguimiento: $scope.Despachos.OrigenReporte,
                    Mostrar: $scope.Despachos.Mostrar,
                    Anulado: anuladodespachos,
                    CodigosOficinas: CodigosOficinas,
                    Listado: 1,
                    ReportarCliente: 1
                };

                DetalleSeguimientoVehiculosFactory.Consultar(filtros).
                    then(function (response) {

                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoSeguimientosPlanilla = [];
                                $scope.ListadoSeguimientosOrden = [];

                                if ($scope.TipoConsulta == 0) {
                                    var concidencias = 0
                                    if (filtros.SitioReporteSeguimiento.Codigo !== CODIGO_ESTADO_REPORTE_INICIAL) {
                                        concidencias++
                                    }
                                    if (filtros.TipoOrigenSeguimiento.Codigo !== $scope.ListaTipoOrigenSeguimiento[0].Codigo) {
                                        concidencias++
                                    }
                                    if (filtros.PuestoControl !== undefined) {
                                        if (filtros.PuestoControl.Codigo !== undefined) {
                                            concidencias++
                                        }
                                    }
                                    if (filtros.Vehiculo.Placa !== '' && filtros.Vehiculo.Placa !== undefined) {
                                        concidencias++
                                    }
                                    response.data.Datos.forEach(function (item) {
                                        var con = 0
                                        if (filtros.SitioReporteSeguimiento.Codigo == item.SitioReporteSeguimiento.Codigo && filtros.SitioReporteSeguimiento.Codigo !== CODIGO_ESTADO_REPORTE_INICIAL) {
                                            con++
                                        }
                                        if (filtros.TipoOrigenSeguimiento.Codigo == item.TipoOrigenSeguimiento.Codigo && filtros.TipoOrigenSeguimiento.Codigo !== $scope.ListaTipoOrigenSeguimiento[0].Codigo) {
                                            con++
                                        }
                                        if (filtros.PuestoControl !== undefined) {
                                            if (filtros.PuestoControl.Codigo == item.PuestoControl.Codigo && filtros.PuestoControl.Codigo !== undefined) {
                                                con++
                                            }
                                        }
                                        if (filtros.Vehiculo.Placa == item.Vehiculo.Placa && filtros.Vehiculo.Placa !== '' && filtros.Vehiculo.Placa !== undefined) {
                                            con++
                                        }
                                        if (con == concidencias) {
                                            $scope.ListadoSeguimientosOrden.push(item)
                                        }
                                        if (item.Observaciones !== '') {
                                            var str = item.Observaciones;
                                            item.Observaciones = str.substring(0, 10);
                                            item.ObservacionCompleta = str.substring(0, 250);
                                            item.OcultarVerMas = true;
                                        } else {
                                            item.OcultarVerMas = false;
                                        }

                                    })
                                    if ($scope.ListadoSeguimientosOrden.length == 0) {
                                        $scope.totalRegistrosOrden = 0;
                                        $scope.totalPaginasOrden = 0;
                                        $scope.paginaActualOrden = 1;
                                        $scope.ResultadoSinRegistrosOrden = 'No hay datos para mostrar';
                                        $scope.BuscandoOrden = false;

                                    } else {
                                        $scope.totalRegistrosOrden = $scope.ListadoSeguimientosOrden[0].TotalRegistros;
                                        $scope.totalPaginasOrden = Math.ceil($scope.totalRegistrosOrden / $scope.cantidadRegistrosPorPaginas);
                                        $scope.BuscandoOrden = false;
                                        $scope.ResultadoSinRegistrosOrden = '';
                                    }

                                } else if ($scope.TipoConsulta == 1) {
                                    var concidencias = 0
                                    if (filtros.SitioReporteSeguimiento.Codigo !== CODIGO_ESTADO_REPORTE_INICIAL) {
                                        concidencias++
                                    }
                                    if (filtros.TipoOrigenSeguimiento.Codigo !== $scope.ListaTipoOrigenSeguimiento[0].Codigo) {
                                        concidencias++
                                    }
                                    if (filtros.PuestoControl !== undefined) {
                                        if (filtros.PuestoControl.Codigo !== undefined) {
                                            concidencias++
                                        }
                                    }
                                    if (filtros.Vehiculo.Placa !== '' && filtros.Vehiculo.Placa !== undefined) {
                                        concidencias++
                                    }
                                    response.data.Datos.forEach(function (item) {
                                        var con = 0
                                        if (filtros.SitioReporteSeguimiento.Codigo == item.SitioReporteSeguimiento.Codigo && filtros.SitioReporteSeguimiento.Codigo !== CODIGO_ESTADO_REPORTE_INICIAL) {
                                            con++
                                        }
                                        if (filtros.TipoOrigenSeguimiento.Codigo == item.TipoOrigenSeguimiento.Codigo && filtros.TipoOrigenSeguimiento.Codigo !== $scope.ListaTipoOrigenSeguimiento[0].Codigo) {
                                            con++
                                        }
                                        if (filtros.PuestoControl !== undefined) {
                                            if (filtros.PuestoControl.Codigo == item.PuestoControl.Codigo && filtros.PuestoControl.Codigo !== undefined) {
                                                con++
                                            }
                                        }
                                        if (filtros.Vehiculo.Placa == item.Vehiculo.Placa && filtros.Vehiculo.Placa !== '' && filtros.Vehiculo.Placa !== undefined) {
                                            con++
                                        }
                                        if (con == concidencias) {
                                            $scope.ListadoSeguimientosPlanilla.push(item)
                                        }
                                        if (item.Observaciones !== '') {
                                            var str = item.Observaciones;
                                            item.Observaciones = str.substring(0, 10);
                                            item.ObservacionCompleta = str.substring(0, 250);
                                            item.OcultarVerMas = true;
                                        } else {
                                            item.OcultarVerMas = false;
                                        }

                                    })
                                    if ($scope.ListadoSeguimientosPlanilla.length == 0) {
                                        $scope.totalRegistros = 0;
                                        $scope.totalPaginas = 0;
                                        $scope.paginaActual = 1;
                                        $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                        $scope.Buscando = false;

                                    } else {
                                        $scope.totalRegistros = $scope.ListadoSeguimientosPlanilla[0].TotalRegistros;
                                        $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                        $scope.Buscando = false;
                                        $scope.ResultadoSinRegistros = '';
                                    }
                                }
                            } else {
                                if ($scope.TipoConsulta == 1) {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                } else {
                                    $scope.totalRegistrosOrden = 0;
                                    $scope.totalPaginasOrden = 0;
                                    $scope.paginaActualOrden = 1;
                                    $scope.ResultadoSinRegistrosOrden = 'No hay datos para mostrar';
                                    $scope.BuscandoOrden = false;
                                }
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }


        //--------------funciones de mascaras html-angular------------------------------------------------------------------------------------------------------------------
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskPlaca = function () {
            MascaraPlacaGeneral($scope)
        };

        //inicializacion de divisiones
        $scope.MostrarConsultaPlanillasDespachos = function () {
            $('#ConsultaOrdenesCargue').hide();
            $('#ConsultaPlanillasDespachos').show();
            $('#ConsultaRemesasPlanillasDespachos').hide();
            $('#ConsultaFlotaPropia').hide();
            $('#ConsultaRNDC').hide();
            $scope.TipoConsulta = 1; // Planillas despachos
            Find();
        };
        $scope.MostrarConsultaRemesasPlanillasDespachos = function () {
            $('#ConsultaOrdenesCargue').hide();
            $('#ConsultaPlanillasDespachos').hide();
            $('#ConsultaRemesasPlanillasDespachos').show();
            $('#ConsultaFlotaPropia').hide();
            $('#ConsultaRNDC').hide();
            FindRemesa();
        };

        console.clear()
        $scope.ConsultarDetaleSeguimientos = function (Planilla) {
            showModal('modaldetalleSeguimientoPlanilla')
            $scope.ListadoDetalleSeguimientosPlanilla = [];
            $scope.ListadoDetalleSeguimientosPlanillaGPS = [];
            $scope.iniciamapa = true
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoConsulta: 1,
                NumeroDocumentoPlanilla: Planilla,
                Anulado: 0
            }
            DetalleSeguimientoVehiculosFactory.Consultar(filtros).
                then(function (response) {

                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoDetalleSeguimientosPlanilla = response.data.Datos
                            filtros.ConsultaGPS = 1
                            BloqueoPantalla.start('Consultando reportes GPS...');
                            DetalleSeguimientoVehiculosFactory.Consultar(filtros).
                                then(function (response) {
                                    BloqueoPantalla.stop();
                                    if (response.data.ProcesoExitoso === true) {
                                        if (response.data.Datos.length > 0) {
                                            $scope.ListadoDetalleSeguimientosPlanillaGPS = response.data.Datos
                                            var marker = [];
                                            var map, infoWindow;
                                            function initMap(posicion) {
                                                if (map === void 0) {
                                                    var mapOptions = {
                                                        zoom: 15,
                                                        center: new window.google.maps.LatLng(posicion.Latitud, posicion.Longitud),
                                                        disableDefaultUI: true,
                                                        mapTypeId: window.google.maps.MapTypeId.ROADMAP
                                                    }
                                                    //Asignacion al id gmapsplanilla que se encuentra en el html
                                                    map = new window.google.maps.Map(document.getElementById('gmapsplanillaGPS'), mapOptions);
                                                }
                                            }

                                            function setMarker(map, position, title, content) {
                                                var markers;
                                                var markerOptions = {
                                                    position: position,
                                                    map: map,
                                                    //draggable opcion que permite mover el marcador
                                                    draggable: false,
                                                    //bounce le da animacion al marcador
                                                    //animation: google.maps.Animation.BOUNCE,
                                                    title: title,
                                                };

                                                markers = new window.google.maps.Marker(markerOptions);
                                                //dragend permite capturar las coordenadas seleccionadas en el mapa
                                                markers.addListener('dragend', function (event) {
                                                    //escribimos las coordenadas de la posicion actual del marcador dentro los input en el html 
                                                    $scope.Modelo.Latitud = this.getPosition().lat();
                                                    $scope.Modelo.Longitud = this.getPosition().lng();
                                                });
                                                //Variable donde se asignan los datos del marcador
                                                marker.push(markers);

                                                //var infoWindowOptions = {
                                                //    content: content
                                                //};
                                                //infoWindow = new window.google.maps.InfoWindow(infoWindowOptions);
                                                //infoWindow.open(map, markers);

                                            }
                                            for (var i = 0; i < $scope.ListadoDetalleSeguimientosPlanillaGPS.length; i++) {
                                                var posicion = $scope.ListadoDetalleSeguimientosPlanillaGPS[i]
                                                if (posicion.Longitud !== 0 && posicion.Latitud !== 0) {
                                                    if ($scope.iniciamapa) {
                                                        initMap(posicion)
                                                        $scope.iniciamapa = false
                                                    }
                                                    setMarker(map, new window.google.maps.LatLng(posicion.Latitud, posicion.Longitud), '', '');

                                                }
                                            }
                                            for (var i = 0; i < $scope.ListadoDetalleSeguimientosPlanilla.length; i++) {
                                                var posicion = $scope.ListadoDetalleSeguimientosPlanilla[i]
                                                if (posicion.Longitud !== 0 && posicion.Latitud !== 0) {
                                                    if ($scope.iniciamapa) {
                                                        initMap(posicion)
                                                        $scope.iniciamapa = false
                                                    }
                                                    setMarker(map, new window.google.maps.LatLng(posicion.Latitud, posicion.Longitud), '', '');
                                                }
                                            }
                                        }
                                    }
                                }, function (response) {
                                    ShowError(response.statusText);
                                    BloqueoPantalla.stop();
                                });
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

        }
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_SITIO_REPORTE_SEGUIMIENDO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoSitioReporteSeguimientos = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoSitioReporteSeguimientos = response.data.Datos;
                    }
                    else {
                        $scope.ListadoSitioReporteSeguimientos = []
                    }
                }
            }, function (response) {
            });
        $scope.ShowList = function (item) {
            if (item.show == true) {
                item.show = false
            } else {
                item.show = true
            }
        }

        $scope.VerSeguimiento = function (item) {
            $scope.VariableDetalle = 'DETALLE'
            $scope.MensajesErrorSeguimiento = [];
            $scope.CodigoNovedadSeguimiento = 0;
            $scope.ListadoDetalleSeguimientosPlanilla = []

            showModal('modalnuevoseguimientoplanilla');
            $scope.ModalDespachos = {}
            DetalleSeguimientoVehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: item.Codigo, TipoConsulta: item.TipoConsulta }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.Codigo > 0) {
                            $scope.ModalDespachos = response.data.Datos;
                            $scope.CargarPuestoControlRutas(response.data.Datos.Ruta);
                            $scope.Modelo.Orden = response.data.Datos.Orden;
                            ConsultarRemesas();
                            if ($scope.ModalDespachos.ReportarCliente === 1) {
                                $scope.Reportar = true;
                            } else {
                                $scope.Reportar = false;
                            }
                            if ($scope.ModalDespachos.Prioritario === 1) {
                                $scope.Prioritario = true;
                            } else {
                                $scope.Prioritario = false;
                            }
                            $scope.DeshabilitarDetalle = true;
                            $scope.iniciarMapaPlanilla($scope.ModalDespachos.Latitud, $scope.ModalDespachos.Longitud)
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoConsulta: 1,
                NumeroDocumentoPlanilla: item.NumeroDocumentoPlanilla,
                Anulado: 0
            }
            DetalleSeguimientoVehiculosFactory.Consultar(filtros).
                then(function (response) {

                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoDetalleSeguimientosPlanilla = response.data.Datos

                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

            $scope.Modelo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
            $scope.Modelo.NumeroDocumentoPlanilla = item.NumeroDocumentoPlanilla;
            $scope.Modelo.NumeroDocumentoOrden = item.NumeroDocumentoOrden;
            $scope.Modelo.NumeroPlanilla = item.NumeroPlanilla;
            $scope.Modelo.NumeroOrden = item.NumeroOrden;
            $scope.Modelo.NumeroManifiesto = item.NumeroManifiesto;
            $scope.Modelo.TipoOrigenSeguimiento = { Codigo: CODIGO_TIPO_ORIGEN_SEGUIMIENTO_MANUAL };

            $scope.CodigoSitioReporteSeguimiento = item.SitioReporteSeguimiento.Codigo;
            if ($scope.ListaSitioReporteModal.length > 0 && $scope.CodigoSitioReporteSeguimiento > 0) {
                $scope.Modelo.SitioReporteSeguimiento = $linq.Enumerable().From($scope.ListaSitioReporteModal).First('$.Codigo == ' + item.SitioReporteSeguimiento.Codigo);
            }

            $scope.CodigoNovedadSeguimiento = item.NovedadSeguimiento.Codigo;
            if ($scope.ListaNovedadSeguimiento.length > 0 && $scope.CodigoNovedadSeguimiento > 0) {
                $scope.Modelo.NovedadSeguimiento = $linq.Enumerable().From($scope.ListaNovedadSeguimiento).First('$.Codigo == ' + item.NovedadSeguimiento.Codigo);
            }

            $scope.CodigoPuestoControl = item.PuestoControl.Codigo;
            if ($scope.ListaPuntosControl.length > 0 && $scope.CodigoPuestoControl > 0) {
                $scope.Modelo.PuestoControl = $linq.Enumerable().From($scope.ListaPuntosControl).First('$.PuestoControl.Codigo == ' + item.PuestoControl.Codigo);
            }

            $scope.Modelo.Ubicacion = item.Ubicacion;
            $scope.Modelo.Observaciones = item.ObservacionCompleta;
            $scope.Modelo.Latitud = item.Latitud;
            $scope.Modelo.Longitud = item.Longitud;
            //$scope.Modelo.EnvioReporteCliente = false

        }

        /*---------------------------------------------------------------------Funciones grid remesas----------------------------------------------------------------------------*/
        function ConsultarRemesas() {
            /*Cargar el combo Novedad seguimiento */
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_NOVEDAD_SEGUIMIENTO } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListaNovedadSeguimiento = [];
                        $scope.ListaNovedadSeguimiento = response.data.Datos;
                        if ($scope.CodigoNovedadSeguimiento !== undefined && $scope.CodigoNovedadSeguimiento !== '' && $scope.CodigoNovedadSeguimiento !== null && $scope.CodigoNovedadSeguimiento !== 0) {
                            $scope.Modelo.NovedadSeguimiento = $linq.Enumerable().From($scope.ListaNovedadSeguimiento).First('$.Codigo ==' + $scope.CodigoNovedadSeguimiento);
                        } else {
                            $scope.Modelo.NovedadSeguimiento = '';
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroPlanilla: $scope.Modelo.NumeroPlanilla,
                Orden: $scope.Modelo.Orden
                //Pagina: $scope.paginaActualPlanillaRemesas,
                //RegistrosPagina: $scope.cantidadRegistrosPorPaginasPlanillaRemesas
            };
            DetalleSeguimientoVehiculosFactory.ConsultarRemesas(filtros).
                then(function (response) {

                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoSeguimientosRemesasPlanillaDespachos = [];
                            response.data.Datos.forEach(function (item) {
                                item.SitioReporteSeguimiento = $linq.Enumerable().From($scope.ListaSitioReporteModal).First('$.Codigo == ' + item.SitioReporteSeguimiento.Codigo);
                                if ($scope.ListaNovedadSeguimiento.length) {
                                    item.NovedadSeguimiento = $linq.Enumerable().From($scope.ListaNovedadSeguimiento).First('$.Codigo == ' + item.NovedadSeguimiento.Codigo);
                                } else {
                                    $scope.ListaNovedadSeguimiento = [];
                                    $scope.Modelo.NovedadSeguimiento = '';
                                }
                                if (item.ReportarCliente === 1) {
                                    item.ReportarCliente = true;
                                } else {
                                    item.ReportarCliente = false;
                                }
                                $scope.ListadoSeguimientosRemesasPlanillaDespachos.push(item);
                            });
                            if ($scope.ListadoSeguimientosRemesasPlanillaDespachos.length > 0) {
                                $scope.totalRegistrosPlanillaRemesas = $scope.ListadoSeguimientosRemesasPlanillaDespachos.length;
                                $scope.BuscandoPlanillaRemesas = true;
                                $scope.ResultadoSinRegistrosPlanillaRemesas = '';
                            } else {
                                $scope.totalRegistrosPlanillaRemesas = 0;
                                $scope.paginaActualPlanillaRemesas = 1;
                                $scope.ResultadoSinRegistrosPlanillaRemesas = 'No hay datos para mostrar';
                                $scope.BuscandoPlanillaRemesas = false;
                            }
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }


        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.CargarMapa = function (item) {
            showModal('modalMostrarMapa');
            $scope.iniciarMapa(item.Latitud, item.Longitud);
        };
        $scope.CargarMapa2 = function (item) {
            showModal('modalMostrarMapa2');
            $scope.iniciarMapa(item.Latitud, item.Longitud);
        };
        //funcion que muestra la modal distribucion 
        $scope.AbrirDistribucion = function (item) {
            /*Cargar el combo de tipo identificaciones*/
            $scope.MensajesErrorModal = []

            $scope.NumeroDocumentoRemesa = item.NumeroDocumentoRemesa;
            $scope.Distribucion.NumeroRemesa = item.NumeroRemesa;
            $scope.ResultadoSinRegistrosDistribucion = '';

            //funcion que consulta grid lista distribución remesas
            $scope.ListaDistribucion = [];
            var filtroDistribucion = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroRemesa: $scope.Distribucion.NumeroRemesa,
            };
            DetalleDistribucionRemesasFactory.Consultar(filtroDistribucion).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            response.data.Datos.forEach(function (item) {
                                if (item.Observaciones !== '') {
                                    var str = item.Observaciones;
                                    item.Observaciones = str.substring(0, 10);
                                    item.ObservacionCompleta = str.substring(0, 250);
                                    item.OcultarVerMas = true;
                                } else {
                                    item.OcultarVerMas = false;
                                }
                                $scope.ListaDistribucion.push(item);
                            });
                        } else {
                            $scope.DeshabilitarBotonNuevaDistribucion = true;
                            $scope.ResultadoSinRegistrosDistribucion = 'No hay distribuciones';
                        }
                    }
                }, function (response) {
                });
            showModal('modalDistribuciones');
        };

        //funcion que consulta detalles de la remesa
        $scope.CargarDatosRemesa = function () {
            /*Cargar autocomplete de productos transportados*/
            ProductoTransportadosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListaProductoTransportados = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListaProductoTransportados = response.data.Datos;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            //Filtros enviados
            var remesafiltros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Distribucion.NumeroRemesa,
                Obtener: 1
            };
            RemesasFactory.Obtener(remesafiltros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.NombreProductoTransportado = response.data.Datos.ProductoTransportado.Nombre;
                        $scope.NumeroCantidad = response.data.Datos.CantidadCliente;
                        $scope.NumeroPeso = response.data.Datos.PesoCliente;
                    } else {
                        ShowError(response.statusText);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };

        /*----------------------------------------------------------------------------------------------Funciones mapa----------------------------------------------------------------------------------*/
        $scope.iniciarMapaPlanilla = function (lat, lng) {
            //inicialización de variables
            var marker = [];
            var map, infoWindow;
            $scope.lat = lat
            $scope.lng = lng
            function initMap(posicion) {
                if (map === void 0) {
                    var mapOptions = {
                        zoom: 15,
                        center: new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude),
                        disableDefaultUI: true,
                        mapTypeId: window.google.maps.MapTypeId.ROADMAP
                    }
                    //Asignacion al id gmapsplanilla que se encuentra en el html
                    map = new window.google.maps.Map(document.getElementById('gmapsplanilla'), mapOptions);
                }
            }

            function setMarker(map, position, title, content) {
                var markers;
                var markerOptions = {
                    position: position,
                    map: map,
                    //draggable opcion que permite mover el marcador
                    draggable: true,
                    //bounce le da animacion al marcador
                    animation: google.maps.Animation.BOUNCE,
                    title: title,
                };

                markers = new window.google.maps.Marker(markerOptions);
                //dragend permite capturar las coordenadas seleccionadas en el mapa
                markers.addListener('dragend', function (event) {
                    //escribimos las coordenadas de la posicion actual del marcador dentro los input en el html 
                    $scope.Modelo.Latitud = this.getPosition().lat();
                    $scope.Modelo.Longitud = this.getPosition().lng();
                });
                //Variable donde se asignan los datos del marcador
                marker.push(markers);

                var infoWindowOptions = {
                    content: content
                };
                infoWindow = new window.google.maps.InfoWindow(infoWindowOptions);
                infoWindow.open(map, markers);

            }

            function localizacion(posicion) {
                initMap(posicion);
                //Condición que muestra la ubicación del api o la ubicación segón latitud y longitud 
                if ($scope.lat != undefined && $scope.lng != undefined && $scope.lat != 0 && $scope.lng != 0) {
                    setMarker(map, new window.google.maps.LatLng($scope.lat, $scope.lng), 'Posición del usuario', 'Me encuentro aquí');
                } else {
                    setMarker(map, new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude), 'Posición actual del usuario', 'Me encuentro aquí');
                }
            }

            function errores(error) {
                MensajeInfo("Info", "Ha ocurrido un error al intentar obtener la información de las cordenadas GPS. Error:" + error, false);
            }

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(localizacion, errores);
            } else {
                MensajeInfo("Info", "Tu dispositivo no soporta o no tiene habilitada la 'Geolocalización'", false);
            }

        }

        $scope.iniciarMapa = function (lat, lng) {
            var marker = [];
            var map, infoWindow;
            $scope.lat = lat
            $scope.lng = lng
            function initMap(posicion) {
                if (map === void 0) {
                    var mapOptions = {
                        zoom: 15,
                        center: new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude),
                        disableDefaultUI: true,
                        mapTypeId: window.google.maps.MapTypeId.ROADMAP
                    }

                    map = new window.google.maps.Map(document.getElementById('gmaps'), mapOptions);
                }
            }

            function setMarker(map, position, title, content) {
                var markers;
                var markerOptions = {
                    position: position,
                    map: map,
                    draggable: true,
                    animation: google.maps.Animation.BOUNCE,
                    title: title,
                    //icon: 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png'
                };

                markers = new window.google.maps.Marker(markerOptions);
                markers.addListener('dragend', function (event) {
                    //escribimos las coordenadas de la posicion actual del marcador dentro los input 
                    $scope.Modelo.Latitud = this.getPosition().lat();
                    $scope.Modelo.Longitud = this.getPosition().lng();
                });
                marker.push(markers);

                var infoWindowOptions = {
                    content: content
                };
                infoWindow = new window.google.maps.InfoWindow(infoWindowOptions);
                infoWindow.open(map, markers);

            }

            function localizacion(posicion) {
                initMap(posicion);
                if ($scope.lat != undefined && $scope.lng != undefined && $scope.lat != 0 && $scope.lng != 0) {
                    setMarker(map, new window.google.maps.LatLng($scope.lat, $scope.lng), 'Posición del usuario', 'Me encuentro aquí');
                } else {
                    setMarker(map, new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude), 'Posición actual del usuario', 'Me encuentro aquí');
                }
            }

            function errores(error) {
                MensajeInfo("Info", "Ha ocurrido un error al intentar obtener la información de las cordenadas GPS. Error:" + error, false);
            }

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(localizacion, errores);
            } else {
                MensajeInfo("Info", "Tu dispositivo no soporta o no tiene habilitada la 'Geolocalización'", false);
            }

        }

        /*----------------------------------------------------------------------------------------------Funciones foto----------------------------------------------------------------------------------*/
        $scope.ConsultarFoto = function (detalle) {

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroRemesa: detalle.NumeroRemesa,
                Codigo: detalle.Codigo
            };

            DetalleDistribucionRemesasFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.Fotografia.FotoBits !== undefined && response.data.Datos.Fotografia.FotoBits !== null && response.data.Datos.Fotografia.FotoBits !== '') {
                            $scope.Foto = [];
                            $scope.Foto.push({
                                NombreFoto: response.data.Datos.Fotografia.NombreFoto,
                                TipoFoto: response.data.Datos.Fotografia.TipoFoto,
                                ExtensionFoto: response.data.Datos.Fotografia.ExtensionFoto,
                                FotoBits: response.data.Datos.Fotografia.FotoBits,
                                ValorDocumento: 1,
                                FotoCargada: 'data:' + response.data.Datos.Fotografia.TipoFoto + ';base64,' + response.data.Datos.Fotografia.FotoBits
                            });
                        } else {
                            $scope.Foto = [];
                        }
                    }
                    else {
                        ShowError('No se logró consultar la foto ' + response.data.MensajeOperacion);
                        closeModal('ModalRecibirDistribucion', 1);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    closeModal('ModalRecibirDistribucion', 1);
                });
            showModal('ModalFoto');
        };

        $scope.CerrarModalFoto = function () {
            closeModal('ModalFoto', 1);
        }
        /*----------------------------------------------------------------------------------------------Funciones Firma----------------------------------------------------------------------------------*/
        $scope.ConsultarFirma = function (detalle) {
            $scope.LimpiarFirma();
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroRemesa: detalle.NumeroRemesa,
                Codigo: detalle.Codigo
            };
            DetalleDistribucionRemesasFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.Firma !== undefined && response.data.Datos.Firma !== null && response.data.Datos.Firma !== '') {
                            var image = new Image();
                            image.src = "data:image/png;base64," + response.data.Datos.Firma;
                            var img = cargaContextoCanvas('Firma');
                            img.drawImage(image, 0, 0);
                        }
                        $('#botonFirma').click();
                    }
                    else {
                        ShowError('No se logró consultar la firma ' + response.data.MensajeOperacion);
                        closeModal('ModalRecibirDistribucion', 1);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    closeModal('ModalRecibirDistribucion', 1);
                });
            showModal('ModalFirma');
            $('#botonFirma').click();
        };

        $scope.LimpiarFirma = function () {
            var canvas = document.getElementById('Firma');
            var context = canvas.getContext("2d");
            context.clearRect(0, 0, canvas.width, canvas.height);
        };

        $scope.CerrarModalFirma = function () {
            closeModal('ModalFirma', 1);
        };

        $scope.InfoContenedor = function (item) {
            showModal('modalContenedor')
            $scope.Contenedor = {}
            $scope.Contenedor = angular.copy(item)
            $scope.Contenedor.FechaDevolucion = new Date(item.FechaDevolucion)
            try {
                $scope.Contenedor.CiudadDevolucion = $scope.CargarCiudad(item.CiudadDevolucion.Codigo)
            } catch (e) {

            }
        }

    }]);