﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios
Imports EncoExpres.GesCarga.Negocio.Seguridad

''' <summary>
''' Controlador <see cref="EstudioSeguridadDocumentosController"/>
''' </summary>
<Authorize>
Public Class EstudioSeguridadDocumentosController
    Inherits Base
    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As EstudioSeguridadDocumentos) As Respuesta(Of IEnumerable(Of EstudioSeguridadDocumentos))
        Return New LogicaEstudioSeguridadDocumentos(CapaPersistenciaEstudioSeguridadDocumentos).Consultar(filtro)
    End Function
    <HttpPost>
    <ActionName("InsertarTemporal")>
    Public Function Guardar(ByVal entidad As EstudioSeguridadDocumentos) As Respuesta(Of Long)
        Return New LogicaEstudioSeguridadDocumentos(CapaPersistenciaEstudioSeguridadDocumentos).InsertarTemporal(entidad)
    End Function
    <HttpPost>
    <ActionName("EliminarDocumento")>
    Public Function EliminarDocumento(ByVal entidad As EstudioSeguridadDocumentos) As Respuesta(Of Boolean)
        Return New LogicaEstudioSeguridadDocumentos(CapaPersistenciaEstudioSeguridadDocumentos).EliminarDocumento(entidad)
    End Function
    <HttpPost>
    <ActionName("EliminarDocumentoDefinitivo")>
    Public Function EliminarDocumentoDefinitivo(ByVal entidad As EstudioSeguridadDocumentos) As Respuesta(Of Boolean)
        Return New LogicaEstudioSeguridadDocumentos(CapaPersistenciaEstudioSeguridadDocumentos).EliminarDocumentoDefinitivo(entidad)
    End Function
    <HttpPost>
    <ActionName("LimpiarDocumentoTemporalUsuario")>
    Public Function LimpiarDocumentoTemporalUsuario(ByVal entidad As EstudioSeguridadDocumentos) As Respuesta(Of Boolean)
        Return New LogicaEstudioSeguridadDocumentos(CapaPersistenciaEstudioSeguridadDocumentos).LimpiarDocumentoTemporalUsuario(entidad)
    End Function

End Class
