﻿
PRINT 'gsp_insertar_detalle_remesa'
go
DROP PROCEDURE gsp_insertar_detalle_remesa
GO
create procedure gsp_insertar_detalle_remesa
(
@par_EMPR_Codigo  smallint
,@par_ENRE_Numero  numeric(18,0) 
,@par_PRTR_Codigo  numeric(18,0)
,@par_Descripcion  varchar(200) = null
,@par_Serie  varchar(50)= null
,@par_Modelo  varchar(50)= null
,@par_Cantidad  numeric(18,2)= null
,@par_Peso  numeric(18,2)= null
,@par_Remision  varchar(50)= null
,@par_Valor_Mercancia  money= null
,@par_TERC_Codigo_Destinatario  numeric(18,0)= null
,@par_CIUD_Codigo_Destinatario  numeric(18,0)= null
,@par_Direccion_Destinatario  varchar(150)= null
,@par_Telefonos_Destinatario  varchar(100)= null
)
AS BEGIN
INSERT INTO Detalle_Remesas
           (EMPR_Codigo
           ,ENRE_Numero
           ,PRTR_Codigo
           ,Descripcion
           ,Serie
           ,Modelo
           ,Cantidad
           ,Peso
           ,Remision
           ,Valor_Mercancia
           ,TERC_Codigo_Destinatario
           ,CIUD_Codigo_Destinatario
           ,Direccion_Destinatario
           ,Telefonos_Destinatario
           ,Cumplido
           )
     VALUES
           (@par_EMPR_Codigo 
           ,@par_ENRE_Numero 
           ,@par_PRTR_Codigo 
           ,ISNULL(@par_Descripcion,'')
           ,ISNULL(@par_Serie ,'')
           ,ISNULL(@par_Modelo ,'')
           ,ISNULL(@par_Cantidad ,0 )
           ,ISNULL(@par_Peso  ,0)
           ,ISNULL(@par_Remision  ,'')
           ,ISNULL(@par_Valor_Mercancia ,0)
           ,@par_TERC_Codigo_Destinatario  
           ,@par_CIUD_Codigo_Destinatario 
		   ,ISNULL(@par_Direccion_Destinatario ,'')
           ,ISNULL(@par_Telefonos_Destinatario ,'')
           ,0
          )

		  select  @@IDENTITY as Codigo
END
GO