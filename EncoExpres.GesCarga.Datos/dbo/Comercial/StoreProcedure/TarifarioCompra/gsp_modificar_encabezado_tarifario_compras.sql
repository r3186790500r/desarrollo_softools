﻿PRINT 'gsp_modificar_encabezado_tarifario_compras'
GO
DROP PROCEDURE gsp_modificar_encabezado_tarifario_compras
GO
CREATE PROCEDURE gsp_modificar_encabezado_tarifario_compras
(
@par_EMPR_Codigo smallint
,@par_Numero numeric(18,0) = null
,@par_Tarifario_Base smallint = null
,@par_Fecha_Vigencia_Inicio date = null
,@par_Fecha_Vigencia_Fin date = null
,@par_Estado smallint = null
,@par_USUA_Codigo_Modifica smallint = null
)
AS BEGIN


update Encabezado_Tarifario_Carga_compras
           set Tarifario_Base = @par_Tarifario_Base
           ,Fecha_Vigencia_Inicio = @par_Fecha_Vigencia_Inicio
           ,Fecha_Vigencia_Fin = @par_Fecha_Vigencia_Fin
           ,Estado = @par_Estado
           ,USUA_Codigo_Modifica = @par_USUA_Codigo_Modifica
           ,Fecha_Crea = getdate()

		   where EMPR_Codigo = @par_EMPR_Codigo
		   and Numero  = @par_Numero
delete Detalle_Tarifario_Carga_compras where ETCC_Numero = @par_Numero and EMPR_Codigo = @par_EMPR_Codigo

SELECT @par_Numero AS Codigo

END
GO