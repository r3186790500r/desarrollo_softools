﻿Imports EncoExpres.GesCarga.Entidades.Basico.Tesoreria
Imports EncoExpres.GesCarga.Fachada.Basico.Tesoreria
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources
Imports EncoExpres.GesCarga.Fachada.Basico

Namespace Basico.Tesoreria
    Public NotInheritable Class LogicaCuentaBancarias
        Inherits LogicaBase(Of CuentaBancarias)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As PersistenciaCuentaBancarias
        ReadOnly _persistenciaCuentaOficinas As IPersistenciaBase(Of CuentaBancariaOficinas)

        Sub New(persistencia As IPersistenciaBase(Of CuentaBancarias), persistenciaCuentaOficinas As IPersistenciaBase(Of CuentaBancariaOficinas))
            _persistencia = persistencia
            _persistenciaCuentaOficinas = persistenciaCuentaOficinas
        End Sub

        Public Overrides Function Consultar(filtro As CuentaBancarias) As Respuesta(Of IEnumerable(Of CuentaBancarias))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of CuentaBancarias))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of CuentaBancarias))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As CuentaBancarias) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Codigo.Equals(0) Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As CuentaBancarias) As Respuesta(Of CuentaBancarias)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of CuentaBancarias)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of CuentaBancarias)(consulta)
        End Function
        Public Function CuentaBancariaOficinas(filtro As CuentaBancarias) As Respuesta(Of IEnumerable(Of CuentaBancariaOficinas))
            Dim consulta = _persistencia.CuentaBancariaOficinas(filtro.CodigoEmpresa, filtro.Codigo)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of CuentaBancariaOficinas))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of CuentaBancariaOficinas))(consulta)
        End Function
        Private Function DatosRequeridos(entidad As CuentaBancarias) As Respuesta(Of CuentaBancarias)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of CuentaBancarias) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of CuentaBancarias) With {.ProcesoExitoso = True}

        End Function
        Public Function Anular(entidad As CuentaBancarias) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaCuentaBancarias).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function

        Public Function ConsultarCuentaOficinas(filtro As CuentaBancariaOficinas) As Respuesta(Of IEnumerable(Of CuentaBancariaOficinas))
            Dim consulta = CType(_persistenciaCuentaOficinas, PersistenciaCuentaBancariaOficinas).ConsultarCuentaOficinas(filtro.CodigoEmpresa, filtro.CuentaBancaria.Codigo)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of CuentaBancariaOficinas))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of CuentaBancariaOficinas))(consulta)
        End Function


        Public Function GuardarCuentaOficinas(detalle As IEnumerable(Of CuentaBancariaOficinas)) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            valor = CType(_persistenciaCuentaOficinas, PersistenciaCuentaBancariaOficinas).InsertarCuentaOficina(detalle)

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If
            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True}
        End Function

    End Class

End Namespace