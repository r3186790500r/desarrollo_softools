﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */


	PRINT 'gsp_consultar_impuestos'
GO
DROP PROCEDURE gsp_consultar_impuestos	
GO
CREATE PROCEDURE gsp_consultar_impuestos(  
  @par_EMPR_Codigo SMALLINT,  
  @par_Codigo NUMERIC = NULL,
  @par_CodigoAlterno VARCHAR(20) = NULL, 
  @par_Nombre VARCHAR(100)= NULL,   
  @par_CATA_TRAI_Codigo NUMERIC= NULL,
  @par_CATA_TIIM_Codigo NUMERIC= NULL,
  @par_Estado SMALLINT = NULL,
  @par_NumeroPagina INT = NULL,  
  @par_RegistrosPagina INT = NULL  
 )  
 AS  
 BEGIN  
  SET NOCOUNT ON;  
   DECLARE  
    @CantidadRegistros INT  
   SELECT @CantidadRegistros = (  
     SELECT DISTINCT   
      COUNT(1)   
      FROM Encabezado_Impuestos  ENIM
      , usuarios USCR 
      , usuarios USMO 

      WHERE  
      ENIM.EMPR_Codigo = @par_EMPR_Codigo 
	  AND ENIM.Codigo = isnull(@par_Codigo ,ENIM.Codigo)
      AND ENIM.Codigo = ISNULL(@par_Codigo,ENIM.Codigo)   
      AND ENIM.Codigo_Alterno = ISNULL(@par_CodigoAlterno,ENIM.Codigo_Alterno) 
	  AND ENIM.Nombre LIKE'%'+ISNULL(@par_Nombre,ENIM.Nombre)+'%'
	  AND ENIM.CATA_TRAI_Codigo = ISNULL(@par_CATA_TRAI_Codigo,ENIM.CATA_TRAI_Codigo) 
	  AND ENIM.CATA_TIIM_Codigo = ISNULL(@par_CATA_TIIM_Codigo,ENIM.CATA_TIIM_Codigo) 
	  AND ENIM.Estado = ISNULL(@par_Estado,ENIM.Estado) 
	  AND ENIM.EMPR_Codigo = USCR.EMPR_Codigo
      AND ENIM.USUA_Codigo_Crea = USCR.Codigo
	  AND ENIM.EMPR_Codigo = USMO.EMPR_Codigo
      AND ENIM.USUA_Codigo_Crea = USMO.Codigo

	        );         
     WITH Pagina AS  
     (  
      SELECT   
      ENIM.EMPR_Codigo,
	  ENIM.Codigo_Alterno,  
	  ENIM.Codigo, 
	  ENIM.Nombre,
	  ENIM.Label, 
	  ENIM.CATA_TIIM_Codigo, 
	  TIIM.Campo1 Tipo_Impuesto,
	  ENIM.CATA_TRAI_Codigo, 
	  TRAI.Campo1 Tipo_Recaudo,
	  ENIM.Valor_Tarifa, 
	  ENIM.Valor_Base, 
	  ENIM.Operacion,
	  ENIM.Estado,
      USCR.Codigo USUA_Codigo_Crea ,
	  ENIM.Fecha_Crea ,
	  USMO.Codigo USUA_Codigo_Modifica ,
	  ENIM.Fecha_Modifica,
	  ENIM.PLUC_Codigo,
	  Obtener=0,
      ROW_NUMBER() OVER(ORDER BY ENIM.Codigo) AS RowNumber  
	  
      FROM  
     Encabezado_Impuestos  ENIM
      , usuarios USCR 
      , usuarios USMO 
	  , Valor_Catalogos TIIM
	  , Valor_Catalogos TRAI
      WHERE  
       ENIM.EMPR_Codigo = @par_EMPR_Codigo   
      AND ENIM.Codigo > 0  
	  AND ENIM.Codigo = isnull(@par_Codigo ,ENIM.Codigo)
      AND ENIM.Codigo_Alterno = ISNULL(@par_CodigoAlterno,ENIM.Codigo_Alterno) 
	  AND ENIM.Nombre LIKE'%'+ ISNULL(@par_Nombre,ENIM.Nombre )+'%' 
	  AND ENIM.CATA_TRAI_Codigo = ISNULL(@par_CATA_TRAI_Codigo,ENIM.CATA_TRAI_Codigo) 
	  AND ENIM.CATA_TIIM_Codigo = ISNULL(@par_CATA_TIIM_Codigo,ENIM.CATA_TIIM_Codigo) 
	  AND ENIM.Estado = ISNULL(@par_Estado,ENIM.Estado) 
	  AND ENIM.EMPR_Codigo = USCR.EMPR_Codigo
      AND ENIM.USUA_Codigo_Crea = USCR.Codigo
	  AND ENIM.EMPR_Codigo = USMO.EMPR_Codigo
      AND ENIM.USUA_Codigo_Crea = USMO.Codigo
	  AND ENIM.EMPR_Codigo = TIIM.EMPR_Codigo
	  AND ENIM.CATA_TIIM_Codigo = TIIM.Codigo
	  AND ENIM.EMPR_Codigo = TRAI.EMPR_Codigo
	  AND ENIM.CATA_TRAI_Codigo = TRAI.Codigo

     ) 
   SELECT DISTINCT  
	 EMPR_Codigo, 
	 Codigo_Alterno,
	 Codigo,
	 Nombre,
	 Label,
	 CATA_TIIM_Codigo,
	 Tipo_Impuesto,
	 CATA_TRAI_Codigo,
	 Tipo_Recaudo,
	 Valor_Base,
	 Operacion,
	 Valor_Tarifa,
	 Estado,
	 USUA_Codigo_Crea,
	 Fecha_Crea,
	 USUA_Codigo_Modifica,
	 Fecha_Modifica,
	 PLUC_Codigo,
	 Obtener,
   @CantidadRegistros AS TotalRegistros,  
   @par_NumeroPagina AS PaginaObtener,  
   @par_RegistrosPagina AS RegistrosPagina  
   FROM  
    Pagina  
   WHERE  
    RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
    AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
   order by Codigo  
   END   
GO