﻿Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Fachada.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources

Namespace Basico.Despachos
    Public NotInheritable Class LogicaFotosVehiculos
        Inherits LogicaBase(Of FotosVehiculos)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of FotosVehiculos)

        Sub New(persistencia As IPersistenciaBase(Of FotosVehiculos))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As FotosVehiculos) As Respuesta(Of IEnumerable(Of FotosVehiculos))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of FotosVehiculos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of FotosVehiculos))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As FotosVehiculos) As Respuesta(Of Long)
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As FotosVehiculos) As Respuesta(Of FotosVehiculos)
            Throw New NotImplementedException()
        End Function
        Private Function DatosRequeridos(entidad As FotosVehiculos) As Respuesta(Of FotosVehiculos)
            Throw New NotImplementedException()

        End Function
        Public Function InsertarTemporal(entidad As FotosVehiculos) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long
            valor = CType(_persistencia, PersistenciaFotosVehiculos).InsertarTemporal(entidad)

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo.Equals(0), String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function
        Public Function EliminarFoto(entidad As FotosVehiculos) As Respuesta(Of Boolean)
            Dim resultado As Boolean = False
            Dim mensaje As String = String.Empty

            resultado = CType(_persistencia, PersistenciaFotosVehiculos).EliminarFoto(entidad)

            If Not resultado Then
                mensaje = String.Format("La foto no se logro eliminar, Contacte con el administrador del sistema")
            End If

            Return New Respuesta(Of Boolean)(resultado, mensaje)
        End Function
        Public Function LimpiarTemporalUsuario(entidad As FotosVehiculos) As Respuesta(Of Boolean)
            Dim resultado As Boolean = False
            Dim mensaje As String = String.Empty

            resultado = CType(_persistencia, PersistenciaFotosVehiculos).LimpiarTemporalUsuario(entidad)

            If Not resultado Then
                mensaje = String.Format("")
            End If

            Return New Respuesta(Of Boolean)(resultado, mensaje)
        End Function
    End Class

End Namespace