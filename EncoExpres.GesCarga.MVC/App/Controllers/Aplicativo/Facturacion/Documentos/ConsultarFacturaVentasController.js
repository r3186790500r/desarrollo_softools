﻿EncoExpresApp.controller("ConsultarFacturaVentasCtrl", ['$scope', '$timeout', '$linq', '$routeParams', 'OficinasFactory', 'FacturasFactory', 'blockUI', 'blockUIConfig', 'EmpresasFactory', 'TercerosFactory',
    'EncabezadoComprobantesContablesFactory', 'ConceptosFacturacionFactory', 'ValorCatalogosFactory',
    function ($scope, $timeout, $linq, $routeParams, OficinasFactory, FacturasFactory, blockUI, blockUIConfig, EmpresasFactory, TercerosFactory,
        EncabezadoComprobantesContablesFactory, ConceptosFacturacionFactory, ValorCatalogosFactory) {

        $scope.MapaSitio = [{ Nombre: 'Facturación' }, { Nombre: 'Documentos' }, { Nombre: 'Facturas Venta' }];
        $scope.Titulo = 'CONSULTAR FACTURAS VENTA';
        //-----------------------------------------------------------------DECLARACION VARIABLES---------------------------------------------------------------------------------//
        $scope.Buscando = false;
        $scope.ResultadoSinRegistros = '';
        $scope.Deshabilitar = false;
        $scope.MensajesError = [];
        var filtros = {};
        $scope.pref = '';
        $scope.totalRegistros = 0;
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.totalPaginas = 0;
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.ArchivoFactura = "";

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ===' + OPCION_MENU_FACTURA_VENTAS); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;

        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy === true) {
            $scope.FechaFinal = new Date();
        }

        $scope.Estados = {
            Adquiente: '',
            Notificacion: '',
            Fecha: ''
        };
        //-----------------------------------------------------------------DECLARACION VARIABLES---------------------------------------------------------------------------------//
        //----------------------------------------VERIFICAR PROVEEDOR FACTURA ELECTRONICA---------------------------------------//
        $scope.MostrarModuloFacturaElectronica = false;
        if ($scope.Sesion.Empresa.EmpresaFacturaElectronica !== undefined && $scope.Sesion.Empresa.EmpresaFacturaElectronica !== '' && $scope.Sesion.Empresa.EmpresaFacturaElectronica !== null) {
            $scope.MostrarModuloFacturaElectronica = true;
        }
        //----------------------------------------VERIFICAR PROVEEDOR FACTURA ELECTRONICA---------------------------------------//
        //-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------//
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };

        $scope.Imprimir = function () {
            Print();
        };
        $scope.ImprimirReporte = function (numero) {
            $scope.NumeroFactura = numero;
            PrintReporte();
        };
        //-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------//

        if ($routeParams !== undefined && $routeParams.Numero !== null) {
            $scope.ListaFacturas = [];
            if (parseInt($routeParams.Numero) > CERO) {
                $scope.Numero = parseInt($routeParams.Numero);
                //-- Filtro de busqueda en Get 
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: $scope.Numero,
                    CataTipoFactura: CODIGO_CATALOGO_TIPO_FACTURA,
                    Estado: -1,
                    Pagina: $scope.paginaActual,
                    RegistrosPagina: $scope.cantidadRegistrosPorPagina
                };
                FacturasFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {

                                $scope.ListaFacturas = response.data.Datos;

                                /*----------------------------*/
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
                //-- Filtro de busqueda en Get 
            }
        }


        $scope.GenerarMovimientoContable = function (NumeroDocumento) {
            EncabezadoComprobantesContablesFactory.Guardar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, NumeroDocumento: NumeroDocumento, TipoDocumentoOrigen: { Codigo: CODIGO_TIPO_DOCUMENTO_FACTURAS } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('El movimiento contable se regeneró correctamente');
                    } else {
                        ShowError('No se generó el movimiento contable, por favor verifique la parametrización ');
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };
        //---- Clientes
        $scope.ListadoCliente = [];
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) === 0 || value.length === 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CLIENTE,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListadoCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCliente);
                }
            }
            return $scope.ListadoCliente;
        };
        //--Cargar el combo de forma pago
        $scope.ListadoFormaPago = ValorCatalogosFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Catalogo: { Codigo: CODIGO_CATALOGO_FORMA_PAGO_VENTAS },
            Sync: true
        }).Datos;
        $scope.ListadoFormaPago.splice($scope.ListadoFormaPago.findIndex(item => item.Codigo === CODIGO_CATALOGO_FORMA_PAGO_VENTAS_NA), 1);
        $scope.ListadoFormaPago.push({ Codigo: -1, Nombre: '(TODAS)' });
        $scope.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo === -1');
        //--Cargar Estados Factura Electronica
        $scope.ListadoEstadosFael = ValorCatalogosFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Catalogo: { Codigo: CODIGO_CATALOGO_ESTADOS_FACTURA_ELECTRONICA },
            Sync: true
        }).Datos;
        $scope.ListadoEstadosFael.push({ Nombre: "Pendiente", CampoAuxiliar2: "label label-warning" });


        $scope.ListadoEstados = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'DEFINITIVO', Codigo: ESTADO_DEFINITIVO },
            { Nombre: 'BORRADOR', Codigo: ESTADO_BORRADOR },
            { Nombre: 'ANULADO', Codigo: 2 }
        ];
        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo === -1');

        //-- cargar ijo EmPrefpresa --//
        EmpresasFactory.Consultar(
            { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, }
        ).then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                $scope.pref = response.data.Datos[0].Prefijo;
                //console.log("Empresa Prefijo: ", $scope.pref);
            }
        });
        //--Carga oficinas
        $scope.ListadoOficinas = [];
        if ($scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas) {
            OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1 })
                    if (response.data.ProcesoExitoso === true) {
                        response.data.Datos.forEach(function (item) {
                            $scope.ListadoOficinas.push(item)
                        });
                        $scope.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo === -1');
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        } else {
            if ($scope.Sesion.UsuarioAutenticado.ListadoOficinas.length > 0) {
                $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1 })
                $scope.Sesion.UsuarioAutenticado.ListadoOficinas.forEach(function (item) {
                    $scope.ListadoOficinas.push(item)
                });
                $scope.Oficina = $scope.ListadoOficinas[0]
            } else {
                ShowError('El usuario no tiene oficinas asociadas');
            }
        }
        //---Conceptos Facturacion
        $scope.ListadoConceptosFactura = ConceptosFacturacionFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, Sync: true }).Datos;
        //---Conceptos Facturacion
        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar === PERMISO_ACTIVO) {
                document.location.href = '#!GestionarFacturaVentas';
            }
        };

        //----------------------------------------ANULACION----------------------------------------//
        //--Alerta Confirmación Anulación
        $scope.ConfirmacionAnularfactura = function (ItemFactura) {
            $scope.NumeroAnulacion = ItemFactura.Numero;
            $scope.NumeroDocumentoAnula = ItemFactura.NumeroDocumento;
            $scope.TipoDocumentoAnula = ItemFactura.TipoDocumento;
            $scope.MensajesErrorAnular = [];
            $scope.CausaAnulacion = "";
            if (ItemFactura.Estado === ESTADO_BORRADOR) {
                showModal('modalConfirmacionAnularfactura');
            }
            else {
                if (ItemFactura.Estado === ESTADO_DEFINITIVO && ItemFactura.RemesasPaqueteria > CERO && ItemFactura.FormaPago.Codigo === CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CONTADO) {
                    showModal('modalConfirmacionAnularfactura');
                }
                else {
                    ShowError('No se puede anular una factura en definitivo');
                }
            }
        };
        //--Funcion que solicita los datos de la anulación
        $scope.SolicitarDatosAnulacion = function () {
            closeModal('modalConfirmacionAnularfactura');
            showModal('modalDatosAnularfactura');
        };

        $scope.Anular = function () {
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.NumeroAnulacion,
                TipoDocumento: $scope.TipoDocumentoAnula,
                UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                CausaAnulacion: $scope.CausaAnulacion,
            };
            if (DatosRequeridosAnular()) {
                FacturasFactory.Anular(entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            ShowSuccess('Se anuló la factura  No.' + $scope.NumeroDocumentoAnula);
                            closeModal('modalDatosAnularfactura', 1);
                            Find();
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };

        function DatosRequeridosAnular() {
            var continuar = true;
            $scope.MensajesErrorAnular = [];

            if ($scope.CausaAnulacion === undefined || $scope.CausaAnulacion === null || $scope.CausaAnulacion === "") {
                continuar = false;
                $scope.MensajesErrorAnular.push('debe ingresar la causa de anulación');
            }
            return continuar;
        }
        //----------------------------------------ANULACION----------------------------------------//
        ///---------------------------------------------------------------------Funcion Buscar Facturas-----------------------------------------------------------------------------///
        $scope.Buscar = function () {
            if (DatosRequeridos()) {
                if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                    Find();
                }
            }
        };

        function Find() {
            $scope.ListaFacturas = [];
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.Buscando = true;
            var filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroDocumento: $scope.NumeroInicial,

                FechaInicial: $scope.NumeroInicial > 0 ? undefined : $scope.FechaInicial,
                FechaFinal: $scope.NumeroInicial > 0 ? undefined : $scope.FechaFinal,
                CataTipoFactura: CODIGO_CATALOGO_TIPO_FACTURA,
                OficinaFactura: { Codigo: $scope.Oficina.Codigo },

                Cliente: { Codigo: $scope.Cliente === undefined ? '' : $scope.Cliente.Codigo },
                FormaPago: { Codigo: $scope.FormaPago.Codigo },

                Estado: $scope.Estado.Codigo,
                UsuarioConsulta: { Codigo: $scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas ? 0 : $scope.Sesion.UsuarioAutenticado.Codigo },
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPagina,
            };
            blockUI.delay = 1000;
            FacturasFactory.Consultar(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {

                            $scope.ListaFacturas = response.data.Datos;
                            /*----------------------------*/
                            $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                            $scope.Buscando = false;
                            $scope.ResultadoSinRegistros = '';
                        }
                        else {
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            blockUI.stop();
            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 1000);
        };

        ///*---------------------------------------------------------------------Funcion validar datos requeridos-----------------------------------------------------------------------------*/
        $scope.FechaInicial = new Date();
        $scope.FechaFinal = new Date();
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy === true) {
            $scope.FechaInicial = new Date();
            $scope.FechaFinal = new Date();
        }
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.FechaInicial === null || $scope.FechaInicial === undefined || $scope.FechaInicial === '')
                && ($scope.FechaFinal === null || $scope.FechaFinal === undefined || $scope.FechaFinal === '')
                && ($scope.NumeroInicial === null || $scope.NumeroInicial === undefined || $scope.NumeroInicial === '' || $scope.NumeroInicial === 0 || isNaN($scope.NumeroInicial) === true)

            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false;

            } else if (($scope.NumeroInicial !== null && $scope.NumeroInicial !== undefined && $scope.NumeroInicial !== '' && $scope.NumeroInicial !== 0)
                || ($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')
                || ($scope.FechaFinal !== null && $scope.FechaFinal !== undefined && $scope.FechaFinal !== '')

            ) {
                if (($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')
                    && ($scope.FechaFinal !== null && $scope.FechaFinal !== undefined && $scope.FechaFinal !== '')) {
                    if ($scope.FechaFinal < $scope.FechaInicial) {
                        $scope.MensajesError.push('La fecha final debe ser mayor a la fecha inicial');
                        continuar = false;
                    } else if ((($scope.FechaFinal - $scope.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false;
                    }
                }
                else {
                    if (($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')) {
                        $scope.FechaFinal = $scope.FechaInicial;
                    }
                    else {
                        $scope.FechaInicial = $scope.FechaFinal;
                    }
                }
            }

            return continuar;
        }
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        //Obtiene la URL para llamar el proyecto ASP
        $scope.urlASP = '';
        $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);

        //Función que ejecuta el boton de vista preliminar
        $scope.DesplegarInforme = function (NumeroFactura, OpcionPDf, OpcionEXCEL) {
            if ($scope.ValidarPermisos.AplicaImprimir === PERMISO_ACTIVO) {
                $scope.FiltroArmado = '';
                //$scope.TipoDocumento = TIPO_FACTURA_TRANSPORTE_ESPECIAL

                //Depende del listado seleccionado se enviará el nombre por parametro
                $scope.NumeroFactura = NumeroFactura;
                $scope.NombreReporte = NOMBRE_REPORTE_FACT
                //Arma el filtro
                $scope.ArmarFiltro();
                //Llama a la pagina ASP con el filtro por GET
                if (OpcionPDf === 1) {
                    window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&Prefijo=' + $scope.pref + '&TipoFactura=' + CODIGO_CATALOGO_TIPO_FACTURA);
                }
                if (OpcionEXCEL === 1) {
                    window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionEXCEL + '&Prefijo=' + $scope.pref + '&TipoFactura=' + CODIGO_CATALOGO_TIPO_FACTURA);
                }
                //Se limpia el filtro para poder hacer mas consultas
                $scope.FiltroArmado = '';
            };
        };
        // funcion enviar parametros al proyecto ASP armando el filtro
        $scope.ArmarFiltro = function () {
            $scope.FiltroArmado = '';
            if ($scope.NumeroFactura !== undefined && $scope.NumeroFactura !== '' && $scope.NumeroFactura !== null) {
                $scope.FiltroArmado += '&Numero=' + $scope.NumeroFactura;
            }
        }
        ///*----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        //---------------------------------------------------------------------Proceso Factura Electronica----------------------------------------------------------------------------//
        $scope.ProcesarFacturaElectronica = function (Factura) {
            if (ValidarProcesoFacturaElectronica(Factura)) {
                switch ($scope.Sesion.Empresa.EmpresaFacturaElectronica.Proveedor.Codigo) {
                    case PROVEEDOR_FACTURA_ELECTRONICA.SAPHETY:
                        var Response = FacturasFactory.ObtenerDatosFacturaElectronicaSaphety({
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Numero: Factura.Numero,
                            Sync: true
                        });
                        //Bloqueo Pantalla
                        blockUIConfig.autoBlock = true;
                        blockUIConfig.delay = 0;
                        blockUI.start("Reportando Factura...");
                        $timeout(function () {
                            blockUI.message("Reportando Factura...");
                            Gestionar_Factura_Electronica_SAPHETY(Response.Datos);
                        }, 100);
                        //Bloqueo Pantalla
                        break;
                    default:
                        ShowError("No se ha asignado proveerdor electrónico");
                        break;
                }
            }
        };

        function ValidarProcesoFacturaElectronica(factura) {
            var continuar = true;
            var txtErrores = "";
            if (factura.Estado === ESTADO_BORRADOR) {
                continuar = false;
                txtErrores += "</br>- La Factura debe estar en estado definitivo";
            }
            if (factura.Anulado === ESTADO_ANULADO) {
                continuar = false;
                txtErrores += "</br>- La Factura no puede estar anulada";
            }
            if (factura.EstadoFAEL === ESTADO_DEFINITIVO) {
                continuar = false;
                txtErrores += "</br>- La Factura ya fue emitida electrónicamente";
            }
            if (txtErrores !== "") {
                ShowError("Error al reportar la Factura: " + txtErrores);
            }
            return continuar;
        }

        function Gestionar_Factura_Electronica_SAPHETY(Factura) {


            try {
                //--Valida Destinatario
                var Destinatario = "";
                if (Factura.FacturarA.Codigo != undefined && Factura.FacturarA.Codigo != null && Factura.FacturarA.Codigo != '' && Factura.FacturarA.Codigo > 0) {
                    Destinatario = Factura.FacturarA;
                }
                else {
                    Destinatario = Factura.Cliente;
                }
                //--Valida Destinatario
                //--Calculo de totales
                var subtotal = 0;
                var descuento = 0;
                for (var i = 0; i < Factura.Remesas.length; i++) {
                    subtotal += Factura.Remesas[i].Remesas.TotalFleteCliente;
                }
                for (var j = 0; j < Factura.OtrosConceptos.length; j++) {
                    if (Factura.OtrosConceptos[j].ConceptosVentas.Operacion !== 2) {
                        subtotal += Factura.OtrosConceptos[j].Valor_Concepto;
                    }
                    else {
                        descuento += Factura.OtrosConceptos[j].Valor_Concepto;
                    }
                }
                //--Calculo de totales
                //-- Obtener Token Saphety
                var TokenObj = Obtener_Token_Saphety();
                if (TokenObj.IsValid === false) {
                    throw "No se pudo generar el token de acceso: " + TokenObj.Errors[0].Code + " " + TokenObj.Errors[0].Description;
                }
                //-- Obtener Token Saphety
                //-- Obtener CUFE Saphety

                var objCufe = {
                    "IssueDate": Factura.FechaCreacion,
                    "CustomerDocumentNumber": Destinatario.NumeroIdentificacion,
                    "IssuerNitNumber": Factura.Empresas.NumeroIdentificacion,
                    //"IssuerNitNumber": 830031653,
                    "SerieTechnicalKey": Factura.OficinaFactura.ClaveTecnicaFactura,
                    "DocumentNumber": Factura.Prefijo + Factura.NumeroDocumento.toString(),
                    "TotalIva": Factura.ValorTotalIva.toString(),
                    "TotalInc": '0',
                    "TotalIca": '0',
                    "TotalGrossAmount": subtotal.toString(),
                    "TotalPayableAmount": (subtotal + Factura.ValorTotalIva - descuento).toString(),
                    "SupplierTimeZoneCode": "America/Bogota"
                };
                var CUFE = Obtener_CUFE_Saphety(objCufe, TokenObj.ResultData);
                if (CUFE.IsValid === false) {
                    throw "No se pudo generar el CUFE: " + CUFE.Errors[0].Code + " " + TokenObj.Errors[0].Description;
                }
                //console.log("cufe data: ", objCufe);
                //console.log("CUFE: ", CUFE.ResultData);
                //-- Obtener CUFE Saphety
                //-- Generar QR-code
                var UrlQrCodeAmbiente = '';
                if ($scope.Sesion.Empresa.EmpresaFacturaElectronica.Ambiente.Codigo === AMBIENTE_FACTURA_ELECTRONICA.PRUEBAS ||
                    $scope.Sesion.Empresa.EmpresaFacturaElectronica.Ambiente.Codigo === AMBIENTE_FACTURA_ELECTRONICA.HABILITACION) {
                    UrlQrCodeAmbiente = AMBIENTE_HABILITACION_DIAN_FACTURA_ELECTRONICA.replace(REPLACE_CUFE, CUFE.ResultData);
                }
                else {
                    UrlQrCodeAmbiente = AMBIENTE_PRODUCCION_DIAN_FACTURA_ELECTRONICA.replace(REPLACE_CUFE, CUFE.ResultData);
                }
                var imgQRCode = qrcodeGenerator(JsObjToString({
                    NumFac: Factura.Prefijo + Factura.NumeroDocumento.toString(),
                    FecFac: Formatear_Fecha(Factura.FechaCreacion.toString(), FORMATO_FECHA_yyyy + "-" + FORMATO_FECHA_MM + "-" + FORMATO_FECHA_dd),
                    HorFac: Formatear_Fecha(Factura.FechaCreacion.toString(), FORMATO_FECHA_HH + ":" + FORMATO_FECHA_mm + ":" + FORMATO_FECHA_ss) + "-05:00",
                    NitFac: Factura.Empresas.NumeroIdentificacion,
                    //NitFac: '830031653',
                    DocAdq: Destinatario.NumeroIdentificacion,
                    ValFac: (subtotal - Factura.ValorTotalIva).toString(),
                    ValIva: Factura.ValorTotalIva.toString(),
                    ValOtroIm: '0',
                    ValTolFac: (subtotal + Factura.ValorTotalIva - descuento).toString(),
                    CUFE: CUFE.ResultData,
                    QRCode: UrlQrCodeAmbiente

                }));
                if (imgQRCode === undefined || imgQRCode === '' || imgQRCode === null) {
                    throw "Error al generar el Código QR";
                }
                //console.log("QR Encode: ", imgQRCode);
                //-- Generar QR-code
                //-- Almacena Informacion Parcial de factura electronica(para generar Reporte Factura)
                var ResponseAlmacenaFael = FacturasFactory.GuardarFacturaElectronica({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: Factura.Numero,
                    NumeroDocumento: Factura.NumeroDocumento,
                    FacturaElectronica: {
                        CUFE: CUFE.ResultData,
                        FechaEnvio: Factura.FechaCreacion.toString(),
                        QRcode: imgQRCode.bytes
                    },
                    Sync: true
                });
                //console.log("Guardado Factura: ", ResponseAlmacenaFael);
                if (ResponseAlmacenaFael.ProcesoExitoso !== true) {
                    throw "Error al guardar factura electronica :" + ResponseAlmacenaFael.MensajeOperacion;
                }
                //-- Almacena Informacion Parcial de factura electronica(para generar Reporte Factura)
                //--Generar Reporte Factura (PDF)
                Generar_PDF_Factura_Saphety({
                    Empresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NombRepo: NOMBRE_REPORTE_FACT,
                    Numero: Factura.Numero,
                    OpcionExportarPdf: 1,
                    Prefijo: $scope.pref,
                    TipoExportar: TIPO_REPORTE_FACTURA_GENERA,
                    TipoFactura: CODIGO_CATALOGO_TIPO_FACTURA
                });
                //--Generar Reporte Factura (PDF)
                //-- Cargar Factura En Base 64
                var ResponseDocumentoReporte = FacturasFactory.ObtenerDocumentoReporteSaphety({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: Factura.Numero,
                    Sync: true
                });
                //console.log("documento reporte factura: ", ResponseDocumentoReporte);
                if (ResponseDocumentoReporte.ProcesoExitoso !== true) {
                    throw "Error al obtener documento reporte factura :" + ResponseAlmacenaFael.MensajeOperacion;
                }
                else {
                    if (ResponseDocumentoReporte.Datos.FacturaElectronica == null) {
                        throw "Error al obtener documento reporte factura: No existe reporte factura, contacte con administrador";
                    }
                }
                //-- Cargar Factura En Base 64
                //-- Generar Json Oject para el reporte de factura a saphety
                var JsonFactura = GenerarJsonFactura(Factura, ResponseDocumentoReporte.Datos.FacturaElectronica.Archivo);
                //-- Generar Json Oject para el reporte de factura a saphety
                //--Envio Factura Electronica
                var RespEnvioPruebas = null;
                var RespEnvioHabilitacion = null;
                var RespEnvioProduccion = null;
                var objArchivo = null;
                var concatError = "";
                switch ($scope.Sesion.Empresa.EmpresaFacturaElectronica.Ambiente.Codigo) {
                    case AMBIENTE_FACTURA_ELECTRONICA.PRUEBAS:
                        RespEnvioPruebas = Emision_Factura_Saphety(JsonFactura, TokenObj.ResultData);
                        if (RespEnvioPruebas.IsValid === false) {
                            console.log("Pruebas: ", RespEnvioPruebas);
                            concatError = RespEnvioPruebas.Errors[0].Field !== null ? RespEnvioPruebas.Errors[0].Field : "";
                            concatError += RespEnvioPruebas.Errors[0].Code !== null ? " " + RespEnvioPruebas.Errors[0].Code : "";
                            concatError += RespEnvioPruebas.Errors[0].Description !== null ? " " + RespEnvioPruebas.Errors[0].Description : "";
                            throw "No se pudo generar la emisión - prueba de la factura: " + concatError;
                        }
                        objArchivo = {
                            Archivo: RespEnvioPruebas.ResultData.Content,
                            TipoArchivo: RespEnvioPruebas.ResultData.ContentType,
                            DecripcionArchivo: "UBL Documento"
                        };
                        break;
                    case AMBIENTE_FACTURA_ELECTRONICA.HABILITACION:
                        RespEnvioHabilitacion = Habilitar_Factura_Saphety(JsonFactura, TokenObj.ResultData);
                        if (RespEnvioHabilitacion.IsValid === false) {
                            console.log("Habilitacion: ", RespEnvioHabilitacion);
                            concatError = RespEnvioHabilitacion.Errors[0].Field !== null ? RespEnvioHabilitacion.Errors[0].Field : "";
                            concatError += RespEnvioHabilitacion.Errors[0].Code !== null ? " " + RespEnvioHabilitacion.Errors[0].Code : "";
                            concatError += RespEnvioHabilitacion.Errors[0].Description !== null ? " " + RespEnvioHabilitacion.Errors[0].Description : "";
                            throw "No se pudo generar la habilitación de la factura: " + concatError;
                        }
                        objArchivo = {
                            Archivo: RespEnvioHabilitacion.ResultData.Content,
                            TipoArchivo: RespEnvioHabilitacion.ResultData.ContentType,
                            DecripcionArchivo: "UBL Documento"
                        };
                        break;
                    case AMBIENTE_FACTURA_ELECTRONICA.PRODUCCION:
                        RespEnvioProduccion = Emision_Factura_Saphety(JsonFactura, TokenObj.ResultData);
                        if (RespEnvioProduccion.IsValid === false) {
                            console.log("Producción: ", RespEnvioProduccion);
                            concatError = RespEnvioProduccion.Errors[0].Field !== null ? RespEnvioProduccion.Errors[0].Field : "";
                            concatError += RespEnvioProduccion.Errors[0].Code !== null ? " " + RespEnvioProduccion.Errors[0].Code : "";
                            concatError += RespEnvioProduccion.Errors[0].Description !== null ? " " + RespEnvioProduccion.Errors[0].Description : "";
                            throw "No se pudo generar la emisión - Producción de la factura: " + concatError;
                        }
                        objArchivo = {
                            Archivo: RespEnvioProduccion.ResultData.Content,
                            TipoArchivo: RespEnvioProduccion.ResultData.ContentType,
                            DecripcionArchivo: "UBL Documento"
                        };
                        break;
                }
                //--Envio Factura Electronica
                //--Guarda Informacion Final de factura enviada
                var ResponseActualizaFael = FacturasFactory.GuardarFacturaElectronica({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: Factura.Numero,
                    NumeroDocumento: Factura.NumeroDocumento,
                    FacturaElectronica: {
                        ID_Pruebas: RespEnvioPruebas === null ? '' : RespEnvioPruebas.ResultData.Id,
                        ID_Habilitacion: RespEnvioHabilitacion === null ? '' : RespEnvioHabilitacion.ResultData.Id,
                        ID_Emision: RespEnvioProduccion === null ? '' : RespEnvioProduccion.ResultData.Id,
                        Archivo: objArchivo === null ? '' : objArchivo.Archivo,
                        TipoArchivo: objArchivo === null ? '' : objArchivo.TipoArchivo,
                        DecripcionArchivo: objArchivo === null ? '' : objArchivo.DecripcionArchivo
                    },
                    EstadoFAEL: CODIGO_UNO,
                    Sync: true
                });
                if (ResponseActualizaFael.ProcesoExitoso !== true) {
                    throw "Error al guardar factura electronica :" + ResponseActualizaFael.MensajeOperacion;
                }
                //--Guarda Informacion Final de factura enviada
                ShowSuccess("Factura Electrónica Generada Exitósamente");
                blockUI.stop();
                $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 1000);
                Find();
            }
            catch (err) {
                ShowError(err);
                blockUI.stop();
                $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 1000);
            }
        }
        //-- Generar Json Factura Saphety
        function GenerarJsonFactura(Factura, PDFReporte) {
            //--Valida Destinatario
            var Destinatario = "";
            if (Factura.FacturarA.Codigo != undefined && Factura.FacturarA.Codigo != null && Factura.FacturarA.Codigo != '' && Factura.FacturarA.Codigo > 0) {
                Destinatario = Factura.FacturarA;
            }
            else {
                Destinatario = Factura.Cliente;
            }

            //--Valida Destinatario
            var subtotal = 0;
            var TotalDescuento = 0;
            var indLinea = 1;
            var JsonFactura = {};
            var i = 0;
            var j = 0;
            JsonFactura.TaxSubTotals = [];
            JsonFactura.TaxTotals = [];

            if (Factura.NumeroOrdenCompra === "") {
                JsonFactura.DocumentReferences = []; // Anexo Tecnico 1.8
            } else {
                if (Factura.NumeroOrdenCompra != 'undefined' || Factura.NumeroOrdenCompra != null) {
                    JsonFactura.DocumentReferences = [];
                }

            }

            JsonFactura.WithholdingTaxSubTotals = [];
            JsonFactura.WithholdingTaxTotals = [];
            //------Totales de retención de impuestos
            //-- obtener Ids Impuestos
            var listadoIdImpuestos = [];
            Factura.DetalleImpuestosFacturas.forEach(function (itmIm) {
                listadoIdImpuestos.push(itmIm.ImpuestoFacturas.Codigo);
            });
            //-- Agrupa Impuestos
            var Tmpimpuestos = [];
            $.each(listadoIdImpuestos, function (i, el) {
                if ($.inArray(el, Tmpimpuestos) === -1) Tmpimpuestos.push(el);
            });
            //-- Genera Listado Objeto Concepto
            var ListadoObjImpuesto = [];
            for (i = 0; i < Tmpimpuestos.length; i++) {
                var nombreImpuesto = '';
                switch (Tmpimpuestos[i]) {
                    case CODGIO_CATALOGO_IMPUESTO_IVA:
                        nombreImpuesto = NOMBRE_IMPUESTO_IVA;
                        break;
                    case CODIGO_CATALOGO_RETENCION_FUENTE:
                        nombreImpuesto = NOMBRE_RETENCION_FUENTE;
                        break;
                    case CODIGO_CATALOGO_RETENCION_ICA:
                        nombreImpuesto = NOMBRE_RETENCION_ICA;
                        break;
                }
                var imp = {
                    Codigo: Tmpimpuestos[i],
                    Nombre: nombreImpuesto,
                    Valor_impuesto: 0,
                    Valor_base: 0,
                    Valor_tarifa: 0
                };
                ListadoObjImpuesto.push(imp);
            }
            //-- Suma Valores impuestos
            for (i = 0; i < ListadoObjImpuesto.length; i++) {
                for (j = 0; j < Factura.DetalleImpuestosFacturas.length; j++) {
                    if (ListadoObjImpuesto[i].Codigo === Factura.DetalleImpuestosFacturas[j].ImpuestoFacturas.Codigo) {
                        ListadoObjImpuesto[i].Valor_impuesto += Factura.DetalleImpuestosFacturas[j].Valor_impuesto;
                        ListadoObjImpuesto[i].Valor_base += Factura.DetalleImpuestosFacturas[j].Valor_base;
                        ListadoObjImpuesto[i].Valor_tarifa = parseFloat(Factura.DetalleImpuestosFacturas[j].Valor_tarifa * 100).toFixed(3);
                    }
                }
            }
            for (i = 0; i < ListadoObjImpuesto.length; i++) {
                var impuesto = {};
                var retencion = {};
                switch (ListadoObjImpuesto[i].Codigo) {
                    case CODGIO_CATALOGO_IMPUESTO_IVA:
                        impuesto = {
                            TaxCategory: NOMBRE_IMPUESTO_IVA,
                            TaxPercentage: ListadoObjImpuesto[i].Valor_tarifa,
                            TaxableAmount: ListadoObjImpuesto[i].Valor_base.toString(),
                            TaxAmount: ListadoObjImpuesto[i].Valor_impuesto.toString()
                        };
                        JsonFactura.TaxSubTotals.push(impuesto);
                        JsonFactura.TaxTotals.push({ TaxCategory: ListadoObjImpuesto[i].Nombre, TaxAmount: ListadoObjImpuesto[i].Valor_impuesto.toString() });
                        break;
                    case CODIGO_CATALOGO_RETENCION_FUENTE:
                        retencion = {
                            WithholdingTaxCategory: NOMBRE_RETENCION_FUENTE,
                            TaxPercentage: ListadoObjImpuesto[i].Valor_tarifa,
                            TaxableAmount: ListadoObjImpuesto[i].Valor_base.toString(),
                            TaxAmount: ListadoObjImpuesto[i].Valor_impuesto.toString()
                        };
                        JsonFactura.WithholdingTaxSubTotals.push(retencion);
                        JsonFactura.WithholdingTaxTotals.push({ WithholdingTaxCategory: ListadoObjImpuesto[i].Nombre, TaxAmount: ListadoObjImpuesto[i].Valor_impuesto.toString() });
                        break;
                    case CODIGO_CATALOGO_RETENCION_ICA:
                        retencion = {
                            WithholdingTaxCategory: NOMBRE_RETENCION_ICA,
                            TaxPercentage: ListadoObjImpuesto[i].Valor_tarifa,
                            TaxableAmount: ListadoObjImpuesto[i].Valor_base.toString(),
                            TaxAmount: ListadoObjImpuesto[i].Valor_impuesto.toString()
                        };
                        JsonFactura.WithholdingTaxSubTotals.push(retencion);
                        JsonFactura.WithholdingTaxTotals.push({ WithholdingTaxCategory: ListadoObjImpuesto[i].Nombre, TaxAmount: ListadoObjImpuesto[i].Valor_impuesto.toString() });
                        break;
                }
            }
            if (JsonFactura.TaxSubTotals.length === 0) {
                JsonFactura.TaxSubTotals.push({
                    TaxCategory: NOMBRE_IMPUESTO_IVA,
                    TaxPercentage: "0",
                    TaxableAmount: "0",
                    TaxAmount: "0"
                });
            }
            if (JsonFactura.TaxTotals.length === 0) {
                JsonFactura.TaxTotals.push({ TaxCategory: NOMBRE_IMPUESTO_IVA, TaxAmount: "0" });
            }
            if (JsonFactura.WithholdingTaxSubTotals.length === 0) {
                delete JsonFactura['WithholdingTaxSubTotals'];
                delete JsonFactura['WithholdingTaxTotals'];
            }
            //-----Totales de retención de impuestos
            JsonFactura.DeliveryDate = Factura.FechaCreacion.toString();
            JsonFactura.PaymentMeans = [{
                Code: "1",//--No se especifica un medio de pago
                Mean: Factura.FormaPago.Codigo.toString(),
                DueDate: Formatear_Fecha(Factura.FechaVence.toString(), FORMATO_FECHA_yyyy + "-" + FORMATO_FECHA_MM + "-" + FORMATO_FECHA_dd)
            }];
            JsonFactura.IssuerParty = {
                DocumentContacts: [
                    {
                        Name: Factura.Empresas.RazonSocial,
                        //Name: 'EncoExpres SAS',
                        Telephone: Factura.Empresas.Telefono,
                        //Telephone: (7446586).toString(),
                        Email: Factura.Empresas.Email,
                        //Email: 'gerencia@EncoExpres.co',
                        Type: "SellerContact"
                    }
                ],
                Identification: {
                    DocumentNumber: Factura.Empresas.NumeroIdentificacion,
                    //DocumentNumber: '830031653',
                    DocumentType: Factura.Empresas.TipoNumeroIdentificacion,
                    CountryCode: "CO",
                    CheckDigit: Factura.Empresas.DigitoChequeo
                    //CheckDigit: '3'
                }
            };
            JsonFactura.CustomerParty = {
                DocumentContacts: [
                    {
                        Name: Destinatario.NombreCompleto,
                        Telephone: Destinatario.Telefonos,
                        Email: Destinatario.Correo,
                        Type: "BuyerContact"
                    }
                ],
                LegalType: Destinatario.TipoNaturaleza.Nombre,
                Identification: {
                    DocumentNumber: Destinatario.NumeroIdentificacion,
                    DocumentType: Destinatario.TipoIdentificacion.Nombre,
                    CountryCode: "CO",
                    //CheckDigit: Destinatario.DigitoChequeo.toString()
                },
                Name: Destinatario.NombreCompleto,
                Email: Destinatario.Correo,
                Address: {
                    //DepartmentName: Destinatario.CiudadDireccion.Departamento.Nombre,
                    DepartmentCode: Destinatario.Departamentos.CodigoAlterno.toString(),
                    CityCode: Destinatario.Ciudad.CodigoAlterno.toString(),
                    //CityName: Destinatario.CiudadDireccion.Nombre,
                    AddressLine: Destinatario.Direccion,
                    PostalCode: Destinatario.Ciudad.CodigoPostal.toString(),
                    Country: "CO"
                },
                //TaxScheme: "49",
                //Anexo Tecnico 1.8 18-AGO-2021
                TaxScheme: "ZZ",
                Person: {
                    firstName: Destinatario.Nombre,
                    middleName: '',
                    familyName: Destinatario.PrimeroApellido + " " + Destinatario.SegundoApellido
                },
                //ResponsabilityTypes: ["O-06", "O-07", "O-09", "O-14", "O-48", "O-99", "A-29"]
                ResponsabilityTypes: ["R-99-PN"]
            };
            if ((Destinatario.TipoIdentificacion.Nombre).toUpperCase() == 'NIT') {
                JsonFactura.CustomerParty.Identification.CheckDigit = Destinatario.DigitoChequeo.toString();
            }

            JsonFactura.Currency = "COP";
            var Lineas = [];
            //--Agrega Remesas a Lineas
            for (i = 0; i < Factura.Remesas.length; i++) {
                var DetalleRemesa = {
                    Number: indLinea.toString(),
                    Quantity: "1",
                    QuantityUnitOfMeasure: "NAR",//--Unidad de medida en el estandar
                    TaxSubTotals: [{
                        TaxCategory: NOMBRE_IMPUESTO_IVA,
                        TaxPercentage: "0",
                        TaxableAmount: "0",
                        TaxAmount: "0"
                    }],
                    TaxTotals: [{ TaxCategory: NOMBRE_IMPUESTO_IVA, TaxAmount: "0" }],
                    UnitPrice: Factura.Remesas[i].Remesas.TotalFleteCliente.toString(),
                    GrossAmount: Factura.Remesas[i].Remesas.TotalFleteCliente.toString(),
                    NetAmount: Factura.Remesas[i].Remesas.TotalFleteCliente.toString(),
                    Item: { Description: (Factura.Remesas[i].Remesas.MBLContenedor === '' ? "MBL" : Factura.Remesas[i].Remesas.MBLContenedor) + " - " + Factura.Remesas[i].Remesas.Ruta.Nombre }
                };
                subtotal += Factura.Remesas[i].Remesas.TotalFleteCliente;
                Lineas.push(DetalleRemesa);
                indLinea += 1;
            }
            //--Agrega Remesas a Lineas
            //-----Agrega Otros Conceptos a las lineas
            var SUMtaxableAmount = 0;
            for (i = 0; i < Factura.OtrosConceptos.length; i++) {
                if (Factura.OtrosConceptos[i].ConceptosVentas.Operacion !== 2) {
                    subtotal += Factura.OtrosConceptos[i].Valor_Concepto;
                    var Detalle = {
                        Number: indLinea.toString(),
                        Quantity: "1",
                        QuantityUnitOfMeasure: "NAR",//--Unidad de medida en el estandar
                        TaxSubTotals: [],
                        TaxTotals: [],
                        WithholdingTaxSubTotals: [],
                        WithholdingTaxTotals: [],
                        UnitPrice: Factura.OtrosConceptos[i].Valor_Concepto.toString(),
                        GrossAmount: Factura.OtrosConceptos[i].Valor_Concepto.toString(),
                        NetAmount: Factura.OtrosConceptos[i].Valor_Concepto.toString(),
                        Item: {
                            Description: Factura.OtrosConceptos[i].ConceptosVentas.Nombre + " Remesa " + Factura.OtrosConceptos[i].Remesas.NumeroDocumento.toString()
                        }
                    };
                    var DetalleImpuestosConceptos = Factura.OtrosConceptos[i].DetalleImpuestoConceptosFacturas;
                    var objSubIVA = { TaxCategory: '', TaxPercentage: 0, TaxableAmount: 0, TaxAmount: 0 };
                    var objSubRETEFUENTE = { WithholdingTaxCategory: '', TaxPercentage: 0, TaxableAmount: 0, TaxAmount: 0 };
                    var objSubRETEICA = angular.copy(objSubRETEFUENTE);
                    var objIVA = { TaxCategory: '', TaxAmount: 0 };
                    var objRETEFUENTE = { WithholdingTaxCategory: '', TaxAmount: 0 };
                    var objRETEICA = angular.copy(objRETEFUENTE);
                    for (j = 0; j < DetalleImpuestosConceptos.length; j++) {
                        switch (DetalleImpuestosConceptos[j].ENIM_Codigo) {
                            case CODGIO_CATALOGO_IMPUESTO_IVA:
                                objSubIVA.TaxCategory = NOMBRE_IMPUESTO_IVA;
                                objSubIVA.TaxPercentage = parseFloat(DetalleImpuestosConceptos[j].Valor_tarifa * 100).toFixed(3);
                                objSubIVA.TaxableAmount += DetalleImpuestosConceptos[j].Valor_base;
                                objSubIVA.TaxAmount += DetalleImpuestosConceptos[j].Valor_impuesto;

                                objIVA.TaxCategory = NOMBRE_IMPUESTO_IVA;
                                objIVA.TaxAmount += DetalleImpuestosConceptos[j].Valor_impuesto;
                                break;
                            case CODIGO_CATALOGO_RETENCION_FUENTE:
                                objSubRETEFUENTE.WithholdingTaxCategory = NOMBRE_RETENCION_FUENTE;
                                objSubRETEFUENTE.TaxPercentage = parseFloat(DetalleImpuestosConceptos[j].Valor_tarifa * 100).toFixed(3);
                                objSubRETEFUENTE.TaxableAmount += DetalleImpuestosConceptos[j].Valor_base;
                                objSubRETEFUENTE.TaxAmount += DetalleImpuestosConceptos[j].Valor_impuesto;

                                objRETEFUENTE.WithholdingTaxCategory = NOMBRE_RETENCION_FUENTE;
                                objRETEFUENTE.TaxAmount += DetalleImpuestosConceptos[j].Valor_impuesto;
                                break;
                            case CODIGO_CATALOGO_RETENCION_ICA:
                                objSubRETEICA.WithholdingTaxCategory = NOMBRE_RETENCION_ICA;
                                objSubRETEICA.TaxPercentage = parseFloat(DetalleImpuestosConceptos[j].Valor_tarifa * 100).toFixed(3);
                                objSubRETEICA.TaxableAmount += DetalleImpuestosConceptos[j].Valor_base;
                                objSubRETEICA.TaxAmount += DetalleImpuestosConceptos[j].Valor_impuesto;

                                objRETEICA.WithholdingTaxCategory = NOMBRE_RETENCION_ICA;
                                objRETEICA.TaxAmount += DetalleImpuestosConceptos[j].Valor_impuesto;
                                break;
                        }
                    }

                    if (objSubIVA.TaxAmount > 0) {
                        SUMtaxableAmount += objSubIVA.TaxableAmount;
                        objSubIVA.TaxableAmount = objSubIVA.TaxableAmount.toString();
                        objSubIVA.TaxAmount = objSubIVA.TaxAmount.toString();
                        objIVA.TaxAmount = objIVA.TaxAmount.toString();
                        Detalle.TaxSubTotals.push(objSubIVA);
                        Detalle.TaxTotals.push(objIVA);
                    }
                    else {
                        Detalle.TaxSubTotals.push({ TaxCategory: NOMBRE_IMPUESTO_IVA, TaxPercentage: "0", TaxableAmount: "0", TaxAmount: "0" });
                        Detalle.TaxTotals.push({ TaxCategory: NOMBRE_IMPUESTO_IVA, TaxAmount: "0" });
                    }

                    if (objSubRETEFUENTE.TaxAmount > 0) {
                        objSubRETEFUENTE.TaxableAmount = objSubRETEFUENTE.TaxableAmount.toString();
                        objSubRETEFUENTE.TaxAmount = objSubRETEFUENTE.TaxAmount.toString();
                        objRETEFUENTE.TaxAmount = objRETEFUENTE.TaxAmount.toString();
                        Detalle.WithholdingTaxSubTotals.push(objSubRETEFUENTE);
                        Detalle.WithholdingTaxTotals.push(objRETEFUENTE);
                    }

                    if (objSubRETEICA.TaxAmount > 0) {
                        objSubRETEICA.TaxableAmount = objSubRETEICA.TaxableAmount.toString();
                        objSubRETEICA.TaxAmount = objSubRETEICA.TaxAmount.toString();
                        objRETEICA.TaxAmount = objRETEICA.TaxAmount.toString();
                        Detalle.WithholdingTaxSubTotals.push(objSubRETEICA);
                        Detalle.WithholdingTaxTotals.push(objRETEICA);
                    }

                    if (Detalle.WithholdingTaxSubTotals.length === 0) {
                        delete Detalle['WithholdingTaxSubTotals'];
                        delete Detalle['WithholdingTaxTotals'];
                    }

                    Lineas.push(Detalle);
                    indLinea += 1;
                }
                else {
                    TotalDescuento += Factura.OtrosConceptos[i].Valor_Concepto;
                }
            }
            JsonFactura.Lines = Lineas;
            //-----Agrega Otros Conceptos a las lineas
            //----Totales
            JsonFactura.Total = {
                GrossAmount: subtotal.toString(),
                TotalBillableAmount: (subtotal + Factura.ValorTotalIva).toString(),
                PayableAmount: (subtotal + Factura.ValorTotalIva - TotalDescuento).toString(),
                TaxableAmount: SUMtaxableAmount.toString()
            };
            if (TotalDescuento > 0) {
                JsonFactura.Total.AllowancesTotalAmount = TotalDescuento.toString();
            }
            /*    console.log('factura', Factura)*/
            var OrderReference = 'OrderReference'

            if (Factura.NumeroOrdenCompra != 'undefined' & Factura.NumeroOrdenCompra != null) {
                JsonFactura.DocumentReferences.push({
                    //$id: "15",
                    DocumentReferred: Factura.NumeroOrdenCompra,
                    IssueDate: Factura.Fecha,
                    Type: OrderReference
                });
            }

            //----Totales
            //--Fecha Emision
            JsonFactura.IssueDate = Factura.FechaCreacion.toString();
            //--Fecha Emision
            //--Fecha Vencimiento
            JsonFactura.DueDate = Factura.FechaVence.toString();
            //--Fecha Vencimiento
            //--Prefijo
            JsonFactura.SeriePrefix = Factura.Prefijo;
            //--Prefijo
            //--Numero Factura
            JsonFactura.SerieNumber = Factura.NumeroDocumento.toString();
            //--Numero Factura
            //--Tipo Operacion
            JsonFactura.OperationType = "10";//Estandar
            //--Tipo Operacion
            //--Modo Envio
            JsonFactura.IssueMode = TIPO_REPORTE_FACTURA_GENERA;
            //--Modo Envio
            //--Numero de documento
            JsonFactura.CorrelationDocumentId = "FV" + Factura.Prefijo + Factura.NumeroDocumento.toString() + "-" + new Date(Factura.FechaCreacion).getTime();
            //--Numero de documento
            //--llave externa
            JsonFactura.SerieExternalKey = Factura.OficinaFactura.ClaveExternaFactura;
            //--llave externa
            //--Descuentos
            var Descuentos = [];
            var AcuDesc = 1;
            for (i = 0; i < Factura.OtrosConceptos.length; i++) {
                var porcentaje = (Factura.OtrosConceptos[i].Valor_Concepto * 100) / subtotal;
                if (Factura.OtrosConceptos[i].ConceptosVentas.Operacion === 2) {
                    var objdesc = {
                        ChargeIndicator: "false",
                        BaseAmount: subtotal.toString(),
                        ReasonCode: "00", // Anexo Tecnico 1.8 18-AGO-2021 cambia el valor 11 por 00
                        Reason: Factura.OtrosConceptos[i].ConceptosVentas.Nombre + " Remesa " + Factura.OtrosConceptos[i].Remesas.NumeroDocumento.toString(),
                        Amount: Factura.OtrosConceptos[i].Valor_Concepto.toString(),
                        Percentage: porcentaje.toFixed(3),
                        SequenceIndicator: AcuDesc.toString()
                    };
                    Descuentos.push(objdesc);
                    AcuDesc += 1;
                }
            }
            if (Descuentos.length > 0) {
                JsonFactura.AllowanceCharges = Descuentos;
            }
            //--Descuentos
            //--Pdf Reporte
            JsonFactura.PdfData = { Pdf: PDFReporte };
            //--Pdf Reporte
            //console.log("factura: ", JSON.stringify(JsonFactura));
            return JsonFactura;
        }
        //-- Generar Json Factura Saphety
        //-- Ejecuta servicio de Obtener token por jquery
        function Obtener_Token_Saphety() {
            var JQDataSend = $.ajax({
                type: "POST",
                url: ObtenerUrlApiSaphety() + API_GET_TOKEN,
                headers: {
                    "Accept": "application / json",
                    'Content-Type': 'application/json'
                },
                dataType: 'json',
                async: false,
                data: JSON.stringify({
                    username: $scope.Sesion.Empresa.EmpresaFacturaElectronica.Usuario,
                    password: $scope.Sesion.Empresa.EmpresaFacturaElectronica.Clave,
                    virtual_operator: $scope.Sesion.Empresa.EmpresaFacturaElectronica.OperadorVirtual
                })
            });
            return JSON.parse(JQDataSend.responseText);
        }
        //-- Ejecuta servicio de Obtener token por jquery
        //-- Ejecuta servicio de Obtener CUFE por jquery
        function Obtener_CUFE_Saphety(configCUFE, TOKEN) {
            var JQDataSend = $.ajax({
                type: "POST",
                url: ObtenerUrlApiSaphety() + API_GET_CUFE.replace(REPLACE_VIRTUAL_OPERATOR, $scope.Sesion.Empresa.EmpresaFacturaElectronica.OperadorVirtual),
                crossDomain: true,
                headers: {
                    "Authorization": TOKEN.token_type + " " + TOKEN.access_token,
                    "Content-Type": "application/json"
                },
                processData: false,
                async: false,
                dataType: 'json',
                data: JSON.stringify(configCUFE),
            });
            return JSON.parse(JQDataSend.responseText);
        }
        //-- Ejecuta servicio de Obtener CUFE por jquery
        //-- Genera Documento PDF Factura Electronica
        function Generar_PDF_Factura_Saphety(configFacturaObj) {
            var ResuDataObje = {
                dataResult: '',
                dataError: ''
            };
            var JQDataSend = $.ajax({
                type: "GET",
                url: $scope.urlASP + '/Reportes/Reportes.aspx',
                async: false,
                data: $.param(configFacturaObj),
                success: function (data) {
                    ResuDataObje.dataResult = true;
                },
                error: function (data) {
                    ResuDataObje.dataResult = false;
                    ResuDataObje.dataError = { error: true, descript: "Error " + data.status };
                }
            });
            return ResuDataObje;
        }
        //-- Genera Documento PDF Factura Electronica
        //-- Ejecuta servicio de Habilitacion Factura Electronica por jquery
        function Habilitar_Factura_Saphety(jsonfactura, TOKEN) {
            var JQDataSend = $.ajax({
                type: "POST",
                url: ObtenerUrlApiSaphety() + API_POST_HABILITA_FACTURA.replace(REPLACE_VIRTUAL_OPERATOR, $scope.Sesion.Empresa.EmpresaFacturaElectronica.OperadorVirtual),
                crossDomain: true,
                headers: {
                    "Authorization": TOKEN.token_type + " " + TOKEN.access_token,
                    "Content-Type": "application/json"
                },
                processData: false,
                async: false,
                dataType: 'json',
                data: JSON.stringify(jsonfactura),
            });
            return JSON.parse(JQDataSend.responseText);
        }
        //-- Ejecuta servicio de Habilitacion Factura Electronica por jquery
        //-- Ejecuta servicio de Emision Factura Electronica por jquery
        function Emision_Factura_Saphety(jsonfactura, TOKEN) {
            var JQDataSend = $.ajax({
                type: "POST",
                url: ObtenerUrlApiSaphety() + API_POST_EMISION_FACTURA.replace(REPLACE_VIRTUAL_OPERATOR, $scope.Sesion.Empresa.EmpresaFacturaElectronica.OperadorVirtual),
                crossDomain: true,
                headers: {
                    "Authorization": TOKEN.token_type + " " + TOKEN.access_token,
                    "Content-Type": "application/json"
                },
                processData: false,
                async: false,
                dataType: 'json',
                data: JSON.stringify(jsonfactura),
            });
            return JSON.parse(JQDataSend.responseText);
        }
        //-- Ejecuta servicio de Habilitacion Factura Electronica por jquery
        function ObtenerUrlApiSaphety() {
            var url = "";
            switch ($scope.Sesion.Empresa.EmpresaFacturaElectronica.Ambiente.Codigo) {
                case AMBIENTE_FACTURA_ELECTRONICA.PRUEBAS:
                case AMBIENTE_FACTURA_ELECTRONICA.HABILITACION:
                    url = GEN_URL_SAPHETY;
                    break;
                case AMBIENTE_FACTURA_ELECTRONICA.PRODUCCION:
                    url = GEN_URL_SAPHETY_PRODUCCION;
                    break;
            }
            return url;
        }
        //-- Generar Archivo Factura
        $scope.GenerarArchivosFactura = function (Factura) {

            switch ($scope.Sesion.Empresa.EmpresaFacturaElectronica.Proveedor.Codigo) {
                case PROVEEDOR_FACTURA_ELECTRONICA.SAPHETY:
                    var Response = FacturasFactory.ObtenerDatosFacturaElectronicaSaphety({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Numero: Factura.Numero,
                        Sync: true
                    });
                    //Bloqueo Pantalla
                    blockUIConfig.autoBlock = true;
                    blockUIConfig.delay = 0;
                    blockUI.start("Generando Archivo Factura...");
                    $timeout(function () {
                        blockUI.message("Generando Archivo Factura...");
                        Gestionar_Archivo_Factura_SAPHETY(Response.Datos);
                    }, 100);
                    //Bloqueo Pantalla
                    break;
                default:
                    ShowError("No se ha asignado proveerdor electrónico");
                    break;
            }
        };

        function Gestionar_Archivo_Factura_SAPHETY(Factura) {
            try {
                //--Generar Reporte Factura (PDF)
                Generar_PDF_Factura_Saphety({
                    Empresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NombRepo: NOMBRE_REPORTE_FACT,
                    Numero: Factura.Numero,
                    OpcionExportarPdf: 1,
                    Prefijo: $scope.pref,
                    TipoExportar: TIPO_REPORTE_FACTURA_GENERA,
                    TipoFactura: CODIGO_CATALOGO_TIPO_FACTURA
                });
                //--Generar Reporte Factura (PDF)
                //-- Cargar Factura En Base 64
                var ResponseDocumentoReporte = FacturasFactory.ObtenerDocumentoReporteSaphety({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: Factura.Numero,
                    Sync: true
                });
                if (ResponseDocumentoReporte.ProcesoExitoso !== true) {
                    throw "Error al obtener documento reporte factura :" + ResponseAlmacenaFael.MensajeOperacion;
                }
                else {
                    if (ResponseDocumentoReporte.Datos.FacturaElectronica == null) {
                        throw "Error al obtener documento reporte factura: No existe reporte factura, contacte con administrador";
                    }
                }
                //-- Cargar Factura En Base 64
                //-- Generar Json Oject para el reporte de factura a saphety
                showModal('modalArchivoFactura');
                var JsonFactura = GenerarJsonFactura(Factura, ResponseDocumentoReporte.Datos.FacturaElectronica.Archivo);
                $scope.ArchivoFactura = JSON.stringify(JsonFactura, null, 2);
                blockUI.stop();
                $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 1000);
                //-- Generar Json Oject para el reporte de factura a saphety
            }
            catch (err) {
                ShowError(err);
                blockUI.stop();
                $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 1000);
            }
        }
        //-- Generar Archivo Factura
        //--Estados Factura Electronica
        $scope.EstadosFacturaElectronica = function (Factura) {
            if (ValidarProcesoEstadosFacturaElectronica(Factura)) {
                switch ($scope.Sesion.Empresa.EmpresaFacturaElectronica.Proveedor.Codigo) {
                    case PROVEEDOR_FACTURA_ELECTRONICA.SAPHETY:
                        var Response = FacturasFactory.ObtenerDatosFacturaElectronicaSaphety({
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Numero: Factura.Numero,
                            Sync: true
                        });
                        //Bloqueo Pantalla
                        blockUIConfig.autoBlock = true;
                        blockUIConfig.delay = 0;
                        blockUI.start("Estados Factura...");
                        $timeout(function () {
                            blockUI.message("Estados Factura...");
                            Gestionar_Estados_Factura_Electronica_SAPHETY(Response.Datos);
                        }, 100);
                        //Bloqueo Pantalla
                        break;
                    default:
                        ShowError("No se ha asignado proveerdor electrónico");
                        break;
                }
            }
        };

        function ValidarProcesoEstadosFacturaElectronica(factura) {
            var continuar = true;
            var txtErrores = "";
            if (factura.Estado === ESTADO_BORRADOR) {
                continuar = false;
                txtErrores += "</br>- La Factura debe estar en estado definitivo";
            }
            if (factura.EstadoFAEL === ESTADO_INACTIVO) {
                continuar = false;
                txtErrores += "</br>- La Factura debe estar emitida electrónicamente";
            }
            if (txtErrores !== "") {
                ShowError("Error al reportar la Factura: " + txtErrores);
            }
            return continuar;
        }

        function Gestionar_Estados_Factura_Electronica_SAPHETY(Factura) {
            var IdDocumento = "";
            if (Factura.FacturaElectronica.ID_Pruebas != "") {
                IdDocumento = Factura.FacturaElectronica.ID_Pruebas;
            }
            else if (Factura.FacturaElectronica.ID_Habilitacion != "") {
                IdDocumento = Factura.FacturaElectronica.ID_Habilitacion;
            }
            else {
                IdDocumento = Factura.FacturaElectronica.ID_Emision;
            }

            try {
                if (IdDocumento != "") {
                    //-- Obtener Token Saphety
                    var TokenObj = Obtener_Token_Saphety();
                    if (TokenObj.IsValid === false) {
                        throw "No se pudo generar el token de acceso: " + TokenObj.Errors[0].Code + " " + TokenObj.Errors[0].Description;
                    }
                    //-- Obtener Token Saphety
                    var ObjEstado = Obtener_Estados_Saphety(IdDocumento, TokenObj.ResultData);
                    if (ObjEstado.IsValid === false) {
                        throw "No se encontro el estado de la factura";
                    }
                    else {
                        for (var i = 0; i < $scope.ListadoEstadosFael.length; i++) {
                            if ($scope.ListadoEstadosFael[i].CampoAuxiliar2 == ObjEstado.ResultData.DocumentStatus) {
                                $scope.Estados.Adquiente = $scope.ListadoEstadosFael[i];
                            }
                            if ($scope.ListadoEstadosFael[i].CampoAuxiliar2 == ObjEstado.ResultData.MainEmailNotificationStatus) {
                                $scope.Estados.Notificacion = $scope.ListadoEstadosFael[i];
                            }
                        }
                        if ($scope.Estados.Adquiente == '') {
                            $scope.Estados.Adquiente = $scope.ListadoEstadosFael[$scope.ListadoEstadosFael.length - 1];
                        }

                        if ($scope.Estados.Notificacion == '') {
                            $scope.Estados.Notificacion = $scope.ListadoEstadosFael[$scope.ListadoEstadosFael.length - 1];
                        }
                        $scope.Estados.Fecha = new Date(ObjEstado.ResultData.DocumentStatusDate);
                        showModal('modalEstadosFacturaElectronica');
                    }

                    blockUI.stop();
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 1000);
                }
                else {
                    throw "No se encontro el Id del documento de la factura";
                }

            } catch (err) {
                if (err instanceof SyntaxError) {
                    ShowError("El Id del documento no es válido");
                }
                else {
                    ShowError(err);
                }
                blockUI.stop();
                $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 1000);
            }
        }

        function Obtener_Estados_Saphety(Id_Dodumento, TOKEN) {
            var JQDataSend = $.ajax({
                type: "GET",
                url: ObtenerUrlApiSaphety() + API_GET_ESTADOS.replace(REPLACE_VIRTUAL_OPERATOR, $scope.Sesion.Empresa.EmpresaFacturaElectronica.OperadorVirtual) + Id_Dodumento,
                crossDomain: true,
                headers: {
                    "Authorization": TOKEN.token_type + " " + TOKEN.access_token,
                    "Content-Type": "application/json"
                },
                processData: false,
                async: false
            });
            return JSON.parse(JQDataSend.responseText);
        }
        //--Estados Factura Electronica
        //---------------------------------------------------------------------Proceso Factura Electronica----------------------------------------------------------------------------//
    }]);
