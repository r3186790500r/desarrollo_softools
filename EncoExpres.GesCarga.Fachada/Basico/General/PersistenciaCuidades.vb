﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Repositorio.Basico.General

Namespace Basico.General
    Public NotInheritable Class PersistenciaCiudades
        Implements IPersistenciaBase(Of Ciudades)

        Public Function Consultar(filtro As Ciudades) As IEnumerable(Of Ciudades) Implements IPersistenciaBase(Of Ciudades).Consultar
            Return New RepositorioCiudades().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As Ciudades) As Long Implements IPersistenciaBase(Of Ciudades).Insertar
            Return New RepositorioCiudades().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As Ciudades) As Long Implements IPersistenciaBase(Of Ciudades).Modificar
            Return New RepositorioCiudades().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As Ciudades) As Ciudades Implements IPersistenciaBase(Of Ciudades).Obtener
            Return New RepositorioCiudades().Obtener(filtro)
        End Function
        Public Function Anular(entidad As Ciudades) As Boolean
            Return New RepositorioCiudades().Anular(entidad)
        End Function

        Public Function ConsultarCodigoDane(entidad As CodigoDaneCiudades) As IEnumerable(Of CodigoDaneCiudades)
            Return New RepositorioCiudades().ConsultarCodigoDane(entidad)
        End Function
        Public Function ConsultarOficinas(filtro As Ciudades) As IEnumerable(Of Oficinas)
            Return New RepositorioCiudades().ConsultarOficinas(filtro)
        End Function

        Public Function GuardarOficinas(entidad As Ciudades)
            Return New RepositorioCiudades().GuardarOficinas(entidad)
        End Function

        Public Function ConsultarRegiones(filtro As Ciudades) As IEnumerable(Of RegionesPaises)
            Return New RepositorioCiudades().ConsultarRegiones(filtro)
        End Function
        Public Function ConsultarAlterno(filtro As Ciudades) As IEnumerable(Of Ciudades)
            Return New RepositorioCiudades().ConsultarAlterno(filtro)
        End Function
        Public Function GuardarRegiones(entidad As Ciudades)
            Return New RepositorioCiudades().GuardarRegiones(entidad)
        End Function

    End Class

End Namespace

