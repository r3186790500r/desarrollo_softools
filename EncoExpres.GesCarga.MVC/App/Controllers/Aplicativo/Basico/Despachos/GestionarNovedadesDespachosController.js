﻿EncoExpresApp.controller("GestionarNovedadesDespachosCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'NovedadesDespachosFactory', 'ConceptoVentasFactory', 'ConceptoLiquidacionFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, NovedadesDespachosFactory, ConceptoVentasFactory, ConceptoLiquidacionFactory) {

        $scope.Titulo = 'GESTIONAR NOVEDADES DESPACHOS';
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'General' }, { Nombre: 'Novedades Despachos' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_NOVEDADES_DESPACHOS);

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_NOVEDADES_DESPACHOS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.ListaFacturacion = [];
        $scope.ListaLiquidacionPlanilla = [];

        $scope.ListadoEstados = [
            { Codigo: 0, Nombre: "INACTIVO" },
            { Codigo: 1, Nombre: "ACTIVO" },
        ]
        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');

        /* Obtener parametros*/
        if ($routeParams.Codigo > 0) {
            $scope.Codigo = $routeParams.Codigo;
            Obtener();
        }
        else {
            $scope.Codigo = 0;          
        }

        /*Cargar el autocomplete de facturación*/
        ConceptoVentasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, ConceptoSistema: -1, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaFacturacion = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListaFacturacion = response.data.Datos
                        if ($scope.CodigoFacturacion !== undefined && $scope.CodigoFacturacion !== '' && $scope.CodigoFacturacion !== null) {
                            $scope.ModeloFacturacion = $linq.Enumerable().From($scope.ListaFacturacion).First('$.Codigo ==' + $scope.CodigoFacturacion);
                        } else {
                            $scope.ModeloFacturacion = '';
                        }
                    } else {
                        $scope.ListaFacturacion = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar el autocomplete de liquidacion planilla*/
        ConceptoLiquidacionFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ConceptoSistema: -1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaLiquidacionPlanilla = [];
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.forEach(function (item) {
                            if (item.ConceptoSistema != 1) {
                                $scope.ListaLiquidacionPlanilla.push(item)
                            }                           
                        });
                        if ($scope.CodigoLiquidacionPlanilla !== undefined && $scope.CodigoLiquidacionPlanilla !== '' && $scope.CodigoLiquidacionPlanilla !== null) {
                            $scope.ModeloLiquidacionPlanilla = $linq.Enumerable().From($scope.ListaLiquidacionPlanilla).First('$.Codigo ==' + $scope.CodigoLiquidacionPlanilla);
                        } else {
                            $scope.ModeloLiquidacionPlanilla = '';
                        }
                    } else {
                        $scope.ListaLiquidacionPlanilla = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando novedad despacho código No. ' + $scope.Codigo);

            $timeout(function () {
                blockUI.message('Cargando novedad despacho Código No.' + $scope.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
            };

            blockUI.delay = 1000;
            NovedadesDespachosFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        $scope.Codigo = response.data.Datos.Codigo;
                        $scope.ModeloCodigoAlterno = response.data.Datos.CodigoAlterno;
                        $scope.ModeloNombre = response.data.Datos.Nombre;                       
                        
                        $scope.CodigoFacturacion = response.data.Datos.Conceptos.COVECodigo
                        if ($scope.ListaFacturacion.length > 0 && $scope.CodigoFacturacion > 0) {
                            $scope.ModeloFacturacion = $linq.Enumerable().From($scope.ListaFacturacion).First('$.Codigo ==' + response.data.Datos.Conceptos.COVECodigo)
                        }

                        $scope.CodigoLiquidacionPlanilla = response.data.Datos.Conceptos.CLPDCodigo
                        if ($scope.ListaLiquidacionPlanilla.length > 0 && $scope.CodigoLiquidacionPlanilla > 0) {
                            $scope.ModeloLiquidacionPlanilla = $linq.Enumerable().From($scope.ListaLiquidacionPlanilla).First('$.Codigo ==' + response.data.Datos.Conceptos.CLPDCodigo)
                        }

                        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado)

                    } else {
                        ShowError('No se logro consultar la novedad despacho código ' + $scope.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarNovedadesDespachos';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarNovedadesDespachos';
                });

            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        $scope.ConfirmacionGuardar = function () {
            if (DatosRequeridos()) {
                showModal('modalConfirmacionGuardar');
            }
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');

            if ($scope.ModeloFacturacion === "") {
                $scope.ModeloFacturacion = { Codigo: 0 };
            }
            if ($scope.ModeloLiquidacionPlanilla === "") {
                $scope.ModeloLiquidacionPlanilla = { Codigo: 0 };
            }

            $scope.ListaConceptos = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NODECodigo: $scope.Codigo,
                COVECodigo: $scope.ModeloFacturacion == undefined ? 0 :$scope.ModeloFacturacion.Codigo,
                CLPDCodigo: $scope.ModeloLiquidacionPlanilla == undefined ? 0: $scope.ModeloLiquidacionPlanilla.Codigo,
            };

            $scope.DatosGuardar = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
                CodigoAlterno: $scope.ModeloCodigoAlterno,
                Nombre: $scope.ModeloNombre,
                Estado: $scope.ModalEstado.Codigo,
                Conceptos: $scope.ListaConceptos,
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            }

            NovedadesDespachosFactory.Guardar($scope.DatosGuardar).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Codigo === 0) {
                                ShowSuccess('Se guardó la novedad despacho "' + $scope.ModeloNombre + '"');
                            }
                            else {
                                ShowSuccess('Se Modificó la novedad despacho "' + $scope.ModeloNombre + '"');
                            }
                            location.href = '#!ConsultarNovedadesDespachos/' + response.data.Datos;
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }
                    else {
                        ShowError(response.statusText);
                    }
                }, function (response) {
                    if (response.statusText.includes('IX_Novedades_Despacho_Nombre')) {
                        ShowError('No se pudo guardar, el nombre ingresado ya está siendo utilizado')
                    } else {
                        ShowError(response.statusText);
                    }
                });
        };

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if ($scope.ModeloNombre === undefined || $scope.ModeloNombre === '' || $scope.ModeloNombre === null) {
                $scope.MensajesError.push('Debe ingresar el nombre de la novedad despacho');
                continuar = false;
            }
            if ($scope.ModalEstado.Nombre === undefined || $scope.ModalEstado.Nombre  === '' || $scope.ModalEstado.Nombre === null) {
                $scope.MensajesError.push('Debe ingresar el estado de la novedad despacho');
                continuar = false;
            }      
            return continuar;
        }

        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarNovedadesDespachos/' + $scope.Codigo;
        };
        $scope.MaskMayus = function () {
            try { $scope.ModeloNombre = $scope.ModeloNombre.toUpperCase() } catch (e) { }
        };
        $scope.MaskNumero = function (option) {
            try { $scope.ModeloCodigoAlterno = MascaraNumero($scope.ModeloCodigoAlterno) } catch (e) { }
        };
    }]);