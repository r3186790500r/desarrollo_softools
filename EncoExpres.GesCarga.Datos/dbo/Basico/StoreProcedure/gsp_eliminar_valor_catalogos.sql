﻿PRINT 'gsp_eliminar_valor_catalogos' 
GO
DROP PROCEDURE gsp_eliminar_valor_catalogos
GO
CREATE PROCEDURE gsp_eliminar_valor_catalogos(
  @par_EMPR_Codigo SMALLINT,
  @par_Codigo NUMERIC,
  @par_CATA_Codigo NUMERIC
  )
AS
BEGIN
  
  DELETE Valor_Catalogos 
  WHERE EMPR_Codigo = @par_EMPR_Codigo 
    AND Codigo = @par_Codigo
    AND CATA_Codigo = @par_CATA_Codigo

    SELECT @@ROWCOUNT AS Numero

END

GO