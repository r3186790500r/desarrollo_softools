﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios
Imports EncoExpres.GesCarga.Repositorio.SeguridadUsuarios

Namespace SeguridadUsuarios
    Public NotInheritable Class PersistenciaModuloAplicaciones
        Implements IPersistenciaBase(Of ModuloAplicaciones)
        Public Function Consultar(filtro As ModuloAplicaciones) As IEnumerable(Of ModuloAplicaciones) Implements IPersistenciaBase(Of ModuloAplicaciones).Consultar
            Return New RepositorioModuloAplicaciones().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As ModuloAplicaciones) As Long Implements IPersistenciaBase(Of ModuloAplicaciones).Insertar
            Return New RepositorioModuloAplicaciones().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As ModuloAplicaciones) As Long Implements IPersistenciaBase(Of ModuloAplicaciones).Modificar
            Return New RepositorioModuloAplicaciones().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As ModuloAplicaciones) As ModuloAplicaciones Implements IPersistenciaBase(Of ModuloAplicaciones).Obtener
            Return New RepositorioModuloAplicaciones().Obtener(filtro)
        End Function
        Public Function ConsultarModuloPorUsuario(codigoEmpresa As Short, codigoUsuario As Short) As IEnumerable(Of ModuloAplicaciones)
            Return New RepositorioModuloAplicaciones().ConsultarModuloPorUsuario(codigoEmpresa, codigoUsuario)
        End Function

    End Class
End Namespace
