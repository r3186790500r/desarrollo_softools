﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 30/09/2018
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	
PRINT 'sp_anular_encabezado_comprobante_contables'
GO
DROP PROCEDURE sp_anular_encabezado_comprobante_contables
GO  
CREATE PROCEDURE sp_anular_encabezado_comprobante_contables( 
  @par_EMPR_Codigo SMALLINT,  
  @par_TIDO_Codigo SMALLINT,  
  @par_Numero NUMERIC,  
  @par_USUA_Codigo_Anula SMALLINT,  
  @par_Causa_Anulacion VARCHAR (150),  
  @par_OFIC_Codigo_Anula SMALLINT  
  )  
AS  
BEGIN  
    
  UPDATE  Encabezado_Comprobante_Contables  SET  
    Fecha_Anula = GETDATE(),  
    Anulado = 1,  
    CATA_ESIC_Codigo = 13004,   
    USUA_Codigo_Anula = @par_USUA_Codigo_Anula,  
    Causa_Anulacion = @par_Causa_Anulacion  
  
  WHERE EMPR_Codigo = @par_EMPR_Codigo   
    AND Numero = @par_Numero  
  
 SELECT @@ROWCOUNT AS Numero  
   
END  
  
GO