﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.Almacen
Imports EncoExpres.GesCarga.Negocio.Basico.Almacen

''' <summary>
''' Controlador <see cref="UnidadEmpaqueReferenciasController"/>
''' </summary>
<Authorize>
Public Class UnidadEmpaqueReferenciasController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As UnidadEmpaqueReferencias) As Respuesta(Of IEnumerable(Of UnidadEmpaqueReferencias))
        Return New LogicaUnidadEmpaqueReferencias(CapaPersistenciaUnidadEmpaqueReferencias).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As UnidadEmpaqueReferencias) As Respuesta(Of UnidadEmpaqueReferencias)
        Return New LogicaUnidadEmpaqueReferencias(CapaPersistenciaUnidadEmpaqueReferencias).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As UnidadEmpaqueReferencias) As Respuesta(Of Long)
        Return New LogicaUnidadEmpaqueReferencias(CapaPersistenciaUnidadEmpaqueReferencias).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As UnidadEmpaqueReferencias) As Respuesta(Of Boolean)
        Return New LogicaUnidadEmpaqueReferencias(CapaPersistenciaUnidadEmpaqueReferencias).Anular(entidad)
    End Function

End Class