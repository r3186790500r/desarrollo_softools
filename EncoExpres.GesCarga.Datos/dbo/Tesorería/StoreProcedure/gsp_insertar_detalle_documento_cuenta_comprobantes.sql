﻿PRINT  'gsp_insertar_detalle_documento_cuenta_comprobantes'
GO
DROP PROCEDURE gsp_insertar_detalle_documento_cuenta_comprobantes
GO
CREATE PROCEDURE gsp_insertar_detalle_documento_cuenta_comprobantes
(
@par_EMPR_Codigo SMALLINT,
@par_ENDC_Codigo NUMERIC,
@par_EDCO_Codigo NUMERIC,
@par_Valor_Pago_Recaudo MONEY,
@par_Fecha_Anula DATETIME = NULL,
@par_Causa_Anulacion VARCHAR (150) =NULL
)
AS 
BEGIN
INSERT INTO Detalle_Documento_Cuenta_Comprobantes
(
EMPR_Codigo,
ENDC_Codigo,
EDCO_Codigo,
Valor_Pago_Recaudo,
Anulado,
Fecha_Anula,
Causa_Anulacion
)
VALUES 
(
@par_EMPR_Codigo,
@par_ENDC_Codigo,
@par_EDCO_Codigo,
@par_Valor_Pago_Recaudo,
0,
ISNULL(@par_Fecha_Anula,'1900/01/01'),
ISNULL(@par_Causa_Anulacion,'')
)

SELECT @@IDENTITY AS Codigo


END
GO