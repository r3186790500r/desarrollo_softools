﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Comercial.Documentos
Imports EncoExpres.GesCarga.Entidades.Despachos.Planilla
Imports EncoExpres.GesCarga.Entidades.Tesoreria
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios
Imports EncoExpres.GesCarga.Entidades.Despachos.Remesa
Imports EncoExpres.GesCarga.Entidades.Despachos

Namespace Paqueteria
    <JsonObject>
    Public Class PlanillaIndicadores
        Inherits Planillas
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="LineaNegocioTransportes"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Numero = Read(lector, "Numero")
            NumeroDocumento = Read(lector, "Numero_Documento")
            ValorFleteTransportador = Read(lector, "Valor_Flete_Transportador")
            ValorAnticipo = Read(lector, "Valor_Anticipo")
            CantidadNovedades = Read(lector, "Novedades")
            Oficina = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo"), .Nombre = Read(lector, "Oficina")}
            Conductor = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Conductor"), .Nombre = Read(lector, "Conductor")}
            Tenedor = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Tenedor"), .Nombre = Read(lector, "Tenedor")}
            Cliente = New Tercero With {.Nombre = Read(lector, "Cliente")}
            Remesa = New Remesas With {.NumeroDocumento = Read(lector, "Numero_Remesa"), .CantidadCliente = Read(lector, "Cantidad_Cliente"), .PesoCliente = Read(lector, "Peso_Cliente"), .ValorFleteCliente = Read(lector, "Valor_Flete_Cliente")}
            Producto = New ProductoTransportados With {.Codigo = Read(lector, "PRTR_Codigo"), .Nombre = Read(lector, "Producto")}
            Nombre = Read(lector, "Nombre")
            TarifaTransportes = New TarifaTransportes With {.Codigo = Read(lector, "TATC_Codigo_Compra")}

            If Read(lector, "Obtener") = 1 Then
                TipoDocumento = Read(lector, "TIDO_Codigo")
                FechaHoraSalida = Read(lector, "Fecha_Hora_Salida")
                Fecha = Read(lector, "Fecha")
                LineaNegocioTransportes = New LineaNegocioTransportes With {.Codigo = Read(lector, "LNTC_Codigo_Compra")}
                TipoLineaNegocioTransportes = New TipoLineaNegocioTransportes With {.Codigo = Read(lector, "TLNC_Codigo_Compra")}
                Ruta = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo"), .Nombre = Read(lector, "NombreRuta")}
                TarifaTransportes = New TarifaTransportes With {.Codigo = Read(lector, "TATC_Codigo_Compra")}
                TipoTarifaTransportes = New TipoTarifaTransportes With {.Codigo = Read(lector, "TTTC_Codigo_Compra")}
                NumeroTarifarioCompra = Read(lector, "ETCC_Numero")

                Tenedor = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Tenedor"), .Nombre = Read(lector, "Tenedor")}
                Peso = Read(lector, "Peso")

                ValorImpuestos = Read(lector, "Valor_Impuestos")
                ValorPagarTransportador = Read(lector, "Valor_Pagar_Transportador")
                ValorFleteCliente = Read(lector, "Valor_Flete_Cliente")
                ValorSeguroMercancia = Read(lector, "Valor_Seguro_Mercancia")
                ValorOtrosCobros = Read(lector, "Valor_Otros_Cobros")
                ValorTotalCredito = Read(lector, "Valor_Total_Credito")
                ValorTotalContado = Read(lector, "Valor_Total_Contado")
                ValorTotalAlcobro = Read(lector, "Valor_Total_Alcobro")
                ValorAuxiliares = Read(lector, "Valor_Auxiliares")
                Observaciones = Read(lector, "Observaciones")
                Anulado = Read(lector, "Anulado")
                Estado = New Estado With {.Codigo = Read(lector, "Estado")}
                FechaCrea = Read(lector, "Fecha_Crea")
                FechaModifica = Read(lector, "Fecha_Modifica")
                FechaAnula = Read(lector, "Fecha_Anula")
                CausaAnulacion = Read(lector, "Causa_Anula")
                Oficina = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo"), .Nombre = Read(lector, "NombreOficina")}
                Numeracion = Read(lector, "Numeracion")
                NombreConductor = Read(lector, "NombreConductor")
                NombreTenedor = Read(lector, "NombreTenedor")
                IdentificacionConductor = Read(lector, "IdentificacionConductor")
                IdentificacionTenedor = Read(lector, "IdentificacionTenedor")
                Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo"), .Placa = Read(lector, "PlacaVehiculo")}
                LineaNegocio = Read(lector, "LineaNegocio")
                TipoLineaNegocio = Read(lector, "TipoLineaNegocio")
                TipoTarifa = Read(lector, "TipoTarifa")
                NombreTrarifa = Read(lector, "NombreTrarifa")
                'Cumplido = New Cumplido With {.Numero = Read(lector, "ECPD_Numero"), .NumeroDocumento = Read(lector, "NumeroCumplido")}
                Manifiesto = New Manifiesto With {.Numero = Read(lector, "ENMC_Numero"), .NumeroDocumento = Read(lector, "NumeroManifiesto"), .CantidadViajesDia = Read(lector, "Cantidad_viajes_dia"), .Tipo_Manifiesto = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIMA_Codigo")}}

            Else
                NombreConductor = Read(lector, "NombreConductor")
                Fecha = Read(lector, "Fecha")
                Manifiesto = New Manifiesto With {.Numero = Read(lector, "ENMC_Numero"), .NumeroDocumento = Read(lector, "NumeroManifiesto")}
                Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo"), .Placa = Read(lector, "PlacaVehiculo")}
                Ruta = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo"), .Nombre = Read(lector, "NombreRuta")}
                Anulado = Read(lector, "Anulado")
                Estado = New Estado With {.Codigo = Read(lector, "Estado")}
                TotalRegistros = Read(lector, "TotalRegistros")
            End If
            Semirremolque = New Semirremolques With {.Codigo = Read(lector, "SEMI_Codigo"), .Placa = Read(lector, "SEMI_PLaca")}
            Cumplido = New Cumplido With {.Numero = Read(lector, "ECPD_Numero"), .NumeroDocumento = Read(lector, "NumeroCumplido")}
            ValorSeguroPoliza = Read(lector, "Valor_Seguro_Poliza")
            AnticipoPagadoA = Read(lector, "Anticipo_Pagado_A")
            PagoAnticipo = Read(lector, "PagoAnticipo")
            PesoCumplido = Read(lector, "PesoCumplido")
            Km_Ruta_Base = Read(lector, "Kilometros")
            Km_Ruta = Read(lector, "Kilometros")
            Km_Galon = Read(lector, "Rendimiento_Galon")
            If Km_Galon = 0 Then
                Gal_Estimados = 0
            Else
                Gal_Estimados = Km_Ruta / Km_Galon
            End If
            Liquidacion = New Liquidacion With {.Numero = Read(lector, "ELPD_Numero"), .NumeroDocumento = Read(lector, "NumeroLiquidacion")}
            TipoVehiculoCodigo = Read(lector, "CATA_TIVE_Codigo")
            UsuarioCumplido = Read(lector, "UsuarioCumplido")
            CiudadOrigen = Read(lector, "CiudadOrigen")
            CiudadDestino = Read(lector, "CiudadDestino")
            FechaCumplido = Read(lector, "FechaCumplido")
            TipoDespacho = Read(lector, "TipoDespacho")
            FleteUnitario = Read(lector, "FleteUnitario")
            PesoCargue = Read(lector, "PesoCargue")
            NumeroTarifario = Read(lector, "NumeroTarifario")
            ValorReanticipos = Read(lector, "ValorReanticipos")
            PesoFaltante = Read(lector, "PesoFaltante")
            Cantidad = Read(lector, "Cantidad")
            Km_Inicial = Read(lector, "Km_Final")
        End Sub

        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property IndicadorDespachos As Long
        <JsonProperty>
        Public Property Producto As ProductoTransportados
        <JsonProperty>
        Public Property Remesa As Remesas
        <JsonProperty>
        Public Property Cliente As Tercero
        <JsonProperty>
        Public Property LineaNegocioTransportes As LineaNegocioTransportes
        <JsonProperty>
        Public Property TipoLineaNegocioTransportes As TipoLineaNegocioTransportes
        <JsonProperty>
        Public Property NumeroTarifarioCompra As Long
        <JsonProperty>
        Public Property ValorAuxiliares As Double
        <JsonProperty>
        Public Property Numeracion As String
        <JsonProperty>
        Public Property DesdeDespacharSolicitud As Boolean
        <JsonProperty>
        Public Property NumeroSolicitud As Long
        <JsonProperty>
        Public Property IDDetalleSolicitud As Long
        <JsonProperty>
        Public Property NombreConductor As String
        <JsonProperty>
        Public Property NombreTenedor As String
        <JsonProperty>
        Public Property IdentificacionConductor As Double
        <JsonProperty>
        Public Property IdentificacionTenedor As Double
        <JsonProperty>
        Public Property Manifiesto As Manifiesto
        <JsonProperty>
        Public Property LineaNegocio As String
        <JsonProperty>
        Public Property TipoLineaNegocio As String
        <JsonProperty>
        Public Property TipoTarifa As String
        <JsonProperty>
        Public Property NombreTrarifa As String
        <JsonProperty>
        Public Property NumeroTarifario As Double
        <JsonProperty>
        Public Property Cumplido As Cumplido
        <JsonProperty>
        Public Property ListadoAuxiliar As IEnumerable(Of DetalleAuxiliaresPlanillaDespachos)
        <JsonProperty>
        Public Property ListadoImpuestos As IEnumerable(Of DetalleImpuestosPlanilla)
        <JsonProperty>
        Public Property ListadoRemesas As IEnumerable(Of DetallePlanillas)
        <JsonProperty>
        Public Property ListaConceptosLiquidacion As IEnumerable(Of DetalleLiquidacion)
        <JsonProperty>
        Public Property ListaImpuestosConceptosLiquidacion As IEnumerable(Of ImpuestoConceptoLiquidacionPlanillaDespacho)
        <JsonProperty>
        Public Property Liquidacion As Liquidacion

        <JsonProperty>
        Public Property CuentaPorPagar As EncabezadoDocumentoCuentas
        <JsonProperty>
        Public Property DocumentosRelacionados As IEnumerable(Of EncabezadoDocumentos)
        <JsonProperty>
        Public Property CantidadNovedades As Double
        <JsonProperty>
        Public Property AutorizacionAnticipo As Short
        <JsonProperty>
        Public Property AutorizacionFlete As Short
        <JsonProperty>
        Public Property ValorFleteAutorizacion As Double
        <JsonProperty>
        Public Property strValorFleteAutorizacion As String
        <JsonProperty>
        Public Property strValorFleteTransportador As String
        <JsonProperty>
        Public Property ValorSeguroPoliza As Double
        <JsonProperty>
        Public Property DetalleTiempos As IEnumerable(Of DetalleTiemposPlanillaDespachos)
        <JsonProperty>
        Public Property DetalleTiemposRemesa As IEnumerable(Of DetalleTiemposPlanillaDespachos)
        <JsonProperty>
        Public Property AnticipoPagadoA As String
        <JsonProperty>
        Public Property UsuarioConsulta As Usuarios
        <JsonProperty>
        Public Property TipoVehiculoCodigo As Integer
        <JsonProperty>
        Public Property Km_Ruta As Double
        <JsonProperty>
        Public Property Km_Ruta_Base As Double
        <JsonProperty>
        Public Property Km_Galon As Double
        <JsonProperty>
        Public Property Gal_Estimados As Double
        <JsonProperty>
        Public Property Km_Adicionales As Double
        <JsonProperty>
        Public Property Gal_Adicionales As Double
        <JsonProperty>
        Public Property Horas_Inicio As Double
        <JsonProperty>
        Public Property Horas_Fin As Double
        <JsonProperty>
        Public Property Horas_Termo As Double
        <JsonProperty>
        Public Property Horas_Galon_Termo As Double
        <JsonProperty>
        Public Property Galon_Estimado_Termo As Double
        <JsonProperty>
        Public Property Horas_Adicionales_Termo As Double
        <JsonProperty>
        Public Property Galones_Adicionales_Termo As Double
        <JsonProperty>
        Public Property ValorReanticipos As Double
        <JsonProperty>
        Public Property PesoFaltante As Double
        <JsonProperty>
        Public Property ConsultaLegalización As Double

        <JsonProperty>
        Public Property ConsultaLegalizaciónVariosConductores As Double
        <JsonProperty>
        Public Property Km_Inicial As Double

    End Class
End Namespace
