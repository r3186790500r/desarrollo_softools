﻿EncoExpresApp.controller("CumplidoGuiasPaqueteriaCtrl", ['$scope', '$timeout', '$linq', 'blockUI', 'blockUIConfig', '$routeParams', 'ValorCatalogosFactory', 'RemesaGuiasFactory', 'CiudadesFactory', 'TercerosFactory', 'VehiculosFactory', 'OficinasFactory',
    'RemesasFactory',
    function ($scope, $timeout, $linq, blockUI, blockUIConfig, $routeParams, ValorCatalogosFactory, RemesaGuiasFactory, CiudadesFactory, TercerosFactory, VehiculosFactory, OficinasFactory,
        RemesasFactory) {

        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Procesos' }, { Nombre: 'Cumplir Remesas' }];


        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        var Continuar;
        $scope.DeshabilitarDatosDestinatario = true;
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = '';
        $scope.ModalError = '';
        $scope.MostrarMensajeError = false;
        $scope.Buscando = false;

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CUMPLIR_GUIAS); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;

        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.FechaInicial = new Date();
            $scope.FechaFinal = new Date();
        }

        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };

        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                Find();
            }
        };
        if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
            if ($routeParams.Codigo > 0) {
                $scope.Numero = $routeParams.Codigo;
                Find();
            }
        }
        //--Ciudades
        $scope.ListadoCiudades = [];
        $scope.ListadoCiudades = CiudadesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, Sync: true }).Datos;
        //--Ciudades
        //-- Vehiculos
        $scope.ListadoVehiculos = [];
        $scope.AutocompleteVehiculos = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListadoVehiculos = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoVehiculos);
                }
            }
            return $scope.ListadoVehiculos;
        };
        //--Cliente
        $scope.ListadoCliente = [];
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CLIENTE,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListadoCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCliente);
                }
            }
            return $scope.ListadoCliente;
        };
        //--Cargar el combo de tipo entrega
        $scope.ListadoTipoEntregaRemesaPaqueteria = [];
        $scope.ListadoTipoEntregaRemesaPaqueteria = ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_ENTREGA_REMESA_PAQUETERIA }, Sync: true }).Datos;
        $scope.TipoEntregaRemesaPaqueteria = $scope.ListadoTipoEntregaRemesaPaqueteria[CERO];
        //--Tipo Identificacion 
        $scope.ListadoTipoIdentificacion = [];
        $scope.ListadoTipoIdentificacion = ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IDENTIFICACION_TERCERO }, Sync: true }).Datos;
        //--Tipo Identificacion 
        //--Novedades
        $scope.ListadoNovedades = [];
        $scope.ListadoNovedades = ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 203 }, Sync: true }).Datos;
        //--Novedades

        //--Carga oficinas
        $scope.ListadoOficinas = [];
        if ($scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas) {
            OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1 })
                    if (response.data.ProcesoExitoso === true) {
                        response.data.Datos.forEach(function (item) {
                            $scope.ListadoOficinas.push(item);
                        });
                        $scope.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == -1');
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        } else {
            if ($scope.Sesion.UsuarioAutenticado.ListadoOficinas.length > 0) {
                $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1 })
                $scope.Sesion.UsuarioAutenticado.ListadoOficinas.forEach(function (item) {
                    $scope.ListadoOficinas.push(item);
                });
                $scope.Oficina = $scope.ListadoOficinas[0];
            } else {
                ShowError('El usuario no tiene oficinas asociadas');
            }
        }


        $scope.Buscar = function () {
            if (DatosRequeridos()) {
                if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                    Find();
                }
            }
        };

        function Find() {
            $scope.ListadoRemesasPaqueteria = [];
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            var filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroInicial: $scope.NumeroInicial,
                FechaInicial: $scope.NumeroInicial > 0 ? undefined : $scope.FechaInicial,
                FechaFinal: $scope.NumeroInicial > 0 ? undefined : $scope.FechaFinal,
                NumeroPlanillaInicial: $scope.NumeroPlanillaDespacho,
                PlanillaEntrega: $scope.NumeroPlanillaEntrega,
                Remesa: $scope.Remesa,
                CodigoOficina: $scope.Oficina.Codigo,
                TipoEntregaRemesaPaqueteria: $scope.TipoEntregaRemesaPaqueteria.Codigo,
                UsuarioConsulta: { Codigo: $scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas ? 0 : $scope.Sesion.UsuarioAutenticado.Codigo },
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPaginas
            };

            if (!$scope.Buscando) {
                $scope.Buscando = true;
                RemesaGuiasFactory.ConsultarRemesasPendientesControlEntregas(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoRemesasPaqueteria = response.data.Datos;
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
            blockUI.stop();
        }

        function DatosRequeridos() {
            var continuar = true;
            $scope.MensajesError = [];
            if (($scope.FechaInicial === null || $scope.FechaInicial === undefined || $scope.FechaInicial === '')
                && ($scope.FechaFinal === null || $scope.FechaFinal === undefined || $scope.FechaFinal === '')
                && ($scope.NumeroInicial === null || $scope.NumeroInicial === undefined || $scope.NumeroInicial === '' || $scope.NumeroInicial === 0 || isNaN($scope.NumeroInicial) === true)

            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false;

            } else if (($scope.NumeroInicial !== null && $scope.NumeroInicial !== undefined && $scope.NumeroInicial !== '' && $scope.NumeroInicial !== 0)
                || ($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')
                || ($scope.FechaFinal !== null && $scope.FechaFinal !== undefined && $scope.FechaFinal !== '')

            ) {
                if (($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')
                    && ($scope.FechaFinal !== null && $scope.FechaFinal !== undefined && $scope.FechaFinal !== '')) {
                    if ($scope.FechaFinal < $scope.FechaInicial) {
                        $scope.MensajesError.push('La fecha final debe ser mayor a la fecha inicial');
                        continuar = false;
                    } else if ((($scope.FechaFinal - $scope.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false;
                    }
                }
                else {
                    if (($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')) {
                        $scope.FechaFinal = $scope.FechaInicial;
                    }
                    else {
                        $scope.FechaInicial = $scope.FechaFinal;
                    }
                }
            }
            return continuar;
        }

        //----------------------------Modal control Entregas
        $scope.AbrirModalControlEntregas = function (item) {
            $scope.LimpiarModalControlEntregas();
            var RespObtenerRemesa = RemesasFactory.Obtener({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: item.Remesa.Numero,
                Obtener: 1,
                Sync: true
            }).Datos;
            $scope.ControlEntrega = RespObtenerRemesa;
            console.log($scope.ControlEntrega);
            $scope.ControlEntrega.Recibe = {
                FechaRecibe: new Date(),
                CantidadRecibe: $scope.ControlEntrega.CantidadCliente,
                PesoRecibe: $scope.ControlEntrega.PesoCliente,
                TipoIdentificacionRecibe: $scope.ControlEntrega.Destinatario.TipoIdentificacion.Codigo,
                NumeroIdentificacion: $scope.ControlEntrega.Destinatario.NumeroIdentificacion,
                NombreRecibe: $scope.ControlEntrega.Destinatario.NombreCompleto,
                TelefonoRecibe: $scope.ControlEntrega.Destinatario.Telefonos,
                NovedadEntrega: $linq.Enumerable().From($scope.ListadoNovedades).First('$.Codigo == ' + $scope.ListadoNovedades[0].Codigo),
                ObservacionesRecibe: ''
            };

            if ($scope.ControlEntrega.Destinatario.TipoIdentificacion.Codigo > 0) {
                $scope.ControlEntrega.Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo == ' + $scope.ControlEntrega.Destinatario.TipoIdentificacion.Codigo);
                $scope.ControlEntrega.Recibe.TipoIdentificacionRecibe = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo == ' + $scope.ControlEntrega.Destinatario.TipoIdentificacion.Codigo);
            }
            else {
                $scope.ControlEntrega.Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo == ' + CODIGO_NO_APLICA_TIPO_IDENTIFICACION);
                $scope.ControlEntrega.Recibe.TipoIdentificacionRecibe = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo == ' + CODIGO_NO_APLICA_TIPO_IDENTIFICACION);
            }
            showModal('modalControlEntregas');
        };

        //--Fotografia
        $scope.CargarImagen = function (element) {
            var continuar = true;
            $scope.TipoImagen = element;
            $scope.NombreDocumento = element.files[0].name;
            if (element === undefined) {
                ShowError("Solo se pueden cargar archivos en formato JPEG, PNG", false);
                continuar = false;
            }

            if (continuar == true) {
                if (element.files[0].type !== 'image/jpeg' && element.files[0].type !== 'image/png') {
                    ShowError("Solo se pueden cargar archivos en formato JPEG, PNG ", false);
                    continuar = false;
                }
            }

            if (continuar == true) {
                var reader = new FileReader();
                reader.onload = $scope.AsignarImagenListado;
                reader.readAsDataURL(element.files[0]);
            }

        };

        $scope.AsignarImagenListado = function (e) {
            try {
                RedimHorizontal('')
                RedimVertical('')
            } catch (e) {
            }
            $scope.$apply(function () {
                $scope.Convertirfoto = null
                var TipoImagen = ''

                var name = $scope.TipoImagen.files[0].name.split('.')

                $scope.NombreFoto = name[0]

                if ($scope.TipoImagen.files[0].type == 'image/jpeg') {
                    var img = new Image()
                    img.src = e.target.result
                    //Se detecta primero la orientacion de la imagen luego se redimenciona para bajar la resolucion a un tamaño estandar de 500x700 o 700x500 segun su orientacion
                    //la imagen es vertical
                    $timeout(function () {
                        if (img.height > img.width) {
                            RedimVertical(img)
                            $scope.Convertirfoto = document.getElementById('CanvasVertical').toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/g, '')
                        }
                        //la imagen es horizontal
                        else {
                            RedimHorizontal(img)
                            $scope.Convertirfoto = document.getElementById('CanvasHorizontal').toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/g, '')
                        }
                        TipoImagen = 'JPEG'
                        $scope.TipoImagen = TipoImagen
                        $scope.TipoFoto = 'image/jpeg'
                        AsignarFotoListado()

                    }, 100)
                }
                else if ($scope.TipoImagen.files[0].type == 'image/png') {
                    var img = new Image()
                    img.src = e.target.result
                    //la imagen es vertical
                    $timeout(function () {
                        if (img.height > img.width) {
                            RedimVertical(img)
                            $scope.Convertirfoto = document.getElementById('CanvasVertical').toDataURL("image/png").replace(/data:image\/png;base64,/g, '')
                        }
                        //la imagen es horizontal
                        else {
                            RedimHorizontal(img)
                            $scope.Convertirfoto = document.getElementById('CanvasHorizontal').toDataURL("image/png").replace(/data:image\/png;base64,/g, '')
                        }
                        TipoImagen = 'PNG'
                        $scope.TipoImagen = TipoImagen
                        $scope.TipoFoto = 'image/png'
                        AsignarFotoListado()
                    }, 400)
                }
                else if ($scope.TipoImagen.files[0].type == 'application/pdf') {
                    $scope.Convertirfoto = e.target.result.replace(/data:application\/pdf;base64,/g, '')
                    TipoImagen = 'PDF'
                    $scope.TipoFoto = 'application/pdf'
                    $scope.TipoImagen = TipoImagen
                    AsignarFotoListado()

                }
                var input = document.getElementById('inputImg')
                input.value = ''
            });
        }

        function AsignarFotoListado() {
            $scope.Foto = [];
            $scope.Foto.push({
                NombreFoto: $scope.NombreFoto,
                TipoFoto: $scope.TipoFoto,
                ExtensionFoto: $scope.TipoImagen,
                FotoBits: $scope.Convertirfoto,
                ValorDocumento: 1,
                FotoCargada: 'data:' + $scope.TipoFoto + ';base64,' + $scope.Convertirfoto
            });
        }

        function RedimVertical(img) {
            //Se carga el contexto vans donde se redimencionara la imagen
            var ctx = cargaContextoCanvas('CanvasVertical');
            if (ctx) {
                // dentro del canvaz se dibija la imagen que se recive por parametro
                ctx.drawImage(img, 0, 0, 499, 699);
            }
        }

        function RedimHorizontal(img) {
            //Se carga el contexto vans donde se redimencionara la imagen
            var ctx = cargaContextoCanvas('CanvasHorizontal');
            if (ctx) {
                // dentro del canvaz se dibija la imagen que se recive por parametro
                ctx.drawImage(img, 0, 0, 699, 499);
            }
        }

        $scope.EliminarFoto = function (item) {
            item = [];
            $scope.Foto = []
            $scope.ModificarFoto = true;
            //$("#Foto").attr("src", "");
        }


        //--Fotografia
        $scope.LimpiarFirma = function () {
            try {
                var canvas = document.getElementById('Firma');
                var context = canvas.getContext('2d')
                context.clearRect(0, 0, canvas.width, canvas.height);
            } catch (e) {

            }

        }
        $scope.LimpiarModalControlEntregas = function () {
            $scope.LimpiarFirma();
            $scope.EliminarFoto();
        };

        $scope.VerificarCantidad = function () {
            if (MascaraDecimales($scope.ControlEntrega.Recibe.CantidadRecibe) > MascaraDecimales($scope.ControlEntrega.CantidadCliente)) {
                ShowError('La cantidad recibida no puede ser mayor a la cantidad designada en el producto');
                $scope.ControlEntrega.Recibe.CantidadRecibe = $scope.ControlEntrega.CantidadCliente;
            }
        };

        $scope.VerificarPeso = function () {
            if (MascaraDecimales($scope.ControlEntrega.Recibe.PesoRecibe) > MascaraDecimales($scope.ControlEntrega.PesoCliente)) {
                ShowError('El peso recibido no puede ser mayor al peso designado en el producto');
                $scope.ControlEntrega.Recibe.PesoRecibe = $scope.ControlEntrega.PesoCliente;
            }
        };

        $scope.ValidarCumplirRemesaPaqueteria = function () {
            if (DatosRequeridosControlEntrega()) {
                ShowConfirm('¿Está seguro de guardar la información?', $scope.GuardarPaqueteria);
            }
        };

        $scope.GuardarPaqueteria = function () {
            var canvas = document.getElementById('Firma');
            var image = new Image();
            image.src = canvas.toDataURL("image/png");
            $scope.Firma = image.src.replace(/data:image\/png;base64,/g, '');

            $scope.DatosGuardar = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.ControlEntrega.Numero,
                CantidadRecibe: $scope.ControlEntrega.Recibe.CantidadRecibe,
                PesoRecibe: $scope.ControlEntrega.Recibe.PesoRecibe,
                CodigoTipoIdentificacionRecibe: $scope.ControlEntrega.Recibe.TipoIdentificacionRecibe.Codigo,
                NumeroIdentificacionRecibe: $scope.ControlEntrega.Recibe.NumeroIdentificacion,
                NombreRecibe: $scope.ControlEntrega.Recibe.NombreRecibe,
                TelefonoRecibe: $scope.ControlEntrega.Recibe.TelefonoRecibe,
                NovedadEntrega: { Codigo: $scope.ControlEntrega.Recibe.NovedadEntrega.Codigo },
                ObservacionesRecibe: $scope.ControlEntrega.Recibe.ObservacionesRecibe,
                Fotografia: $scope.Foto[0],
                Firma: $scope.Firma,
                Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
            };

            RemesaGuiasFactory.GuardarEntrega($scope.DatosGuardar).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            ShowSuccess('La remesa fue recibida por ' + $scope.ControlEntrega.Recibe.NombreRecibe);
                            closeModal('modalControlEntregas', 1);
                            Find();
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }
                    else {
                        ShowError(response.statusText);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

        };

        function DatosRequeridosControlEntrega() {
            $scope.MensajesErrorRecibir = [];
            var continuar = true;
            var ControlEntregaRecibe = $scope.ControlEntrega.Recibe;
            if (ControlEntregaRecibe.CantidadRecibe === undefined || ControlEntregaRecibe.CantidadRecibe === '' || ControlEntregaRecibe.CantidadRecibe === null || ControlEntregaRecibe.CantidadRecibe === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar la cantidad');
                continuar = false;
            }
            if (ControlEntregaRecibe.PesoRecibe === undefined || ControlEntregaRecibe.PesoRecibe === '' || ControlEntregaRecibe.PesoRecibe === null || ControlEntregaRecibe.PesoRecibe === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el peso');
                continuar = false;
            }
            if (ControlEntregaRecibe.TipoIdentificacionRecibe === undefined || ControlEntregaRecibe.TipoIdentificacionRecibe === '' || ControlEntregaRecibe.TipoIdentificacionRecibe === null || ControlEntregaRecibe.TipoIdentificacionRecibe.Codigo === CODIGO_NO_APLICA_TIPO_IDENTIFICACION) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el tipo de identificación');
                continuar = false;
            }
            if (ControlEntregaRecibe.NumeroIdentificacion === undefined || ControlEntregaRecibe.NumeroIdentificacion === '' || ControlEntregaRecibe.NumeroIdentificacion === null) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el número de identificación');
                continuar = false;
            }
            if (ControlEntregaRecibe.NombreRecibe === undefined || ControlEntregaRecibe.NombreRecibe === '' || ControlEntregaRecibe.NombreRecibe === null || ControlEntregaRecibe.NombreRecibe === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el nombre de la persona que recibe');
                continuar = false;
            }
            if (ControlEntregaRecibe.TelefonoRecibe === undefined || ControlEntregaRecibe.TelefonoRecibe === '' || ControlEntregaRecibe.TelefonoRecibe === null || ControlEntregaRecibe.TelefonoRecibe === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el teléfono');
                continuar = false;
            }
            return continuar;
        }

        //--------------------------------------------------------------------------------------------------Funciones validar identificacion recibe-------------------------------------------------------
        $scope.VerificarRecibe = function (identificacion) {
            $scope.ValidarIdentificacion = '';
            $scope.ValidarIdentificacion = identificacion;
            if ($scope.ValidarIdentificacion !== '') {
                if ($scope.ValidarIdentificacion === undefined || $scope.ValidarIdentificacion === null || isNaN($scope.ValidarIdentificacion) === true) {
                    ShowInfo('No hay registro de la identificación ingresada.');
                    $scope.ControlEntrega.Recibe.NumeroIdentificacion = '';
                    $scope.ControlEntrega.Recibe.NombreRecibe = '';
                    $scope.ControlEntrega.Recibe.TelefonoRecibe = '';
                } else {
                    TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, NumeroIdentificacion: $scope.ValidarIdentificacion }).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.length > 0) {
                                    if (response.data.Datos[0].Estado.Codigo === 1) {
                                        item = response.data.Datos[0];
                                        $scope.ControlEntrega.Recibe.NumeroIdentificacion = item.NumeroIdentificacion;
                                        $scope.ControlEntrega.Recibe.NombreRecibe = item.NombreCompleto;
                                        $scope.ControlEntrega.Recibe.TelefonoRecibe = item.Telefonos;
                                    } else {
                                        $scope.ControlEntrega.Recibe.NumeroIdentificacion = '';
                                        $scope.ControlEntrega.Recibe.NombreRecibe = '';
                                        $scope.ControlEntrega.Recibe.TelefonoRecibe = '';
                                        ShowInfo('El destinatario ingresado se encuentra inactivo en el sistema.');
                                    }
                                } else {
                                    ShowInfo('No hay registro de la identificación ingresada.');
                                }
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
        };

        //----------------------------Modal control Entregas


        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskMayusGrid = function (item) {
            return MascaraMayus(item)
        };
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item)
        };
        $scope.MaskMoneda = function (option) {
            MascaraMonedaGeneral($scope)
        };
        $scope.MaskMonedaGrid = function (item) {
            return MascaraMoneda(item)
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };
        $scope.MaskHora = function () {
            MascaraHorasGeneral($scope)
        };
        $scope.MaskHoraGrid = function (item) {
            return MascaraHora(item)
        };
    }]);