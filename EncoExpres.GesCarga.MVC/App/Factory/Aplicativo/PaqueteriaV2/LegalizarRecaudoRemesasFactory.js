﻿EncoExpresApp.factory('LegalizarRecaudoRemesasFactory', ['$http', function ($http) {

    var urlService = GetUrl('LegalizarRecaudoRemesas');
    var factory = {};
    var request = {
        method: '',
        url: '',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        data: {}
    };

    factory.Consultar = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Consultar';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.Obtener = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Obtener';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.Guardar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Guardar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.EliminarGuia = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/EliminarGuia';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    return factory;
}]);