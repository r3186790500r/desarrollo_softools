﻿PRINT 'gsp_eliminar_legalizacion_gastos_ruta'
GO
DROP PROCEDURE gsp_eliminar_legalizacion_gastos_ruta
GO
CREATE PROCEDURE gsp_eliminar_legalizacion_gastos_ruta
(@par_EMPR_Codigo SMALLINT,    
@par_RUTA_Codigo NUMERIC)    
AS    
BEGIN    
DELETE Plantilla_Ruta_Legalizacion_Gastos_Conductor WHERE EMPR_Codigo = @par_EMPR_Codigo and RUTA_Codigo = @par_RUTA_Codigo    
END
GO