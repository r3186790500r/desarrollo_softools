﻿PRINT 'gsp_consultar_tercero_cliente_cupo_oficina'
GO
DROP PROCEDURE gsp_consultar_tercero_cliente_cupo_oficina
GO
CREATE PROCEDURE gsp_consultar_tercero_cliente_cupo_oficina
(@par_EMPR_Codigo SMALLINT,    
@par_TERC_Codigo NUMERIC)    
AS    
BEGIN    
	SELECT 
	TCCO.EMPR_Codigo,
	OFIC.Codigo AS OFIC_Codigo,
	OFIC.Nombre AS OFIC_Nombre,
	TCCO.Cupo,
	TCCO.Saldo_Cupo as Saldo

	FROM Tercero_Clientes_Cupo_Oficinas TCCO

	 INNER JOIN Oficinas OFIC ON
	 TCCO.EMPR_Codigo = OFIC.EMPR_Codigo
	 AND TCCO.OFIC_Codigo = OFIC.Codigo

	 WHERE TCCO.EMPR_Codigo = @par_EMPR_Codigo
	 AND TCCO.TERC_Codigo = @par_TERC_Codigo

END
GO