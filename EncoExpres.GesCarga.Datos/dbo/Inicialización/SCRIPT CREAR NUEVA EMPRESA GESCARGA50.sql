-- SCRIPT CREACION EMPRESA GESTRANS.NET CARGA 5.0
-- Fecha: 07-MAY-2019
-- Autor: JESUS CUADROS 

SELECT * FROM Empresas

/*Variables*/
DECLARE @EMPR_Codigo_Nueva SMALLINT = 12 --CODIGO NUEVA EMPRESA
DECLARE @EMPR_Codigo_Copia SMALLINT = 10 --CODIGO EMPRESA A COPIAR 

-- INSERT Empresas
PRINT 'Insertar Empresas'
INSERT INTO Empresas(Codigo,Codigo_Alterno,Nombre_Razon_Social,Nombre_Corto_Razon_Social,CATA_TIID_Codigo,Numero_Identificacion,Digito_Chequeo,PAIS_Codigo,CIUD_Codigo,Direccion,Telefonos
,Codigo_Postal,Emails,Pagina_Web,Estado,USUA_Codigo_Modifica,Fecha_Modifica,Mensaje_Banner,Numero_Usuarios_Aplicativo,Numero_Usuarios_Gesphone,Numero_Usuarios_Portal,Prefijo,Genera_Manifiesto
,Reportar_RNDC,Aprobar_Liquidacion_Despachos,Aprobar_Pagos_CxP,Dias_Presentacion_Cumplido,Multa_Extemporaneidad_Cumplido,TERC_Codigo_Aseguradora,Numero_Poliza,Fecha_Vencimiento_Poliza
,Dias_Vigencia_Plan_Puntos,Dias_Vigencia_Enturne,Valor_Transaccion_SIPLAFT,Codigo_UIAF,Codigo_Regional_Ministerio,Codigo_Ministerio,Valor_Poliza,Valor_Galon_Combustilbe)
SELECT @EMPR_Codigo_Nueva,Codigo_Alterno
,'PRUEBA SCRIPT'  --Nombre_Razon_Social
,'PRUEBA SCRIPT'  --Nombre_Corto_Razon_Social
,CATA_TIID_Codigo
,''  --Numero_Identificacion
,Digito_Chequeo,PAIS_Codigo,CIUD_Codigo,Direccion,Telefonos,Codigo_Postal,Emails,Pagina_Web,Estado,USUA_Codigo_Modifica,Fecha_Modifica,Mensaje_Banner,Numero_Usuarios_Aplicativo
,Numero_Usuarios_Gesphone,Numero_Usuarios_Portal,Prefijo,Genera_Manifiesto,Reportar_RNDC,Aprobar_Liquidacion_Despachos,Aprobar_Pagos_CxP,Dias_Presentacion_Cumplido,Multa_Extemporaneidad_Cumplido
,TERC_Codigo_Aseguradora,Numero_Poliza,Fecha_Vencimiento_Poliza,Dias_Vigencia_Plan_Puntos,Dias_Vigencia_Enturne,Valor_Transaccion_SIPLAFT,Codigo_UIAF,Codigo_Regional_Ministerio,Codigo_Ministerio
,Valor_Poliza,Valor_Galon_Combustilbe FROM Empresas WHERE Codigo = @EMPR_Codigo_Copia



-- CATALOGOS
PRINT 'Insertar Catalogos'
INSERT INTO Catalogos ([EMPR_Codigo], [Codigo], Nombre_Corto, [Nombre], [Actualizable], [Numero_Columnas], [Numero_Campos_Llave])
SELECT @EMPR_Codigo_Nueva, [Codigo], Nombre_Corto, [Nombre], [Actualizable], [Numero_Columnas], [Numero_Campos_Llave]
FROM Catalogos WHERE EMPR_Codigo = @EMPR_Codigo_Copia


-- VALOR CATALOGOS
PRINT 'Insertar Valor_Catalogos'
INSERT INTO Valor_Catalogos ([EMPR_Codigo], [Codigo], [CATA_Codigo], [Campo1], [Campo2], [Campo3], [Campo4], [Campo5], Estado, USUA_Codigo_Crea, Fecha_Crea)
SELECT @EMPR_Codigo_Nueva, [Codigo], [CATA_Codigo], [Campo1], [Campo2], [Campo3], [Campo4], [Campo5], Estado, USUA_Codigo_Crea, getdate()
FROM Valor_Catalogos WHERE EMPR_Codigo = @EMPR_Codigo_Copia



-- INSERTAR GRUPO USUARIOS administrador
-- Desactivar/Activar el IDENTITY del campo Codigo

SET IDENTITY_INSERT Grupo_Usuarios  OFF
SET IDENTITY_INSERT Ciudades  OFF
SET IDENTITY_INSERT Conceptos_Liquidacion_Planilla_Despachos OFF
SET IDENTITY_INSERT Grupo_Usuarios  ON

PRINT 'Insertar Grupo_Usuarios'
INSERT INTO Grupo_Usuarios ([EMPR_Codigo], Codigo, Codigo_Grupo, [Nombre], Usua_Codigo_Crea, Fecha_Crea)
SELECT @EMPR_Codigo_Nueva, 1, Codigo_Grupo, [Nombre], Usua_Codigo_Crea, Fecha_Crea
FROM Grupo_Usuarios WHERE EMPR_Codigo = @EMPR_Codigo_Copia AND Codigo = 1

-- Insertar Usuarios (Usuario 0=SYSTEM 1=ADMIN)
-- Desactivar/Activar el IDENTITY del campo Codigo

SET IDENTITY_INSERT Grupo_Usuarios OFF
SET IDENTITY_INSERT Usuarios OFF 
SET IDENTITY_INSERT Usuarios ON

PRINT 'Insertar Usuarios'
INSERT INTO Usuarios(EMPR_Codigo,Codigo,Codigo_Usuario,Codigo_Alterno,Nombre,Descripcion,Habilitado,Clave,Dias_Cambio_Clave,Manager,Login,CATA_APLI_Codigo,TERC_Codigo_Empleado,TERC_Codigo_Cliente
,TERC_Codigo_Conductor,TERC_Codigo_Proveedor,TERC_Codigo_Transportador,Fecha_Ultimo_Ingreso,Intentos_Ingreso,Fecha_Ultimo_Cambio_Clave,Mensaje_Banner,Fecha_Ultima_Actividad,Consulta_Otras_Oficinas) 
SELECT 
@EMPR_Codigo_Nueva,Codigo,Codigo_Usuario,Codigo_Alterno,Nombre,Descripcion,Habilitado,Clave,Dias_Cambio_Clave,Manager,Login,CATA_APLI_Codigo,TERC_Codigo_Empleado,TERC_Codigo_Cliente
,TERC_Codigo_Conductor,TERC_Codigo_Proveedor,TERC_Codigo_Transportador,Fecha_Ultimo_Ingreso,Intentos_Ingreso,Fecha_Ultimo_Cambio_Clave,Mensaje_Banner,Fecha_Ultima_Actividad,Consulta_Otras_Oficinas
FROM Usuarios WHERE EMPR_Codigo = @EMPR_Codigo_Copia AND Codigo IN (0,1)


-- INSERTAR MODULO APLICACIONES
PRINT 'Insertar Modulo_Aplicaciones'
INSERT INTO Modulo_Aplicaciones ([EMPR_Codigo], [Codigo], [Nombre], CATA_APLI_Codigo, [Orden_Menu], [Mostrar_Menu], [Imagen], Estado)
SELECT @EMPR_Codigo_Nueva, [Codigo], [Nombre], CATA_APLI_Codigo, [Orden_Menu], [Mostrar_Menu], [Imagen], Estado
FROM Modulo_Aplicaciones WHERE EMPR_Codigo = @EMPR_Codigo_Copia


-- INSERTAR MENU APLICACIONES
PRINT 'Insertar Menu_Aplicaciones' 
INSERT INTO Menu_Aplicaciones([EMPR_Codigo], [Codigo], [MOAP_Codigo], [Nombre], [Padre_Menu], [Orden_Menu], Mostrar_Menu,
[Aplica_Habilitar], [Aplica_Consultar], [Aplica_Actualizar], [Aplica_Eliminar_Anular], [Aplica_Imprimir], [Aplica_Contabilidad],
[Opcion_Permiso], [Opcion_Listado], [Aplica_Ayuda], [URL_Pagina], [URL_Ayuda], [Imagen])
SELECT @EMPR_Codigo_Nueva, [Codigo], [MOAP_Codigo], [Nombre], [Padre_Menu], [Orden_Menu], Mostrar_Menu,
[Aplica_Habilitar], [Aplica_Consultar], [Aplica_Actualizar], [Aplica_Eliminar_Anular], [Aplica_Imprimir], [Aplica_Contabilidad],
[Opcion_Permiso], [Opcion_Listado], [Aplica_Ayuda], [URL_Pagina], [URL_Ayuda], [Imagen]
FROM Menu_Aplicaciones WHERE EMPR_Codigo = @EMPR_Codigo_Copia

-- INSERTAR PERMISO_GRUPO_USUARIOS PARA GRUS_CODIGO = 1 ADMINISTRADORES
PRINT 'Insertar Permiso_Grupo_Usuarios'
INSERT INTO Permiso_Grupo_Usuarios ([EMPR_Codigo], [MEAP_Codigo], [GRUS_Codigo], [Habilitar], 
[Consultar], [Actualizar], [Eliminar_Anular], [Imprimir], Permiso, Contabilidad)
SELECT @EMPR_Codigo_Nueva, [MEAP_Codigo], 1, [Habilitar], 
[Consultar], [Actualizar], [Eliminar_Anular], [Imprimir], Permiso, Contabilidad
FROM Permiso_Grupo_Usuarios WHERE EMPR_Codigo = @EMPR_Codigo_Copia AND GRUS_Codigo = 1


-- INSERTAR USUARIO GRUPO USUARIOS
-- Usuario 1-ADMIN --> GRUPO ADMIN GRUS_Codigo = 1
PRINT 'Insertar Usuario_Grupo_Usuarios'
INSERT INTO Usuario_Grupo_Usuarios ([EMPR_Codigo], [USUA_Codigo], [GRUS_Codigo], USUA_Codigo_Crea, Fecha_Crea)
SELECT @EMPR_Codigo_Nueva, 1, 1, USUA_Codigo_Crea, getdate()
FROM Usuario_Grupo_Usuarios WHERE EMPR_Codigo = @EMPR_Codigo_Copia AND GRUS_Codigo = 1 AND USUA_Codigo = 1



-- INSERTAR MONEDAS
SET IDENTITY_INSERT Monedas OFF
SET IDENTITY_INSERT Usuarios OFF
SET IDENTITY_INSERT Monedas ON

PRINT 'Insertar Monedas'
INSERT INTO Monedas(EMPR_Codigo,Codigo,Nombre,Nombre_Corto,Simbolo,Local,Estado,USUA_Codigo_Crea,Fecha_Crea,USUA_Codigo_Modifica,Fecha_Modifica)
SELECT @EMPR_Codigo_Nueva,Codigo,Nombre,Nombre_Corto,Simbolo,Local,Estado,USUA_Codigo_Crea,Fecha_Crea,USUA_Codigo_Modifica,Fecha_Modifica
FROM Monedas WHERE EMPR_Codigo = @EMPR_Codigo_Copia


-- INSERTAR PAISES
-- Desactivar/Activar el IDENTITY del campo Codigo
SET IDENTITY_INSERT Paises OFF
SET IDENTITY_INSERT Monedas OFF
SET IDENTITY_INSERT Paises ON

PRINT 'Insertar Paises'
INSERT INTO Paises ([EMPR_Codigo],[Codigo],[Codigo_Alterno],[Nombre],[Nombre_Corto],
[MONE_Codigo],[Estado],[USUA_Codigo_Crea],[Fecha_Crea])
SELECT @EMPR_Codigo_Nueva,[Codigo],[Codigo_Alterno],[Nombre],[Nombre_Corto],
1,[Estado],[USUA_Codigo_Crea],getdate()
FROM Paises WHERE EMPR_Codigo = @EMPR_Codigo_Copia

-- INSERTAR DEPARTAMENTOS
-- Desactivar/Activar el IDENTITY del campo Codigo
SET IDENTITY_INSERT Departamentos OFF
SET IDENTITY_INSERT Paises OFF
SET IDENTITY_INSERT Departamentos ON

PRINT 'Insertar departamentos'
INSERT INTO Departamentos ([EMPR_Codigo], [Codigo], [PAIS_Codigo], [Codigo_Alterno], Nombre, [Codigo_DANE], Estado, USUA_Codigo_Crea, Fecha_Crea)
SELECT @EMPR_Codigo_Nueva, [Codigo], [PAIS_Codigo], [Codigo_Alterno], Nombre, [Codigo_DANE], Estado, USUA_Codigo_Crea, getdate()
FROM Departamentos WHERE EMPR_Codigo = @EMPR_Codigo_Copia


-- INSERTAR CODIGO_DANE_CIUDADES

PRINT 'Insertar Codigo_DANE_Ciudades'
INSERT INTO Codigo_DANE_Ciudades ([EMPR_Codigo], [Codigo], [Nombre], [Codigo_Ciudad], [Codigo_Departamento])
SELECT @EMPR_Codigo_Nueva, [Codigo], [Nombre], [Codigo_Ciudad], [Codigo_Departamento]
FROM Codigo_DANE_Ciudades WHERE EMPR_Codigo = @EMPR_Codigo_Copia

-- INSERTAR CIUDADES
-- Desactivar/Activar el IDENTITY del campo Codigo
SET IDENTITY_INSERT Ciudades OFF
SET IDENTITY_INSERT Departamentos OFF
SET IDENTITY_INSERT Ciudades ON

PRINT 'Insertar Ciudades'
INSERT INTO Ciudades ([EMPR_Codigo],[Codigo], [Codigo_Alterno], [Nombre], DEPA_Codigo, Aplica_Oficina, Estado, USUA_Codigo_Crea, Fecha_Crea)
SELECT @EMPR_Codigo_Nueva, [Codigo], [Codigo_Alterno], [Nombre], DEPA_Codigo, Aplica_Oficina, Estado, USUA_Codigo_Crea, getdate()
FROM Ciudades WHERE EMPR_Codigo = @EMPR_Codigo_Copia


-- INSERTAR ZONA CIUDADES
-- Desactivar/Activar el IDENTITY del campo Codigo
SET IDENTITY_INSERT Zona_Ciudades  OFF
SET IDENTITY_INSERT ciudades OFf
SET IDENTITY_INSERT Zona_Ciudades  ON

INSERT INTO Zona_Ciudades ([EMPR_Codigo], [Codigo], [CIUD_Codigo], [Nombre], [Estado], [USUA_Codigo_Crea], [Fecha_Crea], [USUA_Codigo_Modifica], [Fecha_Modifica])
VALUES (@EMPR_Codigo_Nueva, 0, 0, '(N/A)', 0, 1, getdate(), null, null)


-- INSERTAR TERCEROS
-- Tercero=0 y Tercero=1
PRINT 'Insertar Terceros'
INSERT INTO Terceros(EMPR_Codigo,Codigo,Codigo_Alterno,Codigo_Contable,CATA_TINT_Codigo,CATA_TIID_Codigo,Numero_Identificacion,Digito_Chequeo,CIUD_Codigo_Identificacion,CIUD_Codigo_Nacimiento
,Razon_Social,Representante_Legal,Nombre,Apellido1,Apellido2,CATA_SETE_Codigo,PAIS_Codigo,CIUD_Codigo,Barrio,Direccion,Codigo_Postal,Telefonos,Celulares,Emails,Observaciones,Foto,Justificacion_Bloqueo
,Reporto_Contabilidad,Codigo_Retorno_Contabilidad,Mensaje_Retorno_Contabilidad,BANC_Codigo,CATA_TICB_Codigo,Numero_Cuenta_Bancaria,TERC_Codigo_Beneficiario,Titular_Cuenta_Bancaria
,Estado,USUA_Codigo_Crea,Fecha_Crea,USUA_Modifica,Fecha_Modifica,CATA_TIAN_Codigo,CATA_TIVC_Codigo,Cupo,Saldo)
SELECT @EMPR_Codigo_Nueva,Codigo,Codigo_Alterno,Codigo_Contable,CATA_TINT_Codigo,CATA_TIID_Codigo,Numero_Identificacion,Digito_Chequeo,CIUD_Codigo_Identificacion,CIUD_Codigo_Nacimiento
,Razon_Social,Representante_Legal,Nombre,Apellido1,Apellido2,CATA_SETE_Codigo,PAIS_Codigo,CIUD_Codigo,Barrio,Direccion,Codigo_Postal,Telefonos,Celulares,Emails,Observaciones,Foto,Justificacion_Bloqueo
,Reporto_Contabilidad,Codigo_Retorno_Contabilidad,Mensaje_Retorno_Contabilidad,BANC_Codigo,CATA_TICB_Codigo,Numero_Cuenta_Bancaria,TERC_Codigo_Beneficiario,Titular_Cuenta_Bancaria
,Estado,USUA_Codigo_Crea,Fecha_Crea,USUA_Modifica,Fecha_Modifica,CATA_TIAN_Codigo,CATA_TIVC_Codigo,Cupo,Saldo
FROM Terceros WHERE EMPR_Codigo = @EMPR_Codigo_Copia AND Codigo IN (0,1)


-- Insertarle todos los perfiles al Tercero=1 -- Catalogo Perfiles = 14 -- 1400 (No Aplica)
PRINT 'Insertar Perfil_Terceros'
INSERT INTO Perfil_Terceros ([EMPR_Codigo], [TERC_Codigo], [Codigo])
SELECT @EMPR_Codigo_Nueva, 1, Codigo FROM Valor_Catalogos WHERE EMPR_Codigo = @EMPR_Codigo_Copia AND Codigo <> 1400 AND CATA_Codigo = 14


-- Insertar Oficinas con Codigo = 0 y Codigo = 1
PRINT 'Insertar Oficinas'
INSERT INTO Oficinas(EMPR_Codigo,Codigo,Codigo_Alterno,Nombre,CIUD_Codigo,Direccion,ZOCI_Codigo,Telefono,Email,Resolucion_Facturacion
,CATA_TIOF_Codigo,REPA_Codigo,Estado,USUA_Codigo_Crea,Fecha_Crea,USUA_Modifica,Fecha_Modifica,Aplica_Enturnamiento
)SELECT @EMPR_Codigo_Nueva,Codigo,Codigo_Alterno,Nombre,CIUD_Codigo,Direccion,ZOCI_Codigo,Telefono,Email,Resolucion_Facturacion
,CATA_TIOF_Codigo,REPA_Codigo,Estado,USUA_Codigo_Crea,Fecha_Crea,USUA_Modifica,Fecha_Modifica,Aplica_Enturnamiento
FROM Oficinas WHERE EMPR_Codigo = @EMPR_Codigo_Copia AND Codigo IN (0,1)


-- Insertar Usuario_Oficinas Oficina = 1
PRINT 'Insertar Usuario_Oficinas'
INSERT INTO Usuario_Oficinas (EMPR_Codigo, USUA_Codigo, OFIC_Codigo)
VALUES(@EMPR_Codigo_Nueva,1,1)


-- INSERTAR TIPO DOCUMENTOS
PRINT 'Insertar Tipo_Documentos'
INSERT INTO Tipo_Documentos ([EMPR_Codigo], [Codigo], [Nombre], [CATA_TIGN_Codigo], [Consecutivo], [Consecutivo_Hasta], [Controlar_Consecutivo_Hasta],
[Numero_Documentos_Faltantes_Aviso], [Notas_Inicio], [Notas_Fin], [Tipo_Documento_Sistema], Cierre_Contable, Estado, USUA_Codigo_Crea, Fecha_Crea)
SELECT @EMPR_Codigo_Nueva, Row_Number() Over (Order By Codigo) + 380, [Nombre], [CATA_TIGN_Codigo], [Consecutivo], [Consecutivo_Hasta], [Controlar_Consecutivo_Hasta],
[Numero_Documentos_Faltantes_Aviso], [Notas_Inicio], [Notas_Fin], [Tipo_Documento_Sistema], Cierre_Contable, Estado, 1, getdate()
FROM Tipo_Documentos WHERE EMPR_Codigo = @EMPR_Codigo_Copia


-- INICIALIZAR CONSECUTIVOS TIPO_DOCUMENTOS
PRINT 'Insertar Inicializar Consecutivo Tipo Documentos'
UPDATE Tipo_Documentos SET [Consecutivo] = 1000000, [Consecutivo_Hasta] = 1999999
WHERE EMPR_Codigo = @EMPR_Codigo_Nueva


UPDATE Tipo_Documentos SET [Consecutivo] = 1000001, [Consecutivo_Hasta] = 1999999
WHERE EMPR_Codigo = @EMPR_Codigo_Nueva AND Nombre = 'Pre Factura' 


 --Insertar Consecutivo_Documento_Oficinas
-- --Desactivar/Activar el IDENTITY del campo Codigo
--INSERT INTO [Consecutivo_Documento_Oficinas] ([EMPR_Codigo], [Codigo], [TIDO_Codigo], [OFIC_Codigo], [Consecutivo], [Consecutivo_Hasta], [Numero_Documentos_Faltantes_Aviso])
--SELECT 4, [Codigo], [TIDO_Codigo], [OFIC_Codigo], [Consecutivo], [Consecutivo_Hasta], [Numero_Documentos_Faltantes_Aviso]
--FROM [Consecutivo_Documento_Oficinas] WHERE EMPR_Codigo = 2

-- INSERTAR COLORES VEHICULOS
-- Desactivar/Activar el IDENTITY del campo Codigo

SET IDENTITY_INSERT Color_Vehiculos  OFF
SET IDENTITY_INSERT Ciudades OFF
SET IDENTITY_INSERT Zona_Ciudades OFF
SET IDENTITY_INSERT Color_Vehiculos  ON

PRINT 'Insertar Color_Vehiculos'
INSERT INTO Color_Vehiculos ( EMPR_Codigo, Codigo, Codigo_Alterno, Nombre, Estado, USUA_Codigo_Crea, Fecha_Crea)
SELECT @EMPR_Codigo_Nueva, Codigo, Codigo_Alterno, Nombre, Estado, USUA_Codigo_Crea, getdate()
FROM Color_Vehiculos WHERE EMPR_Codigo = @EMPR_Codigo_Copia


-- INSERTAR MARCAS VEHICULOS
-- Desactivar/Activar el IDENTITY del campo Codigo

SET IDENTITY_INSERT Marca_Vehiculos  OFF
SET IDENTITY_INSERT Color_Vehiculos  OFF
SET IDENTITY_INSERT Marca_Vehiculos  ON

PRINT 'Insertar Marca_Vehiculos'
INSERT INTO Marca_Vehiculos ( EMPR_Codigo, Codigo, Codigo_Alterno, Nombre, Estado, USUA_Codigo_Crea, Fecha_Crea)
SELECT @EMPR_Codigo_Nueva, Codigo, Codigo_Alterno, Nombre, Estado, USUA_Codigo_Crea, getdate()
FROM Marca_Vehiculos WHERE EMPR_Codigo = @EMPR_Codigo_Copia


-- INSERTAR LINEAS VEHICULOS
-- Desactivar/Activar el IDENTITY del campo Codigo
SET IDENTITY_INSERT Linea_Vehiculos  OFF
SET IDENTITY_INSERT Marca_Vehiculos  OFF
SET IDENTITY_INSERT Linea_Vehiculos  ON

PRINT 'Insertar Linea_Vehiculos'
INSERT INTO Linea_Vehiculos ( EMPR_Codigo, Codigo, Codigo_Alterno, MAVE_Codigo, Nombre, Estado, USUA_Codigo_Crea, Fecha_Crea)
SELECT @EMPR_Codigo_Nueva, Codigo, Codigo_Alterno, MAVE_Codigo, Nombre, Estado, 1, getdate()
FROM Linea_Vehiculos WHERE EMPR_Codigo = @EMPR_Codigo_Copia


-- INSERTAR MARCAS SEMIRREMOLQUES
-- Desactivar/Activar el IDENTITY del campo Codigo
SET IDENTITY_INSERT Marca_Semirremolques  OFF
SET IDENTITY_INSERT Linea_Vehiculos  OFF
SET IDENTITY_INSERT Marca_Semirremolques  ON

PRINT 'Insertar Marca_Semirremolques'
INSERT INTO Marca_Semirremolques( EMPR_Codigo, Codigo, Codigo_Alterno, Nombre, Estado, USUA_Codigo_Crea, Fecha_Crea)
SELECT @EMPR_Codigo_Nueva, Codigo, Codigo_Alterno, Nombre, Estado, 1, getdate()
FROM Marca_Semirremolques WHERE EMPR_Codigo = @EMPR_Codigo_Copia


-- INSERTAR BANCOS
PRINT 'Insertar Bancos'
INSERT INTO Bancos( EMPR_Codigo, Codigo_Alterno, Nombre, Estado, USUA_Codigo_Crea, Fecha_Crea)
SELECT @EMPR_Codigo_Nueva, Codigo_Alterno, Nombre, Estado, 1, getdate()
FROM Bancos WHERE EMPR_Codigo = @EMPR_Codigo_Copia


-- INSERTAR TARIFA TRANSPORTE CARGA

PRINT 'Insertar Tarifa_Transporte_Carga'
INSERT INTO Tarifa_Transporte_Carga ([EMPR_Codigo], [Codigo], [Nombre],  [Estado])
SELECT @EMPR_Codigo_Nueva, [Codigo], [Nombre],  [Estado]
FROM Tarifa_Transporte_Carga WHERE EMPR_Codigo = @EMPR_Codigo_Copia


-- INSERTAR LINEA NEGOCIO TRANSPORTE CARGA

PRINT 'Insertar Linea_Negocio_Transporte_Carga'
INSERT INTO [Linea_Negocio_Transporte_Carga] ([EMPR_Codigo], [Codigo], [Nombre],  [Estado])
SELECT @EMPR_Codigo_Nueva, [Codigo], [Nombre],  [Estado]
FROM [Linea_Negocio_Transporte_Carga] WHERE EMPR_Codigo = @EMPR_Codigo_Copia


-- INSERTAR TIPO LINEA NEGOCIO CARGA
PRINT 'Insertar Tipo_Linea_Negocio_Carga'
INSERT INTO Tipo_Linea_Negocio_Carga ([EMPR_Codigo], LNTC_Codigo, [Codigo], [Nombre],  [Estado])
SELECT @EMPR_Codigo_Nueva, LNTC_Codigo, [Codigo], [Nombre],  [Estado]
FROM Tipo_Linea_Negocio_Carga WHERE EMPR_Codigo = @EMPR_Codigo_Copia


-- INSERTAR TIPO LINEA TARIFA TRANSPORTE CARGA
PRINT 'Insertar Tipo_Linea_Tarifa_Transporte_Carga'
INSERT INTO Tipo_Linea_Tarifa_Transporte_Carga ([EMPR_Codigo], LNTC_Codigo, TLNC_Codigo, TATC_Codigo)
SELECT @EMPR_Codigo_Nueva, LNTC_Codigo, TLNC_Codigo, TATC_Codigo
FROM Tipo_Linea_Tarifa_Transporte_Carga WHERE EMPR_Codigo = @EMPR_Codigo_Copia


-- INSETAR TIPO_TARIFA_TRANSPORTE_CARGA
PRINT 'Insertar Tipo_Tarifa_Transporte_Carga'
INSERT INTO Tipo_Tarifa_Transporte_Carga ([EMPR_Codigo], [TATC_Codigo], [Codigo], [CATA_CONT_Codigo], [CATA_DECO_Codigo],
[CATA_TIVE_Codigo], [CATA_CAVE_Codigo], [CATA_RPVK_Codigo], [CATA_RPVJ_Codigo], [CATA_TIMO_Codigo], [Estado])
SELECT @EMPR_Codigo_Nueva, [TATC_Codigo], [Codigo], [CATA_CONT_Codigo], [CATA_DECO_Codigo],
[CATA_TIVE_Codigo], [CATA_CAVE_Codigo], [CATA_RPVK_Codigo], [CATA_RPVJ_Codigo], [CATA_TIMO_Codigo], [Estado]
FROM Tipo_Tarifa_Transporte_Carga WHERE EMPR_Codigo = @EMPR_Codigo_Copia


-- INSERTAR Entidad_Documento_Gestion_Documentos
PRINT 'Insertar Entidad_Documento_Gestion_Documentos'
INSERT INTO Entidad_Documento_Gestion_Documentos(EMPR_Codigo,Codigo,Nombre,TIDO_Codigo,Estado,USUA_Codigo_Crea,Fecha_Crea)
SELECT @EMPR_Codigo_Nueva,Codigo,Nombre,TIDO_Codigo,Estado,USUA_Codigo_Crea,Fecha_Crea
FROM Entidad_Documento_Gestion_Documentos WHERE EMPR_Codigo = @EMPR_Codigo_Copia


-- INSERTAR Documento_Gestion_Documentos
PRINT 'Insertar Documento_Gestion_Documentos'
INSERT INTO Documento_Gestion_Documentos(EMPR_Codigo,Codigo,Nombre,Estado,USUA_Codigo_Crea,Fecha_Crea,Cantidad_Repeticiones)
SELECT @EMPR_Codigo_Nueva,Codigo,Nombre,Estado,USUA_Codigo_Crea,Fecha_Crea,Cantidad_Repeticiones
FROM Documento_Gestion_Documentos WHERE EMPR_Codigo = @EMPR_Codigo_Copia


-- INSERTAR Configuracion_Documento_Gestion_Documentos
PRINT 'Insertar Configuracion_Documento_Gestion_Documentos'
INSERT INTO Configuracion_Documento_Gestion_Documentos(EMPR_Codigo,Codigo,EDGD_Codigo,DOGD_Codigo,Aplica_Referencia,Aplica_Emisor,Aplica_Fecha_Emision,
Aplica_Fecha_Vencimiento,Aplica_Archivo,Aplica_Eliminar,Aplica_Descargar,Tamano,Nombre_Tamano,Habilitado,CATA_TIAD_Codigo,Estado,USUA_Codigo_Crea,Fecha_Crea)
SELECT @EMPR_Codigo_Nueva,Codigo,EDGD_Codigo,DOGD_Codigo,Aplica_Referencia,Aplica_Emisor,Aplica_Fecha_Emision,Aplica_Fecha_Vencimiento,Aplica_Archivo,Aplica_Eliminar,
Aplica_Descargar,Tamano,Nombre_Tamano,Habilitado,CATA_TIAD_Codigo,Estado,USUA_Codigo_Crea,Fecha_Crea
FROM Configuracion_Documento_Gestion_Documentos WHERE EMPR_Codigo = @EMPR_Codigo_Copia


-- SCRIPT CREACION EMPRESA GESTRANS.NET CARGA 5.0
-- Fecha: 02-MAR-2020
-- Autor: CRISTIAN BENITEZ 

-- INSERTAR CONFIGURACIÓN CATALOGOS
PRINT 'Insertar Configuracion_Catalogos'
INSERT INTO Configuracion_Catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo
,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
SELECT @EMPR_Codigo_Nueva,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo
,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo
FROM Configuracion_Catalogos WHERE EMPR_Codigo = @EMPR_Codigo_Copia


-- INSERTAR Productos_Ministerio_Transporte
-- Desactivar/Activar el IDENTITY del campo Codigo

SET IDENTITY_INSERT Productos_Ministerio_Transporte  OFF
SET IDENTITY_INSERT Marca_Semirremolques  OFF
SET IDENTITY_INSERT Productos_Ministerio_Transporte  ON


PRINT 'Insertar Productos_Ministerio_Transporte'
INSERT INTO Productos_Ministerio_Transporte (EMPR_Codigo,Codigo,Codigo_Alterno,Nombre,Descripcion,Estado
,Fecha_Crea,USUA_Codigo_Crea,Fecha_Modifica,USUA_Codigo_Modifica)
SELECT @EMPR_Codigo_Nueva,Codigo,Codigo_Alterno,Nombre,Descripcion,Estado
,Fecha_Crea,USUA_Codigo_Crea,Fecha_Modifica,USUA_Codigo_Modifica
FROM Productos_Ministerio_Transporte WHERE EMPR_Codigo = @EMPR_Codigo_Copia

 -- INSERTAR Unidad_Empaque_Producto_Transportados

SET IDENTITY_INSERT Unidad_Empaque_Producto_Transportados  OFF
SET IDENTITY_INSERT Productos_Ministerio_Transporte  OFF
SET IDENTITY_INSERT Unidad_Empaque_Producto_Transportados  ON

 PRINT 'Insertar Unidad_Empaque_Producto_Transportados'
INSERT INTO Unidad_Empaque_Producto_Transportados(EMPR_Codigo,Codigo,Codigo_Alterno,Nombre_Corto,Descripcion,Estado
,Fecha_Crea,USUA_Codigo_Crea,Fecha_Modifica,USUA_Codigo_Modifica )
SELECT  @EMPR_Codigo_Nueva,Codigo,Codigo_Alterno,Nombre_Corto,Descripcion,Estado,Fecha_Crea,USUA_Codigo_Crea,Fecha_Modifica,USUA_Codigo_Modifica 
FROM Unidad_Empaque_Producto_Transportados WHERE EMPR_Codigo = @EMPR_Codigo_Copia


-- INSERTAR Unidad_Medida_Producto_Transportados
PRINT 'Insertar Unidad_Medida_Producto_Transportados'
INSERT INTO Unidad_Medida_Producto_Transportados(EMPR_Codigo,Codigo,Nombre_Corto,Descripcion,Estado,Fecha_Crea,USUA_Codigo_Crea,Fecha_Modifica,USUA_Codigo_Modifica)
SELECT @EMPR_Codigo_Nueva,Codigo,Nombre_Corto,Descripcion,Estado,Fecha_Crea,USUA_Codigo_Crea,Fecha_Modifica,USUA_Codigo_Modifica
FROM Unidad_Medida_Producto_Transportados WHERE EMPR_Codigo = @EMPR_Codigo_Copia


 -- INSERTAR Validacion_Procesos
PRINT 'Insertar Validacion_Procesos'
INSERT INTO Validacion_Procesos(EMPR_Codigo,PROC_Codigo,Estado,USUA_Codigo_Crea,Fecha_Crea,USUA_Codigo_Modifica,Fecha_Modifica)
SELECT @EMPR_Codigo_Nueva,PROC_Codigo,Estado,USUA_Codigo_Crea,Fecha_Crea,USUA_Codigo_Modifica,Fecha_Modifica
FROM Validacion_Procesos WHERE EMPR_Codigo  = @EMPR_Codigo_Copia


 -- INSERTAR Aseguradoras y Empresas GPS
 PRINT 'Insertar Terceros Aseguradoras'
 INSERT INTO Terceros (EMPR_Codigo,Codigo,Codigo_Alterno,Codigo_Contable,CATA_TINT_Codigo,CATA_TIID_Codigo,Numero_Identificacion,Digito_Chequeo
,CIUD_Codigo_Identificacion,CIUD_Codigo_Nacimiento,Razon_Social,Representante_Legal,Nombre,Apellido1,Apellido2,CATA_SETE_Codigo,PAIS_Codigo
,CIUD_Codigo,Barrio,Direccion,Codigo_Postal,Telefonos,Celulares,Emails,Observaciones,Foto,Justificacion_Bloqueo,Reporto_Contabilidad
,Codigo_Retorno_Contabilidad,Mensaje_Retorno_Contabilidad,BANC_Codigo,CATA_TICB_Codigo,Numero_Cuenta_Bancaria,TERC_Codigo_Beneficiario
,Titular_Cuenta_Bancaria,Estado,USUA_Codigo_Crea,Fecha_Crea,USUA_Modifica,Fecha_Modifica,CATA_TIAN_Codigo,CATA_TIVC_Codigo,Cupo,Saldo)
SELECT @EMPR_Codigo_Nueva,Codigo,Codigo_Alterno,Codigo_Contable,CATA_TINT_Codigo,CATA_TIID_Codigo,Numero_Identificacion,Digito_Chequeo
,CIUD_Codigo_Identificacion,CIUD_Codigo_Nacimiento,Razon_Social,Representante_Legal,Nombre,Apellido1,Apellido2,CATA_SETE_Codigo,PAIS_Codigo
,CIUD_Codigo,Barrio,Direccion,Codigo_Postal,Telefonos,Celulares,Emails,Observaciones,Foto,Justificacion_Bloqueo,Reporto_Contabilidad
,Codigo_Retorno_Contabilidad,Mensaje_Retorno_Contabilidad,BANC_Codigo,CATA_TICB_Codigo,Numero_Cuenta_Bancaria,TERC_Codigo_Beneficiario
,Titular_Cuenta_Bancaria,Estado,USUA_Codigo_Crea,Fecha_Crea,USUA_Modifica,Fecha_Modifica,CATA_TIAN_Codigo,CATA_TIVC_Codigo,Cupo,Saldo
FROM Terceros WHERE EMPR_Codigo = @EMPR_Codigo_Copia and Codigo  BETWEEN  100 AND  138 


 -- INSERTAR Plan_Unico_Cuentas 
 -- Desactivar/Activar el IDENTITY del campo Codigo
SET IDENTITY_INSERT Plan_Unico_Cuentas  OFF
SET IDENTITY_INSERT Productos_Ministerio_Transporte  OFF
SET IDENTITY_INSERT Unidad_Empaque_Producto_Transportados  OFF
SET IDENTITY_INSERT Plan_Unico_Cuentas  ON

PRINT 'Insertar Plan_Unico_Cuentas'
INSERT INTO Plan_Unico_Cuentas(EMPR_Codigo,Codigo,CATA_CCPU_Codigo,Codigo_Cuenta,Codigo_Alterno,Nombre,Exige_Centro_Costo,Exige_Tercero
,Exige_Valor_Base,Valor_Base,Exige_Documento_Cruce,Estado,USUA_Codigo_Crea,Fecha_Crea,USUA_Modifica,Fecha_Modifica)
SELECT @EMPR_Codigo_Nueva,Codigo,CATA_CCPU_Codigo,Codigo_Cuenta,Codigo_Alterno,Nombre,Exige_Centro_Costo,Exige_Tercero
,Exige_Valor_Base,Valor_Base,Exige_Documento_Cruce,Estado,USUA_Codigo_Crea,Fecha_Crea,USUA_Modifica,Fecha_Modifica
FROM Plan_Unico_Cuentas WHERE EMPR_Codigo = @EMPR_Codigo_Copia


-- INSERT  Encabezado_Impuestos
PRINT 'Insertar Encabezado_Impuestos'
INSERT INTO Encabezado_Impuestos(EMPR_Codigo,Codigo,Codigo_Alterno,Nombre,Label,CATA_TRAI_Codigo,CATA_TIIM_Codigo,Operacion,PLUC_Codigo
,Valor_Tarifa,Valor_Base,Estado,USUA_Codigo_Crea,Fecha_Crea,USUA_Codigo_Modifica,Fecha_Modifica)
SELECT @EMPR_Codigo_Nueva,Codigo,Codigo_Alterno,Nombre,Label,CATA_TRAI_Codigo,CATA_TIIM_Codigo,Operacion,PLUC_Codigo
,Valor_Tarifa,Valor_Base,Estado,USUA_Codigo_Crea,Fecha_Crea,USUA_Codigo_Modifica,Fecha_Modifica
FROM Encabezado_Impuestos WHERE EMPR_Codigo = @EMPR_Codigo_Copia

-- INSERT Impuestos_Tipo_Documentos 
PRINT 'Insertar Impuestos_Tipo_Documentos'
INSERT INTO Impuestos_Tipo_Documentos(EMPR_Codigo,TIDO_Codigo,ENIM_Codigo,Estado)
SELECT @EMPR_Codigo_Nueva,TIDO_Codigo,ENIM_Codigo,Estado
FROM Impuestos_Tipo_Documentos where EMPR_Codigo = @EMPR_Codigo_Copia


-- INSERT Detalle_Departamento_Impuestos
PRINT 'Insertar Detalle_Departamento_Impuestos'
INSERT INTO Detalle_Departamento_Impuestos(EMPR_Codigo,ENIM_Codigo,DEPA_Codigo,PLUC_Codigo,Valor_Tarifa,Valor_Base,Estado
,USUA_Codigo_Crea,Fecha_Crea,USUA_Codigo_Modifica,Fecha_Modifica)
SELECT @EMPR_Codigo_Nueva,ENIM_Codigo,DEPA_Codigo,PLUC_Codigo,Valor_Tarifa,Valor_Base,
Estado,USUA_Codigo_Crea,Fecha_Crea,USUA_Codigo_Modifica,Fecha_Modifica
FROM Detalle_Departamento_Impuestos WHERE EMPR_Codigo = @EMPR_Codigo_Copia


-- INSERT Detalle_Ciudad_Impuestos 
PRINT 'Insertar Detalle_Ciudad_Impuestos'
INSERT INTO Detalle_Ciudad_Impuestos(EMPR_Codigo,ENIM_Codigo,CIUD_Codigo,PLUC_Codigo,Valor_Tarifa,Valor_Base,Estado
,USUA_Codigo_Crea,Fecha_Crea,USUA_Codigo_Modifica,Fecha_Modifica)
SELECT @EMPR_Codigo_Nueva,ENIM_Codigo,CIUD_Codigo,PLUC_Codigo,Valor_Tarifa,Valor_Base,Estado
,USUA_Codigo_Crea,Fecha_Crea,USUA_Codigo_Modifica,Fecha_Modifica
FROM Detalle_Ciudad_Impuestos WHERE EMPR_Codigo = @EMPR_Codigo_Copia



-- INSERT Encabezado_Concepto_Contables
 -- Desactivar/Activar el IDENTITY del campo Codigo
SET IDENTITY_INSERT Encabezado_Concepto_Contables  OFF
SET IDENTITY_INSERT Plan_Unico_Cuentas  OFF
SET IDENTITY_INSERT Encabezado_Concepto_Contables  ON

PRINT 'Insertar Encabezado_Concepto_Contables'
INSERT INTO Encabezado_Concepto_Contables(EMPR_Codigo,Codigo,Codigo_Alterno,Nombre,TIDO_Codigo,CATA_DOOR_Codigo,CATA_TICC_Codigo,CATA_TINA_Codigo,
OFIC_Codigo_Aplica,Observaciones,OFIC_Codigo,Fuente,Fuente_Anulacion,Estado,USUA_Codigo_Crea,Fecha_Crea,USUA_Codigo_Modifica,Fecha_Modifica)
SELECT @EMPR_Codigo_Nueva,Codigo,Codigo_Alterno,Nombre,TIDO_Codigo,CATA_DOOR_Codigo,CATA_TICC_Codigo,CATA_TINA_Codigo,OFIC_Codigo_Aplica,Observaciones,OFIC_Codigo
,Fuente,Fuente_Anulacion,Estado,USUA_Codigo_Crea,Fecha_Crea,USUA_Codigo_Modifica,Fecha_Modifica
FROM Encabezado_Concepto_Contables WHERE EMPR_Codigo = @EMPR_Codigo_Copia


-- INSERT Detalle_Concepto_Contables
 -- Desactivar/Activar el IDENTITY del campo Codigo
SET IDENTITY_INSERT Detalle_Concepto_Contables  OFF
SET IDENTITY_INSERT Encabezado_Concepto_Contables  OFF
SET IDENTITY_INSERT Detalle_Concepto_Contables  ON

PRINT 'Insertar Detalle_Concepto_Contables'
INSERT INTO Detalle_Concepto_Contables(EMPR_Codigo,Codigo,ECCO_Codigo,PLUC_Codigo,CATA_NACC_Codigo,CATA_TCUC_Codigo,CATA_DOCR_Codigo,CATA_TEPC_Codigo
,CATA_CCPC_Codigo,Valor_Base,Porcentaje_Tarifa,Genera_Cuenta,Prefijo,Codigo_Anexo,Sufijo_Codigo_Anexo,Campo_Auxiliar) 
SELECT @EMPR_Codigo_Nueva,Codigo,ECCO_Codigo,PLUC_Codigo,CATA_NACC_Codigo,CATA_TCUC_Codigo,CATA_DOCR_Codigo,CATA_TEPC_Codigo,CATA_CCPC_Codigo,Valor_Base
,Porcentaje_Tarifa,Genera_Cuenta,Prefijo,Codigo_Anexo,Sufijo_Codigo_Anexo,Campo_Auxiliar
FROM Detalle_Concepto_Contables WHERE EMPR_Codigo = @EMPR_Codigo_Copia



-- INSERT -	Cajas (Debe dejar el registro de Código 0-(NO APLICA)) (Apagar y Prender el Identity)
-- Desactivar/Activar el IDENTITY del campo Codigo
SET IDENTITY_INSERT Cajas  OFF
SET IDENTITY_INSERT Detalle_Concepto_Contables  OFF
SET IDENTITY_INSERT Cajas  ON

PRINT 'Insertar Cajas'
INSERT INTO Cajas(EMPR_Codigo,Codigo,Codigo_Alterno,Nombre,PLUC_Codigo,OFIC_Codigo,Estado
,USUA_Codigo_Crea,Fecha_Crea,USUA_Modifica,Fecha_Modifica)
SELECT @EMPR_Codigo_Nueva,Codigo,Codigo_Alterno,Nombre,PLUC_Codigo,OFIC_Codigo,Estado
,USUA_Codigo_Crea,Fecha_Crea,USUA_Modifica,Fecha_Modifica
FROM Cajas WHERE EMPR_Codigo = @EMPR_Codigo_Copia AND Codigo = 0


-- INSERTAR Conceptos_Ventas
-- Desactivar/Activar el IDENTITY del campo Codigo

SET IDENTITY_INSERT Conceptos_Ventas  OFF
SET IDENTITY_INSERT Cajas  OFF
SET IDENTITY_INSERT Conceptos_Ventas  ON

PRINT 'Insertar Conceptos_Ventas'
INSERT INTO Conceptos_Ventas(EMPR_Codigo,Codigo,Codigo_Alterno,Nombre,Operacion,Concepto_Sistema
,Estado,Fecha_Crea,USUA_Codigo_Crea,Fecha_Modifica,USUA_Codigo_Modifica)
SELECT @EMPR_Codigo_Nueva,Codigo,Codigo_Alterno,Nombre,Operacion,Concepto_Sistema
,Estado,Fecha_Crea,USUA_Codigo_Crea,Fecha_Modifica,USUA_Codigo_Modifica
FROM Conceptos_Ventas WHERE EMPR_Codigo = @EMPR_Codigo_Copia AND Codigo = 0



-- INSERT Conceptos_Liquidacion_Planilla_Despachos (Apagar y Prender el Identity)
-- Desactivar/Activar el IDENTITY del campo Codigo

SET IDENTITY_INSERT Conceptos_Liquidacion_Planilla_Despachos  OFF
SET IDENTITY_INSERT Conceptos_Ventas  OFF
SET IDENTITY_INSERT Conceptos_Liquidacion_Planilla_Despachos  ON

PRINT 'Insertar Conceptos_Liquidacion_Planilla_Despachos'
INSERT INTO Conceptos_Liquidacion_Planilla_Despachos(EMPR_Codigo,Codigo,Codigo_Alterno,Nombre,Operacion,PLUC_Codigo,Valor_Fijo,Valor_Porcentaje
,Concepto_Sistema,Aplica_Valor_Base_Impuestos,Estado,Fecha_Crea,USUA_Codigo_Crea,Fecha_Modifica,USUA_Codigo_Modifica)
SELECT @EMPR_Codigo_Nueva,Codigo,Codigo_Alterno,Nombre,Operacion,PLUC_Codigo,Valor_Fijo,Valor_Porcentaje
,Concepto_Sistema,Aplica_Valor_Base_Impuestos,Estado,Fecha_Crea,USUA_Codigo_Crea,Fecha_Modifica,USUA_Codigo_Modifica
FROM Conceptos_Liquidacion_Planilla_Despachos WHERE EMPR_Codigo = @EMPR_Codigo_Copia AND Codigo BETWEEN 0 AND 5



/*Insertar vehiculo --> Codigo = 0*/
INSERT INTO Vehiculos (
EMPR_Codigo,Codigo,Placa,Codigo_Alterno,SEMI_Codigo,CATA_TIDV_Codigo,TERC_Codigo_Propietario,TERC_Codigo_Tenedor
,TERC_Codigo_Conductor,TERC_Codigo_Afiliador,COVE_Codigo,MAVE_Codigo,LIVE_Codigo,Modelo,Modelo_Repotenciado,CATA_TIVE_Codigo,Cilindraje
,CATA_TICA_Codigo,Numero_Ejes,Numero_Motor,Numero_Serie,Tarjeta_Propiedad,Peso_Bruto,Peso_Tara,Capacidad_Kilos,Capacidad_Galones,Kilometraje
,Fecha_Actuliza_Kilometraje,ESSE_Numero,Fecha_Ultimo_Cargue,Estado,CATA_CAVE_Codigo,CATA_CAIV_Codigo,Justificacion_Bloqueo,TERC_Codigo_Proveedor_GPS
,URL_GPS,Usuario_GPS,Clave_GPS,Telefono_GPS,TERC_Codigo_Aseguradora_SOAT,Referencia_SOAT,Fecha_Vencimiento_SOAT,USUA_Codigo_Crea,Fecha_Crea
,USUA_Codigo_Modifica,Fecha_Modifica,Identificador_GPS,Reportar_RNDC,Capacidad_Volumen
)
SELECT 
@EMPR_Codigo_Nueva,Codigo,Placa,Codigo_Alterno,SEMI_Codigo,CATA_TIDV_Codigo,TERC_Codigo_Propietario,TERC_Codigo_Tenedor
,TERC_Codigo_Conductor,TERC_Codigo_Afiliador,COVE_Codigo,MAVE_Codigo,LIVE_Codigo,Modelo,Modelo_Repotenciado,CATA_TIVE_Codigo,Cilindraje
,CATA_TICA_Codigo,Numero_Ejes,Numero_Motor,Numero_Serie,Tarjeta_Propiedad,Peso_Bruto,Peso_Tara,Capacidad_Kilos,Capacidad_Galones,Kilometraje
,Fecha_Actuliza_Kilometraje,ESSE_Numero,Fecha_Ultimo_Cargue,Estado,CATA_CAVE_Codigo,CATA_CAIV_Codigo,Justificacion_Bloqueo,TERC_Codigo_Proveedor_GPS
,URL_GPS,Usuario_GPS,Clave_GPS,Telefono_GPS,TERC_Codigo_Aseguradora_SOAT,Referencia_SOAT,Fecha_Vencimiento_SOAT,USUA_Codigo_Crea,Fecha_Crea
,USUA_Codigo_Modifica,Fecha_Modifica,Identificador_GPS,Reportar_RNDC,Capacidad_Volumen
FROM Vehiculos WHERE EMPR_Codigo = @EMPR_Codigo_Copia AND Codigo = 0


/*Insertar Rutas --> Codigo = 0*/
INSERT INTO Rutas (
EMPR_Codigo,Codigo,Codigo_Alterno,Nombre,CIUD_Codigo_Origen,CIUD_Codigo_Destino,CATA_TIRU_Codigo,Duracion_Horas
,Porcentaje_Anticipo,Kilometros,Puntos,Estado,USUA_Codigo_Crea,Fecha_Crea,USUA_Codigo_Modifica,Fecha_Modifica
)
SELECT 
@EMPR_Codigo_Nueva,Codigo,Codigo_Alterno,Nombre,CIUD_Codigo_Origen,CIUD_Codigo_Destino,CATA_TIRU_Codigo,Duracion_Horas
,Porcentaje_Anticipo,Kilometros,Puntos,Estado,USUA_Codigo_Crea,Fecha_Crea,USUA_Codigo_Modifica,Fecha_Modifica
FROM Rutas WHERE EMPR_Codigo = @EMPR_Codigo_Copia AND Codigo = 0



-- INSERTAR Empresa_Manifiesto_Electronico
PRINT 'Empresa_Manifiesto_Electronico'
INSERT INTO Empresa_Manifiesto_Electronico(EMPR_Codigo,Codigo_Regional_Ministerio,Codigo_Empresa_Ministerio,Usuario_Manifiesto_Electronico,
Numero_Resolucion_Habilitacion_Empresa,Clave_Manifiesto_Electronico,CodigoMinisterioTransporte,Enlace_WS_Ministerio) 
SELECT @EMPR_Codigo_Nueva,Codigo_Regional_Ministerio,Codigo_Empresa_Ministerio,'ALTERNATIVA',
Numero_Resolucion_Habilitacion_Empresa,'ALTERNATIVA',CodigoMinisterioTransporte,Enlace_WS_Ministerio
FROM  Empresa_Manifiesto_Electronico WHERE EMPR_Codigo = @EMPR_Codigo_Copia


PRINT 'Empresa_Factura_Electronica'
INSERT INTO Empresa_Factura_Electronica(EMPR_Codigo,CATA_PRFE,Usuario,Clave,Operador_Virtual,Clave_Tecnica_Factura,Clave_Externa_Factura,
Prefijo_Nota_Credito,Clave_Externa_Nota_Credito,Prefijo_Factura,Dominio_Ftp,Ssh_Host_Key,Puerto_Ftp,ID_Empresa,CATA_TAFE_Codigo,Firma_Digital,
Prefijo_Nota_Debito,Clave_Externa_Nota_Debito)
SELECT @EMPR_Codigo_Nueva,CATA_PRFE,Usuario,Clave,Operador_Virtual,Clave_Tecnica_Factura,Clave_Externa_Factura,
Prefijo_Nota_Credito,Clave_Externa_Nota_Credito,Prefijo_Factura,Dominio_Ftp,Ssh_Host_Key,Puerto_Ftp,ID_Empresa,CATA_TAFE_Codigo,Firma_Digital,
Prefijo_Nota_Debito,Clave_Externa_Nota_Debito
FROM Empresa_Factura_Electronica WHERE EMPR_Codigo = @EMPR_Codigo_Copia



-- Insertar Perfil Terceros Aseguradoras = 1402
INSERT INTO Perfil_Terceros 
SELECT @EMPR_Codigo_Nueva, Codigo, 1402 FROM Terceros WHERE EMPR_Codigo = @EMPR_Codigo_Nueva AND Codigo >= 100 AND Codigo <= 138

/*INICIALIZACIÓN CONSECUTIVO*/
PRINT 'INICIALIZACIÓN CONSECUTIVO'
UPDATE Tipo_Documentos SET Consecutivo = (select CASE WHEN MAX(Codigo)+1 < 1000000 OR MAX(Codigo)+1 IS NULL THEN 1000000 ELSE MAX(Codigo)+1 END FROM Terceros WHERE EMPR_Codigo = @EMPR_Codigo_Nueva) WHERE  EMPR_Codigo = @EMPR_Codigo_Nueva AND Codigo = 10
UPDATE Tipo_Documentos SET Consecutivo = (select CASE WHEN MAX(Codigo)+1 < 1000000 OR MAX(Codigo)+1 IS NULL THEN 1000000 ELSE MAX(Codigo)+1 END FROM Vehiculos WHERE EMPR_Codigo = @EMPR_Codigo_Nueva) WHERE  EMPR_Codigo = @EMPR_Codigo_Nueva AND Codigo = 20
UPDATE Tipo_Documentos SET Consecutivo = (select CASE WHEN MAX(Numero)+1 < 1000000 OR MAX(Numero)+1 IS NULL THEN 1000000 ELSE MAX(Numero)+1 END FROM Encabezado_Documento_Comprobantes WHERE EMPR_Codigo = @EMPR_Codigo_Nueva and TIDO_Codigo = 30) WHERE  EMPR_Codigo = @EMPR_Codigo_Nueva AND Codigo = 30
UPDATE Tipo_Documentos SET Consecutivo = (select CASE WHEN MAX(Numero)+1 < 1000000 OR MAX(Numero)+1 IS NULL THEN 1000000 ELSE MAX(Numero)+1 END FROM Encabezado_Documento_Comprobantes WHERE EMPR_Codigo = @EMPR_Codigo_Nueva and TIDO_Codigo = 40) WHERE  EMPR_Codigo = @EMPR_Codigo_Nueva AND Codigo = 40
UPDATE Tipo_Documentos SET Consecutivo = (select CASE WHEN MAX(Numero_Documento)+1 < 1000000 OR MAX(Numero_Documento)+1 IS NULL THEN 1000000 ELSE MAX(Numero_Documento)+1 END FROM Encabezado_Comprobante_Contables WHERE EMPR_Codigo = @EMPR_Codigo_Nueva) WHERE  EMPR_Codigo = @EMPR_Codigo_Nueva AND Codigo = 45
UPDATE Tipo_Documentos SET Consecutivo = (select CASE WHEN MAX(Codigo)+1 < 1000000 OR MAX(Codigo)+1 IS NULL THEN 1000000 ELSE MAX(Codigo)+1 END FROM Semirremolques WHERE EMPR_Codigo = @EMPR_Codigo_Nueva) WHERE  EMPR_Codigo = @EMPR_Codigo_Nueva AND Codigo = 50
UPDATE Tipo_Documentos SET Consecutivo = (select CASE WHEN MAX(Numero)+1 < 1000000 OR MAX(Numero)+1 IS NULL THEN 1000000 ELSE MAX(Numero)+1 END FROM Encabezado_Tarifario_Carga_Ventas WHERE EMPR_Codigo = @EMPR_Codigo_Nueva) WHERE  EMPR_Codigo = @EMPR_Codigo_Nueva AND Codigo = 60
UPDATE Tipo_Documentos SET Consecutivo = (select CASE WHEN MAX(Numero)+1 < 1000000 OR MAX(Numero)+1 IS NULL THEN 1000000 ELSE MAX(Numero)+1 END FROM Encabezado_Tarifario_Carga_Compras WHERE EMPR_Codigo = @EMPR_Codigo_Nueva) WHERE  EMPR_Codigo = @EMPR_Codigo_Nueva AND Codigo = 70
UPDATE Tipo_Documentos SET Consecutivo = (select CASE WHEN MAX(Numero)+1 < 1000000 OR MAX(Numero)+1 IS NULL THEN 1000000 ELSE MAX(Numero)+1 END FROM Encabezado_Documento_Cuentas WHERE EMPR_Codigo = @EMPR_Codigo_Nueva and TIDO_Codigo = 80) WHERE  EMPR_Codigo = @EMPR_Codigo_Nueva AND Codigo = 80
UPDATE Tipo_Documentos SET Consecutivo = (select CASE WHEN MAX(Numero)+1 < 1000000 OR MAX(Numero)+1 IS NULL THEN 1000000 ELSE MAX(Numero)+1 END FROM Encabezado_Documento_Cuentas WHERE EMPR_Codigo = @EMPR_Codigo_Nueva and TIDO_Codigo = 85) WHERE  EMPR_Codigo = @EMPR_Codigo_Nueva AND Codigo = 85
UPDATE Tipo_Documentos SET Consecutivo = (select CASE WHEN MAX(Numero_Documento)+1 < 1000000 OR MAX(Numero_Documento)+1 IS NULL THEN 1000000 ELSE MAX(Numero_Documento)+1 END FROM Encabezado_Solicitud_Orden_Servicios WHERE EMPR_Codigo = @EMPR_Codigo_Nueva and TIDO_Codigo = 90) WHERE  EMPR_Codigo = @EMPR_Codigo_Nueva AND Codigo = 90
UPDATE Tipo_Documentos SET Consecutivo = (select CASE WHEN MAX(Numero_Documento)+1 < 1000000 OR MAX(Numero_Documento)+1 IS NULL THEN 1000000 ELSE MAX(Numero_Documento)+1 END FROM Encabezado_Solicitud_Orden_Servicios WHERE EMPR_Codigo = @EMPR_Codigo_Nueva and TIDO_Codigo = 91) WHERE  EMPR_Codigo = @EMPR_Codigo_Nueva AND Codigo = 91
UPDATE Tipo_Documentos SET Consecutivo = (select CASE WHEN MAX(Numero_Documento)+1 < 1000000 OR MAX(Numero_Documento)+1 IS NULL THEN 1000000 ELSE MAX(Numero_Documento)+1 END FROM Encabezado_Orden_Cargues WHERE EMPR_Codigo = @EMPR_Codigo_Nueva) WHERE  EMPR_Codigo = @EMPR_Codigo_Nueva AND Codigo = 95
UPDATE Tipo_Documentos SET Consecutivo = (select CASE WHEN MAX(Numero_Documento)+1 < 1000000 OR MAX(Numero_Documento)+1 IS NULL THEN 1000000 ELSE MAX(Numero_Documento)+1 END FROM Encabezado_Remesas WHERE EMPR_Codigo = @EMPR_Codigo_Nueva and TIDO_Codigo = 100) WHERE  EMPR_Codigo = @EMPR_Codigo_Nueva AND Codigo = 100
UPDATE Tipo_Documentos SET Consecutivo = (select CASE WHEN MAX(Numero_Documento)+1 < 1000000 OR MAX(Numero_Documento)+1 IS NULL THEN 1000000 ELSE MAX(Numero_Documento)+1 END FROM Encabezado_Remesas WHERE EMPR_Codigo = @EMPR_Codigo_Nueva and TIDO_Codigo = 110) WHERE  EMPR_Codigo = @EMPR_Codigo_Nueva AND Codigo = 110
UPDATE Tipo_Documentos SET Consecutivo = (select CASE WHEN MAX(Numero_Documento)+1 < 1000000 OR MAX(Numero_Documento)+1 IS NULL THEN 1000000 ELSE MAX(Numero_Documento)+1 END FROM Encabezado_Planilla_Despachos WHERE EMPR_Codigo = @EMPR_Codigo_Nueva AND TIDO_Codigo = 130) WHERE  EMPR_Codigo = @EMPR_Codigo_Nueva AND Codigo = 130
UPDATE Tipo_Documentos SET Consecutivo = (select CASE WHEN MAX(Numero_Documento)+1 < 1000000 OR MAX(Numero_Documento)+1 IS NULL THEN 1000000 ELSE MAX(Numero_Documento)+1 END FROM Encabezado_Manifiesto_Carga WHERE EMPR_Codigo = @EMPR_Codigo_Nueva) WHERE  EMPR_Codigo = @EMPR_Codigo_Nueva AND Codigo = 140
UPDATE Tipo_Documentos SET Consecutivo = (select CASE WHEN MAX(Numero_Documento)+1 < 1000000 OR MAX(Numero_Documento)+1 IS NULL THEN 1000000 ELSE MAX(Numero_Documento)+1 END FROM Encabezado_Planilla_Despachos WHERE EMPR_Codigo = @EMPR_Codigo_Nueva AND TIDO_Codigo = 150) WHERE  EMPR_Codigo = @EMPR_Codigo_Nueva AND Codigo = 150
UPDATE Tipo_Documentos SET Consecutivo = (select CASE WHEN MAX(Numero_Documento)+1 < 1000000 OR MAX(Numero_Documento)+1 IS NULL THEN 1000000 ELSE MAX(Numero_Documento)+1 END FROM Encabezado_Cumplido_Planilla_Despachos WHERE EMPR_Codigo = @EMPR_Codigo_Nueva AND TIDO_Codigo = 155) WHERE  EMPR_Codigo = @EMPR_Codigo_Nueva AND Codigo = 155
UPDATE Tipo_Documentos SET Consecutivo = (select CASE WHEN MAX(Numero_Documento)+1 < 1000000 OR MAX(Numero_Documento)+1 IS NULL THEN 1000000 ELSE MAX(Numero_Documento)+1 END FROM Encabezado_Liquidacion_Planilla_Despachos WHERE EMPR_Codigo = @EMPR_Codigo_Nueva ) WHERE  EMPR_Codigo = @EMPR_Codigo_Nueva AND Codigo = 160
UPDATE Tipo_Documentos SET Consecutivo = (select CASE WHEN MAX(Numero_Documento)+1 < 1000000 OR MAX(Numero_Documento)+1 IS NULL THEN 1000000 ELSE MAX(Numero_Documento)+1 END FROM Encabezado_Facturas WHERE EMPR_Codigo = @EMPR_Codigo_Nueva  AND TIDO_Codigo = 170) WHERE  EMPR_Codigo = @EMPR_Codigo_Nueva AND Codigo = 170
UPDATE Tipo_Documentos SET Consecutivo = (select CASE WHEN MAX(Numero_Documento)+1 < 1000000 OR MAX(Numero_Documento)+1 IS NULL THEN 1000000 ELSE MAX(Numero_Documento)+1 END FROM Encabezado_Facturas WHERE EMPR_Codigo = @EMPR_Codigo_Nueva  AND TIDO_Codigo = 175) WHERE  EMPR_Codigo = @EMPR_Codigo_Nueva AND Codigo = 175
UPDATE Tipo_Documentos SET Consecutivo = (select CASE WHEN MAX(Numero_Documento)+1 < 1000000 OR MAX(Numero_Documento)+1 IS NULL THEN 1000000 ELSE MAX(Numero_Documento)+1 END FROM Encabezado_Recolecciones WHERE EMPR_Codigo = @EMPR_Codigo_Nueva ) WHERE  EMPR_Codigo = @EMPR_Codigo_Nueva AND Codigo = 200
--UPDATE Tipo_Documentos SET Consecutivo = (select CASE WHEN MAX(Numero_Documento)+1 < 1000000 OR MAX(Numero_Documento)+1 IS NULL THEN 1000000 ELSE MAX(Numero_Documento)+1 END FROM Encabezado_Planilla_Recolecciones WHERE EMPR_Codigo = @EMPR_Codigo ) WHERE  EMPR_Codigo = @EMPR_Codigo AND Codigo = 205
--UPDATE Tipo_Documentos SET Consecutivo = (select CASE WHEN MAX(Numero_Documento)+1 < 1000000 OR MAX(Numero_Documento)+1 IS NULL THEN 1000000 ELSE MAX(Numero_Documento)+1 END FROM Encabezado_Planilla_Entregas WHERE EMPR_Codigo = @EMPR_Codigo ) WHERE  EMPR_Codigo = @EMPR_Codigo AND Codigo = 210
UPDATE Tipo_Documentos SET Consecutivo = (select CASE WHEN MAX(Numero_Documento)+1 < 1000000 OR MAX(Numero_Documento)+1 IS NULL THEN 1000000 ELSE MAX(Numero_Documento)+1 END FROM Encabezado_Estudio_Seguridad WHERE EMPR_Codigo = @EMPR_Codigo_Nueva ) WHERE  EMPR_Codigo = @EMPR_Codigo_Nueva AND Codigo = 220
UPDATE Tipo_Documentos SET Consecutivo = (select CASE WHEN MAX(Numero)+1 < 1000000 OR MAX(Numero)+1 IS NULL THEN 1000000 ELSE MAX(Numero)+1 END FROM Encabezado_Inspecciones WHERE EMPR_Codigo = @EMPR_Codigo_Nueva ) WHERE  EMPR_Codigo = @EMPR_Codigo_Nueva AND Codigo = 240


-- INSERTAR NOVEDADES DESPACHO
INSERT INTO Novedades_Despacho (EMPR_Codigo, Codigo, Codigo_Alterno, Nombre, Novedad_Sistema, Estado, Fecha_Crea, USUA_Codigo_Crea, Fecha_Modifica, USUA_Codigo_Modifica)
SELECT @EMPR_Codigo_Nueva, Codigo, Codigo_Alterno, Nombre, Novedad_Sistema, Estado, getdate(), 1, Null, Null
FROM Novedades_Despacho
WHERE EMPR_Codigo = @EMPR_Codigo_Copia
AND Codigo IN (13, 15)


-- ELIMINAR Sitios Cargue Descargue
PRINT 'Eliminar Sitios_cargue_descargue' 
DELETE Sitios_Cargue_Descargue WHERE EMPR_Codigo = @EMPR_Codigo_Nueva


--eliminando la copia de la tabla Detalle_Ciudad_Impuestos, ya que esta debe arrancar vacia
PRINT 'Eliminar Detalle_Ciudad_Impuestos' 
DELETE Detalle_Ciudad_Impuestos WHERE EMPR_Codigo = @EMPR_Codigo_Nueva

/*insertar todos los Procesos definidos en la tabla Procesos 1-32 
y todos estos Procesos en la tabla Validacion_Procesos*/

--PRINT 'INSERTAR PROCESOS'
--INSERT INTO Procesos VALUES (
--Codigo,Nombre,Descripcion,Nombre_Crea,Fecha_Crea,Nombre_Modifica,Fecha_Modifica
--)SELECT 
--Codigo,Nombre,Descripcion,Nombre_Crea,Fecha_Crea,Nombre_Modifica,Fecha_Modifica
--FROM procesos

/*Eliminar usuarios empresa Base*/
DELETE FROM Usuarios 
WHERE EMPR_Codigo = @EMPR_Codigo_Nueva  AND codigo <> 1 and codigo <> 0

-- Colocar Nuevo Nombre Empresa y en Tercero Código 1
UPDATE Empresas SET Nombre_Razon_Social = 'AGROPAISA SAS', Nombre_Corto_Razon_Social = 'AGROPAISA SAS',Numero_Identificacion = '123456789', Digito_Chequeo = 6, Prefijo = 'AGROP'
WHERE Codigo = @EMPR_Codigo_Nueva

-- Actualizar el NIT Nueva Empresa en Terceros Codigo 1
UPDATE Terceros SET Razon_Social = 'AGROPAISA SAS', Representante_Legal = 'AGROPAISA SAS', Numero_Identificacion = '123456789', Digito_Chequeo = 6
WHERE EMPR_Codigo = @EMPR_Codigo_Nueva
AND Codigo = 1



