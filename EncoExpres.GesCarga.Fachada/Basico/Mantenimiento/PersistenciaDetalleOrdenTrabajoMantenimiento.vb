﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Mantenimiento
Imports EncoExpres.GesCarga.Repositorio.Mantenimiento

Namespace Mantenimiento

    Public NotInheritable Class PersistenciaDetalleOrdenTrabajoMantenimiento
        Implements IPersistenciaBase(Of DetalleOrdenTrabajoMantenimiento)
        Public Function Consultar(filtro As DetalleOrdenTrabajoMantenimiento) As IEnumerable(Of DetalleOrdenTrabajoMantenimiento) Implements IPersistenciaBase(Of DetalleOrdenTrabajoMantenimiento).Consultar
            Return New RepositorioDetalleOrdenTrabajoMantenimiento().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As DetalleOrdenTrabajoMantenimiento) As Long Implements IPersistenciaBase(Of DetalleOrdenTrabajoMantenimiento).Insertar
            Return New RepositorioDetalleOrdenTrabajoMantenimiento().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As DetalleOrdenTrabajoMantenimiento) As Long Implements IPersistenciaBase(Of DetalleOrdenTrabajoMantenimiento).Modificar
            Return New RepositorioDetalleOrdenTrabajoMantenimiento().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As DetalleOrdenTrabajoMantenimiento) As DetalleOrdenTrabajoMantenimiento Implements IPersistenciaBase(Of DetalleOrdenTrabajoMantenimiento).Obtener
            Return New RepositorioDetalleOrdenTrabajoMantenimiento().Obtener(filtro)
        End Function

        Public Function Anular(entidad As DetalleOrdenTrabajoMantenimiento) As Boolean
            Return New RepositorioDetalleOrdenTrabajoMantenimiento().Anular(entidad)
        End Function

    End Class
End Namespace
