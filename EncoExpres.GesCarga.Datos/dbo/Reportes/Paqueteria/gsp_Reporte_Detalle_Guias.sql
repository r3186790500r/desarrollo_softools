﻿PRINT 'gsp_Reporte_Detalle_Guias'
GO
DROP PROCEDURE gsp_Reporte_Detalle_Guias
GO
CREATE PROCEDURE gsp_Reporte_Detalle_Guias(  
@par_EMPR_Codigo NUMERIC,    
@par_Numero_Planilla  NUMERIC
) 
  
  AS        
  BEGIN  
  SELECT DISTINCT
      
  DGPR.EMPR_Codigo AS EMPR_Codigo_Guias
  ,DGPR.ENPR_Numero AS ENPR_Numero_Guias
  ,DGPR.ENRE_Numero AS ENRE_Numero_Guias
  ,ENRE.Numero_Documento AS Numero_Documento_Guias         
  ,ENRE.Fecha AS Fecha_Guias         
  ,ISNULL(ENRE.TERC_Codigo_Cliente, 0) AS TERC_Codigo_Cliente_Guias   
  ,ISNULL(TCLI.Nombre,'') + ' ' + ISNULL(TCLI.Apellido1, '')+ ' ' + ISNULL(TCLI.Apellido2,'')+ ' ' + ISNULL(TCLI.Razon_Social,'') AS Nombre_Cliente_Guias
  ,ISNULL(ENRE.RUTA_Codigo, 0) AS RUTA_Codigo_Guias
  ,ISNULL(RUTA.Nombre, '') AS Nombre_Ruta_Guias
  ,ISNULL(ENRE.Peso_Cliente,0) AS Peso_Cliente_Guias   
  ,ISNULL(ENRE.Cantidad_Cliente,0) AS Cantidad_Cliente_Guias  
  ,ISNULL(ENRE.TERC_Codigo_Remitente, 0) AS TERC_Codigo_Remitente_Guias   
  ,ISNULL(TREM.Nombre,'') + ' ' + ISNULL(TREM.Apellido1, '')+ ' ' + ISNULL(TREM.Apellido2,'')+ ' ' + ISNULL(TREM.Razon_Social,'') AS Nombre_Remitente_Guias 
  ,ISNULL(ENRE.TERC_Codigo_Destinatario, 0) AS TERC_Codigo_Destinatario   
  ,ISNULL(TDES.Nombre,'') + ' ' + ISNULL(TDES.Apellido1, '')+ ' ' + ISNULL(TDES.Apellido2,'')+ ' ' + ISNULL(TDES.Razon_Social,'') AS Nombre_Destinatario_Guias 
  ,ISNULL(ENRE.CIUD_Codigo_Destinatario, 0) AS CIUD_Codigo_Destinatario_Guias    
  ,ISNULL(CIUD.Nombre, '') AS Nombre_Ciudad_Guias 	
  ,ISNULL(ENRE.PRTR_Codigo, 0) AS PRTR_Codigo_Guias    
  ,ISNULL(PRTR.Nombre, '') AS Nombre_Producto_Transportados_Guias  
  ,ISNULL(ENRE.Direccion_Destinatario, '') AS Direccion_Destinatario_Guias    
  ,ISNULL(ENRE.Telefonos_Destinatario, '') AS Telefonos_Destinatario_Guias 
  ,ISNULL(ENRE.Observaciones, '') AS Observaciones_Guias
               
  FROM         
    	  
      Detalle_Guias_Planilla_Recolecciones DGPR,
	  Encabezado_Remesas ENRE,
	  Encabezado_Planilla_Recolecciones ENPR,
	  Terceros TCLI,
	  Terceros TREM,
	  Terceros TDES,
	  Producto_Transportados PRTR,
	  Ciudades CIUD,
	  Rutas RUTA
	 
  WHERE

     DGPR.EMPR_Codigo = ENRE.EMPR_Codigo                      
     AND DGPR.ENRE_Numero = ENRE.Numero  
                     
     AND ENRE.EMPR_Codigo = TCLI.EMPR_Codigo                      
     AND ENRE.TERC_Codigo_Cliente = TCLI.Codigo   

	 AND ENRE.EMPR_Codigo = TREM.EMPR_Codigo                      
     AND ENRE.TERC_Codigo_Remitente = TREM.Codigo   

	 AND ENRE.EMPR_Codigo = TDES.EMPR_Codigo                      
     AND ENRE.TERC_Codigo_Destinatario = TDES.Codigo 

	 AND ENRE.EMPR_Codigo = CIUD.EMPR_Codigo                      
     AND ENRE.CIUD_Codigo_Destinatario = CIUD.Codigo 

	 AND ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                      
     AND ENRE.PRTR_Codigo = PRTR.Codigo 
	 
	 AND ENRE.EMPR_Codigo = RUTA.EMPR_Codigo                      
     AND ENRE.RUTA_Codigo = RUTA.Codigo   
	 
	 AND DGPR.EMPR_Codigo = @par_EMPR_Codigo  
     AND DGPR.ENPR_Numero = @par_Numero_Planilla     
      
END  
GO