﻿Imports System.Xml
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Despachos.Procesos.General
Namespace Despachos.Procesos
    Public Class GestionReporteRemesaRNDC
#Region "Propiedades"

        Public Remesa As RemesasRNDC

        Public Const TIPO_REVERSA_REMESA_ANULACION As String = "A"
        Public Const TIPO_REVERSA_REMESA_LIBERAR_MANIFIESTO As String = "L"

        Public REMCIUDAD_DESTI_TRANSBORDO As String
        Public REMMOTIVOTRANSBORDO As String
        Public REMREMITENTE As String

        Const NOMBRE_TAG As String = "variables"

        Dim strSQL As String
        Dim solicitud As Solicitud
        Dim objGeneral As General
        Dim acceso As Acceso
        Dim wsManifiestoElectronico As ManifiestoElectronico.IBPMServicesservice
        Dim strRespuesta As String

        Public Property EnlaceMinisterio As String
            Get
                Return Me.wsManifiestoElectronico.Url
            End Get
            Set(value As String)
                Me.wsManifiestoElectronico.Url = value
            End Set
        End Property

#End Region

#Region "Constantes"
        Public Const NOMBRE_TAG_CONSULTA As String = "documento"
        Public Const ERROR_CIUDAD_DESTINATARIO_IGUAL_CIUDAD_REMITENTE As String = "La ciudad del remitente no puede ser igual a la ciudad del destinatario. Solamente pueden ser iguales cuando la naturaleza"
#End Region

#Region "Constructor"
        Sub New(ByVal Empresa As Empresas, ByVal acceso As Acceso, ByVal solicitud As Solicitud, ByVal Remesa As RemesasRNDC)
            Me.solicitud = New Solicitud
            Me.acceso = New Acceso
            objGeneral = New General
            Me.Remesa = Remesa
            AjustarValoresParaMinisterio()

            wsManifiestoElectronico = New ManifiestoElectronico.IBPMServicesservice
            Me.solicitud = solicitud
            Me.acceso = acceso
            Me.EnlaceMinisterio = Empresa.ListadoRNDC.EnlaceWSMinisterio
        End Sub

        Sub New()
            solicitud = New Solicitud
            acceso = New Acceso
            objGeneral = New General
            wsManifiestoElectronico = New ManifiestoElectronico.IBPMServicesservice
        End Sub
#End Region

#Region "Funciones Publicas"
        Public Sub AjustarValoresParaMinisterio()

            Dim dteAxiliarInicial As Date = Date.Now
            Dim dteAxiliarFinal As Date = Date.Now
            Date.TryParse(Remesa.FechaRemesa, dteAxiliarInicial)
            Date.TryParse(Remesa.FechaRemesa, dteAxiliarFinal)

            dteAxiliarFinal = dteAxiliarFinal.AddDays(4) 'Se agregan 4 dias para evitar conflictos con fechas
            dteAxiliarInicial = dteAxiliarInicial.AddMinutes(15) 'Se agrega 15 minutos para validar las 00 horas
            dteAxiliarFinal = dteAxiliarFinal.AddMinutes(15) 'Se agrega 15 minutos para validar las 00 horas

            Remesa.FECHACITAPACTADACARGUE = Format(dteAxiliarInicial.Date, "dd/MM/yyyy")
            Remesa.HORACITAPACTADACARGUE = dteAxiliarInicial.ToString("HH:mm")
            Remesa.FECHACITAPACTADADESCARGUE = Format(dteAxiliarFinal.Date, "dd/MM/yyyy")
            Remesa.HORACITAPACTADADESCARGUEREMESA = dteAxiliarFinal.ToString("HH:mm")

            If Remesa.DESCRIPCIONCORTAPRODUCTO.Length >= 60 Then
                Remesa.DESCRIPCIONCORTAPRODUCTO = Remesa.DESCRIPCIONCORTAPRODUCTO.Substring(0, 59)
            End If

        End Sub

        Public Sub Crear_XML_Remesa(ByRef XmlDocumento As XmlDocument, ByVal TipoFormato As Tipos_Procesos, ByRef lstVariablesDestinatario As List(Of String))
            Try

                Dim xmlElementoRoot As XmlElement = XmlDocumento.DocumentElement
                Dim NuevoElemento As XmlElement = XmlDocumento.CreateElement(NOMBRE_TAG)
                xmlElementoRoot.AppendChild(NuevoElemento)

                Select Case TipoFormato
                    Case Tipos_Procesos.FORMATO_INGRESO
                        If Remesa.NUMNITEMPRESATRANSPORTE <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMNITEMPRESATRANSPORTE", Remesa.NUMNITEMPRESATRANSPORTE)
                        End If
                        If Remesa.CONSECUTIVOREMESA <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CONSECUTIVOREMESA", Remesa.CONSECUTIVOREMESA)
                        End If
                        If Remesa.CONSECUTIVOINFORMACIONCARGA <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CONSECUTIVOINFORMACIONCARGA", Remesa.CONSECUTIVOINFORMACIONCARGA)
                        End If
                        If Remesa.CONSECUTIVOCARGADIVIDIDA <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CONSECUTIVOCARGADIVIDIDA", Remesa.CONSECUTIVOCARGADIVIDIDA)
                        End If
                        If Remesa.CODOPERACIONTRANSPORTE <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODOPERACIONTRANSPORTE", Remesa.CODOPERACIONTRANSPORTE)
                        End If
                        If Remesa.CODNATURALEZACARGA <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODNATURALEZACARGA", Remesa.CODNATURALEZACARGA)
                        End If
                        If Remesa.CODNATURALEZACARGA = 3 Or Remesa.CODNATURALEZACARGA = 4 Then
                            If Remesa.PERMISOCARGAEXTRA <> String.Empty Then
                                AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "PERMISOCARGAEXTRA", Remesa.PERMISOCARGAEXTRA)
                            End If
                        End If
                        If Remesa.CANTIDADCARGADA <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CANTIDADCARGADA", Val(Remesa.CANTIDADCARGADA))
                        Else
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CANTIDADCARGADA", CERO)
                        End If
                        If Remesa.UNIDADMEDIDACAPACIDAD <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "UNIDADMEDIDACAPACIDAD", Remesa.UNIDADMEDIDACAPACIDAD)
                        End If
                        If Remesa.CODTIPOEMPAQUE <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODTIPOEMPAQUE", Remesa.CODTIPOEMPAQUE)
                        End If
                        If Remesa.PESOCONTENEDORVACIO <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "PESOCONTENEDORVACIO", Remesa.PESOCONTENEDORVACIO)
                        End If
                        If Remesa.MERCANCIAREMESA <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "MERCANCIAREMESA", Remesa.MERCANCIAREMESA)
                        End If
                        If Remesa.DESCRIPCIONCORTAPRODUCTO <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "DESCRIPCIONCORTAPRODUCTO", Remesa.DESCRIPCIONCORTAPRODUCTO)
                        End If
                        If Remesa.Remitente.CODTIPOIDTERCERO <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODTIPOIDREMITENTE", Remesa.Remitente.CODTIPOIDTERCERO)
                        End If
                        If Remesa.Remitente.NUMIDTERCERO <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMIDREMITENTE", Remesa.Remitente.NUMIDTERCERO & Remesa.Remitente.DIGITOCHEQUEO)
                        End If
                        If Remesa.Remitente.CODSEDETERCERO <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODSEDEREMITENTE", Remesa.Remitente.CODSEDETERCERO)
                        End If
                        If Remesa.Destinatario.CODTIPOIDTERCERO <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODTIPOIDDESTINATARIO", Remesa.Destinatario.CODTIPOIDTERCERO)
                        End If
                        If Remesa.Destinatario.NUMIDTERCERO <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMIDDESTINATARIO", Remesa.Destinatario.NUMIDTERCERO & Remesa.Destinatario.DIGITOCHEQUEO)
                        End If
                        If Remesa.Destinatario.CODSEDETERCERO <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODSEDEDESTINATARIO", Remesa.Destinatario.CODSEDETERCERO)
                        End If
                        If Remesa.DUENOPOLIZA <> String.Empty Then 'vacio
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "DUENOPOLIZA", Remesa.DUENOPOLIZA)
                        End If
                        If Remesa.NUMPOLIZATRANSPORTE <> String.Empty Then 'vacio
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMPOLIZATRANSPORTE", Remesa.NUMPOLIZATRANSPORTE)
                        End If
                        If Remesa.COMPANIASEGURO <> String.Empty Then 'vacio
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "COMPANIASEGURO", Remesa.COMPANIASEGURO)
                        End If
                        If Remesa.FECHAVENCIMIENTOPOLIZACARGA <> String.Empty Then 'vacio
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "FECHAVENCIMIENTOPOLIZACARGA", Remesa.FECHAVENCIMIENTOPOLIZACARGA)
                        End If
                        If Remesa.FECHALLEGADACARGUE <> String.Empty Then 'vacio
                        End If
                        If Remesa.HORALLEGADACARGUEREMESA <> String.Empty Then 'vacio
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "HORALLEGADACARGUEREMESA", Remesa.HORALLEGADACARGUEREMESA)
                        End If
                        If Remesa.FECHAENTRADACARGUE <> String.Empty Then 'vacio
                        End If
                        If Remesa.HORAENTRADACARGUEREMESA <> String.Empty Then 'vacio
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "HORAENTRADACARGUEREMESA", Remesa.HORAENTRADACARGUEREMESA)
                        End If
                        If Remesa.FECHASALIDACARGUE <> String.Empty Then 'vacio
                        End If
                        If Remesa.HORASALIDACARGUEREMESA <> String.Empty Then 'vacio
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "HORASALIDACARGUEREMESA", Remesa.HORASALIDACARGUEREMESA)
                        End If
                        REM: Fechas Cargues y pactadas
                        If Remesa.HORASPACTOCARGA <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "HORASPACTOCARGA", Remesa.HORASPACTOCARGA)
                        End If
                        If Remesa.MINUTOSPACTOCARGA <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "MINUTOSPACTOCARGA", Remesa.MINUTOSPACTOCARGA)
                        End If
                        If Remesa.HORASPACTODESCARGUE <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "HORASPACTODESCARGUE", Remesa.HORASPACTODESCARGUE)
                        End If
                        If Remesa.MINUTOSPACTODESCARGUE <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "MINUTOSPACTODESCARGUE", Remesa.MINUTOSPACTODESCARGUE)
                        End If
                        If Remesa.FECHACITAPACTADACARGUE <> String.Empty Then 'fecha ini remesa
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "FECHACITAPACTADACARGUE", Remesa.FECHACITAPACTADACARGUE)
                        End If
                        If Remesa.HORACITAPACTADACARGUE <> String.Empty Then 'hora ini remesa
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "HORACITAPACTADACARGUE", Remesa.HORACITAPACTADACARGUE)
                        End If
                        AjustarValoresParaMinisterio()
                        If Remesa.FECHACITAPACTADADESCARGUE <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "FECHACITAPACTADADESCARGUE", Remesa.FECHACITAPACTADADESCARGUE)
                        End If
                        If Remesa.HORACITAPACTADADESCARGUEREMESA <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "HORACITAPACTADADESCARGUEREMESA", Remesa.HORACITAPACTADADESCARGUEREMESA)
                        End If
                        REM: Fechas Cargues y pactadas
                        If Remesa.Remitente.CODTIPOIDTERCERO <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODTIPOIDPROPIETARIO", Remesa.Remitente.CODTIPOIDTERCERO)
                        End If
                        If Remesa.Remitente.NUMIDTERCERO <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMIDPROPIETARIO", Remesa.Remitente.NUMIDTERCERO & Remesa.Remitente.DIGITOCHEQUEO)
                        End If
                        If Remesa.Remitente.CODSEDETERCERO <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODSEDEPROPIETARIO", Remesa.Remitente.CODSEDETERCERO)
                        End If
                    Case Tipos_Procesos.FORTMATO_CONSULTA_PROCESOS
                        Dim strAxuliarCampos As String = String.Empty
                        For Each Items As String In lstVariablesDestinatario
                            strAxuliarCampos &= Items
                        Next
                        If strAxuliarCampos <> String.Empty Then strAxuliarCampos = "," & strAxuliarCampos
                        xmlElementoRoot = XmlDocumento.DocumentElement
                        Dim txtNodo As XmlText = XmlDocumento.CreateTextNode("ingresoid" & strAxuliarCampos)
                        xmlElementoRoot.LastChild.AppendChild(txtNodo)

                        NuevoElemento = XmlDocumento.CreateElement(NOMBRE_TAG_CONSULTA)
                        xmlElementoRoot.AppendChild(NuevoElemento)
                        xmlElementoRoot = XmlDocumento.DocumentElement

                        xmlElementoRoot = XmlDocumento.DocumentElement
                        NuevoElemento = XmlDocumento.CreateElement("NUMNITEMPRESATRANSPORTE")
                        txtNodo = XmlDocumento.CreateTextNode(Remesa.NUMNITEMPRESATRANSPORTE)
                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

                        xmlElementoRoot = XmlDocumento.DocumentElement
                        NuevoElemento = XmlDocumento.CreateElement("CONSECUTIVOREMESA")
                        txtNodo = XmlDocumento.CreateTextNode("'" & Remesa.CONSECUTIVOREMESA & "'")
                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

                End Select
            Catch ex As Exception

            End Try
        End Sub

        Public Sub Crear_XML_Cumplido_Remesa(ByRef XmlDocumento As XmlDocument, ByVal TipoFormato As Tipos_Procesos, ByRef lstVariablesDestinatario As List(Of String))
            Try

                Dim xmlElementoRoot As XmlElement = XmlDocumento.DocumentElement
                Dim NuevoElemento As XmlElement = XmlDocumento.CreateElement(NOMBRE_TAG)
                xmlElementoRoot.AppendChild(NuevoElemento)

                Select Case TipoFormato
                    Case Tipos_Procesos.FORMATO_INGRESO
                        If Remesa.NUMNITEMPRESATRANSPORTE <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMNITEMPRESATRANSPORTE", Remesa.NUMNITEMPRESATRANSPORTE)
                        End If
                        If Remesa.CONSECUTIVOREMESA <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CONSECUTIVOREMESA", Remesa.CONSECUTIVOREMESA)
                        End If
                        If Remesa.CONSECUTIVOINFORMACIONCARGA <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMMANIFIESTOCARGA", Remesa.NUMMANIFIESTOCARGA)
                        End If
                        If Remesa.TIPOCUMPLIDOREMESA <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "TIPOCUMPLIDOREMESA", Remesa.TIPOCUMPLIDOREMESA)
                        End If
                        If Remesa.CANTIDADCARGADA <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CANTIDADCARGADA", Val(Remesa.CANTIDADCARGADA))
                        Else
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CANTIDADCARGADA", CERO)
                        End If
                        If Remesa.CANTIDADENTREGADA <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CANTIDADENTREGADA", Val(Remesa.CANTIDADENTREGADA))
                        Else
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CANTIDADENTREGADA", CERO)
                        End If
                        If Remesa.UNIDADMEDIDACAPACIDAD <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "UNIDADMEDIDACAPACIDAD", Remesa.UNIDADMEDIDACAPACIDAD)
                        End If
                        If Remesa.FECHALLEGADACARGUE <> String.Empty Then 'vacio
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "FECHALLEGADACARGUE", Format(Date.Parse(Remesa.FECHALLEGADACARGUE), "dd/MM/yyyy"))
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "HORALLEGADACARGUEREMESA", Date.Parse(Remesa.FECHALLEGADACARGUE).ToString("HH:mm"))
                        End If
                        If Remesa.FECHAENTRADACARGUE <> String.Empty Then 'vacio
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "FECHAENTRADACARGUE", Format(Date.Parse(Remesa.FECHAENTRADACARGUE), "dd/MM/yyyy"))
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "HORAENTRADACARGUEREMESA", Date.Parse(Remesa.FECHAENTRADACARGUE).ToString("HH:mm"))
                        End If
                        If Remesa.FECHASALIDACARGUE <> String.Empty Then 'vacio
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "FECHASALIDACARGUE", Format(Date.Parse(Remesa.FECHASALIDACARGUE), "dd/MM/yyyy"))
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "HORASALIDACARGUEREMESA", Date.Parse(Remesa.FECHASALIDACARGUE).ToString("HH:mm"))
                        End If
                        If Remesa.FECHALLEGADADESCARGUE <> String.Empty Then 'vacio
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "FECHALLEGADADESCARGUE", Format(Date.Parse(Remesa.FECHALLEGADADESCARGUE), "dd/MM/yyyy"))
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "HORALLEGADADESCARGUECUMPLIDO", Date.Parse(Remesa.FECHALLEGADADESCARGUE).ToString("HH:mm"))
                        End If
                        If Remesa.FECHAENTRADADESCARGUE <> String.Empty Then 'vacio
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "FECHAENTRADADESCARGUE", Format(Date.Parse(Remesa.FECHAENTRADADESCARGUE), "dd/MM/yyyy"))
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "HORAENTRADADESCARGUECUMPLIDO", Date.Parse(Remesa.FECHAENTRADADESCARGUE).ToString("HH:mm"))
                        End If
                        If Remesa.FECHASALIDADESCARGUE <> String.Empty Then 'vacio
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "FECHASALIDADESCARGUE", Format(Date.Parse(Remesa.FECHASALIDADESCARGUE), "dd/MM/yyyy"))
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "HORASALIDADESCARGUECUMPLIDO", Date.Parse(Remesa.FECHASALIDADESCARGUE).ToString("HH:mm"))
                        End If

                    Case Tipos_Procesos.FORTMATO_CONSULTA_PROCESOS
                        Dim strAxuliarCampos As String = String.Empty
                        For Each Items As String In lstVariablesDestinatario
                            strAxuliarCampos &= Items
                        Next
                        If strAxuliarCampos <> String.Empty Then strAxuliarCampos = "," & strAxuliarCampos
                        xmlElementoRoot = XmlDocumento.DocumentElement
                        Dim txtNodo As XmlText = XmlDocumento.CreateTextNode("ingresoid" & strAxuliarCampos)
                        xmlElementoRoot.LastChild.AppendChild(txtNodo)

                        NuevoElemento = XmlDocumento.CreateElement(NOMBRE_TAG_CONSULTA)
                        xmlElementoRoot.AppendChild(NuevoElemento)
                        xmlElementoRoot = XmlDocumento.DocumentElement

                        xmlElementoRoot = XmlDocumento.DocumentElement
                        NuevoElemento = XmlDocumento.CreateElement("NUMNITEMPRESATRANSPORTE")
                        txtNodo = XmlDocumento.CreateTextNode(Remesa.NUMNITEMPRESATRANSPORTE)
                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

                        xmlElementoRoot = XmlDocumento.DocumentElement
                        NuevoElemento = XmlDocumento.CreateElement("CONSECUTIVOREMESA")
                        txtNodo = XmlDocumento.CreateTextNode("'" & Remesa.CONSECUTIVOREMESA & "'")
                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

                End Select
            Catch ex As Exception

            End Try
        End Sub
        Public Sub Crear_XML_Cumplido_Inicial_Remesa(ByRef XmlDocumento As XmlDocument, ByVal TipoFormato As Tipos_Procesos, ByRef lstVariablesDestinatario As List(Of String))
            Try

                Dim xmlElementoRoot As XmlElement = XmlDocumento.DocumentElement
                Dim NuevoElemento As XmlElement = XmlDocumento.CreateElement(NOMBRE_TAG)
                xmlElementoRoot.AppendChild(NuevoElemento)

                Select Case TipoFormato
                    Case Tipos_Procesos.FORMATO_INGRESO
                        If Remesa.NUMNITEMPRESATRANSPORTEALTERNO <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMNITEMPRESATRANSPORTE", Remesa.NUMNITEMPRESATRANSPORTE)
                        End If
                        If Remesa.CONSECUTIVOREMESA <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "INGRESOIDREMESA", Remesa.NUMERORADICADOREMESA)
                        End If
                        If Remesa.NUMMANIFIESTOCARGA <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "INGRESOIDMANIFIESTO", Remesa.NUMERORADICADOMANIFIESTO)
                        End If
                        'If Remesa.NUMIDGPS <> String.Empty Then
                        '    AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMIDGPS", "8300596993")
                        'End If
                        If Remesa.NUMPLACA <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMPLACA", Remesa.NUMPLACA)
                        End If
                        If Remesa.FECHASALIDACARGUE <> String.Empty Then 'vacio
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "FECHASALIDACARGUE", Format(Date.Parse(Remesa.FECHASALIDACARGUE), "dd/MM/yyyy"))
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "HORASALIDACARGUE", Date.Parse(Remesa.FECHASALIDACARGUE).ToString("HH:mm"))
                        End If
                        If Remesa.FECHALLEGADADESCARGUE <> String.Empty Then 'vacio
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "FECHALLEGADADESCARGUE", Format(Date.Parse(Remesa.FECHALLEGADADESCARGUE), "dd/MM/yyyy"))
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "HORALLEGADADESCARGUE", Date.Parse(Remesa.FECHALLEGADADESCARGUE).ToString("HH:mm"))
                        End If
                        If Remesa.OBSERVACIONES <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "OBSERVACIONES", Remesa.OBSERVACIONES)
                        End If


                    Case Tipos_Procesos.FORTMATO_CONSULTA_PROCESOS
                        Dim strAxuliarCampos As String = String.Empty
                        For Each Items As String In lstVariablesDestinatario
                            strAxuliarCampos &= Items
                        Next
                        If strAxuliarCampos <> String.Empty Then strAxuliarCampos = "," & strAxuliarCampos
                        xmlElementoRoot = XmlDocumento.DocumentElement
                        Dim txtNodo As XmlText = XmlDocumento.CreateTextNode("ingresoid" & strAxuliarCampos)
                        xmlElementoRoot.LastChild.AppendChild(txtNodo)

                        NuevoElemento = XmlDocumento.CreateElement(NOMBRE_TAG_CONSULTA)
                        xmlElementoRoot.AppendChild(NuevoElemento)
                        xmlElementoRoot = XmlDocumento.DocumentElement

                        xmlElementoRoot = XmlDocumento.DocumentElement
                        NuevoElemento = XmlDocumento.CreateElement("NUMNITEMPRESATRANSPORTE")
                        txtNodo = XmlDocumento.CreateTextNode(Remesa.NUMNITEMPRESATRANSPORTE)
                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

                        xmlElementoRoot = XmlDocumento.DocumentElement
                        NuevoElemento = XmlDocumento.CreateElement("CONSECUTIVOREMESA")
                        txtNodo = XmlDocumento.CreateTextNode("'" & Remesa.CONSECUTIVOREMESA & "'")
                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

                End Select
            Catch ex As Exception

            End Try
        End Sub

        Public Sub Crear_XML_Anulacion_Remesa(ByRef XmlDocumento As XmlDocument, ByVal TipoFormato As Tipos_Procesos, ByRef lstVariablesDestinatario As List(Of String))
            Try

                Dim xmlElementoRoot As XmlElement = XmlDocumento.DocumentElement
                Dim NuevoElemento As XmlElement = XmlDocumento.CreateElement(NOMBRE_TAG)
                xmlElementoRoot.AppendChild(NuevoElemento)

                Select Case TipoFormato
                    Case Tipos_Procesos.FORMATO_INGRESO
                        If Remesa.NUMNITEMPRESATRANSPORTE <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMNITEMPRESATRANSPORTE", Remesa.NUMNITEMPRESATRANSPORTE)
                        End If
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "MOTIVOANULACIONREMESA", "D")
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "MOTIVOREVERSAREMESA", "A")
                        If Remesa.CONSECUTIVOREMESA <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CONSECUTIVOREMESA", Remesa.CONSECUTIVOREMESA)
                        End If

                    Case Tipos_Procesos.FORTMATO_CONSULTA_PROCESOS
                        Dim strAxuliarCampos As String = String.Empty
                        For Each Items As String In lstVariablesDestinatario
                            strAxuliarCampos &= Items
                        Next
                        If strAxuliarCampos <> String.Empty Then strAxuliarCampos = "," & strAxuliarCampos
                        xmlElementoRoot = XmlDocumento.DocumentElement
                        Dim txtNodo As XmlText = XmlDocumento.CreateTextNode("ingresoid" & strAxuliarCampos)
                        xmlElementoRoot.LastChild.AppendChild(txtNodo)

                        NuevoElemento = XmlDocumento.CreateElement(NOMBRE_TAG_CONSULTA)
                        xmlElementoRoot.AppendChild(NuevoElemento)
                        xmlElementoRoot = XmlDocumento.DocumentElement

                        xmlElementoRoot = XmlDocumento.DocumentElement
                        NuevoElemento = XmlDocumento.CreateElement("NUMNITEMPRESATRANSPORTE")
                        txtNodo = XmlDocumento.CreateTextNode(Remesa.NUMNITEMPRESATRANSPORTE)
                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

                        xmlElementoRoot = XmlDocumento.DocumentElement
                        NuevoElemento = XmlDocumento.CreateElement("CONSECUTIVOREMESA")
                        txtNodo = XmlDocumento.CreateTextNode("'" & Remesa.CONSECUTIVOREMESA & "'")
                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

                End Select
            Catch ex As Exception

            End Try
        End Sub

        Public Sub Crear_XML_Anulacion_Cumplido_Remesa(ByRef XmlDocumento As XmlDocument, ByVal TipoFormato As Tipos_Procesos, ByRef lstVariablesDestinatario As List(Of String))
            Try

                Dim xmlElementoRoot As XmlElement = XmlDocumento.DocumentElement
                Dim NuevoElemento As XmlElement = XmlDocumento.CreateElement(NOMBRE_TAG)
                xmlElementoRoot.AppendChild(NuevoElemento)

                Select Case TipoFormato
                    Case Tipos_Procesos.FORMATO_INGRESO
                        If Remesa.NUMNITEMPRESATRANSPORTE <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "NUMNITEMPRESATRANSPORTE", Remesa.NUMNITEMPRESATRANSPORTE)
                        End If
                        AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CODMOTIVOANULACIONCUMPLIDO", "D")
                        'AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "MOTIVOREVERSAREMESA", "A")
                        If Remesa.CONSECUTIVOREMESA <> String.Empty Then
                            AdicionarNodoXml(XmlDocumento, xmlElementoRoot, "CONSECUTIVOREMESA", Remesa.CONSECUTIVOREMESA)
                        End If

                    Case Tipos_Procesos.FORTMATO_CONSULTA_PROCESOS
                        Dim strAxuliarCampos As String = String.Empty
                        For Each Items As String In lstVariablesDestinatario
                            strAxuliarCampos &= Items
                        Next
                        If strAxuliarCampos <> String.Empty Then strAxuliarCampos = "," & strAxuliarCampos
                        xmlElementoRoot = XmlDocumento.DocumentElement
                        Dim txtNodo As XmlText = XmlDocumento.CreateTextNode("ingresoid" & strAxuliarCampos)
                        xmlElementoRoot.LastChild.AppendChild(txtNodo)

                        NuevoElemento = XmlDocumento.CreateElement(NOMBRE_TAG_CONSULTA)
                        xmlElementoRoot.AppendChild(NuevoElemento)
                        xmlElementoRoot = XmlDocumento.DocumentElement

                        xmlElementoRoot = XmlDocumento.DocumentElement
                        NuevoElemento = XmlDocumento.CreateElement("NUMNITEMPRESATRANSPORTE")
                        txtNodo = XmlDocumento.CreateTextNode(Remesa.NUMNITEMPRESATRANSPORTE)
                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

                        xmlElementoRoot = XmlDocumento.DocumentElement
                        NuevoElemento = XmlDocumento.CreateElement("CONSECUTIVOREMESA")
                        txtNodo = XmlDocumento.CreateTextNode("'" & Remesa.CONSECUTIVOREMESA & "'")
                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

                End Select
            Catch ex As Exception

            End Try
        End Sub

        Public Function Reportar_Remesa(ByVal TipoFormato As Tipos_Procesos, ByVal ResultadoReporte As ResultReporteMinisterio) As Double
            Try
                ResultadoReporte.NumConfirmacion = General.CERO
                ResultadoReporte.StrError = String.Empty

                Dim xmlDoc As New XmlDocument
                Dim declaration As XmlDeclaration
                declaration = xmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
                xmlDoc.AppendChild(declaration)
                Dim ElementoRaiz As XmlElement = xmlDoc.CreateElement("root")
                xmlDoc.AppendChild(ElementoRaiz)

                acceso.Crear_XML_Acceso(xmlDoc)
                solicitud.Crear_XML_Solicitud(xmlDoc, TipoFormato, Tipos_Documentos.SOLICITUD_WS_MINISTERIO_PROCESO_REMESA)
                Me.Crear_XML_Remesa(xmlDoc, TipoFormato, ResultadoReporte.LstVariables)
                ResultadoReporte.XmlEnvio = Me.objGeneral.Formatear_XML(xmlDoc.InnerXml) & vbNewLine
                'Reportar_Remesa = ConsumirServicioRNDC(wsManifiestoElectronico.AtenderMensajeBPM(xmlDoc.OuterXml), TipoFormato, ResultadoReporte.StrError, ResultadoReporte.LstVariables)
                ResultadoReporte.NumConfirmacion = ConsumirServicioRNDC(wsManifiestoElectronico.AtenderMensajeBPM(xmlDoc.OuterXml), TipoFormato, ResultadoReporte.StrError, ResultadoReporte.LstVariables)
            Catch ex As Exception
                ResultadoReporte.NumConfirmacion = General.CERO
                ResultadoReporte.StrError = ex.Message
            End Try
        End Function

        Public Function Reportar_Cumplido_Remesa(ByVal TipoFormato As Tipos_Procesos, ByVal ResultadoReporte As ResultReporteMinisterio) As Double
            Try
                ResultadoReporte.NumConfirmacion = General.CERO
                ResultadoReporte.StrError = String.Empty

                Dim xmlDoc As New XmlDocument
                Dim declaration As XmlDeclaration
                declaration = xmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
                xmlDoc.AppendChild(declaration)
                Dim ElementoRaiz As XmlElement = xmlDoc.CreateElement("root")
                xmlDoc.AppendChild(ElementoRaiz)

                acceso.Crear_XML_Acceso(xmlDoc)
                solicitud.Crear_XML_Solicitud(xmlDoc, TipoFormato, Tipos_Documentos.SOLICITUD_WS_MINISTERIO_PROCESO_CUMPLIR_REMESA)
                Me.Crear_XML_Cumplido_Remesa(xmlDoc, TipoFormato, ResultadoReporte.LstVariables)
                ResultadoReporte.XmlEnvio = Me.objGeneral.Formatear_XML(xmlDoc.InnerXml) & vbNewLine
                'Reportar_Remesa = ConsumirServicioRNDC(wsManifiestoElectronico.AtenderMensajeBPM(xmlDoc.OuterXml), TipoFormato, ResultadoReporte.StrError, ResultadoReporte.LstVariables)
                ResultadoReporte.NumConfirmacion = ConsumirServicioRNDC(wsManifiestoElectronico.AtenderMensajeBPM(xmlDoc.OuterXml), TipoFormato, ResultadoReporte.StrError, ResultadoReporte.LstVariables)
            Catch ex As Exception
                ResultadoReporte.NumConfirmacion = General.CERO
                ResultadoReporte.StrError = ex.Message
            End Try
        End Function
        Public Function Reportar_Cumplido_Inicial_Remesa(ByVal TipoFormato As Tipos_Procesos, ByVal ResultadoReporte As ResultReporteMinisterio) As Double
            Try
                ResultadoReporte.NumConfirmacion = General.CERO
                ResultadoReporte.StrError = String.Empty

                Dim xmlDoc As New XmlDocument
                Dim declaration As XmlDeclaration
                declaration = xmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
                xmlDoc.AppendChild(declaration)
                Dim ElementoRaiz As XmlElement = xmlDoc.CreateElement("root")
                xmlDoc.AppendChild(ElementoRaiz)

                acceso.Crear_XML_Acceso(xmlDoc)
                solicitud.Crear_XML_Solicitud(xmlDoc, TipoFormato, Tipos_Documentos.SOLICITUD_WS_MINISTERIO_PROCESO_CUMPLIR_INICIAL_REMESA)
                Me.Crear_XML_Cumplido_Inicial_Remesa(xmlDoc, TipoFormato, ResultadoReporte.LstVariables)
                ResultadoReporte.XmlEnvio = Me.objGeneral.Formatear_XML(xmlDoc.InnerXml) & vbNewLine
                'Reportar_Remesa = ConsumirServicioRNDC(wsManifiestoElectronico.AtenderMensajeBPM(xmlDoc.OuterXml), TipoFormato, ResultadoReporte.StrError, ResultadoReporte.LstVariables)
                ResultadoReporte.NumConfirmacion = ConsumirServicioRNDC(wsManifiestoElectronico.AtenderMensajeBPM(xmlDoc.OuterXml), TipoFormato, ResultadoReporte.StrError, ResultadoReporte.LstVariables)
            Catch ex As Exception
                ResultadoReporte.NumConfirmacion = General.CERO
                ResultadoReporte.StrError = ex.Message
            End Try
        End Function

        Public Function Reportar_Anulado_Remesa(ByVal TipoFormato As Tipos_Procesos, ByVal ResultadoReporte As ResultReporteMinisterio) As Double
            Try
                ResultadoReporte.NumConfirmacion = General.CERO
                ResultadoReporte.StrError = String.Empty

                Dim xmlDoc As New XmlDocument
                Dim declaration As XmlDeclaration
                declaration = xmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
                xmlDoc.AppendChild(declaration)
                Dim ElementoRaiz As XmlElement = xmlDoc.CreateElement("root")
                xmlDoc.AppendChild(ElementoRaiz)

                acceso.Crear_XML_Acceso(xmlDoc)
                solicitud.Crear_XML_Solicitud(xmlDoc, TipoFormato, Tipos_Documentos.SOLICITUD_WS_MINISTERIO_PROCESO_ANULAR_REMESA)
                Me.Crear_XML_Anulacion_Remesa(xmlDoc, TipoFormato, ResultadoReporte.LstVariables)
                ResultadoReporte.XmlEnvio = Me.objGeneral.Formatear_XML(xmlDoc.InnerXml) & vbNewLine
                'Reportar_Remesa = ConsumirServicioRNDC(wsManifiestoElectronico.AtenderMensajeBPM(xmlDoc.OuterXml), TipoFormato, ResultadoReporte.StrError, ResultadoReporte.LstVariables)
                ResultadoReporte.NumConfirmacion = ConsumirServicioRNDC(wsManifiestoElectronico.AtenderMensajeBPM(xmlDoc.OuterXml), TipoFormato, ResultadoReporte.StrError, ResultadoReporte.LstVariables)
            Catch ex As Exception
                ResultadoReporte.NumConfirmacion = General.CERO
                ResultadoReporte.StrError = ex.Message
            End Try
        End Function
        Public Function Reportar_Anulado_Cumplido_Remesa(ByVal TipoFormato As Tipos_Procesos, ByVal ResultadoReporte As ResultReporteMinisterio) As Double
            Try
                ResultadoReporte.NumConfirmacion = General.CERO
                ResultadoReporte.StrError = String.Empty

                Dim xmlDoc As New XmlDocument
                Dim declaration As XmlDeclaration
                declaration = xmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
                xmlDoc.AppendChild(declaration)
                Dim ElementoRaiz As XmlElement = xmlDoc.CreateElement("root")
                xmlDoc.AppendChild(ElementoRaiz)

                acceso.Crear_XML_Acceso(xmlDoc)
                solicitud.Crear_XML_Solicitud(xmlDoc, TipoFormato, Tipos_Documentos.SOLICITUD_WS_MINISTERIO_PROCESO_ANULAR_CUMPLIDO_REMESA)
                Me.Crear_XML_Anulacion_Cumplido_Remesa(xmlDoc, TipoFormato, ResultadoReporte.LstVariables)
                ResultadoReporte.XmlEnvio = Me.objGeneral.Formatear_XML(xmlDoc.InnerXml) & vbNewLine
                'Reportar_Remesa = ConsumirServicioRNDC(wsManifiestoElectronico.AtenderMensajeBPM(xmlDoc.OuterXml), TipoFormato, ResultadoReporte.StrError, ResultadoReporte.LstVariables)
                ResultadoReporte.NumConfirmacion = ConsumirServicioRNDC(wsManifiestoElectronico.AtenderMensajeBPM(xmlDoc.OuterXml), TipoFormato, ResultadoReporte.StrError, ResultadoReporte.LstVariables)
            Catch ex As Exception
                ResultadoReporte.NumConfirmacion = General.CERO
                ResultadoReporte.StrError = ex.Message
            End Try
        End Function


        Public Function ConsumirServicioRNDC(ByVal strMensaje As String, ByVal TipoFormato As Tipos_Procesos, ByRef strErrores As String, ByRef lstVariables As List(Of String)) As Double
            Try
                ConsumirServicioRNDC = General.CERO
                Dim xmlRespuesta As New XmlDocument
                xmlRespuesta.LoadXml(strMensaje)
                Dim lstxmlObjetos As New List(Of XmlElement)
                For Each Item As XmlNode In xmlRespuesta.DocumentElement
                    lstxmlObjetos.Add(Item)
                Next

                Select Case TipoFormato
                    Case Tipos_Procesos.FORMATO_INGRESO
                        If xmlRespuesta.GetElementsByTagName("ErrorMSG").Count > 0 Then
                            ConsumirServicioRNDC = General.CERO
                            strErrores = xmlRespuesta.GetElementsByTagName("ErrorMSG")(0).InnerText
                        End If
                        If xmlRespuesta.GetElementsByTagName("ingresoid").Count > 0 Then
                            ConsumirServicioRNDC = Val(xmlRespuesta.GetElementsByTagName("ingresoid")(0).InnerText)
                            strErrores = String.Empty
                        End If
                    Case Tipos_Procesos.FORTMATO_CONSULTA_PROCESOS
                        If xmlRespuesta.GetElementsByTagName("ErrorMSG").Count > 0 Then
                            ConsumirServicioRNDC = General.CERO
                            strErrores = xmlRespuesta.GetElementsByTagName("ErrorMSG")(0).InnerText
                        End If
                        If xmlRespuesta.GetElementsByTagName("ingresoid").Count > 0 Then
                            ConsumirServicioRNDC = Val(xmlRespuesta.GetElementsByTagName("ingresoid")(0).InnerText)
                            strErrores = String.Empty
                            For i As Integer = 0 To lstVariables.Count - 1
                                If xmlRespuesta.GetElementsByTagName(lstVariables(i)).Count > 0 Then
                                    lstVariables(i) = xmlRespuesta.GetElementsByTagName(lstVariables(i))(0).InnerText
                                Else
                                    lstVariables(i) = String.Empty
                                End If
                            Next
                        End If
                End Select

            Catch ex As Exception
                strErrores = ex.Message
            End Try
        End Function

        Public Sub AdicionarNodoXml(ByRef XmlDocumento As XmlDocument, ByRef xmlElementoRoot As XmlElement, Elemento As String, ElementoNodo As String)
            xmlElementoRoot.LastChild.AppendChild(XmlDocumento.CreateElement(Elemento))
            xmlElementoRoot.LastChild.LastChild.AppendChild(XmlDocumento.CreateTextNode(ElementoNodo))
        End Sub
#End Region

    End Class
End Namespace

