﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Gesphone
Imports System.Transactions


Namespace Gesphone

    ''' <summary>
    ''' Clase <see cref="RepositorioDetalleSeguimientoCargaVehiculos"/>
    ''' </summary>
    Public NotInheritable Class RepositorioDetalleSeguimientoCargaVehiculos
        Inherits RepositorioBase(Of DetalleSeguimientoCargaVehiculos)

        Public Overrides Function Consultar(filtro As DetalleSeguimientoCargaVehiculos) As IEnumerable(Of DetalleSeguimientoCargaVehiculos)
            Dim lista As New List(Of DetalleSeguimientoCargaVehiculos)
            Dim item As DetalleSeguimientoCargaVehiculos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.NumeroDocumento) Then
                    conexion.AgregarParametroSQL("@par_Numero_Documento", filtro.NumeroDocumento)
                End If
                If Not IsNothing(filtro.NombreCliente) Then
                    conexion.AgregarParametroSQL("@par_Nombre_Cliente", filtro.NombreCliente)
                End If
                If filtro.Estado >= Cero Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If

                If filtro.Numero > Cero Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If

                If filtro.Pagina Then
                    conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                End If

                If filtro.RegistrosPagina Then
                    conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_seguimiento_carga_vehiculos]")

                While resultado.Read
                    item = New DetalleSeguimientoCargaVehiculos(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As DetalleSeguimientoCargaVehiculos) As Long
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENSV_Numero", entidad.NumeroSeguimiento)
                conexion.AgregarParametroSQL("@par_Numero_Documento", entidad.NumeroDocumento)
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
                conexion.AgregarParametroSQL("@par_Fecha_Hora_Reporte", entidad.FechaReporte, SqlDbType.DateTime)
                conexion.AgregarParametroSQL("@par_CATA_TIRS_Codigo", entidad.TipoSeguimiento.Codigo)
                conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", entidad.Cliente.Codigo)
                conexion.AgregarParametroSQL("@par_CATA_NOSC_Codigo", entidad.NovedadSeguimiento.Codigo)
                conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                conexion.AgregarParametroSQL("@par_Identificacion_Recibido_Cliente", entidad.IdentificacionRecibido)
                conexion.AgregarParametroSQL("@par_Nombre_Recibido_Cliente", entidad.NombreRecibido)
                conexion.AgregarParametroSQL("@par_CIUD_Recibido_Cliente", entidad.CiudadRecibido.Codigo)
                conexion.AgregarParametroSQL("@par_Direccion_Recibido_Cliente", entidad.DireccionRecibido)
                conexion.AgregarParametroSQL("@par_Telefono_Recibido_Cliente", entidad.TelefonoRecibido)
                conexion.AgregarParametroSQL("@par_Cumplir", entidad.Cumplir)
                conexion.AgregarParametroSQL("@par_Reportar_Cliente", entidad.ReportarCliente)
                conexion.AgregarParametroSQL("@par_Envio_Reporte_Cliente", entidad.EnvioReporte)
                conexion.AgregarParametroSQL("@par_Longitud", entidad.Longitud)
                conexion.AgregarParametroSQL("@par_Latitud", entidad.Latitud)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_OFIC_Codigo_Crea", entidad.OficinaCrea)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                conexion.AgregarParametroSQL("@par_Cantidad_Recibido_Cliente", entidad.CantidadResividoCliente)
                conexion.AgregarParametroSQL("@par_Peso_Recibido_Cliente", entidad.PesoResividoCliente)
                conexion.AgregarParametroSQL("@par_Firma", entidad.Firma, SqlDbType.Image)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_seguimiento_carga_vehiculos")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using
                If entidad.Codigo > Cero Then
                    transaccion.Complete()
                Else
                    entidad.Numero = Cero
                End If

            End Using
            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As DetalleSeguimientoCargaVehiculos) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ID", entidad.Numero)
                conexion.AgregarParametroSQL("@par_ENSV_Numero", entidad.NumeroSeguimiento)
                conexion.AgregarParametroSQL("@par_Numero_Documento", entidad.NumeroDocumento)
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
                conexion.AgregarParametroSQL("@par_Fecha_Hora_Reporte", entidad.FechaReporte, SqlDbType.DateTime)
                conexion.AgregarParametroSQL("@par_CATA_TIRS_Codigo", entidad.TipoSeguimiento.Codigo)
                conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", entidad.Cliente.Codigo)
                conexion.AgregarParametroSQL("@par_CATA_NOSC_Codigo", entidad.NovedadSeguimiento.Codigo)
                conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                conexion.AgregarParametroSQL("@par_Identificacion_Recibido_Cliente", entidad.IdentificacionRecibido)
                conexion.AgregarParametroSQL("@par_Nombre_Recibido_Cliente", entidad.NombreRecibido)
                conexion.AgregarParametroSQL("@par_CIUD_Recibido_Cliente", entidad.CiudadRecibido.Codigo)
                conexion.AgregarParametroSQL("@par_Direccion_Recibido_Cliente", entidad.DireccionRecibido)
                conexion.AgregarParametroSQL("@par_Telefono_Recibido_Cliente", entidad.TelefonoRecibido)
                conexion.AgregarParametroSQL("@par_Cumplir", entidad.Cumplir)
                conexion.AgregarParametroSQL("@par_Reportar_Cliente", entidad.ReportarCliente)
                conexion.AgregarParametroSQL("@par_Envio_Reporte_Cliente", entidad.EnvioReporte)
                conexion.AgregarParametroSQL("@par_Longitud", entidad.Longitud)
                conexion.AgregarParametroSQL("@par_Latitud", entidad.Latitud)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)
                conexion.AgregarParametroSQL("@par_Cantidad_Recibido_Cliente", entidad.CantidadResividoCliente)
                conexion.AgregarParametroSQL("@par_Peso_Recibido_Cliente", entidad.PesoResividoCliente)
                conexion.AgregarParametroSQL("@par_Firma", entidad.Firma, SqlDbType.Image)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_detalle_seguimiento_carga_vehiculos")

                While resultado.Read
                    entidad.Numero = resultado.Item("Numero").ToString()
                End While

                resultado.Close()

                If entidad.Numero.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Numero
        End Function

        Public Overrides Function Obtener(filtro As DetalleSeguimientoCargaVehiculos) As DetalleSeguimientoCargaVehiculos
            Dim item As New DetalleSeguimientoCargaVehiculos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_detalle_seguimiento_carga_vehiculos]")

                While resultado.Read
                    item = New DetalleSeguimientoCargaVehiculos(resultado)
                End While

            End Using

            Return item
        End Function
        Public Function Anular(entidad As DetalleSeguimientoCargaVehiculos) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ID", entidad.ID)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Anula", entidad.UsuarioAnula.Codigo)
                conexion.AgregarParametroSQL("@par_Causa_Anula", entidad.CausaAnulacion)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_anular_detalle_seguimiento_carga_vehiculos]")

                While resultado.Read
                    anulo = IIf((resultado.Item("ID")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function
    End Class

End Namespace