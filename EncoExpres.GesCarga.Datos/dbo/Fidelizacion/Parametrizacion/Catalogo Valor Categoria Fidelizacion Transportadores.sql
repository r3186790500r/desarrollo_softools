﻿PRINT 'Catalogo Categoria Fidelizacion Transportadores - 188 - CFTR'
GO
DELETE Valor_Catalogos WHERE CATA_Codigo = 188
GO
DELETE Configuracion_Catalogos WHERE CATA_Codigo = 188
GO
DELETE Catalogos WHERE Codigo = 188
GO
------------------------------------------CATALOGOS------------------------------------------------------
INSERT INTO Catalogos (EMPR_Codigo, Codigo, Nombre_Corto, Nombre, Actualizable, Numero_Columnas, Numero_Campos_Llave)
SELECT Codigo, 188, 'CFTR','Categoria Fidelizacion Transportadores', 0, 2, 1 FROM Empresas
GO
-----------------------------------------VALOR CATALOGOS--------------------------------------------------
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, USUA_Codigo_Crea,Fecha_Crea)
SELECT Codigo, 188, 18801, 'NINGUNA', '', '', '', 1, GETDATE() FROM Empresas
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, USUA_Codigo_Crea,Fecha_Crea) 
SELECT Codigo, 188, 18802, 'HERMANO', '', '', '', 1, GETDATE() FROM Empresas
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, USUA_Codigo_Crea,Fecha_Crea) 
SELECT Codigo, 188, 18803, 'PRIMO', '', '', '', 1, GETDATE() FROM Empresas
GO