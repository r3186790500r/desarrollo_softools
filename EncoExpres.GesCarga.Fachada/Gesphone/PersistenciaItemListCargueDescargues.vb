﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Gesphone
Imports EncoExpres.GesCarga.Repositorio.Gesphone

Namespace Gesphone
    Public NotInheritable Class PersistenciaItemListCargueDescargues
        Implements IPersistenciaBase(Of ItemListCargueDescargues)

        Public Function Consultar(filtro As ItemListCargueDescargues) As IEnumerable(Of ItemListCargueDescargues) Implements IPersistenciaBase(Of ItemListCargueDescargues).Consultar
            Return New RepositorioItemListCargueDescargues().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As ItemListCargueDescargues) As Long Implements IPersistenciaBase(Of ItemListCargueDescargues).Insertar
            Return New RepositorioItemListCargueDescargues().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As ItemListCargueDescargues) As Long Implements IPersistenciaBase(Of ItemListCargueDescargues).Modificar
            Return New RepositorioItemListCargueDescargues().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As ItemListCargueDescargues) As ItemListCargueDescargues Implements IPersistenciaBase(Of ItemListCargueDescargues).Obtener
            Return New RepositorioItemListCargueDescargues().Obtener(filtro)
        End Function
        Public Function Anular(entidad As ItemListCargueDescargues) As Boolean
            Return New RepositorioItemListCargueDescargues().Anular(entidad)
        End Function
    End Class

End Namespace

