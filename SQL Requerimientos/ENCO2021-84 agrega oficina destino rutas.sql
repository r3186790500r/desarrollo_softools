USE [GESCARGA50_DESA]
GO

/****** Object:  StoredProcedure [dbo].[gsp_insertar_rutas]    Script Date: 24/01/2022 11:14:48 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER TABLE [dbo].[Rutas]
DROP CONSTRAINT if exists [FK_Rutas_Oficinas_Destino];
GO

ALTER TABLE [dbo].[Rutas]
DROP COLUMN if exists [OFIC_Codigo_Destino];
GO

ALTER TABLE [dbo].[Rutas]
ADD OFIC_Codigo_Destino SMALLINT

ALTER TABLE [dbo].[Rutas]  WITH CHECK 
ADD CONSTRAINT [FK_Rutas_Oficinas_Destino] FOREIGN KEY([EMPR_Codigo], [OFIC_Codigo_Destino])
REFERENCES [dbo].[Oficinas] ([EMPR_Codigo], [Codigo])
GO

ALTER TABLE [dbo].[Rutas] 
CHECK CONSTRAINT [FK_Rutas_Oficinas_Destino]
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_insertar_rutas]       
(              
@par_EMPR_Codigo SMALLINT,              
@par_Codigo_Alterno VARCHAR (20) = NULL,              
@par_Nombre VARCHAR (100),              
@par_CIUD_Codigo_Origen NUMERIC,              
@par_CIUD_Codigo_Destino NUMERIC,  
@par_OFIC_Codigo_Destino SMALLINT, 
@par_CATA_TIRU_Codigo NUMERIC = NULL,            
@par_Duracion_Horas NUMERIC(18,2) = NULL,            
@par_Porcentaje_Anticipo NUMERIC(18,2) = NULL,        
@par_Plan_Puntos NUMERIC = NULL,           
@par_Kilometros NUMERIC(18,2) = NULL,        
@par_Estado SMALLINT,            
@par_USUA_Codigo_Crea SMALLINT              
)              
AS               
BEGIN              
              
INSERT INTO  Rutas              
(              
EMPR_Codigo ,              
Codigo_Alterno,              
Nombre,              
CIUD_Codigo_Origen,              
CIUD_Codigo_Destino,    
OFIC_Codigo_Destino,
CATA_TIRU_Codigo,           
Duracion_Horas,          
Porcentaje_Anticipo,    
Kilometros,    
Puntos,          
Estado,             
USUA_Codigo_Crea,              
Fecha_Crea              
)              
VALUES               
(              
@par_EMPR_Codigo ,               
ISNULL(@par_Codigo_Alterno,''),              
@par_Nombre,              
@par_CIUD_Codigo_Origen,              
@par_CIUD_Codigo_Destino,  
@par_OFIC_Codigo_Destino, 
@par_CATA_TIRU_Codigo,          
ISNULL(@par_Duracion_Horas, 0),            
ISNULL(@par_Porcentaje_Anticipo, 0),        
@par_Kilometros,    
ISNULL(@par_Plan_Puntos, 0),      
@par_Estado,             
@par_USUA_Codigo_Crea,              
GETDATE()              
)              
              
SELECT @@IDENTITY AS Codigo              
              
END              
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_modificar_rutas]   
(        
@par_EMPR_Codigo SMALLINT,        
@par_Codigo NUMERIC,        
@par_Codigo_Alterno VARCHAR (20),        
@par_Nombre VARCHAR (100),        
@par_CIUD_Codigo_Origen NUMERIC,        
@par_CIUD_Codigo_Destino NUMERIC,     
@par_OFIC_Codigo_Destino SMALLINT, 
@par_CATA_TIRU_Codigo NUMERIC = null,        
@par_Duracion_Horas NUMERIC(18,2) = NULL,      
@par_Porcentaje_Anticipo NUMERIC(18,2) = NULL,    
@par_Kilometros NUMERIC(18,2) = NULL,    
@par_Plan_Puntos NUMERIC = NULL,       
@par_Estado SMALLINT,        
@par_USUA_Codigo_Modifica SMALLINT        
)        
AS         
BEGIN        
 UPDATE Rutas        
 SET        
 Codigo_Alterno = @par_Codigo_Alterno,        
 Nombre = @par_Nombre,        
 CIUD_Codigo_Origen = @par_CIUD_Codigo_Origen,        
 CIUD_Codigo_Destino = @par_CIUD_Codigo_Destino,    
 OFIC_Codigo_Destino = @par_OFIC_Codigo_Destino,
 CATA_TIRU_Codigo = ISNULL(@par_CATA_TIRU_Codigo,4400),        
 Duracion_Horas = ISNULL(@par_Duracion_Horas, 0),      
 Porcentaje_Anticipo = ISNULL(@par_Porcentaje_Anticipo, 0),     
 Puntos = ISNULL(@par_Plan_Puntos, 0),     
 Kilometros = @par_Kilometros,   
 Estado = @par_Estado,        
 USUA_Codigo_Modifica = @par_USUA_Codigo_Modifica,        
 Fecha_Modifica = GETDATE()        
 WHERE         
 EMPR_Codigo =  @par_EMPR_Codigo        
 AND Codigo = @par_Codigo        
        
SELECT @par_Codigo AS Codigo        
END        
GO


CREATE OR ALTER PROCEDURE [dbo].[gsp_obtener_rutas]   
(        
 @par_EMPR_Codigo SMALLINT,        
 @par_Codigo NUMERIC        
)        
AS         
BEGIN        
        
 SELECT        
  1 AS Obtener,        
  RUTA.EMPR_Codigo,        
  RUTA.Codigo,        
  RUTA.Codigo_Alterno,        
  RUTA.Nombre,        
  RUTA.Duracion_Horas,      
  RUTA.Porcentaje_Anticipo,    
  RUTA.Puntos,      
  RUTA.Estado,        
  RUTA.Kilometros,        
  RUTA.CIUD_Codigo_Origen,        
  RUTA.CIUD_Codigo_Destino,        
  CIOR.Nombre AS CiudadOrigen,        
  CIDE.Nombre AS CiudadDestino, 
  RUTA.OFIC_Codigo_Destino, 
  OFDE.Nombre AS OficinaDestino,
  ISNULL(RUTA.CATA_TIRU_Codigo,4400) AS CATA_TIRU_Codigo ,        
  ISNULL(TIRU.Campo1,'') as TipoRuta,        
  ROW_NUMBER() OVER(ORDER BY RUTA.Nombre) AS RowNumber        
 FROM        
  Rutas RUTA        
  LEFT JOIN Valor_Catalogos as TIRU        
  ON  RUTA.EMPR_Codigo = TIRU.EMPR_Codigo         
  AND RUTA.CATA_TIRU_Codigo = TIRU.Codigo
  
  LEFT JOIN Oficinas as OFDE        
  ON  RUTA.EMPR_Codigo = OFDE.EMPR_Codigo         
  AND RUTA.OFIC_Codigo_Destino = OFDE.Codigo,

  Ciudades CIOR,        
  Ciudades CIDE        
 WHERE        
  RUTA.EMPR_Codigo = CIOR.EMPR_Codigo         
  AND RUTA.CIUD_Codigo_Origen = CIOR.Codigo        
        
  AND RUTA.EMPR_Codigo = CIDE.EMPR_Codigo         
  AND RUTA.CIUD_Codigo_Destino = CIDE.Codigo        
        
  AND RUTA.EMPR_Codigo = @par_EMPR_Codigo        
  AND RUTA.Codigo = @par_Codigo        
        
END        
GO
