﻿Print 'Catalogo 33 Estado Semirremolques'
GO
DELETE Valor_Catalogos WHERE CATA_Codigo = 33
GO
DELETE Configuracion_Catalogos WHERE CATA_Codigo = 33
GO
DELETE Catalogos WHERE Codigo = 33
GO
------------------------------------------CATALOGOS------------------------------------------------------
INSERT INTO Catalogos VALUES (1,33,'ESSE','Estado Semirremolques',0,2,1)
GO
-----------------------------------------VALOR CATALOGOS--------------------------------------------------
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1, USUA_Codigo_Crea,Fecha_Crea) 
VALUES 
(1,33,3300,UPPER('(NO APLICA)'),1,GETDATE()),
(1,33,3301,UPPER('ACTIVO'),1,GETDATE()),
(1,33,3302,UPPER('INACTIVO'),1,GETDATE()),
(1,33,3303,UPPER('BLOQUEADO'),1,GETDATE())

GO

-----------------------------------------CONFIGURACION CATALOGOS------------------------------------------
INSERT INTO configuracion_catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
VALUES 
(1,33,1,'Codigo',1,1,1,1,0,0,0,0,0,1,0),
(1,33,2,'Nombre',1,1,1,1,0,0,0,0,0,1,0)
GO 