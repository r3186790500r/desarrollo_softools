﻿
Imports System.Diagnostics
Imports System.IO
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Net
Imports System.Threading
Public Class serIntegracionPSL

#Region "Variables Generales"
    ' Variables
    Private intCodigoEmpresa As Integer
    Private lonNumeRegi As Long
    Private lonIntervaloMilisegundosEjecucionProceso As Long

    ' Hilos Procesos
    Private HilProcesoTerceros As Thread
    Private HilProcesoSolicitudesAnticipos As Thread
    Private HilProcesoSolicitudesAnticiposEQP As Thread
    Private HilProcesoEgresosAnticipos As Thread
    Private HilProcesoLiquidacionPlanillas As Thread
    Private HilProcesoFacturasTransporte As Thread
    Private HilProcesoLegalizacionGastos As Thread
    Private HilProcesoPolizasPlanillas As Thread
    Private HilProcesoConsultaEstadoSolicitudAnticiposPSL As Thread
    Private HilProcesoCruceAnticiposLiquidacionesPlanillas As Thread

#End Region

#Region "Constantes"
    ' GESTRANS
    Const TIDO_TERCEROS As Integer = 80
    Const TIDO_COMPROBANTE_EGRESO As Integer = 30
    Const TIDO_CUENTA_X_PAGAR As Integer = 85
    Const TIDO_LIQUIDACION_PLANILLA_DESPACHO As Integer = 160
    Const TIDO_PLANILLA_DESPACHO As Integer = 150
    Const TIDO_FACTURA_VENTA As Integer = 170
    Const TIDO_LEGALIZACION_GASTOS As Integer = 230
    Const DOCUMENTO_ORIGEN_ANTICIPO As Integer = 2604
    Const DOCUMENTO_ORIGEN_SOBREANTICIPO As Integer = 2605
    Const MEDIO_PAGO_EFECTIVO As Long = 5101
    Const MEDIO_PAGO_TRANSFERENCIA As Long = 5102
    Const MEDIO_PAGO_CAMBISTA As Long = 5104
    Const TIPO_IDENTIFICACION_NIT_GESTRANS As Long = 102
    Const CONCEPTO_LIQUIDACION_ANTICIPO As Long = 1
    Const CONCEPTO_LIQUIDACION_SOBREANTICIPO As Long = 2
    Const CONCEPTO_LIQUIDACION_POLIZA As Long = 4
    Const CONCEPTO_LIQUIDACION_CRUCE_ANTICIPO As Long = 78

    ' Forma Pago Anticipos
    Const FORMA_PAGO_DESPACHO_TRANSFERENCIA As String = "TRANSFERENCIA"
    Const FORMA_PAGO_DESPACHO_OTROS As String = "OTROS"

    ' Liquidacion
    Const CONCEPTO_LIQUIDACION_ANTICIPO_GESTRANS As Integer = 1

    ' Procesos PSL
    Const COMPANIA_PSL As String = "01"
    Const DIVISION_PSL As String = "010001"
    Const USUARIO_PSL As String = "GESTRANS"
    Const MONEDA_PESOS_PSL As String = "COP"
    Const PARAMETROS_IMPORTAR_TERCEROS_PSL As String = ""
    Const PARAMETROS_IMPORTAR_CXP_PSL As String = "|Liberar=S|Totales=S|"
    Const PARAMETROS_IMPORTAR_CXC_PSL As String = "|Liberar=N|Totales=S|"
    Const TABLA_IMPORTAR_TERCEROS_PSL As String = "TGE"
    Const TABLA_IMPORTAR_ANTICIPOS_PSL As String = "CXP"
    Const TABLA_IMPORTAR_LIQUIDACIONES_PSL As String = "CXP"
    Const TABLA_IMPORTAR_FACTURAS_PSL As String = "CXC"
    Const TABLA_IMPORTAR_CRUCE_ANTICIPOS_PSL As String = "CAP"
    Const TABLA_IMPORTAR_LEGALIZACION_GASTOS_PSL As String = "CON"

    ' Valores generales
    Const VALOR_N As String = "N"
    Const VALOR_S As String = "S"
    Const VALOR_CERO As String = "0"
    Const VALOR_NULO_SQL_PSL As String = "Null"
    Const VALOR_ACTIVIDAD_ECONOMICA As String = "4923"
    Const VALOR_ACTIVIDAD_ECONOMICA_ALTERNA As String = "00"

    ' Valor Campos Tabla Terceros Gestrans
    Const VALOR_CATALOGO_CLIENTE As String = "1401"
    Const VALOR_CATALOGO_PROPIETARIO As String = "1408"
    Const VALOR_CATALOGO_CONDUCTOR As String = "1403"
    Const VALOR_CATALOGO_TENEDOR As String = "1412"
    Const VALOR_CATALOGO_AFILIADOR As String = "1406"
    Const VALOR_ACTIVIDIDAD_ECONOMICA As String = "4923"

    ' Valor Campos Tabla Terceros PSL
    Const OPERACION_CREACION_TERCEROS_PSL As String = "C"
    Const OPERACION_MODIFICACION_TERCEROS_PSL As String = "M"
    Const CONTROL_TERCEROS_PSL As String = "TERCEROS"
    Const ESTADO_TERCEROS_PSL As String = "AC"
    Const PERFIL_TRANSPORTADOR_TERCEROS_PSL As String = "TRANC"

    Const TIPO_NATURALEZA_TERCERO_JURIDICA_PSL As String = "J"
    Const TIPO_IDENTIFICACION_NIT_PSL As String = "N"
    Const TIPO_NATURALEZA_TERCERO_NATURAL_PSL As String = "N"
    Const TIPO_IDENTIFICACION_NATURAL_PSL As String = "C"
    Const TIPO_REGIMEN_IVA_NIT_PSL As String = "C"
    Const TIPO_REGIMEN_IVA_NATURAL_PSL As String = "S"

    ' Valor Campos PSL SOLICITUD ANTICIPO
    Const CONTROL_SOLICITUD_ANTICIPO_PSL As String = "SOLI"
    Const TIPO_CONSECUTIVO_SOLICITUD_ANTICIPO_PSL As String = "SANT"
    Const CLASE_CONSECUTIVO_SOLICITUD_ANTICIPO_PSL As String = "SACO"
    Const CONCEPTO_DOCUMENTO_SOLICITUD_ANTICIPO_PSL As String = "ANTFL"
    Const FORMA_PAGO_SOLICITUD_ANTICIPO_PSL As String = "01"
    Const PREFIJO_SOLICITUD_ANTICIPO_PSL As String = "1" ' Número de Anticipos Generados
    Const TIPO_PROVEEDOR_SOLICITUD_ANTICIPO_PSL As String = "TRANC"
    Const ESTABLECIMIENTO_SOLICITUD_ANTICIPO_PSL As String = "00"
    Const CENTRO_RESPONSABILIDAD_SOLICITUD_ANTICIPO_PSL As String = "131001"

    ' Valor Campos PSL EGRESOS ANTICIPOS
    Const TIPO_DUENO_VEHICULO_NA As String = "2100"
    Const TIPO_DUENO_VEHICULO_TERCERO As String = "2101"
    Const TIPO_DUENO_VEHICULO_PROPIO As String = "2102"
    Const TIPO_DUENO_VEHICULO_SOCIOS As String = "2103"

    ' Valor Campos PSL EGRESOS ANTICIPO EFECTIVO
    Const CONTROL_EGRESO_EFECTIVO_ANTICIPO_PSL As String = "EGRE_EFECTIVO"
    Const TIPO_CONSECUTIVO_EGRESO_EFECTIVO_ANTICIPO_PSL As String = "ANTT"
    Const CLASE_CONSECUTIVO_EGRESO_EFECTIVO_ANTICIPO_PSL As String = "ANCO"
    Const CONCEPTO_DOCUMENTO_EGRESO_EFECTIVO_ANTICIPO_PSL As String = "ANEFP"
    Const FORMA_PAGO_EGRESO_EFECTIVO_ANTICIPO_PSL As String = "01"

    Const PREFIJO_EGRESO_ANTICIPO_PSL As String = "1" ' Primer Anticipo Generado
    Const PREFIJO_EGRESO_SOBREANTICIPO_PSL As String = "2" ' Segundo Anticipo Generado FALTA: Contemplar un segundo sobreanticipo

    Const TIPO_PROVEEDOR_EGRESO_EFECTIVO_ANTICIPO_PSL As String = "TRANC"
    Const ESTABLECIMIENTO_EGRESO_EFECTIVO_ANTICIPO_PSL As String = "00"
    Const CENTRO_RESPONSABILIDAD_EGRESO_EFECTIVO_ANTICIPO_PSL As String = "131001"
    Const CONCEPTO_DESCUENTO_EGRESO_EFECTIVO_ANTICIPO_PSL = "DESEF"
    Const CUENTA_DESCUENTO_EGRESO_EFECTIVO_ANTICIPO_PSL = "42104001"

    ' Valor Campos PSL EGRESOS ANTICIPO CAMBISTA
    Const CONTROL_EGRESO_CAMBISTA_ANTICIPO_PSL As String = "EGRE_CAMBISTA"
    Const TIPO_CONSECUTIVO_EGRESO_CAMBISTA_ANTICIPO_PSL As String = "VCAMB"
    Const CLASE_CONSECUTIVO_EGRESO_CAMBISTA_ANTICIPO_EGRE_EFECTIVO_PSL As String = "ANCO"
    Const CLASE_CONSECUTIVO_EGRESO_CAMBISTA_ANTICIPO_EGRE_CAMBISTA_PSL As String = "NDCO"
    Const CONCEPTO_DOCUMENTO_EGRESO_CAMBISTA_ANTICIPO_PSL As String = "ANTMS"

    Const FORMA_PAGO_EGRESO_CAMBISTA_ANTICIPO_PSL As String = "01"
    Const TIPO_PROVEEDOR_EGRESO_CAMBISTA_ANTICIPO_PSL_PRIMER_REGISTRO As String = "ANTCA"
    Const TIPO_PROVEEDOR_EGRESO_CAMBISTA_ANTICIPO_PSL_SEGUNDO_REGISTRO As String = "CAMBI"
    Const ESTABLECIMIENTO_EGRESO_CAMBISTA_ANTICIPO_PSL As String = "00"
    Const CENTRO_RESPONSABILIDAD_EGRESO_CAMBISTA_ANTICIPO_PSL As String = "131001"

    ' Valor Campos PSL EGRESOS SOBRE ANTICIPO EQUIPO PROPIO TRANSFERENCIA
    Const CONCEPTO_DOCUMENTO_EGRESO_TRANSFERENCIA_ANTICIPO_EQP_PSL As String = "ANTEQP"
    Const TIPO_CONSECUTIVO_EGRESO_TRANSFERENCIA_ANTICIPO_EQP_PSL As String = "NDEQP"
    Const TIPO_PROVEEDOR_EGRESO_TRANSFERENCIA_ANTICIPO_EQP_PSL As String = "ANTEQ"
    Const TIPO_PREFIJO_EGRESO_TRANSFERENCIA_ANTICIPO_EQP_PSL As String = "EQP"
    Const ESTABLECIMIENTO_EGRESO_TRANSFERENCIA_ANTICIPO_EQP_PSL As String = "00"

    ' Valor Campos PSL LIQUIDACIONES
    Const TIPO_CONSECUTIVO_LIQUIDACION_SUMA_PSL As String = "LIQTE"
    Const TIPO_CONSECUTIVO_LIQUIDACION_RESTA_PSL As String = "NCL"
    Const CLASE_CONSECUTIVO_LIQUIDACION_FLETE_PSL As String = "FACO"
    Const CLASE_CONSECUTIVO_LIQUIDACION_NC_PSL As String = "NCCO"
    Const ESTABLECIMIENTO_LIQUIDACION_PSL As String = "00"
    Const CENTRO_RESPONSABILIDAD_LIQUIDACION_PSL As String = "131001"
    Const PREFIJO_LIQUIDACION_PSL As String = "LIQ"
    Const TIPO_PROVEEDOR_LIQUIDACION_PSL As String = "TRANC"
    Const CONCEPTO_LIQUIDACION_FLETE_PSL As String = "FLETE"
    Const CONCEPTO_LIQUIDACION_NC_PSL As String = "NOFLE"
    Const OPERACION_SUMA_CONCEPTO_LIQUIDACION As String = "1"
    Const OPERACION_RESTA_CONCEPTO_LIQUIDACION As String = "2"
    Const PREFIJO_LIQUIDACION_NC_PSL As String = "NC"
    Const FORMA_PAGO_LIQUIDACION As String = "01"

    ' Valor Campos PSL POLIZA PLANILLAS
    Const TIPO_CONSECUTIVO_POLIZA_PSL As String = "NCPOT"
    Const CLASE_CONSECUTIVO_POLIZA_NC_PSL As String = "NCCO"
    Const ESTABLECIMIENTO_POLIZA_PSL As String = "00"
    Const PREFIJO_POLIZA_PSL As String = "MC"
    Const TIPO_PROVEEDOR_POLIZA_PSL As String = "ANTCA"
    Const CONCEPTO_DOCUMENTO_POLIZA_PSL As String = "ANPTR"
    Const CONCEPTO_DETALLE_POLIZA_PSL_PRIMER_REGISTRO As String = "PPTRA"
    Const CONCEPTO_DETALLE_POLIZA_PSL_SEGUNDO_REGISTRO As String = "INPTR"
    Const ACTIVIDAD_ECONOMICA_POLIZA_PSL As String = "00"

    ' Valor Campos PSL FACTURAS
    Const TIPO_CONSECUTIVO_FACTURACION_PSL As String = "FE"
    Const CLASE_CONSECUTIVO_FACTURACION_PSL As String = "FAVE"
    Const CENTRO_RESPONSABILIDAD_FACTURACION_PSL As String = "131001"
    Const CONCEPTO_FACTURACION_PSL As String = "FTRAN"
    Const TIPO_FACTURA_FACTURACION_PSL As String = "CR"

    ' Valor Campos PSL LEGALIZACION GASTOS CONDUCTORES
    Const FUENTE_LEGALIZACION_GASTOS_PSL As String = "LEEQP"
    Const CENTRO_RESPONSABILIDAD_LEGALIZACION_GASTOS_PSL As String = "111021"
    Const TIPO_MONEDA_LEGALIZACION_GASTOS_PSL As String = "L"
    Const AJUSTE_INFLACION_LEGALIZACION_GASTOS_PSL As String = "N"
    Const GASTO_CRUCE_ANTICIPO As String = "CRUCE ANTICIPOS"
    Const GASTO_SALDO_FAVOR_EMPRESA As String = "SALDO NO LEGALIZADO"
    Const GASTO_SALDO_FAVOR_CONDUCTOR As String = "REEMBOLSO"

    Const CUENTA_ANTICIPO_SIN_PLANILLA As String = "13301504"
    Const CUENTA_ANTICIPO_CON_PLANILLA As String = "13301503"

    'Valor Campos PSL CRUCE ANTICIPOS
    Const CONCEPTO_DOCUMENTO_CRUCE_ANTICIPOS_PSL As String = "APCXP"
    Const CONCEPTO_DIVISION_CRUCE_ANTICIPOS_PSL As String = "010001"
    Const CONCEPTO_TASA_CRUCE_ANTICIPO_PSL As String = "1"
    Const TIPO_CONSECUTIVO_CRUCE_ANTICIPO_PSL As String = "LIQ"

#End Region

    Protected Overrides Sub OnStart(ByVal args() As String)
        Dim objGeneral As New clsGeneral

        Try
            objGeneral.Guardar_Mensaje_Log("inicio servicio interfaz gestrans-psl")

            Me.HilProcesoTerceros = New Thread(AddressOf Proceso_Interfaz_Terceros)
            Me.HilProcesoTerceros.Start()

            Me.HilProcesoSolicitudesAnticipos = New Thread(AddressOf Proceso_Interfaz_Solicitud_Anticipos)
            Me.HilProcesoSolicitudesAnticipos.Start()

            Me.HilProcesoSolicitudesAnticiposEQP = New Thread(AddressOf Proceso_Interfaz_Solicitud_Anticipos_EQP)
            Me.HilProcesoSolicitudesAnticiposEQP.Start()

            Me.HilProcesoEgresosAnticipos = New Thread(AddressOf Proceso_Interfaz_Egresos_Anticipos)
            Me.HilProcesoEgresosAnticipos.Start()

            Me.HilProcesoLiquidacionPlanillas = New Thread(AddressOf Proceso_Interfaz_Liquidaciones_Planillas)
            Me.HilProcesoLiquidacionPlanillas.Start()

            Me.HilProcesoFacturasTransporte = New Thread(AddressOf Proceso_Interfaz_Facturas_Transporte)
            Me.HilProcesoFacturasTransporte.Start()

            Me.HilProcesoLegalizacionGastos = New Thread(AddressOf Proceso_Interfaz_Legalizacion_Gastos)
            Me.HilProcesoLegalizacionGastos.Start()

            Me.HilProcesoPolizasPlanillas = New Thread(AddressOf Proceso_Interfaz_Polizas_Planillas)
            Me.HilProcesoPolizasPlanillas.Start()

            Me.HilProcesoConsultaEstadoSolicitudAnticiposPSL = New Thread(AddressOf Proceso_Interfaz_Consulta_Estado_Solicitud_Anticipos_PSL)
            Me.HilProcesoConsultaEstadoSolicitudAnticiposPSL.Start()

            Me.HilProcesoCruceAnticiposLiquidacionesPlanillas = New Thread(AddressOf Proceso_Interfaz_Cruce_Anticipos_Liquidaciones_Planillas)
            Me.HilProcesoCruceAnticiposLiquidacionesPlanillas.Start()

        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("OnStart: " & ex.Message)
        End Try

    End Sub

    Protected Overrides Sub OnStop()
        Dim objGeneral As New clsGeneral
        Try
            Me.HilProcesoTerceros.Abort()
            Me.HilProcesoSolicitudesAnticipos.Abort()
            Me.HilProcesoSolicitudesAnticiposEQP.Abort()
            Me.HilProcesoEgresosAnticipos.Abort()
            Me.HilProcesoLiquidacionPlanillas.Abort()
            Me.HilProcesoFacturasTransporte.Abort()
            Me.HilProcesoLegalizacionGastos.Abort()
            Me.HilProcesoPolizasPlanillas.Abort()
            Me.HilProcesoConsultaEstadoSolicitudAnticiposPSL.Abort()
            Me.HilProcesoCruceAnticiposLiquidacionesPlanillas.Abort()

            objGeneral.Guardar_Mensaje_Log("Finalizó Servicio Interfaz GESTRANS-PSL")
        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("OnStop: " & ex.Message)
        End Try

    End Sub

    Public Sub New()
        Dim objGeneral As New clsGeneral

        Try
            ' Esta llamada es exigida por el diseñador.
            InitializeComponent()

            Call Cargar_Datos_App_Config()

            '--- SOLO PARA PRUEBAS

            'PROCESO TERCEROS
            'Me.HilProcesoTerceros = New Thread(AddressOf Proceso_Interfaz_Terceros)
            'Me.HilProcesoTerceros.Start()

            'PROCESO SOLICITUD ANTICIPOS PROPIETARIO TERCEROS
            'Me.HilProcesoSolicitudesAnticipos = New Thread(AddressOf Proceso_Interfaz_Solicitud_Anticipos)
            'Me.HilProcesoSolicitudesAnticipos.Start()

            'PROCESO SOLICITUD ANTICIPOS EQUIPO PROPIO
            'Me.HilProcesoSolicitudesAnticipos = New Thread(AddressOf Proceso_Interfaz_Solicitud_Anticipos_EQP)
            'Me.HilProcesoSolicitudesAnticipos.Start()

            'PROCESO EGRESOS EFECTIVO Y CAMBISTA
            'Me.HilProcesoEgresosAnticipos = New Thread(AddressOf Proceso_Interfaz_Egresos_Anticipos)
            'Me.HilProcesoEgresosAnticipos.Start()

            'PROCESO LIQUIDACION PLANILLAS-MANIFIESTOS
            'Me.HilProcesoLiquidacionPlanillas = New Thread(AddressOf Proceso_Interfaz_Liquidaciones_Planillas)
            'Me.HilProcesoLiquidacionPlanillas.Start()

            'PROCESO FACTURACION TRANSPORTE
            'Me.HilProcesoFacturasTransporte = New Thread(AddressOf Proceso_Interfaz_Facturas_Transporte)
            'Me.HilProcesoFacturasTransporte.Start()

            'PROCESO LEGALIZACION GASTOS
            'Me.HilProcesoLegalizacionGastos = New Thread(AddressOf Proceso_Interfaz_Legalizacion_Gastos)
            'Me.HilProcesoLegalizacionGastos.Start()

            'PROCESO POLIZAS PLANILLAS
            'Me.HilProcesoPolizasPlanillas = New Thread(AddressOf Proceso_Interfaz_Polizas_Planillas)
            'Me.HilProcesoPolizasPlanillas.Start()

            'PROCESO CONSULTA PSL Estado Solicitud Anticipos
            'Me.HilProcesoConsultaEstadoSolicitudAnticiposPSL = New Thread(AddressOf Proceso_Interfaz_Consulta_Estado_Solicitud_Anticipos_PSL)
            'Me.HilProcesoConsultaEstadoSolicitudAnticiposPSL.Start()

            'PROCESO CRUCE ANTICIPOS LIQUIDACIONES PLANILLAS
            'Me.HilProcesoCruceAnticiposLiquidacionesPlanillas = New Thread(AddressOf Proceso_Interfaz_Cruce_Anticipos_Liquidaciones_Planillas)
            'Me.HilProcesoCruceAnticiposLiquidacionesPlanillas.Start()

        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("New: " & ex.Message)
        End Try
    End Sub

    Private Sub Cargar_Datos_App_Config()
        Dim objGeneral As New clsGeneral
        Try
            Me.intCodigoEmpresa = ConfigurationSettings.AppSettings.Get("Empresa")
            lonIntervaloMilisegundosEjecucionProceso = ConfigurationSettings.AppSettings.Get("IntervaloMilisegundosEjecucionProceso")

        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Cargar_Datos_App_Config: " & ex.Message)
        End Try
    End Sub

    Private Sub Registrar_Proceso_PSL(ByVal strTablaImportar As String, ByRef lonIdenProcPSL As Long)
        Dim objGeneral As New clsGeneral

        Try
            Dim strMensProcPSL As String

            objGeneral.ConexionPSL.Open()
            objGeneral.ComandoSQLPSL = objGeneral.ConexionSQLPSL.CreateCommand()
            objGeneral.ComandoSQLPSL.Connection = objGeneral.ConexionSQLPSL()
            objGeneral.ComandoSQLPSL.CommandText = "sp_registrar_proceso"
            objGeneral.ComandoSQLPSL.CommandType = CommandType.StoredProcedure
            objGeneral.ComandoSQLPSL.Parameters.Clear()

            ' IN
            objGeneral.ComandoSQLPSL.Parameters.Add("@as_cia", SqlDbType.VarChar, 2) : objGeneral.ComandoSQLPSL.Parameters("@as_cia").Value = COMPANIA_PSL
            objGeneral.ComandoSQLPSL.Parameters.Add("@as_usuario", SqlDbType.VarChar, 10) : objGeneral.ComandoSQLPSL.Parameters("@as_usuario").Value = USUARIO_PSL
            objGeneral.ComandoSQLPSL.Parameters.Add("@as_parametros", SqlDbType.VarChar, 4000) : objGeneral.ComandoSQLPSL.Parameters("@as_parametros").Value = PARAMETROS_IMPORTAR_TERCEROS_PSL
            objGeneral.ComandoSQLPSL.Parameters.Add("@as_origen", SqlDbType.VarChar, 3) : objGeneral.ComandoSQLPSL.Parameters("@as_origen").Value = strTablaImportar
            ' OUT
            objGeneral.ComandoSQLPSL.Parameters.Add("@an_identificador", SqlDbType.Decimal) : objGeneral.ComandoSQLPSL.Parameters("@an_identificador").Direction = ParameterDirection.Output
            objGeneral.ComandoSQLPSL.Parameters.Add("@as_mensajeerror", SqlDbType.VarChar, 4000) : objGeneral.ComandoSQLPSL.Parameters("@as_mensajeerror").Direction = ParameterDirection.Output

            lonNumeRegi = Val(objGeneral.ComandoSQLPSL.ExecuteNonQuery().ToString())

            lonIdenProcPSL = objGeneral.ComandoSQLPSL.Parameters("@an_identificador").Value ' Si es -1 Error
            strMensProcPSL = objGeneral.ComandoSQLPSL.Parameters("@as_mensajeerror").Value

            objGeneral.ConexionSQLPSL.Close()
            objGeneral.ConexionSQLPSL.Dispose()

        Catch ex As Exception
            lonIdenProcPSL = 0
            objGeneral.Guardar_Mensaje_Log("Registrar_Proceso_PSL: " & ex.Message)

            If objGeneral.ConexionPSL.State = ConnectionState.Open Then
                objGeneral.ConexionPSL.Close()
            End If

        End Try
    End Sub

    Private Sub Registrar_Proceso_CXP_PSL(ByVal strTablaImportar As String, ByRef lonIdenProcPSL As Long)
        Dim objGeneral As New clsGeneral

        Try
            Dim strMensProcPSL As String

            objGeneral.ConexionPSL.Open()
            objGeneral.ComandoSQLPSL = objGeneral.ConexionSQLPSL.CreateCommand()
            objGeneral.ComandoSQLPSL.Connection = objGeneral.ConexionSQLPSL()
            objGeneral.ComandoSQLPSL.CommandText = "sp_registrar_proceso"
            objGeneral.ComandoSQLPSL.CommandType = CommandType.StoredProcedure
            objGeneral.ComandoSQLPSL.Parameters.Clear()

            ' IN
            objGeneral.ComandoSQLPSL.Parameters.Add("@as_cia", SqlDbType.VarChar, 2) : objGeneral.ComandoSQLPSL.Parameters("@as_cia").Value = COMPANIA_PSL
            objGeneral.ComandoSQLPSL.Parameters.Add("@as_usuario", SqlDbType.VarChar, 10) : objGeneral.ComandoSQLPSL.Parameters("@as_usuario").Value = USUARIO_PSL
            objGeneral.ComandoSQLPSL.Parameters.Add("@as_parametros", SqlDbType.VarChar, 4000) : objGeneral.ComandoSQLPSL.Parameters("@as_parametros").Value = PARAMETROS_IMPORTAR_CXP_PSL
            objGeneral.ComandoSQLPSL.Parameters.Add("@as_origen", SqlDbType.VarChar, 3) : objGeneral.ComandoSQLPSL.Parameters("@as_origen").Value = strTablaImportar
            ' OUT
            objGeneral.ComandoSQLPSL.Parameters.Add("@an_identificador", SqlDbType.Decimal) : objGeneral.ComandoSQLPSL.Parameters("@an_identificador").Direction = ParameterDirection.Output
            objGeneral.ComandoSQLPSL.Parameters.Add("@as_mensajeerror", SqlDbType.VarChar, 4000) : objGeneral.ComandoSQLPSL.Parameters("@as_mensajeerror").Direction = ParameterDirection.Output

            lonNumeRegi = Val(objGeneral.ComandoSQLPSL.ExecuteNonQuery().ToString())

            lonIdenProcPSL = objGeneral.ComandoSQLPSL.Parameters("@an_identificador").Value ' Si es -1 Error
            strMensProcPSL = objGeneral.ComandoSQLPSL.Parameters("@as_mensajeerror").Value

            objGeneral.ConexionSQLPSL.Close()
            objGeneral.ConexionSQLPSL.Dispose()


        Catch ex As Exception
            lonIdenProcPSL = 0
            objGeneral.Guardar_Mensaje_Log("Registrar_Proceso_CXP_PSL: " & ex.Message)

            If objGeneral.ConexionPSL.State = ConnectionState.Open Then
                objGeneral.ConexionPSL.Close()
            End If
        End Try
    End Sub

    Private Sub Registrar_Proceso_CXC_PSL(ByVal strTablaImportar As String, ByRef lonIdenProcPSL As Long)
        Dim objGeneral As New clsGeneral

        Try
            Dim strMensProcPSL As String

            objGeneral.ConexionPSL.Open()
            objGeneral.ComandoSQLPSL = objGeneral.ConexionSQLPSL.CreateCommand()
            objGeneral.ComandoSQLPSL.Connection = objGeneral.ConexionSQLPSL()
            objGeneral.ComandoSQLPSL.CommandText = "sp_registrar_proceso"
            objGeneral.ComandoSQLPSL.CommandType = CommandType.StoredProcedure
            objGeneral.ComandoSQLPSL.Parameters.Clear()

            ' IN
            objGeneral.ComandoSQLPSL.Parameters.Add("@as_cia", SqlDbType.VarChar, 2) : objGeneral.ComandoSQLPSL.Parameters("@as_cia").Value = COMPANIA_PSL
            objGeneral.ComandoSQLPSL.Parameters.Add("@as_usuario", SqlDbType.VarChar, 10) : objGeneral.ComandoSQLPSL.Parameters("@as_usuario").Value = USUARIO_PSL
            objGeneral.ComandoSQLPSL.Parameters.Add("@as_parametros", SqlDbType.VarChar, 4000) : objGeneral.ComandoSQLPSL.Parameters("@as_parametros").Value = PARAMETROS_IMPORTAR_CXC_PSL
            objGeneral.ComandoSQLPSL.Parameters.Add("@as_origen", SqlDbType.VarChar, 3) : objGeneral.ComandoSQLPSL.Parameters("@as_origen").Value = strTablaImportar
            ' OUT
            objGeneral.ComandoSQLPSL.Parameters.Add("@an_identificador", SqlDbType.Decimal) : objGeneral.ComandoSQLPSL.Parameters("@an_identificador").Direction = ParameterDirection.Output
            objGeneral.ComandoSQLPSL.Parameters.Add("@as_mensajeerror", SqlDbType.VarChar, 4000) : objGeneral.ComandoSQLPSL.Parameters("@as_mensajeerror").Direction = ParameterDirection.Output

            lonNumeRegi = Val(objGeneral.ComandoSQLPSL.ExecuteNonQuery().ToString())

            lonIdenProcPSL = objGeneral.ComandoSQLPSL.Parameters("@an_identificador").Value ' Si es -1 Error
            strMensProcPSL = objGeneral.ComandoSQLPSL.Parameters("@as_mensajeerror").Value

            objGeneral.ConexionSQLPSL.Close()
            objGeneral.ConexionSQLPSL.Dispose()


        Catch ex As Exception
            lonIdenProcPSL = 0
            objGeneral.Guardar_Mensaje_Log("Registrar_Proceso_CXP_PSL: " & ex.Message)

            If objGeneral.ConexionPSL.State = ConnectionState.Open Then
                objGeneral.ConexionPSL.Close()
            End If
        End Try
    End Sub

    Private Sub Informar_Proceso_PSL(ByVal lonIdenProcPSL As Long)
        Dim objGeneral = New clsGeneral

        Try
            Dim strMensProcPSL As String

            objGeneral.ConexionPSL.Open()
            objGeneral.ComandoSQLPSL = objGeneral.ConexionSQLPSL.CreateCommand()
            objGeneral.ComandoSQLPSL.Connection = objGeneral.ConexionSQLPSL()
            objGeneral.ComandoSQLPSL.CommandText = "sp_procesar_importacion"
            objGeneral.ComandoSQLPSL.CommandType = CommandType.StoredProcedure
            objGeneral.ComandoSQLPSL.Parameters.Clear()

            ' IN
            objGeneral.ComandoSQLPSL.Parameters.Add("@as_cia", SqlDbType.VarChar, 2) : objGeneral.ComandoSQLPSL.Parameters("@as_cia").Value = COMPANIA_PSL
            objGeneral.ComandoSQLPSL.Parameters.Add("@an_identificador", SqlDbType.Decimal) : objGeneral.ComandoSQLPSL.Parameters("@an_identificador").Value = lonIdenProcPSL
            ' OUT
            objGeneral.ComandoSQLPSL.Parameters.Add("@as_mensajeerror", SqlDbType.VarChar, 4000) : objGeneral.ComandoSQLPSL.Parameters("@as_mensajeerror").Direction = ParameterDirection.Output

            lonNumeRegi = Val(objGeneral.ComandoSQLPSL.ExecuteNonQuery().ToString())

            lonIdenProcPSL = objGeneral.ComandoSQLPSL.Parameters("@an_identificador").Value ' Si es -1 Error
            strMensProcPSL = objGeneral.ComandoSQLPSL.Parameters("@as_mensajeerror").Value

            objGeneral.ConexionSQLPSL.Close()
            objGeneral.ConexionSQLPSL.Dispose()


        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Informar_Proceso_PSL: " & ex.Message)
            If objGeneral.ConexionPSL.State = ConnectionState.Open Then
                objGeneral.ConexionPSL.Close()
            End If
        End Try
    End Sub

    Private Function Insertar_GESTRANS_Documento_Interfaz_Contable_PSL(dtsRow As DataRow, strTabImpPSL As String, ByVal lonIdenProc As Long) As Long
        Dim objGeneral = New clsGeneral

        Try
            Dim lonConsecutivo As Long

            objGeneral.ConexionGESTRANS.Open()
            objGeneral.ComandoSQLGESTRANS = objGeneral.ConexionSQLGESTRANS.CreateCommand()
            objGeneral.ComandoSQLGESTRANS.Connection = objGeneral.ConexionSQLGESTRANS()
            objGeneral.ComandoSQLGESTRANS.CommandText = "gsp_insertar_documentos_interfaz_contable_PSL"
            objGeneral.ComandoSQLGESTRANS.CommandType = CommandType.StoredProcedure

            objGeneral.ComandoSQLGESTRANS.Parameters.Clear()
            ' IN
            objGeneral.ComandoSQLGESTRANS.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : objGeneral.ComandoSQLGESTRANS.Parameters("@par_EMPR_Codigo").Value = dtsRow.Item("EMPR_Codigo")
            objGeneral.ComandoSQLGESTRANS.Parameters.Add("@par_TIDO_Codigo_Origen", SqlDbType.Decimal) : objGeneral.ComandoSQLGESTRANS.Parameters("@par_TIDO_Codigo_Origen").Value = dtsRow.Item("TIDO_Codigo")
            objGeneral.ComandoSQLGESTRANS.Parameters.Add("@par_Numero_Origen", SqlDbType.Decimal) : objGeneral.ComandoSQLGESTRANS.Parameters("@par_Numero_Origen").Value = dtsRow.Item("Numero")
            objGeneral.ComandoSQLGESTRANS.Parameters.Add("@par_Numero_Documento_Origen", SqlDbType.Decimal) : objGeneral.ComandoSQLGESTRANS.Parameters("@par_Numero_Documento_Origen").Value = dtsRow.Item("Documento_Origen")
            objGeneral.ComandoSQLGESTRANS.Parameters.Add("@par_TIPO_Documento_PSL", SqlDbType.VarChar, 50) : objGeneral.ComandoSQLGESTRANS.Parameters("@par_TIPO_Documento_PSL").Value = strTabImpPSL
            objGeneral.ComandoSQLGESTRANS.Parameters.Add("@par_USUA_Codigo_Crea", SqlDbType.Int) : objGeneral.ComandoSQLGESTRANS.Parameters("@par_USUA_Codigo_Crea").Value = 0 ' Sistema
            objGeneral.ComandoSQLGESTRANS.Parameters.Add("@par_Numero_Proceso_PSL", SqlDbType.Int) : objGeneral.ComandoSQLGESTRANS.Parameters("@par_Numero_Proceso_PSL").Value = lonIdenProc
            objGeneral.ComandoSQLGESTRANS.Parameters.Add("@par_Proceso_PSL", SqlDbType.Int) : objGeneral.ComandoSQLGESTRANS.Parameters("@par_Proceso_PSL").Value = 0

            ' OUT
            objGeneral.ComandoSQLGESTRANS.Parameters.Add("@par_Codigo", SqlDbType.Decimal) : objGeneral.ComandoSQLGESTRANS.Parameters("@par_Codigo").Direction = ParameterDirection.Output

            lonNumeRegi = Val(objGeneral.ComandoSQLGESTRANS.ExecuteNonQuery().ToString())

            lonConsecutivo = objGeneral.ComandoSQLGESTRANS.Parameters("@par_Codigo").Value

            objGeneral.ConexionSQLGESTRANS.Close()
            objGeneral.ConexionSQLGESTRANS.Dispose()

            Return lonConsecutivo

        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Insertar_GESTRANS_Documento_Interfaz_Contable_PSL: " & ex.Message)
            If objGeneral.ConexionGESTRANS.State = ConnectionState.Open Then
                objGeneral.ConexionGESTRANS.Close()
            End If
            Return 0
        End Try
    End Function

    Private Sub Proceso_Interfaz_Terceros()
        Dim objGeneral As New clsGeneral
        Try
            While (True)
                objGeneral.Guardar_Mensaje_Log("Inicio Interfaz Terceros GESTRANS-PSL")

                Call Interfaz_Terceros()

                Thread.Sleep(lonIntervaloMilisegundosEjecucionProceso)
            End While
        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Proceso_Interfaz_Terceros: " & ex.Message)
        End Try
    End Sub

    Private Sub Interfaz_Terceros()
        Dim objGeneral As New clsGeneral

        Try
            Dim strSQL As String, dtsTerceros As DataSet, lonIdenProcPSL As Long

            strSQL = "SELECT TERC.CATA_TINT_Codigo, TERC.CATA_TIID_Codigo, TERC.Numero_Identificacion, TERC.Nombre, TERC.Apellido1,"
            strSQL += "TERC.Razon_Social, CIUD.Codigo_Alterno AS Codigo_Dane, TERC.Codigo, "
            strSQL += "TERC.Apellido2, TERC.CIUD_Codigo, SUBSTRING(TERC.Direccion, 1, 30) Direccion, SUBSTRING(TERC.Direccion, 31, 60) Direccion2, "
            strSQL += "TERC.Telefonos, TERC.Celulares, TERC.Emails "
            strSQL += "FROM Terceros As TERC, Ciudades As CIUD "
            strSQL += "WHERE TERC.EMPR_Codigo = CIUD.EMPR_Codigo "
            strSQL += "AND TERC.CIUD_Codigo = CIUD.Codigo "
            strSQL += "AND Terc.EMPR_Codigo = " & intCodigoEmpresa.ToString
            'strSQL += "AND (TERC.Fecha_Crea > GETDATE() - 120 OR TERC.Fecha_Modifica > GETDATE() - 120)"
            strSQL += "AND (TERC.Fecha_Crea > DATEADD(MI,-5,GETDATE()) OR TERC.Fecha_Modifica > DATEADD(MI,-5,GETDATE()))"
            'strSQL += "AND Numero_Identificacion = '900950495'" ' TEMPORAL PRUEBAS

            dtsTerceros = objGeneral.Retorna_Dataset(strSQL)

            If dtsTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                Call Registrar_Proceso_PSL(TABLA_IMPORTAR_TERCEROS_PSL, lonIdenProcPSL)

                Call Insertar_Terceros_PSL(dtsTerceros, lonIdenProcPSL)

                Call Informar_Proceso_PSL(lonIdenProcPSL)

            End If

        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Interfaz_Terceros: " & ex.Message)
        End Try
    End Sub

    Private Sub Insertar_Terceros_PSL(ByRef dtsTerceros As DataSet, ByVal lonIdenProcPSL As Long)
        Dim objGeneral As New clsGeneral
        Try
            Dim intCon As Integer, strSQL As String, lonRes As Long
            ' Campos tabla GESTRANS
            Dim strTipoNatu As String, strTipoIden As String, strNumeIden As String, strNombre As String, strApellido1 As String
            Dim strApellido2 As String, strCodiCiud As String, strDireccion As String, strTelefonos As String, strCelulares As String, strEmails As String
            Dim strDireccion2 As String, strCliente As String, strTipoCli As String, strActivEcon As String, strOperTercPSL As String, strCodiTerc As String
            Dim bolPerfilCliente As Boolean, bolPerfilTransportador As Boolean, strRegiTerc As String = ""

            For intCon = 0 To dtsTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                strTipoNatu = dtsTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("CATA_TINT_Codigo").ToString()
                strTipoIden = dtsTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("CATA_TIID_Codigo").ToString()
                strNumeIden = dtsTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Identificacion").ToString()
                If strTipoIden = TIPO_IDENTIFICACION_NIT_GESTRANS Then
                    strApellido1 = dtsTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Razon_Social").ToString()
                Else
                    strApellido1 = dtsTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Apellido1").ToString()
                End If
                strNombre = dtsTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Nombre").ToString()
                strApellido2 = dtsTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Apellido2").ToString()
                strCodiCiud = dtsTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("CIUD_Codigo").ToString()
                strDireccion = dtsTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Direccion").ToString()
                strDireccion2 = dtsTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Direccion2").ToString()
                strTelefonos = dtsTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Telefonos").ToString()
                strCelulares = dtsTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Celulares").ToString()
                strEmails = dtsTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Emails").ToString()
                strCodiCiud = dtsTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Codigo_Dane").ToString()
                strCodiTerc = dtsTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Codigo").ToString()

                ' Valida Perfiles (Propietario/Conductor/Tenedor/Afiliador corresponde a PerfilTransportador y si es Cliente)
                If Retorna_Perfil_Tercero(strCodiTerc, bolPerfilTransportador, bolPerfilCliente) Then
                    strActivEcon = VALOR_ACTIVIDAD_ECONOMICA
                Else
                    strActivEcon = VALOR_CERO
                End If
                If bolPerfilCliente Then
                    strCliente = VALOR_S
                    strTipoCli = "COMER"
                Else
                    strCliente = VALOR_N
                    strTipoCli = ""
                End If

                If strTipoIden = TIPO_IDENTIFICACION_NIT_GESTRANS Then
                    strTipoNatu = TIPO_NATURALEZA_TERCERO_JURIDICA_PSL
                    strTipoIden = TIPO_IDENTIFICACION_NIT_PSL
                    strRegiTerc = TIPO_REGIMEN_IVA_NIT_PSL
                Else
                    strTipoNatu = TIPO_NATURALEZA_TERCERO_NATURAL_PSL
                    strTipoIden = TIPO_IDENTIFICACION_NATURAL_PSL
                    strRegiTerc = TIPO_REGIMEN_IVA_NATURAL_PSL
                End If

                strTelefonos = Mid$(strTelefonos, 1, 22) ' Longitud Telefonos 22
                strDireccion2 = IIf(Equals(strDireccion2, String.Empty), DBNull.Value.ToString, strDireccion2)

                If Existe_Tercero_PSL(strNumeIden) Then
                    strOperTercPSL = OPERACION_MODIFICACION_TERCEROS_PSL
                Else
                    strOperTercPSL = OPERACION_CREACION_TERCEROS_PSL
                End If

                ' Insertar tabla general UN_IMPOTERCGENE
                strSQL = "INSERT INTO UN_IMPOTERCGENE("
                strSQL += "itgoperacion, itgcontrol, itgcompania, itgcoditerce, itgnaturaleza,"
                strSQL += "itgtipoiden, itgnit, itgrazosoci, itgseguapel, itgnombre,"
                strSQL += "itgactiecon, itgdireline1, itgdireline2, itgciudad, itgtelefono1," ' itgEstado (Falta definir campo)
                strSQL += "itgagenrete, itggrancont, itgautorete, itgexenrete, itgregiiva,"
                strSQL += "itgemail, itgmatrestaprov, itgmatrestaclie, itgmatrreteicaprov, itgIdentificador,"
                strSQL += "itgtipoprov, itgtipoclie"
                strSQL += ") VALUES('"
                strSQL += strOperTercPSL & "','" & CONTROL_TERCEROS_PSL & "','" & COMPANIA_PSL & "','" & strNumeIden & "','" & strTipoNatu & "','"
                strSQL += strTipoIden & "','" & strNumeIden & "','" & strApellido1 & "','" & strApellido2 & "','" & strNombre & "','"
                strSQL += strActivEcon & "','" & strDireccion & "','" & strDireccion2 & "','" & strCodiCiud & "','" & strTelefonos & "','"
                strSQL += VALOR_N & "','" & VALOR_N & "','" & VALOR_N & "','" & VALOR_N & "','" & strRegiTerc & "','"
                strSQL += strEmails & "','" & VALOR_S & "','" & strCliente & "','" & VALOR_S & "'," & lonIdenProcPSL.ToString & ",'"
                strSQL += PERFIL_TRANSPORTADOR_TERCEROS_PSL & "','" & strTipoCli & "'"
                strSQL += ")"

                lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)

                ' Si el Tercero es Nuevo y Codigo Actividad Economica 4923 Asiganda a (PROPIETARIO, CONDUCTOR, TENEDOR, AFILIADOR)
                If strOperTercPSL = OPERACION_CREACION_TERCEROS_PSL And strActivEcon = VALOR_ACTIVIDAD_ECONOMICA Then

                    ' Insertar Tabla general UN_TERCACTIECON
                    strSQL = "INSERT INTO UN_TERCACTIECON("
                    strSQL += "taetercero, taeactividad, taecompania, taedivision, taedefecto,"
                    strSQL += "eobcodigo, eobnombre, eobfecha, eobjustificacion, eobusuario,"
                    strSQL += "taeprincipal"
                    strSQL += ") VALUES("
                    strSQL += "'" & strNumeIden & "','" & VALOR_ACTIVIDAD_ECONOMICA_ALTERNA & "','" & COMPANIA_PSL & "','" & COMPANIA_PSL & "','" & VALOR_S & "','"
                    strSQL += ESTADO_TERCEROS_PSL & "','Activo', GETDATE(), " & VALOR_NULO_SQL_PSL & ", 'GESTRANS', '"
                    strSQL += VALOR_S & "')"

                    lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)

                    ' Insertar Tabla general UN_MULTIPROVE
                    strSQL = "INSERT INTO UN_MULTIPROVE"
                    strSQL += "(mprtiprove, mprestablecimiento, mprproveedor, mprcompania, eobcodigo"
                    strSQL += ",eobnombre, eobfecha, eobjustificacion, eobusuario"
                    strSQL += ") VALUES("
                    strSQL += "'ANTCA','" & VALOR_CERO & VALOR_CERO & "','" & strNumeIden & "','" & COMPANIA_PSL & "','" & ESTADO_TERCEROS_PSL
                    strSQL += "', 'Activo', GETDATE(), 'ANTICIPO CAMBISTA', 'GESTRANS')"

                    lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)
                End If

            Next

        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Interfaz_Terceros_PSL: " & ex.Message)
        End Try

    End Sub

    ' Autor: Jesús Cuadros 
    ' Fecha Creación: 26-JUL-2019
    ' Observaciones Creación: La interfaz de solicitud de anticipos se genera para las CxP de Antipos con medio pago Transferencia
    ' Fecha Modificación: 
    ' Observaciones Modificación:
    Private Sub Proceso_Interfaz_Solicitud_Anticipos()
        Dim objGeneral As New clsGeneral
        Try
            While (True)
                objGeneral.Guardar_Mensaje_Log("Inicio Interfaz Solicitud Anticipos GESTRANS-PSL")

                Call Interfaz_Solicitud_Anticipos()

                Thread.Sleep(lonIntervaloMilisegundosEjecucionProceso)
            End While
        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Proceso_Interfaz_Solicitud_Anticipos: " & ex.Message)
        End Try
    End Sub

    Private Sub Interfaz_Solicitud_Anticipos()
        Dim objGeneral As New clsGeneral

        Try
            Dim strSQL As String, dtsCuentasPagar As DataSet, lonIdenProcPSL As Long

            strSQL = "SELECT TOP 100 *, TERC.Numero_Identificacion AS Identificacion_Anticipo,"
            strSQL += " OFIC.Codigo_Alterno AS OFIC_Codigo_Alterno"
            strSQL += " FROM Encabezado_Documento_Cuentas AS ENDC, Terceros AS TERC,"
            strSQL += " Encabezado_Planilla_Despachos AS ENPD, Oficinas AS OFIC,"
            strSQL += " Vehiculos AS VEHI, Terceros AS PROP"
            strSQL += " WHERE ENDC.EMPR_Codigo = ENPD.EMPR_Codigo"
            strSQL += " AND ENDC.Codigo_Documento_Origen = ENPD.Numero"
            strSQL += " AND ENPD.EMPR_Codigo = OFIC.EMPR_Codigo "
            strSQL += " AND ENPD.OFIC_Codigo = OFIC.Codigo"
            strSQL += " AND ENDC.EMPR_Codigo = TERC.EMPR_Codigo"
            strSQL += " AND IIF(ENPD.Anticipo_Pagado_A = 'Tenedor', ENPD.TERC_Codigo_Tenedor ,ENDC.TERC_Codigo ) = TERC.Codigo "
            strSQL += " AND ENDC.EMPR_Codigo = " & intCodigoEmpresa.ToString
            strSQL += " AND ENDC.TIDO_Codigo = " & TIDO_CUENTA_X_PAGAR.ToString
            strSQL += " AND ENDC.CATA_DOOR_Codigo = " & DOCUMENTO_ORIGEN_ANTICIPO.ToString
            strSQL += " AND ENDC.Codigo_Alterno = '" & FORMA_PAGO_DESPACHO_TRANSFERENCIA & "'"
            strSQL += " AND ENDC.Anulado = 0"
            strSQL += " AND ENPD.EMPR_Codigo = VEHI.EMPR_Codigo"
            strSQL += " AND ENPD.VEHI_Codigo = VEHI.Codigo"
            strSQL += " AND VEHI.EMPR_Codigo = PROP.EMPR_Codigo"
            strSQL += " AND VEHI.TERC_Codigo_Propietario = PROP.Codigo"
            strSQL += " AND NOT EXISTS (SELECT Numero_Origen FROM Documentos_Interfaz_Contable_PSL"
            strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
            strSQL += " AND TIDO_Codigo_Origen = " & TIDO_CUENTA_X_PAGAR.ToString
            strSQL += " AND Numero_Origen = ENDC.Numero)"
            strSQL += " AND PROP.Codigo <> 1 " ' VEHICULOS PROPIETARIOS TERCEROS 

            dtsCuentasPagar = objGeneral.Retorna_Dataset(strSQL)

            If dtsCuentasPagar.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                Call Registrar_Proceso_CXP_PSL(TABLA_IMPORTAR_ANTICIPOS_PSL, lonIdenProcPSL)

                Call Insertar_Solicitud_Anticipos_PSL(dtsCuentasPagar, lonIdenProcPSL)

                Call Informar_Proceso_PSL(lonIdenProcPSL)

            End If

        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Interfaz_Solicitud_Anticipos: " & ex.Message)
        End Try

    End Sub

    Private Sub Insertar_Solicitud_Anticipos_PSL(ByRef dtsCuentasPagar As DataSet, ByVal lonIdenProcPSL As Long)
        Dim objGeneral As New clsGeneral

        Try
            Dim intCon As Integer, strSQL As String, lonRes As Long, lonConsecutivo As Long
            ' Campos tabla GESTRANS
            Dim lonNumCtaPag As Long, datFecCtaPag As Date, lonCodiTerc As Long, dblValoTota As Double, strNumeIden As String
            Dim strFecCtaPag As String, lonNumDocOri As Long, strNumDocAnticipo As String, strCentResp

            For intCon = 0 To dtsCuentasPagar.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                lonNumCtaPag = dtsCuentasPagar.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero").ToString()
                lonNumDocOri = dtsCuentasPagar.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Documento_Origen").ToString() ' No. Planilla
                datFecCtaPag = dtsCuentasPagar.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha").ToString()
                lonCodiTerc = dtsCuentasPagar.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("TERC_Codigo").ToString()
                dblValoTota = dtsCuentasPagar.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Total").ToString()
                strNumDocAnticipo = dtsCuentasPagar.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Anticipo").ToString() ' edpComentario
                strCentResp = dtsCuentasPagar.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("OFIC_Codigo_Alterno").ToString() ' Codigo Alterno Oficina Planilla

                strNumeIden = Retorna_Identificacion_Tenedor_Planilla(lonNumDocOri).ToString ' edpProveedor

                ' Formatear Valores
                strFecCtaPag = "CONVERT(DATE, '" & datFecCtaPag.Month & "/" & datFecCtaPag.Day & "/" & datFecCtaPag.Year & "', 101)" ' 101=MM/DD/YYYY
                lonNumCtaPag -= 1000000 ' Este campo es solo de 6 digitos, por eso se resta 1,000,000

                lonConsecutivo = Insertar_GESTRANS_Documento_Interfaz_Contable_PSL(dtsCuentasPagar.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon), TABLA_IMPORTAR_ANTICIPOS_PSL & "-SOLICITUD ANTICIPO", lonIdenProcPSL)

                ' Insertar Encabezado tabla CP_ENCDOCPAGOENTRA
                strSQL = "INSERT INTO CP_ENCDOCPAGOENTRA("
                strSQL += "edpconseingre, edpcontrol, edpvalorcontr, edpcompania, edpdivision, "
                strSQL += "edptipocons, edpconsecutivo, edpclasedocum, edptipodocu, edpnumero, "
                strSQL += "edpproveedor, edpestablecimiento, edpconcdocu, edpmoneda, edpfechexpe, "
                strSQL += "edpfechrece, edpcentrorespo, edpformapago, edpfechaconta, edptotmonedlocal,"
                strSQL += "edpIdentificador, edpprefijo, edptipoprove, edpcomentario) VALUES("
                strSQL += lonConsecutivo.ToString & ",'" & CONTROL_SOLICITUD_ANTICIPO_PSL & "'," & dblValoTota.ToString & ",'" & COMPANIA_PSL & "','" & DIVISION_PSL & "','"
                strSQL += TIPO_CONSECUTIVO_SOLICITUD_ANTICIPO_PSL & "','" & lonNumCtaPag.ToString & "','" & CLASE_CONSECUTIVO_SOLICITUD_ANTICIPO_PSL & "','" & TIPO_CONSECUTIVO_SOLICITUD_ANTICIPO_PSL & "','" & lonNumDocOri.ToString & "','"
                strSQL += strNumeIden & "','" & ESTABLECIMIENTO_SOLICITUD_ANTICIPO_PSL & "','" & CONCEPTO_DOCUMENTO_SOLICITUD_ANTICIPO_PSL & "','" & MONEDA_PESOS_PSL & "'," & strFecCtaPag & ","
                strSQL += strFecCtaPag & ",'" & strCentResp & "','" & FORMA_PAGO_SOLICITUD_ANTICIPO_PSL & "'," & strFecCtaPag & "," & dblValoTota.ToString & ","
                strSQL += lonIdenProcPSL.ToString & ",'" & PREFIJO_SOLICITUD_ANTICIPO_PSL & "','" & TIPO_PROVEEDOR_SOLICITUD_ANTICIPO_PSL & "','" & strNumDocAnticipo & "'"
                strSQL += ")"
                lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)

                ' Insertar Detalle tabla CP_DETDOCPAGOENTRA
                strSQL = "INSERT INTO CP_DETDOCPAGOENTRA("
                strSQL += "ddpconseingre, ddpcontrol, ddpcompania, ddptipocons, ddpconsecutivo,"
                strSQL += "ddpsecuencia, ddpunidaventa, ddpcantidad, ddppreciounitabruto"
                strSQL += ") VALUES("
                strSQL += lonConsecutivo.ToString & ",'" & CONTROL_SOLICITUD_ANTICIPO_PSL & "','" & COMPANIA_PSL & "','" & TIPO_CONSECUTIVO_SOLICITUD_ANTICIPO_PSL & "'," & lonNumCtaPag.ToString & ","
                strSQL += "1, 'UN', 1, " & dblValoTota.ToString
                strSQL += ")"
                lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)

            Next

        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Insertar_Solicitud_Anticipos_PSL: " & ex.Message)

        End Try
    End Sub

    ' Autor: Jesús Cuadros 
    ' Fecha Creación: 16-AGO-2019
    ' Observaciones Creación: Interfaz Legalización Gastos
    ' Fecha Modificación: 
    ' Observaciones Modificación:
    Private Sub Proceso_Interfaz_Solicitud_Anticipos_EQP()
        Dim objGeneral As New clsGeneral
        Try
            While (True)
                objGeneral.Guardar_Mensaje_Log("Inicio Interfaz Solicitud Anticipos EQP GESTRANS-PSL")

                Call Interfaz_Solicitud_Anticipos_EQP()

                Thread.Sleep(lonIntervaloMilisegundosEjecucionProceso)
            End While
        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Inicio Interfaz Solicitud Anticipos EQP GESTRANS-PSL: " & ex.Message)

        End Try
    End Sub


    Private Sub Interfaz_Solicitud_Anticipos_EQP()
        Dim objGeneral As New clsGeneral

        Try
            Dim strSQL As String, dtsLegaGast As DataSet, lonIdenProcPSL As Long

            strSQL = "SELECT TOP 100 *, ENDC.Observaciones AS ENCO_Observaciones, COND.Numero_Identificacion AS IDENTIFICACION_CONDUCTOR, ENPD.Numero_Documento AS NUMERO_PLANILLA,"
            strSQL += " ENDC.Numero AS Numero_Documento, CONVERT(VARCHAR(10),ENDC.Fecha,103) AS Fecha_Comprobante"
            strSQL += " FROM Encabezado_Documento_Comprobantes AS ENDC,"
            strSQL += " Encabezado_Planilla_Despachos AS ENPD,"
            strSQL += " Terceros AS COND,Vehiculos AS VEHI,Terceros AS PROP"
            strSQL += " WHERE ENDC.EMPR_Codigo = ENPD.EMPR_Codigo "
            strSQL += " AND ENDC.Documento_Origen = ENPD.Numero_Documento"

            strSQL += " AND ENPD.EMPR_Codigo = COND.EMPR_Codigo"
            strSQL += " AND ENPD.TERC_Codigo_Conductor = COND.Codigo"
            strSQL += " AND ENPD.EMPR_Codigo = VEHI.EMPR_Codigo"
            strSQL += " AND ENPD.VEHI_Codigo = VEHI.Codigo"
            strSQL += " AND VEHI.EMPR_Codigo = PROP.EMPR_Codigo"
            strSQL += " AND VEHI.TERC_Codigo_Propietario = PROP.Codigo"

            strSQL += " AND ENDC.EMPR_Codigo = " & intCodigoEmpresa.ToString
            strSQL += " AND ENDC.TIDO_Codigo = " & TIDO_COMPROBANTE_EGRESO.ToString
            strSQL += " AND ENDC.CATA_DOOR_Codigo = " & DOCUMENTO_ORIGEN_SOBREANTICIPO.ToString
            strSQL += " AND ENDC.Anulado = 0"
            strSQL += " AND ENDC.Estado = 1"

            strSQL += " AND NOT EXISTS (SELECT Numero_Origen FROM Documentos_Interfaz_Contable_PSL"
            strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
            strSQL += " AND TIDO_Codigo_Origen = " & TIDO_COMPROBANTE_EGRESO.ToString
            strSQL += " AND Numero_Origen = ENDC.Numero)"
            strSQL += " AND PROP.Codigo = 1 " ' VEHICULOS PROPIETARIOS EQUIPO PROPIO

            dtsLegaGast = objGeneral.Retorna_Dataset(strSQL)

            If dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                Call Registrar_Proceso_PSL(TABLA_IMPORTAR_LEGALIZACION_GASTOS_PSL, lonIdenProcPSL)

                Call Insertar_Solicitud_Anticipos_EQP_PSL(dtsLegaGast, lonIdenProcPSL)

                Call Informar_Proceso_PSL(lonIdenProcPSL)

            End If

        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Interfaz_Solicitud_Anticipos_EQP: " & ex.Message)

        End Try
    End Sub

    Private Sub Insertar_Solicitud_Anticipos_EQP_PSL(ByRef dtsLegaGast As DataSet, ByVal lonIdenProcPSL As Long)
        Dim objGeneral As New clsGeneral

        Try
            Dim intCon As Integer, strSQL As String, lonRes As Long, lonConsecutivo As Long, intNumeSecu As Integer = 1
            Dim strFecha As String, intPeriodo As Integer, intAno As Integer, intOperacion As Integer = 1, strFechOrig As String
            ' Campos tabla GESTRANS
            Dim strFechaComp As String, strNumIdeCon As String, lonNumero As Long, lonNumeDocu As Long, lonNumePlan As Long
            Dim dblValoGast As Double, lonNumeAnte As Long = 0
            Dim strObservacion As String, datFecha As Date

            For intCon = 0 To dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                lonNumero = dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero").ToString() ' EQP
                lonNumeDocu = dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString() ' No. Anticipo EQP
                lonNumePlan = dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("NUMERO_PLANILLA").ToString() ' No. Planilla EQP
                datFecha = dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha").ToString() 'EQP
                strFechaComp = dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha_Comprobante").ToString() 'EQP
                dblValoGast = dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Pago_Recaudo_Transferencia").ToString() ' EQP
                strObservacion = dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("ENCO_Observaciones").ToString() ' EQP
                strNumIdeCon = dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("IDENTIFICACION_CONDUCTOR").ToString() 'EQP

                ' Formatear Valores
                strFechOrig = datFecha.Day & "-" & datFecha.Month & "-" & datFecha.Year
                intPeriodo = datFecha.Month
                intAno = datFecha.Year

                ' Para los conceptos la intOperacion = 1

                If lonNumero <> lonNumeAnte Then

                    ' Registrar en Tabla Control GESTRANS
                    lonConsecutivo = Insertar_GESTRANS_Documento_Interfaz_Contable_PSL(dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon), TABLA_IMPORTAR_LEGALIZACION_GASTOS_PSL & "-SOLICITUD ANTICIPO EQP", lonIdenProcPSL)
                    intNumeSecu = 1

                    ' Inserta Un Encabezado para cada Legalizacion 
                    strSQL = "INSERT INTO CO_MOVIMENTRA (mencomprobante, menfuente, menlote, mendivision, menseculine,"
                    strSQL += " menperiodo, menano, mencompania, menfechcont, menfechtran,"
                    strSQL += " menoperacion, mencuenta, mencodielem1, menvalomoneloca,"
                    strSQL += " mencodimoneextr, menvalomoneextr, mentasacamb, menvalobasereteiva, mendiasdife,"
                    strSQL += " menorigen, mentipomone, menajusinfl, mencantidad, mencomentario,"
                    strSQL += " menidentificador"
                    strSQL += ") VALUES("
                    strSQL += lonConsecutivo.ToString & ",'" & CONCEPTO_DOCUMENTO_EGRESO_TRANSFERENCIA_ANTICIPO_EQP_PSL & "','0','" & DIVISION_PSL & "'," & intNumeSecu.ToString & ","
                    strSQL += intPeriodo.ToString & "," & intAno.ToString & ",'" & COMPANIA_PSL & "','" & strFechaComp & "','" & strFechaComp & "',"
                    strSQL += VALOR_NULO_SQL_PSL & "," & VALOR_NULO_SQL_PSL & ",''," & VALOR_NULO_SQL_PSL & ","
                    strSQL += VALOR_NULO_SQL_PSL & "," & VALOR_NULO_SQL_PSL & ", 0, 0, 0,"
                    strSQL += "'" & lonNumeDocu.ToString & "','" & TIPO_MONEDA_LEGALIZACION_GASTOS_PSL & "'," & VALOR_NULO_SQL_PSL & ",0," & VALOR_NULO_SQL_PSL & ","
                    strSQL += lonIdenProcPSL.ToString
                    strSQL += ")"

                    lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)


                    ' ANTICIPO intOperacion = 2 porque la operación 2=Débito
                    intOperacion = 2
                    intNumeSecu += 1

                    strSQL = "INSERT INTO CO_MOVIMENTRA (mencomprobante, menfuente, menlote, mendivision, menseculine,"
                    strSQL += " menperiodo, menano, mencompania, menfechcont, menfechtran,"
                    strSQL += " menoperacion, mencuenta, mencodielem1, menvalomoneloca,"
                    strSQL += " mencodimoneextr, menvalomoneextr, mentasacamb, menvalobasereteiva, mendiasdife,"
                    strSQL += " menorigen, mentipomone, menajusinfl, mencantidad, mencomentario,"
                    strSQL += " menidentificador, mencodielem3, mencodielem4"
                    strSQL += ") VALUES("
                    strSQL += lonConsecutivo.ToString & ",'" & CONCEPTO_DOCUMENTO_EGRESO_TRANSFERENCIA_ANTICIPO_EQP_PSL & "','0','" & DIVISION_PSL & "'," & intNumeSecu.ToString & ","
                    strSQL += intPeriodo.ToString & "," & intAno.ToString & ",'" & COMPANIA_PSL & "','" & strFechaComp & "','" & strFechaComp & "',"
                    strSQL += intOperacion.ToString & ",'" & CUENTA_ANTICIPO_SIN_PLANILLA & "',''," & dblValoGast.ToString & ","
                    strSQL += "'" & MONEDA_PESOS_PSL & "'," & dblValoGast & ", 0, 0, 0,"
                    strSQL += "'" & lonNumeDocu.ToString & "','" & TIPO_MONEDA_LEGALIZACION_GASTOS_PSL & "','" & AJUSTE_INFLACION_LEGALIZACION_GASTOS_PSL & "',0,'ANTICIPO EQP ID " & strObservacion & " PLANILLA " & lonNumePlan.ToString & "',"
                    strSQL += lonIdenProcPSL.ToString & ",'" & strNumIdeCon.ToString & "', '" & strObservacion & "'"
                    strSQL += ")"

                    lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)


                    intNumeSecu += 1
                    intOperacion = 1


                    strSQL = "INSERT INTO CO_MOVIMENTRA (mencomprobante, menfuente, menlote, mendivision, menseculine,"
                    strSQL += " menperiodo, menano, mencompania, menfechcont, menfechtran,"
                    strSQL += " menoperacion, mencuenta, mencodielem1, menvalomoneloca,"
                    strSQL += " mencodimoneextr, menvalomoneextr, mentasacamb, menvalobasereteiva, mendiasdife,"
                    strSQL += " menorigen, mentipomone, menajusinfl, mencantidad, mencomentario,"
                    strSQL += " menidentificador, mencodielem3, mencodielem4"
                    strSQL += ") VALUES("
                    strSQL += lonConsecutivo.ToString & ",'" & CONCEPTO_DOCUMENTO_EGRESO_TRANSFERENCIA_ANTICIPO_EQP_PSL & "','0','" & DIVISION_PSL & "'," & intNumeSecu.ToString & ","
                    strSQL += intPeriodo.ToString & "," & intAno.ToString & ",'" & COMPANIA_PSL & "','" & strFechaComp & "','" & strFechaComp & "',"
                    strSQL += intOperacion.ToString & ",'" & CUENTA_ANTICIPO_CON_PLANILLA & "',''," & dblValoGast.ToString & ","
                    strSQL += "'" & MONEDA_PESOS_PSL & "'," & dblValoGast & ", 0, 0, 0,"
                    strSQL += "'" & lonNumeDocu.ToString & "','" & TIPO_MONEDA_LEGALIZACION_GASTOS_PSL & "','" & AJUSTE_INFLACION_LEGALIZACION_GASTOS_PSL & "',0,'ANTICIPO EQP ID " & strObservacion & " PLANILLA " & lonNumePlan.ToString & "',"
                    strSQL += lonIdenProcPSL.ToString & ",'" & strNumIdeCon.ToString & "', '" & lonNumePlan.ToString & "'"
                    strSQL += ")"

                    lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)
                End If

                lonNumeAnte = lonNumero

            Next

        Catch ex As Exception

        End Try
    End Sub

    ' Autor: Jesús Cuadros 
    ' Fecha Creación: 30-JUL-2019
    ' Observaciones Creación: La interfaz de egresos anticipos se genera para los Egresos de Anticipos con medio pago Efectivo y Cambista
    ' Fecha Modificación: 
    ' Observaciones Modificación:
    Private Sub Proceso_Interfaz_Egresos_Anticipos()
        Dim objGEneral As New clsGeneral
        Try
            While (True)
                objGEneral.Guardar_Mensaje_Log("Inicio Interfaz Egresos GESTRANS-PSL")

                Call Interfaz_Egresos_Anticipos()

                Thread.Sleep(lonIntervaloMilisegundosEjecucionProceso)
            End While
        Catch ex As Exception
            objGEneral.Guardar_Mensaje_Log("Proceso_Interfaz_Egresos_Anticipos: " & ex.Message)
        End Try
    End Sub

    Private Sub Interfaz_Egresos_Anticipos()
        Dim objGeneral As New clsGeneral

        Try
            Dim strSQL As String, dtsCompEgre As DataSet, lonIdenProcPSL As Long

            strSQL = "SELECT TOP 100 ENDC.*, ISNULL(CONVERT(VARCHAR(10),VEH.CATA_TIDV_Codigo),'') AS TIDV_Codigo, CIUD.Codigo_Alterno AS CIUD_Codigo_Dane, OFIC.Codigo_Alterno AS OFIC_Codigo_Alterno,"
            strSQL += "CASE WHEN ISNULL(CAJ.Codigo_Alterno,0) <> 0 THEN CONVERT(VARCHAR(5),CAJ.Codigo_Alterno) ELSE '0' END AS CAJ_Codigo_Alterno"
            strSQL += " FROM Encabezado_Documento_Comprobantes AS ENDC,"
            strSQL += " Encabezado_Planilla_Despachos AS ENPD INNER JOIN Vehiculos VEH ON VEH.Codigo = ENPD.VEHI_Codigo, Rutas AS RUT, Ciudades AS CIUD, Oficinas AS OFIC"
            strSQL += " LEFT JOIN Cajas CAJ ON CAJ.OFIC_Codigo = OFIC.Codigo"
            strSQL += " WHERE ENDC.EMPR_Codigo = ENPD.EMPR_Codigo"
            strSQL += " AND ENDC.Documento_Origen = ENPD.Numero_Documento"
            'strSQL += " AND ENDC.EMPR_Codigo = VEH.EMPR_Codigo"
            'strSQL += " AND ENDC.VEHI_Codigo = VEH.Codigo"
            strSQL += " AND ENPD.EMPR_Codigo = RUT.EMPR_Codigo"
            strSQL += " AND	ENPD.RUTA_Codigo = RUT.Codigo"
            strSQL += " AND	RUT.EMPR_Codigo = CIUD.EMPR_Codigo "
            strSQL += " AND	RUT.CIUD_Codigo_Origen = CIUD.Codigo"
            strSQL += " AND ENPD.EMPR_Codigo = OFIC.EMPR_Codigo "
            strSQL += " AND ENPD.OFIC_Codigo = OFIC.Codigo"
            strSQL += " AND ENDC.EMPR_Codigo = " & intCodigoEmpresa.ToString
            strSQL += " AND ENDC.TIDO_Codigo = " & TIDO_COMPROBANTE_EGRESO.ToString
            strSQL += " AND ENDC.Estado = 1"
            strSQL += " AND ENDC.Anulado = 0"
            strSQL += " AND NOT EXISTS (SELECT Numero_Origen FROM Documentos_Interfaz_Contable_PSL"
            strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
            strSQL += " AND TIDO_Codigo_Origen = " & TIDO_COMPROBANTE_EGRESO.ToString
            strSQL += " AND Numero_Origen = ENDC.Numero)"
            strSQL += " AND VEH.CATA_TIDV_Codigo = " & TIPO_DUENO_VEHICULO_TERCERO
            'strSQL += " AND IIF(CATA_FPDC_Codigo = " & MEDIO_PAGO_TRANSFERENCIA & ",0,1) = 1"
            'strSQL += " AND IIF(((VEH.Codigo IS NULL) OR (VEH.Codigo IS NOT NULL AND VEH.CATA_TIDV_Codigo = " & TIPO_DUENO_VEHICULO_TERCERO & ")),1,0) = 1"

            dtsCompEgre = objGeneral.Retorna_Dataset(strSQL)

            If dtsCompEgre.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                Call Registrar_Proceso_CXP_PSL(TABLA_IMPORTAR_ANTICIPOS_PSL, lonIdenProcPSL)

                Call Insertar_Egresos_Anticipos_PSL(dtsCompEgre, lonIdenProcPSL)

                Call Informar_Proceso_PSL(lonIdenProcPSL)

            End If

        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Interfaz_Egresos_Anticipos: " & ex.Message)
        End Try
    End Sub

    Private Sub Insertar_Egresos_Anticipos_PSL(ByRef dtsCuentasPagar As DataSet, ByVal lonIdenProcPSL As Long)
        Dim objGeneral As New clsGeneral

        Try
            Dim intCon As Integer, strSQL As String, lonRes As Long, lonConsecutivo As Long
            ' Campos tabla GESTRANS
            Dim lonNumeEgre As Long, datFechEgre As Date, lonCodiTerc As Long, dblValoTota As Double, strNumeIdenTene As String
            Dim strnumeIdenCond As String
            Dim strFechEgre As String, strFechaControl As String, lonMediPago As Long, dblValoDeta As Integer, lonNumDocOri As Long
            Dim lonCodCtaCam As Long, strNumeIdenCamb As String, lonTipoAnti As Long, strTipPreAntPSL As String = ""
            Dim lonTipDueVeh As Long, strCodiDaneOrig As String, strCentResp As String, strConsPref As String, strCodiCaja As String

            For intCon = 0 To dtsCuentasPagar.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                lonNumeEgre = dtsCuentasPagar.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero").ToString()
                lonNumDocOri = dtsCuentasPagar.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Documento_Origen").ToString() ' No. Planilla
                datFechEgre = dtsCuentasPagar.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha").ToString()
                lonCodiTerc = dtsCuentasPagar.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("TERC_Codigo_Titular").ToString()
                dblValoTota = dtsCuentasPagar.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Pago_Recaudo_Total").ToString()
                lonMediPago = dtsCuentasPagar.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("CATA_FPDC_Codigo").ToString()
                lonCodCtaCam = dtsCuentasPagar.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("CUBA_Codigo").ToString() ' Cuenta Bancaria / Cambista 
                lonTipDueVeh = IIf(dtsCuentasPagar.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("TIDV_Codigo").ToString() = String.Empty, 0, dtsCuentasPagar.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("TIDV_Codigo").ToString()) ' Tipo Dueño Vehiculo
                strCodiDaneOrig = dtsCuentasPagar.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("CIUD_Codigo_Dane").ToString() ' Codigo Dane Origen
                strCentResp = dtsCuentasPagar.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("OFIC_Codigo_Alterno").ToString() ' Codigo Alterno Oficina Planilla
                strCodiCaja = dtsCuentasPagar.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("CAJ_Codigo_Alterno").ToString() ' Codigo Alterno Caja Egreso Efectivo

                lonTipoAnti = dtsCuentasPagar.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("CATA_DOOR_Codigo").ToString() ' Anticipo/SobreAnticipo 
                If lonTipoAnti = DOCUMENTO_ORIGEN_ANTICIPO Then
                    strTipPreAntPSL = PREFIJO_EGRESO_ANTICIPO_PSL
                ElseIf lonTipoAnti = DOCUMENTO_ORIGEN_SOBREANTICIPO Then
                    strTipPreAntPSL = PREFIJO_EGRESO_SOBREANTICIPO_PSL
                End If

                strNumeIdenTene = Retorna_Identificacion_Tenedor_Planilla(lonNumDocOri).ToString
                ' Formatear Valores
                strFechEgre = "CONVERT(DATE, '" & datFechEgre.Month & "/" & datFechEgre.Day & "/" & datFechEgre.Year & "', 101)" ' 101=MM/DD/YYYY

                If lonTipDueVeh = TIPO_DUENO_VEHICULO_PROPIO And lonTipoAnti = DOCUMENTO_ORIGEN_SOBREANTICIPO And lonMediPago = MEDIO_PAGO_TRANSFERENCIA Then

                    strnumeIdenCond = Retorna_Identificacion_Conductor_Planilla(lonNumDocOri).ToString
                    strConsPref = Retorna_Consecutivo_Prefijo_EQP(lonNumDocOri).ToString
                    strFechaControl = datFechEgre.ToString("dd-MM-yyyy")

                    lonConsecutivo = Insertar_GESTRANS_Documento_Interfaz_Contable_PSL(dtsCuentasPagar.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon), TABLA_IMPORTAR_ANTICIPOS_PSL & "-EGRESO EFECTIVO", lonIdenProcPSL)

                    strSQL = "INSERT INTO CP_ENCDOCPAGOENTRA("
                    strSQL += "edpconseingre, edpcontrol, edpvalorcontr, edpcompania, edpdivision, "
                    strSQL += "edptipocons, edpconsecutivo, edpclasedocum, edptipodocu, edpnumero, "
                    strSQL += "edpproveedor, edpestablecimiento, edpconcdocu, edpmoneda, edpfechexpe, "
                    strSQL += "edpfechrece, edpcentrorespo, edpfechaconta, edpidentificador,"
                    strSQL += "edpprefijo, edptipoprove, edpestabcia, edpactiecon) VALUES("
                    strSQL += lonConsecutivo.ToString & ",'" & strFechaControl & "'," & dblValoTota.ToString & ",'" & COMPANIA_PSL & "','" & DIVISION_PSL & "','"
                    strSQL += TIPO_CONSECUTIVO_EGRESO_TRANSFERENCIA_ANTICIPO_EQP_PSL & "','" & lonNumeEgre.ToString & "','" & CLASE_CONSECUTIVO_EGRESO_CAMBISTA_ANTICIPO_EGRE_CAMBISTA_PSL & "','" & TIPO_CONSECUTIVO_EGRESO_TRANSFERENCIA_ANTICIPO_EQP_PSL & "','" & lonNumDocOri & "','"
                    strSQL += strnumeIdenCond & "','" & ESTABLECIMIENTO_EGRESO_EFECTIVO_ANTICIPO_PSL & "','" & CONCEPTO_DOCUMENTO_EGRESO_TRANSFERENCIA_ANTICIPO_EQP_PSL & "','" & MONEDA_PESOS_PSL & "'," & strFechEgre & ","
                    strSQL += strFechEgre & ",'" & strCentResp & "'," & strFechEgre & ",'" & lonIdenProcPSL.ToString & "','"
                    strSQL += TIPO_PREFIJO_EGRESO_TRANSFERENCIA_ANTICIPO_EQP_PSL & strConsPref & "','" & TIPO_PROVEEDOR_EGRESO_TRANSFERENCIA_ANTICIPO_EQP_PSL & "','" & strCodiDaneOrig & "','" & ESTABLECIMIENTO_EGRESO_TRANSFERENCIA_ANTICIPO_EQP_PSL & "'"
                    strSQL += ")"
                    lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)

                    ' Primer registro tabla detalle con Valor 98.6% del valor total
                    'dblValoDeta = dblValoTota * (98.6 / 100)
                    strSQL = "INSERT INTO CP_DETDOCPAGOENTRA("
                    strSQL += "ddpconseingre, ddpcontrol, ddpcompania, ddptipocons, ddpconsecutivo,"
                    strSQL += "ddpsecuencia, ddpdescripcion, ddpunidaventa, ddpcantidad, ddppreciounitabruto,"
                    strSQL += "ddpconcepcxp"
                    strSQL += ") VALUES("
                    strSQL += (lonConsecutivo.ToString + "1") & ",'" & strFechaControl & "','" & COMPANIA_PSL & "','" & TIPO_CONSECUTIVO_EGRESO_TRANSFERENCIA_ANTICIPO_EQP_PSL & "'," & lonNumeEgre.ToString & ","
                    strSQL += "1, 'ANTICIPO_PLANILLA_" & lonNumDocOri & "','UN', 1, " & dblValoTota.ToString & ",'"
                    strSQL += TIPO_PROVEEDOR_EGRESO_TRANSFERENCIA_ANTICIPO_EQP_PSL & "'"
                    strSQL += ")"
                    lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)

                ElseIf lonTipDueVeh = TIPO_DUENO_VEHICULO_TERCERO Or lonTipDueVeh = VALOR_CERO Then
                    If lonMediPago = MEDIO_PAGO_EFECTIVO Then

                        ' Si la forma de pago es EFECTIVO se inserta un registro en la tabla CP_ENCDOCPAGOENTRA y 2 registros en la tabla Detalle tabla CP_DETDOCPAGOENTRA

                        lonConsecutivo = Insertar_GESTRANS_Documento_Interfaz_Contable_PSL(dtsCuentasPagar.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon), TABLA_IMPORTAR_ANTICIPOS_PSL & "-EGRESO EFECTIVO", lonIdenProcPSL)

                        strSQL = "INSERT INTO CP_ENCDOCPAGOENTRA("
                        strSQL += "edpconseingre, edpcontrol, edpvalorcontr, edpcompania, edpdivision, "
                        strSQL += "edptipocons, edpconsecutivo, edpclasedocum, edptipodocu, edpnumero, "
                        strSQL += "edpproveedor, edpestablecimiento, edpconcdocu, edpmoneda, edpfechexpe, "
                        strSQL += "edpfechrece, edpcentrorespo, edpformapago, edpfechaconta, edptotmonedlocal,"
                        strSQL += "edpIdentificador, edpprefijo, edptipoprove, edpsumibieser) VALUES("
                        strSQL += lonConsecutivo.ToString & ",'" & CONTROL_EGRESO_EFECTIVO_ANTICIPO_PSL & "'," & dblValoTota.ToString & ",'" & COMPANIA_PSL & "','" & DIVISION_PSL & "','"
                        strSQL += TIPO_CONSECUTIVO_EGRESO_EFECTIVO_ANTICIPO_PSL & "','" & lonNumeEgre.ToString & "','" & CLASE_CONSECUTIVO_EGRESO_EFECTIVO_ANTICIPO_PSL & "','" & TIPO_CONSECUTIVO_EGRESO_EFECTIVO_ANTICIPO_PSL & "','" & lonNumDocOri & "','"
                        strSQL += strNumeIdenTene & "','" & ESTABLECIMIENTO_EGRESO_EFECTIVO_ANTICIPO_PSL & "','" & strCodiCaja & "','" & MONEDA_PESOS_PSL & "'," & strFechEgre & ","
                        strSQL += strFechEgre & ",'" & strCentResp & "','" & FORMA_PAGO_EGRESO_EFECTIVO_ANTICIPO_PSL & "'," & strFechEgre & "," & dblValoTota.ToString & ","
                        strSQL += lonIdenProcPSL.ToString & ",'" & strTipPreAntPSL & "','" & TIPO_PROVEEDOR_EGRESO_EFECTIVO_ANTICIPO_PSL & "','" & strNumeIdenTene & "'"
                        strSQL += ")"
                        lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)

                        ' Primer registro tabla detalle con Valor 98.6% del valor total
                        dblValoDeta = dblValoTota * (98.6 / 100)
                        strSQL = "INSERT INTO CP_DETDOCPAGOENTRA("
                        strSQL += "ddpconseingre, ddpcontrol, ddpcompania, ddptipocons, ddpconsecutivo,"
                        strSQL += "ddpsecuencia, ddpunidaventa, ddpcantidad, ddppreciounitabruto"
                        strSQL += ") VALUES("
                        strSQL += (lonConsecutivo.ToString + "1") & ",'" & CONTROL_EGRESO_EFECTIVO_ANTICIPO_PSL & "','" & COMPANIA_PSL & "','" & TIPO_CONSECUTIVO_EGRESO_EFECTIVO_ANTICIPO_PSL & "'," & lonNumeEgre.ToString & ","
                        strSQL += "1, 'UN', 1, " & dblValoDeta.ToString
                        strSQL += ")"
                        lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)

                        ' Segundo registro tabla detalle con Valor 1.4% del valor total
                        dblValoDeta = dblValoTota - dblValoDeta
                        'dblValoDeta = dblValoTota * (1.4 / 100)
                        strSQL = "INSERT INTO CP_DETDOCPAGOENTRA("
                        strSQL += "ddpconseingre, ddpcontrol, ddpcompania, ddptipocons, ddpconsecutivo,"
                        strSQL += "ddpsecuencia, ddpunidaventa, ddpcantidad, ddppreciounitabruto, ddpcuenta"
                        strSQL += ") VALUES("
                        strSQL += (lonConsecutivo.ToString + "2") & ",'" & CONTROL_EGRESO_EFECTIVO_ANTICIPO_PSL & "','" & COMPANIA_PSL & "','" & TIPO_CONSECUTIVO_EGRESO_EFECTIVO_ANTICIPO_PSL & "'," & lonNumeEgre.ToString & ","
                        strSQL += "2, 'UN', 1, " & dblValoDeta.ToString & ",'" & CUENTA_DESCUENTO_EGRESO_EFECTIVO_ANTICIPO_PSL & "'"
                        strSQL += ")"
                        lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)

                    ElseIf lonMediPago = MEDIO_PAGO_CAMBISTA Then
                        ' Si la forma de pago es CAMBISTA se insertan dos registros en la tabla CP_ENCDOCPAGOENTRA y dos registros en la tabla detalle

                        strNumeIdenCamb = Retorna_Identificacion_Cambista_Egreso(lonCodCtaCam)

                        lonConsecutivo = Insertar_GESTRANS_Documento_Interfaz_Contable_PSL(dtsCuentasPagar.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon), TABLA_IMPORTAR_ANTICIPOS_PSL & "-EGRESO CAMBISTA", lonIdenProcPSL)

                        ' Primer registro tabla encabezado con tercero beneficiario
                        strSQL = "INSERT INTO CP_ENCDOCPAGOENTRA("
                        strSQL += "edpconseingre, edpcontrol, edpvalorcontr, edpcompania, edpdivision, "
                        strSQL += "edptipocons, edpconsecutivo, edpclasedocum, edptipodocu, edpnumero, "
                        strSQL += "edpproveedor, edpestablecimiento, edpconcdocu, edpmoneda, edpfechexpe, "
                        strSQL += "edpfechrece, edpcentrorespo, edpformapago, edpfechaconta, edptotmonedlocal,"
                        strSQL += "edpIdentificador, edpprefijo, edptipoprove, edpsumibieser) VALUES("

                        strSQL += lonConsecutivo.ToString & ",'" & CONTROL_EGRESO_EFECTIVO_ANTICIPO_PSL & "'," & dblValoTota.ToString & ",'" & COMPANIA_PSL & "','" & DIVISION_PSL & "','"
                        strSQL += TIPO_CONSECUTIVO_EGRESO_EFECTIVO_ANTICIPO_PSL & "','" & lonNumeEgre.ToString & "','" & CLASE_CONSECUTIVO_EGRESO_CAMBISTA_ANTICIPO_EGRE_EFECTIVO_PSL & "','" & TIPO_CONSECUTIVO_EGRESO_EFECTIVO_ANTICIPO_PSL & "','" & lonNumDocOri.ToString & "','"
                        strSQL += strNumeIdenTene & "','" & ESTABLECIMIENTO_EGRESO_EFECTIVO_ANTICIPO_PSL & "','" & CONCEPTO_DOCUMENTO_EGRESO_CAMBISTA_ANTICIPO_PSL & "','" & MONEDA_PESOS_PSL & "'," & strFechEgre & ","
                        strSQL += strFechEgre & ",'" & strCentResp & "','" & FORMA_PAGO_EGRESO_EFECTIVO_ANTICIPO_PSL & "'," & strFechEgre & "," & dblValoTota.ToString & ","
                        strSQL += lonIdenProcPSL.ToString & ",'" & strTipPreAntPSL & "','" & TIPO_PROVEEDOR_EGRESO_CAMBISTA_ANTICIPO_PSL_PRIMER_REGISTRO & "','" & strNumeIdenTene & "'"
                        strSQL += ")"
                        lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)

                        ' Segundo registro tabla encabezado con tercero cambista
                        strSQL = "INSERT INTO CP_ENCDOCPAGOENTRA("
                        strSQL += "edpconseingre, edpcontrol, edpvalorcontr, edpcompania, edpdivision, "
                        strSQL += "edptipocons, edpconsecutivo, edpclasedocum, edptipodocu, edpnumero, "
                        strSQL += "edpproveedor, edpestablecimiento, edpconcdocu, edpmoneda, edpfechexpe, "
                        strSQL += "edpfechrece, edpcentrorespo, edpformapago, edpfechaconta, edptotmonedlocal,"
                        strSQL += "edpIdentificador, edpprefijo, edptipoprove, edpsumibieser) VALUES("
                        strSQL += lonConsecutivo.ToString & ",'" & CONTROL_EGRESO_CAMBISTA_ANTICIPO_PSL & "'," & dblValoTota.ToString & ",'" & COMPANIA_PSL & "','" & DIVISION_PSL & "','"
                        strSQL += TIPO_CONSECUTIVO_EGRESO_CAMBISTA_ANTICIPO_PSL & "','" & lonNumeEgre.ToString & "','" & CLASE_CONSECUTIVO_EGRESO_CAMBISTA_ANTICIPO_EGRE_CAMBISTA_PSL & "','" & TIPO_CONSECUTIVO_EGRESO_CAMBISTA_ANTICIPO_PSL & "','" & lonNumDocOri.ToString & "','"
                        strSQL += strNumeIdenCamb & "','" & ESTABLECIMIENTO_EGRESO_CAMBISTA_ANTICIPO_PSL & "','" & CONCEPTO_DOCUMENTO_EGRESO_CAMBISTA_ANTICIPO_PSL & "','" & MONEDA_PESOS_PSL & "'," & strFechEgre & ","
                        strSQL += strFechEgre & ",'" & strCentResp & "','" & FORMA_PAGO_EGRESO_CAMBISTA_ANTICIPO_PSL & "'," & strFechEgre & "," & dblValoTota.ToString & ","
                        strSQL += lonIdenProcPSL.ToString & ",'" & strTipPreAntPSL & "','" & TIPO_PROVEEDOR_EGRESO_CAMBISTA_ANTICIPO_PSL_SEGUNDO_REGISTRO & "','" & strNumeIdenTene & "'"
                        strSQL += ")"
                        lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)

                        ' Primer registro tabla detalle
                        strSQL = "INSERT INTO CP_DETDOCPAGOENTRA("
                        strSQL += "ddpconseingre, ddpcontrol, ddpcompania, ddptipocons, ddpconsecutivo,"
                        strSQL += "ddpsecuencia, ddpunidaventa, ddpcantidad, ddppreciounitabruto"
                        strSQL += ") VALUES("
                        strSQL += lonConsecutivo.ToString & "1" & ",'" & CONTROL_EGRESO_EFECTIVO_ANTICIPO_PSL & "','" & COMPANIA_PSL & "','" & TIPO_CONSECUTIVO_EGRESO_EFECTIVO_ANTICIPO_PSL & "'," & lonNumeEgre.ToString & ","
                        strSQL += "1, 'UN', 1, " & dblValoTota.ToString
                        strSQL += ")"
                        lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)

                        ' Segundo registro tabla detalle 
                        'dblValoDeta = dblValoTota * (1.4 / 100)
                        strSQL = "INSERT INTO CP_DETDOCPAGOENTRA("
                        strSQL += "ddpconseingre, ddpcontrol, ddpcompania, ddptipocons, ddpconsecutivo,"
                        strSQL += "ddpsecuencia, ddpunidaventa, ddpcantidad, ddppreciounitabruto"
                        strSQL += ") VALUES("
                        strSQL += lonConsecutivo.ToString & "2" & ",'" & CONTROL_EGRESO_CAMBISTA_ANTICIPO_PSL & "','" & COMPANIA_PSL & "','" & TIPO_CONSECUTIVO_EGRESO_CAMBISTA_ANTICIPO_PSL & "'," & lonNumeEgre.ToString & ","
                        strSQL += "1, 'UN', 1, " & dblValoTota.ToString
                        strSQL += ")"
                        lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)

                    End If
                End If
            Next

        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Interfaz_Egresos_Anticipos_PSL: " & ex.Message)

        End Try
    End Sub

    ' Autor: Jesús Cuadros 
    ' Fecha Creación: 13-AGO-2019
    ' Observaciones Creación: Interfaz Liquidaciones Planilla Terceros
    ' Fecha Modificación: 
    ' Observaciones Modificación:
    Private Sub Proceso_Interfaz_Liquidaciones_Planillas()
        Dim objGeneral As New clsGeneral
        Try
            While (True)
                objGeneral.Guardar_Mensaje_Log("Inicio Interfaz Liquidaciones GESTRANS-PSL")

                Call Interfaz_Liquidaciones_Planillas()

                Thread.Sleep(lonIntervaloMilisegundosEjecucionProceso)
            End While
        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Proceso_Interfaz_Liquidaciones_Planillas: " & ex.Message)
        End Try
    End Sub

    Private Sub Interfaz_Liquidaciones_Planillas()
        Dim objGeneral As New clsGeneral

        Try
            Dim strSQL As String, dtsLiquPlan As DataSet, lonIdenProcPSL As Long

            strSQL = "SELECT TOP 100 ELPD.EMPR_Codigo, ELPD.TIDO_Codigo, ELPD.Numero_Documento AS Documento_Origen, OFIC.Codigo_Alterno AS OFIC_Codigo_Alterno,"
            strSQL += " ELPD.Numero, ELPD.Numero_Documento, CONVERT(VARCHAR(10),ECPD.Fecha_Crea,120) AS Fecha, ELPD.ENPD_Numero, ELPD.ENMC_Numero, ELPD.Valor_Base_Impuestos AS Valor_Flete_Transportador,"
            strSQL += " DLPD.CLPD_Codigo AS Codigo_Concepto, DLPD.ID, DLPD.Observaciones, DLPD.Valor AS Valor_Concepto, CLPD.Nombre AS Nombre_Concepto, CLPD.Operacion AS Operacion_Concepto,"
            strSQL += " CIUD.Codigo_Alterno AS Ciudad_Origen, ENPD.Numero_Documento AS Numero_Planilla, CONVERT(VARCHAR(10),GETDATE(),120) AS Fecha_Dia"
            strSQL += " FROM Encabezado_Liquidacion_Planilla_Despachos As ELPD"
            strSQL += " LEFT JOIN Detalle_Liquidacion_Planilla_Despachos AS DLPD ON ELPD.Numero = DLPD.ELPD_Numero"
            strSQL += " LEFT JOIN Conceptos_Liquidacion_Planilla_Despachos AS CLPD ON DLPD.CLPD_Codigo = CLPD.Codigo,"
            strSQL += " Encabezado_Planilla_Despachos AS ENPD,Encabezado_Cumplido_Planilla_Despachos AS ECPD,"
            strSQL += " Rutas AS RUTA,"
            strSQL += " Ciudades AS CIUD,"
            strSQL += " Oficinas AS OFIC"
            strSQL += " WHERE ELPD.EMPR_Codigo = ENPD.EMPR_Codigo"
            strSQL += " AND ELPD.ENPD_Numero = ENPD.Numero"
            strSQL += " AND ENPD.EMPR_Codigo = RUTA.EMPR_Codigo"
            strSQL += " AND ENPD.RUTA_Codigo = RUTA.Codigo"
            strSQL += " AND ENPD.EMPR_Codigo = ECPD.EMPR_Codigo"
            strSQL += " AND ENPD.Numero = ECPD.ENPD_Numero"
            strSQL += " AND RUTA.EMPR_Codigo = CIUD.EMPR_Codigo"
            strSQL += " AND RUTA.CIUD_Codigo_Origen = CIUD.Codigo"
            strSQL += " AND ENPD.EMPR_Codigo = OFIC.EMPR_Codigo "
            strSQL += " AND ENPD.OFIC_Codigo = OFIC.Codigo"
            strSQL += " AND ELPD.EMPR_Codigo = " & intCodigoEmpresa.ToString
            strSQL += " AND ELPD.Estado = 1"
            strSQL += " AND ELPD.Anulado = 0"
            strSQL += " AND NOT EXISTS (SELECT Numero_Origen FROM Documentos_Interfaz_Contable_PSL"
            strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
            strSQL += " AND TIDO_Codigo_Origen = " & TIDO_LIQUIDACION_PLANILLA_DESPACHO.ToString
            strSQL += " AND Numero_Origen = ELPD.Numero)"

            'strSQL = "SELECT ELPD.EMPR_Codigo, ELPD.TIDO_Codigo, ELPD.Numero_Documento AS Documento_Origen,"
            'strSQL += "ELPD.Numero, ELPD.Numero_Documento, ELPD.Fecha, ELPD.ENPD_Numero, ELPD.ENMC_Numero, ELPD.Valor_Flete_Transportador,"
            'strSQL += "DLPD.CLPD_Codigo AS Codigo_Concepto, DLPD.ID, DLPD.Observaciones, DLPD.Valor AS Valor_Concepto, CLPD.Nombre AS Nombre_Concepto, CLPD.Operacion AS Operacion_Concepto,"
            'strSQL += "CIUD.Codigo_Alterno AS Ciudad_Origen, ENPD.Numero_Documento AS Numero_Planilla"
            'strSQL += " FROM Encabezado_Liquidacion_Planilla_Despachos As ELPD,"
            'strSQL += " Detalle_Liquidacion_Planilla_Despachos As DLPD,"
            'strSQL += " Conceptos_Liquidacion_Planilla_Despachos AS CLPD,"
            'strSQL += " Encabezado_Planilla_Despachos AS ENPD,"
            'strSQL += " Rutas AS RUTA,"
            'strSQL += " Ciudades AS CIUD"
            'strSQL += " WHERE ELPD.EMPR_Codigo = DLPD.EMPR_Codigo "
            'strSQL += " AND ELPD.Numero = DLPD.ELPD_Numero "
            'strSQL += " AND DLPD.EMPR_Codigo = CLPD.EMPR_Codigo"
            'strSQL += " AND DLPD.CLPD_Codigo = CLPD.Codigo"
            'strSQL += " AND ELPD.EMPR_Codigo = ENPD.EMPR_Codigo"
            'strSQL += " AND ELPD.ENPD_Numero = ENPD.Numero"
            'strSQL += " AND ENPD.EMPR_Codigo = RUTA.EMPR_Codigo"
            'strSQL += " AND ENPD.RUTA_Codigo = RUTA.Codigo"
            'strSQL += " AND RUTA.EMPR_Codigo = CIUD.EMPR_Codigo"
            'strSQL += " AND RUTA.CIUD_Codigo_Origen = CIUD.Codigo"
            'strSQL += " AND ELPD.EMPR_Codigo = " & intCodigoEmpresa.ToString
            'strSQL += " AND ELPD.Estado = 1"
            'strSQL += " AND ELPD.Anulado = 0"
            'strSQL += " AND DLPD.Valor > 0"
            'strSQL += " AND NOT EXISTS (SELECT Numero_Origen FROM Documentos_Interfaz_Contable_PSL"
            'strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
            'strSQL += " AND TIDO_Codigo_Origen = " & TIDO_LIQUIDACION_PLANILLA_DESPACHO.ToString
            'strSQL += " AND Numero_Origen = ELPD.Numero)"

            'strSQL += " AND ENPD.Numero_Documento = 1000413" ' PRUEBAS No Planilla

            dtsLiquPlan = objGeneral.Retorna_Dataset(strSQL)

            If dtsLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                Call Registrar_Proceso_CXP_PSL(TABLA_IMPORTAR_LIQUIDACIONES_PSL, lonIdenProcPSL)

                Call Insertar_Liquidaciones_Planillas_PSL(dtsLiquPlan, lonIdenProcPSL)

                Call Informar_Proceso_PSL(lonIdenProcPSL)

            End If

        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Interfaz_Liquidaciones_Planillas: " & ex.Message)
        End Try
    End Sub

    Private Sub Insertar_Liquidaciones_Planillas_PSL(ByRef dtsLiquPlan As DataSet, ByVal lonIdenProcPSL As Long)
        Dim objGeneral As New clsGeneral

        Try
            Dim intCon As Integer, strSQL As String, lonRes As Long, lonConsecutivo As Long, intNumeSecu As Integer = 1
            Dim strFechaControl As String, strControl As String, dblValoConcSuma As Double = 0, dblValoConcRest As Double = 0
            ' Campos tabla GESTRANS
            Dim datFechLiqu As Date, dblValoFlet As Double, dblValFleEnc As Double, strNumeIden As String, lonNumeMani As Long
            Dim strFecha As String, dblValoConc As Double, lonNumeLiqu As Long, lonNumeDocu As Long, lonNumLiqAnt As Long = 0
            Dim lonCodiConc As Long, strNombConc As String, strOperConc As String, strTipoConsLiqu As String, strOrigen As String
            Dim lonNumePlan As Long, strvaloConc As String, strCodiConc As String, strCentResp As String, datFechAct As Date
            Dim strFechaAct As String

            For intCon = 0 To dtsLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                lonNumeLiqu = dtsLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero").ToString()
                lonNumeDocu = dtsLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString() ' No. Liquidacion
                lonNumeMani = dtsLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("ENMC_Numero").ToString() ' No. Manifiesto
                datFechLiqu = dtsLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha").ToString()
                datFechAct = dtsLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha_Dia").ToString()
                dblValoFlet = dtsLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Flete_Transportador").ToString()
                dblValoConc = IIf(dtsLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Concepto").ToString() = "", 0, dtsLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Concepto").ToString())
                strValoConc = IIf(dblValoConc = 0, "", dblValoConc.ToString)
                lonCodiConc = IIf(dtsLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Codigo_Concepto").ToString() = "", 0, dtsLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Codigo_Concepto").ToString())
                strCodiConc = IIf(lonCodiConc = 0, "", lonCodiConc.ToString)
                strNombConc = IIf(dtsLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Nombre_Concepto").ToString() = "", "", dtsLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Nombre_Concepto").ToString())
                strOperConc = IIf(dtsLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Operacion_Concepto").ToString() = "", "", dtsLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Operacion_Concepto").ToString())
                strOrigen = dtsLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Ciudad_Origen").ToString()
                strCentResp = dtsLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("OFIC_Codigo_Alterno").ToString() ' Codigo Alterno Oficina Planilla
                lonNumePlan = dtsLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Planilla")

                ' Formatear Valores
                strFechaControl = datFechLiqu.Day & "-" & datFechLiqu.Month & "-" & datFechLiqu.Year
                strFecha = "CONVERT(DATE, '" & datFechLiqu.Month & "/" & datFechLiqu.Day & "/" & datFechLiqu.Year & "', 101)" ' 101=MM/DD/YYYY
                strFechaAct = "CONVERT(DATE, '" & datFechAct.Month & "/" & datFechAct.Day & "/" & datFechAct.Year & "', 101)" ' 101=MM/DD/YYYY

                ' Tipo de Consecutivo PSL depende si el concepto + o -
                If strOperConc = OPERACION_SUMA_CONCEPTO_LIQUIDACION Then
                    strTipoConsLiqu = TIPO_CONSECUTIVO_LIQUIDACION_SUMA_PSL
                Else
                    strTipoConsLiqu = TIPO_CONSECUTIVO_LIQUIDACION_RESTA_PSL
                End If

                strNumeIden = Retorna_Identificacion_Tenedor_Planilla(lonNumePlan)
                strControl = "LIQUIDACION"

                ' Se inserta 1 Encabezado con Flete + Conceptos Suma, 1 Encabezado con Conceptos Resta Diferentes a Anticipo-SobreAnticipo y 1 Detalle con Flete
                If lonNumeLiqu <> lonNumLiqAnt Then
                    intNumeSecu = 1

                    lonRes = Retorna_Suma_Conceptos_Liquidacion(lonNumeLiqu, dblValoConcSuma, dblValoConcRest)

                    ' Registrar en Tabla Control GESTRANS
                    lonConsecutivo = Insertar_GESTRANS_Documento_Interfaz_Contable_PSL(dtsLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon), TABLA_IMPORTAR_LIQUIDACIONES_PSL & "-LIQUIDACION PLANILLA", lonIdenProcPSL)

                    ' Primer registro tabla encabezado con Valor Flete Transportador + Conceptos que Suman
                    dblValFleEnc = dblValoFlet + dblValoConcSuma

                    strSQL = "INSERT INTO CP_ENCDOCPAGOENTRA("
                    strSQL += "edpconseingre, edpcontrol, edpvalorcontr, edpcompania, edpdivision, "
                    strSQL += "edptipocons, edpconsecutivo, edpclasedocum, edptipodocu, edpnumero, "
                    strSQL += "edpproveedor, edpestablecimiento, edpconcdocu, edpmoneda, edpfechexpe, "
                    strSQL += "edpfechrece, edpcentrorespo, edpfechaconta, edpformapago,"
                    strSQL += "edpIdentificador, edpprefijo, edptipoprove, edpestabcia"
                    strSQL += ") VALUES( "
                    strSQL += lonConsecutivo.ToString & ",'" & strControl & "'," & dblValFleEnc.ToString & ",'" & COMPANIA_PSL & "','" & DIVISION_PSL & "','"
                    strSQL += TIPO_CONSECUTIVO_LIQUIDACION_SUMA_PSL & "','" & lonConsecutivo.ToString & "','" & CLASE_CONSECUTIVO_LIQUIDACION_FLETE_PSL & "','" & TIPO_CONSECUTIVO_LIQUIDACION_SUMA_PSL & "','" & lonNumePlan.ToString & "','"
                    strSQL += strNumeIden & "','" & ESTABLECIMIENTO_LIQUIDACION_PSL & "','" & CONCEPTO_LIQUIDACION_FLETE_PSL & "','" & MONEDA_PESOS_PSL & "'," & strFecha & ","
                    strSQL += strFecha & ",'" & strCentResp & "'," & strFechaAct & ",'" & FORMA_PAGO_LIQUIDACION & "',"
                    strSQL += lonIdenProcPSL.ToString & ",'" & PREFIJO_LIQUIDACION_PSL & "','" & TIPO_PROVEEDOR_LIQUIDACION_PSL & "','" & strOrigen & "'"
                    strSQL += ")"
                    lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)

                    ' Segundo registro tabla encabezado con Conceptos que Restan (NC) sin Anticipos-SobreAnticipos
                    If dblValoConcRest > 0 Then

                        strSQL = "INSERT INTO CP_ENCDOCPAGOENTRA("
                        strSQL += "edpconseingre, edpcontrol, edpvalorcontr, edpcompania, edpdivision, "
                        strSQL += "edptipocons, edpconsecutivo, edpclasedocum, edptipodocu, edpnumero, "
                        strSQL += "edpproveedor, edpestablecimiento, edpconcdocu, edpmoneda, edpfechexpe, "
                        strSQL += "edpfechrece, edpcentrorespo, edpfechaconta,edpformapago,"
                        strSQL += "edpIdentificador, edpprefijo, edptipoprove, edpestabcia"
                        strSQL += ") VALUES("
                        strSQL += lonConsecutivo.ToString + intNumeSecu.ToString & ",'" & strControl & "'," & dblValoConcRest.ToString & ",'" & COMPANIA_PSL & "','" & DIVISION_PSL & "','"
                        strSQL += TIPO_CONSECUTIVO_LIQUIDACION_RESTA_PSL & "','" & lonConsecutivo.ToString & "','" & CLASE_CONSECUTIVO_LIQUIDACION_NC_PSL & "','" & TIPO_CONSECUTIVO_LIQUIDACION_RESTA_PSL & "','" & lonNumePlan.ToString & "','"
                        strSQL += strNumeIden & "','" & ESTABLECIMIENTO_LIQUIDACION_PSL & "','" & CONCEPTO_LIQUIDACION_NC_PSL & "','" & MONEDA_PESOS_PSL & "'," & strFecha & ","
                        strSQL += strFecha & ",'" & strCentResp & "'," & strFechaAct & ",'" & FORMA_PAGO_LIQUIDACION & "',"
                        strSQL += lonIdenProcPSL.ToString & ",'" & PREFIJO_LIQUIDACION_NC_PSL & "','" & TIPO_PROVEEDOR_LIQUIDACION_PSL & "','" & strOrigen & "'"
                        strSQL += ")"
                        lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)

                    End If

                    ' Se inserta un registro en la tabla detalle con Flete Transportador
                    strSQL = "INSERT INTO CP_DETDOCPAGOENTRA("
                    strSQL += "ddpconseingre, ddpcontrol, ddpcompania, ddptipocons, ddpconsecutivo,"
                    strSQL += "ddpsecuencia, ddpunidaventa, ddpcantidad, ddppreciounitabruto, ddpdescripcion"
                    strSQL += ") VALUES("
                    strSQL += lonConsecutivo.ToString + intNumeSecu.ToString & ",'" & strControl & "','" & COMPANIA_PSL & "','" & TIPO_CONSECUTIVO_LIQUIDACION_SUMA_PSL & "'," & lonConsecutivo.ToString.ToString & ","
                    strSQL += intNumeSecu.ToString & ", 'UN', 1, " & dblValoFlet.ToString & ",'FLETE'"
                    strSQL += ")"
                    lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)

                    ' Dependiendo del tipo de concepto que venga en la primera línea del detalle SQL se inserta un registro en el detalle
                    If strOperConc = OPERACION_SUMA_CONCEPTO_LIQUIDACION Then

                        intNumeSecu += 1

                        strSQL = "INSERT INTO CP_DETDOCPAGOENTRA("
                        strSQL += "ddpconseingre, ddpcontrol, ddpcompania, ddptipocons, ddpconsecutivo,"
                        strSQL += "ddpsecuencia, ddpunidaventa, ddpcantidad, ddppreciounitabruto, ddpdescripcion"
                        strSQL += ") VALUES("
                        strSQL += lonConsecutivo.ToString + intNumeSecu.ToString & ",'" & strControl & "','" & COMPANIA_PSL & "','" & TIPO_CONSECUTIVO_LIQUIDACION_SUMA_PSL & "'," & lonConsecutivo.ToString.ToString & ","
                        strSQL += intNumeSecu.ToString & ", 'UN', 1, '" & strvaloConc.ToString & "','FLETE'"
                        strSQL += ")"
                        lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)

                    ElseIf lonCodiConc <> CONCEPTO_LIQUIDACION_ANTICIPO And lonCodiConc <> CONCEPTO_LIQUIDACION_SOBREANTICIPO And dblValoConc <> 0 And lonCodiConc <> CONCEPTO_LIQUIDACION_POLIZA And lonCodiConc <> CONCEPTO_LIQUIDACION_CRUCE_ANTICIPO Then
                        ' Se insertan solo los detalles diferentes a ANTICIPO, SOBREANTICIPO, POLIZA Y CRUCE ANTICIPOS

                        intNumeSecu += 1

                        strSQL = "INSERT INTO CP_DETDOCPAGOENTRA("
                        strSQL += "ddpconseingre, ddpcontrol, ddpcompania, ddptipocons, ddpconsecutivo,"
                        strSQL += "ddpsecuencia, ddpunidaventa, ddpcantidad, ddppreciounitabruto, ddpdescripcion"
                        strSQL += ") VALUES("
                        strSQL += lonConsecutivo.ToString + intNumeSecu.ToString & ",'" & strControl & "','" & COMPANIA_PSL & "','" & TIPO_CONSECUTIVO_LIQUIDACION_RESTA_PSL & "'," & lonConsecutivo.ToString.ToString & ","
                        strSQL += intNumeSecu.ToString & ", 'UN', 1, '" & strvaloConc.ToString & "','FLETE'"
                        strSQL += ")"
                        lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)
                    End If

                ElseIf lonCodiConc <> CONCEPTO_LIQUIDACION_ANTICIPO And lonCodiConc <> CONCEPTO_LIQUIDACION_SOBREANTICIPO And dblValoConc <> 0 Then
                    ' Se insertan solo los detalles diferentes a ANTICIPO y SOBREANTICIPO
                    strSQL = "INSERT INTO CP_DETDOCPAGOENTRA("
                    strSQL += "ddpconseingre, ddpcontrol, ddpcompania, ddptipocons, ddpconsecutivo,"
                    strSQL += "ddpsecuencia, ddpunidaventa, ddpcantidad, ddppreciounitabruto, ddpdescripcion"
                    strSQL += ") VALUES("
                    strSQL += lonConsecutivo.ToString + intNumeSecu.ToString & ",'" & strControl & "','" & COMPANIA_PSL & "','" & strTipoConsLiqu & "'," & lonConsecutivo.ToString & ","
                    strSQL += intNumeSecu.ToString & ", 'UN', 1, '" & strvaloConc.ToString & "','" & strNombConc & "'"
                    strSQL += ")"
                    lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)

                End If

                intNumeSecu += 1
                lonNumLiqAnt = lonNumeLiqu

            Next

        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Interfaz_Liquidaciones_Planillas_PSL: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesús Cuadros 
    ' Fecha Creación: 13-AGO-2019
    ' Observaciones Creación: Interfaz Liquidaciones Planilla Terceros
    ' Fecha Modificación: 
    ' Observaciones Modificación:
    Private Sub Proceso_Interfaz_Cruce_Anticipos_Liquidaciones_Planillas()
        Dim objGeneral As New clsGeneral
        Try
            While (True)
                objGeneral.Guardar_Mensaje_Log("Inicio Cruce Anticipos Interfaz Liquidaciones GESTRANS-PSL")

                Call Interfaz_Cruce_Anticipos_Liquidaciones_Planillas()

                Thread.Sleep(lonIntervaloMilisegundosEjecucionProceso)
            End While
        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Proceso_Interfaz_Cruce_Anticipos_Liquidaciones_Planillas: " & ex.Message)
        End Try
    End Sub

    Private Sub Interfaz_Cruce_Anticipos_Liquidaciones_Planillas()
        Dim objGeneral As New clsGeneral

        Try
            Dim strSQL As String, dtsLiquPlan As DataSet, lonIdenProcPSL As Long

            strSQL = " SELECT TOP 100 ELPD.EMPR_Codigo, ELPD.TIDO_Codigo, ELPD.Numero_Documento AS Documento_Origen,"
            strSQL += " ELPD.Numero, ELPD.Numero_Documento, ELPD.Fecha, ELPD.ENPD_Numero, ELPD.ENMC_Numero, ELPD.Valor_Flete_Transportador,"
            strSQL += " ENPD.Numero_Documento AS Numero_Planilla"
            strSQL += " FROM Encabezado_Liquidacion_Planilla_Despachos As ELPD,"
            strSQL += " Encabezado_Planilla_Despachos AS ENPD"
            strSQL += " WHERE ELPD.EMPR_Codigo = ENPD.EMPR_Codigo"
            strSQL += " AND ELPD.ENPD_Numero = ENPD.Numero"
            strSQL += " AND ELPD.EMPR_Codigo = " & intCodigoEmpresa.ToString
            strSQL += " AND ELPD.Estado = 1"
            strSQL += " AND ELPD.Anulado = 0"
            strSQL += " AND EXISTS (SELECT Numero_Origen FROM Documentos_Interfaz_Contable_PSL"
            strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
            strSQL += " AND TIDO_Codigo_Origen = " & TIDO_LIQUIDACION_PLANILLA_DESPACHO.ToString
            strSQL += " AND Numero_Origen = ELPD.Numero"
            strSQL += " AND ISNULL(Cruce_Anticipos_PSL,0) = 0"
            strSQL += " )"

            dtsLiquPlan = objGeneral.Retorna_Dataset(strSQL)

            If dtsLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                Call Registrar_Proceso_PSL(TABLA_IMPORTAR_CRUCE_ANTICIPOS_PSL, lonIdenProcPSL)

                Call Insertar_Cruce_Anticipos_Liquidaciones_Planillas_PSL(dtsLiquPlan, lonIdenProcPSL)

                Call Informar_Proceso_PSL(lonIdenProcPSL)

            End If

        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Interfaz_Cruce_Anticipos_Liquidaciones_Planillas: " & ex.Message)
        End Try
    End Sub

    Private Sub Insertar_Cruce_Anticipos_Liquidaciones_Planillas_PSL(ByRef dtsLiquPlan As DataSet, ByVal lonIdenProcPSL As Long)
        Dim objGeneral As New clsGeneral

        Try
            Dim intCon As Integer, intCont As Integer, strSQL As String, lonRes As Long, lonConsecutivo As Long, intNumeSecu As Integer = 1
            Dim strFechaControl As String, strControl As String, dblValoConcSuma As Double = 0, dblValoConcRest As Double = 0
            Dim dtsLiquPSL As DataSet, strTipCon As String, dtsConLiq As DataSet
            ' Campos tabla GESTRANS
            Dim datFechLiqu As Date, strConsec As String, strNumeIden As String, strPrefijo As String, lonValor As Long
            Dim strFecha As String, lonNumeLiqu As Long, lonNumeDocu As Long, lonNumLiqAnt As Long = 0
            Dim lonNumePlan As Long, strConsLiqu As String

            'lonConsecutivo = 0

            For intCon = 0 To dtsLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                lonNumeLiqu = dtsLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero").ToString()
                lonNumeDocu = dtsLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString() ' No. Liquidacion
                lonNumePlan = dtsLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Planilla")
                datFechLiqu = dtsLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha").ToString()
                'lonNumeInte = dtsLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("ELPD_Numero") 'Numero Documento_Interfaz_Contable_PSL
                lonConsecutivo = lonNumeDocu

                strNumeIden = Retorna_Identificacion_Tenedor_Planilla(lonNumePlan)

                ' Formatear Valores
                strFechaControl = datFechLiqu.Day & "-" & datFechLiqu.Month & "-" & datFechLiqu.Year
                strFecha = "CONVERT(DATE, '" & datFechLiqu.Month & "/" & datFechLiqu.Day & "/" & datFechLiqu.Year & "', 101)" ' 101=MM/DD/YYYY

                strNumeIden = Retorna_Identificacion_Tenedor_Planilla(lonNumePlan)
                strControl = "CRUCE_ANT"

                'lonConsecutivo = lonConsecutivo + 1

                strSQL = "SELECT edpprefijo, edpnumero, edpsalddocupeso, edpsignoprove, edptipocons, edpconsecutivo"
                strSQL += " FROM cp_encdocpago "
                strSQL += " WHERE edpclasedocum <> 'SACO' "
                strSQL += " AND edpnumero = '" & lonNumePlan.ToString & "'"
                strSQL += " AND edpproveedor = '" & strNumeIden & "'"

                dtsLiquPSL = objGeneral.Retorna_Dataset_PSL(strSQL)
                If dtsLiquPSL.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                    For intCont = 0 To dtsLiquPSL.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1
                        'Excluir ANTT y Sobreanticipos
                        strTipCon = dtsLiquPSL.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont).Item("edptipocons").ToString
                        strPrefijo = dtsLiquPSL.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont).Item("edpprefijo").ToString
                        strConsec = dtsLiquPSL.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont).Item("edpconsecutivo").ToString
                        lonValor = dtsLiquPSL.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont).Item("edpsalddocupeso")

                        If strTipCon <> TIPO_CONSECUTIVO_LIQUIDACION_SUMA_PSL Then
                            lonConsecutivo = lonConsecutivo + 1
                            'Insertar Unico Encabezado 
                            strSQL = "INSERT INTO CP_IMPOCOMPAPLIENCAB("
                            strSQL += "capcontrol, captipodocu, capconsecutivo, capcompania,"
                            strSQL += "capdivision, capproveedor, capfecha, capfechaconta, capmoneda,"
                            strSQL += "captasacamb, capidentificador"
                            strSQL += ") VALUES("
                            strSQL += "'" & strControl & "','" & CONCEPTO_DOCUMENTO_CRUCE_ANTICIPOS_PSL & "','" & lonConsecutivo.ToString & "','" & COMPANIA_PSL & "','"
                            strSQL += CONCEPTO_DIVISION_CRUCE_ANTICIPOS_PSL & "', '" & strNumeIden & "'," & strFecha & "," & strFecha & ",'" & MONEDA_PESOS_PSL & "','"
                            strSQL += CONCEPTO_TASA_CRUCE_ANTICIPO_PSL & "'," & lonIdenProcPSL.ToString
                            strSQL += ")"
                            lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)
                            'Inserta Primer Detalle
                            strSQL = "INSERT INTO CP_IMPOCOMPAPLIDETA("
                            strSQL += "mdpcontrol, mdptipdoccompago, mdpconcompago, "
                            strSQL += "mdpcompania, mdptipcondocapl, mdpconsdocapl,"
                            strSQL += "mdpprefdocapl, mdpnumedocapl, mdpvalor"
                            strSQL += ") VALUES("
                            strSQL += "'" & strControl & "','" & CONCEPTO_DOCUMENTO_CRUCE_ANTICIPOS_PSL & "','" & lonConsecutivo.ToString & "','"
                            strSQL += COMPANIA_PSL & "', '" & strTipCon & "', '" & strConsec & "', '"
                            strSQL += strPrefijo & "','" & lonNumePlan.ToString & "', " & lonValor
                            strSQL += ")"
                            lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)

                            strSQL = "SELECT edpconsecutivo"
                            strSQL += " FROM cp_encdocpago "
                            strSQL += " WHERE edpclasedocum <> 'SACO' "
                            strSQL += " AND edpnumero = '" & lonNumePlan.ToString & "'"
                            strSQL += " AND edpproveedor = '" & strNumeIden & "'"
                            strSQL += " AND edptipocons = '" & TIPO_CONSECUTIVO_LIQUIDACION_SUMA_PSL & "'"

                            dtsConLiq = objGeneral.Retorna_Dataset_PSL(strSQL)

                            If dtsConLiq.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                                strConsLiqu = dtsConLiq.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("edpconsecutivo").ToString
                            Else
                                strConsLiqu = strConsec
                            End If

                            'Inserta Segundo Detalle
                            strSQL = "INSERT INTO CP_IMPOCOMPAPLIDETA("
                            strSQL += "mdpcontrol, mdptipdoccompago, mdpconcompago, "
                            strSQL += "mdpcompania, mdptipcondocapl, mdpconsdocapl,"
                            strSQL += "mdpprefdocapl, mdpnumedocapl, mdpvalor"
                            strSQL += ") VALUES("
                            strSQL += "'" & strControl & "','" & CONCEPTO_DOCUMENTO_CRUCE_ANTICIPOS_PSL & "','" & lonConsecutivo.ToString & "','"
                            strSQL += COMPANIA_PSL & "', '" & TIPO_CONSECUTIVO_LIQUIDACION_SUMA_PSL & "', '" & strConsLiqu & "', '"
                            strSQL += TIPO_CONSECUTIVO_CRUCE_ANTICIPO_PSL & "','" & lonNumePlan.ToString & "', " & lonValor
                            strSQL += ")"
                            lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)

                        End If
                    Next
                End If

                'Actualiza Registro Cruce Anticipos Gestrans
                strSQL = "UPDATE Documentos_Interfaz_Contable_PSL "
                strSQL += " SET Cruce_anticipos_PSL = 1, Fecha_Cruce_Anticipos_PSL = GETDATE() "
                strSQL += " WHERE TIDO_Codigo_Origen = " & TIDO_LIQUIDACION_PLANILLA_DESPACHO.ToString
                strSQL += " AND Numero_Origen = " & lonNumeLiqu.ToString
                lonRes = objGeneral.Ejecutar_SQL(strSQL)

            Next

        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Interfaz_Cruce_Anticipos_Liquidaciones_Planillas: " & ex.Message)
        End Try
    End Sub


    ' Autor: Jesús Cuadros 
    ' Fecha Creación: 14-AGO-2019
    ' Observaciones Creación: Interfaz Facturas Transporte
    ' Fecha Modificación: 
    ' Observaciones Modificación:
    Private Sub Proceso_Interfaz_Facturas_Transporte()
        Dim objGeneral As New clsGeneral
        Try
            While (True)
                objGeneral.Guardar_Mensaje_Log("Inicio Interfaz Facturas GESTRANS-PSL")

                Call Interfaz_Facturas_Transporte()

                Thread.Sleep(lonIntervaloMilisegundosEjecucionProceso)
            End While
        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Proceso_Interfaz_Facturas_Transporte: " & ex.Message)
        End Try
    End Sub

    Private Sub Interfaz_Facturas_Transporte()
        Dim objGeneral As New clsGeneral

        Try
            Dim strSQL As String, dtsFactTran As DataSet, lonIdenProcPSL As Long

            strSQL = "SELECT ENFA.EMPR_Codigo, ENFA.TIDO_Codigo, ENFA.Numero, ENFA.Numero_Documento, ENFA.Fecha, ENFA.Numero_Documento AS Documento_Origen,"
            strSQL += " ENFA.Valor_Remesas AS Valor_Factura, DERF.ENRE_Numero, DERF.ENOS_Numero,"
            strSQL += " ENRE.Numero AS Numero_Remesa, ENRE.ENMC_Numero As Numero_Manifiesto, ENRE.Numero_Documento AS Numero_Documento_Remesa, ENRE.Peso_Cliente, ENRE.Total_Flete_Cliente,"
            strSQL += " TERC.Numero_Identificacion, TERC.Nombre + TERC.Razon_Social AS Nombre_Cliente, ENRE.Fecha_Crea AS Fecha_Remesa, ENPD.Numero_Documento AS Numero_Planilla,"
            strSQL += " CASE WHEN CURE.Peso_Cargue < CURE.Peso_Descargue THEN CURE.Peso_Cargue ELSE CURE.Peso_Descargue END AS Peso_Cumplido"
            strSQL += " FROM Encabezado_Facturas As ENFA, Detalle_Remesas_Facturas As DERF, Encabezado_Remesas As ENRE, Terceros As TERC,"
            strSQL += " Cumplido_Remesas AS CURE, Encabezado_Planilla_Despachos AS ENPD "
            strSQL += " WHERE ENFA.EMPR_Codigo = DERF.EMPR_Codigo"
            strSQL += " And ENFA.Numero = DERF.ENFA_Numero"
            strSQL += " And DERF.EMPR_Codigo = ENRE.EMPR_Codigo"
            strSQL += " And DERF.ENRE_Numero = ENRE.Numero"
            strSQL += " AND ENRE.EMPR_Codigo = CURE.EMPR_Codigo"
            strSQL += " AND ENRE.Numero = CURE.ENRE_Numero"
            strSQL += " And ENFA.EMPR_Codigo = TERC.EMPR_Codigo"
            strSQL += " And ENFA.TERC_Facturar = TERC.Codigo"
            strSQL += " AND ENRE.EMPR_Codigo = ENPD.EMPR_Codigo"
            strSQL += " AND ENRE.ENPD_Numero = ENPD.Numero"
            strSQL += " And ENFA.EMPR_Codigo = " & intCodigoEmpresa.ToString
            strSQL += " And ENFA.Estado = 1"
            strSQL += " And ENFA.Anulado = 0"
            strSQL += " AND NOT EXISTS (SELECT Numero_Origen FROM Documentos_Interfaz_Contable_PSL"
            strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
            strSQL += " AND TIDO_Codigo_Origen = " & TIDO_FACTURA_VENTA.ToString
            strSQL += " AND Numero_Origen = ENFA.Numero)"
            strSQL += " ORDER BY ENFA.Numero ASC"

            dtsFactTran = objGeneral.Retorna_Dataset(strSQL)

            If dtsFactTran.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                Call Registrar_Proceso_CXC_PSL(TABLA_IMPORTAR_FACTURAS_PSL, lonIdenProcPSL)

                Call Insertar_Facturas_Transporte_PSL(dtsFactTran, lonIdenProcPSL)

                Call Informar_Proceso_PSL(lonIdenProcPSL)

            End If

        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Interfaz_Facturas_Transporte: " & ex.Message)
        End Try
    End Sub

    Private Sub Insertar_Facturas_Transporte_PSL(ByRef dtsFactTran As DataSet, ByVal lonIdenProcPSL As Long)
        Dim objGeneral As New clsGeneral

        Try
            Dim intCon As Integer, strSQL As String, lonRes As Long, lonConsecutivo As Long, intNumeSecu As Integer = 1
            Dim strFechaControl As String, strBSPSL As String, strComentarioPSL As String, strControl As String
            ' Campos tabla GESTRANS
            Dim datFechFact As Date, strNumeIden As String, lonNumeMani As Long, lonNumeReme As Long, dblPesoReme As Double
            Dim strPlaca As String = "", strRuta As String = "", strProdTran As String = "", lonNumDocRem As Long, strSedePSL As String = ""
            Dim strDaneOrig As String = "", strDaneDest As String = ""
            Dim strFecha As String, dblValoFact As Double, dblValoReme As Double, lonNumeFact As Long, lonNumeDocu As Long, lonNumFacAnt As Long = 0, strSede As String
            Dim datFechReme As Date, lonPLanilla As Long

            intNumeSecu = 1

            For intCon = 0 To dtsFactTran.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                lonNumeFact = dtsFactTran.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero").ToString()
                lonNumeDocu = dtsFactTran.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString() ' No. Factura
                lonNumeMani = dtsFactTran.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Manifiesto").ToString() ' No. Manifiesto
                lonNumeReme = dtsFactTran.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Remesa").ToString() ' No. Interno Remesa
                lonNumDocRem = dtsFactTran.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento_Remesa").ToString() ' No. Documento Remesa
                datFechFact = dtsFactTran.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha").ToString()
                dblValoFact = dtsFactTran.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Factura").ToString()
                dblValoReme = dtsFactTran.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Total_Flete_Cliente").ToString()
                strNumeIden = dtsFactTran.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Identificacion").ToString()
                dblPesoReme = dtsFactTran.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Peso_Cliente").ToString()
                datFechReme = dtsFactTran.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha_Remesa").ToString()
                lonPLanilla = dtsFactTran.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Planilla").ToString()

                lonRes = Retorna_Informacion_Remesa(lonNumeReme, strPlaca, strRuta, strProdTran, strSedePSL, strDaneOrig, strDaneDest)

                ' Formatear Valores
                strFechaControl = datFechFact.Day & "-" & datFechFact.Month & "-" & datFechFact.Year
                strFecha = "CONVERT(DATE, '" & datFechFact.Month & "/" & datFechFact.Day & "/" & datFechFact.Year & "', 101)" ' 101=MM/DD/YYYY
                strBSPSL = lonPLanilla.ToString ' Manifiesto FALTA No. Planilla
                strComentarioPSL = datFechReme.ToString("dd'/'MM'/'yyyy") & "&" & strPlaca & "&" & lonNumDocRem.ToString & "&" & strDaneOrig & "&" & strDaneDest & "&" & dblPesoReme.ToString  'Fecha, Placa, Remesa, DANE Origen, DANE Destino, Peso
                strControl = "FACTURA"

                ' Control para insertar 1 Encabezado y 1 Detalle más los siguientes detalles que corresponden a las Remesas
                ' No se van a facturar Otros Conceptos porque esto lo hacen en PSL
                If lonNumeFact <> lonNumFacAnt Then

                    ' Registrar en Tabla Control GESTRANS
                    lonConsecutivo = Insertar_GESTRANS_Documento_Interfaz_Contable_PSL(dtsFactTran.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon), TABLA_IMPORTAR_FACTURAS_PSL & "-FACTURA TRANSPORTE", lonIdenProcPSL)

                    ' FALTA: Manejo Sede Cliente 
                    strSede = "01"

                    strSQL = "INSERT INTO CA_ENCDOCVTAENTRA("
                    strSQL += "edvconseingre, edvcontrol, edvvalobruto, edvcompania, edvdivision, "
                    strSQL += "edvtipoconsclie, edvnumedocuclie, edvclasdocuclie, edvtipodocuclie,"
                    strSQL += "edvcliente, edvestabclien, edvconcepto, edvmoneda, edvfechexpe,"
                    strSQL += "edvfechaconta, edvfechcomp, edvIdentificador, edvfechinteobra, edvcomencorto"
                    strSQL += ") VALUES("
                    strSQL += lonConsecutivo.ToString & ",'" & strControl & "'," & dblValoFact.ToString & ",'" & COMPANIA_PSL & "','" & DIVISION_PSL & "','"
                    strSQL += TIPO_CONSECUTIVO_FACTURACION_PSL & "','" & lonConsecutivo.ToString & "','" & CLASE_CONSECUTIVO_FACTURACION_PSL & "','" & TIPO_CONSECUTIVO_FACTURACION_PSL & "','"
                    strSQL += strNumeIden & "','" & strSedePSL & "','" & CONCEPTO_FACTURACION_PSL & "','" & MONEDA_PESOS_PSL & "'," & strFecha & ","
                    strSQL += strFecha & "," & strFecha & "," & lonIdenProcPSL.ToString & "," & strFecha & ",'" & lonNumeDocu.ToString & "'"
                    strSQL += ")"
                    lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)

                    ' Inserta primera Remesa en tabla detalle
                    strSQL = "INSERT INTO CA_DETDOCVTAENTRA("
                    strSQL += " ddvconseingre, ddvcontrol, ddvcompania, ddvtipocons, ddvnumedocu, ddvconseregis,"
                    strSQL += " ddvunidaventa, ddvcanfacunivta, ddvprecunitbrut, ddvdescripcion,"
                    strSQL += " ddvidenbienserv, ddvComentario "
                    strSQL += ") VALUES("
                    strSQL += intNumeSecu.ToString & ",'" & strControl & "','" & COMPANIA_PSL & "','" & TIPO_CONSECUTIVO_FACTURACION_PSL & "'," & lonConsecutivo.ToString & "," & intNumeSecu.ToString & ","
                    strSQL += "'UN', 1, " & dblValoReme.ToString & ",'" & strProdTran & "',"
                    strSQL += "'" & strBSPSL & "','" & strComentarioPSL & "'"
                    strSQL += ")"
                    lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)

                Else
                    ' Registro tabla detalle con Remesas
                    strSQL = "INSERT INTO CA_DETDOCVTAENTRA("
                    strSQL += " ddvconseingre, ddvcontrol, ddvcompania, ddvtipocons, ddvnumedocu, ddvconseregis,"
                    strSQL += " ddvunidaventa, ddvcanfacunivta, ddvprecunitbrut, ddvdescripcion,"
                    strSQL += " ddvidenbienserv, ddvComentario "
                    strSQL += ") VALUES("
                    strSQL += intNumeSecu.ToString & ",'" & strControl & "','" & COMPANIA_PSL & "','" & TIPO_CONSECUTIVO_FACTURACION_PSL & "'," & lonConsecutivo.ToString & "," & intNumeSecu.ToString & ","
                    strSQL += "'UN', 1, " & dblValoReme.ToString & ",'" & strProdTran & "',"
                    strSQL += "'" & strBSPSL & "','" & strComentarioPSL & "'"
                    strSQL += ")"
                    lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)

                End If

                intNumeSecu += 1
                lonNumFacAnt = lonNumeFact

            Next

        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Interfaz_Facturas_Transporte_PSL: " & ex.Message)

        End Try
    End Sub

    ' Autor: Jesús Cuadros 
    ' Fecha Creación: 16-AGO-2019
    ' Observaciones Creación: Interfaz Legalización Gastos
    ' Fecha Modificación: 
    ' Observaciones Modificación:
    Private Sub Proceso_Interfaz_Legalizacion_Gastos()
        Dim objGeneral As New clsGeneral
        Try
            While (True)
                objGeneral.Guardar_Mensaje_Log("Inicio Interfaz Legalización Gastos GESTRANS-PSL")

                Call Interfaz_Legalizacion_Gastos()

                Thread.Sleep(lonIntervaloMilisegundosEjecucionProceso)
            End While
        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Proceso_Interfaz_Legalizacion_Gastos: " & ex.Message)

        End Try
    End Sub


    Private Sub Interfaz_Legalizacion_Gastos()
        Dim objGeneral As New clsGeneral

        Try
            Dim strSQL As String, dtsLegaGast As DataSet, lonIdenProcPSL As Long

            strSQL = "SELECT ELGC.EMPR_Codigo, ELGC.TIDO_Codigo, ELGC.Fecha, ELGC.TERC_Codigo_Conductor, ELGC.Valor_Gastos As VALOR_TOTAL_GASTOS,"
            strSQL += " ELGC.Valor_Anticipos, ELGC.Valor_Reanticipos, CLGC.Nombre As NOMBRE_CONCEPTO, PLUC.Codigo_Cuenta As CUENTA_PUC, DLGC.Valor As VALOR_GASTO, "
            strSQL += " DLGC.Observaciones, TERC.Numero_Identificacion AS IDENTIFICACION_CONDUCTOR, ELGC.Numero, ELGC.Numero_Documento, ENPD.Numero_Documento AS NUMERO_PLANILLA,"
            strSQL += " ELGC.Numero_Documento AS Documento_Origen, CLGC.Codigo AS CODIGO_CONCEPTO, ELGC.Valor_Anticipos AS VALOR_ANTICIPOS, ELGC.Saldo_Favor_Empresa AS SALDO_FAVOR_EMPRESA,"
            strSQL += " VEHI.Codigo_Alterno AS VEHI_CERE, DLGC.Identificacion_Proveedor AS TERC_PROV, ELGC.Saldo_Favor_Conductor AS Saldo_Favor_Conductor, CONVERT(VARCHAR(10),ELGC.Fecha,103) AS Fecha_Legalizacion"
            strSQL += " FROM Encabezado_Legalizacion_Gastos_Conductor As ELGC, Detalle_Legalizacion_Gastos_Conductor As DLGC,"
            strSQL += " Conceptos_Legalizacion_Gastos_Conductor As CLGC, Plan_Unico_Cuentas As PLUC, Terceros AS TERC, Encabezado_Planilla_Despachos AS ENPD,"
            strSQL += " Vehiculos AS VEHI"
            strSQL += " WHERE ELGC.EMPR_Codigo = DLGC.EMPR_Codigo"
            strSQL += " AND ELGC.Numero = DLGC.ELGC_Numero"
            strSQL += " AND ELGC.EMPR_Codigo = TERC.EMPR_Codigo"
            strSQL += " AND ELGC.TERC_Codigo_Conductor = TERC.Codigo"
            strSQL += " AND DLGC.EMPR_Codigo = CLGC.EMPR_Codigo"
            strSQL += " AND DLGC.CLGC_Codigo = CLGC.Codigo"
            strSQL += " AND CLGC.EMPR_Codigo = PLUC.EMPR_Codigo"
            strSQL += " AND CLGC.PLUC_Codigo = PLUC.Codigo"
            strSQL += " AND DLGC.EMPR_Codigo = ENPD.EMPR_Codigo"
            strSQL += " AND DLGC.ENPD_Numero = ENPD.Numero"
            strSQL += " AND ENPD.EMPR_Codigo = VEHI.EMPR_Codigo"
            strSQL += " AND ENPD.VEHI_Codigo = VEHI.Codigo"
            strSQL += " AND ELGC.EMPR_Codigo = " & intCodigoEmpresa.ToString
            strSQL += " AND ELGC.Estado = 1"
            strSQL += " AND ELGC.Anulado = 0"
            strSQL += " AND NOT EXISTS (SELECT Numero_Origen FROM Documentos_Interfaz_Contable_PSL"
            strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
            strSQL += " AND TIDO_Codigo_Origen = " & TIDO_LEGALIZACION_GASTOS.ToString
            strSQL += " AND Numero_Origen = ELGC.Numero)"

            dtsLegaGast = objGeneral.Retorna_Dataset(strSQL)

            If dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                Call Registrar_Proceso_PSL(TABLA_IMPORTAR_LEGALIZACION_GASTOS_PSL, lonIdenProcPSL)

                Call Insertar_Legalizacion_Gastos(dtsLegaGast, lonIdenProcPSL)

                Call Informar_Proceso_PSL(lonIdenProcPSL)

            End If

        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Interfaz_Legalizacion_Gastos: " & ex.Message)

        End Try
    End Sub

    Private Sub Insertar_Legalizacion_Gastos(ByRef dtsLegaGast As DataSet, ByVal lonIdenProcPSL As Long)
        Dim objGeneral As New clsGeneral

        Try
            Dim intCon As Integer, strSQL As String, lonRes As Long, lonConsecutivo As Long, intNumeSecu As Integer = 1
            Dim strFecha As String, intPeriodo As Integer, intAno As Integer, intOperacion As Integer = 1, strFechOrig As String
            ' Campos tabla GESTRANS
            Dim datFecha As Date, strNumIdeCon As String, lonNumero As Long, lonNumeDocu As Long, lonNumePlan As Long
            Dim dblValTotGas As Double, dblValoGast As Double, strCuePUC As String, strNombGast As String, lonNumeAnte As Long = 0
            Dim lonCodiConc As Long, dblValorAnticipos As Double, dblValPenLeg As Double, strCentResp As String, strNumIdePro As String
            Dim lonValSalCon As Long, dblCtaFavEmp As Double, dblCtaFavCon As Double, strFechaLega As String, dblCtaAnticipo As Double

            For intCon = 0 To dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                lonNumero = dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero").ToString()
                lonNumeDocu = dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString() ' No. Legalizacion
                lonNumePlan = dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("NUMERO_PLANILLA").ToString() ' No. Planilla
                datFecha = dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha").ToString()
                dblValTotGas = dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("VALOR_TOTAL_GASTOS").ToString()
                dblValoGast = dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("VALOR_GASTO").ToString()
                strCuePUC = dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("CUENTA_PUC").ToString()
                strNombGast = dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("NOMBRE_CONCEPTO").ToString()
                strNumIdeCon = dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("IDENTIFICACION_CONDUCTOR").ToString()
                lonCodiConc = dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("CODIGO_CONCEPTO").ToString()
                dblValorAnticipos = dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("VALOR_REANTICIPOS").ToString()
                dblValPenLeg = dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("SALDO_FAVOR_EMPRESA").ToString() ' Valor pendiente legalizar
                strCentResp = dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("VEHI_CERE").ToString() ' Centro de Responsabilidad del Vehiculo
                strNumIdePro = dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("TERC_PROV").ToString() ' Identificacion Proveedor Gasto
                lonValSalCon = dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Saldo_Favor_Conductor").ToString() ' Saldo Favor Conductor
                strFechaLega = dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha_Legalizacion").ToString() 'EQP
                dblCtaFavEmp = ConfigurationSettings.AppSettings.Get("CuentaFavorEmpresa")
                dblCtaFavCon = ConfigurationSettings.AppSettings.Get("CuentaFavorConductor")
                dblCtaAnticipo = ConfigurationSettings.AppSettings.Get("CuentaAnticipos")

                ' Formatear Valores
                strFechOrig = datFecha.Day & "-" & datFecha.Month & "-" & datFecha.Year
                strFecha = "CONVERT(DATE, '" & datFecha.Month & "/" & datFecha.Day & "/" & datFecha.Year & "', 101)" ' 101=MM/DD/YYYY
                intPeriodo = datFecha.Month
                intAno = datFecha.Year

                ' Para los conceptos la intOperacion = 1

                If lonNumero <> lonNumeAnte Then

                    ' Registrar en Tabla Control GESTRANS
                    lonConsecutivo = Insertar_GESTRANS_Documento_Interfaz_Contable_PSL(dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon), TABLA_IMPORTAR_LEGALIZACION_GASTOS_PSL & "-LEGALIZACION GASTOS", lonIdenProcPSL)
                    intNumeSecu = 1

                    ' Inserta Un Encabezado para cada Legalizacion 
                    strSQL = "INSERT INTO CO_MOVIMENTRA (mencomprobante, menfuente, menlote, mendivision, menseculine,"
                    strSQL += " menperiodo, menano, mencompania, menfechcont, menfechtran,"
                    strSQL += " menoperacion, mencuenta, mencodielem1, menvalomoneloca,"
                    strSQL += " mencodimoneextr, menvalomoneextr, mentasacamb, menvalobasereteiva, mendiasdife,"
                    strSQL += " menorigen, mentipomone, menajusinfl, mencantidad, mencomentario,"
                    strSQL += " menidentificador"
                    strSQL += ") VALUES("
                    strSQL += lonConsecutivo.ToString & ",'" & FUENTE_LEGALIZACION_GASTOS_PSL & "','0','" & DIVISION_PSL & "'," & intNumeSecu.ToString & ","
                    strSQL += intPeriodo.ToString & "," & intAno.ToString & ",'" & COMPANIA_PSL & "','" & strFechaLega & "','" & strFechaLega & "',"
                    strSQL += VALOR_NULO_SQL_PSL & "," & VALOR_NULO_SQL_PSL & ",''," & VALOR_NULO_SQL_PSL & ","
                    strSQL += VALOR_NULO_SQL_PSL & "," & VALOR_NULO_SQL_PSL & ", 0, 0, 0,"
                    strSQL += "'" & lonNumeDocu.ToString & "','" & TIPO_MONEDA_LEGALIZACION_GASTOS_PSL & "'," & VALOR_NULO_SQL_PSL & ",0," & VALOR_NULO_SQL_PSL & ","
                    strSQL += lonIdenProcPSL.ToString
                    strSQL += ")"

                    lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)

                    If dblValorAnticipos > 0 Then
                        ' ANTICIPO intOperacion = 2 porque la operación 2=Débito
                        intOperacion = 2
                        intNumeSecu += 1

                        strSQL = "INSERT INTO CO_MOVIMENTRA (mencomprobante, menfuente, menlote, mendivision, menseculine,"
                        strSQL += " menperiodo, menano, mencompania, menfechcont, menfechtran,"
                        strSQL += " menoperacion, mencuenta, mencodielem1, menvalomoneloca,"
                        strSQL += " mencodimoneextr, menvalomoneextr, mentasacamb, menvalobasereteiva, mendiasdife,"
                        strSQL += " menorigen, mentipomone, menajusinfl, mencantidad, mencomentario,"
                        strSQL += " menidentificador, mencodielem3, mencodielem4"
                        strSQL += ") VALUES("
                        strSQL += lonConsecutivo.ToString & ",'" & FUENTE_LEGALIZACION_GASTOS_PSL & "','0','" & DIVISION_PSL & "'," & intNumeSecu.ToString & ","
                        strSQL += intPeriodo.ToString & "," & intAno.ToString & ",'" & COMPANIA_PSL & "','" & strFechaLega & "','" & strFechaLega & "',"
                        strSQL += intOperacion.ToString & ",'" & dblCtaAnticipo & "',''," & dblValorAnticipos.ToString & ","
                        strSQL += "'" & MONEDA_PESOS_PSL & "'," & dblValorAnticipos & ", 0, 0, 0,"
                        strSQL += "'" & lonNumeDocu.ToString & "','" & TIPO_MONEDA_LEGALIZACION_GASTOS_PSL & "','" & AJUSTE_INFLACION_LEGALIZACION_GASTOS_PSL & "',0,'" & GASTO_CRUCE_ANTICIPO & "',"
                        strSQL += lonIdenProcPSL.ToString & ",'" & strNumIdeCon.ToString & "', '" & lonNumePlan.ToString & "'"
                        strSQL += ")"

                        lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)

                    End If

                    If dblValPenLeg > 0 Then

                        ' SALDO PENDIENTE LEGALIZAR
                        intOperacion = 1
                        intNumeSecu += 1

                        strSQL = "INSERT INTO CO_MOVIMENTRA (mencomprobante, menfuente, menlote, mendivision, menseculine,"
                        strSQL += " menperiodo, menano, mencompania, menfechcont, menfechtran,"
                        strSQL += " menoperacion, mencuenta, mencodielem1, menvalomoneloca,"
                        strSQL += " mencodimoneextr, menvalomoneextr, mentasacamb, menvalobasereteiva, mendiasdife,"
                        strSQL += " menorigen, mentipomone, menajusinfl, mencantidad, mencomentario,"
                        strSQL += " menidentificador, mencodielem3, mencodielem4"
                        strSQL += ") VALUES("
                        strSQL += lonConsecutivo.ToString & ",'" & FUENTE_LEGALIZACION_GASTOS_PSL & "','0','" & DIVISION_PSL & "'," & intNumeSecu.ToString & ","
                        strSQL += intPeriodo.ToString & "," & intAno.ToString & ",'" & COMPANIA_PSL & "','" & strFechaLega & "','" & strFechaLega & "',"
                        strSQL += intOperacion.ToString & ",'" & dblCtaFavEmp & "',''," & dblValPenLeg.ToString & ","
                        strSQL += "'" & MONEDA_PESOS_PSL & "'," & dblValPenLeg & ", 0, 0, 0,"
                        strSQL += "'" & lonNumeDocu.ToString & "','" & TIPO_MONEDA_LEGALIZACION_GASTOS_PSL & "','" & AJUSTE_INFLACION_LEGALIZACION_GASTOS_PSL & "',0,'" & GASTO_SALDO_FAVOR_EMPRESA & "',"
                        strSQL += lonIdenProcPSL.ToString & ",'" & strNumIdeCon.ToString & "', '" & lonNumePlan.ToString & "'"
                        strSQL += ")"

                        lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)

                    End If

                    If lonValSalCon > 0 Then

                        ' SALDO PENDIENTE LEGALIZAR FAVOR CONDUCTOR
                        intOperacion = 2
                        intNumeSecu += 1

                        strSQL = "INSERT INTO CO_MOVIMENTRA (mencomprobante, menfuente, menlote, mendivision, menseculine,"
                        strSQL += " menperiodo, menano, mencompania, menfechcont, menfechtran,"
                        strSQL += " menoperacion, mencuenta, mencodielem1, menvalomoneloca,"
                        strSQL += " mencodimoneextr, menvalomoneextr, mentasacamb, menvalobasereteiva, mendiasdife,"
                        strSQL += " menorigen, mentipomone, menajusinfl, mencantidad, mencomentario,"
                        strSQL += " menidentificador, mencodielem3, mencodielem4"
                        strSQL += ") VALUES("
                        strSQL += lonConsecutivo.ToString & ",'" & FUENTE_LEGALIZACION_GASTOS_PSL & "','0','" & DIVISION_PSL & "'," & intNumeSecu.ToString & ","
                        strSQL += intPeriodo.ToString & "," & intAno.ToString & ",'" & COMPANIA_PSL & "','" & strFechaLega & "','" & strFechaLega & "',"
                        strSQL += intOperacion.ToString & ",'" & dblCtaFavCon & "',''," & lonValSalCon.ToString & ","
                        strSQL += "'" & MONEDA_PESOS_PSL & "'," & lonValSalCon & ", 0, 0, 0,"
                        strSQL += "'" & lonNumeDocu.ToString & "','" & TIPO_MONEDA_LEGALIZACION_GASTOS_PSL & "','" & AJUSTE_INFLACION_LEGALIZACION_GASTOS_PSL & "',0,'" & GASTO_SALDO_FAVOR_CONDUCTOR & "',"
                        strSQL += lonIdenProcPSL.ToString & ",'" & strNumIdeCon.ToString & "', '" & lonNumePlan.ToString & "'"
                        strSQL += ")"

                        lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)

                    End If

                End If
                intNumeSecu += 1
                intOperacion = 1

                strSQL = "INSERT INTO CO_MOVIMENTRA (mencomprobante, menfuente, menlote, mendivision, menseculine,"
                strSQL += " menperiodo, menano, mencompania, menfechcont, menfechtran,"
                strSQL += " menoperacion, mencuenta, mencodielem1, menvalomoneloca,"
                strSQL += " mencodimoneextr, menvalomoneextr, mentasacamb, menvalobasereteiva, mendiasdife,"
                strSQL += " menorigen, mentipomone, menajusinfl, mencantidad, mencomentario,"
                strSQL += " menidentificador, mencodielem3"
                strSQL += ") VALUES("
                strSQL += lonConsecutivo.ToString & ",'" & FUENTE_LEGALIZACION_GASTOS_PSL & "','0','" & DIVISION_PSL & "'," & intNumeSecu.ToString & ","
                strSQL += intPeriodo.ToString & "," & intAno.ToString & ",'" & COMPANIA_PSL & "','" & strFechaLega & "','" & strFechaLega & "',"
                strSQL += intOperacion.ToString & ",'" & strCuePUC & "','" & strCentResp & "'," & dblValoGast.ToString & ","
                strSQL += "'" & MONEDA_PESOS_PSL & "'," & dblValoGast & ", 0, 0, 0,"
                strSQL += "'" & lonNumeDocu.ToString & "','" & TIPO_MONEDA_LEGALIZACION_GASTOS_PSL & "','" & AJUSTE_INFLACION_LEGALIZACION_GASTOS_PSL & "',0,'" & strNombGast & "',"
                strSQL += lonIdenProcPSL.ToString & ",'" & strNumIdePro.ToString & "'"
                strSQL += ")"

                lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)

                lonNumeAnte = lonNumero

            Next

        Catch ex As Exception

        End Try
    End Sub

    ' Autor: Jesús Cuadros 
    ' Fecha Creación: 13-AGO-2019
    ' Observaciones Creación: Interfaz Polizas Planilla Terceros
    ' Fecha Modificación: 
    ' Observaciones Modificación:
    Private Sub Proceso_Interfaz_Polizas_Planillas()
        Dim objGeneral As New clsGeneral
        Try
            While (True)
                objGeneral.Guardar_Mensaje_Log("Inicio Interfaz Polizas Planillas GESTRANS-PSL")

                Call Interfaz_Polizas_Planillas()

                Thread.Sleep(lonIntervaloMilisegundosEjecucionProceso)
            End While
        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Proceso_Interfaz_Polizas_Planillas: " & ex.Message)
        End Try
    End Sub

    Private Sub Interfaz_Polizas_Planillas()
        Dim objGeneral As New clsGeneral

        Try
            Dim strSQL As String, dtsPlanDesa As DataSet, lonIdenProcPSL As Long

            strSQL = "SELECT TOP 100 ENPD.Fecha,ENPD.EMPR_Codigo, ENPD.TIDO_Codigo, ENPD.Valor_Seguro_Poliza,ENPD.Numero_Documento AS Documento_Origen , ENPD.Numero, CIU.Codigo_Alterno AS Ciudad_Origen,"
            strSQL += " OFI.Codigo_Alterno AS OFIC_Codigo_Alterno"
            strSQL += " FROM Encabezado_Planilla_Despachos AS ENPD,"
            strSQL += " Rutas AS RUT, Ciudades AS CIU, Oficinas AS OFI,"
            strSQL += " Vehiculos AS VEH"
            strSQL += " WHERE ENPD.EMPR_Codigo = RUT.EMPR_Codigo"
            strSQL += " AND ENPD.RUTA_Codigo = RUT.Codigo "
            strSQL += " AND ENPD.EMPR_Codigo = VEH.EMPR_Codigo"
            strSQL += " AND ENPD.VEHI_Codigo = VEH.Codigo  "
            strSQL += " AND RUT.EMPR_Codigo = CIU.EMPR_Codigo"
            strSQL += " AND RUT.CIUD_Codigo_Origen = CIU.Codigo"
            strSQL += " AND ENPD.EMPR_Codigo = OFI.EMPR_Codigo"
            strSQL += " AND ENPD.OFIC_Codigo = OFI.Codigo"
            strSQL += " AND ENPD.EMPR_Codigo = " & intCodigoEmpresa.ToString
            strSQL += " AND ENPD.Valor_Seguro_Poliza > 0"
            strSQL += " AND ENPD.Estado = 1"
            strSQL += " AND ENPD.Anulado = 0"
            strSQL += " AND NOT EXISTS (SELECT Numero_Origen FROM Documentos_Interfaz_Contable_PSL"
            strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
            strSQL += " AND TIDO_Codigo_Origen = " & TIDO_PLANILLA_DESPACHO.ToString
            strSQL += " AND Numero_Origen = ENPD.Numero)"
            strSQL += " AND VEH.CATA_TIDV_Codigo = " & TIPO_DUENO_VEHICULO_TERCERO

            dtsPlanDesa = objGeneral.Retorna_Dataset(strSQL)

            If dtsPlanDesa.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                Call Registrar_Proceso_CXP_PSL(TABLA_IMPORTAR_LIQUIDACIONES_PSL, lonIdenProcPSL)

                Call Insertar_Polizas_Planillas_PSL(dtsPlanDesa, lonIdenProcPSL)

                Call Informar_Proceso_PSL(lonIdenProcPSL)

            End If

        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Interfaz_Polizas_Planillas: " & ex.Message)
        End Try
    End Sub

    Private Sub Insertar_Polizas_Planillas_PSL(ByRef dtsPlanDesa As DataSet, ByVal lonIdenProcPSL As Long)
        Dim objGeneral As New clsGeneral

        Try
            Dim intCon As Integer, strSQL As String, lonRes As Long, lonConsecutivo As Long, intNumeSecu As Integer = 1
            Dim strFechaControl As String, strControl As String, dblValoConcSuma As Double = 0, dblValoConcRest As Double = 0
            ' Campos tabla GESTRANS
            Dim strNumeIden As String, lonNumeDocuPlan As Long, strCentResp As String
            Dim strFecha As String, lonNumePlan As Long, lonNumLiqAnt As Long = 0, strOrigen As String
            Dim strValoPoli As String, datFechPlan As Date, dblValoBase As Double, dblDifeValo As Double
            'Dim lonNumePlan As Long

            For intCon = 0 To dtsPlanDesa.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                lonNumePlan = dtsPlanDesa.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero").ToString() ' No. Planilla
                lonNumeDocuPlan = dtsPlanDesa.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Documento_Origen").ToString() ' No. Documento Planilla
                datFechPlan = dtsPlanDesa.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha").ToString()
                strOrigen = dtsPlanDesa.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Ciudad_Origen").ToString()
                strValoPoli = dtsPlanDesa.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Seguro_Poliza")
                strCentResp = dtsPlanDesa.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("OFIC_Codigo_Alterno").ToString() ' Codigo Alterno Oficina Planilla
                dblValoBase = ConfigurationSettings.AppSettings.Get("ValorBasePoliza")
                dblDifeValo = CDbl(Val(strValoPoli)) - dblValoBase

                lonConsecutivo = Insertar_GESTRANS_Documento_Interfaz_Contable_PSL(dtsPlanDesa.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon), TABLA_IMPORTAR_LIQUIDACIONES_PSL & "-POLIZAS PLANILLA", lonIdenProcPSL)

                ' Formatear Valores
                strFecha = "CONVERT(DATE, '" & datFechPlan.Month & "/" & datFechPlan.Day & "/" & datFechPlan.Year & "', 101)" ' 101=MM/DD/YYYY

                strNumeIden = Retorna_Identificacion_Tenedor_Planilla(lonNumeDocuPlan)
                strControl = "POLIZA"
                strFechaControl = datFechPlan.ToString("dd-MM-yyyy")

                ' Se inserta un encabezado con el valor de la poliza 
                strSQL = "INSERT INTO CP_ENCDOCPAGOENTRA("
                strSQL += "edpconseingre, edpcontrol, edpvalorcontr, edpcompania, edpdivision, "
                strSQL += "edptipocons, edpconsecutivo, edpclasedocum, edptipodocu, edpnumero, "
                strSQL += "edpproveedor, edpestablecimiento, edpconcdocu, edpmoneda, edpfechexpe, "
                strSQL += "edpfechrece, edpcentrorespo, edpfechaconta, edpidentificador,"
                strSQL += "edpprefijo, edptipoprove, edpestabcia, edpactiecon"
                strSQL += ") VALUES( "
                strSQL += lonConsecutivo.ToString & ",'" & strControl & "'," & strValoPoli.ToString & ",'" & COMPANIA_PSL & "','" & DIVISION_PSL & "','"
                strSQL += TIPO_CONSECUTIVO_POLIZA_PSL & "','" & lonConsecutivo.ToString & "','" & CLASE_CONSECUTIVO_POLIZA_NC_PSL & "','" & TIPO_CONSECUTIVO_POLIZA_PSL & "','" & lonNumeDocuPlan.ToString & "','"
                strSQL += strNumeIden & "','" & ESTABLECIMIENTO_LIQUIDACION_PSL & "','" & CONCEPTO_DOCUMENTO_POLIZA_PSL & "','" & MONEDA_PESOS_PSL & "'," & strFecha & ","
                strSQL += strFecha & ",'" & strCentResp & "'," & strFecha & ",'" & lonIdenProcPSL.ToString & "','"
                strSQL += PREFIJO_POLIZA_PSL & "','" & TIPO_PROVEEDOR_POLIZA_PSL & "','" & strOrigen & "','" & ACTIVIDAD_ECONOMICA_POLIZA_PSL & "'"
                strSQL += ")"
                lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)

                ' Se inserta primer registro en la tabla detalle con Valor Base Poliza ($9520)
                strSQL = "INSERT INTO CP_DETDOCPAGOENTRA("
                strSQL += "ddpconseingre, ddpcontrol, ddpcompania, ddptipocons, ddpconsecutivo,"
                strSQL += "ddpsecuencia, ddpunidaventa, ddpcantidad, ddppreciounitabruto, ddpdescripcion,"
                strSQL += "ddpconcepcxp"
                strSQL += ") VALUES("
                strSQL += lonConsecutivo.ToString & "1,'" & strControl & "','" & COMPANIA_PSL & "','" & TIPO_CONSECUTIVO_POLIZA_PSL & "'," & lonConsecutivo.ToString & ","
                strSQL += "1, 'UN', 1, " & dblValoBase.ToString & ",'PROVISION MC PLANILLA " & lonNumeDocuPlan.ToString & "','"
                strSQL += CONCEPTO_DETALLE_POLIZA_PSL_PRIMER_REGISTRO & "'"
                strSQL += ")"
                lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)

                ' Se inserta Segundo registro en la tabla detalle con con la diferencia entre el valor poliza y el valor base
                strSQL = "INSERT INTO CP_DETDOCPAGOENTRA("
                strSQL += "ddpconseingre, ddpcontrol, ddpcompania, ddptipocons, ddpconsecutivo,"
                strSQL += "ddpsecuencia, ddpunidaventa, ddpcantidad, ddppreciounitabruto, ddpdescripcion,"
                strSQL += "ddpconcepcxp"
                strSQL += ") VALUES("
                strSQL += lonConsecutivo.ToString & "2,'" & strControl & "','" & COMPANIA_PSL & "','" & TIPO_CONSECUTIVO_POLIZA_PSL & "'," & lonConsecutivo.ToString & ","
                strSQL += "2, 'UN', 1, " & dblDifeValo.ToString & ",'INGRESO MC PLANILLA " & lonNumeDocuPlan.ToString & "','"
                strSQL += CONCEPTO_DETALLE_POLIZA_PSL_SEGUNDO_REGISTRO & "'"
                strSQL += ")"
                lonRes = objGeneral.Ejecutar_SQL_PSL(strSQL)

            Next

        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Interfaz_Polizas_Planillas_PSL: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesús Cuadros 
    ' Fecha Creación: 30-AGO-2019
    ' Observaciones Creación: Consulta en PSL el Estado de las Solicitudes de Anticipo, deben encontrarse en PAGADO o ANULADO
    ' Fecha Modificación: 
    ' Observaciones Modificación:
    Private Sub Proceso_Interfaz_Consulta_Estado_Solicitud_Anticipos_PSL()
        Dim objGeneral As New clsGeneral
        Try
            While (True)
                objGeneral.Guardar_Mensaje_Log("Inicio Proceso Consulta PSL Estado Solicitud Anticipos")

                Call Consultar_Estado_Solicitud_Anticipos_PSL()

                Thread.Sleep(lonIntervaloMilisegundosEjecucionProceso)
            End While
        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Proceso_Interfaz_Consulta_Estado_Solicitud_Anticipos_PSL: " & ex.Message)
        End Try
    End Sub

    Private Sub Consultar_Estado_Solicitud_Anticipos_PSL()
        Dim objGeneral As New clsGeneral

        Try
            Dim strSQL As String, dtsSoliAnti As DataSet, lonNumePlan As Long, strEstado As String = ""

            strSQL = "SELECT * FROM Documentos_Interfaz_Contable_PSL"
            strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
            strSQL += " AND TIDO_Codigo_Origen = " & TIDO_CUENTA_X_PAGAR
            strSQL += " AND ISNULL(Proceso_PSL,0) = 0" ' Pendientes de procesar por PSL

            dtsSoliAnti = objGeneral.Retorna_Dataset(strSQL)

            For intCon = 0 To dtsSoliAnti.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                lonNumePlan = dtsSoliAnti.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento_Origen").ToString()

                If Consultar_Solicitud_Anticipo_PSL(lonNumePlan, strEstado) Then

                    Call Actualizar_Estado_Solicitud_Anticipo(lonNumePlan, strEstado)

                End If

            Next

        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Consultar_Estado_Solicitud_Anticipos_PSL: " & ex.Message)

        End Try
    End Sub

#Region "Funciones GESTRANS"
    Private Sub Actualizar_Estado_Solicitud_Anticipo(ByVal lonNumePlan As Long, ByVal strEstado As String)
        Dim objGeneral As New clsGeneral
        Dim bolRespuesta As Boolean, strSQL As String

        Try
            strSQL = "UPDATE Documentos_Interfaz_Contable_PSL SET Proceso_PSL = 1,"
            strSQL += " Estado_Proceso_PSL = '" & strEstado & "'"
            strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
            strSQL += " AND TIDO_Codigo_Origen = " & TIDO_CUENTA_X_PAGAR
            strSQL += " AND Numero_Documento_Origen = " & lonNumePlan.ToString

            bolRespuesta = objGeneral.Ejecutar_SQL(strSQL)

        Catch ex As Exception
            bolRespuesta = False
            objGeneral.Guardar_Mensaje_Log("Actualizar_Estado_Solicitud_Anticipo, No. Planilla: " & lonNumePlan & " - " & ex.Message)

        End Try
    End Sub

    Private Function Retorna_Suma_Conceptos_Liquidacion(ByVal lonNumeLiqu As Long, ByRef dblValoPosi As Double, ByRef dblValoNega As Double) As Boolean
        Dim objGeneral As New clsGeneral

        Try
            Dim strSQL As String, dtsLiquidacion As DataSet, intCon As Integer, intOperacion As Integer

            strSQL = "SELECT DLPD.CLPD_Codigo, DLPD.Valor, CLPD.Operacion"
            strSQL += " FROM Detalle_Liquidacion_Planilla_Despachos AS DLPD, Conceptos_Liquidacion_Planilla_Despachos AS CLPD"
            strSQL += " WHERE DLPD.EMPR_Codigo = CLPD.EMPR_Codigo"
            strSQL += " AND DLPD.CLPD_Codigo = CLPD.Codigo"
            strSQL += " AND DLPD.EMPR_Codigo = " & intCodigoEmpresa.ToString
            strSQL += " AND DLPD.ELPD_Numero =" & lonNumeLiqu.ToString
            strSQL += " AND DLPD.Valor > 0"
            strSQL += " AND CLPD.Codigo <> " & CONCEPTO_LIQUIDACION_ANTICIPO
            strSQL += " AND CLPD.Codigo <> " & CONCEPTO_LIQUIDACION_SOBREANTICIPO

            dtsLiquidacion = objGeneral.Retorna_Dataset(strSQL)

            dblValoPosi = 0 : dblValoNega = 0

            For intCon = 0 To dtsLiquidacion.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                intOperacion = dtsLiquidacion.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Operacion").ToString()
                If intOperacion = OPERACION_SUMA_CONCEPTO_LIQUIDACION Then
                    dblValoPosi += dtsLiquidacion.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor")
                ElseIf intOperacion = OPERACION_RESTA_CONCEPTO_LIQUIDACION Then
                    dblValoNega += dtsLiquidacion.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor")
                End If

            Next
            Retorna_Suma_Conceptos_Liquidacion = True

        Catch ex As Exception
            Retorna_Suma_Conceptos_Liquidacion = False
            objGeneral.Guardar_Mensaje_Log("Retorna_Suma_Conceptos_Liquidacion: " & ex.Message)

        End Try


    End Function

    Private Function Retorna_Identificacion_Tenedor_Planilla(ByVal lonNumePlan As Long) As String
        Dim objGeneral As New clsGeneral

        Try
            Dim strSQL As String, dtsIdentificacionTenedor As DataSet

            strSQL = "SELECT TERC.Numero_Identificacion AS Identificacion_Tenedor "
            strSQL += " FROM Encabezado_Planilla_Despachos AS ENPD,"
            strSQL += " Terceros AS TERC"
            strSQL += " WHERE ENPD.EMPR_Codigo = TERC.EMPR_Codigo"
            strSQL += " And ENPD.TERC_Codigo_Tenedor = TERC.Codigo"
            strSQL += " And ENPD.EMPR_Codigo = " & intCodigoEmpresa.ToString
            strSQL += " And ENPD.Numero_Documento = " & lonNumePlan.ToString

            dtsIdentificacionTenedor = objGeneral.Retorna_Dataset(strSQL)

            If dtsIdentificacionTenedor.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                Retorna_Identificacion_Tenedor_Planilla = dtsIdentificacionTenedor.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Identificacion_Tenedor").ToString()
            Else
                Retorna_Identificacion_Tenedor_Planilla = VALOR_CERO
            End If

        Catch ex As Exception
            Retorna_Identificacion_Tenedor_Planilla = VALOR_CERO
            objGeneral.Guardar_Mensaje_Log("Retorna_Identificacion_Tenedor_Planilla: " & ex.Message)

        End Try
        Return Retorna_Identificacion_Tenedor_Planilla

    End Function

    Private Function Retorna_Identificacion_Conductor_Planilla(ByVal lonNumePlan As Long) As String
        Dim objGeneral As New clsGeneral

        Try
            Dim strSQL As String, dtsIdentificacionConductor As DataSet

            strSQL = "SELECT TERC.Numero_Identificacion AS Identificacion_Conductor "
            strSQL += " FROM Encabezado_Planilla_Despachos AS ENPD,"
            strSQL += " Terceros AS TERC"
            strSQL += " WHERE ENPD.EMPR_Codigo = TERC.EMPR_Codigo"
            strSQL += " And ENPD.TERC_Codigo_Conductor = TERC.Codigo"
            strSQL += " And ENPD.EMPR_Codigo = " & intCodigoEmpresa.ToString
            strSQL += " And ENPD.Numero_Documento = " & lonNumePlan.ToString

            dtsIdentificacionConductor = objGeneral.Retorna_Dataset(strSQL)

            If dtsIdentificacionConductor.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                Retorna_Identificacion_Conductor_Planilla = dtsIdentificacionConductor.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Identificacion_Conductor").ToString()
            Else
                Retorna_Identificacion_Conductor_Planilla = VALOR_CERO
            End If

        Catch ex As Exception
            Retorna_Identificacion_Conductor_Planilla = VALOR_CERO
            objGeneral.Guardar_Mensaje_Log("Retorna_Identificacion_Conductor_Planilla: " & ex.Message)

        End Try
        Return Retorna_Identificacion_Conductor_Planilla

    End Function


    Private Function Retorna_Identificacion_Cambista_Egreso(ByVal lonNumCtaCam As Long) As String
        Dim objGeneral As New clsGeneral

        Try
            Dim strSQL As String, dtsIdentificacionCambista As DataSet

            strSQL = "SELECT TERC.Numero_Identificacion AS Identificacion_Cambista"
            strSQL += " FROM Cuenta_Bancarias AS CUBA, Terceros AS TERC"
            strSQL += " WHERE CUBA.EMPR_Codigo = TERC.EMPR_Codigo"
            strSQL += " AND CUBA.TERC_Codigo = TERC.Codigo "
            strSQL += " AND CUBA.EMPR_Codigo = " & intCodigoEmpresa.ToString
            strSQL += " AND CUBA.Codigo = " & lonNumCtaCam.ToString

            dtsIdentificacionCambista = objGeneral.Retorna_Dataset(strSQL)

            If dtsIdentificacionCambista.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                Retorna_Identificacion_Cambista_Egreso = dtsIdentificacionCambista.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Identificacion_Cambista").ToString()
            Else
                Retorna_Identificacion_Cambista_Egreso = VALOR_CERO
            End If

        Catch ex As Exception
            Retorna_Identificacion_Cambista_Egreso = VALOR_CERO
            objGeneral.Guardar_Mensaje_Log("Retorna_Identificacion_Cambista: " & ex.Message)

        End Try
        Return Retorna_Identificacion_Cambista_Egreso

    End Function

    Private Function Retorna_Consecutivo_Prefijo_EQP(ByVal lonNumePlan As Long) As String
        Dim objGeneral As New clsGeneral

        Try
            Dim strSQL As String, dtsConsPrefEQP As DataSet


            strSQL = "SELECT COUNT(Codigo)+1 AS ENDC_Consecutivo"
            strSQL += " FROM Encabezado_Documento_Comprobantes "
            strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
            strSQL += " AND TIDO_Codigo = " & TIDO_COMPROBANTE_EGRESO.ToString
            strSQL += " AND Documento_Origen = " & lonNumePlan.ToString
            strSQL += " AND CATA_DOOR_Codigo = " & DOCUMENTO_ORIGEN_SOBREANTICIPO.ToString
            strSQL += " AND CATA_FPDC_Codigo = " & MEDIO_PAGO_TRANSFERENCIA.ToString


            dtsConsPrefEQP = objGeneral.Retorna_Dataset(strSQL)

            If dtsConsPrefEQP.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                Retorna_Consecutivo_Prefijo_EQP = dtsConsPrefEQP.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("ENDC_Consecutivo").ToString()
            Else
                Retorna_Consecutivo_Prefijo_EQP = VALOR_CERO
            End If

        Catch ex As Exception
            Retorna_Consecutivo_Prefijo_EQP = VALOR_CERO
            objGeneral.Guardar_Mensaje_Log("Retorna_Cosnecutivo_Prefijo_EQP: " & ex.Message)

        End Try
        Return Retorna_Consecutivo_Prefijo_EQP

    End Function

    Private Function Retorna_Perfil_Tercero(ByVal lonCodiTerc As String, ByRef bolPerfilTransportador As Boolean, ByRef bolPerfilCliente As Boolean) As Boolean
        Dim objGeneral As New clsGeneral

        Try
            Dim strSQL As String, dtsPerfilTerceros As DataSet, strPerfil As String
            bolPerfilCliente = False : bolPerfilTransportador = False

            strSQL = "SELECT PETE.Codigo"
            strSQL += " FROM Terceros AS TERC, Perfil_Terceros AS PETE"
            strSQL += " WHERE TERC.EMPR_Codigo = PETE.EMPR_Codigo"
            strSQL += " AND TERC.Codigo = PETE.TERC_Codigo "
            strSQL += " AND TERC.EMPR_Codigo = " & intCodigoEmpresa.ToString
            strSQL += " AND TERC.Codigo = " & lonCodiTerc.ToString

            dtsPerfilTerceros = objGeneral.Retorna_Dataset(strSQL)

            If dtsPerfilTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                For intCon = 0 To dtsPerfilTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1
                    strPerfil = dtsPerfilTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Codigo").ToString()
                    If (strPerfil = VALOR_CATALOGO_PROPIETARIO Or strPerfil = VALOR_CATALOGO_CONDUCTOR Or strPerfil = VALOR_CATALOGO_TENEDOR Or strPerfil = VALOR_CATALOGO_AFILIADOR) Then
                        bolPerfilTransportador = True
                    ElseIf strPerfil = VALOR_CATALOGO_CLIENTE Then
                        bolPerfilCliente = True
                    End If
                Next
                Retorna_Perfil_Tercero = True
            Else
                Retorna_Perfil_Tercero = False
            End If

        Catch ex As Exception
            Retorna_Perfil_Tercero = False
            objGeneral.Guardar_Mensaje_Log("Retorna_Perfil_Tercero: " & ex.Message)

        End Try
        Return Retorna_Perfil_Tercero

    End Function

    Private Function Retorna_Informacion_Remesa(ByVal lonNumeReme As Long, ByRef strPlaca As String, ByRef strRuta As String, ByRef strProdTran As String, ByRef strSedePSL As String, ByRef strDaneOrig As String, ByRef strDaneDest As String) As Boolean
        Dim objGeneral As New clsGeneral

        Try
            Dim strSQL As String, dtsRemesas As DataSet

            strSQL = "SELECT VEHI.Placa AS Placa, RUTA.Nombre AS Ruta, PRTR.Nombre AS Producto, TEDI.Codigo_Alterno AS Sede_PSL, "
            strSQL += " ORIG.Codigo_Alterno AS CIUD_Orig_DANE, DEST.Codigo_Alterno AS CIUD_Dest_DANE "
            strSQL += " FROM Encabezado_Remesas AS ENRE, Vehiculos As VEHI, Rutas AS RUTA, Producto_Transportados AS PRTR, Tercero_Direcciones AS TEDI, "
            strSQL += " Ciudades AS ORIG, Ciudades AS DEST"
            strSQL += " WHERE ENRE.EMPR_Codigo = TEDI.EMPR_Codigo"
            strSQL += " AND ENRE.TEDI_Codigo = TEDI.Codigo "
            strSQL += " AND ENRE.EMPR_Codigo = VEHI.EMPR_Codigo"
            strSQL += " AND ENRE.VEHI_Codigo = VEHI.Codigo"
            strSQL += " AND ENRE.EMPR_Codigo = RUTA.EMPR_Codigo"
            strSQL += " AND ENRE.RUTA_Codigo = RUTA.Codigo"
            strSQL += " AND ENRE.EMPR_Codigo = PRTR.EMPR_Codigo"
            strSQL += " AND ENRE.PRTR_Codigo = PRTR.Codigo"
            strSQL += " AND RUTA.EMPR_Codigo = ORIG.EMPR_Codigo"
            strSQL += " AND RUTA.CIUD_Codigo_Origen = ORIG.Codigo"
            strSQL += " AND RUTA.EMPR_Codigo = DEST.EMPR_Codigo"
            strSQL += " AND RUTA.CIUD_Codigo_Destino = DEST.Codigo"
            strSQL += " AND ENRE.EMPR_Codigo = " & intCodigoEmpresa.ToString
            strSQL += " AND ENRE.Numero = " & lonNumeReme.ToString

            dtsRemesas = objGeneral.Retorna_Dataset(strSQL)

            If dtsRemesas.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                strPlaca = dtsRemesas.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Placa").ToString()
                strRuta = dtsRemesas.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Ruta").ToString()
                strProdTran = dtsRemesas.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Producto").ToString()
                strSedePSL = dtsRemesas.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Sede_PSL").ToString()
                strDaneOrig = dtsRemesas.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("CIUD_Orig_DANE").ToString()
                strDaneDest = dtsRemesas.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("CIUD_Dest_DANE").ToString()

                Retorna_Informacion_Remesa = True
            Else
                strPlaca = ""
                strRuta = ""
                strProdTran = ""
                strSedePSL = ""
                strDaneOrig = ""
                strDaneDest = ""

                Retorna_Informacion_Remesa = False
            End If

        Catch ex As Exception
            Retorna_Informacion_Remesa = False
            objGeneral.Guardar_Mensaje_Log("Retorna_Informacion_Remesa: " & ex.Message)

        End Try
        Return Retorna_Informacion_Remesa

    End Function

#End Region

#Region "Funciones PSL"
    Private Function Consultar_Solicitud_Anticipo_PSL(ByVal lonNumePlan As Long, ByRef strEstado As String) As Boolean
        Dim objGeneral As New clsGeneral

        Try
            Dim strSQL As String, dtsSoliAnti As DataSet, strNombre As String

            strSQL = "SELECT eobcodigo, eobnombre"
            strSQL += " FROM CP_ENCDOCPAGO"
            strSQL += " WHERE edptipocons = '" & TIPO_CONSECUTIVO_SOLICITUD_ANTICIPO_PSL & "'"
            strSQL += " AND edpprefijo = '" & PREFIJO_SOLICITUD_ANTICIPO_PSL & "'"
            strSQL += " AND edpnumero = '" & lonNumePlan & "'"

            dtsSoliAnti = objGeneral.Retorna_Dataset_PSL(strSQL)

            If dtsSoliAnti.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                strEstado = dtsSoliAnti.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("eobcodigo").ToString()
                strNombre = dtsSoliAnti.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("eobnombre").ToString()
                Consultar_Solicitud_Anticipo_PSL = True
            Else
                strEstado = ""
                Consultar_Solicitud_Anticipo_PSL = False
            End If

        Catch ex As Exception
            Consultar_Solicitud_Anticipo_PSL = False
            objGeneral.Guardar_Mensaje_Log("Consultar_Solicitud_Anticipo_PSL: " & ex.Message)
        End Try

        Return Consultar_Solicitud_Anticipo_PSL

    End Function

    Private Function Existe_Tercero_PSL(ByVal strNumeIden As String) As Boolean
        Dim objGeneral As New clsGeneral

        Try
            Dim strSQL As String, dtsTercero As DataSet

            strSQL = "SELECT tgenit"
            strSQL += " FROM un_tercegener"
            strSQL += " WHERE tgenit = '" & strNumeIden & "'"

            dtsTercero = objGeneral.Retorna_Dataset_PSL(strSQL)

            If dtsTercero.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                Existe_Tercero_PSL = True
            Else
                Existe_Tercero_PSL = False
            End If

        Catch ex As Exception
            Existe_Tercero_PSL = False
            objGeneral.Guardar_Mensaje_Log("Existe_Tercero_PSL: " & ex.Message)
        End Try
        Return Existe_Tercero_PSL
    End Function

#End Region
End Class
