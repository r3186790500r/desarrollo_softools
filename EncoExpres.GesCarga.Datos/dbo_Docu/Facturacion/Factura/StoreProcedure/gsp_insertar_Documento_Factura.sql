﻿
Print 'CREATE PROCEDURE gsp_insertar_Documento_Factura'
GO
DROP PROCEDURE gsp_insertar_Documento_Factura
GO

CREATE PROCEDURE gsp_insertar_Documento_Factura (
@par_EMPR_Codigo  smallint,
@par_ENFA_Numero  NUMERIC,
@par_Tipo_Docu	  VARCHAR(50) = '',
@par_Archivo	  VARCHAR(MAX),
@par_Descrip	  VARCHAR(100)
)
AS
BEGIN
	declare @fechaCrea datetime = null
	set @fechaCrea = GETDATE()
	
	IF EXISTS   
	(SELECT ENFA_Numero   
	FROM Documentos_Factura   
	WHERE EMPR_Codigo = @par_EMPR_Codigo   
	AND ENFA_Numero=@par_ENFA_Numero
	AND Tipo_documento=@par_Tipo_Docu) BEGIN  
		 DELETE Documentos_Factura WHERE  EMPR_Codigo = @par_EMPR_Codigo  AND ENFA_Numero=@par_ENFA_Numero AND Tipo_documento=@par_Tipo_Docu
	END  
	
	INSERT INTO Documentos_Factura
	(EMPR_Codigo, ENFA_Numero, Tipo_documento, Fecha_Crea, Archivo, Descripcion)
	VALUES
	(@par_EMPR_Codigo, @par_ENFA_Numero, @par_Tipo_Docu, @fechaCrea, @par_Archivo, @par_Descrip) 
	
	SELECT ENFA_Numero AS ENFA_Numero
	FROM Documentos_Factura
	WHERE EMPR_Codigo = @par_EMPR_Codigo 
	AND ENFA_Numero=@par_ENFA_Numero
	AND Tipo_documento=@par_Tipo_Docu
END
GO