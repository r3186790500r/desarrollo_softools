﻿Imports Newtonsoft.Json
Imports System.Threading
Imports System.Data.SqlClient
Imports System.Net
Imports System.Text
Imports System.IO
Imports System.Configuration.ConfigurationSettings

Public Class JsonPost

#Region "Declaración de Variables"
    Dim thrReporte As Thread
    Dim intCodiEmpr As Byte
    Dim strUsuaWebServ As String
    Dim strClavWebServ As String
    Dim strLogWebServ As String
    Dim strRegWebServ As String
    Dim strTraWebServ As String
    Dim intDiasConsDesp As Integer
    Dim intDiasConsNove As Integer
    Dim TERCCodigoProsegur As Integer
    Dim strCadenaDeConexionSQL As String
    Dim sqlConexion As SqlConnection
    Dim sqlComando As SqlCommand
    Dim strRutaArchLog As String
    Dim stwrLog As StreamWriter
    Dim intNumeMani As Integer
    Dim strPlacVehi As String
    Dim intCodiRuta As Integer
    Dim XML As String
    Dim intEstaRepo As Integer
    Dim strMensSatr As String
    Dim intTiemSuspServ As Integer
    Private strRutaLog As String
    Private strNombLog As String
    Private archLog As StreamWriter

#End Region

    Private urlToPost As String = ""
    Private urlToTrack As String = ""
    Private autToken As String = ""

#Region "Propiedades"
    Public Property CodigoEmpresa As Integer
        Get
            Return Me.intCodiEmpr
        End Get
        Set(ByVal value As Integer)
            Me.intCodiEmpr = value
        End Set
    End Property

    Public Property UsuarioWebServices As String
        Get
            Return Me.strUsuaWebServ
        End Get
        Set(ByVal value As String)
            Me.strUsuaWebServ = value
        End Set
    End Property

    Public Property ClaveWebServices As String
        Get
            Return Me.strClavWebServ
        End Get
        Set(ByVal value As String)
            Me.strClavWebServ = value
        End Set
    End Property

    Public Property UrlLoginWebServices As String
        Get
            Return Me.strLogWebServ
        End Get
        Set(ByVal value As String)
            Me.strLogWebServ = value
        End Set
    End Property

    Public Property UrlRegistroWebServices As String
        Get
            Return Me.strRegWebServ
        End Get
        Set(ByVal value As String)
            Me.strRegWebServ = value
        End Set
    End Property

    Public Property UrlTrackWebServices As String
        Get
            Return Me.strTraWebServ
        End Get
        Set(ByVal value As String)
            Me.strTraWebServ = value
        End Set
    End Property

    Public Property CadenaConexionSQL As String
        Get
            Return Me.strCadenaDeConexionSQL
        End Get
        Set(ByVal value As String)
            Me.strCadenaDeConexionSQL = value
        End Set
    End Property

    Public Property ConexionSQL As SqlConnection
        Get
            Return Me.sqlConexion
        End Get
        Set(ByVal value As SqlConnection)
            Me.sqlConexion = value
        End Set
    End Property

    Public Property ComandoSQL As SqlCommand
        Get
            Return Me.sqlComando
        End Get
        Set(ByVal value As SqlCommand)
            Me.sqlComando = value
        End Set
    End Property

    Public Property RutaLog() As String
        Get
            Return Me.strRutaLog
        End Get
        Set(ByVal value As String)
            Me.strRutaLog = value
        End Set
    End Property

    Public Property ArchivoLog() As StreamWriter
        Get
            Return Me.archLog
        End Get
        Set(ByVal value As StreamWriter)
            Me.archLog = value
        End Set
    End Property
#End Region

    Public Sub New()
        InitializeComponent()
        ' Esta llamada es exigida por el diseñador.
        'InitializeComponent()
        Cargar_Valores_AppConfig()
        autToken = ObtenerAuthToken()
        Consultar_Seguimientos_Mueve(autToken)
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Protected Overrides Sub OnStart(ByVal args() As String)
        'Me.thrReporte = New Thread(AddressOf Iniciar_Proceso_Reporte)
        'Me.thrReporte.Start()
    End Sub

    Protected Overrides Sub OnStop()
        'Me.thrReporte.Suspend()
        ' Agregue el código aquí para realizar cualquier anulación necesaria para detener el servicio.
    End Sub

    Private Sub Iniciar_Proceso_Reporte()
        Try
            While (True)
                Cargar_Valores_AppConfig()
                autToken = ObtenerAuthToken()
                Consultar_Seguimientos_Mueve(autToken)
                Thread.Sleep(Val(AppSettings.Get("TiempoSuspencionProcesoDespacho")))
            End While
        Catch ex As Exception
        End Try
    End Sub

    Function Cargar_Valores_AppConfig() As Boolean
        Try
            Me.CodigoEmpresa = AppSettings.Get("Empresa")
            Me.UsuarioWebServices = AppSettings.Get("Usuario")
            Me.ClaveWebServices = AppSettings.Get("Clave")
            Me.UrlLoginWebServices = AppSettings.Get("ClientUriLogin")
            Me.UrlRegistroWebServices = AppSettings.Get("ClientUriRegistro")
            Me.UrlTrackWebServices = AppSettings.Get("ClientUriTrack")
            Me.CadenaConexionSQL = AppSettings.Get("ConexionSQLGestrans")
            Me.RutaLog = AppSettings.Get("RutaArchivoLog")
            'Me.TiempoSuspencionServicio = AppSettings.Get("TiempoSuspencionProcesoDespacho")
            Me.strNombLog = AppSettings.Get("NombreLog")
            'Me.TERCCodigoProsegur = AppSettings.Get("TERC_Codigo_Prosegur")
            Cargar_Valores_AppConfig = True
        Catch ex As Exception
            'Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaArchivoLog & Me.NombreLog, True)
            'Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & " Resultado: " & ex.Message)
            'Me.ArchivoLog.Close()
            Cargar_Valores_AppConfig = False
        End Try
    End Function

    Public Function ObtenerAuthToken() As String
        Me.urlToPost = Me.UrlLoginWebServices
        Dim uriToPost As New Uri(urlToPost)
        Dim dictData As New Dictionary(Of String, Object)
        dictData.Add("usuario", Me.UsuarioWebServices)
        dictData.Add("clave", Me.ClaveWebServices)

        Dim data = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(dictData, Formatting.Indented))
        Dim result_post = SendRequest(uriToPost, data, "application/json", "POST")

        Dim jsonResult = JsonConvert.DeserializeObject(Of Dictionary(Of String, Object))(result_post)
        ObtenerAuthToken = jsonResult.Item("data").Item(0).Value(Of String)("token")

    End Function

    Public Function Consultar_Seguimientos_Mueve(ByVal autToken As String) As String

        Dim strSQL
        Dim dsDataSet As New DataSet
        Dim dicRetornoMueve As New Dictionary(Of String, Object)
        Dim strRetornoProsegur As String = ""
        Dim strPlaca As String = String.Empty
        Dim strUsuarioGPS As String = String.Empty
        Dim strPasswordGPS As String = String.Empty
        Dim intPlanilla As Integer = 0

        strSQL = "EXEC gsp_consultar_vehiculos_seguimiento_reporte "
        strSQL += Me.CodigoEmpresa.ToString()

        Me.ConexionSQL = New SqlConnection(Me.CadenaConexionSQL)
        Me.ComandoSQL = New SqlCommand(strSQL, Me.ConexionSQL)
        dsDataSet = Me.Retorna_Dataset(strSQL)

        If dsDataSet.Tables(0).Rows.Count > 0 Then
            For Each Registro As DataRow In dsDataSet.Tables(0).Rows
                strPlaca = Registro.Item(4).ToString
                strUsuarioGPS = Registro.Item(8).ToString
                strPasswordGPS = Registro.Item(9).ToString
                intPlanilla = Registro.Item(21).ToString
                dicRetornoMueve = ObtainPositionMueve(Registro)
                If Not IsNothing(dicRetornoMueve) Then
                    InsertarPositionMueve(dicRetornoMueve, intPlanilla, strPlaca)
                End If
            Next
        End If


    End Function

    Public Function ObtainPositionMueve(ByVal Registro As DataRow) As Dictionary(Of String, Object)

        Me.urlToPost = Me.UrlRegistroWebServices
        Me.urlToTrack = Me.UrlTrackWebServices
        Dim uriToPost As New Uri(urlToPost)
        Dim uriToTrack As New Uri(urlToTrack)
        Dim dictData As New Dictionary(Of String, Object)
        Dim dictTrack As New Dictionary(Of String, Object)
        dictData.Add("id_manifiesto", Registro.Item(0).ToString)
        dictData.Add("cod_ruta", Registro.Item(1).ToString)
        dictData.Add("nit_destinatario", Registro.Item(2).ToString)
        dictData.Add("sede", Registro.Item(3).ToString)
        dictData.Add("vehiculo_placa", Registro.Item(4).ToString)
        dictData.Add("conductor_nombre", Registro.Item(5).ToString)
        dictData.Add("conductor_identificacion", Registro.Item(6).ToString)
        dictData.Add("conductor_telefono", Registro.Item(7).ToString)
        dictData.Add("gps_usuario", Registro.Item(8).ToString)
        dictData.Add("gps_password", Registro.Item(9).ToString)
        dictData.Add("plataforma_nit", Registro.Item(2).ToString)
        dictData.Add("origen", Registro.Item(10).ToString)
        dictData.Add("destino", Registro.Item(11).ToString)
        dictData.Add("cod_cargue", Registro.Item(12).ToString)
        dictData.Add("pto_cargue", Registro.Item(13).ToString & "," & Registro.Item(14).ToString)
        dictData.Add("cod_descargue", Registro.Item(15).ToString)
        dictData.Add("pto_descargue", Registro.Item(16).ToString & "," & Registro.Item(17).ToString)
        dictData.Add("producto", Registro.Item(18).ToString)
        dictData.Add("fecha_carga", Registro.Item(19).ToString)
        dictData.Add("tipo_vh", Registro.Item(20).ToString)
        dictData.Add("pin", "null")

        dictTrack.Add("placas", Registro.Item(4).ToString)

        Dim data = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(dictData, Formatting.Indented))
        Dim dataTrack = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(dictTrack, Formatting.Indented))

        Dim result_post = SendRequestRegister(uriToPost, data, "application/json", "POST", autToken)
        Dim track_post = SendRequestTracks(uriToTrack, dataTrack, "application/json", "POST", autToken)

        If Not IsNothing(result_post) And Not IsNothing(track_post) Then
            Dim jsonResult = JsonConvert.DeserializeObject(Of Dictionary(Of String, Object))(result_post)
            Dim jsonTrack = JsonConvert.DeserializeObject(Of Dictionary(Of String, Object))(track_post)
            If jsonTrack.Item("validado").ToString <> "False" Then
                'Dim jsonStatus = jsonTrack.Item("status").ToString
                'If jsonStatus = 200 Then
                ObtainPositionMueve = jsonTrack
            Else
                ObtainPositionMueve = Nothing
                'End If
            End If
        Else
                ObtainPositionMueve = Nothing
        End If

    End Function

    Private Sub InsertarPositionMueve(dicRetornoMueve As Dictionary(Of String, Object), Planilla As Integer, strPlaca As String)
        Try

            'Dim FechaReporte As Date
            Dim Evento As String = String.Empty
            Dim Placa As String = String.Empty
            'Dim Velocidad As String = String.Empty
            'Dim Ubicacion As String = String.Empty
            'Dim Latitud As String = String.Empty
            'Dim Longitud As String = String.Empty
            Dim dblLatitud As Double = 0
            Dim dblLongitud As Double = 0


            Placa = strPlaca
            'FechaReporte = dicRetornoMueve.Item("_creado").ToString
            Evento = dicRetornoMueve.Item("mensaje").ToString
            'Velocidad = dicRetornoMueve.Item("velocidad").ToString
            'Ubicacion = dicRetornoMueve.Item("direccion").ToString
            'Longitud = Math.Round(CDbl(Val(dicRetornoMueve.Item("longitude"))), 10).ToString
            'Latitud = Math.Round(CDbl(Val(dicRetornoMueve.Item("latitud"))), 10).ToString

            If Me.ConexionSQL.State = ConnectionState.Open Then
                Me.ConexionSQL.Close()
            Else
                Me.ConexionSQL.Open()
                Me.ComandoSQL = New SqlCommand("gsp_insertar_seguimientos_empresa_gps_grupo_mueve", Me.ConexionSQL)
                Me.ComandoSQL.CommandType = CommandType.StoredProcedure

                Me.ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : Me.ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.CodigoEmpresa
                'Me.ComandoSQL.Parameters.Add("@par_Fecha_Reporte", SqlDbType.DateTime) : Me.ComandoSQL.Parameters("@par_Fecha_Reporte").Value = Convert.ToDateTime(FechaReporte)
                Me.ComandoSQL.Parameters.Add("@par_VEHI_Placa", SqlDbType.VarChar) : Me.ComandoSQL.Parameters("@par_VEHI_Placa").Value = Placa
                Me.ComandoSQL.Parameters.Add("@par_Nombre_Evento", SqlDbType.VarChar) : Me.ComandoSQL.Parameters("@par_Nombre_Evento").Value = Evento
                'Me.ComandoSQL.Parameters.Add("@par_Latitud", SqlDbType.Money) : Me.ComandoSQL.Parameters("@par_Latitud").Value = Latitud
                'Me.ComandoSQL.Parameters.Add("@par_Longitud", SqlDbType.Money) : Me.ComandoSQL.Parameters("@par_Longitud").Value = Longitud
                'Me.ComandoSQL.Parameters.Add("@par_Velocidad", SqlDbType.VarChar) : Me.ComandoSQL.Parameters("@par_Velocidad").Value = Velocidad
                'Me.ComandoSQL.Parameters.Add("@par_Mensaje_Evento", SqlDbType.VarChar, 50) : Me.ComandoSQL.Parameters("@par_Mensaje_Evento").Value = Ubicacion
                Me.ComandoSQL.Parameters.Add("@par_ENPD_Numero", SqlDbType.Int) : Me.ComandoSQL.Parameters("@par_ENPD_Numero").Value = Planilla

                If Me.ComandoSQL.ExecuteNonQuery = 1 Then
                    Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaLog & Me.strNombLog, True)
                    Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Resultado: Ubicacion del vehiculo: " & Placa & " , Evento: " & Evento)
                    Me.ArchivoLog.Close()
                Else
                    Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaLog & Me.strNombLog, True)
                    Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Resultado: La ubicacion del vehiculo: " & Placa & " , No se pudo guardar ")
                    Me.ArchivoLog.Close()
                End If
                Me.ConexionSQL.Close()
            End If
            'End If
        Catch ex As Exception
            Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaLog & Me.strNombLog, True)
            Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & " Resultado: " & ex.Message)
            Me.ArchivoLog.Close()
        Finally
            If Me.ConexionSQL.State = ConnectionState.Open Then
                Me.ConexionSQL.Close()
            End If
        End Try
    End Sub

    Private Function SendRequest(uri As Uri, jsonDataBytes As Byte(), contentType As String, method As String) As String
        Dim response As String
        Dim request As WebRequest

        request = WebRequest.Create(uri)
        request.ContentLength = jsonDataBytes.Length
        request.ContentType = contentType
        request.Method = method

        Using requestStream = request.GetRequestStream
            requestStream.Write(jsonDataBytes, 0, jsonDataBytes.Length)
            requestStream.Close()

            Using responseStream = request.GetResponse.GetResponseStream
                Using reader As New StreamReader(responseStream)
                    response = reader.ReadToEnd()
                End Using
            End Using
        End Using

        Return response
    End Function

    Private Function SendRequestRegister(uri As Uri, jsonDataBytes As Byte(), contentType As String, method As String, autoToken As String) As String
        Dim response As String
        Dim request As WebRequest

        request = WebRequest.Create(uri)
        request.ContentLength = jsonDataBytes.Length
        request.ContentType = contentType
        request.Method = method
        request.Headers("Authorization") = "Bearer " & autoToken

        Using requestStream = request.GetRequestStream
            requestStream.Write(jsonDataBytes, 0, jsonDataBytes.Length)
            requestStream.Close()
            Try
                Using responseStream = request.GetResponse.GetResponseStream
                    Using reader As New StreamReader(responseStream)
                        response = reader.ReadToEnd()
                    End Using
                End Using
            Catch ex As Exception
                Dim algo As String = ex.Message
            End Try
            'Using responseStream = request.GetResponse.GetResponseStream
            '    Using reader As New StreamReader(responseStream)
            '        response = reader.ReadToEnd()
            '    End Using
            'End Using
        End Using

        Return response
    End Function

    Private Function SendRequestTracks(uri As Uri, jsonDataBytes As Byte(), contentType As String, method As String, autoToken As String) As String
        Dim response As String
        Dim request As WebRequest

        request = WebRequest.Create(uri)
        request.ContentLength = jsonDataBytes.Length
        request.ContentType = contentType
        request.Method = method
        request.Headers("Authorization") = "Bearer " & autoToken

        Using requestStream = request.GetRequestStream
            requestStream.Write(jsonDataBytes, 0, jsonDataBytes.Length)
            requestStream.Close()
            Try
                Using responseStream = request.GetResponse.GetResponseStream
                    Using reader As New StreamReader(responseStream)
                        response = reader.ReadToEnd()
                    End Using
                End Using
            Catch ex As Exception
                Dim algo As String = ex.Message
            End Try
            'Using responseStream = request.GetResponse.GetResponseStream
            '    Using reader As New StreamReader(responseStream)
            '        response = reader.ReadToEnd()
            '    End Using
            'End Using
        End Using

        Return response
    End Function

    Private Function Retorna_Dataset(ByVal strSQL As String, Optional ByRef strError As String = "") As DataSet
        Dim daDataAdapter As SqlDataAdapter
        Dim dsDataSet As DataSet = New DataSet

        Try
            Using ConexionSQL = New SqlConnection(Me.strCadenaDeConexionSQL)
                ConexionSQL.Open()
                daDataAdapter = New SqlDataAdapter(strSQL, ConexionSQL)
                daDataAdapter.Fill(dsDataSet)
                ConexionSQL.Close()
            End Using

        Catch ex As Exception
            strError = ex.Message.ToString()
        Finally
            If ConexionSQL.State() = ConnectionState.Open Then
                ConexionSQL.Close()
            End If
        End Try

        Retorna_Dataset = dsDataSet

        Try
            dsDataSet.Dispose()
        Catch ex As Exception
            strError = ex.Message.ToString()
        End Try

    End Function



End Class
