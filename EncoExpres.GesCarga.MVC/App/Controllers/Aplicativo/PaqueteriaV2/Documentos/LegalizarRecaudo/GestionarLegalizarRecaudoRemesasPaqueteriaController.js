﻿EncoExpresApp.controller("GestionarLegalizarRecaudoRemesasPaqueteriaCtrl", ['$scope', '$timeout', '$linq', '$routeParams', 'blockUI', 'blockUIConfig', 'ValorCatalogosFactory',
    'OficinasFactory', 'LegalizarRecaudoRemesasFactory', 'RemesaGuiasFactory', 'TercerosFactory', 'CierreContableDocumentosFactory',
    function ($scope, $timeout, $linq, $routeParams, blockUI, blockUIConfig, ValorCatalogosFactory,
        OficinasFactory, LegalizarRecaudoRemesasFactory, RemesaGuiasFactory, TercerosFactory, CierreContableDocumentosFactory) {
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        $scope.Titulo = 'LEGALIZAR RECAUDO GUÍA';
        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Documentos' }, { Nombre: 'Legalizar Recaudo Guía' }, { Nombre: 'Gestionar' }];
        $scope.Master = '#!ConsultarLegalizarRecaudoRemesasPaqueteria';
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PAQUETERIA.LEGALIZAR_RECAUDO_REMESAS);
            $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

            $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
            $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
            $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
            $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        //--------------------------Variables-------------------------//
        $scope.MENU_OPCION_GUIA = OPCION_MENU_PAQUETERIA.REMESAS;
        $scope.Deshabilitar = false;
        $scope.Filtro = {
            NumeroInicial: '',
            NumeroFinal: ''
        };
        $scope.Modelo = {
            Numero: 0,
            Planilla: {},
            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Fecha: new Date()
        };
        $scope.ListadoEstados = [
            { Nombre: 'DEFINITIVO', Codigo: ESTADO_DEFINITIVO },
            { Nombre: 'BORRADOR', Codigo: ESTADO_BORRADOR }
        ];
        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + ESTADO_BORRADOR);
        $scope.ListadoRemesas = [];
        $scope.ListadoRemesasGuardadas = [];
        $scope.ListadoMedioPago = [];
        $scope.ListadoConductores = [];
        $scope.ListadoOficinas = [];
        $scope.PagRemesas = {
            Buscando: false,
            totalRegistros: 0,
            totalPaginas: 0,
            paginaActual: 1,
            ResultadoSinRegistros: ''
        };
        $scope.PagRemesasGuardadas = {
            totalRegistros: 0,
            totalPaginas: 0,
            paginaActual: 1,
            ResultadoSinRegistros: ''
        };
        //--------------------------Variables-------------------------//
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //-------------AutoComplete

        $scope.AutocompleteOficinas = function (value) {

            $scope.ListadoOficinasDestino = [];
            $scope.ListadoOficinasDestino = OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Sync: true }).Datos;
            return $scope.ListadoOficinasDestino;
        }
        //----Conductor
        $scope.AutocompleteConductor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CONDUCTOR, ValorAutocomplete: value, Sync: true });
                    $scope.ListadoConductores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoConductores);
                }
            }
            return $scope.ListadoConductores;
        };
        //----Conductor
        //-------------AutoComplete
        //----------Init
        $scope.InitLoad = function () {
            //----Obtener Parametros
            $scope.ListadoMedioPago = ValorCatalogosFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Catalogo: { Codigo: CODIGO_CATALOGO_MEDIO_PAGO },
                Estado: ESTADO_ACTIVO,
                Sync: true
            }).Datos;
            $scope.ListadoMedioPago.splice($scope.ListadoMedioPago.findIndex(item => item.Codigo === CODIGO_MEDIO_PAGO_NO_APLICA), 1);
            $scope.ListadoMedioPago.push({ Codigo: -1, Nombre: "SELECCIONE" });
            //----Oficinas
            if ($scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas) {
                var responseOfic = OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_DEFINITIVO, Sync: true });
                if (responseOfic.ProcesoExitoso == true) {
                    $scope.ListadoOficinas.push({ Codigo: -1, Nombre: "(TODAS)" });
                    responseOfic.Datos.forEach(function (item) {
                        $scope.ListadoOficinas.push(item);
                    });
                    $scope.Filtro.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==-1');
                }
            } else {
                $scope.ListadoOficinas.push({ Codigo: -1, Nombre: "(TODAS)" });
                $scope.ListadoOficinas.push({
                    Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
                    Nombre: $scope.Sesion.UsuarioAutenticado.Oficinas.Nombre
                });
                $scope.Filtro.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==-1');
            }
            if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== 0) {
                if ($routeParams.Numero > 0) {
                    $scope.Modelo.Numero = $routeParams.Numero;
                    PantallaBloqueo(Obtener, 'Obteniendo Documento');
                }
            }
        };
        //----------Init
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------------------------Funciones Generales---------------------------------//
        //--------------------------PAGINACION-----------------------------//
        function ResetPaginacion(obj) {
            obj.totalRegistros = obj.array.length;
            obj.totalPaginas = Math.ceil(obj.totalRegistros / 10);
            $scope.PrimerPagina(obj);
        }
        $scope.PrimerPagina = function (obj) {
            if (obj.totalRegistros > 10) {
                obj.PagArray = [];
                obj.paginaActual = 1;
                for (var i = 0; i < 10; i++) {
                    obj.PagArray.push(obj.array[i]);
                }
            } else {
                obj.PagArray = obj.array;
            }
        };
        $scope.Anterior = function (obj) {
            if (obj.paginaActual > 1) {
                obj.paginaActual -= 1;
                var a = obj.paginaActual * 10;
                if (a < obj.totalRegistros) {
                    obj.PagArray = [];
                    for (var i = a - 10; i < a; i++) {
                        obj.PagArray.push(obj.array[i]);
                    }
                }
            }
            else {
                $scope.PrimerPagina(obj);
            }
        };
        $scope.Siguiente = function (obj) {
            if (obj.paginaActual < obj.totalPaginas) {
                obj.paginaActual += 1;
                var a = obj.paginaActual * 10;
                if (a < obj.totalRegistros) {
                    obj.PagArray = [];
                    for (var i = a - 10; i < a; i++) {
                        obj.PagArray.push(obj.array[i]);
                    }
                } else {
                    $scope.UltimaPagina(obj);
                }
            } else if (obj.paginaActual == obj.totalPaginas) {
                $scope.UltimaPagina(obj);
            }
        };
        $scope.UltimaPagina = function (obj) {
            if (obj.totalRegistros > 10 && obj.totalPaginas > 1) {
                obj.paginaActual = obj.totalPaginas;
                var a = obj.paginaActual * 10;
                obj.PagArray = [];
                for (var i = a - 10; i < obj.array.length; i++) {
                    obj.PagArray.push(obj.array[i]);
                }
            }
        }
        //--------------------------PAGINACION-----------------------------//
        function PantallaBloqueo(fun, mensaje) {
            if (mensaje == undefined) {
                mensaje = "Espere por favor...";
            }
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start(mensaje);
            $timeout(function () { blockUI.message(mensaje); fun(); }, 100);
        }
        //------------Remesas------------//
        $scope.CargarRemesas = function () {
            PantallaBloqueo(ObtenerRemesas, "Cargando Remesas...");
        };
        //--Cargar Remesas
        function ObtenerRemesas() {
            $scope.ListadoRemesas = [];
            $scope.PagRemesas.Buscando = true;
            var filtro = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA,
                NumeroPlanilla: $scope.Filtro.NumeroPlanilla,
                NumeroDocumentoLegalizarGuias: $scope.Filtro.NumeroLegalizarGuia,
                Remesa: { Conductor: $scope.Filtro.Conductor },
                OficinaActual: $scope.Filtro.Oficina,
                Estado: ESTADO_DEFINITIVO,
                Anulado: ESTADO_NO_ANULADO,
                Sync: true
            };
            var response = RemesaGuiasFactory.ConsultarRemesasPorLegalizarRecaudo(filtro);
            if (response.ProcesoExitoso === true) {
                if (response.Datos.length > CERO) {
                    if ($scope.ListadoRemesasGuardadas != undefined && $scope.ListadoRemesasGuardadas.length > CERO) {
                        //Verifica si existen remesas guardadas con la busqueda actual, para no generar duplicidad
                        for (var i = 0; i < response.Datos.length; i++) {
                            var Existe = false;
                            for (var j = 0; j < $scope.ListadoRemesasGuardadas.length; j++) {
                                if (response.Datos[i].Remesa.Numero == $scope.ListadoRemesasGuardadas[j].Numero) {
                                    Existe = true;
                                    break;
                                }
                            }
                            if (!Existe) {
                                response.Datos[i].MetodoPago = $linq.Enumerable().From($scope.ListadoMedioPago).First('$.Codigo ==-1');
                                $scope.ListadoRemesas.push(response.Datos[i]);
                            }
                        }
                    }
                    else {
                        //Agrega directamente al listado
                        $scope.ListadoRemesas = response.Datos;
                        for (var i = 0; i < $scope.ListadoRemesas.length; i++) {
                            $scope.ListadoRemesas[i].MetodoPago = $linq.Enumerable().From($scope.ListadoMedioPago).First('$.Codigo ==-1');
                        }
                    }
                    $scope.PagRemesas.array = $scope.ListadoRemesas;
                    $scope.PagRemesas.ResultadoSinRegistros = '';
                    ResetPaginacion($scope.PagRemesas);
                }
                else {
                    $scope.PagRemesas.totalRegistros = 0;
                    $scope.PagRemesas.totalPaginas = 0;
                    $scope.PagRemesas.paginaActual = 1;
                    $scope.PagRemesas.array = [];
                    $scope.PagRemesas.Buscando = false;
                    $scope.PagRemesas.ResultadoSinRegistros = 'No hay datos para mostrar';
                }
                $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
            }
            else {
                ShowError(response.statusText);
                $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
            }
            $scope.PagRemesas.Buscando = false;
        }
        //--Cargar Remesas
        //--Limpiar Filtro
        $scope.LimpiarRemesas = function () {
            $scope.ListadoRemesas = [];
            $scope.PagRemesas.totalRegistros = 0;
            $scope.PagRemesas.totalPaginas = 0;
            $scope.PagRemesas.paginaActual = 1;
            $scope.PagRemesas.array = [];
            ResetPaginacion($scope.PagRemesas);
        };
        //--Limpiar Filtro
        //--Marcar Todas Las Remesas
        $scope.MarcarRemesas = function () {
            for (var i = 0; i < $scope.ListadoRemesas.length; i++) {
                var item = $scope.ListadoRemesas[i];
                $scope.ListadoRemesasGuardadas.push({
                    Numero: item.Remesa.Numero,
                    NumeroDocumento: item.Remesa.NumeroDocumento,
                    NumeroDocumentoPlanilla: item.NumeroPlanilla,
                    NumeroDocumentoLegalizarRemesas: item.NumeroDocumentoLegalizarGuias,
                    Remitente: item.Remesa.Cliente.Codigo > 0 ? item.Remesa.Cliente : item.Remesa.Remitente,
                    Destinatario: item.Remesa.Destinatario,
                    FormaPago: item.Remesa.FormaPago,
                    MetodoPago: item.MetodoPago,
                    FechaPago: item.FechaPago,
                    Valor: item.Remesa.TotalFleteCliente,
                    DocumentoPago: item.DocumentoPago,
                    Observaciones: item.Observaciones,
                    Legalizo: item.Legalizo == true ? 1 : 0,
                    ResponsableLegalizarRecaudo: item.ResponsableLegalizarRecaudo
                });
            }
            $scope.ListadoRemesas = [];
            $scope.PagRemesas.array = $scope.ListadoRemesas;
            ResetPaginacion($scope.PagRemesas);

            $scope.PagRemesasGuardadas.array = $scope.ListadoRemesasGuardadas;
            ResetPaginacion($scope.PagRemesasGuardadas);

            $scope.Calcular();
        };
        //--Marcar Todas Las Remesas
        //--Agregar Remesa
        $scope.AgregarRemesa = function (ENRE_Numero) {
            var indexReme = 0;
            for (var i = 0; i < $scope.ListadoRemesas.length; i++) {
                if ($scope.ListadoRemesas[i].Remesa.Numero == ENRE_Numero) {
                    indexReme = i;
                    break;
                }
            }
            var item = $scope.ListadoRemesas[indexReme];
            $scope.ListadoRemesasGuardadas.push({
                Numero: item.Remesa.Numero,
                NumeroDocumento: item.Remesa.NumeroDocumento,
                NumeroDocumentoPlanilla: item.NumeroPlanilla,
                NumeroDocumentoLegalizarRemesas: item.NumeroDocumentoLegalizarGuias,
                Remitente: item.Remesa.Cliente.Codigo > 0 ? item.Remesa.Cliente : item.Remesa.Remitente,
                Destinatario: item.Remesa.Destinatario,
                FormaPago: item.Remesa.FormaPago,
                MetodoPago: item.MetodoPago,
                FechaPago: item.FechaPago,
                Valor: item.Remesa.TotalFleteCliente,
                DocumentoPago: item.DocumentoPago,
                Observaciones: item.Observaciones,
                Legalizo: item.Legalizo == true ? 1 : 0,
                ResponsableLegalizarRecaudo: item.ResponsableLegalizarRecaudo
            });

            $scope.ListadoRemesas.splice(indexReme, 1);

            $scope.PagRemesas.array = $scope.ListadoRemesas;
            ResetPaginacion($scope.PagRemesas);

            $scope.PagRemesasGuardadas.array = $scope.ListadoRemesasGuardadas;
            ResetPaginacion($scope.PagRemesasGuardadas);

            $scope.Calcular();
        };
        //--Agregar Remesa
        //--Eliminar Remesa
        $scope.EliminarGuiaGuardada = function (item) {
            var index = 0;
            for (var i = 0; i < $scope.ListadoRemesasGuardadas.length; i++) {
                if ($scope.ListadoRemesasGuardadas[i].Numero == item.Numero) {
                    index = i;
                    break;
                }
            }
            var ResponBorraGuia = LegalizarRecaudoRemesasFactory.EliminarGuia({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: item.Numero,
                NumeroLegalizarRecaudo: item.NumeroLegalizarRecaudo,
                Sync: true
            });
            if (ResponBorraGuia.ProcesoExitoso == true) {
                $scope.ListadoRemesasGuardadas.splice(index, 1);
                $scope.PagRemesasGuardadas.array = $scope.ListadoRemesasGuardadas;
                var tmpPagAct = $scope.PagRemesasGuardadas.paginaActual;
                $scope.Calcular();
                ResetPaginacion($scope.PagRemesasGuardadas);
                if (tmpPagAct != 1) {
                    $scope.PagRemesasGuardadas.paginaActual = tmpPagAct - 1;
                    $scope.Siguiente($scope.PagRemesasGuardadas);
                }
            }
            else {
                ShowError(ResponBorraGuia.statusText);
            }
        };
        //--Eliminar Remesa
        //------------Remesas------------//
        $scope.Calcular = function () {
            $scope.Modelo.ValorContado = 0;
            $scope.Modelo.ValorContraEntrega = 0;
            for (var i = 0; i < $scope.ListadoRemesasGuardadas.length; i++) {
                var item = $scope.ListadoRemesasGuardadas[i];
                switch (item.FormaPago.Codigo) {
                    case CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CONTADO:
                        $scope.Modelo.ValorContado += item.Valor;
                        break;
                    case CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CONTRA_ENTREGA:
                        $scope.Modelo.ValorContraEntrega += item.Valor;
                        break;
                }
            }
            $scope.Modelo.ValorTotalLegalizar = $scope.Modelo.ValorContado + $scope.Modelo.ValorContraEntrega;
            $scope.MaskValores();
        };
        //--volver master
        $scope.VolverMaster = function () {
            if ($scope.Modelo.NumeroDocumento == undefined) {
                document.location.href = $scope.Master;
            }
            else {
                document.location.href = $scope.Master + '/' + $scope.Modelo.NumeroDocumento;
            }
        };
        $scope.AsignaUsuaModifica = function (item) {
            item.UsuarioModifica = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo };
        };
        //--Obtener
        function Obtener() {
            var filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Modelo.Numero
            };
            LegalizarRecaudoRemesasFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Deshabilitar = true;
                        $scope.Modelo.NumeroDocumento = response.data.Datos.NumeroDocumento;
                        $scope.Modelo.ValorContado = response.data.Datos.ValorContado;
                        $scope.Modelo.ValorContraEntrega = response.data.Datos.ValorContraEntregas;
                        $scope.Modelo.Observaciones = response.data.Datos.Observaciones;
                        $scope.Modelo.Oficina = response.data.Datos.Oficina;
                        $scope.MaskValores();

                        //--Valida remesas planilla vs remesas guardadas en detalle legalizacion
                        $scope.ListadoRemesasGuardadas = response.data.Datos.DetalleRemesas;
                        for (var i = 0; i < $scope.ListadoRemesasGuardadas.length; i++) {
                            $scope.ListadoRemesasGuardadas[i].MetodoPago = $linq.Enumerable().From($scope.ListadoMedioPago).First('$.Codigo ==' + $scope.ListadoRemesasGuardadas[i].MetodoPago.Codigo);
                            $scope.ListadoRemesasGuardadas[i].FechaPago = new Date($scope.ListadoRemesasGuardadas[i].FechaPago);
                        }

                        $scope.PagRemesasGuardadas.array = $scope.ListadoRemesasGuardadas;
                        $scope.PagRemesasGuardadas.ResultadoSinRegistros = '';
                        ResetPaginacion($scope.PagRemesasGuardadas);

                        $scope.Calcular();

                        if (response.data.Datos.Anulado == ESTADO_ANULADO) {
                            $scope.ListadoEstados.push({ Nombre: 'ANULADO', Codigo: 2 });
                            $scope.Modelo.Estado = $scope.ListadoEstados[2];
                            $scope.DeshabilitarActualizar = true;

                        }
                        else {
                            $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado);
                            if (response.data.Datos.Estado == ESTADO_DEFINITIVO) {
                                $scope.DeshabilitarActualizar = true;
                            }
                        }
                    }
                    else {
                        ShowError(response.statusText);
                    }
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                }, function (response) {
                    ShowError(response.statusText);
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                });
        }
        //--Obtener
        //--Guardar
        $scope.PeriodoValido = true
        function FindCierre() {
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumentos: { Codigo: CODIGO_TIPO_DOCUMENTO_LEGALIZACION_RECAUDO_REMESAS },
                Sync: true
            };
            var RCierreContable = CierreContableDocumentosFactory.Consultar(filtros)
            if (RCierreContable.ProcesoExitoso === true) {
                if (RCierreContable.Datos.length > 0) {
                    $scope.ListadoTipoDocumentos = RCierreContable.Datos;
                    $scope.ValidarCierre();
                }
            }
        }

        $scope.ValidarCierre = function () {
            if ($scope.ListadoTipoDocumentos.length > 0) {
                var anoFecha = $scope.Modelo.Fecha.getFullYear();
                var mesFecha = $scope.Modelo.Fecha.getMonth();
                for (var k = 0; k < $scope.ListadoTipoDocumentos.length; k++) {
                    var item = $scope.ListadoTipoDocumentos[k];
                    var mes = MascaraNumero(item.Mes.CampoAuxiliar2);
                    if (item.Ano === anoFecha && mes === (mesFecha + 1) && item.EstadoCierre.Codigo === 18002) {
                        ShowError('La fecha se encuentra en un mes y año con cierre contable');
                        $scope.Modelo.Fecha = '';
                        $scope.PeriodoValido = false
                        break;
                    }
                }
            }
        };

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            var Modelo = $scope.Modelo;
            if (Modelo.Fecha == undefined || Modelo.Fecha == '' || Modelo.Fecha == null) {
                $scope.MensajesError.push('Debe ingresar la fecha ');
                continuar = false;
            }
            var count = 0;
            count += $scope.ListadoRemesasGuardadas.length;
            for (var i = 0; i < $scope.ListadoRemesasGuardadas.length; i++) {
                var detalle = $scope.ListadoRemesasGuardadas[i];
                if (detalle.MetodoPago.Codigo == -1 || detalle.MetodoPago.Codigo == undefined || detalle.MetodoPago.Codigo == null) {
                    $scope.MensajesError.push('Debe ingresar un método de pago en las guías guardadas');
                    continuar = false;
                }
                if (detalle.FechaPago == undefined || detalle.FechaPago == '' || detalle.FechaPago == null) {
                    $scope.MensajesError.push('Debe ingresar fecha de pago en las guías guardadas');
                    continuar = false;
                }
                if (continuar == false)
                    break;
            }
            if (count == 0) {
                $scope.MensajesError.push('Debe existir al menos un detalle');
                continuar = false;
            }
            return continuar;
        }
        $scope.ConfirmacionGuardar = function () {
            if ($scope.DeshabilitarActualizar) {
                location.href = $scope.Master + '/' + $scope.Modelo.Planilla.Numero;
            } else {
                showModal('modalConfirmacionGuardar');
            }
        };
        $scope.ValidarGuardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {
                $scope.PeriodoValido = true
                FindCierre()
                if ($scope.PeriodoValido) {
                    PantallaBloqueo(Guardar, 'Generando Legalizacion...');
                }
                //Bloqueo Pantalla
            }
        };
        function Guardar() {
            var ListasRemesasLegalizar = [];
            for (var i = 0; i < $scope.ListadoRemesasGuardadas.length; i++) {
                var item = $scope.ListadoRemesasGuardadas[i];
                var guia = {
                    Numero: item.Numero,
                    NumeroDocumento: item.NumeroDocumento,
                    FormaPago: { Codigo: item.FormaPago.Codigo },
                    MetodoPago: { Codigo: item.MetodoPago.Codigo },
                    FechaPago: item.FechaPago,
                    Valor: item.Valor,
                    DocumentoPago: item.DocumentoPago,
                    Observaciones: item.Observaciones,
                    Legalizo: 1,
                    ResponsableLegalizarRecaudo: item.ResponsableLegalizarRecaudo
                };
                ListasRemesasLegalizar.push(guia);
            }

            var legalizacion = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_LEGALIZACION_RECAUDO_REMESAS,
                Numero: $scope.Modelo.Numero,
                Fecha: $scope.Modelo.Fecha,
                ValorContado: $scope.Modelo.ValorContado,
                ValorContraEntregas: $scope.Modelo.ValorContraEntrega,
                ValorTotalLegalizar: $scope.Modelo.ValorTotalLegalizar,
                Observaciones: $scope.Modelo.Observaciones,
                DetalleRemesas: ListasRemesasLegalizar,
                Oficina: $scope.Modelo.Oficina,
                Estado: $scope.Modelo.Estado.Codigo,
                UsuarioCrea: { Codigo: $scope.Modelo.UsuarioCrea.Codigo }
            };
            LegalizarRecaudoRemesasFactory.Guardar(legalizacion).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > CERO) {
                            if ($scope.Modelo.Numero == CERO) {
                                ShowSuccess('Se guardó la legalización número ' + response.data.Datos + '');
                            }
                            else {
                                ShowSuccess('Se modificó la legalización número ' + response.data.Datos + '');
                            }
                            $timeout(function () {
                                blockUI.stop();
                                blockUIConfig.autoBlock = false;
                                if (blockUI.state().blocking == true) { blockUI.reset(); }
                                location.href = $scope.Master + '/' + response.data.Datos;
                            }, 500);
                        }
                        else {
                            ShowError(response.MensajeOperacion);
                            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                        }
                    }
                    else {
                        ShowError(response.statusText);
                        $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                });
        }
        //--Guardar
        //--Limpiar Oficinas
        $scope.LimpiarOficina = function () {
            if ($scope.Filtro.Conductor != undefined && $scope.Filtro.Conductor != '' && $scope.Filtro.Conductor != null) {
                $scope.Filtro.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo === -1');
            }
        };
        //--Limpiar Conductor
        $scope.LimpiarConductor = function () {
            if ($scope.Filtro.Oficina != undefined && $scope.Filtro.Oficina != '' && $scope.Filtro.Oficina != null) {
                if ($scope.Filtro.Oficina.Codigo != -1) {
                    $scope.Filtro.Conductor = "";
                }
            }
        };
        //--Mascaras
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope);
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope);
        };
        $scope.MaskMayusGrid = function (item) {
            return MascaraMayus(item);
        };
        //--Mascaras
        //----------------------------Funciones Generales---------------------------------//
    }]);