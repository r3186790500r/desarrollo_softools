﻿Imports Newtonsoft.Json

Namespace Basico

    ''' <summary>
    ''' Clase <see cref=" TipoSubsistemas"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class TipoSubsistemas
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="TipoSubsistemas"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="TipoSubsistemas"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")

        End Sub
        ''' <summary>
        ''' Obtiene o establece el nombre del almacen
        ''' </summary>
        <JsonProperty>
        Public Property Nombre As String


#Region "Filtros de Búsqueda"

#End Region
    End Class
End Namespace
