﻿EncoExpresApp.controller("GestionarColorVehiculosCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'ColorVehiculosFactory', function ($scope, $routeParams, $timeout, $linq, blockUI, ColorVehiculosFactory) {

    $scope.Titulo = 'GESTIONAR COLORES VEHÍCULOS';
    $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Despachos' }, { Nombre: 'Colores Veículos' }, { Nombre: 'Gestionar' }];
    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

    try {
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_COLOR_VEHICULOS);
    } catch (e) {
        ShowError('No tiene permiso para visualizar esta pagina')
        document.location.href = '#'
    }

    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

    $scope.Modelo = {
        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        Codigo: 0,
        CodigoAlterno: 0,
        Nombre: '',
        Estado: 0
    }

    $scope.ListadoEstados = [
        { Codigo: 0, Nombre: "INACTIVO" },
        { Codigo: 1, Nombre: "ACTIVO" },
    ]

    /* Obtener parametros*/
    if ($routeParams.Codigo > 0) {
        $scope.Modelo.Codigo = $routeParams.Codigo;
        Obtener();
    }
    else {
        $scope.Modelo.Codigo = 0;
        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
    }

    /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
    function Obtener() {
        blockUI.start('Cargando color vehículo código No. ' + $scope.Modelo.Codigo);

        $timeout(function () {
            blockUI.message('Cargando color vehículo Código No.' + $scope.Modelo.Codigo);
        }, 200);

        filtros = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: $scope.Modelo.Codigo,
        };

        blockUI.delay = 1000;
        ColorVehiculosFactory.Obtener(filtros).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {

                    $scope.Modelo.Codigo = response.data.Datos.Codigo;
                    $scope.Modelo.CodigoAlterno = response.data.Datos.CodigoAlterno;
                    $scope.Modelo.Nombre = response.data.Datos.Nombre;
                    $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado)

                }
                else {
                    ShowError('No se logro consultar el color vehículo código ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                    document.location.href = '#!ConsultarColorVehiculos';
                }
            }, function (response) {
                ShowError(response.statusText);
                document.location.href = '#!ConsultarColorVehiculos';
            });

        blockUI.stop();
    };
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

    $scope.ConfirmacionGuardar = function () {
        showModal('modalConfirmacionGuardar');
    };

    $scope.Guardar = function () {
        closeModal('modalConfirmacionGuardar');
        if (DatosRequeridos()) {

            //Estado
            $scope.Modelo.Estado = $scope.ModalEstado.Codigo;

            ColorVehiculosFactory.Guardar($scope.Modelo).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Modelo.Codigo === 0) {
                                ShowSuccess('Se guardó el color vehículo "' + $scope.Modelo.Nombre + '"');
                                location.href = '#!ConsultarColorVehiculos/' + response.data.Datos;
                            }
                            else {
                                ShowSuccess('Se modificó el color vehículo "' + $scope.Modelo.Nombre + '"');
                                location.href = '#!ConsultarColorVehiculos/' + $scope.Modelo.Codigo;
                            }
                        }
                        else {
                            ShowError('No se pudo guardar el color vehículo, por favor verifique que el código que esta intentando ingresar no corresponda a una color vehículo ya creado');
                        }
                    }
                    else {
                        ShowError('No se pudo guardar el color vehículo, por favor verifique que el código que esta intentando ingresar no corresponda a una color vehículo ya creado');
                    }
                }, function (response) {
                    if (response.statusText.includes('IX_Color_Vehiculos_Nombre')) {
                        ShowError('No se pudo guardar, el nombre ingresado ya está siendo utilizado')
                    } else if (response.statusText.includes('IX_Color_Vehiculos_Codigo_Alterno')) {
                        ShowError('No se pudo guardar, el codigo alterno ingresado ya está siendo utilizado')
                    } else {
                        ShowError(response.statusText);
                    }
                });
        }
    };


    function DatosRequeridos() {
        $scope.MensajesError = [];
        var continuar = true;

        if ($scope.Modelo.Nombre === undefined || $scope.Modelo.Nombre === '') {
            $scope.MensajesError.push('Debe ingresar el nombre del color vehículo');
            continuar = false;
        }
        return continuar;
    }

    $scope.VolverMaster = function () {
        document.location.href = '#!ConsultarColorVehiculos/' + $scope.Modelo.Codigo;
    };
    $scope.MaskMayus = function () {
        try { $scope.Modelo.Nombre = $scope.Modelo.Nombre.toUpperCase() } catch (e) { }
    };
    $scope.MaskMayus = function (option) {
        MascaraMayusGeneral($scope)
    };
}]);