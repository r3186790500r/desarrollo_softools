﻿PRINT 'gsp_limpiar_t_estudio_seguridad_documentos'
GO
DROP PROCEDURE gsp_limpiar_t_estudio_seguridad_documentos
GO
CREATE PROCEDURE gsp_limpiar_t_estudio_seguridad_documentos 
(      
@par_EMPR_Codigo SMALLINT,      
@par_USUA_Codigo SMALLINT,    
@par_Numero NUMERIC = NULL      
)      
AS      
BEGIN      
 DELETE T_Estudio_Seguridad_Documentos       
 WHERE       
 EMPR_Codigo =  @par_EMPR_Codigo      
 AND USUA_Codigo = @par_USUA_Codigo      
 AND ENES_Numero = ISNULL(@par_Numero, 0)    

 SELECT Codigo = @@ROWCOUNT      
   
END      
GO