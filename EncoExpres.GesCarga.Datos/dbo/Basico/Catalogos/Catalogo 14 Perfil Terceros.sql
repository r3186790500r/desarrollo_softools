﻿Print 'Catalogo 14 Perfil Terceros'
GO
DELETE Valor_Catalogos WHERE CATA_Codigo = 14
GO
DELETE Configuracion_Catalogos WHERE CATA_Codigo = 14
GO
DELETE Catalogos WHERE Codigo = 14
GO
------------------------------------------CATALOGOS------------------------------------------------------
INSERT INTO Catalogos VALUES (1,14,'PETE','Perfil Terceros',0,2,1)
GO
-----------------------------------------VALOR CATALOGOS--------------------------------------------------
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1, USUA_Codigo_Crea,Fecha_Crea) 
VALUES 
(1,14,1400,UPPER('(No Aplica)'),1,GETDATE()),
(1,14,1401,UPPER('Cliente'),1,GETDATE()),
(1,14,1402,UPPER('Aseguradora'),1,GETDATE()),
(1,14,1403,UPPER('Conductor'),1,GETDATE()),
(1,14,1404,UPPER('Destinatario'),1,GETDATE()),
(1,14,1405,UPPER('Empleado'),1,GETDATE()),
(1,14,1406,UPPER('Empresa Afiliadora'),1,GETDATE()),
(1,14,1407,UPPER('Empresa Transportadora'),1,GETDATE()),
(1,14,1408,UPPER('Propietario'),1,GETDATE()), 
(1,14,1409,UPPER('Proveedor'),1,GETDATE()),
(1,14,1410,UPPER('Remitente'),1,GETDATE()),
(1,14,1411,UPPER('Seguridad Social'),1,GETDATE()),
(1,14,1412,UPPER('Tenedor'),1,GETDATE())
GO

-----------------------------------------CONFIGURACION CATALOGOS------------------------------------------
INSERT INTO configuracion_catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
VALUES 
(1,14,1,'Codigo',1,1,1,1,0,0,0,0,0,1,0),
(1,14,2,'Nombre',1,1,1,1,0,0,0,0,0,1,0)
GO 