﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Proveedores
Imports System.Transactions

Namespace Proveedores

    Public Class RepositorioPlanillasDespachosProveedores
        Inherits RepositorioBase(Of PlanillasDespachosProveedores)

        Public Overrides Function Insertar(entidad As PlanillasDespachosProveedores) As Long
            'Throw New NotImplementedException()
            Dim inserto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Orden_Servicio", entidad.OrdenServicio)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
                    conexion.AgregarParametroSQL("@par_Numero_Manifiesto", entidad.Manifiesto.NumeroDocumento)
                    conexion.AgregarParametroSQL("@par_Numero_Remesa", entidad.Remesa.NumeroDocumento)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
                    conexion.AgregarParametroSQL("@par_Fecha_Remesa", entidad.Remesa.Fecha, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_Fecha_Manifiesto", entidad.Manifiesto.Fecha, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_Codigo_Cliente", entidad.Cliente.Codigo)
                    conexion.AgregarParametroSQL("@par_Documento_Cliente", entidad.DocumentoCliente)
                    conexion.AgregarParametroSQL("@par_Ruta", entidad.Ruta.Codigo)
                    conexion.AgregarParametroSQL("@par_Vehiculo", entidad.Vehiculo)
                    conexion.AgregarParametroSQL("@par_SEMI_Codigo", entidad.Semirremolque)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", entidad.IdentificacionConductor)
                    conexion.AgregarParametroSQL("@par_Nombre_Conductor", entidad.NombreConductor)
                    conexion.AgregarParametroSQL("@par_Transportador", entidad.Transportador.Codigo)
                    conexion.AgregarParametroSQL("@par_PRTR_Codigo", entidad.Producto.Codigo)
                    conexion.AgregarParametroSQL("@par_Cantidad", entidad.Cantidad)
                    conexion.AgregarParametroSQL("@par_Peso", entidad.Peso)
                    conexion.AgregarParametroSQL("@par_TTTC_Codigo", entidad.TipoTarifa.Codigo)
                    conexion.AgregarParametroSQL("@par_TATC_Codigo", entidad.Tarifa.Codigo)
                    conexion.AgregarParametroSQL("@par_Flete_Transportador", entidad.FleteTransportador, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Anticipo_Transportador", entidad.AnticipoTransportador, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Total_Transportador", entidad.TotalTransportador, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Total_Flete", entidad.TotalFlete, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Pagar", entidad.ValorPagar, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.CodigoUsuarioCrea.Codigo)


                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_planillas_Despachos_Proveedores")
                    While resultado.Read

                        entidad.NumeroDocumento = resultado.Item("NumeroDocumento").ToString
                    End While
                    resultado.Close()

                    If inserto Then
                        transaccion.Complete()
                    Else
                        entidad.NumeroDocumento = Cero
                    End If

                End Using
            End Using
            Return entidad.NumeroDocumento
        End Function

        Public Overrides Function Modificar(entidad As PlanillasDespachosProveedores) As Long
            Dim inserto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                    conexion.AgregarParametroSQL("@par_Orden_Servicio", entidad.OrdenServicio)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
                    conexion.AgregarParametroSQL("@par_Numero_Manifiesto", entidad.Manifiesto.NumeroDocumento)
                    conexion.AgregarParametroSQL("@par_Numero_Remesa", entidad.Remesa.NumeroDocumento)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
                    conexion.AgregarParametroSQL("@par_Fecha_Remesa", entidad.Remesa.Fecha, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_Fecha_Manifiesto", entidad.Manifiesto.Fecha, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_Codigo_Cliente", entidad.Cliente.Codigo)
                    conexion.AgregarParametroSQL("@par_Documento_Cliente", entidad.DocumentoCliente)
                    conexion.AgregarParametroSQL("@par_Ruta", entidad.Ruta.Codigo)
                    conexion.AgregarParametroSQL("@par_Vehiculo", entidad.Vehiculo)
                    conexion.AgregarParametroSQL("@par_SEMI_Codigo", entidad.Semirremolque)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", entidad.IdentificacionConductor)
                    conexion.AgregarParametroSQL("@par_Nombre_Conductor", entidad.NombreConductor)
                    conexion.AgregarParametroSQL("@par_Transportador", entidad.Transportador.Codigo)
                    conexion.AgregarParametroSQL("@par_PRTR_Codigo", entidad.Producto.Codigo)
                    conexion.AgregarParametroSQL("@par_Cantidad", entidad.Cantidad)
                    conexion.AgregarParametroSQL("@par_Peso", entidad.Peso)
                    conexion.AgregarParametroSQL("@par_TTTC_Codigo", entidad.TipoTarifa.Codigo)
                    conexion.AgregarParametroSQL("@par_TATC_Codigo", entidad.Tarifa.Codigo)
                    conexion.AgregarParametroSQL("@par_Flete_Transportador", entidad.FleteTransportador, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Anticipo_Transportador", entidad.AnticipoTransportador, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Total_Transportador", entidad.TotalTransportador, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Total_Flete", entidad.TotalFlete, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Pagar", entidad.ValorPagar, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.CodigoUsuarioCrea.Codigo)



                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_planillas_Despachos_Proveedores")
                    While resultado.Read

                        entidad.NumeroDocumento = resultado.Item("NumeroDocumento").ToString
                    End While
                    resultado.Close()

                    If inserto Then
                        transaccion.Complete()
                    Else
                        entidad.NumeroDocumento = Cero
                    End If

                End Using
            End Using
            Return entidad.NumeroDocumento
        End Function

        Public Overrides Function Consultar(filtro As PlanillasDespachosProveedores) As IEnumerable(Of PlanillasDespachosProveedores)
            Dim lista As New List(Of PlanillasDespachosProveedores)
            Dim item As PlanillasDespachosProveedores


            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If Not IsNothing(filtro) Then
                    If filtro.Numero Then
                        If filtro.Numero > 0 Then
                            conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                        End If
                    End If

                    If filtro.FechaInicial > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                    End If
                    If filtro.FechaFinal > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                    End If

                    If Not IsNothing(filtro.Cliente) Then
                        If Not IsNothing(filtro.Cliente.Nombre) Then
                            conexion.AgregarParametroSQL("@par_Codigo_Cliente", filtro.Cliente.Nombre)
                        End If
                    End If

                    If Not IsNothing(filtro.Transportador) Then
                        If filtro.Transportador.Nombre <> "" And Not IsNothing(filtro.Transportador.Nombre) Then
                            conexion.AgregarParametroSQL("@par_Transportador", filtro.Transportador.Nombre)
                        End If

                    End If
                    If Not IsNothing(filtro.Vehiculo) Then
                        conexion.AgregarParametroSQL("@par_VEHI_Placa", filtro.Vehiculo)
                    End If

                    If Not IsNothing(filtro.Liquidacion) Then
                        If filtro.Liquidacion = 1 Then
                            conexion.AgregarParametroSQL("@par_Liquidacion", filtro.Liquidacion)
                        End If

                    End If

                    If filtro.Estado > -1 And filtro.Estado < 2 Then
                        conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                        conexion.AgregarParametroSQL("@par_Anulado", 0)
                    End If

                    If filtro.Estado >= 2 Then
                        conexion.AgregarParametroSQL("@par_Anulado", 1)
                    End If


                    'Filtro Paginación:

                    If Not IsNothing(filtro.RegistrosPagina) Then
                        If filtro.RegistrosPagina > 0 Then
                            conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                        End If
                    End If

                    If Not IsNothing(filtro.Pagina) Then
                        If filtro.Pagina > 0 Then
                            conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                        End If
                    End If


                End If

                Dim resultado As IDataReader

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_planilla_despachos_Proveedores")


                While resultado.Read
                    item = New PlanillasDespachosProveedores(resultado)
                    lista.Add(item)
                End While

                conexion.CloseConnection()


            End Using
            Return lista
        End Function

        Public Overrides Function Obtener(filtro As PlanillasDespachosProveedores) As PlanillasDespachosProveedores
            Dim item As New PlanillasDespachosProveedores

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_planilla_despachos_proveedores")

                While resultado.Read
                    filtro.Numero = resultado.Item("Numero")
                    item = New PlanillasDespachosProveedores(resultado)
                End While

                conexion.CloseConnection()
                resultado.Close()

            End Using

            Return item
        End Function

        Public Function Anular(entidad As PlanillasDespachosProveedores) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Anula", entidad.UsuarioAnula.Codigo)
                conexion.AgregarParametroSQL("@par_Causa_Anula", entidad.CausaAnulacion)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_anular_planilla_despachos_Proveedores")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                    'anulo = resultado.Item("Numero")
                End While

            End Using

            Return anulo
        End Function
    End Class
End Namespace

