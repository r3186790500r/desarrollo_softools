﻿PRINT 'gsp_insertar_listas_referencias'
GO
DROP PROCEDURE gsp_insertar_listas_referencias
GO
CREATE PROCEDURE gsp_insertar_listas_referencias    
(@par_EMPR_Codigo SMALLINT,    
@par_Numero_Estudio NUMERIC,    
@par_Tipo_Referencia NUMERIC,    
@par_Observaciones VARCHAR(1000)    
)    
AS   
BEGIN    
    
INSERT INTO [dbo].[Detalle_Referencias_Estudio_Seguridad]    
           ([EMPR_Codigo]    
           ,[ENES_Numero]
		   ,[CATA_TRES_Codigo]
		   ,[Observaciones]    
         )    
     VALUES    
           (@par_EMPR_Codigo    
           ,@par_Numero_Estudio    
           ,@par_Tipo_Referencia    
           ,@par_Observaciones)    
END  
GO