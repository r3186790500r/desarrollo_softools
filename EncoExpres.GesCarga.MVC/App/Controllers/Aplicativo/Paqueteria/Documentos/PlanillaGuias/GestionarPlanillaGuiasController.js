﻿EncoExpresApp.controller("GestionarPlanillaGuiasCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'TarifarioComprasFactory', 'TarifaTransportesFactory',
    'TipoTarifaTransportesFactory', 'RutasFactory', 'ValorCatalogosFactory', 'TercerosFactory', 'TipoLineaNegocioTransportesFactory', 'ProductoTransportadosFactory', 'CiudadesFactory',
    'RemesasFactory', 'VehiculosFactory', 'PlanillaGuiasFactory', 'ImpuestosFactory', 'OficinasFactory', 'SemirremolquesFactory', 'ManifiestoFactory',
    'RemesaGuiasFactory', 'blockUIConfig', 'PlanificacionDespachosFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, TarifarioComprasFactory, TarifaTransportesFactory,
        TipoTarifaTransportesFactory, RutasFactory, ValorCatalogosFactory, TercerosFactory, TipoLineaNegocioTransportesFactory, ProductoTransportadosFactory, CiudadesFactory,
        RemesasFactory, VehiculosFactory, PlanillaGuiasFactory, ImpuestosFactory, OficinasFactory, SemirremolquesFactory, ManifiestoFactory,
        RemesaGuiasFactory, blockUIConfig, PlanificacionDespachosFactory) {
        $scope.Titulo = 'GESTIONAR PLANILLA DESPACHOS';
        $scope.MapaSitio = [{ Nombre: 'Despachos' }, { Nombre: 'Paqueteria' }, { Nombre: 'Planilla Despachos' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PLANILLA_DESPACHOS_PAQUETERIA); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        if ($scope.Sesion.UsuarioAutenticado.ManejoEngancheRemesasMasivoPaqueteria) {
            $scope.RemesaPadre = true
        }
        $scope.AplicaImpuestos = false

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,
            Codigo: CERO,
            CodigoAlterno: CERO,
            Nombre: '',
            Planilla: {
                Numero: 0,
                Fecha: new Date(),
                FechaSalida: new Date(),
                DetalleTarifarioCompra: {},
                CalcularContraentregas: true,
            },
            TipoDocumento: 130,
            Remesa: { Cliente: { NombreCompleto: '' }, Ruta: {} },
        };
        $scope.Planilla = {};

        $scope.CargarRemesas = {
            FechaInicial: new Date(),
            FechaFinal: new Date(),
            NumeroInicial: '',
            CiudadRemitente: '',
            CiudadDestinatario: '',
            Cliente: '',
            NumeroDocumentoCliente: '',
            Remitente: ''
        };


        $scope.ListaAuxiliares = [];
        $scope.Filtro = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            CodigoOficina: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
            Codigo: CERO,
            CodigoAlterno: CERO,
            Nombre: '',
            Estado: CERO,
            Remesa: {
                DetalleTarifarioVenta: {},
                Remitente: { Direccion: '' },
                Detalle: { Destinatario: { Direccion: '' }, }
            },
            EstadoRemesaPaqueteria: { Codigo: 6001 }

        }

        $scope.Filtro2 = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            CodigoOficina: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
            Codigo: CERO,
            CodigoAlterno: CERO,
            Nombre: '',
            Estado: CERO,
            Remesa: {
                DetalleTarifarioVenta: {},
                Remitente: { Direccion: '' },
                Detalle: { Destinatario: { Direccion: '' }, }
            },
            EstadoRemesaPaqueteria: { Codigo: 6001 }

        };
        $scope.DetallesAuxiliares = [];
        $scope.ListadoEstados = [
            { Nombre: 'DEFINITIVO', Codigo: ESTADO_DEFINITIVO },
            { Nombre: 'BORRADOR', Codigo: ESTADO_BORRADOR }
        ];

        $scope.ListadoGuiaGuardadas = [];
        $scope.ListadoRemesasContraentregas = [];
        $scope.ListadoTarifas = [];
        //--------------------Funciones AutoComplete
        //----Ciudades
        $scope.ListaCiudades = [];
        $scope.AutocompleteCiudades = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    });
                    $scope.ListaCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListaCiudades);
                }
            }
            return $scope.ListaCiudades;
        };
        //----Ciudades
        //----Cliente
        $scope.ListaCliente = [];
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CLIENTE, ValorAutocomplete: value, Sync: true })
                    $scope.ListaCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListaCliente);
                }
            }
            return $scope.ListaCliente;
        };
        //----Cliente
        //----Remitente
        $scope.ListaRemitente = [];
        $scope.AutocompleteRemitente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_REMITENTE, ValorAutocomplete: value, Sync: true })
                    $scope.ListaRemitente = ValidarListadoAutocomplete(Response.Datos, $scope.ListaRemitente);
                }
            }
            return $scope.ListaRemitente;
        };
        //----Remitente
        //----Conductor
        $scope.ListadoConductores = [];
        $scope.AutocompleteConductor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CONDUCTOR, ValorAutocomplete: value, Sync: true })
                    $scope.ListadoConductores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoConductores);
                }
            }
            return $scope.ListadoConductores;
        };
        //----Conductor
        //----Funcionario
        $scope.ListaFuncionario = [];
        $scope.AutocompleteFuncionario = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_EMPLEADO,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListaFuncionario = ValidarListadoAutocomplete(Response.Datos, $scope.ListaFuncionario);
                }
            }
            return $scope.ListaFuncionario;
        };
        //----Funcionario
        $scope.ListadoVehiculos = [];
        $scope.AutocompleteVehiculos = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListadoVehiculos = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoVehiculos);
                }
            }
            return $scope.ListadoVehiculos;
        };
        //--Filtro Rutas
        $scope.AutocompleteRutasFiltradas = function (value) {
            var tmparr = [];
            if (value.length > 0 && $scope.LustadoRutasFiltrado != undefined) {
                for (var i = 0; i < $scope.LustadoRutasFiltrado.length; i++) {
                    var NombreRuta = $scope.LustadoRutasFiltrado[i].Nombre;
                    if (NombreRuta.likeFind(value + "%")) {
                        tmparr.push($scope.LustadoRutasFiltrado[i]);
                    }
                }
            }
            return tmparr;
        };
        //--Filtro Rutas
        //----Semiremolques
        $scope.ListaSemirremolque = [];
        $scope.AutoCompleteSemiRemolque = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = SemirremolquesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    });
                    $scope.ListaSemirremolque = ValidarListadoAutocomplete(Response.Datos, $scope.ListaSemirremolque);
                }
            }
            return $scope.ListaSemirremolque;
        };
        //----Semiremolques
        //--------------------Funciones AutoComplete

        $scope.CargarDatosFunciones = function () {
            $scope.Modelo.Planilla.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + ESTADO_BORRADOR);
            $scope.ListadoRutas = RutasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_DEFINITIVO, Sync: true }).Datos;

            /*Cargar el combo de tipo entrega*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_ENTREGA_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoEntregaRemesaPaqueteria = [];
                        if (response.data.Datos.length > CERO) {
                            $scope.ListadoTipoEntregaRemesaPaqueteria = response.data.Datos;

                            $scope.Filtro.TipoEntregaRemesaPaqueteria = $scope.ListadoTipoEntregaRemesaPaqueteria[CERO]

                        }
                        else {
                            $scope.ListadoTipoEntregaRemesaPaqueteria = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de tipo transporte*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_TRANSPORTE_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoTransporteRemsaPaqueteria = [];
                        if (response.data.Datos.length > CERO) {
                            $scope.ListadoTipoTransporteRemsaPaqueteria = response.data.Datos;

                            $scope.Filtro.TipoTransporteRemsaPaqueteria = $scope.ListadoTipoTransporteRemsaPaqueteria[CERO]

                        }
                        else {
                            $scope.ListadoTipoTransporteRemsaPaqueteria = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de tipo despacho*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_DESPACHO_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoDespachoRemesaPaqueteria = [];
                        if (response.data.Datos.length > CERO) {
                            $scope.ListadoTipoDespachoRemesaPaqueteria = response.data.Datos;

                            $scope.Filtro.TipoDespachoRemesaPaqueteria = $scope.ListadoTipoDespachoRemesaPaqueteria[CERO]

                        }
                        else {
                            $scope.ListadoTipoDespachoRemesaPaqueteria = []
                        }
                    }
                }, function (response) {
                });

            /*Cargar el combo de tipo servicio*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_SERVICIO_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoServicioRemesaPaqueteria = [];
                        if (response.data.Datos.length > CERO) {
                            $scope.ListadoTipoServicioRemesaPaqueteria = response.data.Datos;

                            $scope.Filtro.TipoServicioRemesaPaqueteria = $scope.ListadoTipoServicioRemesaPaqueteria[CERO]

                        }
                        else {
                            $scope.ListadoTipoServicioRemesaPaqueteria = []
                        }
                    }
                }, function (response) {
                });

            ProductoTransportadosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoProductoTransportados = []
                        if (response.data.Datos.length > CERO) {
                            $scope.ListadoProductoTransportados = response.data.Datos;
                        }
                        else {
                            $scope.ListadoProductoTransportados = [];
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
            /*Cargar el combo de TipoLineaNegocioTransportes*/
            TipoLineaNegocioTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoLineaNegocioTransportes = [];
                        $scope.ListadoTipoLineaNegocioTransportesInicial = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoLineaNegocioTransportesInicial = response.data.Datos;
                            for (var i = 0; i < $scope.ListadoTipoLineaNegocioTransportesInicial.length; i++) {
                                if ($scope.ListadoTipoLineaNegocioTransportesInicial[i].LineaNegocioTransporte !== undefined && $scope.ListadoTipoLineaNegocioTransportesInicial[i].LineaNegocioTransporte !== null) {
                                    if ($scope.ListadoTipoLineaNegocioTransportesInicial[i].LineaNegocioTransporte.Codigo == CODIGO_LINEA_NEGOCIO_TRANPSORTE_CARGA_PAQUETERIA) {
                                        $scope.ListadoTipoLineaNegocioTransportes.push($scope.ListadoTipoLineaNegocioTransportesInicial[i])
                                    }
                                }
                            }
                            $scope.ListadoTipoLineaNegocioTransportesInicial = angular.copy($scope.ListadoTipoLineaNegocioTransportes)
                            $scope.ListadoTipoLineaNegocioTransportes = []
                        }
                        else {
                            $scope.ListadoTipoLineaNegocioTransportes = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de TarifaTransportes*/
            TarifaTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTarifaTransportes = [];
                        $scope.ListadoTarifaTransportesInicial = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTarifaTransportesInicial = response.data.Datos;
                        }
                        else {
                            $scope.ListadoTarifaTransportesInicial = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de TipoTarifaTransportes*/
            TipoTarifaTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoTarifaTransportes = [];
                        $scope.ListadoTipoTarifaTransportesInicial = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoTarifaTransportesInicial = response.data.Datos;
                        }
                        else {
                            $scope.ListadoTipoTarifaTransportesInicial = []
                        }
                    }
                }, function (response) {
                });
            //--Carga Oficinas
            $scope.ListadoOficinas = [];
            $scope.ListadoOficinas = OficinasFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Estado: ESTADO_DEFINITIVO,
                Sync: true
            }).Datos;
            $scope.CargarRemesas.OficinaOrigen = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
            //--Carga Oficinas
        };
        $scope.CargarImpuestos = function (Ruta) {
            if (Ruta.Codigo > 0 && Ruta !== undefined && Ruta !== null && Ruta !== "") {
                if (Ruta.Codigo > 0) {
                    var response = ImpuestosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Estado: 1,
                        CodigoTipoDocumento: 130,
                        CodigoCiudad: Ruta.CiudadOrigen.Codigo,
                        AplicaTipoDocumento: 1,
                        Sync: true
                    });
                    if (response.ProcesoExitoso == true) {
                        $scope.ListadoImpuestos = [];
                        $scope.ListadoImpuestosFiltrado = [];
                        if (response.Datos.length > CERO) {
                            response.Datos.forEach(function (item) {
                                $scope.ListadoImpuestos.push({
                                    Codigo: item.Codigo,
                                    Nombre: item.Nombre,
                                    Operacion: item.Operacion,
                                    ValorBase: item.valor_base,
                                    ValorTarifa: item.Valor_tarifa,
                                    ValorImpuesto: CERO
                                });
                            });
                            $scope.ListadoImpuestosFiltrado = angular.copy($scope.ListadoImpuestos);
                            //---Inicializa valor impuesto vista 
                            $scope.ListadoImpuestosFiltrado.forEach(function (item) {
                                item.ValorBase = 0;
                                item.ValorImpuesto = 0;
                            });
                            //---Inicializa valor impuesto vista

                            //$scope.ListadoImpuestos = response.Datos;
                            //try {
                            //    if ($scope.Modelo.Planilla.DetalleImpuesto !== undefined && $scope.Modelo.Planilla.DetalleImpuesto !== null && $scope.Modelo.Planilla.DetalleImpuesto !== []) {
                            //        $scope.ListadoImpuestosFiltrado = $scope.Modelo.Planilla.DetalleImpuesto;
                            //    }
                            //    else {
                            //        $scope.ListadoImpuestosFiltrado = [];
                            //        if ($scope.ListadoImpuestos.length > 0) {
                            //            for (var i = 0; i < $scope.ListadoImpuestos.length; i++) {
                            //                var impuesto = {
                            //                    Nombre: $scope.ListadoImpuestos[i].Nombre,
                            //                    CodigoImpuesto: $scope.ListadoImpuestos[i].Codigo,
                            //                    ValorTarifa: $scope.ListadoImpuestos[i].Valor_tarifa,
                            //                    ValorBase: $scope.ListadoImpuestos[i].valor_base,
                            //                    ValorImpuesto: 0
                            //                };
                            //                $scope.ListadoImpuestosFiltrado.push(impuesto);
                            //            }
                            //        }
                            //    }
                            //} catch (e) {
                            //    $scope.ListadoImpuestosFiltrado = []
                            //    if ($scope.ListadoImpuestos.length > 0) {
                            //        for (var i = 0; i < $scope.ListadoImpuestos.length; i++) {
                            //            var impuesto = {
                            //                Nombre: $scope.ListadoImpuestos[i].Nombre,
                            //                CodigoImpuesto: $scope.ListadoImpuestos[i].Codigo,
                            //                ValorTarifa: $scope.ListadoImpuestos[i].Valor_tarifa,
                            //                ValorBase: $scope.ListadoImpuestos[i].valor_base,
                            //                ValorImpuesto: 0
                            //            };
                            //            $scope.ListadoImpuestosFiltrado.push(impuesto);
                            //        }
                            //    }
                            //}
                            $scope.Calcular();
                        }
                    }
                    else {
                        ShowError(response.statusText);
                    }
                }
            }
        };
        $scope.FiltrarTarifas = function (Ruta) {
            if (Ruta.Codigo > 0 && Ruta !== undefined && Ruta !== null && Ruta !== "") {
                if (Ruta.Codigo > 0) {
                    $scope.ListaTarifas = [];
                    $scope.ListadoTipoTarifas = [];
                    for (var i = 0; i < $scope.ListadoTarifas.length; i++) {
                        var item = $scope.ListadoTarifas[i]
                        if (item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_MASIVO || item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_SEMIMASIVO) {
                            if (Ruta.Codigo == item.Ruta.Codigo) {
                                //if (item.TipoLineaNegocioTransportes.Codigo == TIPO_LINEA_NEGOCIO_CARGA_MASIVO_NACIONAL || item.TipoLineaNegocioTransportes.Codigo == TIPO_LINEA_NEGOCIO_CARGA_SEMIMASIVO_NACIONAL) {
                                if ($scope.ListaTarifas.length == 0) {
                                    $scope.ListaTarifas.push(item.TipoTarifaTransportes.TarifaTransporte)
                                }
                                else {
                                    var cont = 0
                                    for (var j = 0; j < $scope.ListaTarifas.length; j++) {
                                        if ($scope.ListaTarifas[j].Codigo == item.TipoTarifaTransportes.TarifaTransporte.Codigo) {
                                            cont++
                                        }
                                    }
                                    if (cont == 0) {
                                        $scope.ListaTarifas.push(item.TipoTarifaTransportes.TarifaTransporte)
                                    }
                                }
                                item.Codigo = item.TipoTarifaTransportes.Codigo
                                $scope.ListadoTipoTarifas.push(item)
                                //}
                            }
                        }
                    }
                    //$scope.Modelo.Planilla.TarifarioCompra.Tarifas.forEach(function (item) {

                    //});
                    //if ($scope.Sesion.UsuarioAutenticado.ManejoPorcentajeAfiliado == false) {
                    if ($scope.Sesion.UsuarioAutenticado.ManejoHabilitarLiquidacionPlanillaDespachos == false) {
                        if ($scope.ListaTarifas.length == 0 && $scope.ListadoTipoTarifas == 0) {
                            ShowError('La ruta ingresada no se encuentra asociada al tarifario de compras del tenedor ' + $scope.Modelo.Planilla.Vehiculo.Tenedor.NombreCompleto);
                        }
                        else {
                            try {
                                $scope.Modelo.Planilla.TarifaTransportes = $linq.Enumerable().From($scope.ListaTarifas).First('$.Codigo ==' + $scope.Modelo.Planilla.TarifaTransportes.Codigo);
                                $scope.FiltrarTipoTarifa()
                            } catch (e) {
                                try {
                                    $scope.Modelo.Planilla.TarifaTransportes = $scope.ListaTarifas[0]
                                    $scope.FiltrarTipoTarifa()
                                } catch (e) {

                                }

                            }
                        }
                    }
                }
            }
        }
        $scope.FiltrarTipoTarifa = function () {
            $scope.ListadoTipoTarifa = []
            for (var i = 0; i < $scope.ListadoTipoTarifas.length; i++) {
                var item = $scope.ListadoTipoTarifas[i]
                if ($scope.Modelo.Planilla.TarifaTransportes.Codigo == item.TipoTarifaTransportes.TarifaTransporte.Codigo) {
                    $scope.ListadoTipoTarifa.push(item)
                }
            }
            try {
                $scope.TipoTarifaTransportes = $linq.Enumerable().From($scope.ListadoTipoTarifa).First('$.Codigo ==' + $scope.Modelo.Planilla.TipoTarifaTransportes.Codigo);
                $scope.Modelo.Planilla.TipoTarifaTransportes = undefined
            } catch (e) {
                $scope.TipoTarifaTransportes = $scope.ListadoTipoTarifa[0]
            }
            try {
                $scope.Calcular()
            } catch (e) {

            }
        }
        /*Consultar Tarifario Tenedor*/
        $scope.ObtenerInformacionTenedor = function () {
            try {
                $scope.Modelo.Planilla.Semirremolque = $linq.Enumerable().From($scope.ListaRemolque).First('$.Codigo ==' + $scope.Modelo.Planilla.Vehiculo.Semirremolque.Codigo);

            } catch (e) {

            }
            //if ($scope.Sesion.UsuarioAutenticado.ManejoPorcentajeAfiliado) {
            if ($scope.Sesion.UsuarioAutenticado.ManejoHabilitarLiquidacionPlanillaDespachos) {
                $scope.LustadoRutasFiltrado = [];
                for (var i = 0; i < $scope.ListadoRutas.length; i++) {
                    $scope.LustadoRutasFiltrado.push($scope.ListadoRutas[i]);
                }
                try {
                    $scope.Modelo.Planilla.Peso = $scope.pesotemp
                    $scope.Modelo.Planilla.Ruta = $linq.Enumerable().From($scope.LustadoRutasFiltrado).First('$.Codigo ==' + $scope.Modelo.Planilla.Ruta.Codigo);
                    $scope.CargarImpuestos($scope.Modelo.Planilla.Ruta);
                } catch (e) {

                }
            }
            else {
                if ($scope.Modelo.Planilla.Vehiculo.Tenedor !== undefined) {
                    if ($scope.Modelo.Planilla.Vehiculo.Tenedor.Codigo > 0) {
                        $scope.Modelo.Planilla.Vehiculo.Tenedor.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
                        var objter = angular.copy($scope.Modelo.Planilla.Vehiculo.Tenedor);
                        objter.Sync = true;
                        var response = TercerosFactory.Obtener(objter);
                        if (response.ProcesoExitoso == true) {
                            if (response.Datos.Codigo > 0) {
                                if (response.Datos.Proveedor != undefined) {
                                    if (response.Datos.Proveedor.Tarifario !== undefined) {
                                        if (response.Datos.Proveedor.Tarifario.Codigo > 0) {
                                            $scope.Planilla.TarifarioCompra = response.Datos.Proveedor.Tarifario;
                                            $scope.ObtenerInformacionTarifario();
                                            $scope.Modelo.Planilla.Vehiculo.Tenedor = { NombreCompleto: response.Datos.NombreCompleto, Codigo: response.Datos.Codigo };
                                        }
                                        else {
                                            ShowError('El tenedor del vehículo seleccionado no posee un tarifario de compra asignado');
                                            $scope.Planilla.TarifarioCompra = {};
                                        }
                                    }
                                    else {
                                        ShowError('Error al consultar la información del tarifario');
                                        $scope.Planilla.TarifarioCompra = {};
                                    }
                                }
                                else {
                                    ShowError('Error al consultar la información del tenedor, por favor verifique la parametrización del tercero');
                                    $scope.Planilla.TarifarioCompra = {};
                                }
                            } else {
                                ShowError('Error al consultar la información del tenedor, por favor verifique la parametrización del tercero');
                                $scope.Planilla.TarifarioCompra = {};
                            }
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }
                }
            }
        };
        $scope.ObtenerInformacionTarifario = function () {
            $scope.TarifarioValido = false
            $scope.ListadoTarifaTransportes = []
            $scope.ListadoTipoTarifaTransportes = []
            $scope.Planilla.DetalleTarifarioCompra = { TipoLineaNegocioTransportes: '' }
            if ($scope.Planilla.TarifarioCompra !== undefined) {
                if ($scope.Planilla.TarifarioCompra.Codigo > 0) {
                    $scope.Planilla.TarifarioCompra.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                    TarifarioComprasFactory.Obtener($scope.Planilla.TarifarioCompra).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                $scope.Planilla.TarifarioCompra = response.data.Datos
                                $scope.ListadoTarifas = []
                                $scope.LustadoRutasFiltrado = []
                                for (var i = 0; i < $scope.Planilla.TarifarioCompra.Tarifas.length; i++) {
                                    if ($scope.Planilla.TarifarioCompra.Tarifas[i].TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == 1 || $scope.Planilla.TarifarioCompra.Tarifas[i].TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == 2) {
                                        $scope.ListadoTarifas.push($scope.Planilla.TarifarioCompra.Tarifas[i])
                                    }
                                }
                                for (var i = 0; i < $scope.ListadoRutas.length; i++) {
                                    var aplica = 0
                                    for (var j = 0; j < $scope.ListadoTarifas.length; j++) {
                                        if ($scope.ListadoRutas[i].Codigo == $scope.ListadoTarifas[j].Ruta.Codigo) {
                                            aplica++
                                        }
                                    }
                                    if (aplica > 0) {
                                        $scope.LustadoRutasFiltrado.push($scope.ListadoRutas[i])
                                    }
                                }
                                try {
                                    if ($scope.Modelo.Planilla.Ruta.Codigo > CERO) {
                                        $scope.Modelo.Planilla.Ruta = $linq.Enumerable().From($scope.LustadoRutasFiltrado).First('$.Codigo ==' + $scope.Modelo.Planilla.Ruta.Codigo);
                                        $scope.CargarImpuestos($scope.Modelo.Planilla.Ruta)
                                        $scope.FiltrarTarifas($scope.Modelo.Planilla.Ruta)
                                    }
                                } catch (e) {
                                    $scope.Modelo.Planilla.Ruta = ''
                                }
                            }
                            else {
                                ShowError('No se logro consultar el tarifario compras');
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });

                    blockUI.stop();
                }
            }
        }
        //Calcular valores
        $scope.Calcular = function (ModificaAnticipo) {
            ModificaAnticipo = ModificaAnticipo == undefined ? 0 : ModificaAnticipo;
            $scope.Modelo.Planilla.ValorImpuestos = 0;
            resetVistasImpuestos();
            if ($scope.Sesion.UsuarioAutenticado.ManejoHabilitarLiquidacionPlanillaDespachos) {
                if (!(ModificaAnticipo > 0)) {
                    $scope.Modelo.Planilla.ValorAnticipo = 0
                }
                $scope.Modelo.Planilla.Cantidad = 0
                $scope.Modelo.Planilla.Peso = 0
                //$scope.Modelo.Planilla.ValorFleteTransportador = 0


                //$scope.Modelo.Planilla.Porcentaje = 0
                $scope.Modelo.Planilla.ValorBruto = 0
                $scope.Modelo.Planilla.FleteSugerido = 0
                $scope.Modelo.Planilla.ValorDescuentos = 0
                //$scope.Modelo.Planilla.ValorEstampilla = 0
                $scope.Modelo.Planilla.ValorFondoAyuda = 0
                $scope.Modelo.Planilla.ValorContraEntrega = 0
                $scope.Modelo.Planilla.ValorNetoPagar = 0
                $scope.Modelo.Planilla.ValorUtilidad = 0

                $scope.Modelo.Planilla.ValorPagarTransportador = 0
                $scope.Modelo.Planilla.ValorRetencionFuente = 0
                $scope.Modelo.Planilla.ValorRetencionICA = 0
                $scope.Modelo.Planilla.ValorFleteCliente = 0
                $scope.Modelo.Planilla.ValorSeguroMercancia = 0
                $scope.Modelo.Planilla.ValorOtrosCobros = 0
                $scope.Modelo.Planilla.ValorTotalCredito = 0
                $scope.Modelo.Planilla.ValorTotalContado = 0
                $scope.Modelo.Planilla.ValorTotalAlcobro = 0
                $scope.Modelo.Planilla.ValorPlanillaAdicional = 0
                $scope.Modelo.Planilla.ValorReexpedicion = 0
                //if ($scope.Modelo.Planilla.Estado.Codigo == 0) {
                //    $scope.Modelo.Planilla.ValorPlanillaAdicional = 0
                //    $scope.Modelo.Planilla.ValorReexpedicion = 0
                //}
                $scope.Contado = { Cantidad: 0, FormaPago: 'Contado', ValorCliente: 0, ValorTotalCliente: 0, ValorTransportador: 0, ValorSeguro: 0, ValorTotal:0 };
                $scope.Credito = { Cantidad: 0, FormaPago: 'Crédito', ValorCliente: 0, ValorTotalCliente: 0, ValorTransportador: 0, ValorSeguro: 0, ValorTotal:0 };
                $scope.ContraEntrega = { Cantidad: 0, FormaPago: 'Contra Entrega	', ValorCliente: 0, ValorTotalCliente: 0, ValorTransportador: 0, ValorSeguro: 0, ValorTotal:0 };
                $scope.ListadoTotalesRemesas = [];
                $scope.MostrarContraEntregas();
                if ($scope.Modelo.Planilla.CalcularContraentregas) {
                    for (var i = 0; i < $scope.ListadoRemesasContraentregas.length; i++) {
                        var item = $scope.ListadoRemesasContraentregas[i];
                        console.log('tem', $scope.ListadoRemesasContraentregas)
                        if (item.Calcular) {
                            $scope.ContraEntrega.Cantidad += 1;
                            $scope.ContraEntrega.ValorCliente += item.Remesa.ValorFleteCliente + item.Remesa.ValorSeguroCliente; //--TotalFleteCliente
                            $scope.ContraEntrega.ValorTransportador += item.Remesa.ValorFleteTransportador;
                            $scope.ContraEntrega.ValorSeguro += item.Remesa.ValorSeguroCliente;
                            $scope.ContraEntrega.ValorTotalCliente += item.Remesa.TotalFleteCliente;
                            $scope.ContraEntrega.ValorTotal += item.Remesa.TotalFleteCliente;
                            //$scope.ContraEntrega.ValorTotalCliente += item.Remesa.TotalFleteCliente;
                            //$scope.Modelo.Planilla.ValorContraEntrega += item.Remesa.TotalFleteCliente;
                        }
                    }
                    $scope.Modelo.Planilla.ValorContraEntrega = Math.round($scope.ContraEntrega.ValorTotalCliente);
                }
                try {
                    for (var i = 0; i < $scope.ListadoRemesasFiltradas.length; i++) {
                        var item = $scope.ListadoRemesasFiltradas[i];
                        if (item.Seleccionado) {
                            if (item.Remesa.FormaPago.Codigo == 4901) {
                                $scope.Credito.Cantidad += 1;
                                $scope.Credito.ValorCliente += item.Remesa.ValorFleteCliente + item.Remesa.ValorSeguroCliente; //--TotalFleteCliente
                                $scope.Credito.ValorTransportador += item.Remesa.ValorFleteTransportador;
                                $scope.Credito.ValorSeguro += item.Remesa.ValorSeguroCliente;
                                $scope.Credito.ValorTotalCliente += item.Remesa.TotalFleteCliente;
                                $scope.Credito.ValorTotal += item.Remesa.TotalFleteCliente;
                                console.log('CREDITO', $scope.Credito)
                            }
                            if (item.Remesa.FormaPago.Codigo == 4902) {
                                $scope.Contado.Cantidad += 1;
                                $scope.Contado.ValorCliente += item.Remesa.ValorFleteCliente + item.Remesa.ValorSeguroCliente; //--TotalFleteCliente

                                $scope.Contado.ValorTransportador += item.Remesa.ValorFleteTransportador;
                                $scope.Contado.ValorSeguro += item.Remesa.ValorSeguroCliente;
                                $scope.Contado.ValorTotal += item.Remesa.TotalFleteCliente;
                                console.log('Contado', $scope.Contado)
                            }
                            $scope.Modelo.Planilla.Cantidad += item.Remesa.CantidadCliente;
                            $scope.Modelo.Planilla.Peso += item.Remesa.PesoCliente;
                            $scope.Modelo.Planilla.ValorBruto += item.Remesa.ValorFleteCliente; //--TotalFleteCliente
                            $scope.Modelo.Planilla.ValorSeguroMercancia += item.Remesa.ValorSeguroCliente;
                            if (item.ListaReexpedicionOficinas != null && item.ListaReexpedicionOficinas != undefined) {
                                for (var j = 0; j < item.ListaReexpedicionOficinas.length; j++) {
                                    if (item.ListaReexpedicionOficinas[j].CodigoCiudad == $scope.Modelo.Planilla.Ruta.CiudadOrigen.Codigo) {
                                        $scope.Modelo.Planilla.ValorPlanillaAdicional += item.ListaReexpedicionOficinas[j].Valor
                                    } else {
                                        $scope.Modelo.Planilla.ValorReexpedicion += item.ListaReexpedicionOficinas[j].Valor
                                    }
                                }
                            }
                        }
                    }

                } catch (e) {

                }

                if ($scope.ListadoGuiaGuardadas !== undefined && $scope.ListadoGuiaGuardadas !== null && $scope.ListadoGuiaGuardadas !== []) {
                    for (var i = 0; i < $scope.ListadoGuiaGuardadas.length; i++) {
                        var item = $scope.ListadoGuiaGuardadas[i]
                        if (item.Remesa.FormaPago.Codigo == 4901) { //Credito
                            $scope.Credito.Cantidad += 1;
                            $scope.Credito.ValorCliente += item.Remesa.ValorFleteCliente + item.Remesa.ValorSeguroCliente; //--TotalFleteCliente
                            $scope.Credito.ValorTransportador += item.Remesa.ValorFleteTransportador;
                            $scope.Credito.ValorSeguro += item.Remesa.ValorSeguroCliente;
                            $scope.Credito.ValorTotal += item.Remesa.TotalFleteCliente;
                            console.log('CREDITO', $scope.Credito.ValorTotal)
                          
                        }
                        if (item.Remesa.FormaPago.Codigo == 4902) {//Condtado
                            $scope.Contado.Cantidad += 1;
                            $scope.Contado.ValorCliente += item.Remesa.ValorFleteCliente + item.Remesa.ValorSeguroCliente; //--TotalFleteCliente
                            $scope.Contado.ValorTransportador += item.Remesa.ValorFleteTransportador;
                            $scope.Contado.ValorSeguro += item.Remesa.ValorSeguroCliente;
                            $scope.Contado.ValorTotal += item.Remesa.TotalFleteCliente;
                            console.log('CONTADO', $scope.Contado.ValorTotal)
                        }
                        /*if (item.Remesa.FormaPago.Codigo == 4903) {//Contraentregas
                            $scope.ContraEntrega.Cantidad += 1;
                            $scope.ContraEntrega.ValorCliente += item.Remesa.ValorFleteCliente; //--TotalFleteCliente
                            $scope.ContraEntrega.ValorTransportador += item.Remesa.ValorFleteTransportador;
                            $scope.ContraEntrega.ValorSeguro += item.Remesa.ValorSeguroCliente;
                            $scope.ContraEntrega.ValorTotalCliente += item.Remesa.ValorFleteCliente;
                            //$scope.ContraEntrega.ValorTotalCliente += item.Remesa.TotalFleteCliente;
                            //$scope.Modelo.Planilla.ValorContraEntrega += item.Remesa.TotalFleteCliente;
                        }*/

                        $scope.Modelo.Planilla.Cantidad += item.Remesa.CantidadCliente;
                        $scope.Modelo.Planilla.Peso += item.Remesa.PesoCliente;
                        $scope.Modelo.Planilla.ValorBruto += item.Remesa.ValorFleteCliente; //--TotalFleteCliente
                        $scope.Modelo.Planilla.ValorSeguroMercancia += item.Remesa.ValorSeguroCliente;
                        if (item.ListaReexpedicionOficinas != undefined && item.ListaReexpedicionOficinas != null) {
                            for (var j = 0; j < item.ListaReexpedicionOficinas.length; j++) {
                                if (item.ListaReexpedicionOficinas[j].CodigoCiudad == $scope.Modelo.Planilla.Ruta.CiudadOrigen.Codigo) {
                                    $scope.Modelo.Planilla.ValorPlanillaAdicional += item.ListaReexpedicionOficinas[j].Valor
                                } else {
                                    $scope.Modelo.Planilla.ValorReexpedicion += item.ListaReexpedicionOficinas[j].Valor
                                }
                            }
                        }
                    }
                }


                $scope.ListadoTotalesRemesas.push($scope.Credito);
                $scope.ListadoTotalesRemesas.push($scope.Contado);
                $scope.ListadoTotalesRemesas.push($scope.ContraEntrega);
                console.log('llego', $scope.ListadoTotalesRemesas)
                if (RevertirMV($scope.Modelo.Planilla.Porcentaje) > 200) {
                    ShowError('El porcentaje no puede ser superior a 200');
                    $scope.Modelo.Planilla.Porcentaje = 0;
                }
                if ($scope.Modelo.Planilla.Porcentaje > 0) {
                    $scope.Modelo.Planilla.FleteSugerido = Math.round($scope.Modelo.Planilla.ValorBruto * (RevertirMV($scope.Modelo.Planilla.Porcentaje) / 100));
                }
                if (RevertirMV($scope.Modelo.Planilla.ValorFleteTransportador) > 0) {
                    //$scope.Modelo.Planilla.ValorDescuentos = $scope.Modelo.Planilla.FleteSugerido - RevertirMV($scope.Modelo.Planilla.ValorFleteTransportador) + RevertirMV($scope.Modelo.Planilla.ValorPlanillaAdicional);
                    $scope.Modelo.Planilla.ValorDescuentos = $scope.Modelo.Planilla.FleteSugerido - RevertirMV($scope.Modelo.Planilla.ValorFleteTransportador);
                    try {
                        if ($scope.Modelo.Planilla.Vehiculo.TipoDueno.Codigo == 2102 || $scope.Modelo.Planilla.Vehiculo.TipoDueno.Codigo == 2104) {
                            $scope.Modelo.Planilla.ValorFondoAyuda = Math.round(RevertirMV($scope.Modelo.Planilla.ValorFleteTransportador) * 0.01);
                        }
                    } catch (e) {

                    }

                }

                if ($scope.Modelo.Planilla.ValorAnticipo !== undefined && $scope.Modelo.Planilla.ValorAnticipo !== '') {
                    if (RevertirMV($scope.Modelo.Planilla.ValorAnticipo) > RevertirMV($scope.Modelo.Planilla.ValorFleteTransportador)) {
                        ShowError('El valor del anticipo no puede ser mayor al valor del flete del transportador')
                        $scope.Modelo.Planilla.ValorAnticipo = 0
                    }
                }
                //try {
                //    $scope.ListadoImpuestosFiltrado = []
                //    if ($scope.ListadoImpuestos.length > 0) {
                //        for (var i = 0; i < $scope.ListadoImpuestos.length; i++) {
                //            var impuesto = {
                //                Nombre: $scope.ListadoImpuestos[i].Nombre,
                //                CodigoImpuesto: $scope.ListadoImpuestos[i].Codigo,
                //                ValorTarifa: $scope.ListadoImpuestos[i].Valor_tarifa,
                //                ValorBase: $scope.ListadoImpuestos[i].valor_base,
                //            }
                //            if ($scope.ListadoImpuestos[i].valor_base < parseInt(MascaraNumero($scope.Modelo.Planilla.ValorFleteTransportador))) {
                //                impuesto.ValorBase = parseInt(MascaraNumero($scope.Modelo.Planilla.ValorFleteTransportador))
                //            }

                //            impuesto.ValorImpuesto = impuesto.ValorTarifa * impuesto.ValorBase
                //            impuesto.ValorImpuesto = Math.round(impuesto.ValorImpuesto * 100) / 100
                //            $scope.ListadoImpuestosFiltrado.push(impuesto)
                //        }
                //    }
                //} catch (e) {

                //}
                //try {
                //    if ($scope.ListadoImpuestosFiltrado.length > 0) {
                //        for (var i = 0; i < $scope.ListadoImpuestosFiltrado.length; i++) {
                //            $scope.Modelo.Planilla.ValorImpuestos += parseFloat($scope.ListadoImpuestosFiltrado[i].ValorImpuesto);
                //        }
                //    }
                //    //$scope.Modelo.Planilla.ValorPagarTransportador = parseInt(MascaraNumero($scope.Modelo.Planilla.ValorPagarTransportador)) - $scope.Modelo.Planilla.ValorImpuestos
                //} catch (e) {

                //}
                var TMPImpuestos = [];
                var FleteAcordado = RevertirMV($scope.Modelo.Planilla.ValorFleteTransportador);
                if (FleteAcordado > CERO) {
                    for (var j = 0; j < $scope.ListadoImpuestos.length; j++) {
                        var ValorBase = CERO;
                        if (FleteAcordado >= $scope.ListadoImpuestos[j].ValorBase) {//verifica si aplica o no con el valor existente
                            ValorBase = FleteAcordado;
                        }
                        else {
                            ValorBase = CERO;
                        }
                        TMPImpuestos.push({
                            Codigo: $scope.ListadoImpuestos[j].Codigo,
                            ValorBase: ValorBase,
                            ValorImpuesto: Math.round(ValorBase * $scope.ListadoImpuestos[j].ValorTarifa)
                        });
                    }

                    for (var i = 0; i < $scope.ListadoImpuestosFiltrado.length; i++) {
                        for (var j = 0; j < TMPImpuestos.length; j++) {
                            if ($scope.ListadoImpuestosFiltrado[i].Codigo == TMPImpuestos[j].Codigo) {
                                $scope.ListadoImpuestosFiltrado[i].ValorBase += TMPImpuestos[j].ValorBase;
                                $scope.ListadoImpuestosFiltrado[i].ValorImpuesto += TMPImpuestos[j].ValorImpuesto;
                            }
                        }
                    }

                    for (var i = 0; i < $scope.ListadoImpuestosFiltrado.length; i++) {
                        switch ($scope.ListadoImpuestosFiltrado[i].Operacion) {
                            case 1:
                                $scope.Modelo.Planilla.ValorImpuestos += $scope.ListadoImpuestosFiltrado[i].ValorImpuesto;
                                break;
                            case 2:
                                $scope.Modelo.Planilla.ValorImpuestos -= $scope.ListadoImpuestosFiltrado[i].ValorImpuesto;
                                break;
                            default:
                                $scope.Modelo.Planilla.ValorImpuestos += $scope.ListadoImpuestosFiltrado[i].ValorImpuesto;
                                break;
                        }
                    }
                }
                $scope.Modelo.Planilla.ValorImpuestos = parseFloat($scope.Modelo.Planilla.ValorImpuestos);
                $scope.Modelo.Planilla.ValorPagarTransportador = parseInt(MascaraNumero($scope.Modelo.Planilla.ValorPagarTransportador)) + $scope.Modelo.Planilla.ValorImpuestos;

                $scope.Modelo.Planilla.Peso = ($scope.Modelo.Planilla.Peso).toFixed(2);
                //$scope.Modelo.Planilla.ValorNetoPagar = RevertirMV($scope.Modelo.Planilla.ValorFleteTransportador) - RevertirMV($scope.Modelo.Planilla.ValorEstampilla) - $scope.Modelo.Planilla.ValorFondoAyuda - $scope.Modelo.Planilla.ValorImpuestos - RevertirMV($scope.Modelo.Planilla.ValorPlanillaAdicional);
                $scope.Modelo.Planilla.ValorNetoPagar = RevertirMV($scope.Modelo.Planilla.ValorFleteTransportador) - RevertirMV($scope.Modelo.Planilla.ValorEstampilla) - $scope.Modelo.Planilla.ValorFondoAyuda + $scope.Modelo.Planilla.ValorImpuestos;
                $scope.Modelo.Planilla.ValorPagarTransportador = $scope.Modelo.Planilla.ValorNetoPagar - RevertirMV($scope.Modelo.Planilla.ValorAnticipo) - $scope.Modelo.Planilla.ValorContraEntrega + RevertirMV($scope.Modelo.Planilla.ValorPlanillaAdicional);
                $scope.Modelo.Planilla.ValorUtilidad = Math.round($scope.Modelo.Planilla.ValorBruto - $scope.Modelo.Planilla.FleteSugerido + $scope.Modelo.Planilla.ValorDescuentos);

                if (($scope.Modelo.Planilla.ValorAnticipo == 0 || $scope.Modelo.Planilla.ValorAnticipo == undefined || $scope.Modelo.Planilla.ValorAnticipo == '') && $scope.Modelo.Planilla.ValorPagarTransportador > 0) {
                    if (ModificaAnticipo == 0) {
                        //$scope.Modelo.Planilla.ValorAnticipo = angular.copy($scope.Modelo.Planilla.ValorPagarTransportador);
                        $scope.Modelo.Planilla.ValorAnticipo = $scope.Modelo.Planilla.ValorNetoPagar - $scope.Modelo.Planilla.ValorContraEntrega;
                    }
                    //$scope.Modelo.Planilla.ValorNetoPagar = RevertirMV($scope.Modelo.Planilla.ValorFleteTransportador) - RevertirMV($scope.Modelo.Planilla.ValorEstampilla) - $scope.Modelo.Planilla.ValorFondoAyuda - $scope.Modelo.Planilla.ValorImpuestos - RevertirMV($scope.Modelo.Planilla.ValorPlanillaAdicional);
                    $scope.Modelo.Planilla.ValorNetoPagar = RevertirMV($scope.Modelo.Planilla.ValorFleteTransportador) - RevertirMV($scope.Modelo.Planilla.ValorEstampilla) - $scope.Modelo.Planilla.ValorFondoAyuda + $scope.Modelo.Planilla.ValorImpuestos;
                    $scope.Modelo.Planilla.ValorPagarTransportador = $scope.Modelo.Planilla.ValorNetoPagar - RevertirMV($scope.Modelo.Planilla.ValorAnticipo) - $scope.Modelo.Planilla.ValorContraEntrega + RevertirMV($scope.Modelo.Planilla.ValorPlanillaAdicional);
                    $scope.Modelo.Planilla.ValorUtilidad = Math.round($scope.Modelo.Planilla.ValorBruto - $scope.Modelo.Planilla.FleteSugerido + $scope.Modelo.Planilla.ValorDescuentos);
                }
                //CalcularImpuesto();
                $scope.Modelo.Planilla.ValorAnticipo = parseFloat(MascaraDecimales($scope.Modelo.Planilla.ValorAnticipo));
                $scope.Modelo.Planilla.ValorNetoPagar = Math.round(($scope.Modelo.Planilla.ValorNetoPagar * 100) / 100);
                $scope.Modelo.Planilla.ValorAnticipo = Math.round(($scope.Modelo.Planilla.ValorAnticipo * 100) / 100);
                $scope.Modelo.Planilla.ValorPagarTransportador = Math.round(($scope.Modelo.Planilla.ValorPagarTransportador * 100) / 100);
                $scope.Modelo.Planilla.ValorImpuestos = $scope.MaskValoresGrid(Math.round($scope.Modelo.Planilla.ValorImpuestos));
                $scope.MaskValores();
            }
            else {
                $scope.Modelo.Planilla.Cantidad = 0
                $scope.Modelo.Planilla.Peso = 0
                $scope.Modelo.Planilla.ValorRetencionFuente = 0
                $scope.Modelo.Planilla.ValorRetencionICA = 0
                $scope.Modelo.Planilla.ValorFleteCliente = 0
                $scope.Modelo.Planilla.ValorSeguroMercancia = 0
                $scope.Modelo.Planilla.ValorOtrosCobros = 0
                $scope.Modelo.Planilla.ValorTotalCredito = 0
                $scope.Modelo.Planilla.ValorTotalContado = 0
                $scope.Modelo.Planilla.ValorTotalAlcobro = 0
                $scope.Modelo.Planilla.ValorFleteTransportador = 0
                $scope.Contado = { Cantidad: 0, FormaPago: 'Contado', ValorCliente: 0, ValorTransportador: 0, ValorSeguro: 0 };
                $scope.Credito = { Cantidad: 0, FormaPago: 'Crédito', ValorCliente: 0, ValorTransportador: 0, ValorSeguro: 0 };
                $scope.ContraEntrega = { Cantidad: 0, FormaPago: 'Contra Entrega	', ValorCliente: 0, ValorTransportador: 0, ValorSeguro: 0 };
                $scope.ListadoTotalesRemesas = [];
                try {
                    for (var i = 0; i < $scope.ListadoRemesasFiltradas.length; i++) {
                        var item = $scope.ListadoRemesasFiltradas[i];
                        if (item.Seleccionado) {
                            if (item.Remesa.FormaPago.Codigo == 4901) {
                                $scope.Credito.Cantidad += 1;

                                $scope.Credito.ValorCliente += item.Remesa.ValorFleteCliente + item.Remesa.ValorSeguroCliente; //--TotalFleteCliente

                                $scope.Credito.ValorTransportador += item.Remesa.ValorFleteTransportador;
                                $scope.Credito.ValorSeguro += item.Remesa.ValorSeguroCliente;
                            }
                            if (item.Remesa.FormaPago.Codigo == 4902) {
                                $scope.Contado.Cantidad += 1;
                                $scope.Contado.ValorCliente += item.Remesa.ValorFleteCliente + item.Remesa.ValorSeguroCliente; //--TotalFleteCliente
                                $scope.Contado.ValorTransportador += item.Remesa.ValorFleteTransportador;
                                $scope.Contado.ValorSeguro += item.Remesa.ValorSeguroCliente;
                            }
                            if (item.Remesa.FormaPago.Codigo == 4903) {
                                $scope.ContraEntrega.Cantidad += 1;
                                $scope.ContraEntrega.ValorCliente += item.Remesa.ValorFleteCliente + item.Remesa.ValorSeguroCliente;//--TotalFleteCliente
                                $scope.ContraEntrega.ValorTransportador += item.Remesa.ValorFleteTransportador;
                                $scope.ContraEntrega.ValorSeguro += item.Remesa.ValorSeguroCliente;

                            }
                            $scope.Modelo.Planilla.Cantidad += item.Remesa.CantidadCliente;
                            $scope.Modelo.Planilla.Peso += item.Remesa.PesoCliente;
                            $scope.Modelo.Planilla.ValorSeguroMercancia += item.Remesa.ValorSeguroCliente;
                        }
                    }
                } catch (e) {

                }

                if ($scope.ListadoGuiaGuardadas !== undefined && $scope.ListadoGuiaGuardadas !== null && $scope.ListadoGuiaGuardadas !== []) {
                    for (var i = 0; i < $scope.ListadoGuiaGuardadas.length; i++) {
                        var item = $scope.ListadoGuiaGuardadas[i];
                        if (item.Remesa.FormaPago.Codigo == 4901) {
                            $scope.Credito.Cantidad += 1;
                            $scope.Credito.ValorCliente += item.Remesa.ValorFleteCliente + item.Remesa.ValorSeguroCliente; //--TotalFleteCliente
                            $scope.Credito.ValorTransportador += item.Remesa.ValorFleteTransportador;
                            $scope.Credito.ValorSeguro += item.Remesa.ValorSeguroCliente;
                        }
                        if (item.Remesa.FormaPago.Codigo == 4902) {
                            $scope.Contado.Cantidad += 1;
                            $scope.Contado.ValorCliente += item.Remesa.ValorFleteCliente + item.Remesa.ValorSeguroCliente;//TotalFleteCliente
                            $scope.Contado.ValorTransportador += item.Remesa.ValorFleteTransportador;
                            $scope.Contado.ValorSeguro += item.Remesa.ValorSeguroCliente;
                        }
                        if (item.Remesa.FormaPago.Codigo == 4903) {
                            $scope.ContraEntrega.Cantidad += 1;
                            $scope.ContraEntrega.ValorCliente += item.Remesa.ValorFleteCliente  +item.Remesa.ValorSeguroCliente; //--TotalFleteCliente
                            $scope.ContraEntrega.ValorTransportador += item.Remesa.ValorFleteTransportador;
                            $scope.ContraEntrega.ValorSeguro += item.Remesa.ValorSeguroCliente;
                        }
                        $scope.Modelo.Planilla.Cantidad += item.Remesa.CantidadCliente;
                        $scope.Modelo.Planilla.Peso += item.Remesa.PesoCliente;
                        $scope.Modelo.Planilla.ValorSeguroMercancia += item.Remesa.ValorSeguroCliente;
                    }
                }


                $scope.ListadoTotalesRemesas.push($scope.Credito);
                $scope.ListadoTotalesRemesas.push($scope.Contado);
                $scope.ListadoTotalesRemesas.push($scope.ContraEntrega);
                console.log('total cliente', $scope.ContraEntrega)
                $scope.Modelo.Planilla.ValorFleteTransportador = 0
                //if ($scope.Sesion.UsuarioAutenticado.ManejoPorcentajeAfiliado) {
                if ($scope.Sesion.UsuarioAutenticado.ManejoHabilitarLiquidacionPlanillaDespachos) {
                    try {
                        for (var i = 0; i < $scope.ListadoRemesasFiltradas.length; i++) {
                            var item = $scope.ListadoRemesasFiltradas[i]
                            if (item.Seleccionado) {
                                $scope.Modelo.Planilla.ValorFleteTransportador += item.Remesa.ValorFleteTransportador
                            }
                        }
                    } catch (e) {
                    }
                    if ($scope.ListadoGuiaGuardadas !== undefined && $scope.ListadoGuiaGuardadas !== null && $scope.ListadoGuiaGuardadas !== []) {
                        for (var i = 0; i < $scope.ListadoGuiaGuardadas.length; i++) {
                            var item = $scope.ListadoGuiaGuardadas[i]
                            $scope.Modelo.Planilla.ValorFleteTransportador += item.Remesa.ValorFleteTransportador
                        }
                    }
                }
                else {
                    try {
                        $scope.Modelo.Planilla.ValorFleteTransportador = $scope.TipoTarifaTransportes.ValorFlete

                    } catch (e) {

                    }
                }

                if ($scope.Modelo.Planilla.ValorAnticipo !== undefined && $scope.Modelo.Planilla.ValorAnticipo !== '') {
                    if ($scope.MaskNumeroGrid($scope.Modelo.Planilla.ValorAnticipo) > $scope.MaskNumeroGrid($scope.Modelo.Planilla.ValorFleteTransportador)) {
                        ShowError('El valor del anticipo no puede ser mayor al valor del flete del transportador')
                        $scope.Modelo.Planilla.ValorPagarTransportador = $scope.MaskValoresGrid(Math.round($scope.Modelo.Planilla.ValorFleteTransportador))
                    } else {
                        $scope.Modelo.Planilla.ValorPagarTransportador = $scope.MaskValoresGrid($scope.MaskNumeroGrid($scope.Modelo.Planilla.ValorFleteTransportador) - $scope.MaskNumeroGrid($scope.Modelo.Planilla.ValorAnticipo))
                    }
                } else {
                    $scope.Modelo.Planilla.ValorPagarTransportador = $scope.MaskValoresGrid(Math.round($scope.Modelo.Planilla.ValorFleteTransportador))
                }
                //try {
                //    $scope.ListadoImpuestosFiltrado = []
                //    if ($scope.ListadoImpuestos.length > 0) {
                //        for (var i = 0; i < $scope.ListadoImpuestos.length; i++) {
                //            var impuesto = {
                //                Nombre: $scope.ListadoImpuestos[i].Nombre,
                //                CodigoImpuesto: $scope.ListadoImpuestos[i].Codigo,
                //                ValorTarifa: $scope.ListadoImpuestos[i].Valor_tarifa,
                //                ValorBase: $scope.ListadoImpuestos[i].valor_base,
                //            }
                //            if ($scope.ListadoImpuestos[i].valor_base < parseInt(MascaraNumero($scope.Modelo.Planilla.ValorFleteTransportador))) {
                //                impuesto.ValorBase = parseInt(MascaraNumero($scope.Modelo.Planilla.ValorFleteTransportador))
                //            }

                //            impuesto.ValorImpuesto = impuesto.ValorTarifa * impuesto.ValorBase
                //            impuesto.ValorImpuesto = Math.round(impuesto.ValorImpuesto * 100) / 100
                //            $scope.ListadoImpuestosFiltrado.push(impuesto)
                //        }
                //    }
                //} catch (e) {

                //}
                //try {
                //    $scope.Modelo.Planilla.ValorImpuestos = 0
                //    if ($scope.ListadoImpuestosFiltrado.length > 0) {
                //        for (var i = 0; i < $scope.ListadoImpuestosFiltrado.length; i++) {
                //            $scope.Modelo.Planilla.ValorImpuestos += parseFloat($scope.ListadoImpuestosFiltrado[i].ValorImpuesto);
                //        }
                //    }
                //    $scope.Modelo.Planilla.ValorPagarTransportador = parseInt(MascaraNumero($scope.Modelo.Planilla.ValorPagarTransportador)) - $scope.Modelo.Planilla.ValorImpuestos;
                //} catch (e) {

                //}
                var TMPImpuestos = [];
                var FleteAcordado = RevertirMV($scope.Modelo.Planilla.ValorFleteTransportador);
                if (FleteAcordado > CERO) {
                    for (var j = 0; j < $scope.ListadoImpuestos.length; j++) {
                        var ValorBase = CERO;
                        if (FleteAcordado >= $scope.ListadoImpuestos[j].ValorBase) {//verifica si aplica o no con el valor existente
                            ValorBase = FleteAcordado;
                        }
                        else {
                            ValorBase = CERO;
                        }
                        TMPImpuestos.push({
                            Codigo: $scope.ListadoImpuestos[j].Codigo,
                            ValorBase: ValorBase,
                            ValorImpuesto: Math.round(ValorBase * $scope.ListadoImpuestos[j].ValorTarifa)
                        });
                    }

                    for (var i = 0; i < $scope.ListadoImpuestosFiltrado.length; i++) {
                        for (var j = 0; j < TMPImpuestos.length; j++) {
                            if ($scope.ListadoImpuestosFiltrado[i].Codigo == TMPImpuestos[j].Codigo) {
                                $scope.ListadoImpuestosFiltrado[i].ValorBase += TMPImpuestos[j].ValorBase;
                                $scope.ListadoImpuestosFiltrado[i].ValorImpuesto += TMPImpuestos[j].ValorImpuesto;
                            }
                        }
                    }

                    for (var i = 0; i < $scope.ListadoImpuestosFiltrado.length; i++) {
                        switch ($scope.ListadoImpuestosFiltrado[i].Operacion) {
                            case 1:
                                $scope.Modelo.Planilla.ValorImpuestos += $scope.ListadoImpuestosFiltrado[i].ValorImpuesto;
                                break;
                            case 2:
                                $scope.Modelo.Planilla.ValorImpuestos -= $scope.ListadoImpuestosFiltrado[i].ValorImpuesto;
                                break;
                            default:
                                $scope.Modelo.Planilla.ValorImpuestos += $scope.ListadoImpuestosFiltrado[i].ValorImpuesto;
                                break;
                        }
                    }
                }
                $scope.Modelo.Planilla.ValorImpuestos = parseFloat($scope.Modelo.Planilla.ValorImpuestos);
                $scope.Modelo.Planilla.ValorPagarTransportador = parseInt(MascaraNumero($scope.Modelo.Planilla.ValorPagarTransportador)) + $scope.Modelo.Planilla.ValorImpuestos;

                $scope.Modelo.Planilla.Peso = ($scope.Modelo.Planilla.Peso).toFixed(2);
                $scope.Modelo.Planilla.ValorRetencionFuente = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorRetencionFuente);
                $scope.Modelo.Planilla.ValorRetencionICA = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorRetencionICA);
                $scope.Modelo.Planilla.ValorFleteCliente = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorFleteCliente);
                $scope.Modelo.Planilla.ValorSeguroMercancia = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorSeguroMercancia);
                $scope.Modelo.Planilla.ValorOtrosCobros = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorOtrosCobros);
                $scope.Modelo.Planilla.ValorTotalCredito = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorTotalCredito);
                $scope.Modelo.Planilla.ValorTotalContado = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorTotalContado);
                $scope.Modelo.Planilla.ValorTotalAlcobro = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorTotalAlcobro);
                $scope.Modelo.Planilla.ValorFleteTransportador = $scope.MaskValoresGrid(Math.round($scope.Modelo.Planilla.ValorFleteTransportador));
                $scope.Modelo.Planilla.ValorPagarTransportador = $scope.MaskValoresGrid(Math.round($scope.Modelo.Planilla.ValorPagarTransportador));
                $scope.Modelo.Planilla.ValorImpuestos = $scope.MaskValoresGrid(Math.round($scope.Modelo.Planilla.ValorImpuestos));
                //CalcularImpuesto();
                $scope.MaskValores();
            }

        };
        //--Reset Impuestos
        function resetVistasImpuestos() {
            //---Inicializa valor impuesto vista Factura 
            if ($scope.ListadoImpuestosFiltrado != undefined) {
                $scope.ListadoImpuestosFiltrado.forEach(function (item) {
                    item.ValorBase = CERO;
                    item.ValorImpuesto = CERO;
                });
            }
        }
        //--Reset Impuestos
        //Obtener Informacion ResumenRemesas
        $scope.RemesasAgencia = [];
        $scope.RemesasReex = [];
        $scope.CargarResumenRemesas = function () {
            $scope.RemesasAgencia = [];
            $scope.RemesasReex = [];
            $scope.LimpiarRemesas();
            CargarResumenGeneralRemesas();
        };
        function CargarResumenGeneralRemesas() {
            blockUI.start('Buscando registros ...');
            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumentoPlanilla: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO,
                Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                Sync: true
            };
            blockUI.delay = 1000;
            $scope.ResultadoSinRegistrosResumenRemesas = '';
            var ResponConsulta = PlanificacionDespachosFactory.Consultar(filtros);
            if (ResponConsulta.ProcesoExitoso === true) {
                if (ResponConsulta.Datos.length > 0) {
                    for (var i = 0; i < ResponConsulta.Datos.length; i++) {
                        if (ResponConsulta.Datos[i].Reexpedicion == 0) {
                            $scope.RemesasAgencia.push(ResponConsulta.Datos[i]);
                        }
                        else {
                            $scope.RemesasReex.push(ResponConsulta.Datos[i]);
                        }
                    }
                }
                else {
                    $scope.ResultadoSinRegistrosResumenRemesas = 'No hay datos para mostrar';
                }
            }
            else {
                ShowError(ResponConsulta.statusText);
            }
            blockUI.stop();
        }

        $scope.ListadoRemesas = [];
        //Obtener Informacion ResumenRemesas
        //------------------------------Cargar Detalle Remesas -----------------//
        $scope.CheckRemesas = function (Seleccionado, ResumenRemesa) {
            if (Seleccionado == true) {
                //--Cargar Remesas
                blockUI.start('Buscando registros ...');
                $timeout(function () {
                    blockUI.message('Espere por favor ...');
                }, 100);
                $scope.Buscando = true;
                var filtro = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    TipoDocumento: CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA,
                    Remesa: {
                        CiudadRemitente: { Codigo: ResumenRemesa.CiudadOrigen.Codigo },
                        CiudadDestinatario: { Codigo: ResumenRemesa.CiudadDestino.Codigo }
                    },
                    NumeroPlanilla: 0,
                    Estado: ESTADO_DEFINITIVO,
                    Anulado: ESTADO_NO_ANULADO,
                    CodigoOficinaActualUsuario: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
                    Reexpedicion: ResumenRemesa.Reexpedicion,
                    Sync: true
                };
                blockUI.delay = 1000;
                var ResponConsulta = RemesaGuiasFactory.ConsultarRemesasPorPlanillar(filtro);
                if (ResponConsulta.ProcesoExitoso === true) {
                    var existe = false;
                    if ($scope.ListadoRemesas.length > 0) {
                        for (var i = 0; i < ResponConsulta.Datos.length; i++) {
                            for (var j = 0; j < $scope.ListadoRemesas.length; j++) {
                                if ($scope.ListadoRemesas[j].Remesa.Numero === ResponConsulta.Datos[i].Remesa.Numero) {
                                    existe = true;
                                }
                            }
                            if (!existe) {
                                $scope.ListadoRemesas.push(ResponConsulta.Datos[i]);
                            }
                            existe = false;
                        }
                        $scope.ListadoRemesasFiltradas = $scope.ListadoRemesas;
                    }
                    else {
                        for (var i = 0; i < ResponConsulta.Datos.length; i++) {
                            $scope.ListadoRemesas.push(ResponConsulta.Datos[i]);
                        }
                        $scope.ListadoRemesasFiltradas = $scope.ListadoRemesas;
                    }
                }
                else {
                    ShowError(ResponConsulta.statusText);
                }
                blockUI.stop();
            }
            else {
                var tmpDetalle = [];
                var tmpDetalleAgencia = [];
                var tmpDetalleReex = [];
                for (var i = 0; i < $scope.ListadoRemesas.length; i++) {
                    if ($scope.ListadoRemesas[i].Reexpedicion == 0) {
                        tmpDetalleAgencia.push($scope.ListadoRemesas[i]);
                    }
                    else {
                        tmpDetalleReex.push($scope.ListadoRemesas[i]);
                    }
                }
                if (ResumenRemesa.Reexpedicion == 0) {
                    tmpDetalleAgencia = BorrarElementosArray(tmpDetalleAgencia, ResumenRemesa.CiudadOrigen.Codigo, ResumenRemesa.CiudadDestino.Codigo);
                }
                if (ResumenRemesa.Reexpedicion == 1) {
                    tmpDetalleReex = BorrarElementosArray(tmpDetalleReex, ResumenRemesa.CiudadOrigen.Codigo, ResumenRemesa.CiudadDestino.Codigo);
                }
                $scope.ListadoRemesas = tmpDetalle.concat(tmpDetalleAgencia, tmpDetalleReex);
                $scope.ListadoRemesasFiltradas = $scope.ListadoRemesas;
            }
            $scope.totalRegistros = $scope.ListadoRemesasFiltradas.length;
            //ResetPaginacion();
        };
        function BorrarElementosArray(arr, origen, destino) {
            var i = arr.length;
            while (i--) {
                //if (arr[i].Remesa.CiudadDestinatario.Codigo === value && (arr[i].Seleccion == false || arr[i].Seleccion == undefined) ) {
                if (arr[i].Remesa.CiudadRemitente.Codigo === origen && arr[i].Remesa.CiudadDestinatario.Codigo === destino) {
                    arr.splice(i, 1);
                }
            }
            return arr;
        }
        //------------------------------Cargar Detalle Remesas -----------------//
        //---Limpiar Campo Remesas--//
        $scope.LimpiarRemesas = function () {
            $scope.ListadoRemesas = [];
            $scope.ListadoRemesasFiltradas = [];
            if ($scope.RemesasAgencia.length > 0) {
                for (var i = 0; i < $scope.RemesasAgencia.length; i++) {
                    $scope.RemesasAgencia[i].Seleccion = false;
                }
            }
            if ($scope.RemesasReex.length > 0) {
                for (var i = 0; i < $scope.RemesasReex.length; i++) {
                    $scope.RemesasReex[i].Seleccion = false;
                }
            }
            //ResetPaginacion();
            $scope.Calcular();
        };
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $scope.Codigo = 0;
                $scope.MensajesErrorConslta = [];
                if (DatosRequeridosConsultaRemesas()) {
                    if ($scope.MensajesErrorConslta.length == 0) {
                        //Bloqueo Pantalla
                        blockUIConfig.autoBlock = true;
                        blockUIConfig.delay = 0;
                        blockUI.start("Cargando Remesas...");
                        $timeout(function () {
                            blockUI.message("Cargando Remesas...");
                            Find();
                        }, 100);
                        //Bloqueo Pantalla
                    }
                }
            }
        };
        function Find() {
            $scope.ListadoRemesas = [];
            $scope.Buscando = true;
            var filtro = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA,
                FechaInicial: $scope.CargarRemesas.FechaInicial,
                FechaFinal: $scope.CargarRemesas.FechaFinal,
                NumeroInicial: $scope.CargarRemesas.NumeroInicial,
                Remesa: {
                    CiudadRemitente: $scope.CargarRemesas.CiudadRemitente,
                    CiudadDestinatario: $scope.CargarRemesas.CiudadDestinatario,
                    Cliente: $scope.CargarRemesas.Cliente,
                    NumeroDocumentoCliente: $scope.CargarRemesas.NumeroDocumentoCliente,
                    Remitente: $scope.CargarRemesas.Remitente
                },
                NumeroPlanilla: 0,
                Estado: ESTADO_DEFINITIVO,
                Anulado: ESTADO_NO_ANULADO,
                Reexpedicion: -1,
                CodigoOficinaActualUsuario: $scope.CargarRemesas.OficinaOrigen.Codigo
            };
            RemesaGuiasFactory.ConsultarRemesasPorPlanillar(filtro).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo.Codigo = 0;
                        if (response.data.Datos.length > 0) {
                            if ($scope.ListadoRemesas.length == 0) {
                                $scope.ListadoRemesas = response.data.Datos;
                            } else {
                                for (var i = 0; i < response.data.Datos.length; i++) {
                                    var cont = 0
                                    for (var j = 0; j < $scope.ListadoRemesas.length; j++) {
                                        if ($scope.ListadoRemesas[j].Remesa.Numero == response.data.Datos[i].Remesa.Numero) {
                                            cont++;
                                        }
                                    }
                                    if (cont == 0) {
                                        $scope.ListadoRemesas.push(response.data.Datos[i]);
                                    }
                                }
                            }
                            $scope.ListadoRemesasFiltradas = $scope.ListadoRemesas;
                            $scope.totalRegistros = $scope.ListadoRemesas.length;
                            //$scope.totalPaginas = Math.ceil($scope.totalRegistros / 10);
                            //ResetPaginacion();
                            $scope.Buscando = false;
                            $scope.ResultadoSinRegistros = '';
                        }
                        else {
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }
                        var listado = [];
                        response.data.Datos.forEach(function (item) {
                            listado.push(item);
                        });
                    }
                    blockUI.stop();
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 100);

                }, function (response) {
                    ShowError(response.statusText);
                    blockUI.stop();
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 100);
                });
        }
        function ResetPaginacion() {
            $scope.totalRegistros = $scope.ListadoRemesasFiltradas.length;
            $scope.totalPaginas = Math.ceil($scope.totalRegistros / 10);
            $scope.PrimerPagina();
        }
        //Paginacion
        $scope.PrimerPagina = function () {
            if ($scope.totalRegistros > 10) {
                $scope.ListadoGuias = []
                $scope.paginaActual = 1
                for (var i = 0; i < 10; i++) {
                    $scope.ListadoGuias.push($scope.ListadoRemesasFiltradas[i])
                }
            } else {
                $scope.ListadoGuias = $scope.ListadoRemesasFiltradas
            }
        }
        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual -= 1
                var a = $scope.paginaActual * 10
                if (a < $scope.totalRegistros) {
                    $scope.ListadoGuias = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ListadoGuias.push($scope.ListadoRemesasFiltradas[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual += 1
                var a = $scope.paginaActual * 10
                if (a < $scope.totalRegistros) {
                    $scope.ListadoGuias = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ListadoGuias.push($scope.ListadoRemesasFiltradas[i])
                    }
                } else {
                    $scope.UltimaPagina()
                }
            } else if ($scope.paginaActual == $scope.totalPaginas) {
                $scope.UltimaPagina()
            }
        }
        $scope.UltimaPagina = function () {
            if ($scope.totalRegistros > 10 && $scope.totalPaginas > 1) {
                $scope.paginaActual = $scope.totalPaginas
                var a = $scope.paginaActual * 10
                $scope.ListadoGuias = []
                for (var i = a - 10; i < $scope.ListadoRemesasFiltradas.length; i++) {
                    $scope.ListadoGuias.push($scope.ListadoRemesasFiltradas[i])
                }
            }
        }
        //Paginacion
        //Paginacion Remesa Guardadas
        $scope.paginaActualG = 1;
        $scope.totalRegistrosG = 0;
        $scope.PagListadoGuiaGuardadas = [];
        $scope.PrimerPaginaG = function () {
            if ($scope.totalRegistrosG > 20) {
                $scope.PagListadoGuiaGuardadas = [];
                $scope.paginaActualG = 1;
                for (var i = 0; i < 20; i++) {
                    $scope.PagListadoGuiaGuardadas.push($scope.ListadoGuiaGuardadas[i]);
                }
            } else {
                $scope.PagListadoGuiaGuardadas = $scope.ListadoGuiaGuardadas;
            }
        };
        $scope.AnteriorG = function () {
            if ($scope.paginaActualG > 1) {
                $scope.paginaActualG -= 1;
                var a = $scope.paginaActualG * 20;
                if (a < $scope.totalRegistrosG) {
                    $scope.PagListadoGuiaGuardadas = [];
                    for (var i = a - 20; i < a; i++) {
                        $scope.PagListadoGuiaGuardadas.push($scope.ListadoGuiaGuardadas[i]);
                    }
                }
            }
            else {
                $scope.PrimerPaginaG();
            }
        };
        $scope.SiguienteG = function () {
            if ($scope.paginaActualG < $scope.totalPaginasG) {
                $scope.paginaActualG += 1;
                var a = $scope.paginaActualG * 20;
                if (a < $scope.totalRegistrosG) {
                    $scope.PagListadoGuiaGuardadas = [];
                    for (var i = a - 20; i < a; i++) {
                        $scope.PagListadoGuiaGuardadas.push($scope.ListadoGuiaGuardadas[i]);
                    }
                } else {
                    $scope.UltimaPaginaG();
                }
            } else if ($scope.paginaActualG == $scope.totalPaginasG) {
                $scope.UltimaPaginaG();
            }
        };
        $scope.UltimaPaginaG = function () {
            if ($scope.totalRegistrosG > 20 && $scope.totalPaginasG > 1) {
                $scope.paginaActualG = $scope.totalPaginasG;
                var a = $scope.paginaActualG * 20;
                $scope.PagListadoGuiaGuardadas = [];
                for (var i = a - 20; i < $scope.ListadoGuiaGuardadas.length; i++) {
                    $scope.PagListadoGuiaGuardadas.push($scope.ListadoGuiaGuardadas[i]);
                }
            }
        };
        function ResetPaginacionG() {
            $scope.totalRegistrosG = $scope.ListadoGuiaGuardadas.length;
            $scope.totalPaginasG = Math.ceil($scope.totalRegistrosG / 20);
        }
        //Paginacion Remesa Guardadas

        function DatosRequeridosConsultaRemesas() {
            $scope.MensajesErrorConslta = [];
            var modelo = $scope.CargarRemesas;
            var continuar = true;
            if ((modelo.FechaInicial == null || modelo.FechaInicial == undefined || modelo.FechaInicial == '')
                && (modelo.FechaFinal == null || modelo.FechaFinal == undefined || modelo.FechaFinal == '')
                && (modelo.NumeroInicial == null || modelo.NumeroInicial == undefined || modelo.NumeroInicial == '' || modelo.NumeroInicial == 0)
                && (modelo.NumeroFinal == null || modelo.NumeroFinal == undefined || modelo.NumeroFinal == '' || modelo.NumeroFinal == 0)
            ) {
                $scope.MensajesErrorConslta.push('Debe ingresar los filtros de números o fechas');
                continuar = false

            } else if ((modelo.NumeroInicial !== null && modelo.NumeroInicial !== undefined && modelo.NumeroInicial !== '' && modelo.NumeroInicial !== 0)
                || (modelo.NumeroFinal !== null && modelo.NumeroFinal !== undefined && modelo.NumeroFinal !== '' && modelo.NumeroFinal !== 0)
                || (modelo.FechaInicial !== null && modelo.FechaInicial !== undefined && modelo.FechaInicial !== '')
                || (modelo.FechaFinal !== null && modelo.FechaFinal !== undefined && modelo.FechaFinal !== '')

            ) {
                if ((modelo.NumeroInicial !== null && modelo.NumeroInicial !== undefined && modelo.NumeroInicial !== '' && modelo.NumeroInicial !== 0)
                    && (modelo.NumeroFinal !== null && modelo.NumeroFinal !== undefined && modelo.NumeroFinal !== '' && modelo.NumeroFinal !== 0)) {
                    if (modelo.NumeroFinal < modelo.NumeroInicial) {
                        $scope.MensajesErrorConslta.push('El número final debe ser mayor al número final');
                        continuar = false
                    }
                } else {
                    if ((modelo.NumeroInicial !== null && modelo.NumeroInicial !== undefined && modelo.NumeroInicial !== '' && modelo.NumeroInicial !== 0)) {
                        $scope.Modelo.NumeroFinal = modelo.NumeroInicial
                    } else {
                        $scope.Modelo.NumeroInicial = modelo.NumeroFinal
                    }
                }
                if ((modelo.FechaInicial !== null && modelo.FechaInicial !== undefined && modelo.FechaInicial !== '')
                    && (modelo.FechaFinal !== null && modelo.FechaFinal !== undefined && modelo.FechaFinal !== '')) {
                    if (modelo.FechaFinal < modelo.FechaInicial) {
                        $scope.MensajesErrorConslta.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if (((modelo.FechaFinal - modelo.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesErrorConslta.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }

                else {
                    if ((modelo.FechaInicial !== null && modelo.FechaInicial !== undefined && modelo.FechaInicial !== '')) {
                        $scope.Modelo.FechaFinal = modelo.FechaInicial
                    } else {
                        $scope.Modelo.FechaInicial = modelo.FechaFinal
                    }
                }
            }


            return continuar
        }
        $scope.MarcarRemesas = function (chk) {
            if (chk) {
                for (var i = 0; i < $scope.ListadoRemesasFiltradas.length; i++) {
                    $scope.ListadoRemesasFiltradas[i].Seleccionado = true
                }
            } else {
                for (var i = 0; i < $scope.ListadoRemesasFiltradas.length; i++) {
                    $scope.ListadoRemesasFiltradas[i].Seleccionado = false
                }
            }
        }
        $scope.Filtrar = function () {

            var Filtro = $scope.Filtro2
            var nFiltrosAplica = 0
            if (Filtro.NumeroInicial !== undefined && Filtro.NumeroInicial > 0) {
                nFiltrosAplica++
            }
            if (Filtro.NumeroFinal !== undefined && Filtro.NumeroFinal > 0) {
                nFiltrosAplica++
            }
            if (Filtro.FechaInicial !== undefined && Filtro.FechaInicial > 0) {
                nFiltrosAplica++
            }
            if (Filtro.FechaFinal !== undefined && Filtro.FechaFinal > 0) {
                nFiltrosAplica++
            }
            $scope.ListadoRemesasFiltradas = []
            for (var i = 0; i < $scope.ListadoRemesas.length; i++) {
                var cont = 0
                var item = $scope.ListadoRemesas[i]
                if (Filtro.NumeroInicial !== undefined && Filtro.NumeroInicial > 0) {
                    if (item.Remesa.NumeroDocumento >= Filtro.NumeroInicial) {
                        cont++
                    }
                }
                if (Filtro.NumeroFinal !== undefined && Filtro.NumeroFinal > 0) {
                    if (item.Remesa.NumeroDocumento <= Filtro.NumeroFinal) {
                        cont++
                    }
                }
                if (Filtro.FechaInicial !== undefined && Filtro.FechaInicial > 0) {
                    if (new Date(item.Remesa.Fecha) >= Filtro.FechaInicial) {
                        cont++
                    }
                }
                if (Filtro.FechaFinal !== undefined && Filtro.FechaFinal > 0) {
                    if (new Date(item.Remesa.Fecha) <= Filtro.FechaFinal) {
                        cont++
                    }
                }
                if (cont == nFiltrosAplica) {
                    item.Calcular = true;
                    $scope.ListadoRemesasFiltradas.push(item)
                }
            }
            //ResetPaginacion();
        };
        $scope.AgregarAuxiliar = function () {
            if ($scope.ModeloFuncionario !== '' && $scope.ModeloFuncionario !== undefined && $scope.ModeloFuncionario !== null && $scope.ModeloFuncionario.Codigo !== 0 && $scope.ModeloFuncionario.Codigo !== undefined) {
                var concidencias = 0
                if ($scope.ListaAuxiliares.length > 0) {
                    for (var i = 0; i < $scope.ListaAuxiliares.length; i++) {
                        if ($scope.ModeloFuncionario.Codigo === $scope.ListaAuxiliares[i].Tercero.Codigo) {
                            concidencias++
                            break;
                        }
                    }
                    if (concidencias > 0) {
                        ShowError('El funcionario ya fue ingresado')
                        $scope.ModeloFuncionario = '';
                    } else {
                        $scope.ListaAuxiliares.push({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Tercero: { Codigo: $scope.ModeloFuncionario.Codigo, Nombre: $scope.ModeloFuncionario.NombreCompleto }, Modificarfuncionario: true });
                        $scope.ModeloFuncionario = '';
                    }
                } else {
                    $scope.ListaAuxiliares.push({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Tercero: { Codigo: $scope.ModeloFuncionario.Codigo, Nombre: $scope.ModeloFuncionario.NombreCompleto }, Modificarfuncionario: true });
                    $scope.ModeloFuncionario = '';
                }
            } else {
                ShowError('Debe ingresar un funcionario valido');
            }
            $scope.ValidarDatosFuncionario();
        }
        $scope.ValidarDatosFuncionario = function () {
            for (var i = 0; i < $scope.ListaAuxiliares.length; i++) {
                if ($scope.ListaAuxiliares[i].Horas === '' || $scope.ListaAuxiliares[i].Horas === undefined || $scope.ListaAuxiliares[i].Horas === null || $scope.ListaAuxiliares[i].Horas === 0
                    || $scope.ListaAuxiliares[i].Valor === '' || $scope.ListaAuxiliares[i].Valor === undefined || $scope.ListaAuxiliares[i].Valor === null || $scope.ListaAuxiliares[i].Valor === 0) {
                    //ShowError('Debe ingresar los detalles de horas trabajadas y valor por funcionario');
                } else {
                    $scope.ListaAuxiliares[i].Modificarfuncionario = false;
                }
            }
        }
        $scope.EliminarFuncionario = function (codigo) {
            if ($scope.ListaAuxiliares.length > 0) {
                for (var i = 0; i < $scope.ListaAuxiliares.length; i++) {
                    if (codigo === $scope.ListaAuxiliares[i].Codigo) {
                        $scope.ListaAuxiliares.splice(i, 1);
                    }
                }
            }
        };
        $scope.ListaImpuestos = []
        $scope.AgregarImpuestos = function () {
            if ($scope.Impuestos == '' || $scope.Impuestos == null || $scope.Impuestos == undefined) {
                ShowError('De ingresar el impuesto')
            }
            else {
                if ($scope.ListaImpuestos.length == 0) {
                    $scope.ListaImpuestos.push($scope.Impuestos)
                    $scope.Impuestos = ''
                } else {
                    var cont = 0
                    for (var i = 0; i < $scope.ListaImpuestos.length; i++) {
                        if ($scope.Impuestos.Codigo == $scope.ListaImpuestos[i].Codigo) {
                            cont++
                            break;
                        }
                    }
                    if (cont > 0) {
                        ShowError('El impuesto que intenta agregar ya se encuentra adicionado')
                    } else {
                        $scope.ListaImpuestos.push($scope.Impuestos)
                    }
                    $scope.Impuestos = ''
                }

            }
        }
        $scope.EliminarImpuesto = function (index) {
            $scope.ListaImpuestos.splice(index, 1);
        };

        $scope.VolverMaster = function () {
            if ($scope.Modelo.Planilla.NumeroDocumento == undefined) {
                document.location.href = '#!ConsultarPlanillaGuias';
            }
            else {
                document.location.href = '#!ConsultarPlanillaGuias/' + $scope.Modelo.Planilla.NumeroDocumento;
            }
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item)
        };
        $scope.MaskMoneda = function (option) {
            MascaraMonedaGeneral($scope)
        };
        $scope.MaskMonedaGrid = function (item) {
            return MascaraMoneda(item)
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskValoresGrid = function (item) {
            if (item != undefined || item != '') {
                return MascaraValores(item);
            }
            return '';
        };
        $scope.MaskHora = function () {
            MascaraHorasGeneral($scope)
        };
        $scope.MaskHoraGrid = function (item) {
            return MascaraHora(item)
        };

        $scope.ValidarGuardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {
                //Bloqueo Pantalla
                blockUIConfig.autoBlock = true;
                blockUIConfig.delay = 0;
                blockUI.start("Generando Planilla...");
                $timeout(function () {
                    blockUI.message("Generando Planilla...");
                    Guardar();
                }, 100);
                //Bloqueo Pantalla
            }
        };

        function Guardar() {
            $scope.Modelo.Planilla.Detalles = [];
            if ($scope.ListadoGuiaGuardadas !== undefined && $scope.ListadoGuiaGuardadas !== null && $scope.ListadoGuiaGuardadas !== '') {
                if ($scope.ListadoGuiaGuardadas.length > 0) {
                    $scope.Modelo.Planilla.Detalles = $scope.ListadoGuiaGuardadas;
                }
            }
            if ($scope.ListadoRemesasFiltradas !== undefined && $scope.ListadoRemesasFiltradas !== null && $scope.ListadoRemesasFiltradas !== '') {
                for (var i = 0; i < $scope.ListadoRemesasFiltradas.length; i++) {
                    var guia = $scope.ListadoRemesasFiltradas[i];
                    if (guia.Seleccionado) {
                        $scope.Modelo.Planilla.Detalles.push({
                            Remesa: guia.Remesa,
                            RemesaPaqueteria: { ValorReexpedicion: guia.ValorReexpedicion },
                            ListaReexpedicionOficinas: guia.ListaReexpedicionOficinas
                        });
                    }
                }
            }
            //--Genera Fecha Salida con hora
            $scope.Modelo.Planilla.FechaHoraSalida = new Date($scope.Modelo.Planilla.FechaSalida);
            var Horasminutos = []
            Horasminutos = $scope.Modelo.Planilla.HoraSalida.split(':');
            if (Horasminutos.length > 0) {
                $scope.Modelo.Planilla.FechaHoraSalida.setHours(Horasminutos[0]);
                $scope.Modelo.Planilla.FechaHoraSalida.setMinutes(Horasminutos[1]);
            }
            //if (!($scope.Modelo.Planilla.ValorContraEntrega > 0)) {
            //    $scope.Modelo.Planilla.ValorContraEntrega = $scope.ContraEntrega.ValorCliente;
            //}
            //--Genera Fecha Salida con hora
            //--Detalle Remesas
            var ListadoRemesaDetalle = [];
            for (var i = 0; i < $scope.Modelo.Planilla.Detalles.length; i++) {
                var RemObj = $scope.Modelo.Planilla.Detalles[i].Remesa;
                var RemReexp = $scope.Modelo.Planilla.Detalles[i].ListaReexpedicionOficinas;
                var remesa = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    Remesa: {
                        Numero: RemObj.Numero,
                        NumeroDocumento: RemObj.NumeroDocumento
                    },
                    RemesaPaqueteria: { ValorReexpedicion: $scope.Modelo.Planilla.Detalles[i].RemesaPaqueteria.ValorReexpedicion }
                };
                if (RemReexp != null) {
                    remesa.ListaReexpedicionOficinas = [];
                    for (var j = 0; j < RemReexp.length; j++) {
                        if (RemReexp[j].Estado == 0 && RemReexp[j].TipoReexpedicion.Codigo == VALOR_CATALOGO_TIPO_REEXPEDICION_REMESA_PAQUETERIA.INTERNO) {
                            remesa.ListaReexpedicionOficinas.push({
                                CodigoRemesaPaqueteria: RemReexp[j].CodigoRemesaPaqueteria,
                                TipoReexpedicion: { Codigo: VALOR_CATALOGO_TIPO_REEXPEDICION_REMESA_PAQUETERIA.INTERNO },
                                Oficina: { Codigo: RemReexp[j].Oficina.Codigo },
                                Valor: RemReexp[j].Valor,
                                Modificar: RemReexp[j].Modificar,
                                Estado: $scope.Modelo.Planilla.Estado.Codigo
                            });
                        }
                    }
                }
                ListadoRemesaDetalle.push(remesa);
            }
            //--Detalle Remesas
            //--Detalle Impuestos
            var ListadoDetalleImpuestos = [];
            if ($scope.ListadoImpuestosFiltrado.length > 0) {
                for (var i = 0; i < $scope.ListadoImpuestosFiltrado.length; i++) {
                    var ImpArr = $scope.ListadoImpuestosFiltrado[i];
                    var impuesto = {
                        CodigoImpuesto: ImpArr.Codigo,
                        ValorTarifa: ImpArr.ValorTarifa,
                        ValorBase: ImpArr.ValorBase,
                        ValorImpuesto: ImpArr.ValorImpuesto
                    };
                    ListadoDetalleImpuestos.push(impuesto);
                }
            }
            //--Detalle Impuestos
            //--Auxiliares
            var DetallesAuxiliares = [];
            if ($scope.ListaAuxiliares.length > 0) {
                for (var i = 0; i < $scope.ListaAuxiliares.length; i++) {
                    if (!$scope.ListaAuxiliares[i].Modificarfuncionario) {
                        DetallesAuxiliares.push({
                            Funcionario: $scope.ListaAuxiliares[i].Tercero
                            , NumeroHorasTrabajadas: $scope.ListaAuxiliares[i].Horas
                            , Valor: $scope.ListaAuxiliares[i].Valor
                            , Observaciones: $scope.ListaAuxiliares[i].Observaciones
                        });
                    }
                }
            }
            //--Auxiliares
            //--Genera Anticipo
            if (parseInt($scope.Modelo.Planilla.ValorAnticipo) > CERO) {
                $scope.Modelo.CuentaPorPagar = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_PAGAR,
                    CodigoAlterno: '',
                    Fecha: $scope.Modelo.Planilla.Fecha,
                    Tercero: { Codigo: $scope.Modelo.Planilla.Vehiculo.Conductor.Codigo },
                    DocumentoOrigen: { Codigo: CODIGO_DOCUMENTO_ORIGEN_ANTICIPO },
                    CodigoDocumentoOrigen: CODIGO_DOCUMENTO_ORIGEN_ANTICIPO,
                    Numeracion: '',
                    CuentaPuc: { Codigo: 0 },
                    ValorTotal: $scope.Modelo.Planilla.ValorAnticipo,
                    Abono: 0,
                    Saldo: $scope.Modelo.Planilla.ValorAnticipo,
                    FechaCancelacionPago: $scope.Modelo.Planilla.Fecha,
                    Aprobado: 1,
                    UsuarioAprobo: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    FechaAprobo: $scope.Modelo.Planilla.Fecha,
                    Oficina: { Codigo: $scope.Modelo.Oficina.Codigo },
                    Vehiculo: { Codigo: $scope.Modelo.Planilla.Vehiculo.Codigo },
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                };
            }
            //--Genera Anticipo
            //--Genera Obj Planilla
            var PlanillaDespachos = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumento: 130,
                Planilla: {
                    Numero: $scope.Modelo.Planilla.Numero,
                    Fecha: $scope.Modelo.Planilla.Fecha,
                    FechaHoraSalida: $scope.Modelo.Planilla.FechaHoraSalida,
                    Ruta: { Codigo: $scope.Modelo.Planilla.Ruta.Codigo },
                    Vehiculo: {
                        Codigo: $scope.Modelo.Planilla.Vehiculo.Codigo,
                        Tenedor: { Codigo: $scope.Modelo.Planilla.Vehiculo.Tenedor.Codigo },
                        Conductor: { Codigo: $scope.Modelo.Planilla.Vehiculo.Conductor.Codigo }
                    },
                    Semirremolque: { Codigo: $scope.Modelo.Planilla.Semirremolque != '' && $scope.Modelo.Planilla.Semirremolque != null && $scope.Modelo.Planilla.Semirremolque != undefined ? $scope.Modelo.Planilla.Semirremolque.Codigo : 0 },
                    Cantidad: $scope.Modelo.Planilla.Cantidad,
                    Peso: $scope.Modelo.Planilla.Peso,
                    ValorFleteTransportador: $scope.Modelo.Planilla.ValorFleteTransportador,
                    ValorAnticipo: $scope.Modelo.Planilla.ValorAnticipo,
                    ValorImpuestos: $scope.Modelo.Planilla.ValorImpuestos,
                    ValorPagarTransportador: $scope.Modelo.Planilla.ValorPagarTransportador,
                    ValorFleteCliente: $scope.Modelo.Planilla.ValorFleteCliente,
                    ValorSeguroMercancia: $scope.Modelo.Planilla.ValorSeguroMercancia,
                    ValorOtrosCobros: $scope.Modelo.Planilla.ValorOtrosCobros,
                    ValorTotalCredito: $scope.Modelo.Planilla.ValorTotalCredito,
                    ValorTotalContado: $scope.Modelo.Planilla.ValorTotalContado,
                    ValorTotalAlcobro: $scope.Modelo.Planilla.ValorTotalAlcobro,
                    Observaciones: $scope.Modelo.Planilla.Observaciones,
                    Estado: { Codigo: $scope.Modelo.Planilla.Estado.Codigo },
                    TarifaTransportes: { Codigo: $scope.Sesion.UsuarioAutenticado.ManejoHabilitarLiquidacionPlanillaDespachos ? 0 : $scope.Modelo.Planilla.TarifaTransportes.Codigo },
                    TipoTarifaTransportes: { Codigo: $scope.Sesion.UsuarioAutenticado.ManejoHabilitarLiquidacionPlanillaDespachos ? 0 : $scope.TipoTarifaTransportes.Codigo },
                    RemesaPadre: $scope.Modelo.Planilla.RemesaPadre,
                    RemesaMasivo: $scope.Modelo.Planilla.RemesaMasivo,
                    ValorContraEntrega: $scope.Modelo.Planilla.ValorContraEntrega,
                    ValorBruto: $scope.Modelo.Planilla.ValorBruto,
                    Porcentaje: $scope.Modelo.Planilla.Porcentaje,
                    ValorDescuentos: $scope.Modelo.Planilla.ValorDescuentos,
                    FleteSugerido: $scope.Modelo.Planilla.FleteSugerido,
                    ValorFondoAyuda: $scope.Modelo.Planilla.ValorFondoAyuda,
                    ValorNetoPagar: $scope.Modelo.Planilla.ValorNetoPagar,
                    ValorUtilidad: $scope.Modelo.Planilla.ValorUtilidad,
                    ValorEstampilla: $scope.Modelo.Planilla.ValorEstampilla,
                    GastosAgencia: $scope.Modelo.Planilla.GastosAgencia,
                    ValorReexpedicion: $scope.Modelo.Planilla.ValorReexpedicion,
                    ValorPlanillaAdicional: $scope.Modelo.Planilla.ValorPlanillaAdicional,
                    CalcularContraentregas: $scope.Modelo.Planilla.CalcularContraentregas == true ? 1 : 0,
                    DetallesAuxiliares: DetallesAuxiliares,
                    Detalles: ListadoRemesaDetalle,
                    DetalleImpuesto: ListadoDetalleImpuestos
                },
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                Oficina: { Codigo: $scope.Modelo.Oficina.Codigo },
                CuentaPorPagar: $scope.Modelo.CuentaPorPagar
            };
            //--Genera Obj Planilla
            try {

                PlanillaGuiasFactory.Guardar(PlanillaDespachos).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > CERO) {
                                if ($scope.Modelo.Planilla.Numero == CERO) {
                                    ShowSuccess('Se guardó la planilla con el número ' + response.data.Datos);
                                }
                                else {
                                    ShowSuccess('Se modificó la planilla con el número ' + response.data.Datos);
                                }
                                if ($scope.Sesion.Empresa.GeneraManifiesto === 1 && $scope.Modelo.Planilla.Estado.Codigo == 1 && $scope.Modelo.Planilla.Ruta.TipoRuta.Codigo == 4401 && $scope.Sesion.UsuarioAutenticado.ManejoGenerarManifiestoPaqueteria) {
                                    var Manifiesto = {
                                        Numero: 0,
                                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                        NumeroDocumentoPlanilla: response.data.Datos,
                                        Tipo_Documento: { Codigo: 130 },
                                        Usuario_Crea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                        Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                                        Tipo_Manifiesto: { Codigo: CODIGO_CATALOGO_TIPO_MANIFIESTO_PLANILLA_PAQUETERIA },
                                        Valor_Anticipo: $scope.Modelo.Planilla.ValorAnticipo,
                                        Sync: true
                                    };
                                    var responseMani = ManifiestoFactory.Guardar(Manifiesto);
                                    if (responseMani.ProcesoExitoso == true) {
                                        if (responseMani.Datos > CERO) {
                                            blockUI.stop();
                                            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 100);
                                        }
                                        else {
                                            ShowError(response.statusText);
                                            blockUI.stop();
                                            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 100);
                                        }
                                    }
                                    else {
                                        ShowError(response.statusText);
                                        blockUI.stop();
                                        $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 100);
                                    }
                                }
                                blockUI.stop();
                                $timeout(function () {
                                    blockUI.stop();
                                    blockUIConfig.autoBlock = false;
                                    location.href = '#!ConsultarPlanillaGuias/' + response.data.Datos;
                                }, 100);
                            }
                            else {
                                ShowError(response.statusText);
                                blockUI.stop();
                                $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 100);
                            }
                        }
                        else {
                            ShowError(response.statusText);
                            blockUI.stop();
                            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 100);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                        blockUI.stop();
                        $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 100);
                    });

            }
            catch (e) {
                blockUI.stop();
                $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 100);
            }
        }

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            var Modelo = $scope.Modelo
            var fecha1 = new Date(Modelo.Planilla.Fecha)
            var fecha2 = new Date(Modelo.Planilla.FechaSalida)
            var fecha3 = new Date()
            fecha1.setHours('00')
            fecha1.setMinutes('00')
            fecha1.setSeconds('00')
            fecha1.setMilliseconds('00')
            fecha2.setHours('00')
            fecha2.setMinutes('00')
            fecha2.setSeconds('00')
            fecha2.setMilliseconds('00')
            fecha3.setHours('00')
            fecha3.setMinutes('00')
            fecha3.setSeconds('00')
            fecha3.setMilliseconds('00')
            if (Modelo.Planilla.Fecha == undefined || Modelo.Planilla.Fecha == '' || Modelo.Planilla.Fecha == null) {
                $scope.MensajesError.push('Debe ingresar la fecha ');
                continuar = false;
            }
            else if (fecha1 < fecha3) {
                $scope.MensajesError.push('La fecha ingresada no se puede ser menor a la fecha actual');
                continuar = false;
            }
            if (Modelo.Planilla.Vehiculo == undefined || Modelo.Planilla.Vehiculo == '') {
                $scope.MensajesError.push('Debe ingresar el vehículo');
                continuar = false;
            }
            if (Modelo.Planilla.FechaSalida == undefined || Modelo.Planilla.FechaSalida == '' || Modelo.Planilla.FechaSalida == null) {
                $scope.MensajesError.push('Debe ingresar la fecha de salida');
                continuar = false;
            }
            else if (fecha2 < fecha3) {
                $scope.MensajesError.push('La fecha de salida ingresada no se puede ser menor a la fecha actual');
                continuar = false;
            }
            if (Modelo.Planilla.HoraSalida == undefined || Modelo.Planilla.HoraSalida == '' || Modelo.Planilla.HoraSalida == null) {
                $scope.MensajesError.push('Debe ingresar la hora de salida');
                continuar = false;
            }
            else {
                var horas = Modelo.Planilla.HoraSalida.split(':')
                if (horas.length < 2) {
                    $scope.MensajesError.push('Debe ingresar una hora valida');
                    continuar = false;
                } else if (fecha2 == fecha3) {
                    if (parseInt(horas[0]) < fecha3.getHours()) {
                        $scope.MensajesError.push('La hora ingresada debe ser mayor a la actual');
                        continuar = false;
                    } else if (parseInt(horas[0]) == fecha3.getHours()) {
                        if (parseInt(horas[1]) < fecha3.getMinutes()) {
                            $scope.MensajesError.push('La hora ingresada debe ser mayor a la actual');
                            continuar = false;
                        } else {
                            if (parseInt(horas[0]) > 23 || parseInt(horas[0]) < 0 || parseInt(horas[1]) > 59 || parseInt(horas[1]) < 0) {
                                $scope.MensajesError.push('Debe ingresar una hora valida');
                                continuar = false;
                            }
                        }

                    }

                } else {
                    if (parseInt(horas[0]) > 23 || parseInt(horas[0]) < 0 || parseInt(horas[1]) > 59 || parseInt(horas[1]) < 0) {
                        $scope.MensajesError.push('Debe ingresar una hora valida');
                        continuar = false;
                    }
                }

            }

            if ($scope.contadorrecolecciones === 0 && $scope.contadorguias === 0) {
                $scope.MensajesError.push('Debe seleccionar mínimo un detalle');
                continuar = false;
            }

            if ($scope.Sesion.UsuarioAutenticado.ManejoHabilitarLiquidacionPlanillaDespachos) {
                if (Modelo.Planilla.Porcentaje == undefined || Modelo.Planilla.Porcentaje == '' || Modelo.Planilla.Porcentaje == null) {
                    $scope.MensajesError.push('Debe ingresar el porcentaje a calcular del flete');
                    continuar = false;
                }
                if (Modelo.Planilla.ValorFleteTransportador == undefined || Modelo.Planilla.ValorFleteTransportador == '' || Modelo.Planilla.ValorFleteTransportador == null) {
                    $scope.MensajesError.push('Debe ingresar el valor del flete acordado');
                    continuar = false;
                }
            }
            var CountReme = 0;
            if ($scope.ListadoGuiaGuardadas != undefined) {
                CountReme = $scope.ListadoGuiaGuardadas.length;
            }
            if ($scope.ListadoRemesasFiltradas != undefined) {
                for (var i = 0; i < $scope.ListadoRemesasFiltradas.length; i++) {
                    if ($scope.ListadoRemesasFiltradas[i].Seleccionado == true) {
                        CountReme += 1;
                    }
                }
            }
            if (CountReme <= 0) {
                $scope.MensajesError.push('Debe seleccionar al menos una remesa');
                continuar = false;
            }

            return continuar;
        }

        $scope.ConfirmacionGuardar = function () {
            if ($scope.DeshabilitarActualizar) {
                location.href = '#!ConsultarPlanillaGuias/' + $scope.Modelo.Planilla.Numero;
            } else {
                showModal('modalConfirmacionGuardar');
            }
        };

        function Obtener() {
            $scope.CargarDatosFunciones();
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Modelo.Numero
            };
            PlanillaGuiasFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.esObtener = true;
                        $scope.ListadoGuiaGuardadas = [];
                        $scope.Modelo = angular.copy(response.data.Datos);
                        $scope.Modelo.Remesa = { Cliente: {}, Ruta: {} };
                        $scope.Modelo.Planilla.FechaSalida = new Date($scope.Modelo.Planilla.FechaHoraSalida);

                        $scope.Modelo.Planilla.Semirremolque = $scope.Modelo.Planilla.Semirremolque.Codigo == 0 ? '' : $scope.CargarSemirremolqueCodigo($scope.Modelo.Planilla.Semirremolque.Codigo);//Carga Semirremolque

                        $scope.Modelo.Planilla.HoraSalida = $scope.Modelo.Planilla.FechaSalida.getHours().toString();
                        if ($scope.Modelo.Planilla.FechaSalida.getMinutes().toString().length == 1) {
                            $scope.Modelo.Planilla.HoraSalida += ':0' + $scope.Modelo.Planilla.FechaSalida.getMinutes().toString();
                        } else {
                            $scope.Modelo.Planilla.HoraSalida += ':' + $scope.Modelo.Planilla.FechaSalida.getMinutes().toString();
                        }
                        $scope.Modelo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
                        $scope.Modelo.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo };
                        $scope.Modelo.UsuarioModifica = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo };
                        if ($scope.Modelo.Planilla.Estado.Codigo == 1) {
                            $scope.DeshabilitarActualizar = true;
                        }
                        if ($scope.Modelo.Planilla.CalcularContraentregas > 0) {
                            $scope.Modelo.Planilla.CalcularContraentregas = true;
                        }

                        $scope.Modelo.Planilla.Fecha = new Date($scope.Modelo.Planilla.Fecha);
                        ////Obtiene el detalle de impuestos aplicados
                        //if ($scope.Modelo.Planilla.DetalleImpuesto !== undefined && $scope.Modelo.Planilla.DetalleImpuesto !== null && $scope.Modelo.Planilla.DetalleImpuesto !== []) {
                        //    $scope.ListaImpuestos = $scope.Modelo.Planilla.DetalleImpuesto;
                        //    //formatea el valor de cada impuesto
                        //    for (var i = 0; i < $scope.ListaImpuestos.length; i++) {
                        //        $scope.ListaImpuestos[i].ValorImpuesto = $scope.MaskValoresGrid($scope.ListaImpuestos[i].ValorImpuesto);
                        //    }
                        //}
                        //CalcularImpuesto();
                        $scope.ListadoGuiaGuardadas = []
                        if ($scope.Modelo.Planilla.Detalles !== undefined && $scope.Modelo.Planilla.Detalles !== null && $scope.Modelo.Planilla.Detalles !== []) {
                            $scope.ListadoGuiaGuardadas = $scope.Modelo.Planilla.Detalles;
                            $('#btnFiltros').click();
                            $scope.totalRegistrosG = $scope.ListadoGuiaGuardadas.length;
                            //ResetPaginacionG();
                            //$scope.PrimerPaginaG();
                        }
                        $scope.ListaAuxiliares = []
                        if ($scope.Modelo.Planilla.DetallesAuxiliares !== undefined && $scope.Modelo.Planilla.DetallesAuxiliares !== null && $scope.Modelo.Planilla.DetallesAuxiliares !== []) {
                            for (var i = 0; i < $scope.Modelo.Planilla.DetallesAuxiliares.length; i++) {
                                $scope.ListaAuxiliares.push({
                                    Tercero: $scope.Modelo.Planilla.DetallesAuxiliares[i].Funcionario,
                                    Horas: $scope.Modelo.Planilla.DetallesAuxiliares[i].NumeroHorasTrabajadas,
                                    Valor: $scope.Modelo.Planilla.DetallesAuxiliares[i].Valor,
                                    Observaciones: $scope.Modelo.Planilla.DetallesAuxiliares[i].Observaciones
                                });
                            }
                        }
                        var conductor = $scope.Modelo.Planilla.Vehiculo.Conductor;

                        $scope.Modelo.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==' + $scope.Modelo.Planilla.Oficina.Codigo);
                        $scope.Modelo.Planilla.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + $scope.Modelo.Planilla.Estado.Codigo);

                        //$scope.MostrarGuiasAsociadas()
                        try {
                            $scope.Modelo.Planilla.Vehiculo = $scope.CargarVehiculos($scope.Modelo.Planilla.Vehiculo.Codigo);
                            $scope.Modelo.Planilla.Vehiculo.Conductor = $scope.CargarTercero(conductor.Codigo);
                            $scope.ObtenerInformacionTenedor();
                        } catch (e) {
                        }
                        //--Liquidacion
                        $scope.Modelo.Planilla.ValorReexpedicion = response.data.Datos.Planilla.ValorReexpedicion;
                        $scope.Modelo.Planilla.ValorPlanillaAdicional = response.data.Datos.Planilla.ValorPlanillaAdicional;
                        $scope.Modelo.Planilla.ValorDescuentos = response.data.Datos.Planilla.ValorDescuentos;
                        $scope.Modelo.Planilla.ValorNetoPagar = response.data.Datos.Planilla.ValorNetoPagar;
                        $scope.Modelo.Planilla.ValorAnticipo = response.data.Datos.Planilla.ValorAnticipo;
                        $scope.Modelo.Planilla.ValorPagarTransportador = response.data.Datos.Planilla.ValorPagarTransportador;
                        $scope.Modelo.Planilla.ValorUtilidad = response.data.Datos.Planilla.ValorUtilidad;
                        $scope.MaskValores();
                        //--Liquidacion

                        blockUI.stop();
                        $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 100);
                    }
                    else {
                        ShowError('No se logro consultar el tarifario Compras código ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                        blockUI.stop();
                        $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; document.location.href = '#!ConsultarPlanillaGuias'; }, 100);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    blockUI.stop();
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; document.location.href = '#!ConsultarPlanillaGuias'; }, 100);
                });
        }
        $scope.EliminarGuiaGuardada = function (item) {
            var index = 0;
            for (var i = 0; i < $scope.ListadoGuiaGuardadas.length; i++) {
                if ($scope.ListadoGuiaGuardadas[i].Remesa.Numero == item.Remesa.Numero) {
                    index = i;
                    break;
                }
            }
            var ResponBorraGuia = PlanillaGuiasFactory.EliminarGuia({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Modelo.Planilla.Numero,
                Planilla: {
                    Detalles: [{ Remesa: { Numero: item.Remesa.Numero } }]
                },
                Sync: true
            });
            if (ResponBorraGuia.ProcesoExitoso == true) {
                $scope.ListadoGuiaGuardadas.splice(index, 1);
                $scope.Calcular();
                //ResetPaginacionG();
                $scope.totalRegistrosG = $scope.ListadoGuiaGuardadas.length;
                if ($scope.paginaActualG == 1) {
                    $scope.PrimerPaginaG();
                }
                else {
                    $scope.paginaActualG -= 1;
                    $scope.SiguienteG();
                }
            }
            else {
                ShowError(ResponBorraGuia.statusText);
            }
        };

        $scope.ValidarAnticipoPlanilla = function (anticipo) {
            if ($scope.Sesion.UsuarioAutenticado.ManejoHabilitarLiquidacionPlanillaDespachos) {
            } else {
                anticipo = parseInt(MascaraNumero($scope.Modelo.Planilla.ValorAnticipo))
                if (parseFloat($scope.Modelo.Planilla.Ruta.PorcentajeAnticipo) > 0) {
                    if (anticipo > ((parseFloat($scope.Modelo.Planilla.Ruta.PorcentajeAnticipo) / 100) * parseInt(MascaraNumero($scope.Modelo.Planilla.ValorFleteTransportador)))) {
                        ShowError('El anticipo no puede ser mayor a ' + ((parseFloat($scope.Modelo.Planilla.Ruta.PorcentajeAnticipo) / 100) * parseInt(MascaraNumero($scope.Modelo.Planilla.ValorFleteTransportador))))
                        $scope.Modelo.Planilla.ValorAnticipo = 0
                    } else {
                        $scope.Modelo.Planilla.ValorAnticipo = MascaraValores(anticipo)
                    }
                }
            }
        }
        var EsObtenerPlanilla = false;
        try {
            var Parametros = $routeParams.Codigo.split(',')
            if (Parametros.length > 1) {
                $scope.Modelo.Numero = Parametros[CERO];
                $scope.aplicabase = true;
                EsObtenerPlanilla = true;
            } else {
                if ($routeParams.Codigo > CERO) {
                    $scope.Modelo.Numero = $routeParams.Codigo;
                    EsObtenerPlanilla = true;
                }
                else {
                    $scope.Modelo.Numero = 0;
                    $scope.CargarDatosFunciones();
                }

            }
        } catch (e) {
            if ($routeParams.Codigo > CERO) {
                $scope.Modelo.Numero = $routeParams.Codigo;
                EsObtenerPlanilla = true;
            }
            else {
                $scope.Modelo.Numero = 0;
                $scope.CargarDatosFunciones()
            }

        }
        if (EsObtenerPlanilla) {
            //Bloqueo Pantalla
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Obteniendo Información Planilla...");
            $timeout(function () {
                blockUI.message("Obteniendo Información Planilla...");
                Obtener();
            }, 100);
            //Bloqueo Pantalla
        }
        $scope.ValidarRemesa = function (Numero) {
            $scope.Modelo.Planilla.RemesaMasivo = null
            if (Numero > 0) {
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroDocumento: Numero,
                    TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_REMESAS },
                    Estado: 1,
                    Pagina: 1,
                    RegistrosPagina: 1,
                    //UsuarioConsulta: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                };
                RemesasFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.Modelo.Planilla.RemesaMasivo = response.data.Datos[0].Numero
                            }
                            else {
                                ShowError('El número de remesa ingresado no corresponde a un remesa de masivo o se encuentra anulada')
                                $scope.Modelo.Planilla.RemesaPadre = ''
                                $scope.Modelo.Planilla.RemesaMasivo = null
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }
        $scope.MostrarContraEntregas = function () {
            $scope.ListadoRemesasContraentregas = []
            if ($scope.Modelo.Planilla.CalcularContraentregas) {

                try {
                    for (var i = 0; i < $scope.ListadoRemesasFiltradas.length; i++) {
                        var item = $scope.ListadoRemesasFiltradas[i]
                        if (item.Seleccionado) {
                            if (item.Remesa.FormaPago.Codigo == 4903) {
                                item.Calcular = true;
                                $scope.ListadoRemesasContraentregas.push(item)
                            }
                        }
                    }
                } catch (e) {
                }
                if ($scope.ListadoGuiaGuardadas !== undefined && $scope.ListadoGuiaGuardadas !== null && $scope.ListadoGuiaGuardadas !== []) {
                    for (var i = 0; i < $scope.ListadoGuiaGuardadas.length; i++) {
                        var item = $scope.ListadoGuiaGuardadas[i];
                        if (item.Remesa.FormaPago.Codigo == 4903) {//Contraentregas
                            //if (item.ValorReexpedicion == 0) {
                            item.Calcular = true;
                            $scope.ListadoRemesasContraentregas.push(item);
                            //}
                        }
                    }
                }
            }
        }
        //----------------------- Recalcular Reexpedicion Remesas Liquidacion -----------------------//
        $scope.RecalcularReexpedicionRemesas = function () {
            //--Valida si tiene remesas seleccionadas o guardadas
            var CountReme = 0;
            if ($scope.ListadoGuiaGuardadas != undefined) {
                CountReme = $scope.ListadoGuiaGuardadas.length;
            }
            if ($scope.ListadoRemesasFiltradas != undefined) {
                for (var i = 0; i < $scope.ListadoRemesasFiltradas.length; i++) {
                    if ($scope.ListadoRemesasFiltradas[i].Seleccionado == true) {
                        CountReme += 1;
                    }
                }
            }
            //--Valida si tiene remesas seleccionadas o guardadas

            if ($scope.PorcentajeReexpedicion != undefined && $scope.PorcentajeReexpedicion != '' > 0 && CountReme > 0) {
                //--genera recalculo Remesas Filtradas
                var TMPRemsasFiltradas = angular.copy($scope.ListadoRemesasFiltradas);
                var TMPRemsasGuaradadas = angular.copy($scope.ListadoGuiaGuardadas);
                var ProcesoExitoso = true;
                if ($scope.ListadoRemesasFiltradas != undefined) {
                    for (var i = 0; i < $scope.ListadoRemesasFiltradas.length; i++) {
                        if ($scope.ListadoRemesasFiltradas[i].Seleccionado == true) {
                            var ValorMaximoReexp = 0;
                            var ValorReexpRemesa = 0;
                            var DivisionReexpedicion = 0;
                            var ValorReexCalculado = 0;
                            var ListaReexpedicion = $scope.ListadoRemesasFiltradas[i].ListaReexpedicionOficinas;
                            if (ListaReexpedicion != null) {
                                for (var j = 0; j < ListaReexpedicion.length; j++) {
                                    if (ListaReexpedicion[j].Estado == 1) {
                                        ValorReexpRemesa += ListaReexpedicion[j].Valor;
                                    }
                                    else {
                                        if (ListaReexpedicion[j].TipoReexpedicion.Codigo == VALOR_CATALOGO_TIPO_REEXPEDICION_REMESA_PAQUETERIA.INTERNO) {
                                            DivisionReexpedicion += 1;
                                        }
                                    }
                                }
                                if (DivisionReexpedicion > 0 && $scope.ListadoRemesasFiltradas[i].Remesa.ValorFleteCliente > 0) {
                                    ValorMaximoReexp = (($scope.ListadoRemesasFiltradas[i].Remesa.ValorFleteCliente * $scope.PorcentajeReexpedicion) / 100) - ValorReexpRemesa;
                                    if (ValorMaximoReexp > 0) {
                                        ValorReexCalculado = Math.round(ValorMaximoReexp / DivisionReexpedicion);
                                        for (var j = 0; j < ListaReexpedicion.length; j++) {
                                            if (ListaReexpedicion[j].Estado == 0 && ListaReexpedicion[j].TipoReexpedicion.Codigo == VALOR_CATALOGO_TIPO_REEXPEDICION_REMESA_PAQUETERIA.INTERNO) {
                                                ListaReexpedicion[j].Valor = ValorReexCalculado;
                                                ListaReexpedicion[j].Modificar = 1;
                                            }
                                        }
                                        $scope.ListadoRemesasFiltradas[i].ValorReexpedicion = ListaReexpedicion.sum("Valor");
                                    }
                                    else {
                                        ProcesoExitoso = false;
                                        ShowError('Las reexpediciones de la remesa <strong>' + $scope.ListadoRemesasFiltradas[i].Remesa.NumeroDocumento + '</strong> exceden el valor máximo de reexpedición segun el porcentaje reexpedición diligenciado');
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if (ProcesoExitoso == false) {
                        $scope.ListadoRemesasFiltradas = TMPRemsasFiltradas;
                        $scope.totalRegistros = $scope.ListadoRemesasFiltradas.length;
                        //ResetPaginacion();
                        return;
                    }
                }
                //--genera recalculo Remesas Filtradas
                //--genera recalculo Remesas Guardaddas
                if ($scope.ListadoGuiaGuardadas != undefined) {
                    for (var i = 0; i < $scope.ListadoGuiaGuardadas.length; i++) {
                        var ValorMaximoReexp = 0;
                        var ValorReexpRemesa = 0;
                        var DivisionReexpedicion = 0;
                        var ValorReexCalculado = 0;
                        var ListaReexpedicion = $scope.ListadoGuiaGuardadas[i].ListaReexpedicionOficinas;
                        if (ListaReexpedicion != null) {
                            for (var j = 0; j < ListaReexpedicion.length; j++) {
                                if (ListaReexpedicion[j].Estado == 1) {
                                    ValorReexpRemesa += ListaReexpedicion[j].Valor;
                                }
                                else {
                                    if (ListaReexpedicion[j].TipoReexpedicion.Codigo == VALOR_CATALOGO_TIPO_REEXPEDICION_REMESA_PAQUETERIA.INTERNO) {
                                        DivisionReexpedicion += 1;
                                    }
                                }
                            }
                            if (DivisionReexpedicion > 0 && $scope.ListadoGuiaGuardadas[i].Remesa.ValorFleteCliente > 0) {
                                ValorMaximoReexp = (($scope.ListadoGuiaGuardadas[i].Remesa.ValorFleteCliente * $scope.PorcentajeReexpedicion) / 100) - ValorReexpRemesa;
                                if (ValorMaximoReexp > 0) {
                                    ValorReexCalculado = Math.round(ValorMaximoReexp / DivisionReexpedicion);
                                    for (var j = 0; j < ListaReexpedicion.length; j++) {
                                        if (ListaReexpedicion[j].Estado == 0 && ListaReexpedicion[j].TipoReexpedicion.Codigo == VALOR_CATALOGO_TIPO_REEXPEDICION_REMESA_PAQUETERIA.INTERNO) {
                                            ListaReexpedicion[j].Valor = ValorReexCalculado;
                                            ListaReexpedicion[j].Modificar = 1;
                                        }
                                    }
                                    $scope.ListadoGuiaGuardadas[i].RemesaPaqueteria.ValorReexpedicion = ListaReexpedicion.sum("Valor");
                                }
                                else {
                                    ProcesoExitoso = false;
                                    ShowError('Las reexpediciones de la remesa <strong>' + $scope.ListadoGuiaGuardadas[i].Remesa.NumeroDocumento + '</strong> exceden el valor máximo de reexpedición segun el porcentaje reexpedición diligenciado');
                                    break;
                                }
                            }
                        }
                    }
                    if (ProcesoExitoso == false) {
                        $scope.ListadoGuiaGuardadas = TMPRemsasGuaradadas;
                        $scope.totalRegistrosG = $scope.ListadoGuiaGuardadas.length;
                        //ResetPaginacionG();
                        //$scope.PrimerPaginaG();
                        return;
                    }
                }
                //--genera recalculo Remesas Guardaddas
                $scope.Calcular();
            }
            else {
                ShowError('Se debe diligencia un porcentaje mayor a cero y debe existir almenos una remesa seleccionada');
            }
        };
        //----------------------- Recalcular Reexpedicion Remesas Liquidacion -----------------------//
    }]);