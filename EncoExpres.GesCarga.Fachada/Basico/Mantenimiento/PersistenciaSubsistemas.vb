﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico
Imports EncoExpres.GesCarga.Repositorio.Basico

Namespace Basico
    Public NotInheritable Class PersistenciaSubsistemas
        Implements IPersistenciaBase(Of SubSistemas)
        Public Function Consultar(filtro As SubSistemas) As IEnumerable(Of SubSistemas) Implements IPersistenciaBase(Of SubSistemas).Consultar
            Return New RepositorioSubSistemas().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As SubSistemas) As Long Implements IPersistenciaBase(Of SubSistemas).Insertar
            Return New RepositorioSubSistemas().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As SubSistemas) As Long Implements IPersistenciaBase(Of SubSistemas).Modificar
            Return New RepositorioSubSistemas().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As SubSistemas) As SubSistemas Implements IPersistenciaBase(Of SubSistemas).Obtener
            Return New RepositorioSubSistemas().Obtener(filtro)
        End Function
    End Class
End Namespace
