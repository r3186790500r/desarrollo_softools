﻿Imports Newtonsoft.Json

Namespace Gesphone

    ''' <summary>
    ''' Clase que tiene la informacion correspondiente a las etiquetas preimpresas de una guia
    ''' </summary>
    <JsonObject>
    Public Class SyncEtiquetaPreimpresa

        <JsonProperty>
        Public Property Etiqueta As String

        <JsonProperty>
        Public Property CodigoEmpresa As Integer

        <JsonProperty>
        Public Property NumeroRemesa As Integer

        <JsonProperty>
        Public Property CodigoUsuario As Integer

        <JsonProperty>
        Public Property Eliminar As Boolean

    End Class

    ''' <summary>
    ''' Clase que tiene la informacion correspondiente a las etiquetas preimpresas de una guia
    ''' </summary>
    <JsonObject>
    Public Class SyncRemesaRelacionPlanila

        <JsonProperty>
        Public Property Numero As Long

        <JsonProperty>
        Public Property NumeroDocumento As Long

    End Class

End Namespace

