﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Fidelizacion.Documentos
Imports EncoExpres.GesCarga.Negocio.Fidelizacion.Documentos

<Authorize>
Public Class DetalleDespachosPuntosVehiculosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As DetalleDespachosPuntosVehiculos) As Respuesta(Of IEnumerable(Of DetalleDespachosPuntosVehiculos))
        Return New LogicaDetalleDespachosPuntosVehiculos(CapaPersistenciaDetalleDespachosPuntosVehiculos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As DetalleDespachosPuntosVehiculos) As Respuesta(Of DetalleDespachosPuntosVehiculos)
        Return New LogicaDetalleDespachosPuntosVehiculos(CapaPersistenciaDetalleDespachosPuntosVehiculos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As DetalleDespachosPuntosVehiculos) As Respuesta(Of Long)
        Return New LogicaDetalleDespachosPuntosVehiculos(CapaPersistenciaDetalleDespachosPuntosVehiculos).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As DetalleDespachosPuntosVehiculos) As Respuesta(Of Boolean)
        Return New LogicaDetalleDespachosPuntosVehiculos(CapaPersistenciaDetalleDespachosPuntosVehiculos).Anular(entidad)
    End Function
End Class
