﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Mantenimiento
Imports EncoExpres.GesCarga.Repositorio.Mantenimiento

Namespace Mantenimiento

    Public NotInheritable Class PersistenciaDetallePlanEquipoMantenimiento
        Implements IPersistenciaBase(Of DetallePlanEquipoMantenimiento)
        Public Function Consultar(filtro As DetallePlanEquipoMantenimiento) As IEnumerable(Of DetallePlanEquipoMantenimiento) Implements IPersistenciaBase(Of DetallePlanEquipoMantenimiento).Consultar
            Return New RepositorioDetallePlanEquipoMantenimiento().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As DetallePlanEquipoMantenimiento) As Long Implements IPersistenciaBase(Of DetallePlanEquipoMantenimiento).Insertar
            Return New RepositorioDetallePlanEquipoMantenimiento().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As DetallePlanEquipoMantenimiento) As Long Implements IPersistenciaBase(Of DetallePlanEquipoMantenimiento).Modificar
            Return New RepositorioDetallePlanEquipoMantenimiento().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As DetallePlanEquipoMantenimiento) As DetallePlanEquipoMantenimiento Implements IPersistenciaBase(Of DetallePlanEquipoMantenimiento).Obtener
            Return New RepositorioDetallePlanEquipoMantenimiento().Obtener(filtro)
        End Function

        Public Function Anular(entidad As DetallePlanEquipoMantenimiento) As Boolean
            Return New RepositorioDetallePlanEquipoMantenimiento().Anular(entidad)
        End Function

    End Class
End Namespace
