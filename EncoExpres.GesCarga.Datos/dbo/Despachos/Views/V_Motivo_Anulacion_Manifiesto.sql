﻿PRINT 'V_Motivo_Anulacion_Manifiesto'
GO

DROP VIEW V_Motivo_Anulacion_Manifiesto
GO

CREATE VIEW V_Motivo_Anulacion_Manifiesto
AS
	SELECT
		EMPR_Codigo,
		Codigo,
		CATA_Codigo,
		Campo1 AS Nombre,
		USUA_Codigo_Crea,
		Fecha_Crea,
		USUA_Modifica,
		Fecha_Modifica
	FROM
		Valor_Catalogos
	WHERE
		CATA_Codigo = 90
GO