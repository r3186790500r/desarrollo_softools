﻿DROP PROCEDURE  gsp_obtener_manifiesto_rndc
GO
CREATE PROCEDURE  gsp_obtener_manifiesto_rndc
(
	@par_EMPR_Codigo smallint,
	@par_ENMC_Numero Numeric
)
AS                                
BEGIN                                
	-- Codigo RNDC Tipo Identificacion                                  
	DECLARE @TIID_RNDC_CED CHAR = 'C';--Codigo RNDC Tipo Identificacion Cedula                                  
	DECLARE @TIID_RNDC_NIT CHAR = 'N';--Codigo RNDC Tipo Identificacion NIT                                  
                                  
	SELECT DISTINCT                                
	CONCAT (EMPR.Numero_Identificacion,  IIF(EMPR.Digito_Chequeo IS NULL OR  EMPR.Digito_Chequeo = 0, '', EMPR.Digito_Chequeo) ) AS NUMNITEMPRESATRANSPORTE,                                 
                                  
	--TENEDOR                                
	CASE TENE.CATA_TIID_Codigo  WHEN 101 THEN @TIID_RNDC_CED   WHEN 102 THEN @TIID_RNDC_NIT   ELSE @TIID_RNDC_NIT END  As TENE_CODTIPOIDTERCERO,                                  
	CONCAT (TENE.Numero_Identificacion,  IIF(TENE.Digito_Chequeo IS NULL OR  TENE.Digito_Chequeo = 0, '', TENE.Digito_Chequeo) ) AS TENE_NUMIDTERCERO,                                  
	IIF(TENE.Razon_Social IS NULL OR TENE.Razon_Social = '', TENE.Nombre, TENE.Razon_Social ) AS TENE_NOMIDTERCERO,                                  
	ISNULL(TENE.Apellido1, '') AS TENE_PRIMERAPELLIDOIDTERCERO,                                  
	ISNULL(TENE.Apellido2, '') AS TENE_SEGUNDOAPELLIDOIDTERCERO,                                  
	CIUD_TENE.Codigo_Alterno AS TENE_CODSEDETERCERO,                                  
	CIUD_TENE.Nombre AS TENE_NOMSEDETERCERO,                                  
	ISNULL(TENE.Telefonos, '') AS TENE_NUMTELEFONOCONTACTO,                                  
	ISNULL(TENE.Celulares, '') AS TENE_NUMCELULARPERSONA,                                  
	TENE.Direccion AS TENE_NOMENCLATURADIRECCION,           
	TENE.Reportar_RNDC AS TENE_Reportar_RNDC,                                
	TENE.Codigo AS TENE_Codigo,                                
                                
                                
	--PROPIETARIO                                
	CASE PROP.CATA_TIID_Codigo  WHEN 101 THEN @TIID_RNDC_CED   WHEN 102 THEN @TIID_RNDC_NIT   ELSE @TIID_RNDC_NIT END  As PROP_CODTIPOIDTERCERO,                                  
	CONCAT (PROP.Numero_Identificacion,  IIF(PROP.Digito_Chequeo IS NULL OR  PROP.Digito_Chequeo = 0, '', PROP.Digito_Chequeo) ) AS PROP_NUMIDTERCERO,                                  
	IIF(PROP.Razon_Social IS NULL OR PROP.Razon_Social = '', PROP.Nombre, PROP.Razon_Social ) AS PROP_NOMIDTERCERO,                                  
	ISNULL(PROP.Apellido1, '') AS PROP_PRIMERAPELLIDOIDTERCERO,                                  
	ISNULL(PROP.Apellido2, '') AS PROP_SEGUNDOAPELLIDOIDTERCERO,                                  
	CIUD_PROP.Codigo_Alterno AS PROP_CODSEDETERCERO,                                  
	CIUD_PROP.Nombre AS PROP_NOMSEDETERCERO,                                  
	ISNULL(PROP.Telefonos, '') AS PROP_NUMTELEFONOCONTACTO,                                  
	ISNULL(PROP.Celulares, '') AS PROP_NUMCELULARPERSONA,                                  
	PROP.Direccion AS PROP_NOMENCLATURADIRECCION,                                
	PROP.Reportar_RNDC AS PROP_Reportar_RNDC,                                
	PROP.Codigo AS PROP_Codigo,                                
                                
	--CONDUCTOR                                
	CASE COND.CATA_TIID_Codigo  WHEN 101 THEN @TIID_RNDC_CED   WHEN 102 THEN @TIID_RNDC_NIT   ELSE @TIID_RNDC_NIT END  As COND_CODTIPOIDTERCERO,                                  
	COND.Numero_Identificacion AS COND_NUMIDTERCERO,                                  
	IIF(COND.Razon_Social IS NULL OR COND.Razon_Social = '', COND.Nombre, COND.Razon_Social ) AS COND_NOMIDTERCERO,                                  
	ISNULL(COND.Apellido1, '') AS COND_PRIMERAPELLIDOIDTERCERO,                                  ISNULL(COND.Apellido2, '') AS COND_SEGUNDOAPELLIDOIDTERCERO,                                  
	CIUD_COND.Codigo_Alterno AS COND_CODSEDETERCERO,                                  
	CIUD_COND.Nombre AS COND_NOMSEDETERCERO,                                  
	ISNULL(COND.Telefonos, '') AS COND_NUMTELEFONOCONTACTO,                                  
	ISNULL(COND.Celulares, '') AS COND_NUMCELULARPERSONA,                                  
	COND.Direccion AS COND_NOMENCLATURADIRECCION,                                
	TCOND.Numero_Licencia AS   COND_NUMLICENCIACONDUCCION,                                
	CALC.Campo2 AS COND_CODCATEGORIALICENCIACONDUCCION,            
	TCOND.Fecha_Vencimiento_Licencia AS FECHAVENCIMIENTOLICENCIA,           
	COND.Reportar_RNDC AS COND_Reportar_RNDC,                                
	COND.Codigo AS COND_Codigo,          
        
        
	--VEHICULIO                                
	VEHI.Placa AS NUMPLACA,                     
	SEMI.Placa AS NUMPLACAREMOLQUE,                    
	1 AS UNIDADMEDIDACAPACIDAD,                                
	TIVE.Campo4 AS CODCONFIGURACIONUNIDADCARGA,                                
	MAVE.Codigo_Alterno as CODMARCAVEHICULOCARGA,                                
	LIVE.Codigo_Alterno AS CODLINEAVEHICULOCARGA,                   
	VEHI.Modelo AS ANOFABRICACIONVEHICULOCARGA,                                
	convert(int, VEHI.Peso_Tara) AS PESOVEHICULOVACIO,                                
	convert(int, SEMI.Peso_Vacio) AS PESOSEMIRREMOLQUEVACIO,                                
	COVE.Codigo_Alterno AS CODCOLORVEHICULOCARGA,                                
	0 AS CODTIPOCARROCERIA,                                
	VEHI.Numero_Serie AS NUMCHASIS,                                
	convert(int, VEHI.Capacidad_Kilos) AS CAPACIDADUNIDADCARGA,                        
	VEHI.Referencia_SOAT as NUMSEGUROSOAT,                          
	VEHI.Fecha_Vencimiento_SOAT as FECHAVENCIMIENTOSOAT,                        
	CONCAT (SOAT.Numero_Identificacion,  IIF(SOAT.Digito_Chequeo IS NULL OR  SOAT.Digito_Chequeo = 0, '', SOAT.Digito_Chequeo) ) AS NUMNITASEGURADORASOAT ,                       
	VEHI.Reportar_RNDC AS VEHI_Reportar_RNDC,                                
	VEHI.Codigo AS VEHI_Codigo,        
	SEMI.Reportar_RNDC AS SEMI_Reportar_RNDC,                                
	SEMI.Codigo AS SEMI_Codigo,        
        
	--MANIFIESTO                                
	--CASE ENMA.CATA_TIMA_Codigo                                  
	-- WHEN 8807 THEN --Vacío                                  
	-- 'W'                                   
	-- ELSE                                  
	-- 'G'                                 
	--END AS CODOPERACIONTRANSPORTE ,    
	CASE WHEN TIMA.Campo2 = 'G' THEN 'G'    
	WHEN TIMA.Campo2 = 'D' THEN 'D'    
	ELSE 'G' END  AS CODOPERACIONTRANSPORTE ,       
	ENMA.Cantidad_viajes_dia AS VIAJESDIA,
	--TRANSBORDO
	ISNULL(TICM.Campo2, 'C') AS TIPOCUMPLIDOMANIFIESTO,
	ISNULL(MOSM.Campo2, '') AS MOTIVOSUSPENSIONMANIFIESTO,
	ISNULL(COSM.Campo2, '') AS CONSECUENCIASUSPENSIONMANIFIESTO,
	ISNULL(MATR.Numero_Documento, 0) AS MANNROMANIFIESTOTRANSBORDO,
	--TRANSBORDO
	ENMA.Numero_Documento as NUMMANIFIESTOCARGA,                             
	ENMA.Fecha_Crea as FECHAEXPEDICIONMANIFIESTO,                                
	CIOR.Codigo_Alterno+'000' AS CODMUNICIPIOORIGENMANIFIESTO,                             
	CIDE.Codigo_Alterno+'000'  AS CODMUNICIPIODESTINOMANIFIESTO,                                
	CONVERT(INTEGER,ENMA.Valor_Flete) AS VALORFLETEPACTADOVIAJE,                                
	CONVERT(money,ISNULL(DIPD.Valor_Tarifa,DECI.Valor_Tarifa)*1000) AS RETENCIONICAMANIFIESTOCARGA,                                
	CONVERT(INTEGER, ENMA.Valor_Anticipo) AS VALORANTICIPOMANIFIESTO,                                
	CONVERT(money,DIPD2.Valor_Tarifa*1000) AS RETENCIONFUENTEMANIFIESTO,                                
	CIDE.Codigo_Alterno+'000' AS CODMUNICIPIOPAGOSALDO,                                
	'R' AS CODRESPONSABLEPAGOCARGUE,                                
	'D' AS CODRESPONSABLEPAGODESCARGUE,                                
	ENMA.Fecha_Cumplimiento AS FECHAPAGOSALDOMANIFIESTO,                                
	ENMA.Observaciones AS OBSERVACIONES,                          
	ENCP.Fecha as FECHAENTREGADOCUMENTOS,                          
	0 as VALORADICIONALHORASCARGUE,                          
	0 as VALORADICIONALHORASDESCARGUE,      
	CASE WHEN ENMA.Aceptacion_Electronica IS NULL THEN 'NO'      
	WHEN ENMA.Aceptacion_Electronica = 0 THEN 'NO'      
	WHEN ENMA.Aceptacion_Electronica = 1 THEN 'SI'      
	END AS ACEPTACIONELECTRONICA      
	               
	FROM Encabezado_Manifiesto_Carga AS ENMA                                 
                                
	LEFT JOIN Empresas AS EMPR                                
	ON ENMA.EMPR_Codigo = EMPR.Codigo                      
                                
	LEFT JOIN Terceros AS TENE                                
	ON ENMA.EMPR_Codigo = TENE.EMPR_Codigo                                
	AND ENMA.TERC_Codigo_Tenedor = TENE.Codigo                                
                                  
	LEFT JOIN Ciudades AS CIUD_TENE ON                                  
	TENE.EMPR_Codigo = CIUD_TENE.EMPR_Codigo                                  
	AND TENE.CIUD_Codigo = CIUD_TENE.Codigo

	LEFT JOIN Terceros AS PROP                                
	ON ENMA.EMPR_Codigo = PROP.EMPR_Codigo                                
	AND ENMA.TERC_Codigo_Propietario = PROP.Codigo                                
                                  
	LEFT JOIN Ciudades AS CIUD_PROP ON                                  
	PROP.EMPR_Codigo = CIUD_PROP.EMPR_Codigo                                  
	AND PROP.CIUD_Codigo = CIUD_PROP.Codigo                                  
                                
	LEFT JOIN Terceros AS COND                                
	ON ENMA.EMPR_Codigo = COND.EMPR_Codigo                                
	AND ENMA.TERC_Codigo_Conductor = COND.Codigo                 
                                
	LEFT JOIN Tercero_Conductores AS TCOND                                
	ON ENMA.EMPR_Codigo = TCOND.EMPR_Codigo                                
	AND ENMA.TERC_Codigo_Conductor = TCOND.TERC_Codigo                           
	           
	LEFT JOIN Valor_Catalogos AS CALC                                
	ON TCOND.EMPR_Codigo = CALC.EMPR_Codigo                                
	AND TCOND.CATA_CALC_Codigo = CALC.Codigo                      
    
	LEFT JOIN Valor_Catalogos AS  TIMA ON     
	ENMA.EMPR_Codigo = TIMA.EMPR_Codigo AND    
	ENMA.CATA_TIMA_Codigo = TIMA.Codigo    
    
                                  
	LEFT JOIN Ciudades AS CIUD_COND ON                            
	COND.EMPR_Codigo = CIUD_COND.EMPR_Codigo                                  
	AND COND.CIUD_Codigo_Identificacion = CIUD_COND.Codigo                                  
                                
	LEFT JOIN Vehiculos AS VEHI ON                                  
	ENMA.EMPR_Codigo = VEHI.EMPR_Codigo                              
	AND ENMA.VEHI_Codigo = VEHI.Codigo                          
                    
	LEFT JOIN Semirremolques AS SEMI ON                                  
	ENMA.EMPR_Codigo = SEMI.EMPR_Codigo                                  
	AND (ENMA.SEMI_Codigo = SEMI.Codigo OR VEHI.SEMI_Codigo = SEMI.Codigo)                  
                        
	LEFT JOIN Terceros AS SOAT                                
	ON VEHI.EMPR_Codigo = SOAT.EMPR_Codigo                                
	AND VEHI.TERC_Codigo_Aseguradora_SOAT = SOAT.Codigo                          
                        
	LEFT JOIN Marca_Vehiculos AS MAVE ON                                  
	VEHI.EMPR_Codigo = MAVE.EMPR_Codigo                                  
	AND VEHI.MAVE_Codigo = MAVE.Codigo                                  
                                
	LEFT JOIN Linea_Vehiculos AS LIVE ON                                  
	VEHI.EMPR_Codigo = LIVE.EMPR_Codigo                                  
	AND VEHI.LIVE_Codigo = LIVE.Codigo                                 
	AND MAVE.Codigo = LIVE.MAVE_Codigo                                                                 
	LEFT JOIN Color_Vehiculos AS COVE ON                                  
	VEHI.EMPR_Codigo = COVE.EMPR_Codigo                                  
	AND VEHI.COVE_Codigo = COVE.Codigo                                 
                                
	LEFT JOIN Valor_Catalogos AS TIVE ON                                  
	VEHI.EMPR_Codigo = TIVE.EMPR_Codigo                 
	AND VEHI.CATA_TIVE_Codigo = TIVE.Codigo                                  
                                
	LEFT JOIN Rutas AS RUTA ON                                  
	ENMA.EMPR_Codigo = RUTA.EMPR_Codigo                                  
	AND ENMA.RUTA_Codigo = RUTA.Codigo                                
                                
	LEFT JOIN Ciudades AS CIOR ON                                  
	RUTA.EMPR_Codigo = CIOR.EMPR_Codigo                                  
	AND RUTA.CIUD_Codigo_Origen = CIOR.Codigo                                
                                
	LEFT JOIN Ciudades AS CIDE ON                    
	RUTA.EMPR_Codigo = CIDE.EMPR_Codigo                                  
	AND RUTA.CIUD_Codigo_Destino = CIDE.Codigo                                
                          
	LEFT JOIN Encabezado_Planilla_Despachos AS ENPD ON                                  
	ENPD.EMPR_Codigo = ENMA.EMPR_Codigo                                  
	AND ENPD.ENMC_Numero = ENMA.Numero                          
                          
	LEFT JOIN Detalle_Impuestos_Planilla_Despachos AS DIPD ON                          
	DIPD.EMPR_Codigo = ENPD.EMPR_Codigo                          
	AND DIPD.ENPD_Numero = ENPD.Numero                          
	AND DIPD.ENIM_Codigo = 21                          
          
	LEFT JOIN Detalle_Ciudad_Impuestos AS DECI ON                          
	DECI.EMPR_Codigo = RUTA.EMPR_Codigo                          
	AND DECI.CIUD_Codigo = RUTA.CIUD_Codigo_Origen                          
	AND DECI.ENIM_Codigo = 21                    
              
	LEFT JOIN Detalle_Impuestos_Planilla_Despachos AS DIPD2 ON                          
	DIPD2.EMPR_Codigo = ENPD.EMPR_Codigo                          
	AND DIPD2.ENPD_Numero = ENPD.Numero                          
	AND DIPD2.ENIM_Codigo = 20                    
 
	LEFT JOIN Encabezado_Cumplido_Planilla_Despachos AS ENCP on
	ENMA.EMPR_Codigo = ENCP.EMPR_Codigo
	AND ENMA.Numero = ENCP.ENMC_Numero

	--LEFT JOIN Encabezado_Cumplido_Planilla_Despachos AS ENCP on
	--ENCP.EMPR_Codigo = ENMA.EMPR_Codigo
	--and ENCP.ENPD_Numero = ENMA.ENPD_Numero

	--Tipo Cumplido Manifiesto
	LEFT JOIN Valor_Catalogos AS TICM ON
	ENCP.EMPR_Codigo = TICM.EMPR_Codigo
	AND ENCP.CATA_TICM_Codigo = TICM.Codigo

	--Motivo Suspension Manifiesto
	LEFT JOIN Valor_Catalogos AS MOSM ON
	ENCP.EMPR_Codigo = MOSM.EMPR_Codigo
	AND ENCP.CATA_MOSM_Codigo = MOSM.Codigo

	--Consecuencia Suspension Manifiesto
	LEFT JOIN Valor_Catalogos AS COSM ON
	ENCP.EMPR_Codigo = COSM.EMPR_Codigo
	AND ENCP.CATA_COSM_Codigo = COSM.Codigo

	--Manifiesto Transbordo Original

	LEFT JOIN Encabezado_Manifiesto_Carga MATR ON
	ENMA.EMPR_Codigo = MATR.EMPR_Codigo
	AND ENMA.ENMC_Numero_Transbordo = MATR.Numero
	                
	WHERE ENMA.EMPR_Codigo = @par_EMPR_Codigo                                
	AND ENMA.Numero = @par_ENMC_Numero
END
GO