﻿Print 'V_Tipo_Tarifa_Transporte_Carga'
GO
DROP VIEW V_Tipo_Tarifa_Transporte_Carga
GO
CREATE VIEW V_Tipo_Tarifa_Transporte_Carga
AS
	SELECT TTTC.EMPR_Codigo, TTTC.TATC_Codigo, TTTC.Codigo, VACA.CATA_Codigo, VACA.Campo1 As Nombre, TATC.Nombre As NombreTarifa 
	FROM Tipo_Tarifa_Transporte_Carga TTTC, Tarifa_Transporte_Carga TATC, Valor_Catalogos VACA
	WHERE TTTC.EMPR_Codigo = TATC.EMPR_Codigo
	AND TTTC.TATC_Codigo = TATC.Codigo
	AND TTTC.EMPR_Codigo = VACA.EMPR_Codigo
	AND VACA.Codigo IN (TTTC.CATA_CAVE_Codigo, TTTC.CATA_CONT_Codigo, TTTC.CATA_DECO_Codigo, TTTC.CATA_RPVJ_Codigo,
	TTTC.CATA_RPVK_Codigo, TTTC.CATA_TIVE_Codigo)
	AND VACA.Codigo <> 0
GO