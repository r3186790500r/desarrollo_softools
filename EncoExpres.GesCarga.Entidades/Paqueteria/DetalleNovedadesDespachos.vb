﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios
Imports EncoExpres.GesCarga.Entidades.Basico.Facturacion

Namespace Despachos.Paqueteria
    <JsonObject>
    Public Class DetalleNovedadesDespachos
        Inherits BaseDocumento
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleNovedadesDespachos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "ID")
            NumeroPlanilla = Read(lector, "ENPD_Numero")
            NumeroDocumentoPlanilla = Read(lector, "ENPD_Numero_Documento")
            NumeroRemesa = Read(lector, "ENRE_Numero")
            NumeroDocumentoRemesa = Read(lector, "ENRE_Numero_Documento")
            Novedad = New NovedadesDespachos With {.Codigo = Read(lector, "NODE_Codigo"), .Nombre = Read(lector, "Nombre_Novedad"), .Conceptos = New NovedadesConceptos With {.ConceptoLiquidacion = New ConceptoLiquidacionPlanillaDespacho With {.Codigo = Read(lector, "CLPD_Codigo"), .ValorFijo = Read(lector, "Valor_Fijo")}}}
            ConceptoVenta = New ConceptoFacturacion With {.Codigo = Read(lector, "COVE_Codigo"), .Nombre = Read(lector, "COVE_Nombre"), .Operacion = Read(lector, "OperacionVenta")}
            Observaciones = Read(lector, "Observaciones")
            ValorCompra = Read(lector, "Valor_Compra")
            ValorVenta = Read(lector, "Valor_Venta")
            ValorCosto = Read(lector, "Valor_Costo")
            Proveedor = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Proveedor"), .Nombre = Read(lector, "Nombre_Proveedor")}
            UsuarioCrea = New Usuarios With {.Codigo = Read(lector, "USUA_Codigo_Crea"), .Nombre = Read(lector, "Nombre_Usuario_Crea")}
            FechaCrea = Read(lector, "Fecha_Crea")
            Anulado = Read(lector, "Anulado")
            ConsultaNovedadesDespacho = Read(lector, "ConsultaNovedadesDespacho")
            If ConsultaNovedadesDespacho = 1 Then
                TotalRegistros = Read(lector, "TotalRegistros")
                NumeroManifiesto = Read(lector, "ENMC_Numero")
                NumeroDocumentoManifiesto = Read(lector, "ENMC_Numero_Documento")
            End If

        End Sub

        <JsonProperty>
        Public Property ConsultaNovedadesDespacho As Short
        <JsonProperty>
        Public Property NumeroPlanilla As Integer
        <JsonProperty>
        Public Property NumeroRemesa As Integer
        <JsonProperty>
        Public Property NumeroManifiesto As Integer
        <JsonProperty>
        Public Property NumeroDocumentoPlanilla As Integer
        <JsonProperty>
        Public Property NumeroDocumentoRemesa As Integer
        <JsonProperty>
        Public Property NumeroDocumentoManifiesto As Integer
        <JsonProperty>
        Public Property Novedad As NovedadesDespachos
        <JsonProperty>
        Public Property ConceptoVenta As ConceptoFacturacion
        <JsonProperty>
        Public Property Proveedor As Terceros
        <JsonProperty>
        Public Property ValorCompra As Double
        <JsonProperty>
        Public Property ValorVenta As Double
        <JsonProperty>
        Public Property ValorCosto As Double

    End Class
End Namespace
