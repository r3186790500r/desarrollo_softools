CREATE PROCEDURE [dbo].[gsp_Consultar_Detalle_Legalizacion_Gastos_SIESA_PLEXA]    
(    
@par_EMPR_Codigo SMALLINT,      
@par_ELGC_Numero NUMERIC,  
@par_COND_Codigo NUMERIC  
)    
AS     
BEGIN    
 SELECT PLUC.Codigo_Cuenta,    
 CLGC.Codigo AS Codigo_Concepto,   
 CLGC.Nombre AS Nombre_Concepto,
 CASE WHEN CLGC.Operacion = 2 THEN DLGC.Valor    
 ELSE 0 END AS Valor_Debito,    
 CASE WHEN CLGC.Operacion = 1 THEN DLGC.Valor    
 ELSE 0 END AS Valor_Credito,    
 CLGC.Nombre +' '+ ISNULL(DLGC.Observaciones,'') Observaciones,    
 TERC.Numero_Identificacion AS Identificacion_Tercero,  
 DLGC.ENDC_Codigo, 
 DLGC.ENPD_Numero
   
 FROM Detalle_Legalizacion_Gastos_Conductor DLGC     
  
 INNER JOIN Conceptos_Legalizacion_Gastos_Conductor CLGC    
 ON CLGC.EMPR_Codigo = DLGC.EMPR_Codigo    
 AND CLGC.Codigo = DLGC.CLGC_Codigo    
    
 INNER JOIN Plan_Unico_Cuentas PLUC    
 ON PLUC.EMPR_Codigo = CLGC.EMPR_Codigo    
 AND PLUC.Codigo = CLGC.PLUC_Codigo    
    
 INNER JOIN Terceros TERC    
 ON DLGC.EMPR_Codigo = TERC.EMPR_Codigo  
 AND DLGC.TERC_Codigo_Proveedor = TERC.Codigo    
    
 WHERE DLGC.EMPR_Codigo = @par_EMPR_Codigo   
 AND DLGC.ELGC_Numero = @par_ELGC_Numero  
 AND DLGC.TERC_Codigo_Conductor = @par_COND_Codigo  
    
END     
GO


