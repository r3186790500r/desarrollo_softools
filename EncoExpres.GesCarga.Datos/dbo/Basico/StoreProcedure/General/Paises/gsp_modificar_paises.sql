﻿	/*Crea: Geferson Latorre
	Fecha_Crea: 13/08/2018
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	

PRINT 'gsp_modificar_paises'
GO
DROP PROCEDURE gsp_modificar_paises
GO
CREATE PROCEDURE gsp_modificar_paises 
(    
@par_EMPR_Codigo SMALLINT,    
@par_Codigo NUMERIC,    
@par_Codigo_Alterno VARCHAR(20),    
@par_Nombre VARCHAR(50),    
@par_MONE_Codigo NUMERIC,    
@par_Estado SMALLINT,    
@par_Nombre_Corto CHAR(5) = NULL,  
@par_USUA_Codigo_Modifica SMALLINT    
)    
AS    
BEGIN    
UPDATE Paises    
SET    
Codigo_Alterno = @par_Codigo_Alterno,    
Nombre = @par_Nombre,    
MONE_Codigo = @par_MONE_Codigo,    
Estado = @par_Estado,    
Nombre_Corto = ISNULL(@par_Nombre_Corto, ''),   
USUA_Codigo_Modifica =  @par_USUA_Codigo_Modifica,    
Fecha_Modifica = GETDATE()    
WHERE    
EMPR_Codigo = @par_EMPR_Codigo    
AND Codigo = @par_Codigo    
    
SELECT @par_Codigo AS Codigo    
    
END    
GO
