﻿Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Contabilidad.Procesos
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources

Namespace Contabilidad
    Public NotInheritable Class LogicaCierreContableDocumentos
        Inherits LogicaBase(Of CierreContableDocumentos)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of CierreContableDocumentos)

        Public Sub New(capaPersistenciaCierreContableDocumentos As IPersistenciaBase(Of CierreContableDocumentos))
            _persistencia = capaPersistenciaCierreContableDocumentos
        End Sub

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos de la notificacion de correo.
        ''' </summary>

        Public Overrides Function Consultar(filtro As CierreContableDocumentos) As Respuesta(Of IEnumerable(Of CierreContableDocumentos))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of CierreContableDocumentos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of CierreContableDocumentos))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As CierreContableDocumentos) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            If entidad.Codigo = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As CierreContableDocumentos) As Respuesta(Of CierreContableDocumentos)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of CierreContableDocumentos)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of CierreContableDocumentos)(consulta)
        End Function

    End Class
End Namespace