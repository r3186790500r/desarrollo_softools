﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */

	Print 'gsp_modificar_encabezado_planilla_entregada'
DROP PROCEDURE gsp_modificar_encabezado_planilla_entregada	
GO

CREATE PROCEDURE gsp_modificar_encabezado_planilla_entregada	(  
@par_EMPR_Codigo NUMERIC,
@par_Numero NUMERIC,
@par_Fecha DATE,
@par_VEHI_Codigo NUMERIC,
@par_TERC_Codigo_Conductor NUMERIC,
@par_Cantidad_Total NUMERIC,
@par_Peso_Total NUMERIC,
@par_Estado NUMERIC,
@par_USUA_Codigo_Modifica NUMERIC,
@par_OFIC_Codigo NUMERIC,
@par_Numeracion NUMERIC,
@par_Observaciones VARCHAR(250),
@par_Guias_Seleccionadas VARCHAR(500)
)    
AS BEGIN   


DECLARE @tblGuias table (Numero numeric)    
INSERT INTO @tblGuias    
SELECT * FROM dbo.Func_Dividir_String ('1000038,1000041,1000042,1000084', ',')    
    
-- De las guias traidos sacar los posibles planillados    
DECLARE @tblGuiasPlanilladas table (Numero numeric)    
INSERT INTO @tblGuiasPlanilladas     
SELECT Numero FROM Encabezado_Remesas WHERE EMPR_Codigo = @par_EMPR_Codigo    
AND Numero IN (SELECT Numero FROM @tblGuias)    
AND TIDO_Codigo = 110  
AND ISNULL(ENPE_Numero,0)>0
AND ENPE_Numero <> @par_Numero
    
-- De las guias traidos sacar los posibles anulados    
DECLARE @tblGuiasAnulados table (Numero numeric)    
INSERT INTO @tblGuiasAnulados     
SELECT Numero FROM Encabezado_Remesas WHERE EMPR_Codigo = @par_EMPR_Codigo     
AND Numero in (SELECT Numero FROM @tblGuias)    
AND Anulado = 1    

-- Guardar el listado de tiquetes planillados en un varchar para armar mensaje    
DECLARE @varGuiasPlanillados VARCHAR(500) = ''    
SELECT DISTINCT @varGuiasPlanillados = ISNULL(CAST((SELECT DISTINCT CONVERT(VARCHAR,Numero) + ', '     
FROM @tblGuiasPlanilladas FOR XML PATH(''))as varchar(max)), '')    
FROM @tblGuiasPlanilladas     
    
-- Guardar el listado de tiquetes anulados en un varchar para armar mensaje    
DECLARE @varGuiasAnulados VARCHAR(500) = ''    
SELECT DISTINCT @varGuiasAnulados = ISNULL(CAST((SELECT DISTINCT CONVERT(VARCHAR,Numero) + ', '     
FROM @tblGuiasAnulados FOR XML PATH(''))as varchar(max)), '')    
FROM @tblGuiasAnulados     
    
IF ISNULL(@varGuiasPlanillados,'') <> '' OR ISNULL(@varGuiasAnulados,'') <> '' BEGIN    
 DECLARE @varMensajeProceso VARCHAR(MAX) = ''    
 SET @varMensajeProceso = 'La planilla no se puede generar debido a que las guias presentan las siguientes inconsistencias: ' + char(10) + char(10)    
     
 IF ISNULL(@varGuiasPlanillados,'') <> '' BEGIN    
  SET @varMensajeProceso += '-> Guia No. ' + @varGuiasPlanillados + ' planillado(s).' + char(10)    
 END    
 IF ISNULL(@varGuiasAnulados,'') <> '' BEGIN    
  SET @varMensajeProceso += '-> Guia No. ' + @varGuiasAnulados + ' anulado(s).'    
 END    


END    
ELSE    
BEGIN    


DELETE Detalle_Planilla_Entregas WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENPE_Numero = @par_Numero

UPDATE Encabezado_Planilla_Entregas SET
EMPR_Codigo = @par_EMPR_Codigo ,
Fecha = @par_Fecha,
VEHI_Codigo  = @par_VEHI_Codigo ,
TERC_Codigo_Conductor = @par_TERC_Codigo_Conductor ,
Cantidad_Total = @par_Cantidad_Total ,
Peso_Total = @par_Peso_Total ,
Estado = @par_Estado ,
Fecha_Modifica = GETDATE(),
USUA_Codigo_Modifica = @par_USUA_Codigo_Modifica  ,
OFIC_Codigo = @par_OFIC_Codigo , 
Numeracion =  @par_Numeracion ,
Observaciones = @par_Observaciones 
WHERE EMPR_Codigo= @par_EMPR_Codigo 
AND Numero= @par_Numero  


SELECT  Numero_Documento,Numero FROM Encabezado_Planilla_Entregas WHERE Numero = @par_Numero 
     
  END  
END
GO