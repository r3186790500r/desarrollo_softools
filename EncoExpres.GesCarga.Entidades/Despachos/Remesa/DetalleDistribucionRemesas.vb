﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios

Namespace Despachos.Remesa
    <JsonObject>
    Public Class DetalleDistribucionRemesas
        Inherits BaseDocumento
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleDistribucionRemesas"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            Codigo = Read(lector, "ID")
            Producto = New ProductoTransportados With {.Codigo = Read(lector, "PRTR_Codigo"), .Nombre = Read(lector, "Nombre_Producto")}
            Cantidad = Read(lector, "Cantidad")
            Peso = Read(lector, "Peso")
            Destinatario = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Destinatario"),
                                              .TipoIdentificacion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIID_Codigo")},
                                              .NumeroIdentificacion = Read(lector, "Numero_Identificacion_Destinatario"),
                                              .NombreCompleto = Read(lector, "Nombre_Destinatario"),
                                              .Ciudad = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Destinatario"), .Nombre = Read(lector, "Nombre_Ciudad")},
                                              .Barrio = Read(lector, "Barrio_Destinatario"),
                                              .Direccion = Read(lector, "Direccion_Destinatario"),
                                              .CodigoPostal = Read(lector, "Codigo_Postal_Destinatario"),
                                              .Telefonos = Read(lector, "Telefonos_Destinatario")}
            ZonaDestinatario = New Zonas With {.Codigo = Read(lector, "ZOCI_Codigo_Destinatario"), .Nombre = Read(lector, "Nombre_Zona")}
            SitioEntrega = New SitiosCargueDescargue With {.Codigo = Read(lector, "SICD_Codigo_Entrega"), .Nombre = Read(lector, "Nombre_Sitio_Entrega")}
            NombreRecibe = Read(lector, "Nombre_Recibe")
            TelefonoRecibe = Read(lector, "Telefonos_Recibe")
            UsuarioModifica = New Usuarios With {.Codigo = Read(lector, "USUA_Codigo_Modifica")}

            Obtener = Read(lector, "Obtener")
            If Obtener = 1 Then
                FechaRecibe = Read(lector, "Fecha_Recibe")
                CantidadRecibe = Read(lector, "Cantidad_Recibe")
                PesoRecibe = Read(lector, "Peso_Recibe")
                CodigoTipoIdentificacionRecibe = Read(lector, "CATA_TIID_Codigo_Recibe")
                NumeroIdentificacionRecibe = Read(lector, "Numero_Identificacion_Recibe")
                Firma = Read(lector, "Firma_Recibe")
                ObservacionesRecibe = Read(lector, "Observaciones_Recibe")
            Else
                CodigoEmpresa = Read(lector, "EMPR_Codigo")
                NumeroRemesa = Read(lector, "ENRE_Numero")
                UnidadEmpaque = New UnidadEmpaque With {.Codigo = Read(lector, "UEPT_Codigo"), .Nombre = Read(lector, "Nombre_Unidad_Empaque")}
                DocumentoCliente = Read(lector, "Documento_Cliente")
                FechaDocumentoCliente = Read(lector, "Fecha_Documento_Cliente")
                Observaciones = Read(lector, "Observaciones_Entrega")
                UsuarioModifica = New Usuarios With {.Codigo = Read(lector, "USUA_Codigo_Modifica")}
                UsuarioCrea = New Usuarios With {.Codigo = Read(lector, "USUA_Codigo_Crea"), .Nombre = Read(lector, "Nombre_Usuario_Crea")}
                FechaCrea = Read(lector, "Fecha_Crea")
            End If
            EstadoDistribucionRemesas = New ValorCatalogos With {.Codigo = Read(lector, "CATA_ESDR_Codigo"), .Nombre = Read(lector, "Nombre_Estado")}

        End Sub

        <JsonProperty>
        Public Property NumeroRemesa As Integer
        <JsonProperty>
        Public Property Producto As ProductoTransportados
        <JsonProperty>
        Public Property UnidadEmpaque As UnidadEmpaque
        <JsonProperty>
        Public Property Cantidad As Double
        <JsonProperty>
        Public Property Peso As Double
        <JsonProperty>
        Public Property DocumentoCliente As String
        <JsonProperty>
        Public Property FechaDocumentoCliente As Date
        <JsonProperty>
        Public Property Destinatario As Terceros
        <JsonProperty>
        Public Property ZonaDestinatario As Zonas
        <JsonProperty>
        Public Property SitioEntrega As SitiosCargueDescargue
        <JsonProperty>
        Public Property EstadoDistribucionRemesas As ValorCatalogos
        <JsonProperty>
        Public Property FechaRecibe As Date
        <JsonProperty>
        Public Property CantidadRecibe As Double
        <JsonProperty>
        Public Property PesoRecibe As Double
        <JsonProperty>
        Public Property CodigoTipoIdentificacionRecibe As Integer
        <JsonProperty>
        Public Property NumeroIdentificacionRecibe As Integer
        <JsonProperty>
        Public Property NombreRecibe As String
        <JsonProperty>
        Public Property TelefonoRecibe As String
        <JsonProperty>
        Public Property ObservacionesRecibe As String
        <JsonProperty>
        Public Property Fotografia As FotoDetalleDistribucionRemesas
        <JsonProperty>
        Public Property Firma As Byte()

    End Class
End Namespace
