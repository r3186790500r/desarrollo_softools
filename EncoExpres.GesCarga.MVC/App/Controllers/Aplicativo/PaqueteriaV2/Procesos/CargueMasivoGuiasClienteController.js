﻿EncoExpresApp.controller("CargueMasivoGuiasClienteCtrl", ['$scope', '$timeout', '$linq', 'TercerosFactory', 'blockUI', 'RemesaGuiasFactory', 'ValorCatalogosFactory', 'TercerosFactory', 'ProductoTransportadosFactory', 'CiudadesFactory', 'SitiosTerceroClienteFactory', 'blockUIConfig', 'OficinasFactory', 'ZonasFactory', 'CargueMasivoGuiasFactory', 'TarifarioVentasFactory','SitiosCargueDescargueFactory',
    function ($scope, $timeout, $linq, TercerosFactory, blockUI, RemesaGuiasFactory, ValorCatalogosFactory, TercerosFactory, ProductoTransportadosFactory, CiudadesFactory, SitiosTerceroClienteFactory, blockUIConfig, OficinasFactory, ZonasFactory, CargueMasivoGuiasFactory, TarifarioVentasFactory, SitiosCargueDescargueFactory) {
        console.clear()

        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        $scope.Buscando = false;
        $scope.continuar = false
        $scope.VerErrores = false;
        $scope.DeshabilitarCliente = false;
        $scope.ResultadoSinRegistros = '';
        $scope.MensajesError = [];
        $scope.ListadoEstados = [];
        $scope.ListadoRutasPuntosGestion = [];
        $scope.ListaHorariosEntrega = [];
        $scope.OficinaUsuario = OficinasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo, Sync: true }).Datos;
        $scope.Cliente = '';
        $scope.ListadoZonasGeneral = [];
        $scope.ListadoSitiosEntregaGeneral = [];
        $scope.ManejoReexpedicionOficina = $scope.Sesion.UsuarioAutenticado.ManejoReexpedicionPorOficina;
        var ListaNuevosTerceros = [];
        var UltimoConsecutivoTercero = { Codigo: 0 }
        var strListaNuevosTerceros = '';
        $scope.Modelo = {
            Fecha: new Date(),
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,

            Codigo: 0,
            CodigoAlterno: 0,
            Nombre: '',
            Estado: 0,
            Remesa: {
                TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_REMESAS },
                Numero: 0,
                TipoRemesaDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA },
                DetalleTarifaVenta: {},
                ProductoTransportado: '',
                Fecha: new Date(),
                Remitente: { Direccion: '' },
                Destinatario: { Direccion: '' },
            },
        }
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 20;
        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Procesos' }, { Nombre: 'Cargar Archivo Guías Cliente' }];
        $scope.ListadoZonasGeneral = ZonasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Sync: true }).Datos;
        $scope.ListadoSitiosEntregaGeneral = SitiosCargueDescargueFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,Estado: ESTADO_ACTIVO, Sync: true }).Datos

        $scope.ObtenerTarifario = function () {
            if ($scope.Cliente.Codigo != undefined) {
                PantallaBloqueoTarifario(ObtenerTarifario)
            }
        }

        $scope.TarifarioCliente = [];
        function ObtenerTarifario() {
            if ($scope.Cliente != undefined && $scope.Cliente != null && $scope.Cliente != '') {
                var filtroTarifarioCliente = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.Cliente.Codigo,
                    CodigoLineaNegocio: CODIGO_LINEA_NEGOCIO_TRANPSORTE_CARGA_PAQUETERIA,
                    TarifarioProveedores: false,
                    TarifarioClientes: true
                }
                filtroTarifarioCliente.Sync = true;
                var ResponseTarifarioCliente = TercerosFactory.Consultar(filtroTarifarioCliente);

                if (ResponseTarifarioCliente != undefined) {
                    if (ResponseTarifarioCliente.ProcesoExitoso === true) {
                        if (ResponseTarifarioCliente.Datos.length > 0) {
                            $scope.TarifarioCliente = ResponseTarifarioCliente.Datos
                            console.log(ResponseTarifarioCliente.Datos)
                            blockUI.stop()
                        } else {
                            ShowError('El tarifario de este cliente no tiene Tarifas Paquetería')
                            blockUI.stop()
                        }
                    } else {
                        ShowError(ResponseTarifarioCliente.MensajeOperacion)
                        blockUI.stop()
                    }
                } else {
                    ShowError('El tarifario de este cliente no tiene Tarifas Paquetería')
                    blockUI.stop()
                }
            } else {
                blockUI.stop()
            }
            
        }

        $scope.AutocompleteSitiosClienteDescargue2 = function (value, cliente, ciudad) {
            $scope.ListadoSitiosDescargueAlterno2 = []
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = SitiosTerceroClienteFactory.ConsultarOrdenServicio({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Cliente: { Codigo: cliente.Codigo },
                        CiudadCargue: { Codigo: ciudad.Codigo },
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    for (var i = 0; i < Response.Datos.length; i++) {
                        Response.Datos[i].Codigo = Response.Datos[i].SitioCliente.Codigo
                    }
                    $scope.ListadoSitiosDescargueAlterno2 = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoSitiosDescargueAlterno2)
                }
            }
            return $scope.ListadoSitiosDescargueAlterno2
        }

        $scope.AutocompleteZonasCiudades = function (value, ciudad) {
            $scope.ListadoZonasCiudades = []

            /*Cargar Autocomplete de propietario*/
            blockUIConfig.autoBlock = false;
            var Response = ZonasFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Ciudad: { Codigo: ciudad.Codigo },
                Sync: true
            })

            $scope.ListadoZonasCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoZonasCiudades)

            return $scope.ListadoZonasCiudades
        }
        /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_FORMA_PAGO_VENTAS } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoFormaPago = [];
                    if (response.data.Datos.length > 0) {
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            if (response.data.Datos[i].Codigo != CODIGO_CATALOGO_FORMA_PAGO_VENTAS_NO_APLICA) {
                                $scope.ListadoFormaPago.push(response.data.Datos[i])
                            }
                        }
                    }
                    else {
                        $scope.ListadoFormaPago = []
                    }
                }
            }, function (response) {
            });
        /*Cargar el combo de tipo entrega*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_ENTREGA_REMESA_PAQUETERIA } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoEntregaRemesaPaqueteria = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoEntregaRemesaPaqueteria = response.data.Datos;
                    }
                }
            }, function (response) {
            });
        /*Cargar Autocomplete de CLIENTES*/
        $scope.ListaClientes = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CLIENTE, Sync: true }).Datos



        /*Cargar Autocomplete de CLIENTES*/
        $scope.AutocompleteClientes = function (value) {
            var ListadoClientes = []
            var ResponseListadoTerceros = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CLIENTE, ValorAutocomplete: value, Sync: true });

            if (ResponseListadoTerceros.ProcesoExitoso === true) {
                ListadoClientes = []
                if (ResponseListadoTerceros.Datos.length > 0) {
                    ListadoClientes = ResponseListadoTerceros.Datos;
                }
                else {
                    ListadoClientes = [];
                }
            }

            return ListadoClientes
        }
        /*Autocomplete de productos*/
        ProductoTransportadosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, AplicaTarifario: -1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoProductoTransportados = []
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoProductoTransportados = response.data.Datos;
                    }
                    else {
                        $scope.ListadoProductoTransportados = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar el combo de tipo identificaciones*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IDENTIFICACION_TERCERO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoIdentificacion = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoIdentificacion = response.data.Datos;
                    }
                    else {
                        $scope.ListadoTipoIdentificacion = []
                    }
                }
            }, function (response) {
            });
        /*Cargar el combo de líneas negocio paquetería*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 216 } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaLineasNegocioPaqueteria = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListaLineasNegocioPaqueteria = response.data.Datos;
                    }
                    else {
                        $scope.ListaLineasNegocioPaqueteria = []
                    }
                }
            }, function (response) {
            });


        /*Cargar el combo de Horarios Entrega*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 217 } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaHorariosEntrega = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListaHorariosEntrega = response.data.Datos;
                    }
                    else {
                        $scope.ListaHorariosEntrega = []
                    }
                }
            }, function (response) {
            });
        $scope.ListaRecogerOficinaDestino = [];
        $scope.ListaRecogerOficinaDestino = [
            { Codigo: 0, Nombre: 'NO' },
            { Codigo: 1, Nombre: 'SI' }
        ];
        /*Ciudades*/
        CiudadesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoCiudades = response.data.Datos;
                    }
                    else {
                        $scope.ListadoCiudades = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        /*Estado GUÍA*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_ESTADO_REMESA_PAQUETERIA } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaEstadosGuia = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListaEstadosGuia = response.data.Datos;

                        $scope.Modelo.OficinaOrigen = $scope.Sesion.UsuarioAutenticado.Oficinas;
                        $scope.Modelo.OficinaDestino = $scope.Sesion.UsuarioAutenticado.Oficinas;
                        $scope.Modelo.OficinaActual = $scope.Sesion.UsuarioAutenticado.Oficinas;

                        if ($scope.CodigoEstadoGuia !== undefined && $scope.CodigoEstadoGuia !== null) {
                            $scope.Modelo.EstadoGuia = $linq.Enumerable().From($scope.ListaEstadosGuia).First('$.Codigo ==' + $scope.CodigoEstadoGuia);

                        } else {
                            if ($scope.Sesion.UsuarioAutenticado.Oficinas.CodigoTipoOficina == TIPO_OFICINA_PROPIA) {
                                $scope.Modelo.EstadoGuia = $linq.Enumerable().From($scope.ListaEstadosGuia).First('$.Codigo ==' + ESTADO_GUIA_OFICINA_ORIGEN);

                                $scope.Modelo.OficinaOrigen = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                $scope.Modelo.OficinaDestino = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                if ($scope.ListaOficinas !== undefined && $scope.ListaOficinas !== null) {
                                    if ($scope.ListaOficinas.length > 0) {
                                        $scope.Modelo.OficinaDestino = $linq.Enumerable().From($scope.ListaOficinas).First('$.Codigo ==' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
                                    }
                                }
                                $scope.Modelo.OficinaActual = $scope.Sesion.UsuarioAutenticado.Oficinas;

                            }
                            if ($scope.Sesion.UsuarioAutenticado.Oficinas.CodigoTipoOficina == TIPO_OFICINA_CLIENTE) {
                                $scope.Modelo.EstadoGuia = $linq.Enumerable().From($scope.ListaEstadosGuia).First('$.Codigo ==' + ESTADO_GUIA_RECOGIDA);


                                $scope.Modelo.OficinaOrigen = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                $scope.Modelo.OficinaDestino = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                if ($scope.ListaOficinas !== undefined && $scope.ListaOficinas !== null) {
                                    if ($scope.ListaOficinas.length > 0) {
                                        $scope.Modelo.OficinaDestino = $linq.Enumerable().From($scope.ListaOficinas).First('$.Codigo ==' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
                                    }
                                }

                                $scope.Modelo.OficinaActual = $scope.Sesion.UsuarioAutenticado.Oficinas;

                            }
                            if ($scope.Sesion.UsuarioAutenticado.Oficinas.CodigoTipoOficina == TIPO_OFICINA_RECEPTORIA) {
                                $scope.Modelo.EstadoGuia = $linq.Enumerable().From($scope.ListaEstadosGuia).First('$.Codigo ==' + ESTADO_GUIA_RECOGIDA);

                                $scope.Modelo.OficinaOrigen = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                $scope.Modelo.OficinaDestino = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                if ($scope.ListaOficinas !== undefined && $scope.ListaOficinas !== null) {
                                    if ($scope.ListaOficinas.length > 0) {
                                        $scope.Modelo.OficinaDestino = $linq.Enumerable().From($scope.ListaOficinas).First('$.Codigo ==' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
                                    }
                                }
                                $scope.Modelo.OficinaActual = $scope.Sesion.UsuarioAutenticado.Oficinas;
                            }
                        }


                    }
                    else {
                        $scope.ListaEstadosGuia = []
                    }
                }
            }, function (response) {
            });
        /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
        //Paginacion
        $scope.PrimerPagina = function () {
            if ($scope.totalRegistros > 20) {
                $scope.DataActual = []
                $scope.paginaActual = 1
                for (var i = 0; i < 20; i++) {
                    $scope.DataActual.push($scope.DataArchivo[i])
                }
            }
        }
        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual -= 1
                var a = $scope.paginaActual * 20
                if (a < $scope.totalRegistros) {
                    $scope.DataActual = []
                    for (var i = a - 20; i < a; i++) {
                        $scope.DataActual.push($scope.DataArchivo[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual += 1
                var a = $scope.paginaActual * 20
                if (a < $scope.totalRegistros) {
                    $scope.DataActual = []
                    for (var i = a - 20; i < a; i++) {
                        $scope.DataActual.push($scope.DataArchivo[i])
                    }
                } else {
                    $scope.UltimaPagina()
                }
            } else if ($scope.paginaActual == $scope.totalPaginas) {
                $scope.UltimaPagina()
            }
        }
        $scope.UltimaPagina = function () {
            if ($scope.totalRegistros > 20 && $scope.totalPaginas > 1) {
                $scope.paginaActual = $scope.totalPaginas
                var a = $scope.paginaActual * 20
                $scope.DataActual = []
                for (var i = a - 20; i < $scope.DataArchivo.length; i++) {
                    $scope.DataActual.push($scope.DataArchivo[i])
                }
            }
        }

        //Paginacion MOdal de errores

        $scope.PrimerPaginaModalError = function () {
            if ($scope.totalRegistrosErrores > 20) {
                $scope.DataErrorActual = []
                $scope.paginaActualError = 1
                for (var i = 0; i < 20; i++) {
                    $scope.DataErrorActual.push($scope.ListaErrores[i])
                }
            }
        }
        $scope.AnteriorModalError = function () {
            if ($scope.paginaActualError > 1) {
                $scope.paginaActualError -= 1
                var a = $scope.paginaActualError * 20
                if (a < $scope.totalRegistrosErrores) {
                    $scope.DataErrorActual = []
                    for (var i = a - 20; i < a; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.SiguienteModalError = function () {
            if ($scope.paginaActualError < $scope.totalPaginasErrores) {
                $scope.paginaActualError += 1
                var a = $scope.paginaActualError * 20
                if (a < $scope.totalRegistrosErrores) {
                    $scope.DataErrorActual = []
                    for (var i = a - 20; i < a; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
                else {
                    $scope.UltimaPaginaModalError()

                }
            } else if ($scope.paginaActualError == $scope.totalPaginasErrores) {
                $scope.UltimaPaginaModalError()
            }
        }
        $scope.UltimaPaginaModalError = function () {
            if ($scope.totalRegistrosErrores > 20 && $scope.totalPaginasErrores > 1) {
                $scope.paginaActualError = $scope.totalPaginasErrores
                var a = $scope.paginaActualError * 20
                $scope.DataErrorActual = []
                for (var i = a - 20; i < $scope.ListaErrores.length; i++) {
                    $scope.DataErrorActual.push($scope.ListaErrores[i])
                }
            }
        }


        $scope.PrimerPaginaNoGuardados = function () {
            if ($scope.totalRegistrosNoGuardadas > 20) {
                $scope.ProgramacionesNoGuardadasActual = []
                $scope.paginaActualNoGuardadas = 1
                for (var i = 0; i < 20; i++) {
                    $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                }
            }
        }
        $scope.AnteriorNoGuardados = function () {
            if ($scope.paginaActualNoGuardadas > 1) {
                $scope.paginaActualNoGuardadas -= 1
                var a = $scope.paginaActualNoGuardadas * 20
                if (a < $scope.totalRegistrosNoGuardadas) {
                    $scope.ProgramacionesNoGuardadasActual = []
                    for (var i = a - 20; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.SiguienteNoGuardados = function () {
            if ($scope.paginaActualNoGuardadas < $scope.totalPaginasNoGuardadas) {
                $scope.paginaActualNoGuardadas += 1
                var a = $scope.paginaActualNoGuardadas * 20
                if (a < $scope.totalRegistrosNoGuardadas) {
                    $scope.ProgramacionesNoGuardadasActual = []
                    for (var i = a - 20; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                    }
                }
                else {
                    $scope.UltimaPaginaNoGuardados()

                }
            } else if ($scope.paginaActualNoGuardadas == $scope.totalPaginasNoGuardadas) {
                $scope.UltimaPaginaNoGuardados()
            }
        }
        $scope.UltimaPaginaNoGuardados = function () {
            if ($scope.totalRegistrosNoGuardadas > 20 && $scope.totalPaginasNoGuardadas > 1) {
                $scope.paginaActualNoGuardadas = $scope.totalPaginasNoGuardadas
                var a = $scope.paginaActualNoGuardadas * 20
                $scope.ProgramacionesNoGuardadasActual = []
                for (var i = a - 20; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                    $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                }
            }
        }


        $scope.PrimerPaginaNoGuardadosRemplazo = function () {
            if ($scope.totalRegistrosNoGuardadasRemplazo > 20) {
                $scope.ProgramacionesNoGuardadasActualRemplazo = []
                $scope.paginaActualNoGuardadasRemplazo = 1
                for (var i = 0; i < 20; i++) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                }
            }
        }
        $scope.AnteriorNoGuardadosRemplazo = function () {
            if ($scope.paginaActualNoGuardadasRemplazo > 1) {
                $scope.paginaActualNoGuardadasRemplazo -= 1
                var a = $scope.paginaActualNoGuardadasRemplazo * 20
                if (a < $scope.totalRegistrosNoGuardadasRemplazo) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo = []
                    for (var i = a - 20; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.SiguienteNoGuardadosRemplazo = function () {
            if ($scope.paginaActualNoGuardadasRemplazo < $scope.totalPaginasNoGuardadasRemplazo) {
                $scope.paginaActualNoGuardadasRemplazo += 1
                var a = $scope.paginaActualNoGuardadasRemplazo * 20
                if (a < $scope.totalRegistrosNoGuardadasRemplazo) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo = []
                    for (var i = a - 20; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                    }
                }
                else {
                    $scope.UltimaPaginaNoGuardadosRemplazo()

                }
            } else if ($scope.paginaActualNoGuardadasRemplazo == $scope.totalPaginasNoGuardadasRemplazo) {
                $scope.UltimaPaginaNoGuardadosRemplazo()
            }
        }
        $scope.UltimaPaginaNoGuardadosRemplazo = function () {
            if ($scope.totalRegistrosNoGuardadasRemplazo > 20 && $scope.totalPaginasNoGuardadasRemplazo > 1) {
                $scope.paginaActualNoGuardadasRemplazo = $scope.totalPaginasNoGuardadasRemplazo
                var a = $scope.paginaActualNoGuardadasRemplazo * 20
                $scope.ProgramacionesNoGuardadasActualRemplazo = []
                for (var i = a - 20; i < $scope.ProgramacionesNoGuardadasRemplazo.length; i++) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                }
            }
        }


        $scope.MarcadDesmarcarTodo = function (item) {
            if (item) {
                for (var i = 0; i < $scope.ListaErrores.length; i++) {
                    $scope.ListaErrores[i].Omitido = true
                }
            }
            else {
                for (var i = 0; i < $scope.ListaErrores.length; i++) {
                    $scope.ListaErrores[i].Omitido = false
                }
            }
        }
        $scope.MarcadDesmarcarTodoNoGuardaras = function (item) {
            if (item) {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                    $scope.ProgramacionesNoGuardadas[i].Omitido = true
                }
            }
            else {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                    $scope.ProgramacionesNoGuardadas[i].Omitido = false
                }
            }
        }
        $scope.MarcadDesmarcarTodoNoGuardadasRemplazo = function (item) {
            if ($scope.Option.name == 'Omitido') {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadasRemplazo.length; i++) {
                    $scope.ProgramacionesNoGuardadasRemplazo[i].Option = 'Omitido'
                }
            } else {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadasRemplazo.length; i++) {
                    $scope.ProgramacionesNoGuardadasRemplazo[i].Option = 'Remplazado'
                }
            }

        }
        $scope.LimpiarData = function () {
            $scope.ProgramacionesNoGuardadas = []
            $scope.ProgramacionesGuardadas = []
            $scope.DataVerificada = []
            $scope.DataActual = []
            $scope.DataErrorActual = []
            $scope.DataArchivo = []
            Guardaros = 0
            contGuard = 0
            contGuardGeneral = 0
            $scope.VerErrores = false

        }

        $scope.Archivo = '';
        $scope.ModalConfirmacionNuevoProceso = function () {
            showModal('modalConfirmacionCancelarProceso');
        }
        $scope.CerrarModalNuevoProceso = function (e) {
            var fileVal = document.getElementById("fileModal");
            fileVal.value = '';
            fileVal = document.getElementById("filePrincipal");
            fileVal.value = '';
            closeModal('modalConfirmacionCancelarProceso');


        }
        /*-----------------------------------------------------------------------funciones de lectura y apertura de archivos----------------------------------------------------------------------------*/

        $scope.ListaProgramaciones = []
        $scope.DataArchivo = []
        var X = XLSX;
        function to_json(workbook) {
            var result = {};
            workbook.SheetNames.forEach(function (sheetName) {
                var roa = X.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                if (roa.length > 0) {
                    result[sheetName] = roa;
                }
            });
            return result;
        }
        function fixdata(data) {
            var o = "", l = 0, w = 10240;
            for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w, l * w + w)));
            o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w)));
            return o;
        }
        function Eval(item) {
            if (item == '0' || item == undefined) {
                return true
            } else {
                return false
            }
        }
        var strIdentificacionesDestinatarios = '';
        function AsignarValoresTabla() {
            $scope.NumeroCargue = 0

            if ($scope.DataArchivo.Valores.length > 0) {
                var DataTemporal = []
                for (var i = 0; i < $scope.DataArchivo.Valores.length; i++) {
                    var item = $scope.DataArchivo.Valores[i]

                    if (!Eval(item.Tipo_Entrega) || !Eval(item.Ciudad_Remitente) || !Eval(item.Ciudad_Destinatario)
                    ) {
                        DataTemporal.push(item)
                    }
                }
                $scope.DataArchivo = DataTemporal
                $scope.totalRegistros = ($scope.DataArchivo.length)
                $scope.paginaActual = 1
                $scope.totalPaginas = Math.ceil($scope.totalRegistros / 20);
                blockUI.start();
                blockUI.message('Configurando campos, Esto puede tardar algunos minutos, por favor espere...');

                $timeout(function () {
                    try {
                        var data = []
                        $scope.DataArchivo = $linq.Enumerable().From($scope.DataArchivo).Select(function (x) {
                            x.TipoEntregaRemesaPaqueteria = x.Tipo_Entrega == undefined || x.Tipo_Entrega == '' ? $linq.Enumerable().From($scope.ListadoTipoEntregaRemesaPaqueteria).First('$.Codigo ==6600') : $linq.Enumerable().From($scope.ListadoTipoEntregaRemesaPaqueteria).First('$.Codigo ==' + x.Tipo_Entrega);
                            //x.Cliente = $linq.Enumerable().From($scope.ListaClientes).First('$.Codigo ==' + $scope.Cliente.Codigo);
                            x.Cliente = $scope.Cliente
                            x.validartarifario = true;
                            x.Remitente = {
                                Codigo: $scope.Cliente.Codigo,
                                TipoIdentificacion: $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.Cliente.TipoIdentificacion.Codigo),
                                NumeroIdentificacion: $scope.Cliente.NumeroIdentificacion,
                                Nombre: $scope.Cliente.NombreCompleto,
                                Ciudad: $linq.Enumerable().From($scope.ListadoCiudades).First('$.Codigo ==' + x.Ciudad_Remitente),
                                Direccion: x.Direccion_Remitente,
                                Telefonos: MascaraNumero(x.Telefono_Remitente)
                            }
                            x.CodigoPostalRemitente = x.Codigo_Postal_Remitente
                            x.BarrioRemitente = x.Barrio_Remitente
                            x.ObservacionesRemitente = x.Observaciones_Remitente
                            x.Destinatario = {
                                Codigo: x.Idnt_Destinatario == undefined || x.Idnt_Destinatario == '' ? $scope.Cliente.Codigo : 0,
                                TipoIdentificacion: x.Idnt_Destinatario == undefined || x.Idnt_Destinatario == '' ? $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.Cliente.TipoIdentificacion.Codigo) : $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + x.Tipo_Identificacion_Destinatario),
                                NumeroIdentificacion: x.Idnt_Destinatario == undefined || x.Idnt_Destinatario == '' ? $scope.Cliente.NumeroIdentificacion : MascaraNumero(x.Idnt_Destinatario),
                                Nombre: x.Idnt_Destinatario == undefined || x.Idnt_Destinatario == '' ? $scope.Cliente.NombreCompleto : x.Nombre_Destinatario,
                                Ciudad: x.Ciudad_Destinatario == undefined || x.Ciudad_Destinatario == '' ? $linq.Enumerable().From($scope.ListadoCiudades).First('$.Codigo ==' + $scope.Cliente.Ciudad.Codigo) : $linq.Enumerable().From($scope.ListadoCiudades).First('$.Codigo ==' + x.Ciudad_Destinatario),
                                Direccion: x.Direccion_Destinatario == undefined || x.Direccion_Destinatario == '' ? $scope.Cliente.Direccion : x.Direccion_Destinatario,
                                Telefonos: x.Telefono_Destinatario == undefined || x.Telefono_Destinatario == null ? $scope.Cliente.Telefonos : MascaraNumero(x.Telefono_Destinatario)
                            }

                            x.ZonaDestinatario = x.Zona_Destinatario == undefined ? $linq.Enumerable().From($scope.ListadoZonasGeneral).First('$.Codigo ==0') : $linq.Enumerable().From($scope.ListadoZonasGeneral).First('$.Codigo ==' + x.Zona_Destinatario)
                            x.Latitud = parseFloat(x.Latitud)
                            x.Longitud = parseFloat(x.Longitud)
                            x.Horario_Entrega = $linq.Enumerable().From($scope.ListaHorariosEntrega).First('$.Codigo ==' + parseInt(x.Horario_Entrega))
                            x.BarrioDestinatario = x.Barrio_Destinatario
                            x.CodigoPostalDestinatario = x.Codigo_Postal_Destinatario
                            x.ObservacionesDestinatario = x.Observaciones_Destinatario
                            x.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo ==' + x.Forma_Pago)
                            x.PesoCliente = parseFloat(x.Peso)
                            x.CantidadCliente = parseFloat(x.Unidades)
                            x.Largo = parseFloat(x.Largo)
                            x.Alto = parseFloat(x.Alto)
                            x.Ancho = parseFloat(x.Ancho)
                            x.Peso_Volumetrico = parseFloat(x.Peso_Volumetrico)
                            x.Linea_Negocio_Paqueteria = $linq.Enumerable().From($scope.ListaLineasNegocioPaqueteria).First('$.Codigo ==' + x.Linea_Negocio_Paqueteria)
                            x.Recoger_Oficina_Destino = x.Recoger_Oficina_Destino == undefined || x.Recoger_Oficina_Destino == '' ? $linq.Enumerable().From($scope.ListaRecogerOficinaDestino).First('$.Codigo ==0') : $linq.Enumerable().From($scope.ListaRecogerOficinaDestino).First('$.Codigo ==' + x.Recoger_Oficina_Destino)
                            x.ValorComercialCliente = MascaraValores(x.Valor_Comercial)
                            x.ProductoTransportado = $linq.Enumerable().From($scope.ListadoProductoTransportados).First('$.Codigo ==' + x.Producto)
                            x.DescripcionProductoTransportado = MascaraMayus(x.Descripcion)
                            x.NumeroDocumentoCliente = MascaraMayus(x.Documento_Cliente)
                            x.CentroCosto = x.Centro_Costo
                            x.LineaSerivicioCliente = x.Linea_Servicio == undefined || x.Linea_Servicio == null || x.Linea_Servicio == '' ? '' : x.Linea_Servicio
                            try {
                                x.SitioDescargue = $linq.Enumerable().From($scope.ListadoSitiosEntregaGeneral).First('$.Codigo ==' + MascaraNumero(x.Sitio_Entrega))
                                x.SitioDescargue.DireccionSitio = $linq.Enumerable().From($scope.ListadoSitiosEntregaGeneral).First('$.Codigo ==' + MascaraNumero(x.Sitio_Entrega)).Direccion
                                x.SitioDescargue.Telefono = $linq.Enumerable().From($scope.ListadoSitiosEntregaGeneral).First('$.Codigo ==' + MascaraNumero(x.Sitio_Entrega)).Telefono
                            } catch (e) { x.SitioDescargue = { Codigo: 0, DireccionSitio: '', Telefono: '' } }
                            x.ValorReexpedicion = MascaraValores(x.Valor_Reexpedicion)
                            x.ValorCargue = MascaraValores(x.Acarreo_Local)
                            x.ValorDescargue = MascaraValores(x.Acarreo_Destino)
                            x.FechaEntrega = new Date(x.Fecha_Entrega)
                            x.TipoRemesa = x.Remitente.Ciudad.Codigo != x.Destinatario.Ciudad.Codigo ? CODIGO_TIPO_REMESA_NACIONAL : CODIGO_TIPO_REMESA_URBANA
                            
                            x.ListaTarifaCarga = [
                                { Codigo: 200, Nombre: 'Rango Pesos Valor Kilo' },
                                { Codigo: 201, Nombre: 'Rango Pesos Valor Fijo' }
                            ];
                            x.stRow = ''
                            x.stGeneral = ''
                            x.msGeneral = ''
                            x.stTipoEntregaRemesaPaqueteria = ''
                            x.msTipoEntregaRemesaPaqueteria = ''
                            x.stPreImpreso = ''
                            x.msPreImpreso = ''
                            x.stCliente = ''
                            x.msCliente = ''
                            x.stTipoIdentificacionRemitente = ''
                            x.msTipoIdentificacionRemitente = ''
                            x.stNumeroIdentificacionRemitente = ''
                            x.msNumeroIdentificacionRemitente = ''
                            x.stNombreRemitente = ''
                            x.msNombreRemitente = ''
                            x.stCiudadRemitente = ''
                            x.msCiudadRemitente = ''
                            x.stDireccionRemitente = ''
                            x.msDireccionRemitente = ''
                            x.stTelefonosRemitente = ''
                            x.msTelefonosRemitente = ''
                            x.stTipoIdentificacionDestinatario = ''
                            x.msTipoIdentificacionDestinatario = ''
                            x.stNumeroIdentificacionDestinatario = ''
                            x.msNumeroIdentificacionDestinatario = ''
                            x.stNombreDestinatario = ''
                            x.msNombreDestinatario = ''
                            x.stCiudadDestinatario = ''
                            x.msCiudadDestinatario = ''
                            x.stZonaCiudad = ''
                            x.msZonaCiudad = ''
                            x.stLatitud = ''
                            x.msLatitud = ''
                            x.stLongitud = ''
                            x.msLongitud = ''
                            x.stHorario_Entrega = ''
                            x.msHorario_Entrega = ''
                            x.stDireccionDestinatario = ''
                            x.msDireccionDestinatario = ''
                            x.stTelefonosDestinatario = ''
                            x.msTelefonosDestinatario = ''
                            x.stFormaPago = ''
                            x.msFormaPago = ''
                            x.stPesoCliente = ''
                            x.msPesoCliente = ''
                            x.stCantidadCliente = ''
                            x.msCantidadCliente = ''
                            x.stLargo = ''
                            x.msLargo = ''
                            x.stAlto = ''
                            x.msAlto = ''
                            x.stAncho = ''
                            x.msAncho = ''
                            x.stPeso_Volumetrico = ''
                            x.msPeso_Volumetrico = ''
                            x.stLinea_Negocio_Paqueteria = ''
                            x.msLinea_Negocio_Paqueteria = ''
                            x.stRecoger_Oficina_Destino = ''
                            x.msRecoger_Oficina_Destino = ''
                            x.stValorComercialCliente = ''
                            x.msValorComercialCliente = ''
                            x.stProductoTransportado = ''
                            x.msProductoTransportado = ''
                            x.stFechaEntrega = ''
                            x.msFechaEntrega = ''
                            x.stSitioEntrega = ''
                            x.msSitioEntrega = ''
                            x.Omitido = false
                            x.Error = 0
                            return x
                        }).ToArray();

                        if ($scope.totalRegistros > 20) {
                            for (var i = 0; i < 20; i++) {
                                $scope.DataActual.push($scope.DataArchivo[i])
                            }

                        } else {
                            $scope.DataActual = $scope.DataArchivo
                        }
                        for (var i = 0; i < $scope.DataArchivo.length; i++) {

                            $scope.DataArchivo[i].Pos = i;


                        }

                        if ($scope.DataArchivo.length > 0) {
                            //blockUI.start();
                            //$scope.ValidarRemtientes(0)
                            blockUI.stop();
                        } else {
                            ShowError('El archivo seleccionado no contiene datos para validar, por favor intente con otro archivo')
                            blockUI.stop()
                            $timeout(blockUI.stop(), 1000)

                        }
                    } catch (e) {
                        ShowError('Error en la lectura del archivo por favor intente nuevamente: ' + e)

                        blockUI.stop()
                        $timeout(blockUI.stop(), 1000)
                    }



                }, 1000)
            }

        }
        var conterroresprevalidar = 0
        $scope.ValidarRemtientes = function (inicio) {
            if (inicio + 1 <= $scope.DataArchivo.length) {
                $timeout(function () {
                    blockUI.message('Validando remitentes ' + (inicio + 1) + ' de ' + $scope.DataArchivo.length);
                }, 100);
                //if ($scope.DataArchivo[inicio].Cliente.Codigo !== null && $scope.DataArchivo[inicio].Cliente.Codigo != undefined && $scope.DataArchivo[inicio].Cliente.Codigo > 0) {
                try {
                    var Filtro = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Codigo: $scope.Cliente.Codigo,
                    }
                } catch (e) {
                    var Filtro = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Codigo: 0,
                        NumeroIdentificacion: $scope.DataArchivo[inicio].Remitente.NumeroIdentificacion
                    }
                }
                //Remitente
                TercerosFactory.Obtener(Filtro).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                $scope.DataArchivo[inicio].Remitente.Codigo = response.data.Datos.Codigo
                                $scope.DataArchivo[inicio].Remitente.NumeroIdentificacion = response.data.Datos.NumeroIdentificacion
                                $scope.DataArchivo[inicio].Remitente.Nombre = response.data.Datos.NombreCompleto
                                $scope.DataArchivo[inicio].Remitente.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + response.data.Datos.TipoIdentificacion.Codigo);
                                $scope.DataArchivo[inicio].CodigoPostalRemitente = response.data.Datos.CodigoPostal;
                                $scope.DataArchivo[inicio].BarrioRemitente = response.data.Datos.Barrio;
                                $scope.DataArchivo[inicio].Cliente = $scope.CargarTercero(response.data.Datos.Codigo);
                                if ($scope.DataArchivo[inicio].Remitente.Direccion == undefined || $scope.DataArchivo[inicio].Remitente.Direccion == '') {
                                    $scope.DataArchivo[inicio].Remitente.Direccion = response.data.Datos.Direccion
                                }
                                if ($scope.DataArchivo[inicio].Remitente.Telefonos == undefined || $scope.DataArchivo[inicio].Remitente.Telefonos == '') {
                                    $scope.DataArchivo[inicio].Remitente.Telefonos = response.data.Datos.Telefonos.replace(';', ' - ')
                                }
                                if ($scope.DataArchivo[inicio].Remitente.Ciudad == undefined || $scope.DataArchivo[inicio].Remitente.Ciudad == '') {
                                    $scope.DataArchivo[inicio].Remitente.Ciudad = $linq.Enumerable().From($scope.ListadoCiudades).First('$.Codigo ==' + response.data.Datos.Ciudad.Codigo);
                                }
                                if ($scope.DataArchivo[inicio].Cliente !== undefined && $scope.DataArchivo[inicio].Cliente !== '' && $scope.DataArchivo[inicio].Cliente !== null) {
                                    if (($scope.DataArchivo[inicio].Destinatario.NumeroIdentificacion == undefined || $scope.DataArchivo[inicio].Destinatario.NumeroIdentificacion == '') && $scope.DataArchivo[inicio].Cliente.Codigo > 0) {
                                        $scope.DataArchivo[inicio].Destinatario.Codigo = response.data.Datos.Codigo
                                        $scope.DataArchivo[inicio].Destinatario.NumeroIdentificacion = response.data.Datos.NumeroIdentificacion
                                        $scope.DataArchivo[inicio].Destinatario.Nombre = response.data.Datos.NombreCompleto
                                        $scope.DataArchivo[inicio].Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + response.data.Datos.TipoIdentificacion.Codigo);
                                        if ($scope.DataArchivo[inicio].Destinatario.Direccion == undefined || $scope.DataArchivo[inicio].Destinatario.Direccion == '') {
                                            $scope.DataArchivo[inicio].Destinatario.Direccion = response.data.Datos.Direccion
                                        }
                                        if ($scope.DataArchivo[inicio].Destinatario.Telefonos == undefined || $scope.DataArchivo[inicio].Destinatario.Telefonos == '') {
                                            $scope.DataArchivo[inicio].Destinatario.Telefonos = response.data.Datos.Telefonos.replace(';', ' - ')
                                        }
                                        if ($scope.DataArchivo[inicio].Destinatario.Ciudad == undefined || $scope.DataArchivo[inicio].Destinatario.Ciudad == '') {
                                            $scope.DataArchivo[inicio].Destinatario.Ciudad = $linq.Enumerable().From($scope.ListadoCiudades).First('$.Codigo ==' + response.data.Datos.Ciudad.Codigo);
                                        }
                                    }
                                }
                                try {
                                    $scope.DataArchivo[inicio].ListaLineaSerivicioCliente = response.data.Datos.LineaServicio
                                    $scope.DataArchivo[inicio].LineaSerivicioCliente = $linq.Enumerable().From($scope.DataArchivo[inicio].ListaLineaSerivicioCliente).First('$.Codigo ==' + $scope.DataArchivo[inicio].LineaSerivicioCliente);
                                } catch (e) {

                                }



                            }
                        }
                        $scope.ValidarRemtientes(inicio + 1)
                    }, function (response) {
                        $scope.ValidarRemtientes(inicio + 1)
                    });
                //} else {
                //    $scope.ValidarRemtientes(inicio + 1)
                //}
            }
            else {
                $scope.ValidarDestinatarios(0)
            }
        }
        $scope.ValidarDestinatarios = function (inicio) {
            if (inicio + 1 <= $scope.DataArchivo.length) {

                $timeout(function () {
                    blockUI.message('Validando destinatarios ' + (inicio + 1) + ' de ' + $scope.DataArchivo.length);
                }, 100);
                if (!Eval($scope.DataArchivo[inicio].Destinatario.NumeroIdentificacion)) {
                    $scope.DataArchivo[inicio].Destinatario.Codigo = 0
                    var Filtro = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        NumeroIdentificacion: $scope.DataArchivo[inicio].Destinatario.NumeroIdentificacion
                    }
                    //destinatario
                    TercerosFactory.Obtener(Filtro).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.Codigo > 0) {
                                    $scope.DataArchivo[inicio].Destinatario.Codigo = response.data.Datos.Codigo
                                    $scope.DataArchivo[inicio].Destinatario.NumeroIdentificacion = response.data.Datos.NumeroIdentificacion
                                    $scope.DataArchivo[inicio].Destinatario.Nombre = response.data.Datos.NombreCompleto
                                    $scope.DataArchivo[inicio].Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + response.data.Datos.TipoIdentificacion.Codigo);
                                    if ($scope.DataArchivo[inicio].Destinatario.Direccion == undefined || $scope.DataArchivo[inicio].Destinatario.Direccion == '') {
                                        $scope.DataArchivo[inicio].Destinatario.Direccion = response.data.Datos.Direccion
                                    }
                                    if ($scope.DataArchivo[inicio].Destinatario.Telefonos == undefined || $scope.DataArchivo[inicio].Destinatario.Telefonos == '') {
                                        $scope.DataArchivo[inicio].Destinatario.Telefonos = response.data.Datos.Telefonos.replace(';', ' - ')
                                    }
                                    if ($scope.DataArchivo[inicio].Destinatario.Ciudad == undefined || $scope.DataArchivo[inicio].Destinatario.Ciudad == '') {
                                        $scope.DataArchivo[inicio].Destinatario.Ciudad = $linq.Enumerable().From($scope.ListadoCiudades).First('$.Codigo ==' + response.data.Datos.Ciudad.Codigo);
                                    }
                                    try {
                                        blockUIConfig.autoBlock = false;
                                        var Response = SitiosTerceroClienteFactory.ConsultarOrdenServicio({
                                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                            Cliente: { Codigo: $scope.DataArchivo[inicio].Destinatario.Codigo },
                                            //CiudadCargue: { Codigo: ciudad.Codigo },
                                            Codigo: $scope.DataArchivo[inicio].SitioDescargue.Codigo,
                                            Sync: true
                                        })
                                        for (var i = 0; i < Response.Datos.length; i++) {
                                            Response.Datos[i].Codigo = Response.Datos[i].SitioCliente.Codigo
                                        }
                                        try {
                                            if (Response.Datos.lenght > 0) {
                                                $scope.DataArchivo[inicio].SitioDescargue = Response.Datos[0]
                                            }
                                            $scope.DataArchivo[inicio].Destinatario.Direccion = $scope.DataArchivo[inicio].SitioDescargue.DireccionSitio
                                            $scope.DataArchivo[inicio].Destinatario.Telefonos = $scope.DataArchivo[inicio].SitioDescargue.Telefono
                                        } catch (e) {

                                        }


                                        $scope.DataArchivo[inicio].ListadoSitiosDescargueAlterno2 = []
                                        $scope.DataArchivo[inicio].AutocompleteSitiosClienteDescargue = function (value, cliente, ciudad) {
                                            if (value.length > 0) {
                                                if ((value.length % 3) == 0 || value.length == 2) {
                                                    /*Cargar Autocomplete de propietario*/
                                                    blockUIConfig.autoBlock = false;
                                                    var Response = SitiosTerceroClienteFactory.ConsultarOrdenServicio({
                                                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                                        Cliente: { Codigo: cliente.Codigo },
                                                        CiudadCargue: { Codigo: ciudad.Codigo },
                                                        ValorAutocomplete: value,
                                                        Sync: true
                                                    })
                                                    for (var i = 0; i < Response.Datos.length; i++) {
                                                        Response.Datos[i].Codigo = Response.Datos[i].SitioCliente.Codigo
                                                    }
                                                    $scope.DataArchivo[inicio].ListadoSitiosDescargueAlterno2 = ValidarListadoAutocomplete(Response.Datos, $scope.DataArchivo[inicio].ListadoSitiosDescargueAlterno2)
                                                }
                                            }
                                            return $scope.DataArchivo[inicio].ListadoSitiosDescargueAlterno2
                                        }
                                    } catch (e) {

                                    }
                                }
                            }
                            $scope.ValidarDestinatarios(inicio + 1)
                        }, function (response) {
                            $scope.ValidarDestinatarios(inicio + 1)
                        });
                } else {
                    $scope.ValidarDestinatarios(inicio + 1)
                }

            }
            else {
                $timeout(function () {
                    blockUI.stop();
                }, 1000);
                if (conterroresprevalidar > 0) {
                    ShowError('Se encontraron errores al validar los datos cargados')
                } else {
                    ShowSuccess('Los datos se verificaron correctamente')
                }
            }
        }
        function process_wb(wb) {
            $scope.DataArchivo = []
            $scope.ListaErrores = []
            $scope.DataActual = []
            $scope.DataArchivo = to_json(wb);
            if ($scope.DataArchivo.Valores != undefined) {
                $('#TablaCarge').show();
                $('#btnValida').show();

                $scope.DataArchivo = { Valores: $scope.DataArchivo.Valores }
                AsignarValoresTabla()


            }
            else {
                ShowError('El arhivo cargado no corresponde al formato de carge masivo de las programaciones')
            }
            blockUI.stop()

        }

        $scope.handleFile = function (e) {

            var files = e.files;
            var f = files[0];
            {
                var reader = new FileReader();
                var name = f.name;
                reader.onload = $scope.CargarDocumento
                reader.readAsArrayBuffer(f);
            }
        }

        $scope.CargarDocumento = function (e) {
            $scope.$apply(function () {
                var data = e.target.result;
                $scope.arr = fixdata(data);
                blockUI.start();
                $timeout(function () {
                    blockUI.message('Subiendo Archivo, Por Favor Espere...');
                }, 1000);
                $timeout(function () {
                    try {
                        $scope.wb = X.read(btoa($scope.arr), { type: 'base64' });
                        process_wb($scope.wb);

                    } catch (e) {
                        ShowError('El arhivo seleccionado no corresponde a una hoja de cálculo válida, por favor intente con otro archivo')
                        blockUI.stop()

                    }
                }, 1500);

            });

        }

        function restrow(item) {
            item.stRow = ''
            item.stGeneral = ''
            item.msGeneral = ''
            item.stTipoEntregaRemesaPaqueteria = ''
            item.msTipoEntregaRemesaPaqueteria = ''
            item.stPreImpreso = ''
            item.msPreImpreso = ''
            item.stCliente = ''
            item.msCliente = ''
            item.stTipoIdentificacionRemitente = ''
            item.msTipoIdentificacionRemitente = ''
            item.stNumeroIdentificacionRemitente = ''
            item.msNumeroIdentificacionRemitente = ''
            item.stNombreRemitente = ''
            item.msNombreRemitente = ''
            item.stCiudadRemitente = ''
            item.msCiudadRemitente = ''
            item.stDireccionRemitente = ''
            item.msDireccionRemitente = ''
            item.stTelefonosRemitente = ''
            item.msTelefonosRemitente = ''
            item.stTipoIdentificacionDestinatario = ''
            item.msTipoIdentificacionDestinatario = ''
            item.stNumeroIdentificacionDestinatario = ''
            item.msNumeroIdentificacionDestinatario = ''
            item.stNombreDestinatario = ''
            item.msNombreDestinatario = ''
            item.stCiudadDestinatario = ''
            item.msCiudadDestinatario = ''
            item.stZonaCiudad = ''
            item.msZonaCiudad = ''
            item.stLatitud = ''
            item.msLatitud = ''
            item.stLongitud = ''
            item.msLongitud = ''
            item.stHorario_Entrega = ''
            item.msHorario_Entrega = ''
            item.stDireccionDestinatario = ''
            item.msDireccionDestinatario = ''
            item.stTelefonosDestinatario = ''
            item.msTelefonosDestinatario = ''
            item.stFormaPago = ''
            item.msFormaPago = ''
            item.stPesoCliente = ''
            item.msPesoCliente = ''
            item.stCantidadCliente = ''
            item.msCantidadCliente = ''
            item.stLargo = ''
            item.msLargo = ''
            item.stAlto = ''
            item.msAlto = ''
            item.stAncho = ''
            item.msAncho = ''
            item.stPeso_Volumetrico = ''
            item.msPeso_Volumetrico = ''
            item.stLinea_Negocio_Paqueteria = ''
            item.msLinea_Negocio_Paqueteria = ''
            item.stRecoger_Oficina_Destino = ''
            item.msRecoger_Oficina_Destino = ''
            item.stValorComercialCliente = ''
            item.msValorComercialCliente = ''
            item.stProductoTransportado = ''
            item.msProductoTransportado = ''
            item.stFechaEntrega = ''
            item.msFechaEntrega = ''
            item.stSitioEntrega = ''
            item.msSitioEntrega = ''
        }
        $scope.ValidarDocumento = function () {
            PantallaBloqueoValidacion($scope.ValidarRegistros)
        }

        $scope.ValidarRegistros = function () {
            $scope.ListaErrores = []
            $scope.MensajesError = []
            strIdentificacionesDestinatarios = ''

            var contadorgeneral = 0
            var contadorRegistros = 0
            ListaNuevosTerceros = [];
            strListaNuevosTerceros = '';
            $scope.DataVerificada = []
            $scope.DataVerificadatmp = []

            $scope.DataArchivo = $linq.Enumerable().From($scope.DataArchivo).Select(function (x) {
                x.Error = 0
                x.stRow = ''
                return x
            }).ToArray();


            $scope.DataArchivo = $linq.Enumerable().From($scope.DataArchivo).Select(function (x) {
                x.stCiudadDestinatario = x.Destinatario.Ciudad == undefined || x.Destinatario.Ciudad == "" || x.Destinatario.Ciudad == null && x.Omitido != true ? 'background:red' : ''
                x.msCiudadDestinatario = x.Destinatario.Ciudad == undefined || x.Destinatario.Ciudad == "" || x.Destinatario.Ciudad == null && x.Omitido != true ? 'Ingrese la Ciudad' : ''
                x.stRow = x.Destinatario.Ciudad == undefined || x.Destinatario.Ciudad == "" || x.Destinatario.Ciudad == null && x.Omitido != true ? 'background:yellow' : x.stRow
                x.Error = x.Destinatario.Ciudad == undefined || x.Destinatario.Ciudad == "" || x.Destinatario.Ciudad == null && x.Omitido != true ? 1 : x.Error

                return x
            }).ToArray();

            $scope.DataArchivo = $linq.Enumerable().From($scope.DataArchivo).Select(function (x) {
                x.stTipoIdentificacionDestinatario = x.Destinatario.TipoIdentificacion == undefined || x.Destinatario.TipoIdentificacion == "" || x.Destinatario.TipoIdentificacion == null || x.Destinatario.TipoIdentificacion.Codigo == 100 && x.Omitido != true ? 'background:red' : ''
                x.msTipoIdentificacionDestinatario = x.Destinatario.TipoIdentificacion == undefined || x.Destinatario.TipoIdentificacion == "" || x.Destinatario.TipoIdentificacion == null || x.Destinatario.TipoIdentificacion.Codigo == 100 && x.Omitido != true ? 'Ingrese el tipo de identificación' : ''
                x.stRow = x.Destinatario.TipoIdentificacion == undefined || x.Destinatario.TipoIdentificacion == "" || x.Destinatario.TipoIdentificacion == null || x.Destinatario.TipoIdentificacion.Codigo == 100 && x.Omitido != true ? 'background:yellow' : x.stRow
                x.Error = x.Destinatario.TipoIdentificacion == undefined || x.Destinatario.TipoIdentificacion == "" || x.Destinatario.TipoIdentificacion == null || x.Destinatario.TipoIdentificacion.Codigo == 100 && x.Omitido != true ? 1 : x.Error

                return x
            }).ToArray();
            $scope.DataArchivo = $linq.Enumerable().From($scope.DataArchivo).Select(function (x) {
                x.stNumeroIdentificacionDestinatario = x.Destinatario.NumeroIdentificacion == undefined || x.Destinatario.NumeroIdentificacion == "" || x.Destinatario.NumeroIdentificacion == null && x.Omitido != true ? 'background:red' : ''
                x.msNumeroIdentificacionDestinatario = x.Destinatario.NumeroIdentificacion == undefined || x.Destinatario.NumeroIdentificacion == "" || x.Destinatario.NumeroIdentificacion == null && x.Omitido != true ? 'Ingrese el número de identificacion' : ''
                x.stRow = x.Destinatario.NumeroIdentificacion == undefined || x.Destinatario.NumeroIdentificacion == "" || x.Destinatario.NumeroIdentificacion == null && x.Omitido != true ? 'background:yellow' : x.stRow
                x.Error = x.Destinatario.NumeroIdentificacion == undefined || x.Destinatario.NumeroIdentificacion == "" || x.Destinatario.NumeroIdentificacion == null && x.Omitido != true ? 1 : x.Error
                //Se crea una lista de numeros de identificación para posteriormente validar cuáles Destinatarios ya existen en BD:
                strIdentificacionesDestinatarios = strIdentificacionesDestinatarios + x.Destinatario.NumeroIdentificacion + ','
                return x
            }).ToArray();
            $scope.DataArchivo = $linq.Enumerable().From($scope.DataArchivo).Select(function (x) {
                x.stNombreDestinatario = x.Destinatario.Nombre == undefined || x.Destinatario.Nombre == "" || x.Destinatario.Nombre == null && x.Omitido != true ? 'background:red' : ''
                x.msNombreDestinatario = x.Destinatario.Nombre == undefined || x.Destinatario.Nombre == "" || x.Destinatario.Nombre == null && x.Omitido != true ? 'Ingrese el nombre' : ''
                x.stRow = x.Destinatario.Nombre == undefined || x.Destinatario.Nombre == "" || x.Destinatario.Nombre == null && x.Omitido != true ? 'background:yellow' : x.stRow
                x.Error = x.Destinatario.Nombre == undefined || x.Destinatario.Nombre == "" || x.Destinatario.Nombre == null && x.Omitido != true ? 1 : x.Error

                return x
            }).ToArray();
            $scope.DataArchivo = $linq.Enumerable().From($scope.DataArchivo).Select(function (x) {
                x.stDireccionDestinatario = x.Destinatario.Direccion == undefined || x.Destinatario.Direccion == "" || x.Destinatario.Direccion == null && x.Omitido != true ? 'background:red' : ''
                x.msDireccionDestinatario = x.Destinatario.Direccion == undefined || x.Destinatario.Direccion == "" || x.Destinatario.Direccion == null && x.Omitido != true ? 'Ingrese la dirección' : ''
                x.stRow = x.Destinatario.Direccion == undefined || x.Destinatario.Direccion == "" || x.Destinatario.Direccion == null && x.Omitido != true ? 'background:yellow' : x.stRow
                x.Error = x.Destinatario.Direccion == undefined || x.Destinatario.Direccion == "" || x.Destinatario.Direccion == null && x.Omitido != true ? 1 : x.Error

                return x
            }).ToArray();
            $scope.DataArchivo = $linq.Enumerable().From($scope.DataArchivo).Select(function (x) {
                x.stTelefonosDestinatario = x.Destinatario.Telefonos == undefined || x.Destinatario.Telefonos == "" || x.Destinatario.Telefonos == null && x.Omitido != true ? 'background:red' : ''
                x.msTelefonosDestinatario = x.Destinatario.Telefonos == undefined || x.Destinatario.Telefonos == "" || x.Destinatario.Telefonos == null && x.Omitido != true ? 'Ingrese el teléfono' : ''
                x.stRow = x.Destinatario.Telefonos == undefined || x.Destinatario.Telefonos == "" || x.Destinatario.Telefonos == null && x.Omitido != true ? 'background:yellow' : x.stRow
                x.Error = x.Destinatario.Telefonos == undefined || x.Destinatario.Telefonos == "" || x.Destinatario.Telefonos == null && x.Omitido != true ? 1 : x.Error
                return x
            }).ToArray();
            $scope.DataArchivo = $linq.Enumerable().From($scope.DataArchivo).Select(function (x) {
                x.TipoRemesa = x.Remitente.Ciudad.Codigo != x.Destinatario.Ciudad.Codigo ? CODIGO_TIPO_REMESA_NACIONAL : CODIGO_TIPO_REMESA_URBANA
                return x
            }).ToArray();
            $scope.DataArchivo = $linq.Enumerable().From($scope.DataArchivo).Select(function (x) {
                x.stFormaPago = x.FormaPago == undefined || $.FormaPago == "" && $.Omitido != true ? 'background:red' : ''
                x.msFormaPago = x.FormaPago == undefined || $.FormaPago == "" && $.Omitido != true ? 'Ingrese la forma de pago' : ''
                x.stRow = x.FormaPago == undefined || $.FormaPago == "" && $.Omitido != true ? 'background:yellow' : x.stRow
                x.Error = x.FormaPago == undefined || $.FormaPago == "" && $.Omitido != true ? 1 : x.Error
                return x
            }).ToArray();

            $scope.DataArchivo = $linq.Enumerable().From($scope.DataArchivo).Select(function (x) {
                x.stPeso_Volumetrico = $scope.Sesion.UsuarioAutenticado.ManejoPesoVolumetricoRemesaPauqeteria && ((x.Peso_Volumetrico == undefined || x.Peso_Volumetrico == "") && (x.PesoCliente == undefined || x.PesoCliente == '') && x.Omitido != true) ? 'background:red' : ''
                x.msPeso_Volumetrico = $scope.Sesion.UsuarioAutenticado.ManejoPesoVolumetricoRemesaPauqeteria && ((x.Peso_Volumetrico == undefined || x.Peso_Volumetrico == "") && (x.PesoCliente == undefined || x.PesoCliente == '') && x.Omitido != true) ? 'Ingrese el peso Volumétrico' : ''
                x.stRow = $scope.Sesion.UsuarioAutenticado.ManejoPesoVolumetricoRemesaPauqeteria && ((x.Peso_Volumetrico == undefined || x.Peso_Volumetrico == "") && (x.PesoCliente == undefined || x.PesoCliente == '') && x.Omitido != true) ? 'background:yellow' : x.stRow
                x.Error = $scope.Sesion.UsuarioAutenticado.ManejoPesoVolumetricoRemesaPauqeteria && ((x.Peso_Volumetrico == undefined || x.Peso_Volumetrico == "") && (x.PesoCliente == undefined || x.PesoCliente == '') && x.Omitido != true) ? 1 : x.Error
                return x
            }).ToArray();

            $scope.DataArchivo = $linq.Enumerable().From($scope.DataArchivo).Select(function (x) {
                x.stAlto = (x.PesoCliente == undefined || x.PesoCliente == "") && (x.Alto == "" || x.Alto == undefined) && x.Omitido != true ? 'background:red' : ''
                x.msAlto = (x.PesoCliente == undefined || x.PesoCliente == "") && (x.Alto == "" || x.Alto == undefined) && x.Omitido != true ? 'Ingrese el alto' : ''
                x.stRow = (x.PesoCliente == undefined || x.PesoCliente == "") && (x.Alto == "" || x.Alto == undefined) && x.Omitido != true ? 'background:yellow' : x.stRow
                x.Error = (x.PesoCliente == undefined || x.PesoCliente == "") && (x.Alto == "" || x.Alto == undefined) && x.Omitido != true ? 1 : x.Error
                return x
            }).ToArray();

            $scope.DataArchivo = $linq.Enumerable().From($scope.DataArchivo).Select(function (x) {
                x.stLargo = (x.PesoCliente == undefined || x.PesoCliente == "") && (x.Largo == "" || x.Largo == undefined) && x.Omitido != true ? 'background:red' : ''
                x.msLargo = (x.PesoCliente == undefined || x.PesoCliente == "") && (x.Largo == "" || x.Largo == undefined) && x.Omitido != true ? 'Ingrese el largo' : ''
                x.stRow = (x.PesoCliente == undefined || x.PesoCliente == "") && (x.Largo == "" || x.Largo == undefined) && x.Omitido != true ? 'background:yellow' : x.stRow
                x.Error = (x.PesoCliente == undefined || x.PesoCliente == "") && (x.Largo == "" || x.Largo == undefined) && x.Omitido != true ? 1 : x.Error
                return x
            }).ToArray();

            $scope.DataArchivo = $linq.Enumerable().From($scope.DataArchivo).Select(function (x) {
                x.stAncho = (x.PesoCliente == undefined || x.PesoCliente == "") && (x.Ancho == "" || x.Ancho == undefined) && x.Omitido != true ? 'background:red' : ''
                x.msAncho = (x.PesoCliente == undefined || x.PesoCliente == "") && (x.Ancho == "" || x.Ancho == undefined) && x.Omitido != true ? 'Ingrese el ancho' : ''
                x.stRow = (x.PesoCliente == undefined || x.PesoCliente == "") && (x.Ancho == "" || x.Ancho == undefined) && x.Omitido != true ? 'background:yellow' : x.stRow
                x.Error = (x.PesoCliente == undefined || x.PesoCliente == "") && (x.Ancho == "" || x.Ancho == undefined) && x.Omitido != true ? 1 : x.Error
                return x
            }).ToArray();

            $scope.DataArchivo = $linq.Enumerable().From($scope.DataArchivo).Select(function (x) {
                x.stCantidadCliente = (x.CantidadCliente == undefined || x.CantidadCliente == "") && x.Omitido != true ? 'background:red' : ''
                x.msCantidadCliente = (x.CantidadCliente == undefined || x.CantidadCliente == "") && x.Omitido != true ? 'Ingrese la cantidad' : ''
                x.stRow = (x.CantidadCliente == undefined || x.CantidadCliente == "") && x.Omitido != true ? 'background:yellow' : x.stRow
                x.Error = (x.CantidadCliente == undefined || x.CantidadCliente == "") && x.Omitido != true ? 1 : x.Error
                return x
            }).ToArray();


            $scope.DataArchivo = $linq.Enumerable().From($scope.DataArchivo).Select(function (x) {
                x.stLinea_Negocio_Paqueteria = (x.Linea_Negocio_Paqueteria == undefined || x.Linea_Negocio_Paqueteria == "" || isNaN(x.Linea_Negocio_Paqueteria.Codigo)) && x.Omitido != true ? 'background:red' : ''
                x.msLinea_Negocio_Paqueteria = (x.Linea_Negocio_Paqueteria == undefined || x.Linea_Negocio_Paqueteria == "" || isNaN(x.Linea_Negocio_Paqueteria.Codigo)) && x.Omitido != true ? 'Ingrese la línea negocio' : ''
                x.stRow = (x.Linea_Negocio_Paqueteria == undefined || x.Linea_Negocio_Paqueteria == "" || isNaN(x.Linea_Negocio_Paqueteria.Codigo)) && x.Omitido != true ? 'background:yellow' : x.stRow
                x.Error = (x.Linea_Negocio_Paqueteria == undefined || x.Linea_Negocio_Paqueteria == "" || isNaN(x.Linea_Negocio_Paqueteria.Codigo)) && x.Omitido != true ? 1 : x.Error
                return x
            }).ToArray();

            $scope.DataArchivo = $linq.Enumerable().From($scope.DataArchivo).Select(function (x) {
                x.stValorComercialCliente = (x.ValorComercialCliente == undefined || x.ValorComercialCliente == "") && x.Omitido != true ? 'background:red' : ''
                x.msValorComercialCliente = (x.ValorComercialCliente == undefined || x.ValorComercialCliente == "") && x.Omitido != true ? 'Ingrese el valor' : ''
                x.stRow = (x.ValorComercialCliente == undefined || x.ValorComercialCliente == "") && x.Omitido != true ? 'background:yellow' : x.stRow
                x.Error = (x.ValorComercialCliente == undefined || x.ValorComercialCliente == "") && x.Omitido != true ? 1 : x.Error
                return x
            }).ToArray();

            $scope.DataArchivo = $linq.Enumerable().From($scope.DataArchivo).Select(function (x) {
                x.stProductoTransportado = (x.ProductoTransportado == undefined || x.ProductoTransportado == "") && x.Omitido != true ? 'background:red' : ''
                x.msProductoTransportado = (x.ProductoTransportado == undefined || x.ProductoTransportado == "") && x.Omitido != true ? 'Ingrese el producto' : ''
                x.stRow = (x.ProductoTransportado == undefined || x.ProductoTransportado == "") && x.Omitido != true ? 'background:yellow' : x.stRow
                x.Error = (x.ProductoTransportado == undefined || x.ProductoTransportado == "") && x.Omitido != true ? 1 : x.Error
                return x
            }).ToArray();

            $scope.DataArchivo = $linq.Enumerable().From($scope.DataArchivo).Select(function (x) {
                x.stFechaEntrega = ((x.FechaEntrega == undefined || x.FechaEntrega == "") || (x.FechaEntrega <= new Date())) && x.Omitido != true ? 'background:red' : ''
                x.msFechaEntrega = (x.FechaEntrega == undefined || x.FechaEntrega == "") && x.Omitido != true ? 'Ingrese la fecha' : x.FechaEntrega <= new Date() ? 'La fecha de entrega debe ser mayor a la fecha actual' : ''
                x.stRow = ((x.FechaEntrega == undefined || x.FechaEntrega == "") || (x.FechaEntrega <= new Date())) && x.Omitido != true ? 'background:yellow' : x.stRow
                x.Error = ((x.FechaEntrega == undefined || x.FechaEntrega == "") || (x.FechaEntrega <= new Date())) && x.Omitido != true ? 1 : x.Error
                return x
            }).ToArray();


            //Filtrar los tipos tarifa por los criterios : Ciudad Origen/Destino, Tipo Linea Negocio y Forma Pago
            $scope.DataArchivo = $linq.Enumerable().From($scope.DataArchivo).Select(function (x) {
                x.TarifaCarga = x.TarifaCarga == undefined ? x.ListaTarifaCarga[0] : x.TarifaCarga
                x.ListaAuxTipoTarifas = $linq.Enumerable().From($scope.TarifarioCliente).Where('$.CodigoCiudadOrigen==' + x.Remitente.Ciudad.Codigo).ToArray()
                x.ListaAuxTipoTarifas = $linq.Enumerable().From(x.ListaAuxTipoTarifas).Where('$.CodigoCiudadDestino==' + x.Destinatario.Ciudad.Codigo).ToArray()
                x.ListaAuxTipoTarifas = $linq.Enumerable().From(x.ListaAuxTipoTarifas).Where('$.CodigoTipoLineaNegocioTransportes==' + (x.Remitente.Ciudad.Codigo == x.Destinatario.Ciudad.Codigo ? CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_URBANA : CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_NACIONAL)).ToArray()
                x.ListaAuxTipoTarifas = $linq.Enumerable().From(x.ListaAuxTipoTarifas).Where('$.CodigoFormaPagoTarifa ==' + x.FormaPago.Codigo).ToArray()
                
                console.log(x.ListaAuxTipoTarifas)
                return x
            }).ToArray();


            //Calcular Tarifas:


            $scope.DataArchivo = $linq.Enumerable().From($scope.DataArchivo).Select(function (x) {

                //primero se calculan los valores con las condiciones comerciales del cliente(en caso de que los parametros de la guía coincidan con algún detalle de las condiciones comerciales):
                x.ValorManejoClienteCondicionesComerciales = $scope.Cliente.Cliente.CondicionesComerciales.length > 0 ? $linq.Enumerable().From($scope.Cliente.Cliente.CondicionesComerciales).Where('$.LineaNegocio.Codigo==' + CODIGO_LINEA_NEGOCIO_TRANPSORTE_CARGA_PAQUETERIA).ToArray() : []

                x.TipoLineaNegocio = (x.Remitente.Ciudad.Codigo == x.Destinatario.Ciudad.Codigo) ? CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_URBANA : CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_NACIONAL
                x.ValorManejoClienteCondicionesComerciales = x.ValorManejoClienteCondicionesComerciales.length > 0 ? $linq.Enumerable().From(x.ValorManejoClienteCondicionesComerciales).Where('$.TipoLineaNegocio.Codigo==' + x.TipoLineaNegocio).ToArray() : []
                //x.ValorManejoClienteCondicionesComercialesTarifa1 = x.ValorManejoClienteCondicionesComerciales.length > 0 ? $linq.Enumerable().From(x.ValorManejoClienteCondicionesComerciales).Where('$.Tarifa.Codigo==' + x.TarifaCarga.Codigo).ToArray() : []
                //x.tempTarifa = x.ValorManejoClienteCondicionesComercialesTarifa1.length > 0 ? { Codigo: 0 } : $linq.Enumerable().From(x.ListaTarifaCarga).First('$.Codigo==200')
                //x.ValorManejoClienteCondicionesComerciales = x.ValorManejoClienteCondicionesComercialesTarifa1.length > 0 ? x.ValorManejoClienteCondicionesComercialesTarifa1 : $linq.Enumerable().From(x.ValorManejoClienteCondicionesComerciales).Where('$.Tarifa.Codigo==' + x.ListaTarifaCarga[1].Codigo).ToArray()
                x.CondicionesComercialesFijo = $linq.Enumerable().From(x.ValorManejoClienteCondicionesComerciales).Where('$.Tarifa.Codigo==201').ToArray()
                x.CondicionesComercialesKilo = $linq.Enumerable().From(x.ValorManejoClienteCondicionesComerciales).Where('$.Tarifa.Codigo==200').ToArray()
                // si la guía coincide con algún detalle, se reasigna la tarifa deacuerdo a la condición comercial que se encontró:
               // x.TarifaCarga = x.ValorManejoClienteCondicionesComerciales.length > 0 && x.tempTarifa.Codigo != 0 ? x.tempTarifa : x.TarifaCarga

                x.ListaAuxTipoTarifasFijo = $linq.Enumerable().From(x.ListaAuxTipoTarifas).Where('$.TarifaCarga.Codigo==201').ToArray()
                x.ListaAuxTipoTarifasKilo = $linq.Enumerable().From(x.ListaAuxTipoTarifas).Where('$.TarifaCarga.Codigo==200').ToArray()
                //x.TarifaCarga = x.ListaAuxTipoTarifasTemp.length <= 0 && x.ValorManejoClienteCondicionesComerciales.length <= 0 ? x.TarifaCarga.Codigo == 200 ? $linq.Enumerable().From(x.ListaTarifaCarga).First('$.Codigo==201') : $linq.Enumerable().From(x.ListaTarifaCarga).First('$.Codigo==200') : x.TarifaCarga
                //x.ListaAuxTipoTarifas = $linq.Enumerable().From(x.ListaAuxTipoTarifas).Where('$.TarifaCarga.Codigo==' + x.TarifaCarga.Codigo).ToArray()

                //se calcula el tipo tarifa deacuerdo al peso o peso volumétrico dependiendo si la empresa tiene activo el proceso:
                x.PesoCalcular = $scope.Sesion.UsuarioAutenticado.ManejoPesoVolumetricoRemesaPauqeteria ? x.Peso_Volumetrico > x.PesoCliente ? x.Peso_Volumetrico : x.PesoCliente : x.PesoCliente ;
                x.ListaAuxTipoTarifas = $linq.Enumerable().From(x.ListaAuxTipoTarifas).Select(function (x) {
                    x.ValorCatalogoTipoTarifa.CampoAuxiliar2 = parseFloat(x.ValorCatalogoTipoTarifa.CampoAuxiliar2);
                    x.ValorCatalogoTipoTarifa.CampoAuxiliar3 = parseFloat(x.ValorCatalogoTipoTarifa.CampoAuxiliar3);
                    return x
                }).ToArray()
                //x.ListaAuxTipoTarifas = $linq.Enumerable().From(x.ListaAuxTipoTarifas).Where('$.ValorCatalogoTipoTarifa.CampoAuxiliar2 <=' + parseFloat(x.PesoCalcular)).ToArray()
                //x.ListaAuxTipoTarifas = $linq.Enumerable().From(x.ListaAuxTipoTarifas).Where('$.ValorCatalogoTipoTarifa.CampoAuxiliar3 >=' + parseFloat(x.PesoCalcular)).ToArray()
            

                x.ValorManejoClienteCondicionesComercialesTemp = x.TarifaCarga.Codigo == 200 ? $linq.Enumerable().From(x.CondicionesComercialesKilo).Where('$.TipoTarifa.ValorRangoInicial <=' + parseFloat(x.PesoCalcular)).ToArray() : $linq.Enumerable().From(x.CondicionesComercialesFijo).Where('$.TipoTarifa.ValorRangoInicial <=' + parseFloat(x.PesoCalcular)).ToArray()
                x.ValorManejoClienteCondicionesComercialesTemp = x.TarifaCarga.Codigo == 200 ? $linq.Enumerable().From(x.ValorManejoClienteCondicionesComercialesTemp).Where('$.TipoTarifa.ValorRangoFinal >=' + parseFloat(x.PesoCalcular)).ToArray() : $linq.Enumerable().From(x.ValorManejoClienteCondicionesComercialesTemp).Where('$.TipoTarifa.ValorRangoFinal >=' + parseFloat(x.PesoCalcular)).ToArray()

                x.TarifaCarga = x.ValorManejoClienteCondicionesComercialesTemp.length > 0 ? x.TarifaCarga : x.TarifaCarga.Codigo == 200 ? $linq.Enumerable().From(x.ListaTarifaCarga).First('$.Codigo==201') : $linq.Enumerable().From(x.ListaTarifaCarga).First('$.Codigo==200')

                x.ValorManejoClienteCondicionesComercialesTemp = x.TarifaCarga.Codigo == 200 ? $linq.Enumerable().From(x.CondicionesComercialesKilo).Where('$.TipoTarifa.ValorRangoInicial <=' + parseFloat(x.PesoCalcular)).ToArray() : $linq.Enumerable().From(x.CondicionesComercialesFijo).Where('$.TipoTarifa.ValorRangoInicial <=' + parseFloat(x.PesoCalcular)).ToArray()
                x.ValorManejoClienteCondicionesComercialesTemp = x.TarifaCarga.Codigo == 200 ? $linq.Enumerable().From(x.ValorManejoClienteCondicionesComercialesTemp).Where('$.TipoTarifa.ValorRangoFinal >=' + parseFloat(x.PesoCalcular)).ToArray() : $linq.Enumerable().From(x.ValorManejoClienteCondicionesComercialesTemp).Where('$.TipoTarifa.ValorRangoFinal >=' + parseFloat(x.PesoCalcular)).ToArray()
                x.ValorManejoClienteCondicionesComerciales = x.ValorManejoClienteCondicionesComercialesTemp

                x.ListaAuxTipoTarifasTemp = x.TarifaCarga.Codigo == 200 ? $linq.Enumerable().From(x.ListaAuxTipoTarifasKilo).Where('$.ValorCatalogoTipoTarifa.CampoAuxiliar2 <=' + parseFloat(x.PesoCalcular)).ToArray() : $linq.Enumerable().From(x.ListaAuxTipoTarifasFijo).Where('$.ValorCatalogoTipoTarifa.CampoAuxiliar2 <=' + parseFloat(x.PesoCalcular)).ToArray()
                x.ListaAuxTipoTarifasTemp = x.TarifaCarga.Codigo == 200 ? $linq.Enumerable().From(x.ListaAuxTipoTarifasTemp).Where('$.ValorCatalogoTipoTarifa.CampoAuxiliar3 >=' + parseFloat(x.PesoCalcular)).ToArray() : $linq.Enumerable().From(x.ListaAuxTipoTarifasTemp).Where('$.ValorCatalogoTipoTarifa.CampoAuxiliar3 >=' + parseFloat(x.PesoCalcular)).ToArray()

                x.TarifaCarga = x.ListaAuxTipoTarifasTemp.length > 0 ? x.TarifaCarga : x.ValorManejoClienteCondicionesComerciales.length <= 0 ? x.TarifaCarga.Codigo == 200 ? $linq.Enumerable().From(x.ListaTarifaCarga).First('$.Codigo==201') : $linq.Enumerable().From(x.ListaTarifaCarga).First('$.Codigo==200') : x.TarifaCarga

                x.ListaAuxTipoTarifasTemp = x.TarifaCarga.Codigo == 200 ? $linq.Enumerable().From(x.ListaAuxTipoTarifasKilo).Where('$.ValorCatalogoTipoTarifa.CampoAuxiliar2 <=' + parseFloat(x.PesoCalcular)).ToArray() : $linq.Enumerable().From(x.ListaAuxTipoTarifasFijo).Where('$.ValorCatalogoTipoTarifa.CampoAuxiliar2 <=' + parseFloat(x.PesoCalcular)).ToArray()
                x.ListaAuxTipoTarifasTemp = x.TarifaCarga.Codigo == 200 ? $linq.Enumerable().From(x.ListaAuxTipoTarifasTemp).Where('$.ValorCatalogoTipoTarifa.CampoAuxiliar3 >=' + parseFloat(x.PesoCalcular)).ToArray() : $linq.Enumerable().From(x.ListaAuxTipoTarifasTemp).Where('$.ValorCatalogoTipoTarifa.CampoAuxiliar3 >=' + parseFloat(x.PesoCalcular)).ToArray()

                x.ListaAuxTipoTarifas = x.ListaAuxTipoTarifasTemp


                x.ValorManejoClienteCondicionComercial = x.ValorManejoClienteCondicionesComerciales.length > 0 ? MascaraNumero(x.ValorComercialCliente) * (x.ValorManejoClienteCondicionesComerciales[0].PorcentajeValorDeclarado / 100) : 0

                // Si no se encuentran tarifas en condiciones comerciales ni en el tarifario, se marca el error:
                x.msPesoCliente = x.ListaAuxTipoTarifas.length <= 0 /*&& /*x.ValorManejoClienteCondicionesComerciales.length <= 0 */? 'No hay tarifa disponible para el peso ingresado' : ''
                x.msGeneral = x.ListaAuxTipoTarifas.length <= 0/* && /*x.ValorManejoClienteCondicionesComerciales.length <= 0*/ ? 'No hay tarifa disponible para el peso ingresado' : ''
                x.stGeneral = x.ListaAuxTipoTarifas.length <= 0/* && /*x.ValorManejoClienteCondicionesComerciales.length <= 0*/ ? 'background:red' : ''
                x.stRow = x.ListaAuxTipoTarifas.length <= 0/* && /*x.ValorManejoClienteCondicionesComerciales.length <= 0 */? 'background:yellow' : x.stRow
                x.Error = x.ListaAuxTipoTarifas.length <= 0/* && /*x.ValorManejoClienteCondicionesComerciales.length <= 0 */? 1 : x.Error


                // Si el cálculo de valor manejo condiciones comerciales es menor al manejo minimo parametrizado de condiciones comerciales, se le asigna el valor mínimo parametrizado en condiciones comerciales:
               
               
                
                x.ValorFleteCliente = x.ListaAuxTipoTarifas.length <= 0 && x.ValorManejoClienteCondicionesComerciales.length <= 0 ? 0 : x.ListaAuxTipoTarifas.length > 0 ? x.TarifaCarga.Codigo == TARIFA_RANGO_PESO_VALOR_KILO ? x.PesoCalcular * x.ListaAuxTipoTarifas[0].ValorFlete : x.ListaAuxTipoTarifas[0].ValorFlete : x.ValorManejoClienteCondicionesComerciales[0].FleteMinimo
                x.ValorReexpedicion = x.ListaAuxTipoTarifas.length <= 0 ? 0 : x.ListaAuxTipoTarifas[0].Reexpedicion == 1 && x.ListaAuxTipoTarifas[0].PorcentajeReexpedicion > 0 ? Math.round((x.ListaAuxTipoTarifas[0].PorcentajeReexpedicion / 100) * x.ValorFleteCliente) : 0
                x.ValorFleteCliente = x.ValorReexpedicion > 0 ? x.ValorFleteCliente - x.ValorReexpedicion : x.ValorManejoClienteCondicionesComerciales.length > 0 ? x.Peso_Volumetrico > x.PesoCliente ? (x.ValorFleteCliente - Math.round(x.ValorFleteCliente * (x.ValorManejoClienteCondicionesComerciales[0].PorcentajeVolumen / 100))) > x.ValorManejoClienteCondicionesComerciales[0].FleteMinimo ? x.ValorFleteCliente - Math.round(x.ValorFleteCliente * (x.ValorManejoClienteCondicionesComerciales[0].PorcentajeVolumen / 100)) : x.ValorManejoClienteCondicionesComerciales[0].FleteMinimo : (x.ValorFleteCliente - Math.round(x.ValorFleteCliente * (x.ValorManejoClienteCondicionesComerciales[0].PorcentajeFlete / 100))) > x.ValorManejoClienteCondicionesComerciales[0].FleteMinimo ? x.ValorFleteCliente - Math.round(x.ValorFleteCliente * (x.ValorManejoClienteCondicionesComerciales[0].PorcentajeFlete / 100)) : x.ValorManejoClienteCondicionesComerciales[0].FleteMinimo : x.ValorFleteCliente
                x.ValorManejoCliente = x.ValorReexpedicion > 0 ? Math.round((parseInt(MascaraNumero(x.ValorComercialCliente)) * x.ListaAuxTipoTarifas[0].PorcentajeSeguro) / 100) : x.ListaAuxTipoTarifas.length <= 0 && x.ValorManejoClienteCondicionesComerciales.length <= 0 ? 0 : MascaraNumero(x.ValorComercialCliente) > 0 ? (MascaraNumero(x.ValorComercialCliente) * (x.ValorManejoClienteCondicionesComerciales[0].PorcentajeValorDeclarado / 100)) > x.ValorManejoClienteCondicionesComerciales[0].ManejoMinimo ? (MascaraNumero(x.ValorComercialCliente) * (x.ValorManejoClienteCondicionesComerciales[0].PorcentajeValorDeclarado / 100)) : x.ValorManejoClienteCondicionesComerciales[0].ManejoMinimo : x.ValorManejoClienteCondicionComercial > 0 ? x.ValorManejoClienteCondicionComercial > x.ValorManejoClienteCondicionesComerciales[0].ManejoMinimo ? x.ValorManejoClienteCondicionComercial : x.ValorManejoClienteCondicionesComerciales[0].ManejoMinimo : x.Linea_Negocio_Paqueteria.Codigo == CATALOGO_LINEA_NEGOCIO_PAQUETERIA.PAQUETERIA ? $scope.Cliente.Cliente.Tarifario.Paqueteria.MinimoValorManejo : $scope.Cliente.Cliente.Tarifario.Paqueteria.MinimoValorSeguro
                x.ValorManejoCliente = x.ValorReexpedicion > 0 ? x.ValorManejoCliente : x.ValorManejoClienteCondicionesComerciales.length > 0 ? x.ValorManejoCliente > x.ValorManejoClienteCondicionesComerciales[0].ManejoMinimo ? x.ValorManejoCliente : x.ValorManejoClienteCondicionesComerciales[0].ManejoMinimo : x.ValorManejoCliente
                x.ValorSeguroCliente = $scope.Cliente.Cliente.Tarifario.Paqueteria.MinimoValorSeguro
                x.TotalFleteCliente = x.ValorManejoCliente + x.ValorFleteCliente + MascaraNumero(x.ValorReexpedicion) + MascaraNumero(x.Acarreo_Local) + MascaraNumero(x.Acarreo_Destino)
                
                //x.ValorFleteCliente = x.TarifaCarga.Codigo == TARIFA_RANGO_PESO_VALOR_KILO ? x.PesoCalcular * x.ListaAuxTipoTarifas.ValorFlete : x.ListaAuxTipoTarifas.ValorFlete;
                //x.ValorManejoCliente = x.ListaAuxTipoTarifas.ValorManejo;
                //x.ValorManejoClienteCondicionesComerciales = x.ValorManejoClienteCondicionesComerciales.length > 0 ? $linq.Enumerable().From(x.ValorManejoClienteCondicionesComerciales).Where('$.TipoTarifa.ValorRangoInicial >=' + parseFloat(x.PesoCalcular)).ToArray() : [] x.Linea_Negocio_Paqueteria.Codigo == CATALOGO_LINEA_NEGOCIO_PAQUETERIA.PAQUETERIA ? $scope.Cliente.Cliente.Tarifario.Paqueteria.MinimoValorManejo : $scope.Cliente.Cliente.Tarifario.Paqueteria.MinimoValorSeguro
                //x.ValorSeguroCliente = x.ListaAuxTipoTarifas.ValorSeguro;
                return x
            }).ToArray();

            //Antes de armar la lista de errores , se consulta la lista de identificaciones de destinatarios, la consulta retorna los números de identificación junto con su respectivo código, en caso de no existir el tercero, el código retorna como 0:
            var ListaCodigosDestinatarios = CargueMasivoGuiasFactory.ConsultarTercerosIdentificacion({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, strIdentificaciones: strIdentificacionesDestinatarios, Sync: true }).Datos

            // se asignan los códigos retornados a la lista DataArchivo : 
            if (ListaCodigosDestinatarios != undefined) {
                if (ListaCodigosDestinatarios.length > 0) {
                    $scope.DataArchivo = $linq.Enumerable().From($scope.DataArchivo).Select(function (x) {
                        try {
                            x.Destinatario.Codigo = $linq.Enumerable().From(ListaCodigosDestinatarios).First('$.NumeroIdentificacion==' + x.Destinatario.NumeroIdentificacion).Codigo;
                        } catch (e) {

                            // si el número de identificación no se encuentra en la lista de consulta, quiere decir que se debe crear, se arma la lista de los nuevos terceros:

                            x.Destinatario.Codigo = 0
                            if (ListaNuevosTerceros.length > 0) {
                                var cdr = 0
                                ListaNuevosTerceros.forEach(item => {
                                    if (x.Destinatario.NumeroIdentificacion == item.NumeroIdentificacion) {
                                        cdr++
                                    }
                                })
                                if (cdr == 0) {
                                    ListaNuevosTerceros.push(x.Destinatario)
                                }
                            } else {
                                ListaNuevosTerceros.push(x.Destinatario)
                            }

                            strListaNuevosTerceros = strListaNuevosTerceros + x.Destinatario.NumeroIdentificacion + ','
                            console.log('el tercero con No. Identificación : ' + x.Destinatario.NumeroIdentificacion + ' aún no existe en base de datos. ' + e)
                        }
                        return x
                    }).ToArray();
                } else {
                    $scope.DataArchivo = $linq.Enumerable().From($scope.DataArchivo).Select(function (x) {
                        x.Destinatario.Codigo = 0
                        if (ListaNuevosTerceros.length > 0) {
                            var cdr = 0
                            ListaNuevosTerceros.forEach(item => {
                                if (x.Destinatario.NumeroIdentificacion == item.NumeroIdentificacion) {
                                    cdr++
                                }
                            })
                            if (cdr == 0) {
                                ListaNuevosTerceros.push(x.Destinatario)
                            }
                        } else {
                            ListaNuevosTerceros.push(x.Destinatario)
                        }

                        strListaNuevosTerceros = strListaNuevosTerceros + x.Destinatario.NumeroIdentificacion + ','

                        return x
                    }).ToArray();
                    console.log('No se encontró ningún tercero')
                }
            } else {
                console.log('No hay Terceros con la información ingresada')
            }


            $scope.ListaErrores = $linq.Enumerable().From($scope.DataArchivo).Where('$.Error==1 && $.Omitido == false').ToArray();
            $scope.ListaDatosCorrectos = $linq.Enumerable().From($scope.DataArchivo).Where('$.Error!=1 && $.Omitido == false').ToArray();
            blockUI.stop();


            //if (contadorgeneral > 0) {
            if ($scope.ListaErrores.length > 0) {
                ShowError('Se encontraron inconsistencias en los datos ingresados, por favor corrija los datos marcados o seleccione omitirlos.');
                $scope.paginaActualError = 1
                $scope.DataErrorActual = []
                $scope.totalRegistrosErrores = $scope.ListaErrores.length
                $scope.totalPaginasErrores = Math.ceil($scope.totalRegistrosErrores / 20);
                if ($scope.totalRegistrosErrores > 20) {
                    for (var i = 0; i < 20; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
                else {
                    for (var i = 0; i < $scope.ListaErrores.length; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
            }
            else if ($scope.ListaDatosCorrectos.length <= 0) {
                ShowError('No se encontraron registros para validar, por favor revise que la casilla de omitir no se encuentre marcada en al menos un registro')
            }
            else {
                //blockUI.start();
                //$timeout(function () {
                //    blockUI.message('Validando tarifas');
                //}, 1000);
                // $scope.ValidarTarifas(0)


                showModal('modalConfirmacionGuardarPorgramacion')


                blockUI.stop();
            }

        }

        $scope.CargueMasivo = function () {
            PantallaBloqueo($scope.Guardar)
        }

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardarPorgramacion')

            // se crean los terceros que se identifiquen como nuevos y se actualizan los códigos en DataArchivo:

            UltimoConsecutivoTercero = { Codigo: 0 }
            UltimoConsecutivoTercero = CargueMasivoGuiasFactory.ObtenerUltimoConsecutivoTercero({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Sync: true }).Datos
            var UltimoCodigo = 0
            if (UltimoConsecutivoTercero != undefined && UltimoConsecutivoTercero.Codigo > 0) {
               
                for (var i = 0; i < ListaNuevosTerceros.length; i++) {
                    UltimoConsecutivoTercero.Codigo++;
                    ListaNuevosTerceros[i].Codigo = UltimoConsecutivoTercero.Codigo
                    //se identifica el ultimo codigo para posteriormente actualizar la tabla tipo documentos, ya que el código del tercero se genera como si fuera un documento
                    if (i == ListaNuevosTerceros.length - 1) {
                        UltimoCodigo = UltimoConsecutivoTercero.Codigo
                    }
                }
            }
            if (ListaNuevosTerceros.length > 0) {
                var objGuardar = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: UltimoCodigo,
                    ListaNuevosTerceros: ListaNuevosTerceros,
                    Sync: true

                }

                var ResponseCargueMasivoTerceros = CargueMasivoGuiasFactory.InsertarNuevosTerceros(objGuardar)
                if (ResponseCargueMasivoTerceros != undefined) {
                    console.log('se insertaron ' + ResponseCargueMasivoTerceros.Datos + ' nuevos terceros')
                } else {
                    ShowError('No se insertaron algunos destinatarios, por favor consulte con el administrador del sistema')
                }
            }

            // se actualizan códigos :
            var ListaCodigosNuevosDestinatarios = CargueMasivoGuiasFactory.ConsultarTercerosIdentificacion({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, strIdentificaciones: strListaNuevosTerceros, Sync: true }).Datos

            // se asignan los códigos retornados a la lista de datos correctos : 
            if (ListaCodigosNuevosDestinatarios != undefined) {
                if (ListaCodigosNuevosDestinatarios.length > 0) {
                    $scope.ListaDatosCorrectos = $linq.Enumerable().From($scope.ListaDatosCorrectos).Select(function (x) {
                        try {
                            x.Destinatario.Codigo = $linq.Enumerable().From(ListaCodigosNuevosDestinatarios).First('$.NumeroIdentificacion==' + x.Destinatario.NumeroIdentificacion).Codigo;
                        } catch (e) {

                            // si el número de identificación no se encuentra en la lista de consulta, hubo un error en BD
                            console.log('el destinatario con No. Identificación : ' + x.Destinatario.NumeroIdentificacion + ' no se creó en base de datos. ' + e)
                        }
                        return x
                    }).ToArray();
                } else {
                    console.log('No se encontró ningún destinatario nuevo para crear')
                }
            } else {
                console.log('No hay destinatarios con la información ingresada')
            }

            //primero se obtiene el último Numero_Documento y Número de la tabla Encabezado_Remesas para generar los consecutivos, proceso similar a la creación de terceros:
            var UltimaGuia = CargueMasivoGuiasFactory.ObtenerUltimaGuia({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Sync: true }).Datos
            var UltimoConsecutivoGuia = 0
            if (UltimaGuia != undefined) {
                
                for (var j = 0; j < $scope.ListaDatosCorrectos.length; j++) {
                    UltimaGuia.Remesa.Numero++;
                    UltimaGuia.Remesa.NumeroDocumento++;
                    $scope.ListaDatosCorrectos[j].Numero = UltimaGuia.Remesa.Numero
                    $scope.ListaDatosCorrectos[j].NumeroDocumento = UltimaGuia.Remesa.NumeroDocumento
                    if (j == $scope.ListaDatosCorrectos.length - 1) {
                        UltimoConsecutivoGuia = UltimaGuia.Remesa.NumeroDocumento
                    }
                }
            } else {
                ShowError('Error al guardar las guías')
            }


            // Se crea una sola lista con la información de las Tablas Encabezado_Remesas y Remesas_Paqueteria Respectivamente, para posteriormente insertarlas en el DataTable en el Backend:
            $scope.ListaGuardar = $linq.Enumerable().From($scope.ListaDatosCorrectos).Select(function (item) {
                item = {



                    Remesa: {
                        //Encabezado_Remesas:
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Numero: item.Numero,
                        NumeroDocumento: item.NumeroDocumento,
                        NumeroIdentificacionRecibe: UltimoConsecutivoGuia ,
                        TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA },
                        TipoRemesa: { Codigo: item.TipoRemesa },
                        NumeroDocumentoCliente: item.NumeroDocumentoCliente,
                        FechaDocumentoCliente: MIN_DATE,
                        Fecha: new Date(),
                        Ruta: { Codigo: 0 },
                        ProductoTransportado: { Codigo: item.ProductoTransportado.Codigo },
                        FormaPago: { Codigo: item.FormaPago.Codigo },
                        Cliente: { Codigo: $scope.Cliente.Cliente.Codigo },
                        Remitente: { Codigo: item.Remitente.Codigo, NumeroIdentificacion: item.Remitente.NumeroIdentificacion },
                        CiudadRemitente: { Codigo: item.Remitente.Ciudad.Codigo },
                        DireccionRemitente: item.Remitente.Direccion,
                        TelefonoRemitente: item.Remitente.Telefonos,
                        Observaciones: item.ObservacionesRemitente,
                        CantidadCliente: item.CantidadCliente,
                        PesoCliente: item.PesoCalcular,
                        PesoVolumetricoCliente: item.PesoVolumetricoCliente,
                        ValorComisionOficina: 0,
                        ValorFleteCliente: item.ValorFleteCliente,
                        ValorManejoCliente: item.ValorManejoCliente,
                        ValorSeguroCliente: item.ValorManejoCliente,
                        ValorDescuentoCliente: 0,
                        TotalFleteCliente: item.TotalFleteCliente,
                        ValorComercialCliente: MascaraNumero(item.ValorComercialCliente),
                        ValorCargue: MascaraNumero(item.ValorCargue),
                        ValorDescargue: MascaraNumero(item.ValorDescargue),
                        CantidadTransportador: 0,
                        PesoTransportador: 0,
                        ValorFleteTransportador: 0,
                        TotalFleteTransportador: 0,
                        Destinatario: { Codigo: item.Destinatario.Codigo, NumeroIdentificacion: item.Destinatario.NumeroIdentificacion },
                        CiudadDestinatario: { Codigo: item.Destinatario.Ciudad.Codigo },
                        DireccionDestinatario: item.Destinatario.Direccion,
                        TelefonoDestinatario: item.Destinatario.Telefonos,
                        Estado: ESTADO_ACTIVO,
                        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                        Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                        TarifarioVenta: { Codigo: $scope.Cliente.Cliente.Tarifario.Codigo },
                        NumeroOrdenServicio: 0,
                        BarrioRemitente: item.BarrioRemitente,
                        CodigoPostalRemitente: item.CodigoPostalRemitente,
                        BarrioDestinatario: item.BarrioDestinatario,
                        CodigoPostalDestinatario: item.CodigoPostalDestinatario,
                        DetalleTarifaVenta: { Codigo: item.TarifaCarga != undefined ? item.TarifaCarga.Codigo : 0 },
                        Numeracion: item.No_PreImpreso

                    },


                    //Remesas_Paqueteria:

                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    TransportadorExterno: { Codigo: 0 },
                    NumeroGuiaExterna: '',
                    TipoEntregaRemesaPaqueteria: { Codigo: item.TipoEntregaRemesaPaqueteria.Codigo },

                    EstadoRemesaPaqueteria: { Codigo: $scope.Modelo.EstadoGuia.Codigo },
                    ObservacionesDestinatario: item.ObservacionesDestinatario,
                    ObservacionesRemitente: item.ObservacionesRemitente,
                    OficinaOrigen: { Codigo: $scope.Modelo.OficinaOrigen.Codigo },
                    OficinaDestino: { Codigo: $scope.Modelo.OficinaDestino.Codigo },
                    OficinaActual: { Codigo: $scope.Modelo.OficinaActual.Codigo },
                    DesripcionMercancia: item.DescripcionProductoTransportado,
                    PesoCobrar: item.PesoCalcular,
                    Largo: item.Largo,
                    Alto: item.Alto,
                    Ancho: item.Ancho,
                    PesoVolumetrico: item.Peso_Volumetrico,

                    LineaNegocioPaqueteria: { Codigo: item.Linea_Negocio_Paqueteria.Codigo },
                    RecogerOficinaDestino: item.Recoger_Oficina_Destino != undefined ? item.Recoger_Oficina_Destino.Codigo : 0,
                    ManejaDetalleUnidades: 0,
                    ValorReexpedicion: MascaraNumero(item.ValorReexpedicion),
                    CentroCosto: item.CentroCosto,
                    SitioDescargue: { Codigo: item.SitioDescargue != undefined ? item.SitioDescargue.Codigo : 0 },
                    FechaEntrega: item.FechaEntrega,
                    CodigoZona: item.ZonaDestinatario == undefined ? 0 : item.ZonaDestinatario.Codigo,
                    Latitud: item.Latitud,
                    Longitud: item.Longitud,
                    HorarioEntregaRemesa: { Codigo: item.Horario_Entrega == undefined ? 0 : item.Horario_Entrega.Codigo },
                    FechaInterfazTracking: new Date()

                }
                return item
            }).ToArray();

            CargueMasivoGuiasFactory.InsertarMasivo({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, RemesaPaqueteria: $scope.ListaGuardar }).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        ShowSuccess('se guardaron ' + $scope.ListaGuardar.length + ' guías')

                        $scope.DataActual = []
                        blockUI.stop()
                    } else {
                        ShowError('error :' + response.data.MensajeOperacion + ' ', response.statusText)
                    }

                }, function (response) { 'No se ejecutó el proceso ' + ShowError(response.statusText) });

            blockUI.stop()

        }
       
        /*---------------------------------------------------------------------Funcion validar datos requeridos-----------------------------------------------------------------------------*/
        function DatosRequeridos() {

        }
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskHora = function () {
            MascaraHorasGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (o) {
            return MascaraNumero(o)
        }
        $scope.MaskValoresGrid = function (o) {
            return MascaraValores(o)
        }
        $scope.MaskMayusGrid = function (o) {
            return MascaraMayus(o)
        }
        $scope.MaskplacaGrid = function (o) {
            return MascaraPlaca(o)
        }
        $scope.MaskplacaSemiremolqueGrid = function (o) {
            return MascaraPlacaSemirremolque(o)
        }

        $scope.GenerarPlanilla = function () {
            var entidad = { CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }
            blockUI.start();
            $timeout(function () {
                blockUI.message('Generando Plantilla..');
            }, 1000);
            CargueMasivoGuiasFactory.GenerarPlantilla(entidad).then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    var pdfAsDataUri = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + response.data.Datos.Planitlla;
                    var link = document.createElement('a');
                    link.href = pdfAsDataUri;
                    link.download = "Plantilla_Cargue_Masivo_Guias_Cliente.xlsx";
                    link.click();
                    //window.open(pdfAsDataUri);
                    blockUI.stop();
                    ShowSuccess('La plantilla se generó correctamente')
                    blockUI.stop();
                }
            }, function (response) {
                blockUI.stop();
                ShowError(response.statusText)
            })
            
        }
        $scope.urlASP = '';
        $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);

      
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

       

        $scope.Probar = function () {
            CargueMasivoGuiasFactory.InsertarMasivo({ CodigoEmpresa: 10 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        ShowSuccess('se generó el csv')
                    } else {
                        ShowError('error :' + response.data.MensajeOperacion + ' ', response.statusText)
                    }

                }, function (response) { 'No se ejecutó el proceso ' + ShowError(response.statusText) });
        }

        $scope.ObtenerTercero = function () {
            if ($scope.Cliente.Codigo != undefined) {
                PantallaBloqueoTercero(ObtenerTercero)
            }
        }
        function ObtenerTercero() {
            if ($scope.Cliente != undefined && $scope.Cliente != null && $scope.Cliente != '') {
                if ($scope.Cliente.Codigo > 0) {
                    $scope.Cliente = TercerosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Cliente.Codigo, Sync: true }).Datos;
                    $scope.Cliente.Cliente.Tarifario = TarifarioVentasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Cliente.Cliente.Tarifario.Codigo, Sync: true }).Datos;
                    console.log('Tarifario : ' + $scope.Cliente.Cliente.Tarifario)
                }
            }
            blockUI.stop()
        }

        function PantallaBloqueo(fun) {
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Guardando registros...");
            $timeout(function () { blockUI.message("Espere por favor..."); fun(); }, 100);
        }
        function PantallaBloqueoValidacion(fun) {
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Validando registros...");
            $timeout(function () { blockUI.message("Espere por favor..."); fun(); }, 100);
        }
        function PantallaBloqueoTercero(fun) {
            blockUI.stop()
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Validando información Cliente...");
            $timeout(function () { blockUI.message("Validando información Cliente..."); fun(); }, 100);
        }
        function PantallaBloqueoTarifario(fun) {
           
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Consultando información tarifario paquetería...");
            $timeout(function () { blockUI.message("Consultando información tarifario paquetería..."); fun(); }, 100);
        }
    }]);

