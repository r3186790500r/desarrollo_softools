﻿PRINT 'gsp_obtener_terceros'
GO
DROP PROCEDURE gsp_obtener_terceros
GO
CREATE PROCEDURE gsp_obtener_terceros  
(  
@par_EMPR_Codigo smallint,  
@par_Codigo Numeric  = null,
@par_Identificacion VARCHAR(20) = NULL
)  
AS   
BEGIN  
SELECT   
1 as Obtener,  
TERC.EMPR_Codigo,--  
TERC.Codigo,--  
ISNULL(TERC.Codigo_Alterno,'') AS  Codigo_Alterno,--  
ISNULL(TERC.Codigo_Contable,'') AS  Codigo_Contable,  
TERC.CATA_TINT_Codigo,  
TERC.CATA_TIID_Codigo,  
TERC.Numero_Identificacion,  
TERC.Digito_Chequeo,  
ISNULL(TERC.Razon_Social,'') AS Razon_Social ,  
ISNULL(TERC.Representante_Legal,'') AS  Representante_Legal,  
ISNULL(TERC.Nombre,'') AS  Nombre,  
TERC.Apellido1,  
TERC.Apellido2,  
TERC.CATA_SETE_Codigo,  
TERC.CIUD_Codigo_Identificacion,  
TERC.CIUD_Codigo_Nacimiento,  
TERC.CIUD_Codigo AS CIUD_Codigo_Direccion,  
--0 as CIUD_Codigo_Direccion,  
TERC.Direccion,  
ISNULL(TERC.Codigo_Postal,'') AS Codigo_Postal ,  
TERC.Telefonos,  
ISNULL(TERC.Celulares,'') AS Celulares ,  
ISNULL(TERC.Emails,'') AS Emails ,  
TERC.BANC_Codigo,  
TERC.CATA_TICB_Codigo,  
ISNULL(TERC.Numero_Cuenta_Bancaria,'') AS Numero_Cuenta_Bancaria ,  
ISNULL(TERC.TERC_Codigo_Beneficiario,0) AS TERC_Codigo_Beneficiario,  
ISNULL(TERC.Titular_Cuenta_Bancaria,'') AS  Titular_Cuenta_Bancaria,  
TERC.Observaciones,  
TERC.Foto,  
ISNULL(TERC.Justificacion_Bloqueo,'') AS Justificacion_Bloqueo ,  
TERC.Estado  
FROM Terceros AS TERC  
WHERE TERC.EMPR_Codigo = @par_EMPR_Codigo  
AND TERC.Codigo = isnull(@par_Codigo  ,TERC.Codigo)
AND TERC.Numero_Identificacion =  isnull(@par_Identificacion  ,TERC.Numero_Identificacion)



  
END  
GO
