﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Despachos
Imports EncoExpres.GesCarga.Repositorio.Despachos

Namespace Despachos
    Public NotInheritable Class PersistenciaEnturnamiento
        Implements IPersistenciaBase(Of Enturnamiento)

        Public Function Consultar(filtro As Enturnamiento) As IEnumerable(Of Enturnamiento) Implements IPersistenciaBase(Of Enturnamiento).Consultar
            Return New RepositorioEnturnamiento().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As Enturnamiento) As Long Implements IPersistenciaBase(Of Enturnamiento).Insertar
            Return New RepositorioEnturnamiento().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As Enturnamiento) As Long Implements IPersistenciaBase(Of Enturnamiento).Modificar
            Return New RepositorioEnturnamiento().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As Enturnamiento) As Enturnamiento Implements IPersistenciaBase(Of Enturnamiento).Obtener
            Return New RepositorioEnturnamiento().Obtener(filtro)
        End Function
        Public Function Anular(entidad As Enturnamiento) As Boolean
            Return New RepositorioEnturnamiento().Anular(entidad)
        End Function

    End Class
End Namespace