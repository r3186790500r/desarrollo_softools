﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.Tesoreria
Imports EncoExpres.GesCarga.Repositorio.Basico.Tesoreria

Namespace Basico.Tesoreria
    Public NotInheritable Class PersistenciaDetalleParametrizacionContables
        Implements IPersistenciaBase(Of DetalleParametrizacionContables)

        Public Function Consultar(filtro As DetalleParametrizacionContables) As IEnumerable(Of DetalleParametrizacionContables) Implements IPersistenciaBase(Of DetalleParametrizacionContables).Consultar
            Return New RepositorioDetalleParametrizacionContables().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As DetalleParametrizacionContables) As Long Implements IPersistenciaBase(Of DetalleParametrizacionContables).Insertar
            Return New RepositorioDetalleParametrizacionContables().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As DetalleParametrizacionContables) As Long Implements IPersistenciaBase(Of DetalleParametrizacionContables).Modificar
            Return New RepositorioDetalleParametrizacionContables().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As DetalleParametrizacionContables) As DetalleParametrizacionContables Implements IPersistenciaBase(Of DetalleParametrizacionContables).Obtener
            Return New RepositorioDetalleParametrizacionContables().Obtener(filtro)
        End Function
        Public Function Anular(entidad As DetalleParametrizacionContables) As Boolean
            Return New RepositorioDetalleParametrizacionContables().Anular(entidad)
        End Function
    End Class

End Namespace

