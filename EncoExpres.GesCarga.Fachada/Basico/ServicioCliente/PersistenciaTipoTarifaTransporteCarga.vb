﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.ServicioCliente
Imports EncoExpres.GesCarga.Repositorio.Basico.ServicioCliente

Namespace Basico.ServicioCliente
    Public NotInheritable Class PersistenciaTipoTarifaTransporteCarga
        Implements IPersistenciaBase(Of TipoTarifaTransporteCarga)

        Public Function Consultar(filtro As TipoTarifaTransporteCarga) As IEnumerable(Of TipoTarifaTransporteCarga) Implements IPersistenciaBase(Of TipoTarifaTransporteCarga).Consultar
            Return New RepositorioTipoTarifaTransporteCarga().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As TipoTarifaTransporteCarga) As Long Implements IPersistenciaBase(Of TipoTarifaTransporteCarga).Insertar
            Return New RepositorioTipoTarifaTransporteCarga().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As TipoTarifaTransporteCarga) As Long Implements IPersistenciaBase(Of TipoTarifaTransporteCarga).Modificar
            Return New RepositorioTipoTarifaTransporteCarga().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As TipoTarifaTransporteCarga) As TipoTarifaTransporteCarga Implements IPersistenciaBase(Of TipoTarifaTransporteCarga).Obtener
            Return New RepositorioTipoTarifaTransporteCarga().Obtener(filtro)
        End Function
    End Class

End Namespace

