﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Repositorio.Basico.General
Namespace Basico.General
    Public NotInheritable Class PersistenciaTRM
        Implements IPersistenciaBase(Of TRM)

        Public Function Consultar(filtro As TRM) As IEnumerable(Of TRM) Implements IPersistenciaBase(Of TRM).Consultar
            Return New RepositorioTRM().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As TRM) As Long Implements IPersistenciaBase(Of TRM).Insertar
            Return New RepositorioTRM().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As TRM) As Long Implements IPersistenciaBase(Of TRM).Modificar
            Return New RepositorioTRM().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As TRM) As TRM Implements IPersistenciaBase(Of TRM).Obtener
            Return New RepositorioTRM().Obtener(filtro)
        End Function
        Public Function Anular(entidad As TRM) As Boolean
            Return New RepositorioTRM().Anular(entidad)
        End Function

    End Class

End Namespace

