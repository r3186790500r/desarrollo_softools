﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Paqueteria
Imports EncoExpres.GesCarga.Repositorio.Paqueteria

Namespace Paqueteria
    Public NotInheritable Class PersistenciaPlanillaPaqueteria
        Implements IPersistenciaBase(Of PlanillaPaqueteria)

        Public Function Consultar(filtro As PlanillaPaqueteria) As IEnumerable(Of PlanillaPaqueteria) Implements IPersistenciaBase(Of PlanillaPaqueteria).Consultar
            Return New RepositorioPlanillaPaqueteria().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As PlanillaPaqueteria) As Long Implements IPersistenciaBase(Of PlanillaPaqueteria).Insertar
            Return New RepositorioPlanillaPaqueteria().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As PlanillaPaqueteria) As Long Implements IPersistenciaBase(Of PlanillaPaqueteria).Modificar
            Return New RepositorioPlanillaPaqueteria().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As PlanillaPaqueteria) As PlanillaPaqueteria Implements IPersistenciaBase(Of PlanillaPaqueteria).Obtener
            Return New RepositorioPlanillaPaqueteria().Obtener(filtro)
        End Function
        Public Function Anular(filtro As PlanillaPaqueteria) As PlanillaPaqueteria
            Return New RepositorioPlanillaPaqueteria().Anular(filtro)
        End Function
        Public Function EliminarGuia(entidad As PlanillaPaqueteria) As Boolean
            Return New RepositorioPlanillaPaqueteria().EliminarGuia(entidad)
        End Function
        Public Function EliminarRecoleccion(entidad As PlanillaPaqueteria) As Boolean
            Return New RepositorioPlanillaPaqueteria().EliminarRecoleccion(entidad)
        End Function
        Public Function ConsultarPlanillasPendientesRecepcionar(filtro As PlanillaPaqueteria) As IEnumerable(Of PlanillaPaqueteria)
            Return New RepositorioPlanillaPaqueteria().ConsultarPlanillasPendientesRecepcionar(filtro)
        End Function

        Public Function ObtenerPlanilla(filtro As PlanillaPaqueteria) As IEnumerable(Of PlanillaPaqueteria)
            Return New RepositorioPlanillaPaqueteria().ObtenerPlanillaPaqueteria(filtro)
        End Function

    End Class
End Namespace

