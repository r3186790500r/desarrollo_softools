﻿EncoExpresApp.controller("VerificarProcesoDescargueCtrl", ['$scope', '$linq', '$timeout', 'blockUI', 'blockUIConfig', 'ValidacionPlanillaDespachosFactory', '$routeParams', 'PlanillaDespachosFactory', 'PlanillaGuiasFactory', 'RemesaGuiasFactory', 'PlanillaPaqueteriaFactory',
    function ($scope, $linq, $timeout, blockUI, blockUIConfig, ValidacionPlanillaDespachosFactory, $routeParams, PlanillaDespachosFactory, PlanillaGuiasFactory, RemesaGuiasFactory, PlanillaPaqueteriaFactory) {
        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Procesos' }, { Nombre: 'Verificar Proceso Descargue' }];

        //Declaración Variables:

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PAQUETERIA.VALIDAR_PROCESO_DESCARGUE)
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta página');
            document.location.href = '#';
        }

        $scope.PlacaVehiculo = '';
        $scope.NombreConductor = '';
        $scope.NombreTenedor = '';
        $scope.NombreRuta = '';
        $scope.PlacaSemirremolque = '';
        $scope.MostrarSeccionGuias = false;
        $scope.Planilla = [];
        $scope.ListadoGuias = [];
        $scope.MensajesError = [];
        $scope.MensajesConfirmacion = [];
        $scope.NumeroPlanilla = '';
        $scope.NumeroDocumentoPlanilla = '';
        $scope.Observaciones = '';

        //Métodos:

        $scope.ObtenerPlanilla = function () {
            $scope.MensajesError = [];
            $scope.ListadoGuias = [];
            $scope.MostrarSeccionGuias = false;
            if ($scope.NumeroPlanilla != undefined && $scope.NumeroPlanilla != null && $scope.NumeroPlanilla != '' && !isNaN($scope.NumeroPlanilla)) {
                blockUI.start('Buscando Planilla...');
                $timeout(function () {
                    blockUI.message('Espere por favor...')
                }, 100);

                var ResponsePlanilla = '';

                ResponsePlanilla = PlanillaDespachosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: $scope.NumeroPlanilla, TipoDocumento: 135, Estado: { Codigo: 1 }, RecibirOficina: true, NumeroPlanilla: -1, Anulado: -1, Sync: true }).Datos[0];
                if (ResponsePlanilla != undefined && ResponsePlanilla != '') {
                    var ResponseEncabezadoPlanilla = PlanillaGuiasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: ResponsePlanilla.Numero, Sync: true }).Datos;
                    $scope.Planilla = PlanillaPaqueteriaFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: ResponsePlanilla.Numero, TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_PAQUETERIA, TipoConsulta: 1, Sync: true }).Datos;
                    $scope.NumeroDocumentoPlanilla = ResponseEncabezadoPlanilla.Planilla.Numero;
                    $scope.PlacaVehiculo = $scope.CargarVehiculos(ResponseEncabezadoPlanilla.Planilla.Vehiculo.Codigo).Placa;
                    $scope.NombreConductor = $scope.CargarTercero(ResponseEncabezadoPlanilla.Planilla.Vehiculo.Conductor.Codigo).NombreCompleto;
                    $scope.NombreTenedor = $scope.Planilla.Planilla.Tenedor == null ? '' : $scope.CargarTercero($scope.Planilla.Planilla.Tenedor.Codigo).NombreCompleto;
                    $scope.NombreRuta = ResponseEncabezadoPlanilla.Planilla.Ruta.Nombre;
                    $scope.PlacaSemirremolque = ResponseEncabezadoPlanilla.Planilla.Semirremolque.Codigo == 0 ? '' : $scope.CargarSemirremolqueCodigo(ResponseEncabezadoPlanilla.Planilla.Semirremolque.Codigo).Placa;
                    $scope.FechaSalida = new Date($scope.Planilla.Planilla.FechaHoraSalida);
                    var horaSalida = $scope.Planilla.Planilla.FechaHoraSalida == null ? '00' : new Date($scope.Planilla.Planilla.FechaHoraSalida).getHours().toString();
                    if ($scope.FechaSalida.getMinutes().toString().length == 1) {
                        $scope.HoraSalida = horaSalida += ':0' + $scope.FechaSalida.getMinutes().toString();
                    } else {
                        $scope.HoraSalida = horaSalida += ':' + $scope.FechaSalida.getMinutes().toString();
                    }
                    $scope.MostrarSeccionGuias = true;
                } else {

                    $scope.MensajesError.push('La planilla ingresada no es válida');
                    $("#window").animate({
                        scrollTop: $('.breadcrumb').offset().top
                    }, 200);
                }
                blockUI.stop();
            }
        }

        $scope.ValidarGuia = function () {
            var cont = 0;
            $scope.MensajesError = [];
            if ($scope.NumeroGuia != undefined && $scope.NumeroGuia != null && $scope.NumeroGuia != '' && !isNaN($scope.NumeroGuia)) {
                blockUI.start('Buscando Guía...');
                $timeout(function () {
                    blockUI.message('Espere por favor...')
                }, 100);
                if ($scope.Planilla != undefined && $scope.Planilla.Planilla.Detalles.length > 0) {
                    $scope.Planilla.Planilla.Detalles.forEach(item => {
                        if (item.Remesa.NumeroDocumento == $scope.NumeroGuia) {
                            if ($scope.ListadoGuias.length > 0) {
                                var countListadoGuias = 0;
                                $scope.ListadoGuias.forEach(itemGuia => {
                                    if (itemGuia.Remesa.NumeroDocumento == $scope.NumeroGuia) {
                                        countListadoGuias++;
                                    }

                                });
                                if (countListadoGuias == 0) {
                                    $scope.ListadoGuias.push(item);
                                } else {

                                    $scope.MensajesError.push('La guía ingresada ya se encuentra verificada')
                                    $("#window").animate({
                                        scrollTop: $('.breadcrumb').offset().top
                                    }, 200);
                                }
                            } else {
                                $scope.ListadoGuias.push(item);
                            }
                        } else {
                            cont++;
                        }
                    });

                    if (cont == $scope.Planilla.Planilla.Detalles.length) {

                        $scope.MensajesError.push('La guía ingresada no es válida o no pertenece a esta planilla')
                        $("#window").animate({
                            scrollTop: $('.breadcrumb').offset().top
                        }, 200);
                    }
                }

                blockUI.stop();
            }
        }

        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardarCargue');
        }

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardarCargue');

            if ($scope.DatosRequeridos()) {
                var Entidad = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Planilla: { Numero: $scope.NumeroDocumentoPlanilla },
                    TipoVerificacion: { Codigo: 22102/*Tipo Validación Descargue*/ },
                    Observaciones: $scope.Observaciones,
                    Estado: ESTADO_ACTIVO,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    ListadoGuias: $scope.ListadoGuias,
                    TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_PAQUETERIA,
                    Oficina: { COdigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo }
                }
                $scope.MensajesConfirmacion = [];
                $scope.MensajesError = []
                ValidacionPlanillaDespachosFactory.Guardar(Entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            $scope.MensajesConfirmacion.push('Se han verificado ' + $scope.ListadoGuias.length + ' guías de la planilla ' + $scope.NumeroPlanilla);
                            $("#window").animate({
                                scrollTop: $('.breadcrumb').offset().top
                            }, 200);
                            $scope.MostrarSeccionGuias = false;
                            $scope.ListadoGuias = [];
                            $scope.NumeroDocumentoPlanilla = '';
                            $scope.NumeroPlanilla = '';
                            $scope.NumeroGuia = '';
                            $scope.PlacaVehiculo = '';
                            $scope.NombreConductor = '';
                            $scope.NombreTenedor = '';
                            $scope.NombreRuta = '';
                            $scope.PlacaSemirremolque = '';
                            $scope.FechaSalida = '';
                            $scope.HoraSalida = '';


                        } else {
                            $scope.MensajesError.push(response.data.MensajeOperacion);
                            $("#window").animate({
                                scrollTop: $('.breadcrumb').offset().top
                            }, 200);
                        }
                    });
            }
        }

        $scope.DatosRequeridos = function () {
            var continuar = true;
            if ($scope.ListadoGuias != undefined && $scope.ListadoGuias != null && $scope.ListadoGuias != '') {
                if ($scope.ListadoGuias.length == 0) {
                    $scope.MensajesError.push('debe existir por lo menos una guía verificada')
                    $("#window").animate({
                        scrollTop: $('.breadcrumb').offset().top
                    }, 200);
                    continuar = false;
                }
            } else {
                $scope.MensajesError.push('debe existir por lo menos una guía verificada')
                $("#window").animate({
                    scrollTop: $('.breadcrumb').offset().top
                }, 200);
            }
            return continuar;
        }

        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope);
        }

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);
        }

    }
]);