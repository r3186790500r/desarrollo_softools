﻿DROP VIEW V_Seguimiento_Vehicular
GO

CREATE VIEW V_Seguimiento_Vehicular 
AS    
SELECT                                                                                    
DESV.EMPR_Codigo,                                                                                      
DESV.ENPD_Numero,                                                                      
0 AS ENOC_Numero,                                       
ISNULL(ENMC.Numero,0) as ENMC_Numero,                                                       
ISNULL(ENMC.Numero_Documento,0) AS ENMC_Numero_Documento,                                                                                    
DESV.ID,                                                                           
DESV.Fecha_Reporte,                                                                            
DESV.Ubicacion,                                                                                    
DESV.Longitud,                                                                                    
DESV.Latitud,        
DESV.PUCO_Codigo,                                                              
ISNULL(DESV.CATA_TOSV_Codigo, 0) AS CATA_TOSV_Codigo,                                                                                  
DESV.CATA_SRSV_Codigo,                                                      
DESV.CATA_NOSV_Codigo,                                               
DESV.Observaciones,                                                                                    
DESV.Kilometros_Vehiculo,                            
DESV.Reportar_Cliente,                                                          
ISNULL(DESV.Prioritario,0) AS Prioritario,                                            
DESV.Envio_Reporte_Cliente,                                                                                    
DESV.Fecha_Reporte_Cliente,                                                                      
DESV.Anulado,                                                                                                 
ENPD.TERC_Codigo_Conductor,                  
ENPD.TERC_Codigo_Tenedor,                                                                                     
ENPD.VEHI_Codigo,                                                                                    
ISNULL(ENPD.SEMI_Codigo, 0) AS SEMI_Codigo,                                         
ENPD.RUTA_Codigo,                                                                                                         
ISNULL(DESV.Fecha_Crea,'')AS Fecha_Ultimo_Reporte,                                                                                          
ISNULL(COND.Razon_Social, '') + ISNULL(COND.Nombre, '') + ' ' + ISNULL(COND.Apellido1, '') + ' ' + ISNULL(COND.Apellido2, '') + ' (' + ISNULL(COND.Celulares, '') + ')' AS Nombre_Conductor,                                                                   
    
ISNULL(TENE.Razon_Social, '') + ISNULL(TENE.Nombre, '') + ' ' + ISNULL(TENE.Apellido1,'') + ' ' + ISNULL(TENE.Apellido2, '')  AS Nombre_Tenedor,                                                               
CASE WHEN ISNULL(VEHI.TERC_Codigo_Proveedor_GPS, 0) > 0 THEN                                                          
ISNULL(PGPS.Razon_Social, '') + ISNULL(PGPS.Nombre, '') + ' '+ ISNULL(PGPS.Apellido1,'') + ' ' + ISNULL(PGPS.Apellido2, '')                                                          
ELSE '' END AS Nombre_Proveedor_GPS,                                                                 
ISNULL(VEHI.Placa, '') AS Vehiculo,                                                                                      
ISNULL(SEMI.Placa, '') AS Semirremolque,                                                                        
ISNULL(RUTA.Nombre, '') AS Ruta,                                                                       
' ' AS Puesto_Control, -- Se elimino del Filtro                                                                               
ISNULL(TOSV.Campo1, ' ') AS Tipo_Origen_Seguimiento,                                                            
ISNULL(SRSV.Campo1, ' ') AS Sitio_Reporte_Seguimiento,                                                                                    
ISNULL(NOSV.Campo1, ' ') AS Novedad_Seguimiento,                                                                                        
DESV.Orden,                                                                          
ENPD.Numero_Documento AS Numero_Documento_Planilla,                                                                         
0 AS Numero_Documento_Orden,             
ENPD.Ultimo_Seguimiento_Vehicular,    
CIOR.Nombre AS CiudadOrigen,                                          
CIDE.Nombre AS CiudadDestino,                                          
TIDV.Campo1 AS TipoDueno,                                          
CLIE.Codigo AS CodigoCliente,                                          
ISNULL(CLIE.Razon_Social, '') + ISNULL(CLIE.Nombre, '') + ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '')  AS Cliente,                                             
PRTR.Nombre as Producto,                                          
USUA.Nombre AS UsuarioCrea,                             
COND.Emails AS CorreoConductor,                    
ENPD.ELGC_Numero,    
ENPD.OFIC_Codigo AS ENPD_OFIC_Codigo,    
DESV.TERC_Cliente,    
VEHI.CATA_TIDV_Codigo,    
RUTA.CIUD_Codigo_Destino,    
DESV.PRTR_Codigo,    
ENPD.Fecha_Hora_Salida    
FROM                        
Detalle_Seguimiento_Vehiculos DESV WITH (NOLOCK)                                                                        
                                                                                  
INNER JOIN Encabezado_Planilla_Despachos AS ENPD WITH (NOLOCK)                    
ON DESV.EMPR_Codigo = ENPD.EMPR_Codigo                                                                                    
AND DESV.ENPD_Numero = ENPD.Numero                                                           
                                          
LEFT JOIN Encabezado_Manifiesto_Carga AS ENMC WITH (NOLOCK)                                                                                   
ON ENPD.EMPR_Codigo = ENMC.EMPR_Codigo                                                                                    
AND ENPD.Numero = ENMC.ENPD_Numero              
   
-- Tipo Origen Seguimiento Vehicular                                                                                               
LEFT JOIN Valor_Catalogos AS TOSV WITH (NOLOCK)                                                                                    
ON DESV.EMPR_Codigo = TOSV.EMPR_Codigo                                                                                
AND DESV.CATA_TOSV_Codigo  = TOSV.Codigo                                                                                    
  
-- Sitio Reporte Seguimiento Vehicular                                                                               
LEFT JOIN Valor_Catalogos AS SRSV WITH (NOLOCK)                                                      
ON DESV.EMPR_Codigo = SRSV.EMPR_Codigo                                                                                    
AND DESV.CATA_SRSV_Codigo  = SRSV.Codigo    
                                                                                  
-- Novedad Seguimiento Vehicular                                                    
LEFT JOIN Valor_Catalogos AS NOSV WITH (NOLOCK)                                                                                    
ON DESV.EMPR_Codigo = NOSV.EMPR_Codigo                                                                                    
AND DESV.CATA_NOSV_Codigo  = NOSV.Codigo                                                                         
  
-- JCB: (04-JUN-2020) "Se elimina el filtro"                                                                
--LEFT JOIN Puesto_Controles AS PUCO WITH (NOLOCK)                               
--ON DESV.EMPR_Codigo = PUCO.EMPR_Codigo                                                                  
--AND DESV.PUCO_Codigo = PUCO.Codigo                                                                                  
                   
LEFT JOIN Terceros AS COND WITH (NOLOCK)                                                                 
ON (ENPD.EMPR_Codigo = COND.EMPR_Codigo                                                                                   
AND ENPD.TERC_Codigo_Conductor  = COND.Codigo)                                                                                   
                                                                                      
LEFT JOIN Terceros AS TENE WITH (NOLOCK)                                                                                    
ON (ENPD.EMPR_Codigo = TENE.EMPR_Codigo                                                                                    
AND ENPD.TERC_Codigo_Tenedor  = TENE.Codigo)                                                                                    
                                                          
LEFT JOIN Vehiculos AS VEHI WITH (NOLOCK)                                                                                    
ON (ENPD.EMPR_Codigo = VEHI.EMPR_Codigo                                                                                    
AND ENPD.VEHI_Codigo  = VEHI.Codigo)                                                       
  
-- Proveedor GPS                                        
LEFT JOIN Terceros AS PGPS WITH (NOLOCK)                                                                                
ON VEHI.EMPR_Codigo = PGPS.EMPR_Codigo                                                                                
AND VEHI.TERC_Codigo_Proveedor_GPS  = PGPS.Codigo                                                             
                                                                                   
LEFT JOIN Semirremolques AS SEMI WITH (NOLOCK)                                                                                    
ON (ENPD.EMPR_Codigo = SEMI.EMPR_Codigo                                                                                    
AND ENPD.SEMI_Codigo  = SEMI.Codigo)                                                                           
                                                                         
LEFT JOIN Rutas AS RUTA WITH (NOLOCK)                                                                                    
ON (ENPD.EMPR_Codigo = RUTA.EMPR_Codigo                                                                                    
AND ENPD.RUTA_Codigo  = RUTA.Codigo)                                                   
                                                                            
LEFT JOIN Producto_Transportados as PRTR WITH (NOLOCK)                                          
ON DESV.EMPR_Codigo = PRTR.EMPR_Codigo                                                                                    
AND DESV.PRTR_Codigo = PRTR.Codigo                                            
                                          
LEFT JOIN Ciudades AS CIOR WITH (NOLOCK)                                           
ON  (RUTA.EMPR_Codigo = CIOR.EMPR_Codigo                                                                                    
AND RUTA.CIUD_Codigo_Origen  = CIOR.Codigo)                                           
                               
LEFT JOIN Ciudades AS CIDE WITH (NOLOCK)                                          
ON  (RUTA.EMPR_Codigo = CIDE.EMPR_Codigo                                                                                    
AND RUTA.CIUD_Codigo_Destino  = CIDE.Codigo)                                               
                                          
LEFT JOIN Terceros AS CLIE  WITH (NOLOCK)            
ON DESV.EMPR_Codigo = CLIE.EMPR_Codigo                                                                                    
AND DESV.TERC_Cliente = CLIE.Codigo     
  
-- Tipo Dueño Vehiculo                                       
LEFT JOIN Valor_Catalogos AS TIDV WITH (NOLOCK)                                                                                    
ON VEHI.EMPR_Codigo = TIDV.EMPR_Codigo                                                                                    
AND VEHI.CATA_TIDV_Codigo  = TIDV.Codigo                                             
                                          
LEFT JOIN Usuarios AS USUA WITH (NOLOCK)                                                                                    
ON (DESV.EMPR_Codigo = USUA.EMPR_Codigo                                                                                    
AND DESV.USUA_Codigo_Crea  = USUA.Codigo)     
    
WHERE ISNULL(DESV.Fin_Viaje, 0) = 0    
/* JCB: (03-JUN-2020) Las planillas anuladas se sacan de Seguimiento ?*/  
AND ENPD.Anulado = 0   
/* JCB: (03-JUN-2020) Las planillas cumplidas se sacan de Seguimiento ?*/   
AND ISNULL(ENPD.ECPD_Numero,0) = 0  
/* SC: (23-JUN-2020) Unicamente se exhiben las Planilla Despacho Masivo */
AND ENPD.TIDO_Codigo = 150 
  
  