﻿
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources
Imports EncoExpres.GesCarga.Entidades.Paqueteria
Imports EncoExpres.GesCarga.Fachada.Paqueteria
Imports EncoExpres.GesCarga.Entidades.Despachos.Remesa
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Gesphone
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios

Namespace Paqueteria
    Public NotInheritable Class LogicaRemesaPaqueteria
        Inherits LogicaBase(Of RemesaPaqueteria)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of RemesaPaqueteria)

        Sub New(persistencia As IPersistenciaBase(Of RemesaPaqueteria))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))(consulta)
        End Function

        Public Function ConsultarGuiasPlanillaEntrega(filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))

            Dim consulta = CType(_persistencia, PersistenciaRemesaPaqueteria).ConsultarGuiasPlanillaEntrega(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))(consulta)
        End Function


        Public Function ConsultarGuiasEtiquetasPreimpresas(filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of DetalleGuiaPreimpresa))

            Dim consulta = CType(_persistencia, PersistenciaRemesaPaqueteria).ConsultarGuiasEtiquetasPreimpresas(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of DetalleGuiaPreimpresa))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of DetalleGuiaPreimpresa))(consulta)
        End Function
        Public Overrides Function Guardar(entidad As RemesaPaqueteria) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long = 0

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If Not IsNothing(entidad.parametrosCalculos) And entidad.Remesa.Numero = 0 Then
                validarDatos = ValidarPesoVolumetrico(entidad)
                If validarDatos.ProcesoExitoso Then
                    entidad = validarDatos.Datos
                    validarDatos = CalcularValores(entidad)
                    If validarDatos.ProcesoExitoso Then
                        entidad = validarDatos.Datos
                    End If
                End If
            End If

            If entidad.Remesa.Numero = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then

                If Len(Trim(entidad.MensajeSQL)) > 0 Then
                    Return New Respuesta(Of Long)(entidad.MensajeSQL)
                Else
                    Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
                End If

            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .Numero = entidad.Remesa.Numero, .MensajeOperacion = IIf(entidad.Remesa.Numero > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As RemesaPaqueteria) As Respuesta(Of RemesaPaqueteria)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of RemesaPaqueteria)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of RemesaPaqueteria)(consulta)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="entidad"></param>
        ''' <returns></returns>
        Private Function DatosRequeridos(entidad As RemesaPaqueteria) As Respuesta(Of RemesaPaqueteria)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of RemesaPaqueteria) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of RemesaPaqueteria) With {.ProcesoExitoso = True}

        End Function
        Public Function Anular(entidad As RemesaPaqueteria) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaRemesaPaqueteria).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Remesa.Numero, Recursos.OperacionAnulo)}
        End Function
        Public Function CumplirGuias(entidad As RemesaPaqueteria) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim Cumplio As Boolean = False

            Cumplio = CType(_persistencia, PersistenciaRemesaPaqueteria).CumplirGuias(entidad)

            If Not Cumplio Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionCumplidoGuias)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad, Recursos.OperacionCumplido)}
        End Function
        Public Function GenerarPlanitilla(entidad As RemesaPaqueteria) As Respuesta(Of RemesaPaqueteria)
            Dim mensajeGuardo = "Genero Archivo"
            Dim url As New RemesaPaqueteria

            url = CType(_persistencia, PersistenciaRemesaPaqueteria).GenerarPlanitilla(entidad)

            Return New Respuesta(Of RemesaPaqueteria)(url)
        End Function
        Public Function GuardarEntrega(entidad As Remesas) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim Numero As Long = 0

            Numero = CType(_persistencia, PersistenciaRemesaPaqueteria).GuardarEntrega(entidad)

            Return New Respuesta(Of Long)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad, Recursos.OperacionModifico), .Datos = Numero}
        End Function
        Public Function GuardarBitacoraGuias(entidad As DetalleEstadosRemesa) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim Numero As Long = 0

            Numero = CType(_persistencia, PersistenciaRemesaPaqueteria).GuardarBitacoraGuias(entidad)

            Return New Respuesta(Of Long)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad, Recursos.OperacionModifico), .Datos = Numero}
        End Function
        Public Function InsertarIntentoEntrega(entidad As DetalleIntentoEntregaGuia) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim Numero As Long = 0

            Numero = CType(_persistencia, PersistenciaRemesaPaqueteria).InsertarIntentoEntrega(entidad)

            If Not Numero.Equals(0) Then
                Dim estado As New DetalleEstadosRemesa
                With estado
                    .CodigoEmpresa = entidad.CodigoEmpresa
                    .Numero = entidad.NumeroRemesa
                    .UsuarioCrea = New Usuarios With {.Codigo = entidad.CodigoUsuarioCrea}
                    .Observaciones = "Intento de Entrega: " & entidad.NovedadIntentoEntrega & " - " & entidad.Observaciones
                End With
                Dim guardabit = GuardarBitacoraGuias(estado)

                If guardabit.Datos.Equals(0) Then
                    Return New Respuesta(Of Long) With {.Datos = 0, .ProcesoExitoso = False}
                Else
                    Return New Respuesta(Of Long)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad, Recursos.OperacionModifico), .Datos = Numero}
                End If
            Else
                Return New Respuesta(Of Long) With {.Datos = 0, .ProcesoExitoso = False}
            End If
        End Function

        Public Function ConsultarIndicadores(entidad As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
            Dim Consulta = CType(_persistencia, PersistenciaRemesaPaqueteria).ConsultarIndicadores(entidad)

            If IsNothing(Consulta) Then
                Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))(Consulta)

        End Function
        Public Function AsociarGuiaPlanilla(entidad As RemesaPaqueteria) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaRemesaPaqueteria).AsociarGuiaPlanilla(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, Recursos.OperacionAnulo)}
        End Function
        Public Function ConsultarEstadosRemesa(entidad As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of DetalleEstadosRemesa))
            Dim Consulta = CType(_persistencia, PersistenciaRemesaPaqueteria).ConsultarEstadosRemesa(entidad)

            If IsNothing(Consulta) Then
                Return New Respuesta(Of IEnumerable(Of DetalleEstadosRemesa))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of DetalleEstadosRemesa))(Consulta)

        End Function
        Public Function ConsultarBitacoraGuias(entidad As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of DetalleEstadosRemesa))
            Dim Consulta = CType(_persistencia, PersistenciaRemesaPaqueteria).ConsultarBitacoraGuias(entidad)

            If IsNothing(Consulta) Then
                Return New Respuesta(Of IEnumerable(Of DetalleEstadosRemesa))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of DetalleEstadosRemesa))(Consulta)

        End Function

        Public Function ObtenerControlEntregas(entidad As RemesaPaqueteria) As Respuesta(Of RemesaPaqueteria)
            Dim Consulta = CType(_persistencia, PersistenciaRemesaPaqueteria).ObtenerControlEntregas(entidad)
            If IsNothing(Consulta) Then
                Return New Respuesta(Of RemesaPaqueteria)() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of RemesaPaqueteria)(Consulta)
        End Function

        Public Function ConsultarRemesasPendientesControlEntregas(entidad As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
            Dim Consulta = CType(_persistencia, PersistenciaRemesaPaqueteria).ConsultarRemesasPendientesControlEntregas(entidad)

            If IsNothing(Consulta) Then
                Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))(Consulta)

        End Function

        Public Function ConsultarRemesasPorPlanillar(filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
            Dim Consulta = CType(_persistencia, PersistenciaRemesaPaqueteria).ConsultarRemesasPorPlanillar(filtro)

            If IsNothing(Consulta) Then
                Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))(Consulta)

        End Function
        Public Function ConsultarRemesasRecogerOficina(filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
            Dim Consulta = CType(_persistencia, PersistenciaRemesaPaqueteria).ConsultarRemesasRecogerOficina(filtro)

            If IsNothing(Consulta) Then
                Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))(Consulta)

        End Function

        Public Function ConsultarRemesasRetornoContado(filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
            Dim Consulta = CType(_persistencia, PersistenciaRemesaPaqueteria).ConsultarRemesasRetornoContado(filtro)

            If IsNothing(Consulta) Then
                Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))(Consulta)

        End Function

        Public Function ValidarPreimpreso(entidad As RemesaPaqueteria) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            valor = CType(_persistencia, PersistenciaRemesaPaqueteria).ValidarPreimpreso(entidad)

            If valor = 0 Then
                Return New Respuesta(Of Long)(Recursos.NoSeEncontroRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True}
        End Function

        Public Function CambiarEstadoRemesasPaqueteria(entidad As RemesaPaqueteria) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            valor = CType(_persistencia, PersistenciaRemesaPaqueteria).CambiarEstadoRemesasPaqueteria(entidad)

            If valor = 0 Then
                Return New Respuesta(Of Long)(Recursos.NoSeEncontroRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True}
        End Function
        Public Function CambiarEstadoGuiaPaqueteria(entidad As RemesaPaqueteria) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            valor = CType(_persistencia, PersistenciaRemesaPaqueteria).CambiarEstadoGuiaPaqueteria(entidad)

            If valor = 0 Then
                Return New Respuesta(Of Long)(Recursos.NoSeEncontroRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True}
        End Function
        Public Function CambiarEstadoTrazabilidadGuia(entidad As RemesaPaqueteria) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            valor = CType(_persistencia, PersistenciaRemesaPaqueteria).CambiarEstadoTrazabilidadGuia(entidad)

            If valor = 0 Then
                Return New Respuesta(Of Long)(Recursos.NoSeEncontroRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True}
        End Function

        Public Function ConsultarRemesasPendientesRecibirOficina(filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
            Dim consulta = CType(_persistencia, PersistenciaRemesaPaqueteria).ConsultarRemesasPendientesRecibirOficina(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))(consulta)
        End Function

        Public Function RecibirRemesaPaqueteriaOficinaDestino(entidad As RemesaPaqueteria) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            valor = CType(_persistencia, PersistenciaRemesaPaqueteria).RecibirRemesaPaqueteriaOficinaDestino(entidad)

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True}
        End Function

        Public Function ConsultarRemesasPorLegalizar(filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
            Dim Consulta = CType(_persistencia, PersistenciaRemesaPaqueteria).ConsultarRemesasPorLegalizar(filtro)

            If IsNothing(Consulta) Then
                Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))(Consulta)

        End Function

        Public Function ConsultarRemesasPorLegalizarRecaudo(filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
            Dim Consulta = CType(_persistencia, PersistenciaRemesaPaqueteria).ConsultarRemesasPorLegalizarRecaudo(filtro)

            If IsNothing(Consulta) Then
                Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))(Consulta)

        End Function

        Public Function ConsultarDetalleUnidadesDescargue(filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of DetalleUnidades))
            Dim Consulta = CType(_persistencia, PersistenciaRemesaPaqueteria).ConsultarDetalleUnidadesDescargue(filtro)

            If IsNothing(Consulta) Then
                Return New Respuesta(Of IEnumerable(Of DetalleUnidades))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of DetalleUnidades))(Consulta) With {.ProcesoExitoso = True}

        End Function

        Public Function InsertarEstadosRemesasDescargas(filtro As RemesaPaqueteria) As Boolean
            Dim Consulta = CType(_persistencia, PersistenciaRemesaPaqueteria).InsertarEstadosRemesasDescargas(filtro)

            Return Consulta

        End Function


        Public Function ActualizarSecuencia(filtro As RemesaPaqueteria) As Respuesta(Of RemesaPaqueteria)
            Dim consulta = CType(_persistencia, PersistenciaRemesaPaqueteria).ActualizarSecuencia(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of RemesaPaqueteria)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of RemesaPaqueteria)(consulta)
        End Function
        Public Function ConsultarFoto(filtro As FotoDetalleDistribucionRemesas) As Respuesta(Of IEnumerable(Of FotoDetalleDistribucionRemesas))
            Dim Consulta = CType(_persistencia, PersistenciaRemesaPaqueteria).ConsultarFoto(filtro)

            If IsNothing(Consulta) Then
                Return New Respuesta(Of IEnumerable(Of FotoDetalleDistribucionRemesas))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of FotoDetalleDistribucionRemesas))(Consulta) With {.ProcesoExitoso = True}

        End Function

        Public Function ValidarPesoVolumetrico(entidad As RemesaPaqueteria) As Respuesta(Of RemesaPaqueteria)
            Dim strError As String = String.Empty

            If IsNothing(entidad) Then
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
                Return New Respuesta(Of RemesaPaqueteria)(entidad) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            entidad.PesoVolumetrico = ((entidad.Alto * entidad.Ancho * entidad.Largo) / 1000000) * 400
            entidad.PesoVolumetrico = Math.Round(entidad.PesoVolumetrico, 0)

            Return New Respuesta(Of RemesaPaqueteria)(entidad) With {.ProcesoExitoso = True}

        End Function

        Public Function CalcularValores(entidad As RemesaPaqueteria) As Respuesta(Of RemesaPaqueteria)
            Dim strError As String = String.Empty

            If IsNothing(entidad) Then
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
                Return New Respuesta(Of RemesaPaqueteria)(entidad) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If
            entidad.Remesa.PesoCliente = Math.Round(entidad.Remesa.PesoCliente, 0)
            entidad.PesoCobrar = Math.Round(entidad.PesoCobrar, 0)

            entidad.Remesa.ValorFleteCliente = 0

            entidad.parametrosCalculos.calculo = False
            If entidad.CodigoTarifaTransporte = 302 Then 'TARIFA_PRODUCTO_PAQUETERIA
                For Each item In entidad.ListaTipoTarifaCargaVenta
                    If entidad.CodigoTarifaTransporte = item.CodigoTarifa And item.Codigo = entidad.Remesa.ProductoTransportado.Codigo Then
                        If entidad.parametrosCalculos.ManejoDetalleUnidades = False Then
                            entidad.Remesa.ValorFleteCliente = Math.Round(entidad.Remesa.CantidadCliente * item.ValorFlete, 2)
                        Else
                            For Each valor In entidad.parametrosCalculos.ListadoDetalleUnidadesValorFlete
                                entidad.Remesa.ValorFleteCliente += valor
                            Next
                        End If
                        entidad.detalleTarifa = item
                        If entidad.parametrosCalculos.ManejoDetalleUnidades = False Then
                            If entidad.Remesa.ProductoTransportado.Peso > 0 Then
                                entidad.Remesa.PesoCliente = entidad.Remesa.ProductoTransportado.Peso * entidad.Remesa.CantidadCliente
                            End If
                        End If
                        entidad.parametrosCalculos.calculo = True
                        Exit For
                    End If
                Next
            End If
            Dim Descuento As Double
            If entidad.PesoCobrar > 0 Then
                If entidad.CodigoTarifaTransporte = 200 Then 'TARIFA_RANGO_PESO_VALOR_KILO
                    If (entidad.PesoVolumetrico > entidad.Remesa.PesoCliente) Then
                        entidad.PesoCobrar = entidad.PesoVolumetrico
                    Else
                        entidad.PesoCobrar = entidad.Remesa.PesoCliente
                    End If
                    For Each item In entidad.ListaTipoTarifaCargaVenta
                        If entidad.CodigoTarifaTransporte = item.CodigoTarifa AndAlso
                            entidad.PesoCobrar >= item.PesoMinimo AndAlso
                            entidad.PesoCobrar <= item.PesoMaximo Then
                            If item.CondicionesComerciales = True AndAlso 'FP 2022 01 05 Aplica descuento peso volumen y reajusta peso cobrar de acuerdo
                                entidad.parametrosCalculos.Tarifario.TarifarioBase = 1 Then
                                Descuento = Math.Round(entidad.PesoVolumetrico * (item.PorcentajeVolumen / 100), 0) 'Porcentaje Volumen
                                Dim pesodescontado As Double = Math.Round(entidad.PesoVolumetrico - Descuento, 0) 'Resta el descuento en el peso volumetrico
                                If (pesodescontado > entidad.Remesa.PesoCliente) Then
                                    entidad.PesoCobrar = pesodescontado
                                Else
                                    entidad.PesoCobrar = entidad.Remesa.PesoCliente
                                End If
                            End If
                            entidad.Remesa.ValorFleteCliente = Math.Round((entidad.PesoCobrar * item.ValorFlete), 2)
                            ' FP 2022 01 05 Añade minimo valor de flete paqueteria para calculo valor kilo ED-49 req 017
                            If (entidad.Remesa.ValorFleteCliente < entidad.parametrosCalculos.PaqueteriaFleteMinimo) Then
                                entidad.Remesa.ValorFleteCliente = entidad.parametrosCalculos.PaqueteriaFleteMinimo
                            End If
                            entidad.detalleTarifa = item
                            entidad.parametrosCalculos.calculo = True
                            Exit For
                        End If
                    Next
                End If
                If entidad.CodigoTarifaTransporte = 201 Then 'TARIFA_RANGO_PESO_VALOR_FIJO
                    For Each item In entidad.ListaTipoTarifaCargaVenta
                        If entidad.CodigoTarifaTransporte = item.CodigoTarifa AndAlso
                            entidad.PesoCobrar >= item.PesoMinimo AndAlso
                            entidad.PesoCobrar <= item.PesoMaximo Then
                            entidad.Remesa.ValorFleteCliente = item.ValorFlete
                            entidad.detalleTarifa = item
                            entidad.parametrosCalculos.calculo = True
                            Exit For
                        End If
                    Next
                End If
            End If
            If entidad.parametrosCalculos.calculo = True Then
                entidad.Remesa.ValorFleteTransportador = 0
                entidad.Remesa.TotalFleteCliente = Math.Round(entidad.Remesa.ValorFleteCliente, 0)
                entidad.FletePactado = entidad.Remesa.ValorFleteCliente
                'Condiciones Comerciales
                If entidad.detalleTarifa.CondicionesComerciales = True AndAlso
                    entidad.parametrosCalculos.Tarifario.TarifarioBase = 1 Then
                    If entidad.detalleTarifa.Reexpedicion = False Then 'cuando es de reexpedicion no aplica descuentos
                        'Valida Detalle Unidades y Tarifa Producto, para no aplicar condiciones comerciales
                        If entidad.parametrosCalculos.ManejoDetalleUnidades = True Then
                            If Not entidad.CodigoTarifaTransporte = 302 Then 'TARIFA_PRODUCTO_PAQUETERIA
                                Descuento = Math.Round(entidad.Remesa.ValorFleteCliente * (entidad.detalleTarifa.PorcentajeFlete / 100), 0) 'Porcentaje Flete
                                entidad.Remesa.ValorFleteCliente -= Descuento 'Resta el descuento en valor flete
                                If (entidad.Remesa.ValorFleteCliente < entidad.detalleTarifa.FleteMinimo) Then
                                    entidad.Remesa.ValorFleteCliente = entidad.detalleTarifa.FleteMinimo
                                End If
                            End If
                        Else
                            Descuento = Math.Round(entidad.Remesa.ValorFleteCliente * (entidad.detalleTarifa.PorcentajeFlete / 100), 0) 'Porcentaje Flete
                            entidad.Remesa.ValorFleteCliente -= Descuento 'Resta el descuento en valor flete
                            If (entidad.Remesa.ValorFleteCliente < entidad.detalleTarifa.FleteMinimo) Then
                                entidad.Remesa.ValorFleteCliente = entidad.detalleTarifa.FleteMinimo
                            End If
                        End If

                    End If

                End If
                'Condiciones Comerciales
                'Manejo Seccion Reexpedicion Por Oficina
                If entidad.parametrosCalculos.ManejoReexpedicionOficina = True Then
                    entidad.ValorReexpedicion = 0
                    Dim ValorReexpedicionExterna As Double = 0.0
                    For Each item In entidad.ListaReexpedicionOficinas
                        entidad.ValorReexpedicion += item.Valor
                        If item.TipoReexpedicion.Codigo = 11102 Then
                            ValorReexpedicionExterna += item.Valor
                        End If
                    Next
                    If ValorReexpedicionExterna > 0 Then
                        entidad.Remesa.TotalFleteCliente += ValorReexpedicionExterna
                    End If
                Else
                    'Reexpedicion por tarifa
                    If entidad.detalleTarifa.Reexpedicion = True AndAlso entidad.detalleTarifa.PorcentajeReexpedicion > 0 Then
                        Dim TMPValorFleteCliente = entidad.Remesa.ValorFleteCliente
                        entidad.Remesa.ValorFleteCliente = Math.Round(((100 - entidad.detalleTarifa.PorcentajeReexpedicion) / 100) * TMPValorFleteCliente, 0)
                        entidad.ValorReexpedicion = Math.Round((entidad.detalleTarifa.PorcentajeReexpedicion / 100) * TMPValorFleteCliente, 0)
                    End If
                End If
                'Manejo Seccion Reexpedicion Por Oficina
                'Calculos Seguros valor comecial
                If entidad.Remesa.ValorComercialCliente > 0 Then
                    'Realiza Calculos de seguro para valor comercial cuando exise en la tarifa el valor seguro (Prioridad 1)
                    'FP 2021-12-20 Cambio prioridad, prima porcentaje de oficina sobre el de tarifario (ahora prioridad 2)
                    If entidad.detalleTarifa.PorcentajeSeguro > 0 Then
                        entidad.Remesa.ValorSeguroCliente = Math.Round((entidad.Remesa.ValorComercialCliente * entidad.detalleTarifa.PorcentajeSeguro) / 100, 0)
                    End If

                    'Realiza Calculos de seguro para valor comercial cuando exise el valor seguro en oficina y el valor seguro de la tarifa es 0 (Prioridad 2)
                    'If entidad.parametrosCalculos.OficinaObjPorcentajeSeguroPaqueteria > 0 AndAlso entidad.detalleTarifa.PorcentajeSeguro = 0 Then

                    'FP 2021-12-20 Cambio prioridad, prima porcentaje de oficina sobre el de tarifario (Prioridad 1)
                    If entidad.parametrosCalculos.OficinaObjPorcentajeSeguroPaqueteria > 0 Then
                        entidad.Remesa.ValorSeguroCliente = Math.Round((entidad.Remesa.ValorComercialCliente * entidad.parametrosCalculos.OficinaObjPorcentajeSeguroPaqueteria) / 100)
                    End If
                    'Valida Valores Minimos Generales
                    If entidad.LineaNegocioPaqueteria.Codigo = 21601 Then 'CATALOGO_LINEA_NEGOCIO_PAQUETERIA.PAQUETERIA
                        If entidad.Remesa.ValorSeguroCliente < entidad.parametrosCalculos.PaqueteriaMinimoValorManejo Then
                            entidad.Remesa.ValorSeguroCliente = entidad.parametrosCalculos.PaqueteriaMinimoValorManejo
                        End If

                    ElseIf entidad.LineaNegocioPaqueteria.Codigo = 21602 Then ' CATALOGO_LINEA_NEGOCIO_PAQUETERIA.COURIER
                        If entidad.Remesa.ValorSeguroCliente < entidad.parametrosCalculos.PaqueteriaMinimoValorSeguro Then
                            entidad.Remesa.ValorSeguroCliente = entidad.parametrosCalculos.PaqueteriaMinimoValorSeguro
                        End If
                    End If
                    'Valor Manejo Para Condiciones Comerciales
                    If entidad.detalleTarifa.CondicionesComerciales = True AndAlso
                    entidad.parametrosCalculos.Tarifario.TarifarioBase = 1 Then
                        'Valida Detalle Unidades y Tarifa Producto, para no aplicar condiciones comerciales
                        If entidad.parametrosCalculos.ManejoDetalleUnidades = True Then
                            If Not entidad.CodigoTarifaTransporte = 302 Then 'TARIFA_PRODUCTO_PAQUETERIA
                                Dim tmpValorSeguro = Math.Round(entidad.Remesa.ValorComercialCliente * (entidad.detalleTarifa.PorcentajeValorDeclarado / 100), 0)
                                If tmpValorSeguro > entidad.detalleTarifa.ManejoMinimo Then
                                    entidad.Remesa.ValorSeguroCliente = tmpValorSeguro
                                Else
                                    entidad.Remesa.ValorSeguroCliente = entidad.detalleTarifa.ManejoMinimo
                                End If
                            End If
                        Else
                            Dim tmpValorSeguro = Math.Round(entidad.Remesa.ValorComercialCliente * (entidad.detalleTarifa.PorcentajeValorDeclarado / 100), 0)
                            If tmpValorSeguro > entidad.detalleTarifa.ManejoMinimo Then
                                entidad.Remesa.ValorSeguroCliente = tmpValorSeguro
                            Else
                                entidad.Remesa.ValorSeguroCliente = entidad.detalleTarifa.ManejoMinimo
                            End If
                        End If

                    End If
                Else
                    'Valida Valores Minimos Generales
                    If entidad.LineaNegocioPaqueteria.Codigo = 21601 Then 'CATALOGO_LINEA_NEGOCIO_PAQUETERIA.PAQUETERIA
                        entidad.Remesa.ValorSeguroCliente = entidad.parametrosCalculos.PaqueteriaMinimoValorManejo
                    ElseIf entidad.LineaNegocioPaqueteria.Codigo = 21602 Then 'CATALOGO_LINEA_NEGOCIO_PAQUETERIA.COURIER
                        entidad.Remesa.ValorSeguroCliente = entidad.parametrosCalculos.PaqueteriaMinimoValorSeguro
                    End If
                    'Calculos Seguros valor comecial
                End If
                'Suma Conceptos
                entidad.Remesa.TotalFleteCliente = (entidad.Remesa.ValorFleteCliente) + (entidad.Remesa.ValorSeguroCliente) +
                    (entidad.ValorReexpedicion) + (entidad.Remesa.ValorCargue) + (entidad.Remesa.ValorDescargue)

                entidad.Remesa.ValorFleteCliente = Math.Round(entidad.Remesa.ValorFleteCliente, 0)
                entidad.Remesa.ValorComisionOficina = Math.Round(entidad.Remesa.ValorComisionOficina, 0)
                entidad.Remesa.ValorSeguroCliente = Math.Round(entidad.Remesa.ValorSeguroCliente, 0)
                entidad.ValorReexpedicion = Math.Round(entidad.ValorReexpedicion, 0)
                entidad.Remesa.TotalFleteCliente = Math.Round(entidad.Remesa.TotalFleteCliente, 0)
                entidad.Remesa.ValorFleteTransportador = Math.Round(entidad.Remesa.ValorFleteTransportador, 0)
                entidad.FletePactado = Math.Round(entidad.FletePactado, 0)
            End If
            If entidad.Remesa.RemesaCortesia = 1 Then
                entidad.Remesa.ValorFleteCliente = 0
                entidad.Remesa.ValorComisionOficina = 0
                entidad.Remesa.ValorSeguroCliente = 0
                entidad.ValorReexpedicion = 0
                entidad.Remesa.TotalFleteCliente = 0
            End If

            If entidad.parametrosCalculos.calculo = True Or entidad.Remesa.RemesaCortesia = 1 Then
                Return New Respuesta(Of RemesaPaqueteria)(entidad) With {.ProcesoExitoso = True}
            Else
                Return New Respuesta(Of RemesaPaqueteria)(entidad) With {.ProcesoExitoso = False}
            End If

        End Function

    End Class

End Namespace