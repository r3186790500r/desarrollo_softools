﻿EncoExpresApp.controller("GestionarOrdenTrabajoMantenimientoEspecialCtrl", ['$scope', '$routeParams', 'blockUI', '$timeout', '$linq', 'UnidadReferenciaMantenimientoFactory', 'SistemasFactory', 'SubSistemasFactory', 'TercerosFactory', 'EquipoMantenimientoFactory', 'OrdenTrabajoMantenimientoFactory',
    function ($scope, $routeParams, blockUI, $timeout, $linq, UnidadReferenciaMantenimientoFactory, SistemasFactory, SubSistemasFactory, TercerosFactory, EquipoMantenimientoFactory, OrdenTrabajoMantenimientoFactory) {
        $scope.MensajesError = [];
        $scope.MensajesErrorDetalle = [];
        $scope.ListadoEstados = [];
        $scope.ModificarDetalle = 0;
        $scope.Operacion = 'Adicionar';
        $scope.Validarvalortotal = false
        $scope.Validarvaloriva = false
        $scope.ValidarSubtotal = false

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_ORDENES_TRABAJO);

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        // Se crea la propiedad modelo en el ambito
        $scope.MapaSitio = [{ Nombre: 'Mantenimiento' }, { Nombre: 'Documentos' }, { Nombre: 'Ordenes Trabajo' }];
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Numero: 0,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Detalles: []
        };
        $scope.Modelo.Fecha = new Date()
        $scope.Modal = {}
        /* Obtener parametros del enrutador*/
        if ($routeParams.Codigo !== undefined) {
            $scope.Modelo.Numero = $routeParams.Codigo;
        }
        else {
            $scope.Modelo.Numero = 0;
        }
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Metodo para volver a la página de consulta
        $scope.VolverMaster = function () {
            if ($scope.Modelo.Numero > 0) {
                document.location.href = '#!ConsultarOrdenTrabajoMantenimiento/' + $scope.Modelo.Numero;
            } else {
                document.location.href = '#!ConsultarOrdenTrabajoMantenimiento';
            }
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*Cargar el combo de estados*/
        $scope.ListadoEstados = [
            { Codigo: 0, Nombre: 'BORRADOR' },
            { Codigo: 1, Nombre: 'DEFINITIVO' }
        ];
        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 0');
        /*Cargar el combo de Sistema mantenimiento*/
        SistemasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoSistemaMantenimiento = [];
                    response.data.Datos.forEach(function (item) {
                        $scope.ListadoSistemaMantenimiento.push(item);
                    });
                    $timeout(function () {
                        if ($('#tiposistema')[0].options[0].value == "?") {
                            $('#tiposistema')[0].options[0].remove()
                        }
                    }, 50);

                    $timeout(function () {
                        $('#tiposistema').selectpicker()
                    }, 200);
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar el combo de Subsistemas Mantenimiento*/
        SubSistemasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoSubsistemaMantenimiento = [];
                    $scope.ListadoSubsistemaFiltrado = [];
                    $scope.ListadoSubsistemaMantenimiento = response.data.Datos

                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar el combo Unidad Referencia Mantenimiento*/
        UnidadReferenciaMantenimientoFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoUnidadReferenciaMantenimiento = [];
                    $scope.ListadoUnidadReferenciaMantenimiento = response.data.Datos
                    $scope.Modal.UnidadReferenciaMantenimiento = response.data.Datos[0];
                } else {
                    $scope.ListadoUnidadReferenciaMantenimiento = [];
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar el combo de Equipo Mantenimiento*/
        EquipoMantenimientoFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoEquipoMantenimiento = [];
                    $scope.ListadoEquipoMantenimiento = response.data.Datos;
                    $scope.Modelo.Equipo = response.data.Datos[0];
                    $timeout(function () {
                        if ($('#equipo')[0].options[0].value == "?") {
                            $('#equipo')[0].options[0].remove()
                        }
                    }, 50);

                    $timeout(function () {
                        $('#equipo').selectpicker()
                    }, 200);

                }
            }, function (response) {
                ShowError(response.statusText);
            });
        $scope.AutocompleteProveedores = function (value) {
            $scope.ListaProveedores = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, ValorAutocomplete: value, CadenaPerfiles: PERFIL_PROVEEDOR, Sync: true }).Datos;
            return $scope.ListaProveedores;
        }
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        if ($scope.Modelo.Numero > 0) {
            // Consultar los datos del tercero correspondientes al número que llego como parametro del enrutador
            $scope.Titulo = 'CONSULTAR orden de trabajo de mantenimiento';
            Obtener();
        }
        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando orden de trabajo de mantenimiento ' + $scope.Modelo.Numero);

            $timeout(function () {
                blockUI.message('Cargando  orden de trabajo de mantenimiento ' + $scope.Modelo.Numero);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Modelo.Numero,
            };

            blockUI.delay = 1000;
            OrdenTrabajoMantenimientoFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        $scope.Modelo = response.data.Datos
                        $scope.Modelo.Fecha = new Date($scope.Modelo.Fecha)
                        $scope.Modelo.FechaEntrega = new Date($scope.Modelo.FechaEntrega)
                        $scope.Modelo.Equipo = $linq.Enumerable().From($scope.ListadoEquipoMantenimiento).First('$.Codigo ==' + $scope.Modelo.Equipo.Codigo);
                        $scope.Modelo.Proveedor = $scope.CargarTercero($scope.Modelo.Proveedor.Codigo);
                        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + $scope.Modelo.Estado);
                        $scope.Modelo.UsuarioModifica = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }

                        if ($scope.Modelo.Estado.Codigo == 0) {
                            $scope.Deshabilitar = false;
                        }
                        else {
                            $scope.Deshabilitar = true;
                        }

                    }
                    else {
                        ShowError('No se logro consultar la orden de trabajo de mantenimiento No.' + $scope.Modelo.Numero + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarOrdenTrabajoMantenimiento';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    //ShowError('No se logro consultar la orden de trabajo de mantenimiento No.' + $scope.Modelo.Codigo + '. Por favor contacte el administrador del sistema.');
                    document.location.href = '#!ConsultarOrdenTrabajoMantenimiento';
                });

            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.InicializarDetalle = function () {
            showModal('modalMantenimiento');
            $scope.ModificarDetalle = 0
            $scope.Modal.Cantidad = ''
            $scope.Modal.Descripcion = ''
        }
        $scope.ConfirmacionGuardarOrdenTrabajo = function () {
            showModal('modalConfirmacionGuardarOrdenTrabajo');
        };
        // Metodo para guardar /modificar
        $scope.Guardar = function () {
           
            closeModal('modalConfirmacionGuardarOrdenTrabajo');
            if (DatosRequeridos()) {
                $scope.Modelo.Proveedor = { Codigo: $scope.Modelo.Proveedor.Codigo, NombreCompleto: $scope.Modelo.Proveedor.NombreCompleto };
                $scope.Modelo.Estado = $scope.Modelo.Estado.Codigo;
                OrdenTrabajoMantenimientoFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Numero > 0) {
                                    ShowSuccess('Se modificó la orden de trabajo de mantenimiento No.' + $scope.Modelo.Numero);
                                    location.href = '#!ConsultarOrdenTrabajoMantenimiento/' + $scope.Modelo.Numero;
                                } else {
                                    ShowSuccess('Se guardó la orden de trabajo de mantenimiento No.' + response.data.Datos);
                                    location.href = '#!ConsultarOrdenTrabajoMantenimiento/' + response.data.Datos;
                                }
                                
                            }
                            else {
                                ShowError(response.data.MensajeOperacion);
                            }
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };
        // Valida los datos requridos 
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;

            if ($scope.Modelo.Fecha == null || $scope.Modelo.Fecha == undefined) {
                $scope.MensajesError.push('Debe Ingresar la fecha');
                continuar = false;
            }
            if ($scope.Modelo.FechaEntrega == null || $scope.Modelo.FechaEntrega == undefined) {
                $scope.MensajesError.push('Debe Ingresar la fecha de entrega');
                continuar = false;
            }
            if ($scope.Modelo.Equipo == null || $scope.Modelo.Equipo == undefined) {
                $scope.MensajesError.push('Debe Ingresar el equipo');
                continuar = false;
            }
            if ($scope.Modelo.Proveedor == null || $scope.Modelo.Proveedor == undefined) {
                $scope.MensajesError.push('Debe Ingresar el proveedor');
                continuar = false;
            } else if ($scope.Modelo.Proveedor.Codigo == null || $scope.Modelo.Proveedor.Codigo == undefined) {
                $scope.MensajesError.push('Debe Ingresar un proveedor válido');
                continuar = false;
            }
            if ($scope.Modelo.ContactoEntrega == null || $scope.Modelo.ContactoEntrega == undefined) {
                $scope.MensajesError.push('Debe Ingresar el contacto de entrega');
                continuar = false;
            }
            if ($scope.Modelo.Estado == '' || $scope.Modelo.Estado == null || $scope.Modelo.Estado == undefined) {
                $scope.MensajesError.push('Debe ingresar el estado');
                continuar = false;
            }
            if ($scope.Modelo.Detalles.length == 0) {
                $scope.MensajesError.push('Debe Ingresar mínimo un detalle');
                continuar = false;
            }
            return continuar

        }
        /*Asignar Movimiento Al Comprobante*/
        $scope.GuardarDetalleMantenimiento = function () {
            $scope.MensajesErrorDetalle = [];
            var cont = 0
            for (var i = 0; i < $scope.Modelo.Detalles.length; i++) {
                var item = $scope.Modelo.Detalles[i]
                if (item.SubsistemaMantenimiento.Codigo == $scope.Modal.SubsistemaMantenimiento.Codigo && item.SistemaMantenimiento.Codigo == $scope.Modal.SistemaMantenimiento.Codigo) {
                    cont++
                }
            } if (cont > 0) {
                ShowError('Ya hay un registro con el sistema y subsitema seleccionado')
            } else {
                if (DatosRequeridosOrden()) {
                    CargarDetalles()
                }
            }

        }
        function CargarDetalles() {
            closeModal('modalMantenimiento');
            $scope.Modelo.Detalles.push(
                JSON.parse(JSON.stringify($scope.Modal))
            );
        }
        function DatosRequeridosOrden() {
            $scope.MensajesErrorDetalle = [];
            var continuar = true;

            if ($scope.Modal.SistemaMantenimiento == undefined || $scope.Modal.SistemaMantenimiento == "" || $scope.Modal.SistemaMantenimiento == null) {
                $scope.MensajesErrorDetalle.push('Debe ingresar un sistema de mantenimiento');
                continuar = false;
            }
            else if ($scope.Modal.SubsistemaMantenimiento == undefined || $scope.Modal.SubsistemaMantenimiento == "" || $scope.Modal.SubsistemaMantenimiento == null) {
                $scope.MensajesErrorDetalle.push('Debe ingresar un subsistema de mantenimiento');
                continuar = false;
            }

            if ($scope.Modal.UnidadReferenciaMantenimiento == undefined || $scope.Modal.UnidadReferenciaMantenimiento == "" || $scope.Modal.UnidadReferenciaMantenimiento == null) {
                $scope.MensajesErrorDetalle.push('Debe ingresar la unidad referencia mantenimiento');
                continuar = false;
            }
            if ($scope.Modal.Cantidad == undefined || $scope.Modal.Cantidad == "" || $scope.Modal.Cantidad == null) {
                $scope.MensajesErrorDetalle.push('Debe ingresar la cantidad');
                continuar = false;
            }

            return continuar;
        }

        /*Funcion que permite modificar el detalle de la orden de trabajo de mantenimiento*/
        $scope.EditarDetalleOrdenTrabajo = function (itemDetalle) {
            $scope.tempItem = itemDetalle
            $scope.ModificarDetalle = 1 // variable que identifica si es para editar o agregar
            $('#tiposistema').selectpicker('destroy')

            $scope.Modal.SistemaMantenimiento = $linq.Enumerable().From($scope.ListadoSistemaMantenimiento).First('$.Codigo == ' + itemDetalle.SistemaMantenimiento.Codigo);
            $timeout(function () {
                if ($('#tiposistema')[0].options[0].value == "?" || $('#tiposubsistema')[0].options[0].value == "") {
                    $('#tiposistema')[0].options[0].remove()
                }
            }, 50);
            $timeout(function () {
                $('#tiposistema').selectpicker()
            }, 200);
            $scope.FiltrarSubsistema()
            $timeout(function () {
                $('#tiposubsistema').selectpicker('destroy')
                $scope.Modal.SubsistemaMantenimiento = $linq.Enumerable().From($scope.ListadoSubsistemaMantenimiento).First('$.Codigo == ' + itemDetalle.SubsistemaMantenimiento.Codigo);
                $timeout(function () {
                    if ($('#tiposubsistema')[0].options[0].value == "?" || $('#tiposubsistema')[0].options[0].value == "") {
                        $('#tiposubsistema')[0].options[0].remove()
                    }
                }, 50);
                $timeout(function () {
                    $('#tiposubsistema').selectpicker()
                }, 200);
                $scope.Modal.UnidadReferenciaMantenimiento = $linq.Enumerable().From($scope.ListadoUnidadReferenciaMantenimiento).First('$.Codigo == ' + itemDetalle.UnidadReferenciaMantenimiento.Codigo);
                // $scope.KmUso = itemDetalle.KmUso
                $scope.Modal.Cantidad = itemDetalle.Cantidad;
                $scope.Modal.Descripcion = itemDetalle.Descripcion;
                showModal('modalMantenimiento');
            }, 300)
            showModal('modalMantenimiento');

        }
        $scope.ModificarDetalleOrdenTrabajo = function () {
            if (DatosRequeridosOrden()) {
                closeModal('modalMantenimiento');
                $scope.tempItem.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
                $scope.tempItem.SistemaMantenimiento = $scope.Modal.SistemaMantenimiento;
                $scope.tempItem.SubsistemaMantenimiento = $scope.Modal.SubsistemaMantenimiento;
                $scope.tempItem.UnidadReferenciaMantenimiento = $scope.Modal.UnidadReferenciaMantenimiento;
                $scope.tempItem.Descripcion = $scope.Modal.Descripcion;
                $scope.tempItem.Cantidad = $scope.Modal.Cantidad;
            }
        };


        /*Eliminar Detalle tarea mantenimiento*/
        $scope.ConfirmacionEliminarDetalle = function (indice) {
            $scope.MensajeEliminar = { indice };
            showModal('modalEliminarDetalle');
        };

        $scope.FiltrarSubsistema = function () {
            $('#tiposubsistema').selectpicker('destroy')
            $scope.ListadoSubsistemaFiltrado = []
            for (var i = 0; i < $scope.ListadoSubsistemaMantenimiento.length; i++) {
                var item = $scope.ListadoSubsistemaMantenimiento[i]
                if (item.SistemaMantenimiento  != null && $scope.Modal.SistemaMantenimiento != null) {
                    if (item.SistemaMantenimiento.Codigo == $scope.Modal.SistemaMantenimiento.Codigo) {
                        $scope.ListadoSubsistemaFiltrado.push(item)
                    }
                }
            }
            $scope.Modal.SubsistemaMantenimiento = $scope.ListadoSubsistemaFiltrado[0]
            $timeout(function () {
                if ($('#tiposubsistema')[0].options[0].value == "?" || $('#tiposubsistema')[0].options[0].value == "") {
                    $('#tiposubsistema')[0].options[0].remove()
                }
            }, 50);
            $timeout(function () {
                $('#tiposubsistema').selectpicker()
                $scope.AplicarCampos($scope.Modal.SubsistemaMantenimiento.Nombre)
            }, 200);
        }

        $scope.AplicarCampos = function (item) {

        }


        $scope.EliminarDetalle = function (indice) {
            $scope.Modelo.Detalles.splice(indice, 1);
            closeModal('modalEliminarDetalle');
        };

        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskPlaca = function (option) {
            MascaraPlaca(option)
        };
    }]);