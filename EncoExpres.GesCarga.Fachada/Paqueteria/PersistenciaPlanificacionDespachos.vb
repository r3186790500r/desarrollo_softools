﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Paqueteria
Imports EncoExpres.GesCarga.Repositorio.Paqueteria

Namespace Paqueteria
    Public NotInheritable Class PersistenciaPlanificacionDespachos
        Implements IPersistenciaBase(Of PlanificacionDespachos)

        Public Function Consultar(filtro As PlanificacionDespachos) As IEnumerable(Of PlanificacionDespachos) Implements IPersistenciaBase(Of PlanificacionDespachos).Consultar
            Return New RepositorioPlanificacionDespachos().Consultar(filtro)
        End Function

        Public Function ConsultarDetalleRemesasResumen(filtro As PlanificacionDespachos) As IEnumerable(Of PlanificacionDespachos)
            Return New RepositorioPlanificacionDespachos().ConsultarDetalleRemesasResumen(filtro)
        End Function

        Public Function Insertar(entidad As PlanificacionDespachos) As Long Implements IPersistenciaBase(Of PlanificacionDespachos).Insertar
            Return New RepositorioPlanificacionDespachos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As PlanificacionDespachos) As Long Implements IPersistenciaBase(Of PlanificacionDespachos).Modificar
            Return New RepositorioPlanificacionDespachos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As PlanificacionDespachos) As PlanificacionDespachos Implements IPersistenciaBase(Of PlanificacionDespachos).Obtener
            Return New RepositorioPlanificacionDespachos().Obtener(filtro)
        End Function
    End Class
End Namespace
