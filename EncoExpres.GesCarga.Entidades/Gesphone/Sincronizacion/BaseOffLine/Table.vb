﻿Imports Newtonsoft.Json

Namespace Gesphone

    ''' <summary>
    ''' Clase encargada de definir la información de una tabla y la información almacenada.
    ''' </summary>
    Public Class Table

        ''' <summary>
        ''' Obtiene o establece el nombre de la tabla.
        ''' </summary>
        ''' <returns></returns>
        <JsonProperty("name")>
        Public Property Name As String

        ''' <summary>
        ''' Obtiene o establece la lista de campos de la tabla.
        ''' </summary>
        ''' <returns></returns>
        <JsonProperty("schema")>
        Public Property Schema As List(Of Schema)

        ''' <summary>
        ''' Obtiene o establece los valores a insertar en la tabla.
        ''' </summary>
        ''' <returns></returns>
        <JsonProperty("values")>
        Public Property Values As List(Of List(Of Object))


        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Table"/>
        ''' </summary>
        ''' <param name="name"></param>
        Sub New(name As String)
            Me.Name = name
        End Sub

    End Class
End Namespace


