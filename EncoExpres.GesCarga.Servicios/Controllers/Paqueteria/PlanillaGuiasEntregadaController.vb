﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Despachos.Paqueteria
Imports EncoExpres.GesCarga.Negocio.Despachos.Paqueteria

''' <summary>
''' Controlador <see cref="PlanillaGuiasEntregadaController"/>
''' </summary>
<Authorize>
Public Class PlanillaGuiasEntregadaController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As PlanillaEntregadas) As Respuesta(Of IEnumerable(Of PlanillaEntregadas))
        Return New LogicaPlanillaEntregadas(CapaPersistenciaPlanillaEntregadas).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As PlanillaEntregadas) As Respuesta(Of PlanillaEntregadas)
        Return New LogicaPlanillaEntregadas(CapaPersistenciaPlanillaEntregadas).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As PlanillaEntregadas) As Respuesta(Of Long)
        Return New LogicaPlanillaEntregadas(CapaPersistenciaPlanillaEntregadas).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As PlanillaEntregadas) As Respuesta(Of Boolean)
        Return New LogicaPlanillaEntregadas(CapaPersistenciaPlanillaEntregadas).Anular(entidad)
    End Function
    <HttpPost>
    <ActionName("AnularAuxiliar")>
    Public Function AnularAuxiliar(ByVal entidad As PlanillaEntregadas) As Respuesta(Of Boolean)
        Return New LogicaPlanillaEntregadas(CapaPersistenciaPlanillaEntregadas).AnularAuxiliar(entidad)
    End Function
End Class
