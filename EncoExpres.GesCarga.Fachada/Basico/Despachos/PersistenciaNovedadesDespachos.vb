﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Repositorio.Basico.Despachos

Namespace Basico.Despachos
    Public NotInheritable Class PersistenciaNovedadesDespachos
        Implements IPersistenciaBase(Of NovedadesDespachos)

        Public Function Consultar(filtro As NovedadesDespachos) As IEnumerable(Of NovedadesDespachos) Implements IPersistenciaBase(Of NovedadesDespachos).Consultar
            Return New RepositorioNovedadesDespachos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As NovedadesDespachos) As Long Implements IPersistenciaBase(Of NovedadesDespachos).Insertar
            Return New RepositorioNovedadesDespachos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As NovedadesDespachos) As Long Implements IPersistenciaBase(Of NovedadesDespachos).Modificar
            Return New RepositorioNovedadesDespachos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As NovedadesDespachos) As NovedadesDespachos Implements IPersistenciaBase(Of NovedadesDespachos).Obtener
            Return New RepositorioNovedadesDespachos().Obtener(filtro)
        End Function

        Public Function Anular(entidad As NovedadesDespachos) As Boolean
            Return New RepositorioNovedadesDespachos().Anular(entidad)
        End Function
    End Class

End Namespace

