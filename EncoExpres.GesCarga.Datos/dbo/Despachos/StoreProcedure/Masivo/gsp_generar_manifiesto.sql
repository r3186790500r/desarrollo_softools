﻿DROP PROCEDURE gsp_generar_manifiesto
GO
CREATE PROCEDURE gsp_generar_manifiesto                      
(                      
 @par_EMPR_Codigo smallint,                      
 @par_Numero_Planilla int null,                      
 @par_Numero_Documento_Planilla NUMERIC null,                      
 @par_TIDO_Codigo numeric,                      
 @par_USUA_Codigo_Crea smallint,                      
 @par_OFIC_Codigo numeric,                      
 @par_CATA_TIMA_Codigo smallint,                      
 @par_ESOS_Numero int,                      
 @par_ID int = NULL, 
 @par_Aceptacion_Electronica numeric = NULL
)                      
AS                      
BEGIN                      
 DECLARE @NumeroDocumentoManifiesto INT                      
 DECLARE @NumeroDocumentoRemesa INT                      
 DECLARE @TipoRemesa INT                      
 DECLARE @CiudadOrigen INT                      
 DECLARE @CiudadDestino INT                      
 DECLARE @NumeroRemesa INT                      
 DECLARE @NumeroManifiesto INT                      
                       
          
  if (@par_Numero_Planilla is null OR @par_Numero_Planilla = 0) and @par_Numero_Documento_Planilla> 0                    
  begin                    
  select @par_Numero_Planilla= Numero  from Encabezado_Planilla_Despachos where EMPR_Codigo = @par_EMPR_Codigo and Numero_Documento = @par_Numero_Documento_Planilla AND TIDO_Codigo = @par_TIDO_Codigo            
  end                    
                    
 --Se consulta el número interno del manifiesto                      
 SET @NumeroManifiesto = (                      
 SELECT                      
  ISNULL(ENMC_Numero,0)                      
 FROM                      
  Encabezado_Planilla_Despachos                      
 WHERE                      
  EMPR_Codigo = @par_EMPR_Codigo                      
  AND Numero = @par_Numero_Planilla                      
  AND TIDO_Codigo = @par_TIDO_Codigo)                      
 IF @NumeroManifiesto = 0  
 BEGIN                      
  
  EXEC gsp_generar_consecutivo @par_EMPR_Codigo, 140, @par_OFIC_Codigo, @NumeroDocumentoManifiesto OUTPUT                      
  INSERT INTO Encabezado_Manifiesto_Carga                      
  (                     
  --0               
  EMPR_Codigo                      
  ,TIDO_Codigo                      
  ,Numero_Documento                      
  ,Fecha                      
  ,VEHI_Codigo                      
  --5              
  ,TERC_Codigo_Propietario                      
  ,TERC_Codigo_Tenedor                      
  ,TERC_Codigo_Conductor                      
  ,TERC_Codigo_Segundo_Conductor                      
  ,TERC_Codigo_Afiliador                      
  --10              
  ,SEMI_Codigo                      
  ,TIVE_Codigo                      
  ,RUTA_Codigo                      
  ,Observaciones                      
  ,Fecha_Cumplimiento                      
  --15              
  ,Cantidad_Total                      
  ,Peso_Total                      
  ,Valor_Flete                      
  ,Valor_Retencion_Fuente                      
  ,Valor_ICA                      
  --20              
  ,Valor_Otros_Descuentos                      
  ,Valor_Flete_Neto        --2              
  ,Valor_Anticipo                      
  ,Valor_Pagar        --4              
  ,TERC_Codigo_Aseguradora                      
  --25              
  ,Numero_Poliza                      
  ,Fecha_Vigencia_Poliza                      
  ,ELPD_Numero                      
  ,ECPD_Numero                      
  ,Finalizo_Viaje                      
  --30              
  ,Anulado                      
  ,Siniestro                      
  ,Estado                      
  ,Fecha_Crea                      
  ,USUA_Codigo_Crea                      
  --35              
  ,OFIC_Codigo                      
  ,CATA_TIMA_Codigo                      
  ,Numero_Manifiesto_Electronico                      
  ,ENPD_Numero    
  ,Aceptacion_Electronica
  --40                    
  )                      
  SELECT                      
  --0              
  @par_EMPR_Codigo,                      
  140,--Manifiesto Carga           
  @NumeroDocumentoManifiesto,                      
  ENPD.Fecha,                      
  ENPD.VEHI_Codigo,                      
  --5              
  VEHI.TERC_Codigo_Propietario,                      
  ENPD.TERC_Codigo_Tenedor,                      
  ENPD.TERC_Codigo_Conductor,                      
  0,--TERC_Codigo_Afiliador,                      
  VEHI.TERC_Codigo_Afiliador,                      
  --10              
  ENPD.SEMI_Codigo,                      
  VEHI.CATA_TIVE_Codigo,                      
  ENPD.RUTA_Codigo,                      
  ENPD.Observaciones,                      
  DATEADD(DAY,4,ENPD.Fecha),                      
  --15              
  ENPD.Cantidad,              
  ENPD.Peso,                      
  ENPD.Valor_Flete_Transportador,                      
  ISNULL((select Valor_Impuesto from Detalle_Impuestos_Planilla_Despachos where EMPR_Codigo = @par_EMPR_Codigo and ENPD_Numero = @par_Numero_Planilla and ENIM_Codigo = 20),0) ,--Valor_Retencion_Fuente,                      
  ISNULL((select Valor_Impuesto from Detalle_Impuestos_Planilla_Despachos where EMPR_Codigo = @par_EMPR_Codigo and ENPD_Numero = @par_Numero_Planilla and ENIM_Codigo = 21),0),--Valor_ICA,             --20              
  0,--Valor_Otros_Descuentos,                      
  (ENPD.Valor_Flete_Transportador - (ISNULL((select Valor_Impuesto from Detalle_Impuestos_Planilla_Despachos where EMPR_Codigo = @par_EMPR_Codigo and ENPD_Numero = @par_Numero_Planilla and ENIM_Codigo = 20),0) +     
  ISNULL((select Valor_Impuesto from Detalle_Impuestos_Planilla_Despachos where EMPR_Codigo = @par_EMPR_Codigo and ENPD_Numero = @par_Numero_Planilla and ENIM_Codigo = 21),0))),  --Valor_Flete_Neto                    
                 
  ENPD.Valor_Anticipo,                       
              
  ((ENPD.Valor_Flete_Transportador - (ISNULL((select Valor_Impuesto from Detalle_Impuestos_Planilla_Despachos where EMPR_Codigo = @par_EMPR_Codigo and ENPD_Numero = @par_Numero_Planilla and ENIM_Codigo = 20),0) +     
  ISNULL((select Valor_Impuesto from Detalle_Impuestos_Planilla_Despachos where EMPR_Codigo = @par_EMPR_Codigo and ENPD_Numero = @par_Numero_Planilla and ENIM_Codigo = 21),0))) - ENPD.Valor_Anticipo ) ,    --Valor_Pagar                    
                  
  0,--TERC_Codigo_Aseguradora                      
  --25              
  0,--Numero_Poliza                      
  0,--Fecha_Vigencia_Poliza                      
  0,--ELPD_Numero,                      
  0,--ECPD_Numero,                      
  0,--Finalizo_Viaje,                      
  --30              
  0,--Anulado                      
  0,--Siniestro,                      
  CASE WHEN (SELECT COUNT(*) FROM Encabezado_Autorizaciones WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENPD_Numero = @par_Numero_Planilla AND CATA_ESAU_Codigo = 18301) > 0 THEN 0 ELSE 1 END,--Definitivo,                      
  GETDATE(),                      
  @par_USUA_Codigo_Crea,                      
  --35              
  @par_OFIC_Codigo,                      
  @par_CATA_TIMA_Codigo,                      
  0    ,                  
  @par_Numero_Planilla ,
  @par_Aceptacion_Electronica
   --40          
  FROM                      
   Encabezado_Planilla_Despachos ENPD,                      
   Vehiculos VEHI                      
  WHERE                      
   ENPD.EMPR_Codigo = VEHI.EMPR_Codigo                      
   AND ENPD.VEHI_Codigo = VEHI.Codigo                      
                      
   AND ENPD.EMPR_Codigo = @par_EMPR_Codigo                      
   AND ENPD.Numero = @par_Numero_Planilla                     
                      
  ----------------------------------------------------------------                      
  -----------------INSERTAR DETALLE DEL MANIFIESTO----------------                      
  ----------------------------------------------------------------      
  SET @NumeroManifiesto = (                      
  SELECT                      
   Numero                      
  FROM                      
   Encabezado_Manifiesto_Carga                      
  WHERE                      
   EMPR_Codigo = @par_EMPR_Codigo                      
   AND Numero_Documento = @NumeroDocumentoManifiesto)                      
                      
  IF @par_CATA_TIMA_Codigo = 8806 --Manifiesto paqueteria generación remesa total  
  BEGIN  
  declare @ConsecutivoRemesaManifiestoPaqueteria numeric  
  declare @NumeroRemesaManifiestoPaqueteria numeric  
  EXEC gsp_generar_consecutivo @par_EMPR_Codigo, 115, @par_OFIC_Codigo, @ConsecutivoRemesaManifiestoPaqueteria OUTPUT      
  INSERT INTO Encabezado_Remesas  
  (  
  EMPR_Codigo,  
  TIDO_Codigo,  
  Numero_Documento,  
  CATA_TIRE_Codigo,  
  Documento_Cliente,  
  Fecha_Documento_Cliente,  
  Fecha,  
  RUTA_Codigo,  
  PRTR_Codigo,  
  CATA_FOPR_Codigo,  
  TERC_Codigo_Cliente,  
  TERC_Codigo_Remitente,  
  CIUD_Codigo_Remitente,  
  Barrio_Remitente,  
  Direccion_Remitente,  
  Codigo_Postal_Remitente,  
  Telefonos_Remitente,  
  Observaciones,  
  Cantidad_Cliente,  
  Peso_Cliente,  
  Peso_Volumetrico_Cliente,  
  DTCV_Codigo,  
  Valor_Flete_Cliente,  
  Valor_Manejo_Cliente,  
  Valor_Seguro_Cliente,  
  Valor_Descuento_Cliente,  
  Total_Flete_Cliente,  
  Valor_Comercial_Cliente,  
  VEHI_Codigo,  
  TERC_Codigo_Conductor,  
  SEMI_Codigo,  
  Cantidad_Transportador,  
  Peso_Transportador,  
  Valor_Flete_Transportador,  
  Total_Flete_Transportador,  
  TERC_Codigo_Destinatario,  
  CIUD_Codigo_Destinatario,  
  Barrio_Destinatario,  
  Direccion_Destinatario,  
  Codigo_Postal_Destinatario,  
  Telefonos_Destinatario,  
  Numero_Contenedor,  
  MBL_Contenedor,  
  HBL_Contenedor,  
  DO_Contenedor,  
  Valor_FOB,  
  SICD_Codigo_Devolucion_Contenedor,  
  Fecha_Devolucion_Contenedor,  
  Distribucion,  
  Cumplido,  
  Anulado,  
  Estado,  
  Fecha_Crea,  
  USUA_Codigo_Crea,  
  Fecha_Modifica,  
  USUA_Codigo_Modifica,  
  Fecha_Anula,  
  USUA_Codigo_Anula,  
  Causa_Anula,  
  OFIC_Codigo,  
  Numeracion,  
  ETCC_Numero,  
  ETCV_Numero,  
  ESOS_Numero,  
  ENOC_Numero,  
  ENPR_Numero,  
  ENPD_Numero,  
  ENPE_Numero,  
  ENMC_Numero,  
  ECPD_Numero,  
  ENFA_Numero,  
  Entregado,  
  Distribuido,  
  TEDI_Codigo,  
  CIUD_Codigo_Devolucion_Contenedor,  
  Patio_Devolcuion,  
  Fecha_Devolcuion  
  )  
  SELECT   
  @par_EMPR_Codigo,--EMPR_Codigo  
  115,--TIDO_Codigo  
  @ConsecutivoRemesaManifiestoPaqueteria,--Numero_Documento  
  8811,--CATA_TIRE_Codigo  
  '',--Documento_Cliente  
  '',--Fecha_Documento_Cliente  
  GETDATE(),--Fecha  
  ENPD.RUTA_Codigo,--RUTA_Codigo  
  1,--PRTR_Codigo  
  4901,--CATA_FOPR_Codigo  
  CLIE.Codigo,--TERC_Codigo_Cliente  
  CLIE.Codigo,--TERC_Codigo_Remitente  
  RUTA.CIUD_Codigo_Origen,--CIUD_Codigo_Remitente  
  '',--Barrio_Remitente  
  CLIE.Direccion,--Direccion_Remitente  
  '',--Codigo_Postal_Remitente  
  '',--Telefonos_Remitente  
  '',--Observaciones  
  (SELECT SUM(Cantidad_Cliente) FROM Encabezado_Remesas WHERE EMPR_Codigo = ENPD.EMPR_Codigo AND ENPD_Numero = ENPD.Numero),--Cantidad_Cliente  
  (SELECT SUM(Peso_Cliente) FROM Encabezado_Remesas WHERE EMPR_Codigo = ENPD.EMPR_Codigo AND ENPD_Numero = ENPD.Numero),--Peso_Cliente  
  0,--Peso_Volumetrico_Cliente  
  0,--DTCV_Codigo  
  (SELECT SUM(Valor_Flete_Cliente) FROM Encabezado_Remesas WHERE EMPR_Codigo = ENPD.EMPR_Codigo AND ENPD_Numero = ENPD.Numero),--Valor_Flete_Cliente  
  (SELECT SUM(Valor_Manejo_Cliente) FROM Encabezado_Remesas WHERE EMPR_Codigo = ENPD.EMPR_Codigo AND ENPD_Numero = ENPD.Numero),--Valor_Manejo_Cliente  
  (SELECT SUM(Valor_Seguro_Cliente) FROM Encabezado_Remesas WHERE EMPR_Codigo = ENPD.EMPR_Codigo AND ENPD_Numero = ENPD.Numero),--Valor_Seguro_Cliente  
  (SELECT SUM(Valor_Descuento_Cliente) FROM Encabezado_Remesas WHERE EMPR_Codigo = ENPD.EMPR_Codigo AND ENPD_Numero = ENPD.Numero),--Valor_Descuento_Cliente  
  (SELECT SUM(Total_Flete_Cliente) FROM Encabezado_Remesas WHERE EMPR_Codigo = ENPD.EMPR_Codigo AND ENPD_Numero = ENPD.Numero),--Total_Flete_Cliente  
  (SELECT SUM(Valor_Comercial_Cliente) FROM Encabezado_Remesas WHERE EMPR_Codigo = ENPD.EMPR_Codigo AND ENPD_Numero = ENPD.Numero),--Valor_Comercial_Cliente  
  ENPD.VEHI_Codigo,--VEHI_Codigo  
  ENPD.TERC_Codigo_Conductor,--TERC_Codigo_Conductor  
  ENPD.SEMI_Codigo,--SEMI_Codigo  
  ENPD.Cantidad,--Cantidad_Transportador  
  ENPD.Peso,--Peso_Transportador  
  ENPD.Valor_Flete_Transportador,--Valor_Flete_Transportador  
  ENPD.Valor_Pagar_Transportador,--Total_Flete_Transportador  
  CLIE.Codigo,--TERC_Codigo_Destinatario  
  RUTA.CIUD_Codigo_Destino,--CIUD_Codigo_Destinatario  
  '',--Barrio_Destinatario  
  '',--Direccion_Destinatario  
  '',--Codigo_Postal_Destinatario  
  '',--Telefonos_Destinatario  
  '',--Numero_Contenedor  
  '',--MBL_Contenedor  
  '',--HBL_Contenedor  
  '',--DO_Contenedor  
  0,--Valor_FOB  
  0,--SICD_Codigo_Devolucion_Contenedor  
  '',--Fecha_Devolucion_Contenedor  
  0,--Distribucion  
  0,--Cumplido  
  0,--Anulado  
  1,--Estado  
  GETDATE(),--Fecha_Crea  
  @par_USUA_Codigo_Crea,--USUA_Codigo_Crea  
  NULL,--Fecha_Modifica  
  NULL,--USUA_Codigo_Modifica  
  NULL,--Fecha_Anula  
  NULL,--USUA_Codigo_Anula  
  '',--Causa_Anula  
  OFIC_Codigo,--OFIC_Codigo  
  0,--Numeracion  
  0,--ETCC_Numero  
  0,--ETCV_Numero  
  0,--ESOS_Numero  
  0,--ENOC_Numero  
  0,--ENPR_Numero  
  ENPD.Numero,--ENPD_Numero  
  0,--ENPE_Numero  
  @NumeroManifiesto,--ENMC_Numero  
  0,--ECPD_Numero  
  0,--ENFA_Numero  
  NULL,--Entregado  
  NULL,--Distribuido  
  NULL,--TEDI_Codigo  
  NULL,--CIUD_Codigo_Devolucion_Contenedor  
  NULL,--Patio_Devolcuion  
  NULL--Fecha_Devolcuion  
  FROM   
  Encabezado_Planilla_Despachos as ENPD  
  
  INNER JOIN Rutas as RUTA ON  
  ENPD.EMPR_Codigo = RUTA.EMPR_Codigo  
  AND ENPD.RUTA_Codigo = RUTA.Codigo  
  
  INNER JOIN Empresas as EMPR  
  ON ENPD.EMPR_Codigo = EMPR.Codigo  
    
  INNER JOIN Terceros AS CLIE ON  
  EMPR.Codigo = CLIE.EMPR_Codigo  
  AND EMPR.Numero_Identificacion = CLIE.Numero_Identificacion  
  
  WHERE                      
  ENPD.EMPR_Codigo = @par_EMPR_Codigo                      
  AND ENPD.Numero = @par_Numero_Planilla       
    
  select @NumeroRemesaManifiestoPaqueteria = @@IDENTITY  
  INSERT INTO Detalle_Manifiesto_Carga                      
  (                      
  EMPR_Codigo,                      
  ENMC_Numero,                      
  ENRE_Numero                      
  )                      
  VALUES  
  (  
  @par_EMPR_Codigo,  
  @NumeroManifiesto,  
  @NumeroRemesaManifiestoPaqueteria  
  )  
  END  
  ELSE  
  BEGIN  
   INSERT INTO Detalle_Manifiesto_Carga                      
   (                      
    EMPR_Codigo,                      
    ENMC_Numero,                      
    ENRE_Numero                      
   )                      
   SELECT                      
    EMPR_Codigo,@NumeroManifiesto,Numero                      
   FROM                      
    Encabezado_Remesas                      
   WHERE                      
    EMPR_Codigo = @par_EMPR_Codigo                      
    AND ENPD_Numero = @par_Numero_Planilla                      
  END  
                      
  ----------------------------------------------------------------                  
  --------------ASOCIAR MANIFIESTO A LA PLANILLA------------------                      
  ----------------------------------------------------------------                      
  UPDATE                      
   Encabezado_Planilla_Despachos                      
  SET                      
   ENMC_Numero = @NumeroManifiesto                      
  WHERE                      
   EMPR_Codigo = @par_EMPR_Codigo                      
   AND Numero = @par_Numero_Planilla                      
   AND TIDO_Codigo = @par_TIDO_Codigo                      
                      
  ----------------------------------------------------------------                      
  --------ASOCIAR MANIFIESTO A LAS REMESAS DE LA PLANILLA---------                      
  ----------------------------------------------------------------                      
  UPDATE                      
   Encabezado_Remesas                      
  SET                      
   ENMC_Numero = @NumeroManifiesto          
  WHERE                      
   EMPR_Codigo = @par_EMPR_Codigo                      
   AND ENPD_Numero = @par_Numero_Planilla                      
   AND ENMC_Numero = 0                      
   AND Anulado = 0                      
   AND Estado = 1                      
                      
  ----------------------------------------------------------------                      
  -------------ASOCIAR MANIFIESTO A LA ORDEN DE CARGUE------------                      
  ----------------------------------------------------------------                      
  UPDATE                      
   Encabezado_Orden_Cargues                      
  SET                      
   ENMC_Numero = @NumeroManifiesto                      
  WHERE                      
   EMPR_Codigo = @par_EMPR_Codigo                      
   AND ENPD_Numero = @par_Numero_Planilla                      
   AND ENMC_Numero = 0                      
   AND Anulado = 0                      
   AND Estado = 1                      
                      
                      
  --Si @par_ESOS_Numero es mayor que cero es porque se generó el manifiesto a partir de la planilla de la solicitud de servicio,                      
  --de lo contrario, fue generada desde la planilla de paquetería.                      
                      
  IF @par_ESOS_Numero > 0  BEGIN                      
   ----------------------------------------------------------------                      
   --------------ACTUALIZAR DETALLE DEL DESPACHO-------------------                      
   ----------------------------------------------------------------                      
   UPDATE                      
    Detalle_Despacho_Orden_Servicios                      
   SET                      
    ENMC_Numero = @NumeroManifiesto,                      
    ENMC_Numero_Documento = @NumeroDocumentoManifiesto                      
   WHERE                      
    EMPR_Codigo = @par_EMPR_Codigo                      
    AND ESOS_Numero = @par_ESOS_Numero                      
    AND ID = @par_ID                      
  END                      
  IF @par_Numero_Planilla > 0           
  BEGIN          
 UPDATE Encabezado_Remesas set ENMC_Numero = @NumeroManifiesto where EMPR_Codigo = @par_EMPR_Codigo and ENPD_Numero = @par_Numero_Planilla          
 UPDATE Detalle_Despacho_Orden_Servicios set ENMC_Numero = @NumeroManifiesto, ENMC_Numero_Documento = @NumeroDocumentoManifiesto where EMPR_Codigo = @par_EMPR_Codigo and ENPD_Numero = @par_Numero_Planilla          
  END          
  SELECT  @NumeroDocumentoManifiesto AS Manifiesto                      
                      
 END                      
 ELSE                      
 BEGIN                      
  SELECT -1 AS Manifiesto                      
 END                      
END                      
GO