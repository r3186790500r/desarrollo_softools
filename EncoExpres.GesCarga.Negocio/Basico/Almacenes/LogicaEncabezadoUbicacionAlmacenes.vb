﻿Imports EncoExpres.GesCarga.Entidades.Basico.Almacen
Imports EncoExpres.GesCarga.Fachada.Basico.Almacen
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources

Namespace Basico.Almacen
    Public NotInheritable Class LogicaEncabezadoUbicacionAlmacen
        Inherits LogicaBase(Of EncabezadoUbicacionAlmacenes)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of EncabezadoUbicacionAlmacenes)

        Sub New(persistencia As IPersistenciaBase(Of EncabezadoUbicacionAlmacenes))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As EncabezadoUbicacionAlmacenes) As Respuesta(Of IEnumerable(Of EncabezadoUbicacionAlmacenes))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of EncabezadoUbicacionAlmacenes))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of EncabezadoUbicacionAlmacenes))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As EncabezadoUbicacionAlmacenes) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Codigo = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As EncabezadoUbicacionAlmacenes) As Respuesta(Of EncabezadoUbicacionAlmacenes)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of EncabezadoUbicacionAlmacenes)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of EncabezadoUbicacionAlmacenes)(consulta)
        End Function

        Private Function DatosRequeridos(entidad As EncabezadoUbicacionAlmacenes) As Respuesta(Of EncabezadoUbicacionAlmacenes)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of EncabezadoUbicacionAlmacenes) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of EncabezadoUbicacionAlmacenes) With {.ProcesoExitoso = True}

        End Function
        Public Function Eliminar(entidad As EncabezadoUbicacionAlmacenes) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaEncabezadoUbicacionAlmacenes).Eliminar(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function
    End Class

End Namespace