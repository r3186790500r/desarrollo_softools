﻿----------------------------------------------------------------------------------------------------------
Print 'Catalogo 36 Tipo Documento Genera '
GO
DELETE Valor_Catalogos WHERE CATA_Codigo = 36
GO
DELETE Configuracion_Catalogos WHERE CATA_Codigo = 36
GO
DELETE Catalogos WHERE Codigo = 36
GO
------------------------------------------CATALOGOS------------------------------------------------------
INSERT INTO Catalogos VALUES (1,36,'TIDG','Tipo Documento Genera ',0,2,1)
GO
-----------------------------------------VALOR CATALOGOS--------------------------------------------------
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,36,3600,'(NO APLICA)','','','',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,36,3601,'Factura','','','',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,36,3602,'Comprobante de Egreso','','','',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,36,3603,'Comprobante de Ingreso','','','',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,36,3604,'Comprobante de Causación','','','',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,36,3605,'Factura Otros Conceptos','','','',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,36,3606,'Liquidacion','','','',1,GETDATE())
GO
-----------------------------------------CONFIGURACION CATALOGOS------------------------------------------
INSERT INTO configuracion_catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
VALUES (1,36,1,'Codigo',1,1,1,1,0,0,0,0,0,1,0)
GO 
INSERT INTO configuracion_catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
VALUES (1,36,2,'Nombre',1,1,1,1,0,0,0,0,0,1,0)
GO 
----------------------------------------------------------------------------------------------------------