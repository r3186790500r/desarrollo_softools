﻿Print 'gsp_consultar_tarifario_clientes'
GO
DROP PROCEDURE gsp_consultar_tarifario_clientes
GO
CREATE PROCEDURE gsp_consultar_tarifario_clientes (

@par_EMPR_Codigo			smallint,
@par_TERC_Codigo			numeric = NULL,
@par_LNTC_Codigo			int,
@par_CIUD_Codigo_Origen		numeric,
@par_CIUD_Codigo_Destino	numeric

)

AS
BEGIN

IF ISNULL(@par_TERC_Codigo, 0) > 0 BEGIN


		SELECT DTCV.EMPR_Codigo,

		DTCV.ETCV_Numero,
		DTCV.Codigo As DTCV_Codigo,
		DTCV.LNTC_Codigo, 
		DTCV.TLNC_Codigo, 
		DTCV.TATC_Codigo, 
		DTCV.TTTC_Codigo,
		TECL.CATA_FPCL_Codigo,
		DTCV.Valor_Flete,
		DTCV.Valor_Escolta,
		DTCV.Valor_Otros1, 
		DTCV.Valor_Otros2,
		DTCV.Valor_Otros3,
		DTCV.Valor_Otros4,
		TTTC.Nombre As NombreTipoTarifaCarga,
		TTTC.NombreTarifa As NombreTarifaCarga

		FROM Detalle_Tarifario_Carga_Ventas DTCV

		INNER JOIN
		V_Tipo_Tarifa_Transporte_Carga TTTC ON
		DTCV.EMPR_Codigo = TTTC.EMPR_Codigo
		AND DTCV.TATC_Codigo = TTTC.TATC_Codigo
		AND DTCV.TTTC_Codigo = TTTC.Codigo

		AND TTTC.EstadoTarifa = 1 --> Tarifa Activa
		AND TTTC.EstadoTipoTarifa = 1 --> Tipo Tarifa Activa

		LEFT JOIN
		Tercero_Clientes TECL ON
		DTCV.EMPR_Codigo = TECL.EMPR_Codigo
		AND DTCV.ETCV_Numero = TECL.ETCV_Numero
		AND TECL.TERC_Codigo = @par_TERC_Codigo

		WHERE 
		DTCV.EMPR_Codigo = @par_EMPR_Codigo
		AND DTCV.Estado = 1--> Activo
		AND DTCV.LNTC_Codigo = @par_LNTC_Codigo
		AND DTCV.CIUD_Codigo_Origen = @par_CIUD_Codigo_Origen
		AND DTCV.CIUD_Codigo_Destino = @par_CIUD_Codigo_Destino
	END
	ELSE BEGIN

	SELECT DTCV.EMPR_Codigo,

		DTCV.ETCV_Numero,
		DTCV.Codigo As DTCV_Codigo,
		DTCV.LNTC_Codigo, 
		DTCV.TLNC_Codigo, 
		DTCV.TATC_Codigo, 
		DTCV.TTTC_Codigo,

		6102, --> Forma pago contado
		DTCV.Valor_Flete,
		DTCV.Valor_Escolta,
		DTCV.Valor_Otros1, 
		DTCV.Valor_Otros2,
		DTCV.Valor_Otros3,
		DTCV.Valor_Otros4,
		TTTC.Nombre As NombreTipoTarifaCarga,
		TTTC.NombreTarifa As NombreTarifaCarga

		FROM Detalle_Tarifario_Carga_Ventas DTCV

		INNER JOIN 
		Encabezado_Tarifario_Carga_Ventas ETCV ON
		DTCV.EMPR_Codigo = ETCV.EMPR_Codigo
		AND DTCV.ETCV_Numero = ETCV.Numero
		AND ETCV.Tarifario_Base = 1

		INNER JOIN
		V_Tipo_Tarifa_Transporte_Carga TTTC ON
		DTCV.EMPR_Codigo = TTTC.EMPR_Codigo
		AND DTCV.TATC_Codigo = TTTC.TATC_Codigo
		AND DTCV.TTTC_Codigo = TTTC.Codigo

		AND TTTC.EstadoTarifa = 1 --> Tarifa Activa
		AND TTTC.EstadoTipoTarifa = 1 --> Tipo Tarifa Activa

		WHERE 
		DTCV.EMPR_Codigo = @par_EMPR_Codigo
		AND DTCV.Estado = 1--> Activo
		AND DTCV.LNTC_Codigo = @par_LNTC_Codigo
		AND DTCV.CIUD_Codigo_Origen = @par_CIUD_Codigo_Origen
		AND DTCV.CIUD_Codigo_Destino = @par_CIUD_Codigo_Destino


	END

END
GO