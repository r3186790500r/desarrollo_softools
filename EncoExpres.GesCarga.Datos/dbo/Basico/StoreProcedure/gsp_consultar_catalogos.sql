﻿PRINT 'gsp_consultar_catalogos'
GO
DROP PROCEDURE gsp_consultar_catalogos
GO
CREATE PROCEDURE gsp_consultar_catalogos
(
@par_EMPR_Codigo SMALLINT,
@par_Codigo SMALLINT = NULL
)
AS
BEGIN
	SELECT 
		EMPR_Codigo,
		Codigo,
		Nombre_Corto,
		Nombre,
		Actualizable,
		Numero_Columnas,
		Numero_Campos_Llave
	FROM
		Catalogos
	WHERE 
		EMPR_Codigo = @par_EMPR_Codigo
		AND Codigo = ISNULL(@par_Codigo, Codigo)
END
GO