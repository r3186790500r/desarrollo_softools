﻿PRINT 'gsp_insertar_detalle_distribucion_remesas'
GO
DROP PROCEDURE gsp_insertar_detalle_distribucion_remesas
GO 
CREATE PROCEDURE gsp_insertar_detalle_distribucion_remesas 
(      
@par_EMPR_Codigo SMALLINT         
,@par_ENRE_Numero NUMERIC = NULL     
,@par_PRTR_Codigo NUMERIC = NULL      
,@par_UEPT_Codigo NUMERIC = NULL      
,@par_Cantidad MONEY = NULL     
,@par_Peso MONEY = NULL    
,@par_Documento_Cliente VARCHAR(30) = NULL      
,@par_Fecha_Documento_Cliente DATE = NULL     
,@par_TERC_Codigo_Destinatario NUMERIC = NULL  
,@par_CATA_TIID_Codigo NUMERIC = NULL 
,@par_Numero_Identificacion_Destinatario VARCHAR(30) = NULL      
,@par_Nombre_Destinatario VARCHAR(50) = NULL      
,@par_CIUD_Codigo_Destinatario NUMERIC = NULL      
,@par_ZOCI_Codigo_Destinatario NUMERIC = NULL      
,@par_Barrio_Destinatario VARCHAR(50) = NULL    
,@par_Direccion_Destinatario VARCHAR(150) = NULL    
,@par_Codigo_Postal_Destinatario VARCHAR(50) = NULL    
,@par_Telefonos_Destinatario VARCHAR(100) = NULL    
,@par_Observaciones_Entrega VARCHAR(500) = NULL   
,@par_CATA_ESDR_Codigo NUMERIC = NULL       
,@par_USUA_Codigo_Crea SMALLINT       
)      
AS      
BEGIN      
      
INSERT INTO Detalle_Distribucion_Remesas     
           (EMPR_Codigo,     
            ENRE_Numero,    
            PRTR_Codigo,    
   UEPT_Codigo,    
            Cantidad,    
            Peso,    
            Documento_Cliente,    
   Fecha_Documento_Cliente,     
            TERC_Codigo_Destinatario, 
			CATA_TIID_Codigo,   
            Numero_Identificacion_Destinatario,  
            Nombre_Destinatario,    
            CIUD_Codigo_Destinatario,    
   ZOCI_Codigo_Destinatario,    
            Barrio_Destinatario,    
            Direccion_Destinatario,    
            Codigo_Postal_Destinatario,    
            Telefonos_Destinatario,    
            Observaciones_Entrega,   
            CATA_ESDR_Codigo,    
            Fecha_Crea,    
            USUA_Codigo_Crea,  
   Cantidad_Recibe,  
   Peso_Recibe  
           )      
     VALUES      
           (@par_EMPR_Codigo       
           ,@par_ENRE_Numero       
           ,ISNULL(@par_PRTR_Codigo , 0)      
           ,ISNULL(@par_UEPT_Codigo, 0)      
           ,ISNULL(@par_Cantidad, 0)           
           ,ISNULL(@par_Peso ,0)    
           ,ISNULL(@par_Documento_Cliente, '')      
           ,ISNULL(@par_Fecha_Documento_Cliente, '')           
           ,ISNULL(@par_TERC_Codigo_Destinatario ,0)  
		   ,ISNULL(@par_CATA_TIID_Codigo, 0)
     ,ISNULL(@par_Numero_Identificacion_Destinatario, '')      
           ,ISNULL(@par_Nombre_Destinatario, '')           
           ,ISNULL(@par_CIUD_Codigo_Destinatario ,0)       
     ,ISNULL(@par_ZOCI_Codigo_Destinatario ,0)   
        ,ISNULL(@par_Barrio_Destinatario, '')      
           ,ISNULL(@par_Direccion_Destinatario, '')           
           ,ISNULL(@par_Codigo_Postal_Destinatario, '')    
     ,ISNULL(@par_Telefonos_Destinatario, '')   
     ,ISNULL(@par_Observaciones_Entrega, '')   
     ,ISNULL(@par_CATA_ESDR_Codigo ,0)         
           ,GETDATE()       
           ,@par_USUA_Codigo_Crea     
     ,0  
     ,0  
   )      
      
SELECT @@IDENTITY AS Codigo      
    
END       
GO