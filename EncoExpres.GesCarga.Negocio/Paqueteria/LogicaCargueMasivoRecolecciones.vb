﻿
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources
Imports EncoExpres.GesCarga.Fachada.Paqueteria
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Despachos.Paqueteria

Namespace Paqueteria
    Public NotInheritable Class LogicaCargueMasivoRecolecciones
        Inherits LogicaBase(Of Recolecciones)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of Recolecciones)

        Sub New(persistencia As IPersistenciaBase(Of Recolecciones))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As Recolecciones) As Respuesta(Of IEnumerable(Of Recolecciones))
            Throw New NotImplementedException
        End Function





        Public Overrides Function Guardar(entidad As Recolecciones) As Respuesta(Of Long)
            Throw New NotImplementedException
        End Function

        Public Overrides Function Obtener(filtro As Recolecciones) As Respuesta(Of Recolecciones)
            Throw New NotImplementedException
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="entidad"></param>
        ''' <returns></returns>
        Private Function DatosRequeridos(entidad As Recolecciones) As Respuesta(Of Recolecciones)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of Recolecciones) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of Recolecciones) With {.ProcesoExitoso = True}

        End Function


        Public Function GenerarPlantilla(entidad As Recolecciones) As Respuesta(Of Recolecciones)
            Dim mensajeGuardo = "Genero Archivo"
            Dim url As New Recolecciones

            url = CType(_persistencia, PersistenciaCargueMasivoRecolecciones).GenerarPlantilla(entidad)

            Return New Respuesta(Of Recolecciones)(url)
        End Function



        Public Function InsertarMasivo(entidad As Recolecciones) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If


            valor = CType(_persistencia, PersistenciaCargueMasivoRecolecciones).InsertarMasivo(entidad)


            If valor.Equals(0) Then

                If Len(Trim(entidad.MensajeSQL)) > 0 Then
                    Return New Respuesta(Of Long)(entidad.MensajeSQL)
                Else
                    Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
                End If

            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .Numero = 1, .MensajeOperacion = String.Format(mensajeGuardo, valor, Recursos.OperacionInserto)}

        End Function

        Public Function ConsultarTercerosIdentificacion(entidad As Terceros) As Respuesta(Of IEnumerable(Of Terceros))

            Dim consulta = CType(_persistencia, PersistenciaCargueMasivoRecolecciones).ConsultarTercerosIdentificacion(entidad)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Terceros))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of Terceros))(consulta)

        End Function

        Public Function ObtenerUltimoConsecutivoTercero(filtro As Terceros) As Respuesta(Of Terceros)
            Dim consulta = CType(_persistencia, PersistenciaCargueMasivoRecolecciones).ObtenerUltimoConsecutivoTercero(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of Terceros)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of Terceros)(consulta)
        End Function


        Public Function ObtenerUltimaRecoleccion(filtro As Recolecciones) As Respuesta(Of Recolecciones)
            Dim consulta = CType(_persistencia, PersistenciaCargueMasivoRecolecciones).ObtenerUltimaRecoleccion(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of Recolecciones)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of Recolecciones)(consulta)
        End Function
        Public Function InsertarNuevosTerceros(entidad As Terceros) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long


            valor = CType(_persistencia, PersistenciaCargueMasivoRecolecciones).InsertarNuevosTerceros(entidad)


            If valor.Equals(0) Then

                If Len(Trim(entidad.MensajeSQL)) > 0 Then
                    Return New Respuesta(Of Long)(entidad.MensajeSQL)
                Else
                    Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
                End If

            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .Numero = 1, .MensajeOperacion = String.Format(mensajeGuardo, valor, Recursos.OperacionInserto)}

        End Function


    End Class

End Namespace