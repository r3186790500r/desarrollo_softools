﻿EncoExpresApp.controller("GestionarRemesasPaqueteriaGesphoneCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'TarifarioVentasFactory',
    'ValorCatalogosFactory', 'TercerosFactory', 'ProductoTransportadosFactory', 'CiudadesFactory', 'RemesaGuiasFactory', 'OficinasFactory',
    'blockUIConfig', 'SitiosTerceroClienteFactory', 'SitiosCargueDescargueFactory', 'ZonasFactory', 'PlanillaPaqueteriaFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, TarifarioVentasFactory,
        ValorCatalogosFactory, TercerosFactory, ProductoTransportadosFactory, CiudadesFactory, RemesaGuiasFactory, OficinasFactory,
        blockUIConfig, SitiosTerceroClienteFactory, SitiosCargueDescargueFactory, ZonasFactory, PlanillaPaqueteriaFactory) {
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        $scope.Titulo = 'GESTIONAR GUÍA';
        $scope.MapaSitio = $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Guía' }, { Nombre: 'Gestionar' }];
        $scope.Master = "#!ConsultarRemesasPaqueteriaGesphone";
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PAQUETERIA.REMESAS);
            $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

            $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
            $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
            $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
            $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }
        //--Permisos Paqueteria Remesas
        $scope.CambiarValorFlete = true;
        for (var i = 0; i < $scope.ListadoPermisos.length; i++) {
            if ($scope.ListadoPermisos[i].Codigo == PERMISO_CAMBIAR_VALOR_FLETE_PAQUETERIA) {
                $scope.CambiarValorFlete = false;
                break;
            }
        }
        //--Permisos Paqueteria Remesas
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        //--------------------------Validaciones Proceso-------------------------//
        $scope.ManejoPesoVolumetrico = $scope.Sesion.UsuarioAutenticado.ManejoPesoVolumetricoRemesaPauqeteria;
        $scope.ManejoReexpedicionOficina = $scope.Sesion.UsuarioAutenticado.ManejoReexpedicionPorOficina;
        $scope.ManejoSitiosCargueDescargueGeneral = $scope.Sesion.UsuarioAutenticado.ManejoSitioCargueDescargueGeneral;
        //--------------------------Validaciones Proceso-------------------------//
        //--------------------------Variables-------------------------//
        var CodigoCiudadOrigen = '';
        var CodigoCiudadDestino = '';
        $scope.PesoNormal = true;
        $scope.DeshabilitarOficinaDestino = false;
        $scope.ListadoDireccionesRemitente = [];
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,

            Codigo: 0,
            CodigoAlterno: 0,
            Nombre: '',
            Estado: 0,
            Remesa: {
                TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_REMESAS },
                Numero: 0,
                TipoRemesaDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA },
                //TipoRemesa: {Codigo: }
                DetalleTarifaVenta: {},
                ProductoTransportado: '',
                Fecha: new Date(),
                Remitente: { Direccion: '' },
                Destinatario: { Direccion: '' },
                Numeracion: '',
                RemesaCortesia: false
            }
        };
        //--Receptoria
        $scope.Comision = 0;
        $scope.Agencista = 0;
        var TMPValorFleteCliente = 0;
        var TMPPorcentajeAfiliado = 0;
        var TMPPorcentajeSeguro = 0;
        var OficinaObj;
        //--Receptoria
        $scope.ListaReexpedicionOficinas = [];
        $scope.ListadoDetalleUnidades = [];
        $scope.Modal = {};
        $scope.ListaEstadoDocumento = [{ Codigo: ESTADO_DEFINITIVO, Nombre: 'DEFINITIVO' }];
        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListaEstadoDocumento).First('$.Codigo ==' + ESTADO_DEFINITIVO);

        $scope.ListadoCliente = [];
        $scope.ListadoProveedor = [];
        $scope.ListadoProductoTransportados = [];
        $scope.ListadoCiudades = [];
        $scope.ListadoSitiosCargueAlterno2 = [];
        $scope.ListadoSitiosDescargueAlterno2 = [];
        $scope.ListaOficinas = [];
        $scope.ListaCondicionesComerciales = [];
        $scope.ListadoOficinaRecibe = [];
        //--------------------------Variables-------------------------//
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------AutoComplete
        //--Clientes
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CLIENTE,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListadoCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCliente);
                }
            }
            return $scope.ListadoCliente;
        };
        //--Clientes
        //--Proveedor
        $scope.AutocompleteProveedor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_PROVEEDOR,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListadoProveedor = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoProveedor);
                }
            }
            return $scope.ListadoProveedor;
        };
        //--Proveedor
        //--Producto Transportado
        $scope.AutocompleteProductoTransportado = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = ProductoTransportadosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        AplicaTarifario: -1,
                        Sync: true
                    });
                    $scope.ListadoProductoTransportados = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoProductoTransportados);
                }
            }
            return $scope.ListadoProductoTransportados;
        };
        //--Producto Transportado
        //--Ciudades
        $scope.AutocompleteCiudades = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    });
                    $scope.ListadoCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCiudades);
                }
            }
            return $scope.ListadoCiudades;
        };
        //--Ciudades
        //--Sitios Cargue Cliente
        $scope.AutocompleteSitiosClienteCargue = function (value, cliente, ciudad) {
            $scope.ListadoSitiosCargueAlterno2 = [];
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = "";
                    if ($scope.ManejoSitiosCargueDescargueGeneral == true) {
                        //--Sitios de cargue generales
                        Response = SitiosCargueDescargueFactory.Consultar({
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Ciudad: { Codigo: ciudad.Codigo == undefined ? 0 : ciudad.Codigo },
                            ValorAutocomplete: value,
                            Estado: ESTADO_ACTIVO,
                            Sync: true
                        });
                    }
                    else {
                        //--Sitios de cargue asignados al cliente
                        Response = SitiosTerceroClienteFactory.ConsultarOrdenServicio({
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Cliente: { Codigo: cliente.Codigo == undefined ? 0 : ciudad.Codigo },
                            CiudadCargue: { Codigo: ciudad.Codigo == undefined ? 0 : ciudad.Codigo },
                            ValorAutocomplete: value,
                            Sync: true
                        });
                        for (var i = 0; i < Response.Datos.length; i++) {
                            Response.Datos[i].Codigo = Response.Datos[i].SitioCliente.Codigo;
                        }
                    }
                    $scope.ListadoSitiosCargueAlterno2 = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoSitiosCargueAlterno2);
                }
            }
            return $scope.ListadoSitiosCargueAlterno2;
        };
        //--Sitios Cargue Cliente
        //--Sitios DesCargue Cliente
        $scope.AutocompleteSitiosClienteDescargue = function (value, cliente, ciudad) {
            $scope.ListadoSitiosDescargueAlterno2 = [];
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = "";
                    if ($scope.ManejoSitiosCargueDescargueGeneral == true) {
                        //--Sitios descargue generales
                        Response = SitiosCargueDescargueFactory.Consultar({
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Ciudad: { Codigo: ciudad.Codigo == undefined ? 0 : ciudad.Codigo },
                            ValorAutocomplete: value,
                            Estado: ESTADO_ACTIVO,
                            Sync: true
                        });
                    }
                    else {
                        //--Sitios descargue Cliente
                        Response = SitiosTerceroClienteFactory.ConsultarOrdenServicio({
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Cliente: { Codigo: cliente.Codigo == undefined ? 0 : ciudad.Codigo },
                            CiudadCargue: { Codigo: ciudad.Codigo == undefined ? 0 : ciudad.Codigo },
                            ValorAutocomplete: value,
                            Sync: true
                        });
                        for (var i = 0; i < Response.Datos.length; i++) {
                            Response.Datos[i].Codigo = Response.Datos[i].SitioCliente.Codigo;
                        }
                    }
                    $scope.ListadoSitiosDescargueAlterno2 = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoSitiosDescargueAlterno2);
                }
            }
            return $scope.ListadoSitiosDescargueAlterno2;
        };
        //--Sitios DesCargue Cliente
        //----------AutoComplete
        //----------Init
        $scope.InitLoad = function () {
            //-- Oficina Receptoria
            OficinaObj = OficinasFactory.Obtener({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
                Sync: true
            }).Datos;
            //--Forma de pago
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_FORMA_PAGO_VENTAS } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoFormaPago = [];
                        if (response.data.Datos.length > 0) {
                            for (var i = 0; i < response.data.Datos.length; i++) {
                                if (response.data.Datos[i].Codigo != CODIGO_CATALOGO_FORMA_PAGO_VENTAS_NO_APLICA) {
                                    $scope.ListadoFormaPago.push(response.data.Datos[i]);
                                }
                            }
                            try {
                                $scope.Modelo.Remesa.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo ==' + $scope.Modelo.Remesa.FormaPago.Codigo);
                            } catch (e) {
                                try {
                                } catch (e) {
                                    $scope.Modelo.Remesa.FormaPago = $scope.ListadoFormaPago[1];
                                }
                            }
                        }
                        else {
                            $scope.ListadoFormaPago = []
                        }
                    }
                }, function (response) {
                });
            //--Tipo rexpedición remesa paquetería
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_REEXPEDICION_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoReexpedicionRemesaPaqueteria = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoReexpedicionRemesaPaqueteria = response.data.Datos;
                            $scope.Modelo.Remesa.TipoReexpedicion = $scope.ListadoTipoReexpedicionRemesaPaqueteria[0];
                        }
                        else {
                            $scope.ListadoTipoReexpedicionRemesaPaqueteria = [];
                        }
                    }
                }, function (response) {
                });
            //--Oficinas
            $scope.ListaOficinas = OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Sync: true }).Datos;
            $scope.Modelo.OficinaDestino = $scope.ListaOficinas[0];
            //--Estado Remesa
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_ESTADO_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListaEstadosGuia = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListaEstadosGuia = response.data.Datos;
                            if (!$scope.Modelo.Numero > 0) {
                                $scope.Modelo.OficinaOrigen = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                $scope.Modelo.OficinaDestino = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                $scope.Modelo.OficinaActual = $scope.Sesion.UsuarioAutenticado.Oficinas;

                                if ($scope.CodigoEstadoGuia !== undefined && $scope.CodigoEstadoGuia !== null) {
                                    $scope.Modelo.EstadoGuia = $linq.Enumerable().From($scope.ListaEstadosGuia).First('$.Codigo ==' + $scope.CodigoEstadoGuia);

                                } else {
                                    if ($scope.Sesion.UsuarioAutenticado.Oficinas.CodigoTipoOficina == TIPO_OFICINA_PROPIA) {
                                        $scope.Modelo.EstadoGuia = $linq.Enumerable().From($scope.ListaEstadosGuia).First('$.Codigo ==' + ESTADO_GUIA_OFICINA_ORIGEN);

                                        $scope.Modelo.OficinaOrigen = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                        $scope.Modelo.OficinaDestino = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                        if ($scope.ListaOficinas !== undefined && $scope.ListaOficinas !== null) {
                                            if ($scope.ListaOficinas.length > 0) {
                                                $scope.Modelo.OficinaDestino = $linq.Enumerable().From($scope.ListaOficinas).First('$.Codigo ==' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
                                            }
                                        }
                                        $scope.Modelo.OficinaActual = $scope.Sesion.UsuarioAutenticado.Oficinas;

                                    }
                                    if ($scope.Sesion.UsuarioAutenticado.Oficinas.CodigoTipoOficina == TIPO_OFICINA_CLIENTE) {
                                        $scope.Modelo.EstadoGuia = $linq.Enumerable().From($scope.ListaEstadosGuia).First('$.Codigo ==' + ESTADO_GUIA_RECOGIDA);


                                        $scope.Modelo.OficinaOrigen = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                        $scope.Modelo.OficinaDestino = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                        if ($scope.ListaOficinas !== undefined && $scope.ListaOficinas !== null) {
                                            if ($scope.ListaOficinas.length > 0) {
                                                $scope.Modelo.OficinaDestino = $linq.Enumerable().From($scope.ListaOficinas).First('$.Codigo ==' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
                                            }
                                        }

                                        $scope.Modelo.OficinaActual = $scope.Sesion.UsuarioAutenticado.Oficinas;

                                    }
                                    if ($scope.Sesion.UsuarioAutenticado.Oficinas.CodigoTipoOficina == TIPO_OFICINA_RECEPTORIA) {
                                        $scope.Modelo.EstadoGuia = $linq.Enumerable().From($scope.ListaEstadosGuia).First('$.Codigo ==' + ESTADO_GUIA_RECOGIDA);

                                        $scope.Modelo.OficinaOrigen = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                        $scope.Modelo.OficinaDestino = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                        if ($scope.ListaOficinas !== undefined && $scope.ListaOficinas !== null) {
                                            if ($scope.ListaOficinas.length > 0) {
                                                $scope.Modelo.OficinaDestino = $linq.Enumerable().From($scope.ListaOficinas).First('$.Codigo ==' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
                                            }
                                        }
                                        $scope.Modelo.OficinaActual = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                    }
                                }
                            }
                        }
                        else {
                            $scope.ListaEstadosGuia = []
                        }
                    }
                }, function (response) {
                });
            //--Tipo entrega
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_ENTREGA_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoEntregaRemesaPaqueteria = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoEntregaRemesaPaqueteria = response.data.Datos;

                            if ($scope.CodigoTipoEntregaRemesaPaqueteria !== undefined && $scope.CodigoTipoEntregaRemesaPaqueteria !== null) {
                                if ($scope.CodigoTipoEntregaRemesaPaqueteria > 0) {
                                    $scope.Modelo.TipoEntregaRemesaPaqueteria = $linq.Enumerable().From($scope.ListadoTipoEntregaRemesaPaqueteria).First('$.Codigo ==' + $scope.CodigoTipoEntregaRemesaPaqueteria);
                                } else {
                                    $scope.Modelo.TipoEntregaRemesaPaqueteria = $scope.ListadoTipoEntregaRemesaPaqueteria[1];
                                }
                            } else {
                                $scope.Modelo.TipoEntregaRemesaPaqueteria = $scope.ListadoTipoEntregaRemesaPaqueteria[1];
                            }

                        }

                    }
                }, function (response) {
                });
            //--Tipo transporte
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_TRANSPORTE_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoTransporteRemsaPaqueteria = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoTransporteRemsaPaqueteria = response.data.Datos;
                            try {
                                $scope.Modelo.TipoTransporteRemsaPaqueteria = $linq.Enumerable().From($scope.ListadoTipoTransporteRemsaPaqueteria).First('$.Codigo ==' + $scope.Modelo.TipoTransporteRemsaPaqueteria.Codigo);
                            } catch (e) {
                                $scope.Modelo.TipoTransporteRemsaPaqueteria = $scope.ListadoTipoTransporteRemsaPaqueteria[0]
                            }
                        }
                        else {
                            $scope.ListadoTipoTransporteRemsaPaqueteria = []
                        }
                    }
                }, function (response) {
                });
            //--Tipo despacho*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_DESPACHO_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoDespachoRemesaPaqueteria = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoDespachoRemesaPaqueteria = response.data.Datos;
                            try {
                                $scope.Modelo.TipoDespachoRemesaPaqueteria = $linq.Enumerable().From($scope.ListadoTipoDespachoRemesaPaqueteria).First('$.Codigo ==' + $scope.Modelo.TipoDespachoRemesaPaqueteria.Codigo);
                            } catch (e) {
                                $scope.Modelo.TipoDespachoRemesaPaqueteria = $scope.ListadoTipoDespachoRemesaPaqueteria[0]
                            }
                        }
                        else {
                            $scope.ListadoTipoDespachoRemesaPaqueteria = []
                        }
                    }
                }, function (response) {
                });
            //--Temperatura Producto
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TEMPERATURA_PRODUCTO_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTemperaturaProductoRemesaPaqueteria = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTemperaturaProductoRemesaPaqueteria = response.data.Datos;
                            try {
                                $scope.Modelo.TemperaturaProductoRemesaPaqueteria = $linq.Enumerable().From($scope.ListadoTemperaturaProductoRemesaPaqueteria).First('$.Codigo ==' + $scope.Modelo.TemperaturaProductoRemesaPaqueteria.Codigo);
                            } catch (e) {
                                $scope.Modelo.TemperaturaProductoRemesaPaqueteria = $scope.ListadoTemperaturaProductoRemesaPaqueteria[0]
                            }
                        }
                        else {
                            $scope.ListadoTemperaturaProductoRemesaPaqueteria = []
                        }
                    }
                }, function (response) {
                });
            //--Tipo Servicio
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_SERVICIO_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoServicioRemesaPaqueteria = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoServicioRemesaPaqueteria = response.data.Datos;
                            try {
                                $scope.Modelo.TipoServicioRemesaPaqueteria = $linq.Enumerable().From($scope.ListadoTipoServicioRemesaPaqueteria).First('$.Codigo ==' + $scope.Modelo.TipoServicioRemesaPaqueteria.Codigo);
                            } catch (e) {
                                $scope.Modelo.TipoServicioRemesaPaqueteria = $scope.ListadoTipoServicioRemesaPaqueteria[0]
                            }
                        }
                        else {
                            $scope.ListadoTipoServicioRemesaPaqueteria = []
                        }
                    }
                }, function (response) {
                });
            //--Tipo interfaz
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 67 } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoInterfazRemesaPaqueteria = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoInterfazRemesaPaqueteria = response.data.Datos;
                            try {
                                $scope.Modelo.TipoInterfazRemesaPaqueteria = $linq.Enumerable().From($scope.ListadoTipoInterfazRemesaPaqueteria).First('$.Codigo ==' + $scope.Modelo.TipoInterfazRemesaPaqueteria.Codigo);
                            } catch (e) {
                                $scope.Modelo.TipoInterfazRemesaPaqueteria = $scope.ListadoTipoInterfazRemesaPaqueteria[0]
                            }
                        }
                        else {
                            $scope.ListadoTipoInterfazRemesaPaqueteria = []
                        }
                    }
                }, function (response) {
                });
            //--tipo identificaciones Tercero
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IDENTIFICACION_TERCERO } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoIdentificacion = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoIdentificacion = response.data.Datos;
                            $scope.ListadoTipoIdentificacion.splice(0, 1)
                            if ($scope.CodigoTipoIdentificacionRemitente !== undefined && $scope.CodigoTipoIdentificacionRemitente !== null) {
                                $scope.Modelo.Remesa.Remitente.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.CodigoTipoIdentificacionRemitente);
                            } else {
                                $scope.Modelo.Remesa.Remitente.TipoIdentificacion = $scope.ListadoTipoIdentificacion[0]
                            }
                            if ($scope.CodigoTipoIdentificacionDestinatario !== undefined && $scope.CodigoTipoIdentificacionDestinatario !== null) {
                                $scope.Modelo.Remesa.Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.CodigoTipoIdentificacionDestinatario);
                            } else {
                                $scope.Modelo.Remesa.Destinatario.TipoIdentificacion = $scope.ListadoTipoIdentificacion[0]
                            }

                        }
                        else {
                            $scope.ListadoTipoIdentificacion = []
                        }
                    }
                }, function (response) {
                });
            //--Catalogo Linea Negocio Paqueteria
            $scope.ListadoLineaNegocioPaqueteria = [];
            $scope.ListadoLineaNegocioPaqueteria = ValorCatalogosFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Catalogo: { Codigo: CATALOGO_LINEA_NEGOCIO_PAQUETERIA.CODIGO },
                Sync: true
            }).Datos;
            $scope.Modelo.LineaNegocioPaqueteria = $linq.Enumerable().From($scope.ListadoLineaNegocioPaqueteria).First('$.Codigo ==' + CATALOGO_LINEA_NEGOCIO_PAQUETERIA.PAQUETERIA);
            //--Catalogo Linea Negocio Paqueteria
            //--Catalogo Zonas
            $scope.ListadoZonas = [];
            $scope.ListadoZonas = ZonasFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Estado: ESTADO_DEFINITIVO,
                Ciudad: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Ciudad.Codigo },
                Sync: true
            }).Datos;
            $scope.ListadoZonas.push({ Codigo: -1, Nombre: "(NO APLICA)" });
            $scope.Modelo.Remesa.Zona = $linq.Enumerable().From($scope.ListadoZonas).First('$.Codigo ==' + -1);
            //--Catalogo Zonas
            //--Catalogo Horarios
            $scope.ListadoHorariosEntrega = [];
            $scope.ListadoHorariosEntrega = ValorCatalogosFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Catalogo: { Codigo: CODIGO_CATALOGO_HORARIO_ENTREGA_REMESAS_PAQUETERIA },
                Sync: true
            }).Datos;
            $scope.Modelo.Remesa.HorarioEntrega = $scope.ListadoHorariosEntrega[0];
            //--Catalogo Horarios
            //--Obtener Planilla De Conductor Actual
            var responPlanilla = PlanillaPaqueteriaFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_PAQUETERIA,
                ConsultaPLanillasenRuta: 1,
                Planilla: {
                    Conductor: { Codigo: $scope.Sesion.UsuarioAutenticado.Conductor.Codigo }
                },
                Sync: true
            });
            if (responPlanilla.ProcesoExitoso == true) {
                if (responPlanilla.Datos.length > 0) {
                    $scope.PlanillaPaqueteria = responPlanilla.Datos[0];
                    $scope.ExistePlanilla = true;
                }
            }
            else {
                //--Error de planilla para el conductor
                console.log(responPlanilla);
                $scope.ExistePlanilla = false;
            }
            //--Obtener Planilla De Conductor Actual
            //----Obtener parametros
            if ($routeParams.Numero > 0) {
                $scope.Modelo.Numero = $routeParams.Numero;
                Obtener();
            }
        };
        //----------Init
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------------------------Funciones Generales---------------------------------//
        //--Limpia Sitio Cargue
        $scope.LimpiarSitioCargue = function (Ciudad) {
            if (CodigoCiudadOrigen !== Ciudad.Codigo) {
                CodigoCiudadOrigen = Ciudad.Codigo;
                $scope.Modelo.Remesa.Remitente.SitioCargue = '';
                $scope.Modelo.Remesa.ValorCargue = '';
            }
        };
        //--Limpia Sitio Cargue
        //--Limpia Sitio Descargue
        $scope.LimpiarSitioDescargue = function (Ciudad) {
            if (CodigoCiudadDestino !== Ciudad.Codigo) {
                CodigoCiudadDestino = Ciudad.Codigo;
                $scope.Modelo.Remesa.Destinatario.SitioDescargue = '';
                $scope.Modelo.Remesa.ValorDescargue = '';
            }
        };
        //--Limpia Sitio Descargue
        //--Oficina Rebice
        $scope.ObtenerOficinaRecibe = function () {
            if ($scope.Modelo.RecogerOficinaDestino && $scope.Modelo.Remesa.Destinatario.Ciudad != undefined) {
                if ($scope.Modelo.Remesa.Destinatario.Ciudad.Codigo > 0) {
                    $scope.ListadoOficinaRecibe = OficinasFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CodigoCiudad: $scope.Modelo.Remesa.Destinatario.Ciudad.Codigo,
                        Estado: ESTADO_ACTIVO,
                        Sync: true
                    }).Datos;
                    if ($scope.ListadoOficinaRecibe.length > 0) {
                        $scope.Modelo.OficinaRecibe = $scope.ListadoOficinaRecibe[0];
                    }
                }
            }
        };
        //--Oficina Rebice
        //--Cliente o Remitente
        $scope.ObtenerInformacionRemitente = function (esCliente) {
            var response = null;
            //--Obtiene Informacion ya sea de cliente o remitente
            if (esCliente) {
                if ($scope.Modelo.Remesa.Cliente != undefined && $scope.Modelo.Remesa.Cliente != '' && $scope.Modelo.Remesa.Cliente != null) {
                    response = TercerosFactory.Obtener({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Codigo: $scope.Modelo.Remesa.Cliente.Codigo,
                        Sync: true
                    });
                }
                else {
                    $scope.Modelo.Remesa.Remitente.NumeroIdentificacion = '';
                    LimpiaCamposRemitente();
                }
            }
            else {
                if ($scope.Modelo.Remesa.Remitente.NumeroIdentificacion != undefined && $scope.Modelo.Remesa.Remitente.NumeroIdentificacion != '' && $scope.Modelo.Remesa.Remitente.NumeroIdentificacion != null) {
                    $scope.RemitenteNoRegistrado = true;
                    response = TercerosFactory.Obtener({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        NumeroIdentificacion: $scope.Modelo.Remesa.Remitente.NumeroIdentificacion,
                        Sync: true
                    });
                }
                else {
                    LimpiaCamposRemitente();
                }
            }
            //--Obtiene Informacion ya sea de cliente o remitente
            if (response != null) {
                if (response.ProcesoExitoso == true) {
                    if (response.Datos.Codigo > 0) {
                        $scope.RemitenteNoRegistrado = false;
                        //--Carga Informacion Remitente en el Modelo
                        $scope.Modelo.Remesa.Remitente = response.Datos;
                        if ($scope.Modelo.Remesa.Remitente.Direcciones.length > 0) {
                            $scope.ListadoDireccionesRemitente = $scope.Modelo.Remesa.Remitente.Direcciones;
                        } else {
                            $scope.ListadoDireccionesRemitente = [];
                        }
                        if ($scope.Modelo.Remesa.Remitente.Direccion !== '' && $scope.Modelo.Remesa.Remitente.Direccion !== undefined && $scope.Modelo.Remesa.Remitente.Direccion !== null) {
                            $scope.ListadoDireccionesRemitente.push({ Direccion: $scope.Modelo.Remesa.Remitente.Direccion });
                            $scope.Modelo.Remesa.Remitente.Direccion = $scope.ListadoDireccionesRemitente[0].Direccion;

                        }
                        $scope.Modelo.Remesa.Remitente.Direccion = $scope.ListadoDireccionesRemitente[0].Direccion;
                        $scope.Modelo.Remesa.Remitente.Ciudad = $scope.CargarCiudad($scope.Modelo.Remesa.Remitente.Ciudad.Codigo);
                        $scope.Modelo.Remesa.Remitente.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.Modelo.Remesa.Remitente.TipoIdentificacion.Codigo);
                        $scope.Modelo.Remesa.Remitente.Telefonos = $scope.Modelo.Remesa.Remitente.Telefonos.replace(';', ' - ');
                        $scope.Modelo.Remesa.Remitente.CorreoFacturacion = response.Datos.CorreoFacturacion;
                        //--Manejo Remitente Cliente Condiciones Comerciales Tarifa
                        if (response.Datos.Cliente != undefined && response.Datos.Cliente != '' && response.Datos.Cliente != null) {
                            if (response.Datos.Cliente.CondicionesComercialesTarifas == ESTADO_ACTIVO) {
                                $scope.ListaCondicionesComerciales = response.Datos.Cliente.CondicionesComerciales;
                            }
                        }
                        //--Manejo Remitente Cliente Condiciones Comerciales Tarifa
                        $scope.Modelo.Remesa.Remitente.Nombre = response.Datos.NombreCompleto;
                        //--Linea Servicio Remitente
                        try {
                            $scope.ListadoLineaServicio = [];
                            $scope.ListadoLineaServicio = response.Datos.LineaServicio;
                            $scope.ListadoLineaServicio.push({ Codigo: 19400, Nombre: '(NO APLICA)' });
                            if ($scope.ListadoLineaServicio.length > 0) {
                                $scope.Modelo.Remesa.Remitente.LineaSerivicioCliente = $linq.Enumerable().From($scope.ListadoLineaServicio).First('$.Codigo ==' + 19400);
                            }
                        } catch (e) {
                            $scope.ListadoLineaServicio = [];
                        }
                        //--Linea Servicio Remitente
                        //--Verifica si es cliente o remitente para bloqueo de campos
                        if (esCliente) {
                            $scope.Modelo.Remesa.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo ==' + 4901);
                            $scope.BloqueoRemitente = true;
                            $scope.BloqueoRemitenteNombre = true;
                            $scope.BloqueoRemitenteIdentificacion = true;
                            $scope.Modelo.Remesa.Remitente.Nombre = response.Datos.NombreCompleto;
                        }
                        else {
                            $scope.Modelo.Remesa.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo ==' + CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CONTADO);
                            $scope.BloqueoRemitenteNombre = false;
                            $scope.BloqueoRemitenteIdentificacion = false;
                        }
                        //--Verifica si es cliente o remitente para bloqueo de campos
                        //--Mismo Destinatario Inicial
                        $scope.Modelo.Remesa.Destinatario = angular.copy($scope.Modelo.Remesa.Remitente);
                        $scope.Modelo.Remesa.Destinatario.Ciudad = '';
                        $scope.Modelo.Remesa.Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.Modelo.Remesa.Destinatario.TipoIdentificacion.Codigo);
                        $scope.BloqueoDestinatario = true;
                        //--Mismo Destinatario Inicial
                        //--Obtiener Tarifas
                        //$scope.ValidarTarifarioCliente();
                        //--Obtiener Tarifas
                    }
                    else {
                        LimpiaCamposRemitente();
                    }
                }
                else {
                    LimpiaCamposRemitente();
                }
            }
        };
        function LimpiaCamposRemitente() {
            $scope.Modelo.Remesa.Remitente.Codigo = '';
            $scope.Modelo.Remesa.Remitente.Nombre = '';
            $scope.ListadoDireccionesRemitente = [];
            $scope.Modelo.Remesa.Remitente.Direccion = '';
            $scope.Modelo.Remesa.Remitente.Ciudad = '';
            $scope.Modelo.Remesa.Remitente.TipoIdentificacion = $scope.ListadoTipoIdentificacion[0];
            $scope.Modelo.Remesa.Remitente.Telefonos = '';
            $scope.Modelo.Remesa.Remitente.CorreoFacturacion = '';
            $scope.BloqueoRemitente = false;
            $scope.BloqueoRemitenteNombre = false;
            $scope.BloqueoRemitenteIdentificacion = false;
            $scope.RemitenteNoRegistrado = false;
        }

        function ObtenerRemitenteAlterno(Codigo) {
            var FiltroRemitente = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: Codigo,
                NumeroIdentificacion: $scope.Modelo.Remesa.Remitente.NumeroIdentificacion

            };
            TercerosFactory.Obtener(FiltroRemitente).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.Codigo > 0) {
                            try {
                                $scope.ListadoLineaServicio = []
                                $scope.ListadoLineaServicio = response.data.Datos.LineaServicio
                                $scope.ListadoLineaServicio.push({ Codigo: 19400, Nombre: '(NO APLICA)' });
                                if ($scope.ListadoLineaServicio.length > 0) {
                                    $scope.Modelo.Remesa.Remitente.LineaSerivicioCliente = $linq.Enumerable().From($scope.ListadoLineaServicio).First('$.Codigo ==' + 19400);
                                }
                                try {
                                    $scope.Modelo.Remesa.Remitente.LineaSerivicioCliente = $linq.Enumerable().From($scope.ListadoLineaServicio).First('$.Codigo ==' + $scope.Modelo.Remesa.Remitente.LineaSerivicioCliente.Codigo);
                                } catch (e) {

                                }
                            } catch (e) {
                                $scope.ListadoLineaServicio = []

                            }
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

        }

        //--Cliente o Remitente
        //--Destinatario
        $scope.ObtenerInformacionDestinatario = function () {
            if ($scope.Modelo.Remesa.Destinatario.NumeroIdentificacion != undefined && $scope.Modelo.Remesa.Destinatario.NumeroIdentificacion != '' && $scope.Modelo.Remesa.Destinatario.NumeroIdentificacion != null) {
                $scope.DestinatarioNoRegistrado = true;
                var response = TercerosFactory.Obtener({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroIdentificacion: $scope.Modelo.Remesa.Destinatario.NumeroIdentificacion,
                    Sync: true
                });
                if (response.ProcesoExitoso == true) {
                    if (response.Datos.Codigo > 0) {
                        $scope.DestinatarioNoRegistrado = false;
                        $scope.Modelo.Remesa.Destinatario = response.Datos;
                        $scope.Modelo.Remesa.Destinatario.NumeroIdentificacion = response.Datos.NumeroIdentificacion;
                        $scope.Modelo.Remesa.Destinatario.Nombre = response.Datos.NombreCompleto;
                        $scope.Modelo.Remesa.Destinatario.Ciudad = '';
                        $scope.Modelo.Remesa.Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + response.Datos.TipoIdentificacion.Codigo);
                        if ($scope.Modelo.Remesa.Destinatario.Direcciones.length > 0) {
                            $scope.ListadoDireccionesDestinatario = $scope.Modelo.Remesa.Destinatario.Direcciones;
                        } else {
                            $scope.ListadoDireccionesDestinatario = [];
                        }
                        if ($scope.Modelo.Remesa.Destinatario.Direccion !== '' && $scope.Modelo.Remesa.Destinatario.Direccion !== undefined && $scope.Modelo.Remesa.Destinatario.Direccion !== null) {
                            $scope.ListadoDireccionesDestinatario.push({ Direccion: $scope.Modelo.Remesa.Destinatario.Direccion });
                            $scope.Modelo.Remesa.Destinatario.Direccion = $scope.ListadoDireccionesDestinatario[0].Direccion;

                        }
                        $scope.Modelo.Remesa.Destinatario.Telefonos = $scope.Modelo.Remesa.Destinatario.Telefonos.replace(';', ' - ');
                        $scope.Modelo.Remesa.Destinatario.CorreoFacturacion = response.Datos.CorreoFacturacion;
                        $scope.BloqueoDestinatario = true;
                    }
                    else {
                        LimpiaCamposDestinatario();
                    }
                }
                else {
                    LimpiaCamposRemitente();
                }
            }
            else {
                LimpiaCamposDestinatario();
            }
        };
        function LimpiaCamposDestinatario() {
            $scope.Modelo.Remesa.Destinatario.Nombre = '';
            $scope.ListadoDireccionesDestinatario = [];
            $scope.Modelo.Remesa.Destinatario.Direccion = '';
            $scope.Modelo.Remesa.Destinatario.Ciudad = '';
            $scope.Modelo.Remesa.Destinatario.TipoIdentificacion = $scope.ListadoTipoIdentificacion[0];
            $scope.Modelo.Remesa.Destinatario.Telefonos = '';
            $scope.Modelo.Remesa.Destinatario.CorreoFacturacion = '';
            $scope.BloqueoDestinatario = false;
        }
        //--Destinatario
        //-- Sitios de Cargue y Descargue
        //--Cargue
        $scope.CargarInformacionSitioCargue = function (SitioCargue) {
            if (SitioCargue != undefined && SitioCargue != null && SitioCargue != '') {
                if ($scope.ManejoSitiosCargueDescargueGeneral) {//--Sitio Cargue general
                    if (SitioCargue.Direccion != '')
                        $scope.Modelo.Remesa.Remitente.Direccion = SitioCargue.Direccion;
                    if (SitioCargue.Telefono)
                        $scope.Modelo.Remesa.Remitente.Telefonos = SitioCargue.Telefono;
                    if (SitioCargue.ValorCargue) {
                        $scope.Modelo.Remesa.ValorCargue = SitioCargue.ValorCargue;
                        $scope.MaskValores();
                    }

                }
                else {//--Sitio Cargue cliente
                    if (SitioCargue.DireccionSitio != '')
                        $scope.Modelo.Remesa.Remitente.Direccion = SitioCargue.DireccionSitio;
                    if (SitioCargue.Telefono) {
                        $scope.Modelo.Remesa.Remitente.Telefonos = SitioCargue.Telefono;
                    }
                }
            }
        };
        //--Descargue
        $scope.CargarInformacionSitioDesCargue = function (SitioDesCargue) {
            if (SitioDesCargue != undefined && SitioDesCargue != null && SitioDesCargue != '') {
                if ($scope.ManejoSitiosCargueDescargueGeneral) {//--Sitio Descargue general
                    if (SitioDesCargue.Direccion != '')
                        $scope.Modelo.Remesa.Destinatario.Direccion = SitioDesCargue.Direccion;
                    if (SitioDesCargue.Telefono)
                        $scope.Modelo.Remesa.Destinatario.Telefonos = SitioDesCargue.Telefono;
                    if (SitioDesCargue.ValorDescargue) {
                        $scope.Modelo.Remesa.ValorDescargue = SitioDesCargue.ValorDescargue;
                        $scope.MaskValores();
                    }
                }
                else {//--Sitio Descargue Cliiente
                    if (SitioDesCargue.DireccionSitio != '')
                        $scope.Modelo.Remesa.Destinatario.Direccion = SitioDescargue.DireccionSitio;
                    if (SitioDesCargue.Telefono) {
                        $scope.Modelo.Remesa.Destinatario.Telefonos = SitioDescargue.Telefono;
                    }
                }
            }
        };
        //-- Sitios de Cargue y Descargue
        //---------------------Gestion Tarifas------------------------------//
        function DatosRequeridosTarifas() {
            var Continuar = true;
            if ($scope.Modelo.Remesa.Remitente.Ciudad !== undefined && $scope.Modelo.Remesa.Remitente.Ciudad !== null) {
                if (!($scope.Modelo.Remesa.Remitente.Ciudad.Codigo > 0)) {
                    Continuar = false;
                }
            } else {
                Continuar = false;
            }
            if ($scope.Modelo.Remesa.Destinatario.Ciudad !== undefined && $scope.Modelo.Remesa.Destinatario.Ciudad !== null) {
                if (!($scope.Modelo.Remesa.Destinatario.Ciudad.Codigo > 0)) {
                    Continuar = false;
                }
            } else {
                Continuar = false;
            }
            return Continuar;
        }
        $scope.ValidarTarifarioCliente = function () {
            if (DatosRequeridosTarifas() == true) {
                blockUIConfig.autoBlock = true;
                blockUIConfig.delay = 0;
                blockUI.start("Cargando Tarifas...");
                $timeout(function () { blockUI.message("Cargando Tarifas..."); ObtenerTarifarioCliente(); }, 100);
            }
        };
        //--Obtiene detalle tarifas del cliente teniendo en cuenta origen y destino
        function ObtenerTarifarioCliente() {
            $scope.ListaAuxTipoTarifas = [];
            $scope.ListaTarifaCarga = [];
            $scope.ListaTipoTarifaCargaVenta = [];
            var TipoLineaNegocioAux = 0;

            if ($scope.Modelo.Remesa.Remitente.Ciudad.Codigo == $scope.Modelo.Remesa.Destinatario.Ciudad.Codigo) {
                TipoLineaNegocioAux = CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_URBANA;
                CodigoTipoRemesa = CODIGO_TIPO_REMESA_URBANA;
            }
            else {
                TipoLineaNegocioAux = CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_NACIONAL;
                CodigoTipoRemesa = CODIGO_TIPO_REMESA_NACIONAL;
            }
            $scope.CodigoCliente = 0;
            if ($scope.Modelo.Remesa.Cliente !== undefined && $scope.Modelo.Remesa.Cliente !== null && $scope.Modelo.Remesa.Cliente !== '') {
                $scope.CodigoCliente = $scope.Modelo.Remesa.Cliente.Codigo;
            }

            $scope.TarifaCliente = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.CodigoCliente,
                CodigoCiudadOrigen: $scope.Modelo.Remesa.Remitente.Ciudad.Codigo,
                CodigoCiudadDestino: $scope.Modelo.Remesa.Destinatario.Ciudad.Codigo,
                CodigoLineaNegocio: CODIGO_LINEA_NEGOCIO_TRANPSORTE_CARGA_PAQUETERIA,
                CodigoTipoLineaNegocio: TipoLineaNegocioAux,
                CodigoFormaPago: $scope.Modelo.Remesa.FormaPago.Codigo,
                TarifarioProveedores: false,
                TarifarioClientes: true,
                Sync: true
            };

            var ResponsTercero = TercerosFactory.Consultar($scope.TarifaCliente);
            if (ResponsTercero.ProcesoExitoso == true) {
                if (ResponsTercero.Datos.length > 0) {
                    for (var i = 0; i < ResponsTercero.Datos.length; i++) {
                        $scope.NumeroTarifario = ResponsTercero.Datos[i].NumeroTarifarioVenta;
                        var InsertaTarifa = true;
                        for (var j = 0; j < $scope.ListaTarifaCarga.length; j++) {
                            if ($scope.ListaTarifaCarga[j].Codigo == ResponsTercero.Datos[i].TarifaCarga.Codigo) {
                                InsertaTarifa = false;
                            }
                        }
                        if (InsertaTarifa == true) {
                            $scope.ListaTarifaCarga.push(ResponsTercero.Datos[i].TarifaCarga);
                        }
                        var AuxTipoTarifa = {
                            CodigoDetalleTarifa: ResponsTercero.Datos[i].CodigoDetalleTarifa,
                            Codigo: ResponsTercero.Datos[i].TipoTarifaCarga.Codigo,
                            CodigoTarifa: ResponsTercero.Datos[i].TarifaCarga.Codigo,
                            Nombre: ResponsTercero.Datos[i].TipoTarifaCarga.Nombre,
                            ValorFlete: ResponsTercero.Datos[i].ValorFlete,
                            ValorManejo: ResponsTercero.Datos[i].ValorManejo,
                            ValorSeguro: ResponsTercero.Datos[i].ValorSeguro,
                            PorcentajeAfiliado: ResponsTercero.Datos[i].PorcentajeAfiliado,
                            PorcentajeSeguro: ResponsTercero.Datos[i].PorcentajeSeguro,
                            PesoMinimo: ResponsTercero.Datos[i].ValorCatalogoTipoTarifa.CampoAuxiliar2,
                            PesoMaximo: ResponsTercero.Datos[i].ValorCatalogoTipoTarifa.CampoAuxiliar3,
                            Reexpedicion: ResponsTercero.Datos[i].Reexpedicion,
                            PorcentajeReexpedicion: ResponsTercero.Datos[i].PorcentajeReexpedicion,
                            CondicionesComerciales: false,
                            FleteMinimo: 0,
                            ManejoMinimo: 0,
                            PorcentajeValorDeclarado: 0,
                            PorcentajeFlete: 0,
                            PorcentajeVolumen: 0
                        };
                        //--Valida Condiciones Comerciales con Tarifario
                        for (var j = 0; j < $scope.ListaCondicionesComerciales.length; j++) {
                            if ($scope.ListaCondicionesComerciales[j].LineaNegocio.Codigo == CODIGO_LINEA_NEGOCIO_TRANPSORTE_CARGA_PAQUETERIA &&
                                $scope.ListaCondicionesComerciales[j].Tarifa.Codigo == ResponsTercero.Datos[i].TarifaCarga.Codigo &&
                                $scope.ListaCondicionesComerciales[j].TipoTarifa.Codigo == ResponsTercero.Datos[i].TipoTarifaCarga.Codigo) {
                                AuxTipoTarifa.FleteMinimo = $scope.ListaCondicionesComerciales[j].FleteMinimo;
                                AuxTipoTarifa.ManejoMinimo = $scope.ListaCondicionesComerciales[j].ManejoMinimo;
                                AuxTipoTarifa.PorcentajeValorDeclarado = $scope.ListaCondicionesComerciales[j].PorcentajeValorDeclarado;
                                AuxTipoTarifa.PorcentajeFlete = $scope.ListaCondicionesComerciales[j].PorcentajeFlete;
                                AuxTipoTarifa.PorcentajeVolumen = $scope.ListaCondicionesComerciales[j].PorcentajeVolumen;
                                AuxTipoTarifa.CondicionesComerciales = true;
                            }
                        }
                        //--Valida Condiciones Comerciales con Tarifario
                        $scope.ListaAuxTipoTarifas.push(AuxTipoTarifa);

                        if (i == 0) {
                            $scope.Modelo.Remesa.NumeroTarifarioVenta = ResponsTercero.Datos[i].NumeroTarifarioVenta;
                        }
                        //$scope.TarifarioInvalido = false;
                    }
                    if ($scope.CodigoTarifaCarga !== undefined && $scope.CodigoTarifaCarga !== null) {
                        if ($scope.CodigoTarifaCarga > 0) {
                            $scope.Modelo.Remesa.TarifaCarga = $linq.Enumerable().From($scope.ListaTarifaCarga).First('$.Codigo ==' + $scope.CodigoTarifaCarga);
                        }
                        else {
                            try {
                                $scope.Modelo.Remesa.TarifaCarga = $linq.Enumerable().From($scope.ListaTarifaCarga).First('$.Codigo ==' + 302);//Porducto
                            } catch (e) {
                                $scope.Modelo.Remesa.TarifaCarga = $scope.ListaTarifaCarga[0];
                            }
                        }
                    }
                    else {
                        try {
                            $scope.Modelo.Remesa.TarifaCarga = $linq.Enumerable().From($scope.ListaTarifaCarga).First('$.Codigo ==' + 302);//Porducto
                        } catch (e) {
                            $scope.Modelo.Remesa.TarifaCarga = $scope.ListaTarifaCarga[0];
                        }
                    }
                    try {
                        $scope.ObtenerValoresPaqueteria();

                    } catch (e) {

                    }
                    $scope.CargarTipoTarifa($scope.Modelo.Remesa.TarifaCarga.Codigo);
                }
            }
            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
        }
        $scope.ObtenerValoresPaqueteria = function () {
            $scope.TarifasPaqueteria = false;
            try {
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.NumeroTarifario,
                    Sync: true
                };
                var response = TarifarioVentasFactory.ObtenerDetalleTarifarioPaqueteria(filtros);
                if (response.ProcesoExitoso == true) {
                    if (response.Datos.Paqueteria !== undefined && response.Datos.Paqueteria !== '' && response.Datos.Paqueteria !== null) {
                        $scope.TarifasPaqueteria = true;
                        $scope.PaqueteriaMinimoValorSeguro = response.Datos.Paqueteria.MinimoValorSeguro;
                        $scope.PaqueteriaPorcentajeSeguro = response.Datos.Paqueteria.PorcentajeSeguro;
                        $scope.PaqueteriaMinimoValorManejo = response.Datos.Paqueteria.MinimoValorManejo;
                    }
                }
            } catch (e) {

            }
        };
        //--Carga Tipo Tarifa Inicial
        $scope.CargarTipoTarifa = function (CodigoTarifaCarga) {
            $scope.ListaTipoTarifaCargaVenta = [];
            $scope.ListaAuxTipoTarifas.forEach(function (item) {
                if (CodigoTarifaCarga == item.CodigoTarifa) {
                    $scope.ListaTipoTarifaCargaVenta.push(item);
                }
            });
            $scope.Modelo.Remesa.TipoTarifaCarga = $scope.ListaTipoTarifaCargaVenta[0];
            if ($scope.CodigoTipoTarifaTransporte !== undefined && $scope.CodigoTipoTarifaTransporte !== null) {
                if ($scope.CodigoTipoTarifaTransporte > 0) {
                    $scope.Modelo.Remesa.TipoTarifaCarga = $linq.Enumerable().From($scope.ListaTipoTarifaCargaVenta).First('$.Codigo ==' + $scope.CodigoTipoTarifaTransporte);
                }
            }
            $scope.Calcular();
        };
        //---------------------Gestion Tarifas------------------------------//
        //--Valida Producto
        $scope.ValidarProducto = function () {
            if ($scope.Modelo.Remesa.ProductoTransportado !== undefined) {
                if ($scope.Modelo.Remesa.ProductoTransportado.Codigo > 0) {
                    $scope.Modelo.Remesa.DescripcionProductoTransportado = '';
                } else {
                    $scope.Modelo.Remesa.ProductoTransportado.inserta = true;
                    $scope.Modelo.Remesa.DescripcionProductoTransportado = '';

                }
            }
        };
        //--Valida Producto
        //--Validar Peso
        $scope.ValidarPeso = function () {
            var peso = parseFloat(RevertirMV($scope.Modelo.PesoCobrar));
            if (peso > 0) {
                if ($scope.Modelo.LineaNegocioPaqueteria.Codigo == CATALOGO_LINEA_NEGOCIO_PAQUETERIA.COURIER) {
                    if (peso > 5) {
                        ShowError("El peso debe estar entre 1 y 5 kg cuando la línea de negocio es COURIER");
                        if ($scope.PesoNormal) {
                            $scope.Modelo.Remesa.PesoCliente = 0;
                        }
                        else {
                            $scope.Modelo.Largo = 0;
                            $scope.Modelo.Alto = 0;
                            $scope.Modelo.Ancho = 0;
                            $scope.PesoVolumetrico = 0;
                        }
                        $scope.Modelo.PesoCobrar = 0;
                    }
                }
                if ($scope.Modelo.LineaNegocioPaqueteria.Codigo == CATALOGO_LINEA_NEGOCIO_PAQUETERIA.PAQUETERIA) {
                    if (peso <= 5) {
                        ShowError("El peso debe ser mayor a 5 kg cuando la línea de negocio es PAQUETERÍA");
                        if ($scope.PesoNormal) {
                            $scope.Modelo.Remesa.PesoCliente = 0;
                        }
                        else {
                            $scope.Modelo.Largo = 0;
                            $scope.Modelo.Alto = 0;
                            $scope.Modelo.Ancho = 0;
                            $scope.PesoVolumetrico = 0;
                        }
                        $scope.Modelo.PesoCobrar = 0;
                    }
                }
            }
        };
        //--Validar Peso
        //-----------Calcular----------//
        $scope.Calcular = function () {
            if ($scope.Modelo.Remesa.CantidadCliente == undefined || $scope.Modelo.Remesa.CantidadCliente == 0 || $scope.Modelo.Remesa.CantidadCliente == "") {
                $scope.Modelo.Remesa.CantidadCliente = 1;
            }
            var ValorFleteCliente = 0;
            var PorcentajeAfiliado = 0;
            var PorcentajeSeguro = 0;
            var detalleTarifa = {};
            var Descuento = 0;

            $scope.ValorReexpedicion = 0;
            $scope.Modelo.Remesa.ValorFleteCliente = 0;
            $scope.Modelo.Remesa.ValorManejoCliente = 0;
            $scope.Modelo.Remesa.ValorSeguroCliente = 0;
            $scope.Modelo.Remesa.ValorFleteTransportador = 0;
            var Calculo = false;
            $scope.Modelo.PesoCobrar = RevertirMV($scope.Modelo.Remesa.PesoCliente);
            if ($scope.ManejoPesoVolumetrico) {
                if ($scope.Modelo.Alto !== undefined && $scope.Modelo.Alto !== '' && $scope.Modelo.Alto !== '' &&
                    $scope.Modelo.Ancho !== undefined && $scope.Modelo.Ancho !== '' && $scope.Modelo.Ancho !== '' &&
                    $scope.Modelo.Largo !== undefined && $scope.Modelo.Largo !== '' && $scope.Modelo.Largo !== '') {
                    $scope.Modelo.PesoVolumetrico = ((RevertirMV($scope.Modelo.Alto) * RevertirMV($scope.Modelo.Ancho) * RevertirMV($scope.Modelo.Largo)) / 1000000) * 400
                    $scope.Modelo.PesoVolumetrico = $scope.Modelo.PesoVolumetrico.toFixed(2);
                }
                else {
                    $scope.Modelo.PesoVolumetrico = 0;
                }
                //--Compara Peso volumetrico con peso normal
                if (RevertirMV($scope.Modelo.Remesa.PesoCliente) > 0 || $scope.Modelo.PesoVolumetrico > 0) {
                    if (RevertirMV($scope.Modelo.Remesa.PesoCliente) > $scope.Modelo.PesoVolumetrico) {
                        $scope.Modelo.PesoCobrar = RevertirMV($scope.Modelo.Remesa.PesoCliente);
                    }
                    if ($scope.Modelo.PesoVolumetrico > RevertirMV($scope.Modelo.Remesa.PesoCliente)) {
                        $scope.Modelo.PesoCobrar = RevertirMV($scope.Modelo.PesoVolumetrico);
                        $scope.PesoNormal = false;
                    }
                } else {
                    $scope.Modelo.PesoCobrar = RevertirMV($scope.Modelo.Remesa.PesoCliente);
                }
            } else {
                $scope.Modelo.PesoCobrar = RevertirMV($scope.Modelo.Remesa.PesoCliente);
            }
            $scope.ValidarPeso();
            if ($scope.Modelo.PesoCobrar > 0 && ($scope.Modelo.Remesa.TarifaCarga != null && $scope.Modelo.Remesa.TarifaCarga != undefined)) {

                if ($scope.Modelo.Remesa.TarifaCarga.Codigo == 302) {
                    for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa && ($scope.ListaTipoTarifaCargaVenta[i].Codigo == $scope.Modelo.Remesa.ProductoTransportado.Codigo)) {
                            ValorFleteCliente = parseFloat(($scope.Modelo.Remesa.CantidadCliente * $scope.ListaTipoTarifaCargaVenta[i].ValorFlete).toFixed(2));
                            detalleTarifa = $scope.ListaTipoTarifaCargaVenta[i];//--Detalle Tarifa Para Calculos
                            PorcentajeAfiliado = $scope.ListaTipoTarifaCargaVenta[i].PorcentajeAfiliado;
                            PorcentajeSeguro = $scope.ListaTipoTarifaCargaVenta[i].PorcentajeSeguro;
                            //$scope.Modelo.Remesa.ValorManejoCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorManejo;
                            //$scope.Modelo.Remesa.ValorSeguroCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                            Calculo = true;
                        }
                    }
                }
                if ($scope.Modelo.Remesa.TarifaCarga.Codigo == TARIFA_RANGO_PESO_VALOR_KILO) {
                    for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa &&
                            $scope.Modelo.PesoCobrar >= $scope.ListaTipoTarifaCargaVenta[i].PesoMinimo &&
                            $scope.Modelo.PesoCobrar <= $scope.ListaTipoTarifaCargaVenta[i].PesoMaximo) {
                            ValorFleteCliente = parseFloat(($scope.Modelo.PesoCobrar * $scope.ListaTipoTarifaCargaVenta[i].ValorFlete).toFixed(2));
                            detalleTarifa = $scope.ListaTipoTarifaCargaVenta[i];//--Detalle Tarifa Para Calculos
                            PorcentajeAfiliado = $scope.ListaTipoTarifaCargaVenta[i].PorcentajeAfiliado;
                            PorcentajeSeguro = $scope.ListaTipoTarifaCargaVenta[i].PorcentajeSeguro;
                            //$scope.Modelo.Remesa.ValorManejoCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorManejo;
                            //$scope.Modelo.Remesa.ValorSeguroCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                            Calculo = true;
                            break;
                        }
                    }
                }
                if ($scope.Modelo.Remesa.TarifaCarga.Codigo == TARIFA_RANGO_PESO_VALOR_FIJO) {
                    for (var i = 0; i < $scope.ListaTipoTarifaCargaVenta.length; i++) {
                        if ($scope.Modelo.Remesa.TarifaCarga.Codigo == $scope.ListaTipoTarifaCargaVenta[i].CodigoTarifa &&
                            $scope.Modelo.PesoCobrar >= $scope.ListaTipoTarifaCargaVenta[i].PesoMinimo &&
                            $scope.Modelo.PesoCobrar <= $scope.ListaTipoTarifaCargaVenta[i].PesoMaximo) {
                            ValorFleteCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorFlete;
                            detalleTarifa = $scope.ListaTipoTarifaCargaVenta[i];//--Detalle Tarifa Para Calculos
                            PorcentajeAfiliado = $scope.ListaTipoTarifaCargaVenta[i].PorcentajeAfiliado;
                            PorcentajeSeguro = $scope.ListaTipoTarifaCargaVenta[i].PorcentajeSeguro;
                            //$scope.Modelo.Remesa.ValorManejoCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorManejo;
                            //$scope.Modelo.Remesa.ValorSeguroCliente = $scope.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                            Calculo = true;
                            break;
                        }
                    }
                }
                if (Calculo == false) {
                    $scope.TarifarioInvalido = true;
                    ShowError('No hay tarifa disponible para las condiciones especificadas');
                }
                else {
                    $scope.TarifarioInvalido = false;
                }
                $scope.Modelo.Remesa.ValorFleteCliente = ValorFleteCliente;
                $scope.Modelo.Remesa.ValorFleteTransportador = 0;

                $scope.Modelo.Remesa.TotalFleteCliente = Math.round($scope.Modelo.Remesa.ValorFleteCliente);
                $scope.Modelo.FletePactado = ValorFleteCliente;

                //--Manejo Seccion Reexpedicion Por Oficina
                if ($scope.ManejoReexpedicionOficina) {
                    $scope.ValorReexpedicion = 0;
                    $scope.ValorReexpedicionExterna = 0;
                    try {
                        for (var i = 0; i < $scope.ListaReexpedicionOficinas.length; i++) {
                            $scope.ValorReexpedicion += parseFloat(MascaraDecimales($scope.ListaReexpedicionOficinas[i].Valor));
                            if ($scope.ListaReexpedicionOficinas[i].TipoReexpedicion.Codigo == "11102") {
                                $scope.ValorReexpedicionExterna += parseFloat(MascaraDecimales($scope.ListaReexpedicionOficinas[i].Valor));
                            }
                        }
                        $scope.ValorReexpedicion = MascaraValores($scope.ValorReexpedicion)
                        $scope.Modelo.ValorReexpedicion = $scope.ValorReexpedicion
                        if ($scope.ValorReexpedicionExterna > 0) {
                            $scope.Modelo.Remesa.TotalFleteCliente = parseFloat(MascaraDecimales($scope.Modelo.Remesa.TotalFleteCliente)) + parseFloat(MascaraDecimales($scope.ValorReexpedicionExterna))
                        }

                    } catch (e) {
                    }
                }
                else {
                    //--Reexpedicion por tarifa
                    if (detalleTarifa.Reexpedicion > 0 && detalleTarifa.PorcentajeReexpedicion > 0) {
                        ValorFleteCliente = Math.round(((100 - detalleTarifa.PorcentajeReexpedicion) / 100) * detalleTarifa.ValorFlete);
                        $scope.ValorReexpedicion = Math.round((detalleTarifa.PorcentajeReexpedicion / 100) * detalleTarifa.ValorFlete);
                        $scope.Modelo.ValorReexpedicion = $scope.ValorReexpedicion;
                        $scope.Modelo.Remesa.ValorFleteCliente = ValorFleteCliente;
                    }
                }
                //--Manejo Seccion Reexpedicion Por Oficina

                //--Condiciones Comerciales
                if (detalleTarifa.CondicionesComerciales == true) {
                    if (detalleTarifa.Reexpedicion == 0) {//cuando es de reexpedicion no aplica descuentos
                        if ($scope.PesoNormal == true) {
                            Descuento = Math.round(ValorFleteCliente * (detalleTarifa.PorcentajeFlete / 100));//--Porcentaje Flete
                        }
                        else {
                            Descuento = Math.round(ValorFleteCliente * (detalleTarifa.PorcentajeVolumen / 100));//--Porcentaje Volumen
                        }
                        ValorFleteCliente -= Descuento;//--Resta el descuento en valor flete
                    }
                    if (ValorFleteCliente < detalleTarifa.FleteMinimo) {
                        ValorFleteCliente = detalleTarifa.FleteMinimo;
                    }
                    $scope.Modelo.Remesa.ValorFleteCliente = ValorFleteCliente;
                }
                //--Condiciones Comerciales

                //-----Calculos Seguros valor comecial
                if (parseInt(RevertirMV($scope.Modelo.Remesa.ValorComercialCliente)) > 0) {
                    //--Realiza Calculos de seguro para valor comercia cuando exise en la tarifa el valor seguro (Prioridad 1)
                    if (PorcentajeSeguro > 0) {
                        $scope.Modelo.Remesa.ValorSeguroCliente = Math.round((parseInt(MascaraNumero($scope.Modelo.Remesa.ValorComercialCliente)) * PorcentajeSeguro) / 100);
                    }
                    //--Realiza Calculos de seguro para valor comercia cuando exise el valor seguro en oficina y el valor seguro de la tarifa es 0 (Prioridad 2)
                    if (OficinaObj.PorcentajeSeguroPaqueteria > 0 && PorcentajeSeguro == 0) {
                        $scope.Modelo.Remesa.ValorSeguroCliente = Math.round((parseInt(MascaraNumero($scope.Modelo.Remesa.ValorComercialCliente)) * OficinaObj.PorcentajeSeguroPaqueteria) / 100);
                    }
                    //--Valida Valores Minimos Generales
                    switch ($scope.Modelo.LineaNegocioPaqueteria.Codigo) {
                        case CATALOGO_LINEA_NEGOCIO_PAQUETERIA.PAQUETERIA:
                            if ($scope.Modelo.Remesa.ValorSeguroCliente < $scope.PaqueteriaMinimoValorManejo) {
                                $scope.Modelo.Remesa.ValorSeguroCliente = $scope.PaqueteriaMinimoValorManejo;
                            }
                            break;
                        case CATALOGO_LINEA_NEGOCIO_PAQUETERIA.COURIER:
                            if ($scope.Modelo.Remesa.ValorSeguroCliente < $scope.PaqueteriaMinimoValorSeguro) {
                                $scope.Modelo.Remesa.ValorSeguroCliente = $scope.PaqueteriaMinimoValorSeguro;
                            }
                            break;
                    }
                    //--Valor Manejo Para Condiciones Comerciales
                    if (detalleTarifa.CondicionesComerciales == true) {
                        var tmpValorSeguro = Math.round(RevertirMV($scope.Modelo.Remesa.ValorComercialCliente) * (detalleTarifa.PorcentajeValorDeclarado / 100));
                        if (tmpValorSeguro > detalleTarifa.ManejoMinimo) {
                            $scope.Modelo.Remesa.ValorSeguroCliente = tmpValorSeguro;
                        }
                        else {
                            $scope.Modelo.Remesa.ValorSeguroCliente = detalleTarifa.ManejoMinimo;
                        }
                    }
                }
                else {
                    //--Valida Valores Minimos Generales
                    switch ($scope.Modelo.LineaNegocioPaqueteria.Codigo) {
                        case CATALOGO_LINEA_NEGOCIO_PAQUETERIA.PAQUETERIA:
                            $scope.Modelo.Remesa.ValorSeguroCliente = $scope.PaqueteriaMinimoValorManejo;
                            break;
                        case CATALOGO_LINEA_NEGOCIO_PAQUETERIA.COURIER:
                            $scope.Modelo.Remesa.ValorSeguroCliente = $scope.PaqueteriaMinimoValorSeguro;
                            break;
                    }
                }
                //--Realiza Calculos de seguro para valor comercial cuando tarifas paqueteria existe y los demas seguros estan en 0 (Prioridad 3)
                //if ($scope.TarifasPaqueteria == true && PorcentajeSeguro == 0 && OficinaObj.PorcentajeSeguroPaqueteria == 0) {
                //    var valMinimo = Math.round($scope.PaqueteriaMinimoValorSeguro * ($scope.PaqueteriaPorcentajeSeguro / 100));
                //    var valComercial = Math.round(MascaraNumero($scope.Modelo.Remesa.ValorComercialCliente) * ($scope.PaqueteriaPorcentajeSeguro / 100));
                //    if (MascaraNumero($scope.Modelo.Remesa.ValorComercialCliente) > 0) {
                //        if (valComercial > valMinimo) {
                //            $scope.Modelo.Remesa.ValorSeguroCliente = Math.round(MascaraNumero($scope.Modelo.Remesa.ValorComercialCliente) * ($scope.PaqueteriaPorcentajeSeguro / 100));
                //        }
                //        else {
                //            if (valMinimo > 0) {
                //                $scope.Modelo.Remesa.ValorSeguroCliente = valMinimo;
                //            }
                //        }
                //    }
                //    else {
                //        if (valMinimo > 0) {
                //            $scope.Modelo.Remesa.ValorSeguroCliente = valMinimo;
                //        }
                //    }
                //}

                //--Suma Conceptos
                $scope.Modelo.Remesa.TotalFleteCliente = $scope.Modelo.Remesa.ValorFleteCliente + RevertirMV($scope.Modelo.Remesa.ValorOtros) + $scope.Modelo.Remesa.ValorSeguroCliente +
                    RevertirMV($scope.ValorReexpedicion) + RevertirMV($scope.Modelo.Remesa.ValorCargue) + RevertirMV($scope.Modelo.Remesa.ValorDescargue);
                //--Calculos Factura
                $scope.ValorRemesas = $scope.Modelo.Remesa.TotalFleteCliente;
                //--Calculos Factura
            }
            if ($scope.Modelo.Remesa.RemesaCortesia == true) {
                $scope.Modelo.Remesa.ValorFleteCliente = 0;
                $scope.Comision = 0;
                $scope.Modelo.Remesa.ValorSeguroCliente = 0;
                $scope.ValorReexpedicion = 0;
                $scope.Modelo.Remesa.TotalFleteCliente = 0;
            }
            $scope.MaskValores();
        };
        //-----------Calcular----------//
        //-----------------------------Obtener------------------------------//
        function Obtener() {
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Modelo.Numero
            };
            RemesaGuiasFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        $scope.Modelo = angular.copy(response.data.Datos);
                        //--Estado
                        $scope.CodigoEstadoDocumento = response.data.Datos.Estado;
                        if ($scope.CodigoEstadoDocumento == ESTADO_ACTIVO) {
                            $scope.Deshabilitar = true;
                            $scope.DeshabilitarOficinaDestino = true;
                            $scope.BloqueoRemitenteNombre = true;
                            $scope.BloqueoRemitenteIdentificacion = true;
                            $scope.BloqueoDestinatario = true;

                        } else {
                            $scope.Deshabilitar = false;
                            $scope.DeshabilitarOficinaDestino = false;
                            $scope.BloqueoRemitenteNombre = false;
                            $scope.BloqueoRemitenteIdentificacion = false;
                            $scope.BloqueoDestinatario = false;
                        }

                        $scope.Modelo.Remesa.Numero = response.data.Datos.Remesa.Numero;
                        $scope.Modelo.Remesa.NumeroDocumento = response.data.Datos.Remesa.NumeroDocumento;
                        $scope.Modelo.Remesa.Fecha = new Date(response.data.Datos.Remesa.Fecha);
                        $scope.Modelo.Remesa.Numeracion = response.data.Datos.Numeracion;

                        $scope.Modelo.Remesa.RemesaCortesia = response.data.Datos.Remesa.RemesaCortesia == 1 ? true : false;

                        $scope.CodigoTipoEntregaRemesaPaqueteria = response.data.Datos.TipoEntregaRemesaPaqueteria.Codigo;
                        $scope.Modelo.LineaNegocioPaqueteria = $linq.Enumerable().From($scope.ListadoLineaNegocioPaqueteria).First('$.Codigo ==' + response.data.Datos.LineaNegocioPaqueteria.Codigo);
                        $scope.Modelo.RecogerOficinaDestino = response.data.Datos.RecogerOficinaDestino == 1 ? true : false;
                        $scope.ManejoDetalleUnidades = response.data.Datos.ManejaDetalleUnidades == 1 ? true : false;
                        $scope.ListadoDetalleUnidades = response.data.Datos.DetalleUnidades;
                        if ($scope.ManejoDetalleUnidades == true) {
                            if ($scope.ListadoDetalleUnidades != undefined && $scope.ListadoDetalleUnidades != '' && $scope.ListadoDetalleUnidades != null) {
                                for (var i = 0; i < $scope.ListadoDetalleUnidades.length; i++) {
                                    if ($scope.ManejoPesoVolumetrico) {
                                        if ($scope.ListadoDetalleUnidades[i].PesoVolumetrico > $scope.ListadoDetalleUnidades[i].Peso)
                                            $scope.ListadoDetalleUnidades[i].PesoCobrar = $scope.ListadoDetalleUnidades[i].PesoVolumetrico;
                                        else
                                            $scope.ListadoDetalleUnidades[i].PesoCobrar = $scope.ListadoDetalleUnidades[i].Peso;
                                    }
                                    else {
                                        $scope.ListadoDetalleUnidades[i].PesoCobrar = $scope.ListadoDetalleUnidades[i].Peso;
                                    }
                                    $scope.ListadoDetalleUnidades[i].ValorComercial = MascaraValores($scope.ListadoDetalleUnidades[i].ValorComercial);
                                }
                                IndiceDetalleUnidad();
                            }
                        }

                        if ($scope.ListadoTipoEntregaRemesaPaqueteria !== undefined && $scope.ListadoTipoEntregaRemesaPaqueteria !== null) {
                            if ($scope.ListadoTipoEntregaRemesaPaqueteria.length > 0) {
                                $scope.Modelo.TipoEntregaRemesaPaqueteria = $linq.Enumerable().From($scope.ListadoTipoEntregaRemesaPaqueteria).First('$.Codigo ==' + $scope.CodigoTipoEntregaRemesaPaqueteria);
                            }
                        }

                        if (response.data.Datos.Remesa.Cliente.Codigo > 0) {
                            $scope.Modelo.Remesa.Cliente = $scope.CargarTercero(response.data.Datos.Remesa.Cliente.Codigo);
                        }

                        $scope.Modelo.Remesa.NumeroDocumentoCliente = response.data.Datos.Remesa.NumeroDocumentoCliente;


                        ////-- REMITENTE
                        $scope.CodigoTipoIdentificacionRemitente = response.data.Datos.CodigoTipoIdentificacionRemitente;
                        if ($scope.ListadoTipoIdentificacion !== undefined && $scope.ListadoTipoIdentificacion !== null) {
                            if ($scope.ListadoTipoIdentificacion.length > 0) {
                                $scope.Modelo.Remesa.Remitente.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.CodigoTipoIdentificacionRemitente);
                            }
                        }

                        $scope.Modelo.Remesa.Remitente.NumeroIdentificacion = response.data.Datos.Remesa.Remitente.NumeroIdentificacion;
                        $scope.Modelo.Remesa.Remitente.Nombre = response.data.Datos.Remesa.Remitente.Nombre;
                        $scope.Modelo.Remesa.Remitente.Ciudad = $scope.CargarCiudad(response.data.Datos.Remesa.Remitente.Ciudad.Codigo)
                        $scope.Modelo.Remesa.Destinatario.Ciudad = $scope.CargarCiudad(response.data.Datos.Remesa.Destinatario.Ciudad.Codigo)
                        $scope.Modelo.Remesa.BarrioRemitente = response.data.Datos.Remesa.BarrioRemitente;
                        try {
                            $scope.Modelo.Remesa.Remitente.LineaSerivicioCliente = response.data.Datos.LineaSerivicioCliente;
                            $scope.Modelo.Remesa.CentroCosto = response.data.Datos.CentroCosto;
                            if ($scope.ManejoSitiosCargueDescargueGeneral == true) {
                                $scope.Modelo.Remesa.Remitente.SitioCargue = response.data.Datos.SitioCargue;
                            }
                            else {
                                $scope.Modelo.Remesa.Remitente.SitioCargue = { SitioCliente: response.data.Datos.SitioCargue };
                            }

                            if ($scope.ManejoSitiosCargueDescargueGeneral == true) {
                                $scope.Modelo.Remesa.Destinatario.SitioDescargue = response.data.Datos.SitioDescargue;
                            }
                            else {
                                $scope.Modelo.Remesa.Destinatario.SitioDescargue = { SitioCliente: response.data.Datos.SitioDescargue };
                            }


                            $scope.Modelo.Remesa.FechaEntrega = new Date(response.data.Datos.FechaEntrega);
                            $scope.ObtenerRemitenteAlterno($scope.Modelo.Remesa.Remitente.Codigo);
                        } catch (e) {

                        }


                        //Para el remitente y destinatario por ahora se carga en el combo solo la dirección guardada
                        $scope.ListadoDireccionesRemitente = [];
                        $scope.ListadoDireccionesRemitente.push({ Direccion: response.data.Datos.Remesa.Remitente.Direccion })
                        $scope.Modelo.Remesa.Remitente.Direccion = $scope.ListadoDireccionesRemitente[0].Direccion;

                        $scope.ListadoDireccionesDestinatario = [];
                        $scope.ListadoDireccionesDestinatario.push({ Direccion: response.data.Datos.Remesa.Destinatario.Direccion })
                        $scope.Modelo.Remesa.Destinatario.Direccion = $scope.ListadoDireccionesDestinatario[0].Direccion;

                        $scope.Modelo.Remesa.CodigoPostalRemitente = response.data.Datos.Remesa.CodigoPostalRemitente;
                        $scope.Modelo.Remesa.Remitente.Telefonos = response.data.Datos.Remesa.Remitente.Telefonos;
                        $scope.Modelo.Remesa.ObservacionesRemitente = response.data.Datos.Remesa.ObservacionesRemitente;

                        ////-- DESTINATARIO
                        $scope.ObtenerOficinaRecibe();
                        try {
                            $scope.CodigoTipoIdentificacionDestinatario = response.data.Datos.CodigoTipoIdentificacionDestinatario;
                            if ($scope.ListadoTipoIdentificacion !== undefined && $scope.ListadoTipoIdentificacion !== null) {
                                if ($scope.ListadoTipoIdentificacion.length > 0) {
                                    $scope.Modelo.Remesa.Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.CodigoTipoIdentificacionDestinatario);
                                }
                            }
                        } catch (e) {

                        }


                        $scope.Modelo.Remesa.Destinatario.NumeroIdentificacion = response.data.Datos.Remesa.Destinatario.NumeroIdentificacion;
                        $scope.Modelo.Remesa.Destinatario.Nombre = response.data.Datos.Remesa.Destinatario.Nombre;

                        $scope.Modelo.Remesa.BarrioDestinatario = response.data.Datos.Remesa.BarrioDestinatario;

                        $scope.Modelo.Remesa.CodigoPostalDestinatario = response.data.Datos.Remesa.CodigoPostalDestinatario;
                        $scope.Modelo.Remesa.Destinatario.Telefonos = response.data.Datos.Remesa.Destinatario.Telefonos;
                        $scope.Modelo.Remesa.ObservacionesDestinatario = response.data.Datos.Remesa.ObservacionesDestinatario;
                        if ($scope.Modelo.OficinaRecibe.Codigo > 0) {
                            $scope.Modelo.OficinaRecibe = $linq.Enumerable().From($scope.ListadoOficinaRecibe).First('$.Codigo ==' + response.data.Datos.OficinaRecibe.Codigo);
                        }
                        ////--DETALLE
                        $scope.Modelo.Remesa.FormaPago = response.data.Datos.Remesa.FormaPago;
                        if ($scope.ListadoFormaPago !== undefined && $scope.ListadoFormaPago !== null) {
                            if ($scope.ListadoFormaPago.length > 0) {
                                $scope.Modelo.Remesa.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo ==' + $scope.Modelo.Remesa.FormaPago.Codigo);
                            }
                        }

                        $scope.Modelo.Remesa.PesoCliente = response.data.Datos.Remesa.PesoCliente;
                        $scope.Modelo.Remesa.CantidadCliente = response.data.Datos.Remesa.CantidadCliente;
                        $scope.Modelo.Remesa.ValorComercialCliente = response.data.Datos.Remesa.ValorComercialCliente;
                        $scope.Modelo.Remesa.ProductoTransportado = $scope.CargarProducto(response.data.Datos.Remesa.ProductoTransportado.Codigo);

                        $scope.CodigoTarifaCarga = response.data.Datos.CodigoTarifaTransporte;
                        $scope.CodigoTipoTarifaTransporte = response.data.Datos.CodigoTipoTarifaTransporte;
                        $scope.Modelo.DescripcionProductoTransportado = response.data.Datos.DescripcionProductoTransportado;


                        //Arma Tarifa consultada
                        $scope.ListaTarifaCarga = [{ Codigo: 0, Nombre: '(Seleccione Item)' },
                        { Codigo: response.data.Datos.CodigoTarifaTransporte, Nombre: response.data.Datos.NombreTarifa }];
                        $scope.Modelo.Remesa.TarifaCarga = $linq.Enumerable().From($scope.ListaTarifaCarga).First('$.Codigo ==' + $scope.CodigoTarifaCarga);

                        //Arma Tipo de tarifa consultada
                        $scope.ListaTipoTarifaCargaVenta = [
                            { CodigoDetalleTarifa: 0, Codigo: 0, CodigoTarifa: 0, Nombre: '(Seleccione Item)' },
                            { CodigoDetalleTarifa: response.data.Datos.Remesa.DetalleTarifaVenta.Codigo, Codigo: $scope.CodigoTipoTarifaTransporte, CodigoTarifa: $scope.CodigoTarifaCarga, Nombre: response.data.Datos.NombreTipoTarifa },
                        ];
                        $scope.Modelo.Remesa.TipoTarifaCarga = $linq.Enumerable().From($scope.ListaTipoTarifaCargaVenta).First('$.Codigo ==' + $scope.CodigoTipoTarifaTransporte);

                        ////--LIQUIDACION
                        $scope.Modelo.Remesa.ValorFleteTransportador = response.data.Datos.Remesa.ValorFleteTransportador;
                        $scope.Modelo.Remesa.ValorFleteCliente = response.data.Datos.Remesa.ValorFleteCliente;
                        $scope.Modelo.Remesa.ValorManejoCliente = response.data.Datos.Remesa.ValorManejoCliente;
                        $scope.Modelo.Remesa.ValorSeguroCliente = response.data.Datos.Remesa.ValorSeguroCliente;

                        $scope.ValorReexpedicion = response.data.Datos.ValorReexpedicion;
                        $scope.Modelo.Remesa.TotalFleteCliente = response.data.Datos.Remesa.TotalFleteCliente;
                        $scope.Comision = response.data.Datos.Remesa.ValorComisionOficina;
                        $scope.Agencista = response.data.Datos.Remesa.ValorComisionAgencista;

                        $scope.Modelo.Remesa.Observaciones = response.data.Datos.Remesa.Observaciones;

                        ////--INFORMACION DESPACHO
                        $scope.Modelo.OficinaOrigen = response.data.Datos.OficinaOrigen;
                        if ($scope.Modelo.AjusteFlete > 0) {
                            $scope.Modelo.AjusteFlete = true
                        }

                        $scope.Modelo.Oficina = response.data.Datos.Oficina;

                        try {
                            $scope.CodigoOficina = response.data.Datos.OficinaDestino.Codigo;
                            if ($scope.ListaOficinas !== undefined && $scope.ListaOficinas !== null) {
                                $scope.Modelo.OficinaDestino = $linq.Enumerable().From($scope.ListaOficinas).First('$.Codigo ==' + $scope.CodigoOficina);
                            }
                        } catch (e) {

                        }
                        $scope.Modelo.OficinaActual = response.data.Datos.OficinaActual;
                        $scope.Modelo.EstadoGuia = response.data.Datos.EstadoGuia;
                        $scope.Modelo.Remesa.NumeroPlanilla = response.data.Datos.Remesa.NumeroPlanilla;
                        $scope.Modelo.Remesa.Vehiculo = response.data.Datos.Remesa.Vehiculo;
                        $scope.Modelo.Remesa.Conductor = response.data.Datos.Remesa.Conductor;
                        $scope.Modelo.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                        ////-- ESTADO
                        if ($scope.ListaEstadoDocumento !== undefined && $scope.ListaEstadoDocumento !== null) {
                            if ($scope.ListaEstadoDocumento.length > 0) {
                                $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListaEstadoDocumento).First('$.Codigo ==' + response.data.Datos.Estado);
                            }
                        }

                        $scope.TipoRemesa = response.data.Datos.CodigoTipoRemesa;
                        $scope.AplicaReexpedicion = response.data.Datos.AplicaReexpedicion;
                        $scope.Modelo.EstadoGuia = response.data.Datos.EstadoGuia;
                        $scope.ListaReexpedicionOficinas = response.data.Datos.ListaReexpedicionOficinas
                        if ($scope.ManejoReexpedicionOficina) {
                            $scope.ValorReexpedicion = 0
                            try {
                                for (var i = 0; i < $scope.ListaReexpedicionOficinas.length; i++) {
                                    $scope.ValorReexpedicion += parseFloat(MascaraDecimales($scope.ListaReexpedicionOficinas[i].Valor))
                                }
                                $scope.ValorReexpedicion = MascaraValores($scope.ValorReexpedicion)
                            } catch (e) {
                            }
                        }


                        //--Carga de Zona, Posicion, Horarios Destinatario
                        if (response.data.Datos.CodigoZona > 0) {
                            try {
                                $scope.Modelo.Remesa.Zona = $linq.Enumerable().From($scope.ListadoZonas).First('$.Codigo ==' + response.data.Datos.CodigoZona);
                            } catch (e) {
                                $scope.Modelo.Remesa.Zona = $linq.Enumerable().From($scope.ListadoZonas).First('$.Codigo ==-1');
                            }

                        }
                        $scope.Modelo.Remesa.Latitud = response.data.Datos.Latitud;
                        $scope.Modelo.Remesa.Longitud = response.data.Datos.Longitud;
                        if (response.data.Datos.HorarioEntregaRemesa.Codigo > 0) {
                            try {
                                $scope.Modelo.Remesa.HorarioEntrega = $linq.Enumerable().From($scope.ListadoHorariosEntrega).First('$.Codigo ==' + response.data.Datos.HorarioEntregaRemesa.Codigo);
                            } catch (e) {
                                $scope.Modelo.Remesa.HorarioEntrega = ListadoHorariosEntrega[0];
                            }

                        }


                        $scope.MaskValores();
                    }
                    else {
                        ShowError('No se logro consultar el tarifario ventas código ' + $scope.Modelo.Numero + '. ' + response.data.MensajeOperacion);
                        document.location.href = $scope.Master;
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = $scope.Master;
                });
        }
        //-----------------------------Obtener------------------------------//
        //----------------------------Guardar----------------------------//
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            var modelo = $scope.Modelo
            var dateEntMin = new Date();
            dateEntMin.setDate(dateEntMin.getDate() - 1);
            if (modelo.Remesa.FormaPago == undefined || modelo.Remesa.FormaPago == null || modelo.Remesa.FormaPago == '') {
                $scope.MensajesError.push('Debe ingresar la forma de pago');
                continuar = false;
            }
            else if (modelo.Remesa.FormaPago.Codigo == 6100) {
                $scope.MensajesError.push('Debe ingresar la forma de pago');
                continuar = false;
            }

            if ($scope.Modelo.Remesa.Remitente.Ciudad !== undefined && $scope.Modelo.Remesa.Remitente.Ciudad !== null && $scope.Modelo.Remesa.Destinatario.Ciudad !== undefined && $scope.Modelo.Remesa.Destinatario.Ciudad !== null) {
                if ($scope.Modelo.Remesa.Remitente.Ciudad.Codigo > 0 && $scope.Modelo.Remesa.Destinatario.Ciudad.Codigo > 0) {
                    if ($scope.Modelo.Remesa.Remitente.Ciudad.Codigo !== $scope.Modelo.Remesa.Destinatario.Ciudad.Codigo) {
                        $scope.TipoRemesa = CODIGO_TIPO_REMESA_NACIONAL;
                    } else {
                        $scope.TipoRemesa = CODIGO_TIPO_REMESA_URBANA;
                    }
                } else {
                    $scope.MensajesError.push('Debe seleccionar la ciudad para el remitente y el destinatario');
                    continuar = false;
                }

            } else {
                $scope.MensajesError.push('No hay ciudades seleccionadas para el remitente y/o destinatario');
                continuar = false;
            }

            if (ValidarCampo(modelo.Remesa.ProductoTransportado) == OBJETO_SIN_DATOS) {
                $scope.MensajesError.push('Debe ingresar el producto ');
                continuar = false;
            } else if (modelo.Remesa.ProductoTransportado.Codigo == undefined || modelo.Remesa.ProductoTransportado.Codigo == 0) {
                if (ValidarCampo(modelo.Remesa.DescripcionProductoTransportado) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe ingresar la descripción del producto');
                    continuar = false;
                }
            }

            if (ValidarCampo(modelo.PesoCobrar) == OBJETO_SIN_DATOS) {
                $scope.MensajesError.push('Debe ingresar el peso o las dimensiones');
                continuar = false;
            }
            if (ValidarCampo(modelo.Remesa.ValorComercialCliente) == OBJETO_SIN_DATOS) {
                $scope.MensajesError.push('Debe ingresar el valor comercial');
                continuar = false;
            }
            if (ValidarCampo(modelo.Remesa.CantidadCliente) == OBJETO_SIN_DATOS) {
                $scope.MensajesError.push('Debe ingresar la cantidad');
                continuar = false;
            }

            if (modelo.TipoEntregaRemesaPaqueteria.Codigo == 6600) {
                $scope.MensajesError.push('Debe seleccionar el tipo de entrega');
                continuar = false;
            }

            if (modelo.Remesa.Remitente.Codigo == 0 || modelo.Remesa.Remitente.Codigo == undefined) {
                if (ValidarCampo(modelo.Remesa.Remitente.NumeroIdentificacion) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe ingresar el número de identificación del remitente');
                    continuar = false;
                }
                if (modelo.Remesa.Remitente.TipoIdentificacion.Codigo == 100) {
                    $scope.MensajesError.push('Debe seleccionar el tipo de identificación del remitente');
                    continuar = false;
                }
                if (ValidarCampo(modelo.Remesa.Remitente.Nombre) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe ingresar el nombre del remitente');
                    continuar = false;
                }
                if (ValidarCampo(modelo.Remesa.Remitente.Ciudad) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe seleccionar la ciudad del remitente');
                    continuar = false;
                }
                if (ValidarCampo(modelo.Remesa.Remitente.Direccion) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe seleccionar la dirección del remitente');
                    continuar = false;
                }
                if (ValidarCampo(modelo.Remesa.Remitente.Telefonos) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe ingresar el teléfono del remitente');
                    continuar = false;
                }

            }
            if (modelo.Remesa.Destinatario.Codigo == 0 || modelo.Remesa.Destinatario.Codigo == undefined) {
                if (ValidarCampo(modelo.Remesa.Destinatario.NumeroIdentificacion) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe ingresar el número de identificación del destinatario');
                    continuar = false;
                }
                if (modelo.Remesa.Destinatario.TipoIdentificacion.Codigo == 100) {
                    $scope.MensajesError.push('Debe seleccionar el tipo de identificación del destinatario');
                    continuar = false;
                }
                if (ValidarCampo(modelo.Remesa.Destinatario.Nombre) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe ingresar el nombre del destinatario');
                    continuar = false;
                }
                if (ValidarCampo(modelo.Remesa.Destinatario.Ciudad) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe seleccionar la ciudad del destinatario');
                    continuar = false;
                }
                if (ValidarCampo(modelo.Remesa.Destinatario.Direccion) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe seleccionar la dirección del destinatario');
                    continuar = false;
                }
                if (ValidarCampo(modelo.Remesa.Destinatario.Telefonos) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe ingresar el teléfono del destinatario');
                    continuar = false;
                }
                if (ValidarCampo(modelo.Remesa.Destinatario.SitioDescargue) == OBJETO_SIN_DATOS && modelo.Remesa.Destinatario.Codigo > 0) {
                    $scope.MensajesError.push('Debe seleccionar el sitio de entrega');
                    continuar = false;
                }
            }
            if (modelo.Remesa.FechaEntrega == undefined || modelo.Remesa.FechaEntrega == '' || modelo.Remesa.FechaEntrega == null) {
                $scope.MensajesError.push('Debe ingresar la fecha de entrega');
                continuar = false;
            }
            else if (modelo.Remesa.FechaEntrega < dateEntMin) {
                $scope.MensajesError.push('la fecha entrega debe ser posterior a la fecha actual');
                continuar = false;
            }
            if (ValidarCampo(modelo.Remesa.ValorFleteCliente) == OBJETO_SIN_DATOS && $scope.Modelo.Remesa.RemesaCortesia == false) {
                $scope.MensajesError.push('No hay un flete asignado con las condiciones especificadas');
                continuar = false;
            }
            return continuar;
        }
        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        };
        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {

                $scope.Modelo.Remesa.Fecha = RetornarFechaEspecificaSinTimeZone($scope.Modelo.Remesa.Fecha);
                if ($scope.Modelo.Remesa.ProductoTransportado.Codigo == undefined || $scope.Modelo.Remesa.ProductoTransportado.Codigo == 0) {
                    $scope.Modelo.Remesa.ProductoTransportado = { Nombre: $scope.Modelo.Remesa.ProductoTransportado, Descripcion: $scope.Modelo.Remesa.DescripcionProductoTransportado };
                }
                var ObtjetoRemesa = {

                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: $scope.Modelo.Remesa.Numero,
                    TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA },

                    TipoRemesa: { Codigo: $scope.TipoRemesa },
                    RemesaCortesia: $scope.Modelo.Remesa.RemesaCortesia == true ? 1 : 0,
                    NumeroDocumentoCliente: $scope.Modelo.Remesa.NumeroDocumentoCliente,
                    FechaDocumentoCliente: MIN_DATE,
                    Fecha: $scope.Modelo.Remesa.Fecha,
                    Ruta: { Codigo: 0 },
                    ProductoTransportado: $scope.Modelo.Remesa.ProductoTransportado,
                    FormaPago: $scope.Modelo.Remesa.FormaPago,
                    Cliente: $scope.Modelo.Remesa.Cliente,
                    Remitente: $scope.Modelo.Remesa.Remitente,

                    CiudadRemitente: { Codigo: $scope.Modelo.Remesa.Remitente.Ciudad.Codigo },
                    DireccionRemitente: $scope.Modelo.Remesa.Remitente.Direccion,
                    TelefonoRemitente: $scope.Modelo.Remesa.Remitente.Telefonos,
                    Observaciones: $scope.Modelo.Remesa.Observaciones,

                    CantidadCliente: $scope.Modelo.Remesa.CantidadCliente,
                    PesoCliente: RevertirMV($scope.Modelo.Remesa.PesoCliente),
                    PesoVolumetricoCliente: $scope.Modelo.Remesa.PesoVolumetricoCliente,

                    ValorFleteCliente: $scope.Modelo.Remesa.ValorFleteCliente,
                    ValorFleteTransportador: $scope.Modelo.Remesa.ValorFleteTransportador,
                    ValorManejoCliente: $scope.Modelo.Remesa.ValorManejoCliente,
                    ValorSeguroCliente: $scope.Modelo.Remesa.ValorSeguroCliente,
                    ValorDescuentoCliente: 0,
                    TotalFleteCliente: $scope.Modelo.Remesa.TotalFleteCliente,
                    ValorComercialCliente: $scope.Modelo.Remesa.ValorComercialCliente,

                    CantidadTransportador: $scope.Modelo.Remesa.CantidadTransportador,
                    PesoTransportador: $scope.Modelo.Remesa.PesoTransportador,
                    //ValorFleteTransportador: 0,
                    TotalFleteTransportador: $scope.Modelo.Remesa.TotalFleteTransportador,
                    ValorComisionOficina: $scope.Comision,
                    ValorComisionAgencista: $scope.Agencista,
                    ValorOtros: $scope.Modelo.Remesa.ValorOtros,
                    ValorCargue: $scope.Modelo.Remesa.ValorCargue,
                    ValorDescargue: $scope.Modelo.Remesa.ValorDescargue,

                    Destinatario: {
                        Codigo: $scope.Modelo.Remesa.Destinatario.Codigo,
                        Ciudad: $scope.Modelo.Remesa.Destinatario.Ciudad,
                        TipoIdentificacion: $scope.Modelo.Remesa.Destinatario.TipoIdentificacion,
                        Direccion: $scope.Modelo.Remesa.Destinatario.Direccion,
                        Telefonos: $scope.Modelo.Remesa.Destinatario.Telefonos,
                        NumeroIdentificacion: $scope.Modelo.Remesa.Destinatario.NumeroIdentificacion,
                        Nombre: $scope.Modelo.Remesa.Destinatario.Nombre,
                        Barrio: $scope.Modelo.Remesa.BarrioDestinatario,
                        CorreoFacturacion: $scope.Modelo.Remesa.Destinatario.CorreoFacturacion
                    },
                    CiudadDestinatario: { Codigo: $scope.Modelo.Remesa.Destinatario.Ciudad.Codigo },
                    DireccionDestinatario: $scope.Modelo.Remesa.Destinatario.Direccion,
                    TelefonoDestinatario: $scope.Modelo.Remesa.Destinatario.Telefonos,

                    Estado: $scope.Modelo.Estado.Codigo,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },

                    Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },

                    TarifarioVenta: { Codigo: $scope.Modelo.Remesa.NumeroTarifarioVenta },
                    NumeroOrdenServicio: 0,

                    BarrioRemitente: $scope.Modelo.Remesa.BarrioRemitente,
                    CodigoPostalRemitente: $scope.Modelo.Remesa.CodigoPostalRemitente,
                    BarrioDestinatario: $scope.Modelo.Remesa.BarrioDestinatario,
                    CodigoPostalDestinatario: $scope.Modelo.Remesa.CodigoPostalDestinatario,
                    DetalleTarifaVenta: { Codigo: $scope.Modelo.Remesa.TipoTarifaCarga.CodigoDetalleTarifa }
                };

                $scope.AplicaReexpedicion = 0;
                if ($scope.ListaReexpedicionOficinas !== undefined && $scope.ListaReexpedicionOficinas !== null) {
                    if ($scope.ListaReexpedicionOficinas.length > 0) {
                        $scope.AplicaReexpedicion = 1;
                    }
                }

                var ObtjetoFactura = {};

                if (($scope.Modelo.Remesa.FormaPago.Codigo == CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CONTADO ||
                    $scope.Modelo.Remesa.FormaPago.Codigo == CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CONTRA_ENTREGA) && $scope.Modelo.Remesa.RemesaCortesia == false) {
                    var ClienteFactura = 0;
                    if ($scope.Modelo.Remesa.Cliente == undefined || $scope.Modelo.Remesa.Cliente == '' || $scope.Modelo.Remesa.Cliente == null) {
                        ClienteFactura = $scope.Modelo.Remesa.Remitente.Codigo;
                    }
                    else {
                        ClienteFactura = $scope.Modelo.Remesa.Cliente.Codigo;
                    }
                    ObtjetoFactura = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        TipoDocumento: CODIGO_TIPO_DOCUMENTO_FACTURAS,
                        NumeroPreImpreso: CERO,
                        CataTipoFactura: CODIGO_CATALOGO_TIPO_FACTURA,
                        Fecha: $scope.Modelo.Remesa.Fecha,
                        OficinaFactura: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                        Cliente: { Codigo: ClienteFactura },
                        FacturarA: { Codigo: ClienteFactura },
                        FormaPago: { Codigo: $scope.Modelo.Remesa.FormaPago.Codigo },
                        DiasPlazo: CERO,
                        Observaciones: "",

                        ValorRemesas: $scope.ValorRemesas,
                        ValorOtrosConceptos: CERO,
                        ValorDescuentos: CERO,
                        Subtotal: $scope.ValorRemesas,
                        ValorImpuestos: 0,
                        ValorFactura: $scope.ValorRemesas,
                        ValorTRM: CERO,
                        Moneda: { Codigo: 1 },
                        ValorMonedaAlterna: CERO,
                        ValorAnticipo: CERO,
                        ResolucionFacturacion: $scope.Sesion.UsuarioAutenticado.Oficinas.ResolucionFacturacion,
                        ControlImpresion: 1,
                        Estado: $scope.Modelo.Estado.Codigo,
                        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                        Anulado: CERO
                    };
                }

                //--Manejo Unidades
                var ListaDetalleUnidades = [];
                for (var i = 0; i < $scope.ListadoDetalleUnidades.length; i++) {
                    var detalle = $scope.ListadoDetalleUnidades[i];
                    ListaDetalleUnidades.push({
                        Producto: { Codigo: detalle.Producto.Codigo },
                        Referencia: detalle.Referencia,
                        Peso: detalle.Peso,
                        Largo: detalle.Largo,
                        Alto: detalle.Alto,
                        Ancho: detalle.Ancho,
                        PesoVolumetrico: detalle.PesoVolumetrico,
                        PesoCobrar: detalle.PesoCobrar,
                        ValorComercial: detalle.ValorComercial,
                        DescripcionProducto: detalle.DescripcionProducto
                    });
                }
                //--Manejo Unidades

                $scope.RemesaPaqueteria = {
                    CodigoEmpresa: $scope.Modelo.CodigoEmpresa,
                    Remesa: ObtjetoRemesa,
                    Factura: ObtjetoFactura,
                    TransportadorExterno: { Codigo: 0 },
                    NumeroGuiaExterna: '',
                    TipoTransporteRemsaPaqueteria: $scope.Modelo.TipoTransporteRemsaPaqueteria,
                    TipoDespachoRemesaPaqueteria: $scope.Modelo.TipoDespachoRemesaPaqueteria,
                    TemperaturaProductoRemesaPaqueteria: $scope.Modelo.TemperaturaProductoRemesaPaqueteria,
                    TipoServicioRemesaPaqueteria: $scope.Modelo.TipoServicioRemesaPaqueteria,
                    TipoEntregaRemesaPaqueteria: $scope.Modelo.TipoEntregaRemesaPaqueteria,

                    TipoInterfazRemesaPaqueteria: $scope.Modelo.TipoInterfazRemesaPaqueteria,
                    FechaInterfazTracking: $scope.Modelo.FechaInterfaz, /// ----> fecha interfaz
                    FechaInterfazWMS: $scope.Modelo.FechaInterfaz, /////
                    UsuarioCrea: $scope.Modelo.UsuarioCrea,
                    Oficina: $scope.Modelo.Oficina,
               

                    EstadoRemesaPaqueteria: $scope.Modelo.EstadoGuia,

                    ObservacionesDestinatario: $scope.Modelo.Remesa.ObservacionesDestinatario,
                    ObservacionesRemitente: $scope.Modelo.Remesa.ObservacionesRemitente,

                    OficinaOrigen: { Codigo: $scope.Modelo.OficinaOrigen.Codigo },
                    OficinaDestino: { Codigo: $scope.Modelo.OficinaDestino.Codigo },
                    OficinaActual: { Codigo: $scope.Modelo.OficinaActual.Codigo },
                    DesripcionMercancia: $scope.Modelo.DescripcionProductoTransportado,
                    Reexpedicion: $scope.AplicaReexpedicion,
                    ListaReexpedicionOficinas: $scope.ListaReexpedicionOficinas,

                    //LineaSerivicioCliente: { Codigo: $scope.Modelo.Remesa.Remitente.LineaSerivicioCliente.Codigo },
                    CentroCosto: $scope.Modelo.Remesa.CentroCosto,
                    //SitioCargue: { Codigo: $scope.Modelo.Remesa.Remitente.SitioCargue.Codigo },
                    //SitioDescargue: { Codigo: $scope.Modelo.Remesa.Destinatario.SitioDescargue.Codigo },
                    FechaEntrega: $scope.Modelo.Remesa.FechaEntrega,
                    ValorReexpedicion: $scope.Modelo.ValorReexpedicion,
                    Largo: $scope.Modelo.Largo,
                    Alto: $scope.Modelo.Alto,
                    Ancho: $scope.Modelo.Ancho,
                    PesoVolumetrico: $scope.Modelo.PesoVolumetrico,
                    PesoCobrar: $scope.Modelo.PesoCobrar,
                    FletePactado: $scope.Modelo.FletePactado,
                    AjusteFlete: $scope.Modelo.AjusteFlete == true ? 1 : 0,
                    Numeracion: $scope.Modelo.Remesa.Numeracion,
                    LineaNegocioPaqueteria: { Codigo: $scope.Modelo.LineaNegocioPaqueteria.Codigo },
                    ManejaDetalleUnidades: $scope.ManejoDetalleUnidades == true ? 1 : 0,
                    RecogerOficinaDestino: $scope.Modelo.RecogerOficinaDestino == true ? 1 : 0,
                    OficinaRecibe: $scope.Modelo.OficinaRecibe,
                    DetalleUnidades: ListaDetalleUnidades,
                    CodigoZona: $scope.Modelo.Remesa.Zona.Codigo,
                    Latitud: $scope.Modelo.Remesa.Latitud,
                    Longitud: $scope.Modelo.Remesa.Longitud,
                    HorarioEntregaRemesa: { Codigo: $scope.Modelo.Remesa.HorarioEntrega.Codigo }
                };
                try {
                    if ($scope.Modelo.Remesa.Remitente.LineaSerivicioCliente !== undefined && $scope.Modelo.Remesa.Remitente.LineaSerivicioCliente !== '' && $scope.Modelo.Remesa.Remitente.LineaSerivicioCliente !== null) {
                        $scope.RemesaPaqueteria.LineaSerivicioCliente = { Codigo: $scope.Modelo.Remesa.Remitente.LineaSerivicioCliente.Codigo };
                    }
                } catch (e) {

                }
                try {
                    if ($scope.Modelo.Remesa.Remitente.SitioCargue !== undefined && $scope.Modelo.Remesa.Remitente.SitioCargue !== '' && $scope.Modelo.Remesa.Remitente.SitioCargue !== null) {
                        $scope.RemesaPaqueteria.SitioCargue = { Codigo: $scope.Modelo.Remesa.Remitente.SitioCargue.Codigo };
                    }
                } catch (e) {

                }
                try {
                    if ($scope.Modelo.Remesa.Destinatario.SitioDescargue !== undefined && $scope.Modelo.Remesa.Destinatario.SitioDescargue !== '' && $scope.Modelo.Remesa.Destinatario.SitioDescargue !== null) {
                        $scope.RemesaPaqueteria.SitioDescargue = { Codigo: $scope.Modelo.Remesa.Destinatario.SitioDescargue.Codigo };
                    }
                } catch (e) {

                }


                RemesaGuiasFactory.Guardar($scope.RemesaPaqueteria).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                //if ($scope.Modelo.Numero == 0) {
                                //    ShowSuccess('Se guardó la remesa con el número ' + response.data.Datos + '');
                                //}
                                //else {
                                //    ShowSuccess('Se modificó la remesa con el número ' + response.data.Datos + '');
                                //}
                                //--Proceso Adicionar Guia Planilla
                                var responAsocioRemPlan = RemesaGuiasFactory.AsociarGuiaPlanilla({
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                    UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                    Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,
                                    RemesaPaqueteria: [{
                                        Numero: response.data.Numero,
                                     
                                        NumeroDocumento: response.data.Datos
                                    }],
                                    NumeroPlanilla: $scope.PlanillaPaqueteria.Planilla.Numero,
                                    Sync: true
                                });
                                if (responAsocioRemPlan.ProcesoExitoso == true) {
                                    ShowSuccess('Se guardó la guía con el número ' + response.data.Datos + ' y se asoció a la planilla N. ' + $scope.PlanillaPaqueteria.Planilla.NumeroDocumento);
                                }
                                else {
                                    ShowSuccess('Se guardó la guía con el número ' + response.data.Datos);
                                }
                            }
                            else {
                                ShowError(response.statusText);
                            }
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };
        //----------------------------Guardar----------------------------//
        //--Direcciones Remitente
        $scope.GuardarDireccionRemitente = function () {
            if ($scope.Modal.Ciudad == undefined || $scope.Modal.Ciudad == null || $scope.Modal.Ciudad == '' || $scope.Modal.Direccion == undefined || $scope.Modal.Direccion == null || $scope.Modal.Direccion == '') {
                ShowError('Debe ingresar la cuidad y la dirección');
            } else {
                $scope.Entidad = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Tercero: { Codigo: $scope.Modelo.Remesa.Remitente.Codigo },
                    Direccion: $scope.Modal.Direccion,
                    Telefonos: $scope.Modal.Telefonos,
                    Ciudad: $scope.Modal.Ciudad,
                    Barrio: $scope.Modal.Barrio,
                    CodigoPostal: $scope.Modal.CodigoPostal
                };
                TercerosFactory.InsertarDirecciones($scope.Entidad).then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoDireccionesRemitente.push($scope.Entidad);
                        $scope.Modelo.Remesa.Remitente.Direccion = $scope.ListadoDireccionesRemitente[($scope.ListadoDireccionesRemitente.length - 1)];
                        if ($scope.Modelo.Remesa.Destinatario.Codigo == $scope.Modelo.Remesa.Remitente.Codigo) {
                            $scope.ListadoDireccionesDestinatario.push($scope.Entidad);
                            $scope.Modelo.Remesa.Destinatario.Direccion = $scope.ListadoDireccionesDestinatario[($scope.ListadoDireccionesDestinatario.length - 1)];
                        }
                        closeModal('modalAdicionarDireccionRemitente');
                    } else {
                        ShowError('No se pudo guardar la dirección');
                    }
                });

            }
        };
        //--Direcciones Remitente
        //--Direcciones Destinatario
        $scope.GuardarDireccionDestinatario = function () {
            if ($scope.Modal.Ciudad == undefined || $scope.Modal.Ciudad == null || $scope.Modal.Ciudad == '' || $scope.Modal.Direccion == undefined || $scope.Modal.Direccion == null || $scope.Modal.Direccion == '') {
                ShowError('Debe ingresar la cuidad y la dirección');
            } else {
                $scope.Entidad = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Tercero: { Codigo: $scope.Modelo.Remesa.Destinatario.Codigo },
                    Direccion: $scope.Modal.Direccion,
                    Telefonos: $scope.Modal.Telefonos,
                    Ciudad: $scope.Modal.Ciudad,
                    Barrio: $scope.Modal.Barrio,
                    CodigoPostal: $scope.Modal.CodigoPostal
                }
                TercerosFactory.InsertarDirecciones($scope.Entidad).then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoDireccionesDestinatario.push($scope.Entidad);
                        $scope.Modelo.Remesa.Destinatario.Direccion = $scope.ListadoDireccionesDestinatario[($scope.ListadoDireccionesDestinatario.length - 1)];
                        if ($scope.Modelo.Remesa.Destinatario.Codigo == $scope.Modelo.Remesa.Remitente.Codigo) {
                            $scope.ListadoDireccionesRemitente.push($scope.Entidad);
                            $scope.Modelo.Remesa.Remitente.Direccion = $scope.ListadoDireccionesRemitente[($scope.ListadoDireccionesRemitente.length - 1)];
                        }
                        closeModal('modalAdicionarDireccionDestinatario');
                    } else {
                        ShowError('No se pudo guardar la dirección');
                    }
                });

            }
        }
        //--Direcciones Destinatario
        //--Adicionar Oficina
        $scope.AdicionarOficinas = function () {
            if ($scope.Modelo.Remesa.Oficina == undefined || $scope.Modelo.Remesa.Oficina == null || $scope.Modelo.Remesa.Oficina == '' ||
                $scope.Modelo.Remesa.TipoReexpedicion == undefined || $scope.Modelo.Remesa.TipoReexpedicion == null || $scope.Modelo.Remesa.TipoReexpedicion == ''

            ) {
                ShowError('Debe ingresar la oficina, el tipo de reexpedición')
            } else {
                if ($scope.Modelo.Remesa.TipoReexpedicion.Codigo == 11102 && ($scope.Modelo.Remesa.ProveedorExterno == undefined || $scope.Modelo.Remesa.ProveedorExterno == null || $scope.Modelo.Remesa.ProveedorExterno == '')) {
                    ShowError('Debe ingresar el proveedor externo')
                }
                else {
                    if ($scope.Modelo.Remesa.Valor == undefined || $scope.Modelo.Remesa.Valor == null || $scope.Modelo.Remesa.Valor == '' || $scope.Modelo.Remesa.Valor == 0 || $scope.Modelo.Remesa.Valor == '0') {
                        $scope.Modelo.Remesa.Valor = 0
                    }
                    try {
                        if ($scope.ListaReexpedicionOficinas.length > 0) {
                            var cont = 0
                            for (var i = 0; i < $scope.ListaReexpedicionOficinas.length; i++) {
                                if ($scope.ListaReexpedicionOficinas[i].Oficina.Codigo == $scope.Modelo.Remesa.Oficina.Codigo) {
                                    cont++
                                    break;
                                }
                            }
                            if (cont > 0) {
                                ShowError('La oficina que intenta ingresar ya se encuentra registrada')
                                $scope.Modelo.Remesa.Oficina = ''
                                $scope.Modelo.Remesa.Valor = ''
                            } else {
                                if ($scope.Modelo.Remesa.RemesaCortesia == true) {
                                    $scope.Modelo.Remesa.Valor = 0;
                                }
                                $scope.ListaReexpedicionOficinas.push({
                                    Oficina: $scope.Modelo.Remesa.Oficina,
                                    TipoReexpedicion: $scope.Modelo.Remesa.TipoReexpedicion,
                                    Valor: $scope.Modelo.Remesa.Valor,
                                    ProveedorExterno: $scope.Modelo.Remesa.ProveedorExterno,
                                    Observaciones: $scope.Modelo.Remesa.ObservacionesReexpedicion,
                                    Estado: MascaraNumero($scope.Modelo.Remesa.Valor) > 0 ? 1 : 0
                                });
                                $scope.Modelo.Remesa.Oficina = ''
                                $scope.Modelo.Remesa.Valor = ''
                                $scope.Modelo.Remesa.ObservacionesReexpedicion = ''
                                $scope.Modelo.Remesa.ProveedorExterno = ''

                            }
                        } else {
                            $scope.ListaReexpedicionOficinas = []
                            if ($scope.Modelo.Remesa.RemesaCortesia == true) {
                                $scope.Modelo.Remesa.Valor = 0;
                            }
                            $scope.ListaReexpedicionOficinas.push({
                                Oficina: $scope.Modelo.Remesa.Oficina,
                                TipoReexpedicion: $scope.Modelo.Remesa.TipoReexpedicion,
                                Valor: $scope.Modelo.Remesa.Valor,
                                ProveedorExterno: $scope.Modelo.Remesa.ProveedorExterno,
                                Observaciones: $scope.Modelo.Remesa.ObservacionesReexpedicion,
                                Estado: MascaraNumero($scope.Modelo.Remesa.Valor) > 0 ? 1 : 0
                            });
                            $scope.Modelo.Remesa.Oficina = ''
                            $scope.Modelo.Remesa.Valor = ''
                            $scope.Modelo.Remesa.ObservacionesReexpedicion = ''
                            $scope.Modelo.Remesa.ProveedorExterno = ''
                        }
                    } catch (e) {
                        if ($scope.Modelo.Remesa.RemesaCortesia == true) {
                            $scope.Modelo.Remesa.Valor = 0;
                        }
                        $scope.ListaReexpedicionOficinas = [];
                        $scope.ListaReexpedicionOficinas.push({
                            Oficina: $scope.Modelo.Remesa.Oficina,
                            TipoReexpedicion: $scope.Modelo.Remesa.TipoReexpedicion,
                            Valor: $scope.Modelo.Remesa.Valor,
                            ProveedorExterno: $scope.Modelo.Remesa.ProveedorExterno,
                            Observaciones: $scope.Modelo.Remesa.ObservacionesReexpedicion,
                            Estado: MascaraNumero($scope.Modelo.Remesa.Valor) > 0 ? 1 : 0
                        });
                        $scope.Modelo.Remesa.Oficina = ''
                        $scope.Modelo.Remesa.Valor = ''
                        $scope.Modelo.Remesa.ObservacionesReexpedicion = ''
                        $scope.Modelo.Remesa.ProveedorExterno = ''
                    }
                }
            }
            $scope.Calcular();
        };
        //--Adicionar Oficina
        //--Reexpedicion
        $scope.EliminarOficinaReexpedicion = function (i) {
            $scope.ListaReexpedicionOficinas.splice(i, 1);
            $scope.Calcular();
        };
        //--Reexpedicion
        //--Calcular Ajuste Flete
        $scope.CalcularAjusteFlete = function () {
            if (TMPValorFleteCliente !== RevertirMV($scope.Modelo.Remesa.ValorFleteCliente)) {
                var valMin = RevertirMV($scope.Modelo.FletePactado) - (RevertirMV($scope.Modelo.FletePactado) * 0.4);
                if (RevertirMV($scope.Modelo.Remesa.ValorFleteCliente) < valMin) {
                    ShowError("El valor del flete ajustado no puede ser inferior 40% al flete acordado");
                    $scope.Modelo.Remesa.ValorFleteCliente = MascaraValores($scope.Modelo.FletePactado);
                }

                if (OficinaObj.TipoOficina.Codigo == TIPO_OFICINA_RECEPTORIA) {
                    $scope.Comision = Math.round((RevertirMV($scope.Modelo.Remesa.ValorFleteCliente) * OficinaObj.Porcentaje) / 100);
                    $scope.Agencista = Math.round((RevertirMV($scope.Modelo.Remesa.ValorFleteCliente) * OficinaObj.PorcentajeAgencista) / 100);
                    $scope.Modelo.Remesa.ValorFleteCliente = parseFloat((RevertirMV($scope.Modelo.Remesa.ValorFleteCliente) - $scope.Comision).toFixed(2));
                }

                TMPValorFleteCliente = $scope.Modelo.Remesa.ValorFleteCliente;
                $scope.Modelo.Remesa.ValorFleteTransportador = ($scope.Modelo.Remesa.ValorFleteCliente * TMPPorcentajeAfiliado) / 100;

                $scope.Modelo.Remesa.TotalFleteCliente = RevertirMV($scope.Modelo.Remesa.ValorFleteCliente);
                if (RevertirMV($scope.Modelo.Remesa.ValorManejoCliente) > 0) {
                    $scope.Modelo.Remesa.TotalFleteCliente += RevertirMV($scope.Modelo.Remesa.ValorManejoCliente);
                }
                if (RevertirMV($scope.Modelo.Remesa.ValorSeguroCliente) > 0) {
                    $scope.Modelo.Remesa.TotalFleteCliente += RevertirMV($scope.Modelo.Remesa.ValorSeguroCliente);
                }
                if ($scope.Comision > 0) {
                    $scope.Modelo.Remesa.TotalFleteCliente += $scope.Comision;
                }

                //--Manejo Seccion Reexpedicion Por Oficina
                if ($scope.ManejoReexpedicionOficina) {
                    $scope.ValorReexpedicion = 0;
                    $scope.ValorReexpedicionExterna = 0;
                    try {
                        for (var i = 0; i < $scope.ListaReexpedicionOficinas.length; i++) {
                            $scope.ValorReexpedicion += parseFloat(MascaraDecimales($scope.ListaReexpedicionOficinas[i].Valor));
                            if ($scope.ListaReexpedicionOficinas[i].TipoReexpedicion.Codigo == "11102") {
                                $scope.ValorReexpedicionExterna += parseFloat(MascaraDecimales($scope.ListaReexpedicionOficinas[i].Valor));
                            }
                        }
                        $scope.ValorReexpedicion = MascaraValores($scope.ValorReexpedicion);
                        $scope.Modelo.ValorReexpedicion = RevertirMV($scope.ValorReexpedicion);
                        if ($scope.ValorReexpedicionExterna > 0) {
                            $scope.Modelo.Remesa.TotalFleteCliente = parseFloat(MascaraDecimales($scope.Modelo.Remesa.TotalFleteCliente)) + parseFloat(MascaraDecimales($scope.ValorReexpedicionExterna));
                        }
                    } catch (e) {
                    }
                }
                //--Manejo Seccion Reexpedicion Por Oficina
                if ($scope.Modelo.Remesa.RemesaCortesia == true) {
                    $scope.Modelo.Remesa.ValorFleteCliente = 0;
                    $scope.Comision = 0;
                    $scope.Modelo.Remesa.ValorSeguroCliente = 0;
                    $scope.ValorReexpedicion = 0;
                    $scope.Modelo.Remesa.TotalFleteCliente = 0;
                }
                $scope.MaskValores();
            }
        };
        //--Calcular Ajuste Flete
        //-----------------------Manejo Detalle Unidades------------------------------//
        $scope.TmpEliminarDetalleUnidad = {};
        $scope.EsEditarDetalleUnidad = false;
        $scope.IndiceEditarDetalleUnidad = 0;
        $scope.ListadoDetalleUnidades = [];
        $scope.DetalleUnidades = {
            Producto: '',
            Referencia: '',
            Peso: '',
            Largo: '',
            Alto: '',
            Ancho: '',
            PesoVolumetrico: '',
            PesoCobrar: '',
            ValorComercial: '',
            DescripcionProducto: ''
        };
        $scope.ValidarDetalleUnidades = function (valor) {
            if (valor == true) {
                $scope.Modelo.Remesa.CantidadCliente = $scope.ListadoDetalleUnidades.length;
                var tmpPeso = 0;
                var tmpValorComercial = 0;
                for (var i = 0; i < $scope.ListadoDetalleUnidades.length; i++) {
                    tmpPeso += RevertirMV($scope.ListadoDetalleUnidades[i].PesoCobrar);
                    tmpValorComercial += RevertirMV($scope.ListadoDetalleUnidades[i].ValorComercial);
                }
                $scope.Modelo.Remesa.PesoCliente = tmpPeso.toFixed(2);
                $scope.Modelo.Remesa.PesoCobrar = tmpPeso.toFixed(2);
                $scope.Modelo.Remesa.ValorComercialCliente = tmpValorComercial;
                IndiceDetalleUnidad();
            }
            else {
                $scope.Modelo.Remesa.CantidadCliente = 1;
                $scope.Modelo.Remesa.PesoCliente = '';
                $scope.Modelo.PesoCobrar = '';
                $scope.Modelo.Remesa.ValorComercialCliente = '';
            }
            $scope.Modelo.Largo = '';
            $scope.Modelo.Alto = '';
            $scope.Modelo.Ancho = '';
            $scope.Modelo.PesoVolumetrico = '';
            $scope.Modelo.DescripcionProductoTransportado = '';
            $scope.Calcular();

        };
        function limpiarModalDetalleUnidad() {
            $scope.DetalleUnidades = {
                Producto: '',
                Referencia: '',
                Peso: '',
                Largo: '',
                Alto: '',
                Ancho: '',
                PesoVolumetrico: '',
                PesoCobrar: '',
                ValorComercial: '',
                DescripcionProducto: ''
            };
            $scope.IndiceEditarDetalleUnidad = 0;
            $scope.EsEditarDetalleUnidad = false;
        }

        $scope.ModalDetalleUnidades = function () {
            if ($scope.Modelo.Remesa.FormaPago !== undefined && $scope.Modelo.Remesa.FormaPago !== null && $scope.Modelo.Remesa.FormaPago !== '' &&
                $scope.Modelo.Remesa.TarifaCarga !== undefined && $scope.Modelo.Remesa.TarifaCarga !== null && $scope.Modelo.Remesa.TarifaCarga !== '' &&
                $scope.Modelo.Remesa.ProductoTransportado !== undefined && $scope.Modelo.Remesa.ProductoTransportado !== null && $scope.Modelo.Remesa.ProductoTransportado !== '' &&
                $scope.TarifarioInvalido == false) {
                limpiarModalDetalleUnidad();
                $scope.DetalleUnidades.Producto = $scope.Modelo.Remesa.ProductoTransportado;
                showModal('modalAdicionarDetalleUnidades');
            }
            else {
                ShowError("Se debe seleccionar una tarifa, forma de pago y Producto");
            }
        };
        $scope.CalcularDetalleUnidades = function () {
            var Detalle = $scope.DetalleUnidades;
            var PesoVolumetrico = 0;
            if ($scope.ManejoPesoVolumetrico) {
                if (Detalle.Alto !== undefined && Detalle.Alto !== '' && Detalle.Alto !== '' &&
                    Detalle.Ancho !== undefined && Detalle.Ancho !== '' && Detalle.Ancho !== '' &&
                    Detalle.Largo !== undefined && Detalle.Largo !== '' && Detalle.Largo !== '') {
                    PesoVolumetrico = ((RevertirMV(Detalle.Alto) * RevertirMV(Detalle.Ancho) * RevertirMV(Detalle.Largo)) / 1000000) * 400;
                    PesoVolumetrico = PesoVolumetrico.toFixed(2);
                    $scope.DetalleUnidades.PesoVolumetrico = PesoVolumetrico;
                }
                else {
                    $scope.DetalleUnidades.PesoVolumetrico = '';
                }
                if (PesoVolumetrico > 0 || RevertirMV(Detalle.Peso) > 0) {
                    if (PesoVolumetrico > RevertirMV(Detalle.Peso))
                        $scope.DetalleUnidades.PesoCobrar = PesoVolumetrico;
                    else
                        $scope.DetalleUnidades.PesoCobrar = RevertirMV(Detalle.Peso);
                }
                else {
                    $scope.DetalleUnidades.PesoCobrar = '';
                }
            }
            else {
                $scope.DetalleUnidades.PesoCobrar = RevertirMV(Detalle.Peso);
            }
        };
        $scope.AgregarDetalleUnidades = function () {
            if (DatosRequeridosDetalleUnidades()) {
                if ($scope.EsEditarDetalleUnidad == false) {
                    //--Agrega Nuevo Detalle
                    $scope.ListadoDetalleUnidades.push($scope.DetalleUnidades);
                }
                else {
                    //--Edita Detalle Actual
                    $scope.ListadoDetalleUnidades[$scope.IndiceEditarDetalleUnidad] = $scope.DetalleUnidades;
                }
                $scope.Modelo.Remesa.CantidadCliente = $scope.ListadoDetalleUnidades.length;
                var tmpPeso = 0;
                var tmpValorComercial = 0;
                for (var i = 0; i < $scope.ListadoDetalleUnidades.length; i++) {
                    tmpPeso += RevertirMV($scope.ListadoDetalleUnidades[i].PesoCobrar);
                    tmpValorComercial += RevertirMV($scope.ListadoDetalleUnidades[i].ValorComercial);
                }
                $scope.Modelo.Remesa.PesoCliente = tmpPeso.toFixed(2);
                $scope.Modelo.Remesa.PesoCobrar = tmpPeso.toFixed(2);
                $scope.Modelo.Remesa.ValorComercialCliente = tmpValorComercial;
                $scope.Calcular();
                IndiceDetalleUnidad();
                closeModal('modalAdicionarDetalleUnidades');
            }
        };
        function DatosRequeridosDetalleUnidades() {
            $scope.MensajesErrorDetalleUnidades = [];
            var continuar = true;
            var detalle = $scope.DetalleUnidades;
            if (ValidarCampo(detalle.Producto) == OBJETO_SIN_DATOS) {
                $scope.MensajesErrorDetalleUnidades.push('Debe ingresar el producto ');
                continuar = false;
            }
            if (ValidarCampo(detalle.DescripcionProducto) == OBJETO_SIN_DATOS) {
                $scope.MensajesErrorDetalleUnidades.push('Debe ingresar la descripción de la mercancia');
                continuar = false;
            }
            if (ValidarCampo(detalle.PesoCobrar) == OBJETO_SIN_DATOS) {
                $scope.MensajesErrorDetalleUnidades.push('Debe ingresar el peso o las dimensiones');
                continuar = false;
            }
            return continuar;
        }
        //--Editar Detalle Unidades
        $scope.EditarDetalleUnidades = function (indice) {
            $scope.DetalleUnidades = angular.copy($scope.ListadoDetalleUnidades[indice]);
            $scope.IndiceEditarDetalleUnidad = indice;
            $scope.EsEditarDetalleUnidad = true;
            showModal('modalAdicionarDetalleUnidades');
        };
        //--Editar Detalle Unidades
        //---Eliminar Detalle Unidades
        $scope.ConfirmacionEliminarDetalleUnidades = function (Producto, indice) {
            $scope.TmpEliminarDetalleUnidad = { Producto, Indice: indice };
            showModal('modalEliminarDetalleUnidad');
        };
        $scope.EliminarDetalleUnidad = function (indice) {
            $scope.ListadoDetalleUnidades.splice(indice, 1);
            $scope.Modelo.Remesa.CantidadCliente = $scope.ListadoDetalleUnidades.length;
            var tmpPeso = 0;
            var tmpValorComercial = 0;
            for (var i = 0; i < $scope.ListadoDetalleUnidades.length; i++) {
                tmpPeso += RevertirMV($scope.ListadoDetalleUnidades[i].PesoCobrar);
                tmpValorComercial += RevertirMV($scope.ListadoDetalleUnidades[i].ValorComercial);
            }
            $scope.Modelo.Remesa.PesoCliente = tmpPeso.toFixed(2);
            $scope.Modelo.Remesa.PesoCobrar = tmpPeso.toFixed(2);
            $scope.Modelo.Remesa.ValorComercialCliente = tmpValorComercial;
            IndiceDetalleUnidad();
            $scope.Calcular();
            closeModal('modalEliminarDetalleUnidad');
        };
        //---Eliminar Detalle Unidades
        function IndiceDetalleUnidad() {
            for (var i = 0; i < $scope.ListadoDetalleUnidades.length; i++) {
                $scope.ListadoDetalleUnidades[i].Indice = i;
            }
        }
        //-----------------------Manejo Detalle Unidades------------------------------//
        //-----------------------Mapa Google------------------------------//
        $scope.AbrirMapa = function (lat, lng) {
            showModal('modalMostrarMapa');
            iniciarMapaGoogle(lat, lng);
        };
        function iniciarMapaGoogle(lat, lng) {
            var marker = [];
            var map, infoWindow;
            $scope.MapaLatitud = lat != '' ? lat : 0;
            $scope.MapaLongitud = lng != '' ? lng : 0;
            function initMap(posicion) {
                if (map === void 0) {
                    var mapOptions = {
                        zoom: 15,
                        center: new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude),
                        disableDefaultUI: true,
                        mapTypeId: window.google.maps.MapTypeId.ROADMAP
                    };
                    map = new window.google.maps.Map(document.getElementById('gmaps'), mapOptions);
                }
            }

            function setMarker(map, position, title, content) {
                var markers;
                var markerOptions = {
                    position: position,
                    map: map,
                    draggable: true,
                    animation: google.maps.Animation.BOUNCE,
                    title: title
                    //icon: 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png'
                };
                markers = new window.google.maps.Marker(markerOptions);
                markers.addListener('dragend', function (event) {
                    //escribimos las coordenadas de la posicion actual del marcador dentro los input 
                    $scope.MapaLatitud = this.getPosition().lat();
                    $scope.MapaLongitud = this.getPosition().lng();
                });
                marker.push(markers);

                var infoWindowOptions = {
                    content: content
                };
                infoWindow = new window.google.maps.InfoWindow(infoWindowOptions);
                infoWindow.open(map, markers);

            }

            function localizacion(posicion) {
                initMap(posicion);
                if ($scope.MapaLatitud != undefined && $scope.MapaLongitud != undefined && $scope.MapaLatitud != 0 && $scope.MapaLongitud != 0) {
                    setMarker(map, new window.google.maps.LatLng($scope.MapaLatitud, $scope.MapaLongitud), 'Posición del usuario', 'Destino');
                } else {
                    $scope.MapaLatitud = posicion.coords.latitude;
                    $scope.MapaLongitud = posicion.coords.longitude;
                    setMarker(map, new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude), 'Posición actual del usuario', 'Me encuentro aquí');
                }
            }

            function errores(error) {
                MensajeInfo("Info", "Ha ocurrido un error al intentar obtener la información de las cordenadas GPS. Error:" + error, false);
            }

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(localizacion, errores);
            } else {
                MensajeInfo("Info", "Tu dispositivo no soporta o no tiene habilitada la 'Geolocalización'", false);
            }

        }
        $scope.AsignarPosicion = function () {
            $scope.Modelo.Remesa.Latitud = $scope.MapaLatitud;
            $scope.Modelo.Remesa.Longitud = $scope.MapaLongitud;
            closeModal('modalMostrarMapa');
        };
        //-----------------------Mapa Google------------------------------//
        //-----------------------Mascaras - Volver------------------------------//
        $scope.VolverMaster = function () {
            if ($scope.Modelo.Remesa.NumeroDocumento !== undefined)
                document.location.href = $scope.Master + '/' + $scope.Modelo.Remesa.NumeroDocumento;
            else
                document.location.href = $scope.Master;
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope);
        };
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope);
        };
        $scope.MaskNumeroGrid = function (item) {
            try {
                return parseInt(MascaraNumero(item));

            } catch (e) {
                return 0;
            }
        };
        $scope.MaskMoneda = function (option) {
            MascaraMonedaGeneral($scope);
        };
        $scope.MaskMonedaGrid = function (item) {
            return MascaraMoneda(item);
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope);
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item);
        };
        //----------------------------Funciones Generales---------------------------------//
    }]);