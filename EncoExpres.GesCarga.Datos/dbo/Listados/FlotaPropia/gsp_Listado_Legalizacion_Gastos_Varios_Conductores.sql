﻿DROP  PROCEDURE gsp_Listado_Legalizacion_Gastos_Varios_Conductores    
GO

CREATE PROCEDURE gsp_Listado_Legalizacion_Gastos_Varios_Conductores    
(    
 @par_EMPR_Codigo smallint,      
 @par_Fecha_Inicial DATE = NULL,      
 @par_Fecha_Final DATE = NULL,      
 @par_Numero_Documento NUMERIC = NULL,      
 @par_TERC_Codigo_Conductor NUMERIC = NULL,      
 @par_VEHI_Codigo NUMERIC = NULL,      
 @par_OFIC_Codigo NUMERIC = NULL,      
 @par_Estado NUMERIC = NULL     
)                 
AS    
BEGIN    
 Declare @Estadolist NUMERIC = NULL;    
 Declare @Anuladolist NUMERIC = NULL;    
    
 IF (@par_Estado IS NOT NULL)    
 BEGIN    
  IF(@par_Estado IN (0,1))    
  BEGIN    
   SET @Estadolist = @par_Estado;    
  END    
  ELSE    
  BEGIN    
   SET @Anuladolist = 1;    
  END                 
 END    
 SELECT    
 ELGC.EMPR_Codigo,    
 ELGC.Fecha,    
 ELGC.Numero_Documento ELGC_NumeroDocumento,    
 ENPD.Numero_Documento ENPD_NumeroDocumento,    
 VEHI.Placa,  
 ISNULL(TENE.Razon_Social,'')+' '+ISNULL(TENE.Nombre,'') +' '+ISNULL(TENE.Apellido1,'')+' '+ISNULL(TENE.Apellido2,'') AS Tenedor,    
 RUTA.Nombre AS Ruta,  
 CASE WHEN ELGC.Anulado = 1 THEN 'A'      
 WHEN ELGC.Estado = 1 THEN 'D' ELSE 'B'                   
 END AS Estado      
    
 FROM Encabezado_Legalizacion_Gastos_Conductor ELGC    
    
 LEFT JOIN Encabezado_Planilla_Despachos ENPD ON    
 ELGC.EMPR_Codigo = ENPD.EMPR_Codigo    
 AND ELGC.Numero = ENPD.ELGC_Numero    
  
 LEFT JOIN Vehiculos as VEHI ON  
 ENPD.EMPR_Codigo = VEHI.EMPR_Codigo  
 AND ENPD.VEHI_Codigo = VEHI.Codigo  
    
 LEFT JOIN Terceros TENE ON    
 ENPD.EMPR_Codigo = TENE.EMPR_Codigo    
 AND ENPD.TERC_Codigo_Tenedor = TENE.Codigo    
  
 LEFT JOIN Rutas RUTA ON  
 ENPD.EMPR_Codigo = RUTA.EMPR_Codigo  
 AND ENPD.RUTA_Codigo = RUTA.Codigo  
   
    
 WHERE     
 ELGC.EMPR_Codigo = @par_EMPR_Codigo    
 AND ELGC.Numero_Documento = ISNULL( @par_Numero_Documento,ELGC.Numero_Documento)                
 AND ELGC.Fecha >= ISNULL( @par_Fecha_Inicial,ELGC.Fecha)                 
 AND ELGC.Fecha <= ISNULL( @par_Fecha_Final,ELGC.Fecha)    
 AND ENPD.VEHI_Codigo = ISNULL(@par_VEHI_Codigo,ENPD.VEHI_Codigo)  
 AND ELGC.TERC_Codigo_Conductor <= ISNULL( @par_TERC_Codigo_Conductor,ELGC.TERC_Codigo_Conductor)    
 AND ELGC.OFIC_Codigo =  ISNULL(@par_OFIC_Codigo, ELGC.OFIC_Codigo)    
 AND ELGC.Estado = ISNULL(@Estadolist,ELGC.Estado)    
 AND (ELGC.Anulado = @Anuladolist OR @Anuladolist IS NULL)        
  
    
 order by ELGC.Numero    
END    
GO