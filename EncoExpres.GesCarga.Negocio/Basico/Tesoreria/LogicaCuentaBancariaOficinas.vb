﻿Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.Tesoreria
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Mensajes.My.Resources

Namespace Basico
    Public NotInheritable Class LogicaCuentaBancariaOficinas
        Inherits LogicaBase(Of CuentaBancariaOficinas)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of CuentaBancariaOficinas)

        Sub New(persistencia As IPersistenciaBase(Of CuentaBancariaOficinas))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As CuentaBancariaOficinas) As Respuesta(Of IEnumerable(Of CuentaBancariaOficinas))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of CuentaBancariaOficinas))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of CuentaBancariaOficinas))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As CuentaBancariaOficinas) As Respuesta(Of Long)
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As CuentaBancariaOficinas) As Respuesta(Of CuentaBancariaOficinas)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of CuentaBancariaOficinas)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of CuentaBancariaOficinas)(consulta)
        End Function
    End Class
End Namespace
