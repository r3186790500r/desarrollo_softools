
PRINT 'gsp_consultar_terceros_sincronizacion_CARGA_APP_pendientes_sincronizar'
GO
DROP PROCEDURE gsp_consultar_terceros_sincronizacion_CARGA_APP_pendientes_sincronizar
GO
CREATE PROCEDURE gsp_consultar_terceros_sincronizacion_CARGA_APP_pendientes_sincronizar(
@par_EMPR_Codigo smallint,
@par_Perfiles varchar(max)
)
AS
BEGIN
SELECT DISTINCT
LTRIM(RTRIM(CONCAT(TERC.Razon_Social,' ',TERC.Nombre))) AS Nombres,
LTRIM(RTRIM(CONCAT(TERC.Apellido1,' ',TERC.Apellido2))) AS Apellidos,
TERC.Codigo_Alterno,
TERC.Estado,
TERC.Codigo,
TERC.Numero_Identificacion,
TERC.Direccion,
TERC.CIUD_Codigo,
CIUD.Codigo_Alterno As Ciudad,
TERC.Telefonos,
IIF((SELECT COUNT(1) FROM Perfil_Terceros WHERE EMPR_Codigo = @par_EMPR_Codigo AND TERC_Codigo = TERC.Codigo AND Codigo = 1412) > 0,1,0) As Tenedor,
IIF((SELECT COUNT(1) FROM Perfil_Terceros WHERE EMPR_Codigo = @par_EMPR_Codigo AND TERC_Codigo = TERC.Codigo AND Codigo = 1408) > 0,1,0) As Propietario,
IIF((SELECT COUNT(1) FROM Perfil_Terceros WHERE EMPR_Codigo = @par_EMPR_Codigo AND TERC_Codigo = TERC.Codigo AND Codigo = 1408) > 0,1,0) As Administrador,
ISNULL((SELECT TOP(1) Placa FROM Vehiculos WHERE EMPR_Codigo = @par_EMPR_Codigo AND (TERC_Codigo_Conductor = TERC.Codigo OR TERC_Codigo_Tenedor = TERC.Codigo OR TERC_Codigo_Propietario = TERC.Codigo )),'(N/A).') AS PlacaCabezote,
ISNULL((SELECT TOP(1) Placa FROM Semirremolques WHERE EMPR_Codigo = @par_EMPR_Codigo AND Codigo =( SELECT TOP(1) SEMI_Codigo FROM Vehiculos WHERE EMPR_Codigo = @par_EMPR_Codigo AND (TERC_Codigo_Conductor = TERC.Codigo OR TERC_Codigo_Tenedor = TERC.Codigo OR TERC_Codigo_Propietario = TERC.Codigo ))),'(N/A).') AS PlacaTrailer
FROM Terceros TERC

LEFT JOIN Registro_Sincronizacion_APP_CARGA AS RSAC
ON TERC.EMPR_Codigo = RSAC.EMPR_Codigo  
AND TERC.Codigo = RSAC.Codigo
AND RSAC.CATA_OSCA_Codigo = 23804 -- Origen Sincronizacion CARGA APP Terceros

INNER JOIN Perfil_Terceros PETE ON
TERC.EMPR_Codigo = PETE.EMPR_Codigo AND
TERC.Codigo = PETE.TERC_Codigo
AND PETE.Codigo IN(SELECT * FROM Func_Dividir_String(@par_Perfiles,','))

LEFT JOIN Ciudades CIUD ON
TERC.EMPR_Codigo = CIUD.EMPR_Codigo AND
TERC.CIUD_Codigo = CIUD.Codigo


WHERE TERC.EMPR_Codigo = @par_EMPR_Codigo
AND ISNULL(RSAC.Sincronizado,0) = 1
AND TERC.Fecha_Modifica > RSAC.Fecha_Sincronizacion
END
GO
