﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.ServicioCliente
Imports EncoExpres.GesCarga.Entidades.SIESA
Imports EncoExpres.GesCarga.Negocio

<Authorize>
Public Class ServiciosSIESAController
    Inherits Base
    <HttpPost>
    <ActionName("ConsultarCajasSIESA")>
    Public Function Consultar(ByVal filtro As ServiciosENCOE) As List(Of ServiciosENCOE)
        Return New LogicaServiciosENCOE().Consultar_Cajas_SIESA(filtro)
    End Function

    <HttpPost>
    <ActionName("ConsultarSucursalesTenedorSIESA")>
    Public Function ConsultarSucursalesTenedorSIESA(ByVal filtro As SucursalTenedorSIESA) As List(Of SucursalTenedorSIESA)
        Return New LogicaServiciosENCOE().Consultar_Sucursales_Tenedor_SIESA(filtro)
    End Function

    '<HttpPost>
    '<ActionName("Obtener")>
    'Public Function Obtener(ByVal filtro As ServiciosENCOE) As Respuesta(Of ServiciosENCOE)
    'End Function

    '<HttpPost>
    '<ActionName("Guardar")>
    'Public Function Guardar(ByVal entidad As ServiciosENCOE) As Respuesta(Of Long)

    'End Function

    <HttpPost>
    <ActionName("ConsultarOficinaSIESA")>
    Public Function ConsultarOficina(ByVal filtro As OficinaSIESA) As List(Of OficinaSIESA)
        Return New LogicaServiciosENCOE().Consultar_Oficinas_SIESA(filtro)
    End Function

    <HttpPost>
    <ActionName("ConsultarTerceroSIESA")>
    Public Function ConsultarTercero(ByVal filtro As TerceroSIESA) As List(Of TerceroSIESA)
        Return New LogicaServiciosENCOE().Consultar_Terceros_SIESA(filtro)
    End Function

    <HttpPost>
    <ActionName("ImportarTerceroSIESA")>
    Public Function ImportarTerceroSIESA(ByVal filtro As TerceroSIESA) As String
        Return New LogicaServiciosENCOE().Importar_Tercero_SIESA(filtro)
    End Function


End Class
