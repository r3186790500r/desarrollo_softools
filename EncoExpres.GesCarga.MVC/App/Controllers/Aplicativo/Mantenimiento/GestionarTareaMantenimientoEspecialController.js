﻿EncoExpresApp.controller("GestionarTareaMantenimientoEspecialCtrl", ['$scope', '$routeParams', 'blockUI', '$timeout', 'ValorCatalogosFactory', '$linq', 'UnidadReferenciaMantenimientoFactory', 'SistemasFactory', 'SubSistemasFactory', 'TercerosFactory', 'EquipoMantenimientoFactory', 'TareaMantenimientoFactory',
    function ($scope, $routeParams, blockUI, $timeout, ValorCatalogosFactory, $linq, UnidadReferenciaMantenimientoFactory, SistemasFactory, SubSistemasFactory, TercerosFactory, EquipoMantenimientoFactory, TareaMantenimientoFactory) {
        $scope.ListadoDetalleMantenimiento = [];
        $scope.MensajesError = [];
        $scope.MensajesErrorDetalle = [];
        $scope.ListadoEstados = [];
        $scope.ModificarDetalle = 0;
        $scope.Operacion = 'Adicionar';
        $scope.Validarvalortotal = false
        $scope.Validarvaloriva = false
        $scope.ValidarSubtotal = false


        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_TAREAS_MANTENIMIENTO);

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        // Se crea la propiedad modelo en el ambito
        $scope.MapaSitio = [{ Nombre: 'Mantenimiento' }, { Nombre: 'Documentos' }, { Nombre: 'Tareas Mantenimiento' }];
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Numero: 0,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Nombre : ''
        };
        $scope.ModeloFecha = new Date();
        /* Obtener parametros del enrutador*/
        if ($routeParams.Codigo !== undefined) {
            $scope.Modelo.Numero = $routeParams.Codigo;
        }
        else {
            $scope.Modelo.Numero = 0;
        }
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Metodo para volver a la página de consulta
        $scope.VolverMaster = function () {
            if ($scope.Modelo.Numero > 0) {
                document.location.href = '#!ConsultarTareasMantenimiento/' + $scope.Modelo.Numero;
            } else {
                document.location.href = '#!ConsultarTareasMantenimiento';
            }
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*Cargar el combo de estados*/
        $scope.ListadoEstados = [            
            { Codigo: 1, Nombre: 'DEFINITIVO' },
            { Codigo: 0, Nombre: 'BORRADOR' },
        ];

        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 0');
        /*Cargar el combo de Tipo Mantenimiento*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_MANTENIMIENTO_EQUIPOS } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoMantenimiento = [];
                    $scope.ListadoTipoMantenimiento = response.data.Datos
                    $scope.TipoMantenimiento = response.data.Datos[0];
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar el combo de Sistema mantenimiento*/
        SistemasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoSistemaMantenimiento = [];
                    response.data.Datos.forEach(function (item) {
                        $scope.ListadoSistemaMantenimiento.push(item);
                    });
                    $timeout(function () {
                        if ($('#tiposistema')[0].options[0] != undefined) {
                            if ($('#tiposistema')[0].options[0].value == "?") {
                                $('#tiposistema')[0].options[0].remove()
                            }
                        }
                    }, 50);

                    $timeout(function () {
                        $('#tiposistema').selectpicker()
                    }, 200);
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar el combo de Subsistemas Mantenimiento*/
        SubSistemasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoSubsistemaMantenimiento = [];
                    $scope.ListadoSubsistemaFiltrado = [];
                    $scope.ListadoSubsistemaMantenimiento = response.data.Datos

                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar el combo Unidad Referencia Mantenimiento*/
        UnidadReferenciaMantenimientoFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoUnidadReferenciaMantenimiento = [];
                    $scope.ListadoUnidadReferenciaMantenimiento = response.data.Datos
                    $scope.UnidadReferenciaMantenimiento = response.data.Datos[0];
                } else {
                    $scope.ListadoUnidadReferenciaMantenimiento = [];
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar el combo de Equipo Mantenimiento*/
        EquipoMantenimientoFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoEquipoMantenimiento = [];
                    $scope.ListadoEquipoMantenimiento = response.data.Datos;
                    $scope.EquipoMantenimiento = response.data.Datos[0];
                    $timeout(function () {
                        if ($('#equipo')[0].options[0] != undefined) {
                            if ($('#equipo')[0].options[0].value == "?") {
                                $('#equipo')[0].options[0].remove()
                            }
                        }
                    }, 50);

                    $timeout(function () {
                        $('#equipo').selectpicker()
                    }, 200);

                }
            }, function (response) {
                ShowError(response.statusText);
            });
        // Cargar Autocomplete Proveedores
        TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Autocomplete: 1, Proveedores: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoProveedores = [];
                        response.data.Datos.push({ NombreCompleto: '', Codigo: 0 })
                        $scope.ListadoProveedores = response.data.Datos;
                        $scope.Proveedor = $scope.ListadoProveedores[$scope.ListadoProveedores.length - 1];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        if ($scope.Modelo.Numero > 0) {
            // Consultar los datos del tercero correspondientes al número que llego como parametro del enrutador
            $scope.Titulo = 'CONSULTAR TAREA DE MANTENIMIENTO';
            $scope.Deshabilitar = true;
            Obtener();
        }
        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando tarea de mantenimiento ' + $scope.Modelo.Numero);

            $timeout(function () {
                blockUI.message('Cargando  tarea de mantenimiento ' + $scope.Modelo.Numero);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Modelo.Numero,
            };

            blockUI.delay = 1000;
            TareaMantenimientoFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        $scope.Descripcion = response.data.Datos.Descripcion;
                        $scope.Modelo.Nombre = response.data.Datos.Nombre;
                        $scope.EquipoMantenimiento = $linq.Enumerable().From($scope.ListadoEquipoMantenimiento).First('$.Codigo ==' + response.data.Datos.EquipoMantenimiento.Codigo);
                        $scope.CodigoPlaca = response.data.Datos.Vehiculo.Codigo;

                        if ($scope.ListaPlaca !== undefined) {
                            $scope.Placa = $linq.Enumerable().From($scope.ListaPlaca).First('$.Codigo == ' + $scope.CodigoPlaca);
                        }

                        $scope.ModeloFecha = RetornarFechaEspecifica(new Date(response.data.Datos.Fecha));

                        if (response.data.Datos.Estado == 2) {
                            $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado);
                        }
                        else {
                            $scope.ListadoEstados.push({ Nombre: 'Anulado', Codigo: 2 });
                            $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado);
                        }
                        if (response.data.Datos.Estado == 0) {
                            $scope.Deshabilitar = false;
                            $scope.Bloquear = true;
                        }
                        else if (response.data.Datos.Estado == 1) {
                            $scope.Deshabilitar = true;
                        }

                        $scope.Descripcion = response.data.Datos.Descripcion
                        $scope.ListadoDetalleMantenimiento = response.data.Datos.Detalles;
                        $scope.KmUso = response.data.Datos.Detalles[0].KmUso

                        $scope.KmUso = MascaraValores($scope.KmUso)
                    }
                    else {
                        ShowError('No se logro consultar la tarea de mantenimiento No.' + $scope.Modelo.Numero + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarTareasMantenimiento';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    //ShowError('No se logro consultar la tarea de mantenimiento No.' + $scope.Modelo.Codigo + '. Por favor contacte el administrador del sistema.');
                    document.location.href = '#!ConsultarTareasMantenimiento';
                });

            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.InicializarDetalle = function () {
            showModal('modalMantenimiento');
            $scope.ModificarDetalle = 0
            $scope.Cantidad = 0
            $scope.Proveedor = ''
            $scope.NumeroFactura = 0
            $scope.FechaFactura = ''
            $scope.FechaVenceGarantia = ''
            $scope.Subtotal = 0
            $scope.ValorIVA = 0
            $scope.ValorTotal = 0
            // $scope.KmUso = 0
        }
        $scope.ConfirmacionGuardarTarea = function () {
            showModal('modalConfirmacionGuardarTarea');
        };
        // Metodo para guardar /modificar
        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardarTarea');
            $scope.MaskNumero();
            $scope.Modelo.EquipoMantenimiento = $scope.EquipoMantenimiento;
            $scope.Modelo.Vehiculo = $scope.EquipoMantenimiento.Vehiculo;
            $scope.Modelo.Fecha = $scope.ModeloFecha;
            $scope.Modelo.Estado = $scope.Estado.Codigo
            $scope.Modelo.Descripcion = $scope.Descripcion
            
                
            

            $scope.ListadoDetalleMantenimiento.forEach(function (item) {
                item.KmUso = $scope.Modelo.KmUso = MascaraNumero($scope.KmUso)
            })


            $scope.Modelo.Detalles = $scope.ListadoDetalleMantenimiento;
            $scope.Modelo.Detalles.forEach(detalle => detalle.Proveedor = { Codigo: detalle.Proveedor.Codigo });
            if (DatosRequeridos()) {
                TareaMantenimientoFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Numero == 0) {
                                    ShowSuccess('Se guardó la tarea de mantenimiento No.' + response.data.Datos + ' "'+ $scope.Modelo.Nombre +'"');
                                    location.href = '#!ConsultarTareasMantenimiento/' + response.data.Datos;
                                } else {
                                    ShowSuccess('Se modificó la tarea de mantenimiento No.' + response.data.Datos + ' "' + $scope.Modelo.Nombre + '"');
                                    location.href = '#!ConsultarTareasMantenimiento/' + $scope.Modelo.Numero;
                                }
                            }
                            else {
                                ShowError(response.data.MensajeOperacion);
                            }
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };
        // Valida los datos requridos 
        function DatosRequeridos() {
            $scope.MensajesErrorRecorrido = [];
            var continuar = true;
            if ($scope.EquipoMantenimiento == '' || $scope.EquipoMantenimiento == 0 || $scope.EquipoMantenimiento == undefined) {
                $scope.MensajesError.push('Debe ingresar el equipo mantenimiento');
                continuar = false;
            }
            if ($scope.ModeloFecha == null || $scope.ModeloFecha == undefined) {
                $scope.MensajesError.push('Debe Ingresar la fecha');
                continuar = false;
            }
            if ($scope.KmUso == '' || $scope.KmUso == 0 || $scope.KmUso == undefined) {
                $scope.MensajesError.push('Debe ingresar el valor actual de uso');
                continuar = false;
            }
            if ($scope.Estado == '' || $scope.Estado == 0 || $scope.Estado == undefined) {
                $scope.MensajesError.push('Debe ingresar el estado');
                continuar = false;
            }
            if ($scope.ListadoDetalleMantenimiento.length == 0) {
                $scope.MensajesError.push('Debe Ingresar mínimo un detalle');
                continuar = false;
            }

            if ($scope.Modelo.Nombre == undefined || $scope.Modelo.Nombre == null || $scope.Modelo.Nombre == '') {
                $scope.MensajesError.push('Debe Ingresar un Nombre');
                continuar = false;
            }
            return continuar

        }
        /*Asignar Movimiento Al Comprobante*/
        $scope.GuardarDetalleMantenimiento = function () {
            $scope.MensajesErrorDetalleMantenimiento = [];
            if (DatosRequeridosMovimiento()) {
                CargarlistadoDetalleMantenimiento()
            }
        }
        function CargarlistadoDetalleMantenimiento() {
            closeModal('modalMantenimiento');
            $scope.MaskNumero();
            $scope.ListadoDetalleMantenimiento.push({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                SistemaMantenimiento: $scope.SistemaMantenimiento,
                SubsistemaMantenimiento: $scope.SubsistemaMantenimiento,
                TipoMantenimiento: $scope.TipoMantenimiento,
                UnidadReferenciaMantenimiento: $scope.UnidadReferenciaMantenimiento,
                Descripcion: '',
                Cantidad: $scope.Cantidad,
                Proveedor: $scope.Proveedor,
                NumeroFactura: $scope.NumeroFactura,
                FechaFactura: $scope.FechaFactura,
                FechaVenceGarantia: $scope.FechaVenceGarantia,
                Valor: MascaraNumero($scope.Subtotal),
                ValorIva: MascaraNumero($scope.ValorIVA),
                ValorTotal: MascaraNumero($scope.ValorTotal),
                ENDATiDOCodigo: 1,
                ENDANumero: 1,
                KmUso: $scope.KmUso
            });
        }
        //---------------------------------------------------------------------------------------------------------------//
        $scope.AsignarValorSubtotal = function () {
            if ($scope.Subtotal > 9999999999) {
                $scope.ValidarSubtotal = true
            }
            else {
                $scope.ValidarSubtotal = false
            }
        }
        $scope.AsignarValorIva = function () {
            if ($scope.ValorIVA > 9999999999) {
                $scope.Validarvaloriva = true
            }
            else {
                $scope.Validarvaloriva = false
            }
        }
        $scope.AsignarvalorTotal = function () {
            if ($scope.ValorTotal > 9999999999) {
                $scope.Validarvalortotal = true
            }
            else {
                $scope.Validarvalortotal = false
            }
        }
        //---------------------------------------------------------------------------------------------------------------------------//

        function DatosRequeridosMovimiento() {
            $scope.MensajesErrorDetalle = [];
            var continuar = true;

            if ($scope.SistemaMantenimiento == undefined || $scope.SistemaMantenimiento == "" || $scope.SistemaMantenimiento == null) {
                $scope.MensajesErrorDetalle.push('Debe ingresar un sistema de mantenimiento');
                continuar = false;
            }
            else if ($scope.SubsistemaMantenimiento == undefined || $scope.SubsistemaMantenimiento == "" || $scope.SubsistemaMantenimiento == null) {
                $scope.MensajesErrorDetalle.push('Debe ingresar un subsistema de mantenimiento');
                continuar = false;
            }
            if ($scope.TipoMantenimiento == undefined || $scope.TipoMantenimiento == "" || $scope.TipoMantenimiento == null) {
                $scope.MensajesErrorDetalle.push('Debe ingresar el tipo de mantenimiento');
                continuar = false;
            }
            if ($scope.UnidadReferenciaMantenimiento == undefined || $scope.UnidadReferenciaMantenimiento == "" || $scope.UnidadReferenciaMantenimiento == null) {
                $scope.MensajesErrorDetalle.push('Debe ingresar la unidad referencia mantenimiento');
                continuar = false;
            }
            if ($scope.Cantidad == undefined || $scope.Cantidad == "" || $scope.Cantidad == null) {
                $scope.MensajesErrorDetalle.push('Debe ingresar la cantidad');
                continuar = false;
            }
            if ($scope.Proveedor == undefined || $scope.Proveedor == "" || $scope.Proveedor == null) {
                $scope.MensajesErrorDetalle.push('Debe ingresar el proveedor');
                continuar = false;
            } else if ($scope.Proveedor.Codigo == undefined || $scope.Proveedor.Codigo == "" || $scope.Proveedor.Codigo == null) {
                $scope.MensajesErrorDetalle.push('Debe ingresar un proveedor válido');
                continuar = false;
            }
            if ($scope.NumeroFactura == undefined || $scope.NumeroFactura == "" || $scope.NumeroFactura == null) {
                $scope.MensajesErrorDetalle.push('Debe ingresar el número de factura');
                continuar = false;
            }
            if ($scope.Subtotal == undefined || $scope.Subtotal == "" || $scope.Subtotal == null) {
                $scope.MensajesErrorDetalle.push('Debe ingresar subtotal de la factura');
                continuar = false;
            }
            if ($scope.ValorIVA == undefined || $scope.ValorIVA == "" || $scope.ValorIVA == null) {
                $scope.MensajesErrorDetalle.push('Debe ingresar el valor IVA de la factura');
                continuar = false;
            }
            if ($scope.FechaFactura == undefined || $scope.FechaFactura == "" || $scope.FechaFactura == null) {
                $scope.MensajesErrorDetalle.push('Debe ingresar la fecha de la factura');
                continuar = false;
            }
            if ($scope.FechaVenceGarantia == undefined || $scope.FechaVenceGarantia == "" || $scope.FechaVenceGarantia == null) {
                $scope.MensajesErrorDetalle.push('Debe ingresar la fecha de vencimiento de la garantia');
                continuar = false;
            }

            return continuar;
        }

        /*Funcion que permite modificar el detalle de la tarea de mantenimiento*/
        $scope.EditarDetalleTarea = function (itemDetalle) {
            $scope.tempItem = itemDetalle
            $scope.ModificarDetalle = 1 // variable que identifica si es para editar o agregar
            //$scope.FechaVenceGarantia = new Date(itemDetalle.FechaVenceGarantia)
            $('#tiposistema').selectpicker('destroy')

            $scope.SistemaMantenimiento = $linq.Enumerable().From($scope.ListadoSistemaMantenimiento).First('$.Codigo == ' + itemDetalle.SistemaMantenimiento.Codigo);
            $timeout(function () {
                if ($('#tiposistema')[0].options[0].value == "?" || $('#tiposubsistema')[0].options[0].value == "") {
                    $('#tiposistema')[0].options[0].remove()
                }
            }, 50);
            $timeout(function () {
                $('#tiposistema').selectpicker()
            }, 200);
            $scope.FiltrarSubsistema()
            $timeout(function () {
                $('#tiposubsistema').selectpicker('destroy')
                $scope.SubsistemaMantenimiento = $linq.Enumerable().From($scope.ListadoSubsistemaMantenimiento).First('$.Codigo == ' + itemDetalle.SubsistemaMantenimiento.Codigo);;
                $timeout(function () {
                    if ($('#tiposubsistema')[0].options[0].value == "?" || $('#tiposubsistema')[0].options[0].value == "") {
                        $('#tiposubsistema')[0].options[0].remove()
                    }
                }, 50);
                $timeout(function () {
                    $('#tiposubsistema').selectpicker()
                }, 200);
                $scope.TipoMantenimiento = $linq.Enumerable().From($scope.ListadoTipoMantenimiento).First('$.Codigo == ' + itemDetalle.TipoMantenimiento.Codigo);
                $scope.UnidadReferenciaMantenimiento = $linq.Enumerable().From($scope.ListadoUnidadReferenciaMantenimiento).First('$.Codigo == ' + itemDetalle.UnidadReferenciaMantenimiento.Codigo);
                // $scope.KmUso = itemDetalle.KmUso
                $scope.Cantidad = itemDetalle.Cantidad
                $scope.Proveedor = $linq.Enumerable().From($scope.ListadoProveedores).First('$.Codigo == ' + itemDetalle.Proveedor.Codigo);
                $scope.NumeroFactura = itemDetalle.NumeroFactura
                $scope.FechaFactura = RetornarFechaEspecifica(new Date(itemDetalle.FechaFactura));
                $scope.FechaVenceGarantia = RetornarFechaEspecifica(new Date(itemDetalle.FechaVenceGarantia));
                $scope.Subtotal = itemDetalle.Valor
                $scope.ValorIVA = itemDetalle.ValorIva
                $scope.ValorTotal = itemDetalle.ValorTotal
                showModal('modalMantenimiento');
            }, 300)
            showModal('modalMantenimiento');

        }
        $scope.ModificarDetalleTarea = function () {
            if (DatosRequeridosMovimiento()) {
                closeModal('modalMantenimiento');
                $scope.tempItem.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
                $scope.tempItem.SistemaMantenimiento = $scope.SistemaMantenimiento;
                $scope.tempItem.SubsistemaMantenimiento = $scope.SubsistemaMantenimiento;
                $scope.tempItem.TipoMantenimiento = $scope.TipoMantenimiento;
                $scope.tempItem.UnidadReferenciaMantenimiento = $scope.UnidadReferenciaMantenimiento;
                $scope.tempItem.Descripcion = '';
                $scope.tempItem.Cantidad = $scope.Cantidad;
                $scope.tempItem.Proveedor = $scope.Proveedor;
                $scope.tempItem.NumeroFactura = $scope.NumeroFactura;
                $scope.tempItem.FechaFactura = $scope.FechaFactura;
                $scope.tempItem.FechaVenceGarantia = $scope.FechaVenceGarantia;
                //  $scope.tempItem.KmUso = $scope.KmUso;
                $scope.tempItem.Valor = $scope.Subtotal;
                $scope.tempItem.ValorIva = $scope.ValorIVA;
                $scope.tempItem.ValorTotal = $scope.ValorTotal;
                $scope.tempItem.ENDATiDOCodigo = 1;
                $scope.tempItem.ENDANumero = 1;
            }
        };


        /*Eliminar Detalle tarea mantenimiento*/
        $scope.ConfirmacionEliminarDetalle = function (indice) {
            $scope.MensajeEliminar = { indice };
            showModal('modalEliminarDetalle');
        };

        $scope.FiltrarSubsistema = function () {
            $('#tiposubsistema').selectpicker('destroy')
            $scope.ListadoSubsistemaFiltrado = []
            for (var i = 0; i < $scope.ListadoSubsistemaMantenimiento.length; i++) {
                var item = $scope.ListadoSubsistemaMantenimiento[i]

                if (item.SistemaMantenimiento != null && $scope.SistemaMantenimiento != null) {
                    if (item.SistemaMantenimiento.Codigo == $scope.SistemaMantenimiento.Codigo) {
                        $scope.ListadoSubsistemaFiltrado.push(item)
                    }
                }
            }
            $scope.SubsistemaMantenimiento = $scope.ListadoSubsistemaFiltrado[0]
            $timeout(function () {
                if ($('#tiposubsistema')[0].options[0].value == "?" || $('#tiposubsistema')[0].options[0].value == "") {
                    $('#tiposubsistema')[0].options[0].remove()
                }
            }, 50);
            $timeout(function () {
                $('#tiposubsistema').selectpicker()
                $scope.AplicarCampos($scope.SubsistemaMantenimiento.Nombre)
            }, 200);
        }

        $scope.AplicarCampos = function (item) {
            if (item.toUpperCase() == Mano_Obra_str) {
                $scope.AplicaValores = false
                $scope.AplicaManoObra = true
                $scope.Cantidad = 0
                $scope.ValorRepuestos = 0
            } else {
                $scope.AplicaValores = true
                $scope.AplicaManoObra = false
                $scope.ValorManoObra = 0
            }
        }

        $scope.CalcularTotal = function () {
            $scope.MaskNumero();
            var Subtotal = 0
            var ValorIVA = 0

            ValorIVA = MascaraNumero($scope.ValorIVA)
            if ($scope.Subtotal > 0 && $scope.Subtotal !== '' && $scope.Subtotal !== undefined) {
                Subtotal = $scope.Subtotal
            }
            if ($scope.ValorIVA > 0 && $scope.ValorIVA !== '' && $scope.ValorIVA !== undefined) {

                ValorIVA = $scope.ValorIVA

            }
            $scope.ValorTotal = parseInt(Subtotal) + parseInt(ValorIVA)
            $scope.MaskValores();

        }
        $scope.EliminarDetalle = function (indice) {
            $scope.ListadoDetalleMantenimiento.splice(indice, 1);
            closeModal('modalEliminarDetalle');
        };

        $scope.MaskMayus = function (option) {
            MascaraMayus(option)
        };
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskPlaca = function () {
            MascaraPlacaGeneral($scope)
        };


        $scope.MaskValores = function () {
            try { $scope.KmUso = MascaraValores($scope.KmUso) } catch (e) { }
            try { $scope.Subtotal = MascaraValores($scope.Subtotal) } catch (e) { }
            try { $scope.ValorIVA = MascaraValores($scope.ValorIVA) } catch (e) { }
            try { $scope.ValorTotal = MascaraValores($scope.ValorTotal) } catch (e) { }

        };

        $scope.MaskNumero = function () {
            try { $scope.KmUso = MascaraNumero($scope.KmUso) } catch (e) { }
            try { $scope.Subtotal = MascaraNumero($scope.Subtotal) } catch (e) { }
            try { $scope.ValorIVA = MascaraValores($scope.ValorIVA) } catch (e) { }
            try { $scope.ValorTotal = MascaraValores($scope.ValorTotal) } catch (e) { }
            try { $scope.Cantidad = MascaraValores($scope.Cantidad) } catch (e) { }
        };

        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        }

        $scope.MaskPlaca = function () {
            MascaraPlacaGeneral($scope);
        }

    }]);