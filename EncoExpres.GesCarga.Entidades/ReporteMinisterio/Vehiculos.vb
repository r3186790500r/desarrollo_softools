﻿'Imports System.Data
'Imports System.Data.SqlClient


'Namespace Entidades.ReporteMinisterio
'    Public Class Vehiculos

'#Region "Declaracion Variables"

'        Dim strSQL As String
'        Dim objGeneral As General
'        Public strError As String
'        Public bolModificar As Boolean
'        Public bolFoto As Boolean

'        Private objEmpresa As Empresas
'        Private strPlaca As String
'        Private objPropietario As Tercero
'        Private objTenedor As Tercero
'        Private objConductor As Tercero

'        Private objAfiliador As Tercero
'        'Private objSemirremolque As Semiremolque
'        Private objCataTipoCarroceria As Catalogo
'        Private strTarjetaPropiedad As String
'        Private strOrganismoTransito As String

'        Private objCataMarca As Catalogo
'        Private intModelo As Integer
'        Private intModeloRepotenciado As Integer
'        Private objCataTipoVehiculo As Catalogo
'        Private objCataLinea As Catalogo

'        Private intCilindraje As Integer
'        Private strClase As String
'        Private lonColor As Long
'        Private objCataClasificacionVehiculo As Catalogo
'        Private intNumeroEjes As Integer

'        Private strNumeroMotor As String
'        Private strNumeroSerie As String
'        Private intCapacidad As Integer
'        Private dblPesoBruto As Double
'        Private strRegistroNacionalCarga As String

'        Private strPolizaSOAT As String
'        Private dteFechaVenceSOAT As Date
'        Private objAseguradora As Tercero
'        Private strCertificadoContaminacion As String
'        Private dteFechaVenceCertificado As Date

'        Private dblValorVehiculo As Double
'        Private intTiempoUtil As Integer
'        Private dblValorSeguro As Double
'        Private strCarnetEmpresaAfiliadora As String
'        Private dteFechaVenceAfiliadora As Date

'        Private strRegistroNacionalCombustible As String
'        Private bytFoto As Byte()
'        Private dteFechaVenceRegistroCombustible As Date
'        Private dteFechaActualizoKilometraje As Date
'        Private dblKilometraje As Double

'        Private strCodigo As String
'        Private objCataEstado As Catalogo
'        Private dblCapacidadMetrosCubicos As Double
'        Private dteFechaUltimaRevisionMecanica As Date
'        Private objCataCausaInactividad As Catalogo

'        Private objUsuario As Usuario
'        Private objCataTipoEquipo As Catalogo
'        Private strJustificacionBloqueo As String
'        Private intCategoriaPeajes As Short
'        Private objEstudioSeguridad As EstudioSeguiridadVehiculos

'        Private objValidacion As Validacion
'        Private intTieneGPS As Integer
'        Private strNombreEmpresaGPS As String
'        Private strClaveGPS As String
'        Private strTelefonoGPS As String

'        Private strUsuarioGPS As String
'        Private intInterfazAvantel As Byte
'        Private intInterfazTracker As Byte
'        Private intInterfazSatrack As Byte
'        Private intInterfazGpsColombia As Byte

'        Public objCataTipoGps As Catalogo
'        Public objCataProyectoVehiculo As Catalogo
'        Private lonMarca As Long
'        Private strCodigoLinea As String
'        Private intTipoCapacidadVehiculo As Short

'        Private strPoliza_Hidrocarburos As String
'        Private dteFecha_Vence_Poliza_Hidrocarburos As Date
'        Private strPoliza_RC As String
'        Private dteFecha_Vence_Poliza_RC As Date
'        Private strPoliza_RCE As String
'        Private dteFecha_Vence_Poliza_RCE As Date
'        Private IntRegionGeografica As Integer

'#End Region

'#Region "Declaracion Constantes"

'        Public Const BYTES_IMAGEN_DEFECTO As Integer = 0

'        Public Const TERCERO As Integer = 0
'        Public Const PROPIO As Integer = 1
'        Public Const SOCIO As Integer = 2
'        Private Const SIN_CAUSA_INACTIVIDAD = 1
'        Private Const ESTADO_INACTIVO = 0

'        Const ESTADO_ESTUDIO_SEGURIDAD_APROBADO As Integer = 1
'        Public Const TIPO_FUNCIONAMIENTO_KILOMETROS As String = "Kilómetros"
'        Private Const NOMBRE_CLASE As String = "Vehiculos"
'        Public Const CARGUE_TERCEROS As Boolean = True
'        Public Const NO_CARGUE_TERCEROS As Boolean = False

'        Public Const CAUSA_DESBLOQUEADO As String = "1"
'        Public Const ESTADO_ACTIVO As String = "1"
'        Public Const ESTADO_BLOQUEADO As String = "2"
'        Public Const CAUSA_BLOQUEO_NUEVO_VEHICULO As Integer = 24
'        Public Const JUSTIFICACION_BLOQUEO_NUEVO_VEHICULO As String = "Nuevo Vehículo"

'        'Constantes de Validaciones
'        Const REPLICA_INFORMACION As String = "clsVehiFunc1"
'        Const VALIDA_ESTUDIO_SEGURIDAD As String = "clsVehiVali1"
'        Const VALIDA_FECHA_CERTIFICADO_CONTAMINACION As String = "clsVehiVali2"
'        Const VALIDA_FECHA_REGISTRO_COMBUSTIBLE As String = "clsVehiVali3"
'        Const VALIDA_FECHA_REGISTRO_NACIONAL_CARGA As String = "clsVehiVali4"
'        Const VALIDA_ORGANISMO_TRANSITO As String = "clsVehiVali7"
'        Const VALIDA_CERTIFICADO_COMTAMINACION As String = "clsVehiVali8"
'        Const VALIDA_CERTIFICADO_COMBUSTIBLE As String = "clsVehiVali16"
'        Const VALIDA_PESO_VEHICULO As String = "clsVehiVali20_DISPONIBLE"
'        'VALIDA_VALOR_VEHICULO  VALIDA_AÑOS_UTILIDAD
'        Const VALIDA_VALOR_VEHICULO As String = "clsVehiVali5"
'        Const VALIDA_AÑOS_UTILIDAD As String = "clsVehiVali6"
'        Const VALIDA_CLASE_VEHICULO As String = "clsVehiVali9"

'        Const VALIDA_SEMIRREMOLQUE_X_TIPO_VEHICULO As String = "clsVehiVali10"
'        Const VALIDA_REVISION_TECNICOMECANICA As String = "clsVehiVali11"
'        Const VALIDA_FOTO_VEHICULO As String = "clsVehiVali12"
'        Const VALIDA_DATOS_EMPRESA_AFILIADORA As String = "clsVehiVali13"
'        Const VALIDA_FECHAS_VENCIDAS_VEHICULO As String = "clsVehiVali14"
'        Const VALIDA_CODIGO_VEHICULO As String = "clsVehiVali15"
'        Const VALIDA_DATOS_POLIZA_HIDROCARBUROS As String = "clsVehiVali17"
'        Const VALIDA_DATOS_POLIZA_RC As String = "clsVehiVali18"
'        Const VALIDA_DATOS_POLIZA_RCE As String = "clsVehiVali19"
'        Const VALIDA_INGRESO_REGION As String = "clsVehiVali20"

'        Public Const TIPO_VEHICULO_CODIGO_MINISTERIO_50 As Integer = 50
'        Public Const TIPO_VEHICULO_CODIGO_MINISTERIO_51 As Integer = 51
'        Public Const TIPO_VEHICULO_CODIGO_MINISTERIO_52 As Integer = 52
'        Public Const TIPO_VEHICULO_CODIGO_MINISTERIO_53 As Integer = 53
'        Public Const TIPO_VEHICULO_CODIGO_MINISTERIO_54 As Integer = 54
'        Public Const TIPO_LINEA_VEHICULO_SIN_LINEA As String = "SIN LINEA"
'        Public Const OBLIGA_VALOR_FLETE_MINISTERIO As Byte = 1
'        Public Const INTERFAZ_AVANTEL As Byte = 1

'        Public Const ORIGEN_VEHICULO As Integer = 6

'#End Region

'#Region "Constructor"

'        Sub New()

'            Me.objEmpresa = New Empresas(0)
'            Me.strPlaca = ""
'            Me.objPropietario = New Tercero(Me.objEmpresa)
'            Me.objTenedor = New Tercero(Me.objEmpresa)
'            Me.objConductor = New Tercero(Me.objEmpresa)

'            Me.objAfiliador = New Tercero(Me.objEmpresa)
'            'Me.objSemirremolque = New Semirremolque(Me.objEmpresa)
'            Me.objCataClasificacionVehiculo = New Catalogo(Me.objEmpresa, "CLVE")
'            Me.strTarjetaPropiedad = ""
'            Me.strOrganismoTransito = ""

'            Me.intModelo = 0
'            Me.intModeloRepotenciado = 0
'            Me.objCataTipoVehiculo = New Catalogo(Me.objEmpresa, "TIVE")
'            'Me.objCataLinea = New Catalogo(Me.objEmpresa, "LINE")

'            Me.intCilindraje = 0
'            Me.strClase = ""
'            Me.lonColor = 0
'            Me.objCataTipoCarroceria = New Catalogo(Me.objEmpresa, "TICA")
'            Me.intNumeroEjes = 0

'            Me.strNumeroMotor = ""
'            Me.strNumeroSerie = ""
'            Me.intCapacidad = 0
'            Me.dblPesoBruto = 0
'            Me.strRegistroNacionalCarga = ""

'            Me.strPolizaSOAT = ""
'            Me.dteFechaVenceSOAT = Date.Today
'            Me.objAseguradora = New Tercero(Me.objEmpresa)
'            Me.strCertificadoContaminacion = ""
'            Me.dteFechaVenceCertificado = Date.Today

'            Me.dblValorVehiculo = 0
'            Me.intTiempoUtil = 0
'            Me.dblValorSeguro = 0
'            Me.strCarnetEmpresaAfiliadora = ""
'            Me.dteFechaVenceAfiliadora = Date.Today

'            Me.strRegistroNacionalCombustible = ""
'            Me.dteFechaVenceRegistroCombustible = Date.MinValue
'            Me.dteFechaActualizoKilometraje = Date.MinValue
'            Me.dblKilometraje = 0
'            Me.strCodigo = ""

'            Me.objCataEstado = New Catalogo(Me.objEmpresa, "ESTA")
'            Me.objGeneral = New General()
'            Me.dblCapacidadMetrosCubicos = 0
'            Me.dteFechaUltimaRevisionMecanica = Date.MinValue
'            Me.objCataCausaInactividad = New Catalogo(Me.objEmpresa, "CIVE")

'            Me.objCataTipoEquipo = New Catalogo(Me.objEmpresa, "TIEQ")
'            Me.objUsuario = New Usuario(Me.objEmpresa)
'            Me.strJustificacionBloqueo = ""
'            Me.intCategoriaPeajes = 0
'            Me.objValidacion = New Validacion(Me.objEmpresa)

'            Me.intTieneGPS = 0
'            Me.strNombreEmpresaGPS = ""
'            Me.strClaveGPS = ""
'            Me.strTelefonoGPS = ""
'            Me.strUsuarioGPS = String.Empty

'            Me.intInterfazAvantel = 0
'            Me.intInterfazTracker = 0
'            Me.intInterfazSatrack = 0
'            Me.intInterfazGpsColombia = 0
'            Me.objCataTipoGps = New Catalogo(Me.objEmpresa, "TGPS")

'            Me.lonMarca = 0
'            Me.strCodigoLinea = 0
'            Me.intTipoCapacidadVehiculo = 0


'            Me.dteFecha_Vence_Poliza_Hidrocarburos = Date.MinValue
'            Me.strPoliza_Hidrocarburos = String.Empty
'            Me.dteFecha_Vence_Poliza_RC = Date.MinValue
'            Me.strPoliza_RC = String.Empty
'            Me.dteFecha_Vence_Poliza_RCE = Date.MinValue
'            Me.strPoliza_RCE = String.Empty
'            'Me.objRegionGeografica = New Ciudad
'            Me.IntRegionGeografica = 0

'        End Sub

'        Sub New(ByRef Empresa As Empresas)

'            Me.objEmpresa = Empresa
'            Me.strPlaca = ""
'            Me.objPropietario = New Tercero(Me.objEmpresa)
'            Me.objTenedor = New Tercero(Me.objEmpresa)
'            Me.objConductor = New Tercero(Me.objEmpresa)

'            Me.objAfiliador = New Tercero(Me.objEmpresa)
'            'Me.objSemirremolque = New Semirremolque(Me.objEmpresa)
'            Me.objCataClasificacionVehiculo = New Catalogo(Me.objEmpresa, "CLVE")
'            Me.strTarjetaPropiedad = ""
'            Me.strOrganismoTransito = ""

'            Me.intModelo = 0
'            Me.intModeloRepotenciado = 0
'            Me.objCataTipoVehiculo = New Catalogo(Me.objEmpresa, "TIVE")
'            'Me.objCataLinea = New Catalogo(Me.objEmpresa, "LINE")

'            Me.intCilindraje = 0
'            Me.strClase = ""
'            Me.lonColor = 0
'            Me.objCataTipoCarroceria = New Catalogo(Me.objEmpresa, "TICA")
'            Me.intNumeroEjes = 0

'            Me.strNumeroMotor = ""
'            Me.strNumeroSerie = ""
'            Me.intCapacidad = 0
'            Me.dblPesoBruto = 0
'            Me.strRegistroNacionalCarga = ""

'            Me.strPolizaSOAT = ""
'            Me.dteFechaVenceSOAT = Date.Today
'            Me.objAseguradora = New Tercero(Me.objEmpresa)
'            Me.strCertificadoContaminacion = ""
'            Me.dteFechaVenceCertificado = Date.Today

'            Me.dblValorVehiculo = 0
'            Me.intTiempoUtil = 0
'            Me.dblValorSeguro = 0
'            Me.strCarnetEmpresaAfiliadora = ""
'            Me.dteFechaVenceAfiliadora = Date.Today

'            Me.strRegistroNacionalCombustible = ""
'            Me.dteFechaActualizoKilometraje = Date.MinValue
'            Me.dblKilometraje = 0
'            Me.dteFechaVenceRegistroCombustible = Date.MinValue
'            Me.strCodigo = ""

'            Me.objCataEstado = New Catalogo(Me.objEmpresa, "ESTA")
'            Me.objGeneral = New General()
'            Me.dblCapacidadMetrosCubicos = 0
'            Me.dteFechaUltimaRevisionMecanica = Date.MinValue
'            Me.objCataCausaInactividad = New Catalogo(Me.objEmpresa, "CIVE")

'            Me.objCataTipoEquipo = New Catalogo(Me.objEmpresa, "TIEQ")
'            Me.objUsuario = New Usuario(Me.objEmpresa)
'            Me.strJustificacionBloqueo = ""
'            Me.intCategoriaPeajes = 0
'            Me.objValidacion = New Validacion(Me.objEmpresa)

'            Me.intTieneGPS = 0
'            Me.strNombreEmpresaGPS = ""
'            Me.strClaveGPS = ""
'            Me.strTelefonoGPS = ""
'            Me.strUsuarioGPS = String.Empty

'            Me.intInterfazAvantel = 0
'            Me.intInterfazTracker = 0
'            Me.intInterfazSatrack = 0
'            Me.intInterfazGpsColombia = 0
'            Me.objCataTipoGps = New Catalogo(Me.objEmpresa, "TGPS")

'            Me.objCataProyectoVehiculo = New Catalogo(Me.objEmpresa, "PRVE")
'            Me.lonMarca = 0
'            Me.strCodigoLinea = 0
'            Me.intTipoCapacidadVehiculo = 0

'            Me.dteFecha_Vence_Poliza_Hidrocarburos = Date.MinValue
'            Me.strPoliza_Hidrocarburos = String.Empty
'            Me.dteFecha_Vence_Poliza_RC = Date.MinValue
'            Me.strPoliza_RC = String.Empty
'            Me.dteFecha_Vence_Poliza_RCE = Date.MinValue
'            Me.strPoliza_RCE = String.Empty

'        End Sub

'        Sub New(ByVal Empresa As Empresas, ByVal Placa As String)

'            Me.objEmpresa = Empresa
'            Me.strPlaca = Placa
'            Me.objPropietario = New Tercero(Me.objEmpresa)
'            Me.objTenedor = New Tercero(Me.objEmpresa)
'            Me.objConductor = New Tercero(Me.objEmpresa)

'            Me.objAfiliador = New Tercero(Me.objEmpresa)
'            'Me.objSemirremolque = New Semirremolque(Me.objEmpresa)
'            Me.objCataClasificacionVehiculo = New Catalogo(Me.objEmpresa, "CLVE")
'            Me.strTarjetaPropiedad = ""
'            Me.strOrganismoTransito = ""

'            Me.intModelo = 0
'            Me.intModeloRepotenciado = 0
'            Me.objCataTipoVehiculo = New Catalogo(Me.objEmpresa, "TIVE")

'            Me.intCilindraje = 0
'            Me.strClase = ""
'            Me.lonColor = 0
'            Me.objCataTipoCarroceria = New Catalogo(Me.objEmpresa, "TICA")
'            Me.intNumeroEjes = 0

'            Me.strNumeroMotor = ""
'            Me.strNumeroSerie = ""
'            Me.intCapacidad = 0
'            Me.dblPesoBruto = 0
'            Me.strRegistroNacionalCarga = ""

'            Me.strPolizaSOAT = ""
'            Me.dteFechaVenceSOAT = Date.Today
'            Me.objAseguradora = New Tercero(Me.objEmpresa)
'            Me.strCertificadoContaminacion = ""
'            Me.dteFechaVenceCertificado = Date.Today

'            Me.dblValorVehiculo = 0
'            Me.intTiempoUtil = 0
'            Me.dblValorSeguro = 0
'            Me.strCarnetEmpresaAfiliadora = ""
'            Me.dteFechaVenceAfiliadora = Date.Today

'            Me.strRegistroNacionalCombustible = ""
'            Me.dteFechaActualizoKilometraje = Date.MinValue
'            Me.dblKilometraje = 0
'            Me.dteFechaVenceRegistroCombustible = Date.MinValue
'            Me.strCodigo = ""

'            Me.objCataEstado = New Catalogo(Me.objEmpresa, "ESTA")
'            Me.objGeneral = New General()
'            Me.dblCapacidadMetrosCubicos = 0
'            Me.dteFechaUltimaRevisionMecanica = Date.MinValue
'            Me.objCataCausaInactividad = New Catalogo(Me.objEmpresa, "CIVE")

'            Me.objCataTipoEquipo = New Catalogo(Me.objEmpresa, "TIEQ")
'            Me.strJustificacionBloqueo = ""
'            Me.intCategoriaPeajes = 0
'            Me.objValidacion = New Validacion(Me.objEmpresa)
'            Me.intTieneGPS = 0

'            Me.strNombreEmpresaGPS = ""
'            Me.strClaveGPS = ""
'            Me.strTelefonoGPS = ""
'            Me.strUsuarioGPS = String.Empty
'            Me.intInterfazAvantel = 0

'            Me.intInterfazTracker = 0
'            Me.intInterfazSatrack = 0
'            Me.intInterfazGpsColombia = 0
'            Me.objCataTipoGps = New Catalogo(Me.objEmpresa, "TGPS")
'            Me.objCataProyectoVehiculo = New Catalogo(Me.objEmpresa, "PRVE")
'            Me.lonMarca = 0
'            Me.strCodigoLinea = 0
'            Me.intTipoCapacidadVehiculo = 0

'        End Sub

'        Sub New(ByVal Empresa As Empresas, ByVal CargaEmpresaPlaca As Boolean)

'            Me.objEmpresa = Empresa
'            Me.strPlaca = ""
'            Me.objPropietario = New Tercero(Me.objEmpresa, True)
'            Me.objTenedor = New Tercero(Me.objEmpresa, True)
'            Me.objConductor = New Tercero(Me.objEmpresa, True)

'            '   Me.objSemirremolque = New Semirremolque(Me.objEmpresa)
'            Me.objCataTipoVehiculo = New Catalogo(Me.objEmpresa, "TIVE")
'            Me.objCataEstado = New Catalogo(Me.objEmpresa, "ESTA")
'            Me.objGeneral = New General()
'            Me.intInterfazAvantel = 0
'            Me.intInterfazTracker = 0

'        End Sub

'#End Region

'#Region "Atributos"

'        Public Property InterfazAvantel() As Integer
'            Get
'                Return Me.intInterfazAvantel
'            End Get
'            Set(ByVal value As Integer)
'                Me.intInterfazAvantel = Math.Abs(value)
'            End Set
'        End Property

'        Public Property InterfazTracker() As Integer
'            Get
'                Return Me.intInterfazTracker
'            End Get
'            Set(ByVal value As Integer)
'                Me.intInterfazTracker = Math.Abs(value)
'            End Set
'        End Property

'        Public Property InterfazSatrack() As Integer
'            Get
'                Return Me.intInterfazSatrack
'            End Get
'            Set(ByVal value As Integer)
'                Me.intInterfazSatrack = value
'            End Set
'        End Property

'        Public Property InterfazGpsColombia() As Integer
'            Get
'                Return Me.intInterfazGpsColombia
'            End Get
'            Set(ByVal value As Integer)
'                Me.intInterfazGpsColombia = value
'            End Set
'        End Property

'        Public Property CategoriaPeajes() As Integer
'            Get
'                Return Me.intCategoriaPeajes
'            End Get
'            Set(ByVal value As Integer)
'                Me.intCategoriaPeajes = value
'            End Set
'        End Property

'        Public Property PesoBruto() As Double
'            Get
'                Return Me.dblPesoBruto
'            End Get
'            Set(ByVal value As Double)
'                Me.dblPesoBruto = value
'            End Set
'        End Property

'        Public Property ValorSeguro() As Double
'            Get
'                Return Me.dblValorSeguro
'            End Get
'            Set(ByVal value As Double)
'                Me.dblValorSeguro = value
'            End Set
'        End Property

'        Public Property CapacidadMetrosCubicos() As Double
'            Get
'                Return Me.dblCapacidadMetrosCubicos
'            End Get
'            Set(ByVal value As Double)
'                Me.dblCapacidadMetrosCubicos = value
'            End Set
'        End Property

'        Public Property ValorVehiculo() As Double
'            Get
'                Return Me.dblValorVehiculo
'            End Get
'            Set(ByVal value As Double)
'                Me.dblValorVehiculo = value
'            End Set
'        End Property

'        Public Property CataEstado() As Catalogo
'            Get
'                Return Me.objCataEstado
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataEstado = value
'            End Set
'        End Property

'        Public Property CataCausaInactividad() As Catalogo
'            Get
'                Return Me.objCataCausaInactividad
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataCausaInactividad = value
'            End Set
'        End Property

'        Public Property Usuario() As Usuario
'            Get
'                Return Me.objUsuario
'            End Get
'            Set(ByVal value As Usuario)
'                Me.objUsuario = value
'            End Set
'        End Property

'        Public Property FechaVenceAfiliadora() As Date
'            Get
'                Return Me.dteFechaVenceAfiliadora
'            End Get
'            Set(ByVal value As Date)
'                Me.dteFechaVenceAfiliadora = value
'            End Set
'        End Property

'        Public Property FechaUltimaRevisionMecanica() As Date
'            Get
'                Return Me.dteFechaUltimaRevisionMecanica
'            End Get
'            Set(ByVal value As Date)
'                Me.dteFechaUltimaRevisionMecanica = value
'            End Set
'        End Property

'        Public Property FechaVenceCertificado() As Date
'            Get
'                Return Me.dteFechaVenceCertificado
'            End Get
'            Set(ByVal value As Date)
'                Me.dteFechaVenceCertificado = value
'            End Set
'        End Property

'        Public Property FechaVenceSOAT() As Date
'            Get
'                Return Me.dteFechaVenceSOAT
'            End Get
'            Set(ByVal value As Date)
'                Me.dteFechaVenceSOAT = value
'            End Set
'        End Property

'        Public Property Foto() As Byte()
'            Get
'                Return Me.bytFoto
'            End Get
'            Set(ByVal value As Byte())
'                Me.bytFoto = value
'            End Set
'        End Property

'        Public Property Capacidad() As Double
'            Get
'                Return Me.intCapacidad
'            End Get
'            Set(ByVal value As Double)
'                Me.intCapacidad = value
'            End Set
'        End Property

'        Public Property Cilindraje() As Integer
'            Get
'                Return Me.intCilindraje
'            End Get
'            Set(ByVal value As Integer)
'                Me.intCilindraje = value
'            End Set
'        End Property

'        Public Property Kilometraje() As Double
'            Get
'                Return Me.dblKilometraje
'            End Get
'            Set(ByVal value As Double)
'                Me.dblKilometraje = value
'            End Set
'        End Property

'        Public Property Modelo() As Integer
'            Get
'                Return Me.intModelo
'            End Get
'            Set(ByVal value As Integer)
'                Me.intModelo = value
'            End Set
'        End Property

'        Public Property ModeloRepontenciado() As Integer
'            Get
'                Return Me.intModeloRepotenciado
'            End Get
'            Set(ByVal value As Integer)
'                Me.intModeloRepotenciado = value
'            End Set
'        End Property

'        Public Property NumeroEjes() As Integer
'            Get
'                Return Me.intNumeroEjes
'            End Get
'            Set(ByVal value As Integer)
'                Me.intNumeroEjes = value
'            End Set
'        End Property

'        Public Property CataClasificacionVehiculo() As Catalogo
'            Get
'                Return Me.objCataClasificacionVehiculo
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataClasificacionVehiculo = value
'            End Set
'        End Property

'        Public Property CataTipoEquipo() As Catalogo
'            Get
'                Return Me.objCataTipoEquipo
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataTipoEquipo = value
'            End Set
'        End Property

'        Public Property TiempoUtil() As Integer
'            Get
'                Return Me.intTiempoUtil
'            End Get
'            Set(ByVal value As Integer)
'                Me.intTiempoUtil = value
'            End Set
'        End Property

'        Public Property Afiliador() As Tercero
'            Get
'                Return Me.objAfiliador
'            End Get
'            Set(ByVal value As Tercero)
'                Me.objAfiliador = value
'            End Set
'        End Property

'        Public Property Aseguradora() As Tercero
'            Get
'                Return Me.objAseguradora
'            End Get
'            Set(ByVal value As Tercero)
'                Me.objAseguradora = value
'            End Set
'        End Property

'        Public Property Color() As Long
'            Get
'                Return Me.lonColor
'            End Get
'            Set(ByVal value As Long)
'                Me.lonColor = value
'            End Set
'        End Property

'        Public Property CodigoMarca() As Long
'            Get
'                Return Me.lonMarca
'            End Get
'            Set(ByVal value As Long)
'                Me.lonMarca = value
'            End Set
'        End Property

'        Public Property CataTipoVehiculo() As Catalogo
'            Get
'                Return Me.objCataTipoVehiculo
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataTipoVehiculo = value
'            End Set
'        End Property

'        Public Property CataTipoCarroceria() As Catalogo
'            Get
'                Return Me.objCataTipoCarroceria
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataTipoCarroceria = value
'            End Set
'        End Property

'        Public Property Conductor() As Tercero
'            Get
'                Return Me.objConductor
'            End Get
'            Set(ByVal value As Tercero)
'                Me.objConductor = value
'            End Set
'        End Property

'        Public Property FechaActualizoKilometraje() As Date
'            Get
'                Return Me.dteFechaActualizoKilometraje
'            End Get
'            Set(ByVal value As Date)
'                Me.dteFechaActualizoKilometraje = value
'            End Set
'        End Property

'        Public Property FechaVenceRegistroCombustible() As Date
'            Get
'                Return Me.dteFechaVenceRegistroCombustible
'            End Get
'            Set(ByVal value As Date)
'                Me.dteFechaVenceRegistroCombustible = value
'            End Set
'        End Property

'        Public Property Empresa() As Empresas
'            Get
'                Return Me.objEmpresa
'            End Get
'            Set(ByVal value As Empresas)
'                Me.objEmpresa = value
'            End Set
'        End Property

'        Public Property CataLinea() As Catalogo
'            Get
'                Return Me.objCataLinea
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataLinea = value
'            End Set
'        End Property

'        Public Property Propietario() As Tercero
'            Get
'                Return Me.objPropietario
'            End Get
'            Set(ByVal value As Tercero)
'                Me.objPropietario = value
'            End Set
'        End Property

'        'Public Property Semirremolque() As Semirremolque
'        '    Get
'        '        Return Me.objSemirremolque
'        '    End Get
'        '    Set(ByVal value As Semirremolque)
'        '        Me.objSemirremolque = value
'        '    End Set
'        'End Property

'        Public Property Tenedor() As Tercero
'            Get
'                Return Me.objTenedor
'            End Get
'            Set(ByVal value As Tercero)
'                Me.objTenedor = value
'            End Set
'        End Property

'        Public Property CarnetEmpresaAfiliadora() As String
'            Get
'                Return Me.strCarnetEmpresaAfiliadora
'            End Get
'            Set(ByVal value As String)
'                Me.strCarnetEmpresaAfiliadora = value
'            End Set
'        End Property

'        Public Property Codigo() As String
'            Get
'                Return Me.strCodigo
'            End Get
'            Set(ByVal value As String)
'                Me.strCodigo = value
'            End Set
'        End Property

'        Public Property CertificadoContaminacion() As String
'            Get
'                Return Me.strCertificadoContaminacion
'            End Get
'            Set(ByVal value As String)
'                Me.strCertificadoContaminacion = value
'            End Set
'        End Property

'        Public Property Clase() As String
'            Get
'                Return Me.strClase
'            End Get
'            Set(ByVal value As String)
'                Me.strClase = value
'            End Set
'        End Property

'        Public Property NumeroMotor() As String
'            Get
'                Return Me.strNumeroMotor
'            End Get
'            Set(ByVal value As String)
'                Me.strNumeroMotor = value
'            End Set
'        End Property

'        Public Property NumeroSerie() As String
'            Get
'                Return Me.strNumeroSerie
'            End Get
'            Set(ByVal value As String)
'                Me.strNumeroSerie = value
'            End Set
'        End Property

'        Public Property OrganismoTransito() As String
'            Get
'                Return Me.strOrganismoTransito
'            End Get
'            Set(ByVal value As String)
'                Me.strOrganismoTransito = value
'            End Set
'        End Property


'        Public Property Placa() As String
'            Get
'                Return Me.strPlaca
'            End Get
'            Set(ByVal value As String)
'                Me.strPlaca = value
'            End Set
'        End Property

'        Public Property PolizaSOAT() As String
'            Get
'                Return Me.strPolizaSOAT
'            End Get
'            Set(ByVal value As String)
'                Me.strPolizaSOAT = value
'            End Set
'        End Property

'        Public Property RegistroNacionalCarga() As String
'            Get
'                Return Me.strRegistroNacionalCarga
'            End Get
'            Set(ByVal value As String)
'                Me.strRegistroNacionalCarga = value
'            End Set
'        End Property

'        Public Property RegistroNacionalCombustible() As String
'            Get
'                Return Me.strRegistroNacionalCombustible
'            End Get
'            Set(ByVal value As String)
'                Me.strRegistroNacionalCombustible = value
'            End Set
'        End Property

'        Public Property TarjetaPropiedad() As String
'            Get
'                Return Me.strTarjetaPropiedad
'            End Get
'            Set(ByVal value As String)
'                Me.strTarjetaPropiedad = value
'            End Set
'        End Property

'        Public Property JustificacionBloqueo() As String
'            Get
'                Return Me.strJustificacionBloqueo
'            End Get
'            Set(ByVal value As String)
'                Me.strJustificacionBloqueo = value
'            End Set
'        End Property

'        Public Property EstudioSeguridad() As EstudioSeguiridadVehiculos
'            Get
'                If IsNothing(Me.objEstudioSeguridad) Then
'                    Me.objEstudioSeguridad = New EstudioSeguiridadVehiculos(Me.objEmpresa)
'                End If
'                Return Me.objEstudioSeguridad
'            End Get
'            Set(ByVal value As EstudioSeguiridadVehiculos)
'                If IsNothing(Me.objEstudioSeguridad) Then
'                    Me.objEstudioSeguridad = New EstudioSeguiridadVehiculos(Me.objEmpresa)
'                End If
'                Me.objEstudioSeguridad = value
'            End Set
'        End Property


'        Public Property TieneGPS() As Integer
'            Get
'                Return intTieneGPS
'            End Get
'            Set(ByVal value As Integer)
'                intTieneGPS = Math.Abs(value)
'            End Set
'        End Property

'        Public Property NombreEmpresaGPS() As String
'            Get
'                Return strNombreEmpresaGPS
'            End Get
'            Set(ByVal value As String)
'                strNombreEmpresaGPS = value
'            End Set
'        End Property

'        Public Property ClaveGPS() As String
'            Get
'                Return strClaveGPS
'            End Get
'            Set(ByVal value As String)
'                strClaveGPS = value
'            End Set
'        End Property

'        Public Property TelefonoGPS() As String
'            Get
'                Return strTelefonoGPS
'            End Get
'            Set(ByVal value As String)
'                strTelefonoGPS = value
'            End Set
'        End Property

'        Public Property UsuarioGPS() As String
'            Get
'                Return Me.strUsuarioGPS
'            End Get
'            Set(ByVal value As String)
'                Me.strUsuarioGPS = value
'            End Set
'        End Property

'        Public Property CataTipoGPS() As Catalogo
'            Get
'                Return Me.objCataTipoGps
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataTipoGps = value
'            End Set
'        End Property

'        Public Property CataProyectoVehiculo() As Catalogo
'            Get
'                Return Me.objCataProyectoVehiculo
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataEstado = value
'            End Set
'        End Property

'        Public Property CodigoLinea As String
'            Get
'                Return Me.strCodigoLinea
'            End Get
'            Set(value As String)
'                Me.strCodigoLinea = value
'            End Set
'        End Property

'        Public Property TipoCapacidadVehiculo As Short
'            Get
'                Return Me.intTipoCapacidadVehiculo
'            End Get
'            Set(value As Short)
'                Me.intTipoCapacidadVehiculo = value
'            End Set
'        End Property

'        Public Property Poliza_Hidrocarburos As String
'            Get
'                Return Me.strPoliza_Hidrocarburos
'            End Get
'            Set(value As String)
'                Me.strPoliza_Hidrocarburos = value
'            End Set
'        End Property

'        Public Property Fecha_Vence_Poliza_Hidrocarburos As Date
'            Get
'                Return Me.dteFecha_Vence_Poliza_Hidrocarburos
'            End Get
'            Set(value As Date)
'                Me.dteFecha_Vence_Poliza_Hidrocarburos = value
'            End Set
'        End Property

'        Public Property Poliza_RC As String
'            Get
'                Return Me.strPoliza_RC
'            End Get
'            Set(value As String)
'                Me.strPoliza_RC = value
'            End Set
'        End Property

'        Public Property Fecha_Vence_Poliza_RC As Date
'            Get
'                Return Me.dteFecha_Vence_Poliza_RC
'            End Get
'            Set(value As Date)
'                Me.dteFecha_Vence_Poliza_RC = value
'            End Set
'        End Property
'        Public Property Poliza_RCE As String
'            Get
'                Return Me.strPoliza_RCE
'            End Get
'            Set(value As String)
'                Me.strPoliza_RCE = value
'            End Set
'        End Property

'        Public Property Fecha_Vence_Poliza_RCE As Date
'            Get
'                Return Me.dteFecha_Vence_Poliza_RCE
'            End Get
'            Set(value As Date)
'                Me.dteFecha_Vence_Poliza_RCE = value
'            End Set
'        End Property

'        'Public Property RegionGeografica() As Ciudad
'        '    Get
'        '        Return Me.objRegionGeografica
'        '    End Get
'        '    Set(ByVal value As Ciudad)
'        '        Me.objRegionGeografica = value
'        '    End Set
'        'End Property
'        Public Property RegionGeografica() As Integer
'            Get
'                Return Me.IntRegionGeografica
'            End Get
'            Set(ByVal value As Integer)
'                Me.IntRegionGeografica = value
'            End Set
'        End Property
'#End Region

'#Region "Funciones Publicas"

'        Public Function Cargar_Vehiculo() As Boolean
'            Cargar_Vehiculo = False
'        End Function

'        Public Function Consultar_Vehiculo() As Boolean
'            Try
'                Dim sdrVehiculo As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo, Placa, TERC_Propietario, TERC_Tenedor, TERC_Conductor, "
'                strSQL += "TERC_Afiliador, SEMI_Placa, Propio, REGE_Codigo, Tarjeta_Propiedad, Organismo_Transito, "
'                strSQL += "MARC_Codigo, Modelo, Modelo_Repotenciado, TIVE_Codigo, LINE_Codigo, "
'                strSQL += "TIEQ_Codigo, Cilindraje, Clase, COLO_Codigo, TICA_Codigo, "
'                strSQL += "Numero_Ejes, Numero_Motor, Numero_Serie, Capacidad, Capacidad_Metro_Cubicos, "
'                strSQL += "Peso_Bruto, Registro_Nacional_Carga, Poliza_SOAT, Fecha_Vence_SOAT, TERC_Aseguradora, "
'                strSQL += "Certificado_Contaminacion, Fecha_Vence_Certificado, Valor_Vehiculo, Tiempo_Util,Valor_Seguro, "
'                strSQL += "Carnet_Empresa_Afiliadora, Vence_Empresa_Afiliadora, Registro_Nacional_Combustible, Vence_Registro_Combustible, Categoria_Peajes, "
'                strSQL += "Codigo, Estado, CIVE_Codigo, ESSE_Numero, Kilometraje, "
'                strSQL += "Fecha_Actualiza_Kilometraje, Fecha_Ultima_Revision_Mecanica,Poliza_Hidrocarburos,Fecha_Vence_Poliza_Hidrocarburos,Poliza_RC,Fecha_Vence_Poliza_RC,"
'                strSQL += "Poliza_RCE,Fecha_Vence_Poliza_RCE,Fecha_Crea, USUA_Crea, Fecha_Modifica, "
'                strSQL += "USUA_Modifica, OFIC_Codigo, Justificacion_Bloqueo, Tiene_GPS, Nombre_Empresa_GPS, "
'                strSQL += "Clave_GPS, Telefono_GPS, Usuario_GPS, TGPS_Codigo, Interfaz_Avantel, "
'                strSQL += "Interfaz_Tracker, Interfaz_Satrack, Interfaz_GPS_Colombia, PRVE_Codigo,"
'                strSQL += "'Foto' = CASE WHEN Foto IS NULL THEN NULL ELSE 1 END, CAVE_Codigo "
'                strSQL += " FROM Vehiculos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Placa = '" & Me.strPlaca & "'"

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Call Instanciar_Objetos_Alternos()

'                Me.objGeneral.ConexionSQL.Open()
'                sdrVehiculo = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrVehiculo.Read() Then

'                    'Información Conductor
'                    Dim arrCampos(7), arrValoresCampos(7) As String

'                    arrCampos(0) = "Numero_Identificacion"
'                    arrCampos(1) = "Nombre"
'                    arrCampos(2) = "Apellido1"
'                    arrCampos(3) = "Apellido2"
'                    arrCampos(4) = "Telefono1"
'                    arrCampos(5) = "Celular"
'                    arrCampos(6) = "Estado"

'                    If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "Terceros", arrCampos, arrValoresCampos, 7, General.CAMPO_NUMERICO, "Codigo", sdrVehiculo("TERC_Propietario").ToString, "", Me.strError) Then
'                        Me.objPropietario.Codigo = sdrVehiculo("TERC_Propietario").ToString
'                        Me.objPropietario.NumeroIdentificacion = arrValoresCampos(0)
'                        Me.objPropietario.Nombre = arrValoresCampos(1) & " " & arrValoresCampos(2) & " " & arrValoresCampos(3)
'                        Me.objPropietario.Telefono1 = arrValoresCampos(4)
'                        Me.objPropietario.Celular = arrValoresCampos(5)
'                        Me.objPropietario.CataEstado.Campo1 = arrValoresCampos(6)
'                    End If

'                    If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "Terceros", arrCampos, arrValoresCampos, 7, General.CAMPO_NUMERICO, "Codigo", sdrVehiculo("TERC_Conductor").ToString, "", Me.strError) Then
'                        Me.objConductor.Codigo = sdrVehiculo("TERC_Conductor").ToString
'                        Me.objConductor.NumeroIdentificacion = arrValoresCampos(0)
'                        Me.objConductor.Nombre = arrValoresCampos(1) & " " & arrValoresCampos(2) & " " & arrValoresCampos(3)
'                        Me.objConductor.Telefono1 = arrValoresCampos(4)
'                        Me.objConductor.Celular = arrValoresCampos(5)
'                        Me.objConductor.CataEstado.Campo1 = arrValoresCampos(6)
'                    End If

'                    If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "Terceros", arrCampos, arrValoresCampos, 7, General.CAMPO_NUMERICO, "Codigo", sdrVehiculo("TERC_Tenedor").ToString, "", Me.strError) Then
'                        Me.objTenedor.Codigo = sdrVehiculo("TERC_Tenedor").ToString
'                        Me.objTenedor.NumeroIdentificacion = arrValoresCampos(0)
'                        Me.objTenedor.Nombre = arrValoresCampos(1) & " " & arrValoresCampos(2) & " " & arrValoresCampos(3)
'                        Me.objTenedor.Telefono1 = arrValoresCampos(4)
'                        Me.objTenedor.Celular = arrValoresCampos(5)
'                    End If

'                    If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "Terceros", arrCampos, arrValoresCampos, 7, General.CAMPO_NUMERICO, "Codigo", sdrVehiculo("TERC_Afiliador").ToString, "", Me.strError) Then
'                        Me.objAfiliador.Codigo = sdrVehiculo("TERC_Afiliador").ToString
'                        Me.objAfiliador.NumeroIdentificacion = arrValoresCampos(0)
'                        Me.objAfiliador.Nombre = arrValoresCampos(1) & " " & arrValoresCampos(2) & " " & arrValoresCampos(3)
'                        Me.objAfiliador.Telefono1 = arrValoresCampos(4)
'                        Me.objAfiliador.Celular = arrValoresCampos(5)
'                    End If

'                    '  Me.objSemirremolque.Placa = sdrVehiculo("SEMI_Placa").ToString()

'                    Me.objCataClasificacionVehiculo.Campo1 = (Val(sdrVehiculo("Propio").ToString()))
'                    Me.strTarjetaPropiedad = Val(sdrVehiculo("Tarjeta_Propiedad").ToString())
'                    Me.strOrganismoTransito = sdrVehiculo("Organismo_Transito").ToString()
'                    Me.lonMarca = Val(sdrVehiculo("MARC_Codigo").ToString())
'                    Me.intModelo = Val(sdrVehiculo("modelo").ToString())

'                    Me.intModeloRepotenciado = Val(sdrVehiculo("Modelo_Repotenciado").ToString())
'                    Me.objCataTipoVehiculo.Campo1 = (sdrVehiculo("TIVE_Codigo").ToString())
'                    Me.strCodigoLinea = Trim(sdrVehiculo("LINE_Codigo").ToString())
'                    Me.intCilindraje = sdrVehiculo("Cilindraje").ToString()
'                    Me.strClase = sdrVehiculo("Clase").ToString()

'                    Me.lonColor = Val(sdrVehiculo("COLO_Codigo").ToString())
'                    Me.objCataTipoCarroceria.Campo1 = (sdrVehiculo("TICA_Codigo").ToString())
'                    Me.intNumeroEjes = Val(sdrVehiculo("Numero_Ejes").ToString())
'                    Me.strNumeroMotor = sdrVehiculo("Numero_Motor").ToString()
'                    Me.strNumeroSerie = sdrVehiculo("Numero_Serie").ToString()

'                    Me.intCapacidad = Val(sdrVehiculo("Capacidad").ToString())
'                    Me.dblPesoBruto = Val(sdrVehiculo("Peso_Bruto").ToString())
'                    Me.strRegistroNacionalCarga = sdrVehiculo("Registro_Nacional_Carga").ToString()
'                    Me.strPolizaSOAT = sdrVehiculo("Poliza_SOAT").ToString()
'                    Me.objCataCausaInactividad.Campo1 = (sdrVehiculo("CIVE_Codigo").ToString())

'                    Me.dteFechaVenceSOAT = Date.Parse(sdrVehiculo("Fecha_Vence_Soat").ToString())
'                    Me.objAseguradora.Codigo = Val(sdrVehiculo("TERC_Aseguradora").ToString())
'                    Me.strCertificadoContaminacion = sdrVehiculo("Certificado_Contaminacion").ToString()
'                    Me.dblValorVehiculo = Val(sdrVehiculo("Valor_Vehiculo").ToString())
'                    Me.intTiempoUtil = Val(sdrVehiculo("Tiempo_Util").ToString())

'                    Me.dblValorSeguro = Val(sdrVehiculo("Valor_Seguro").ToString())
'                    Me.strCarnetEmpresaAfiliadora = sdrVehiculo("Carnet_Empresa_Afiliadora").ToString()
'                    Me.dteFechaVenceAfiliadora = Date.Parse(sdrVehiculo("Vence_Empresa_Afiliadora").ToString())
'                    Me.strRegistroNacionalCombustible = sdrVehiculo("Registro_Nacional_Combustible").ToString()
'                    Me.dteFechaVenceCertificado = Date.Parse(sdrVehiculo("Fecha_Vence_Certificado").ToString())

'                    Me.dteFechaVenceRegistroCombustible = Date.Parse(sdrVehiculo("Vence_Registro_Combustible").ToString())
'                    Date.TryParse(sdrVehiculo("Fecha_Actualiza_Kilometraje").ToString(), Me.dteFechaActualizoKilometraje)
'                    Me.dblKilometraje = Val(sdrVehiculo("Kilometraje").ToString())
'                    Me.dblCapacidadMetrosCubicos = Val(sdrVehiculo("Capacidad_Metro_Cubicos").ToString())
'                    Date.TryParse(sdrVehiculo("Fecha_Ultima_Revision_Mecanica").ToString(), Me.dteFechaUltimaRevisionMecanica)

'                    Me.CataEstado.Campo1 = (sdrVehiculo("Estado").ToString())
'                    Me.strCodigo = sdrVehiculo("Codigo").ToString()
'                    'Me.Codigo =sdrVehiculo("Codigo").ToString()
'                    Me.objCataTipoEquipo.Campo1 = (sdrVehiculo("TIEQ_Codigo").ToString())
'                    Me.strJustificacionBloqueo = sdrVehiculo("Justificacion_Bloqueo").ToString()
'                    Me.intCategoriaPeajes = Val(sdrVehiculo("Categoria_Peajes").ToString())
'                    Me.IntRegionGeografica = (sdrVehiculo("REGE_Codigo").ToString())

'                    If Not IsNothing(Me.objEstudioSeguridad) Then
'                        If sdrVehiculo("ESSE_Numero") IsNot DBNull.Value Then
'                            ReDim arrCampos(2), arrValoresCampos(2)

'                            arrCampos(0) = "Numero"
'                            arrCampos(1) = "Fecha"
'                            arrCampos(2) = "Fecha_Aprueba"

'                            If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "Estudio_Seguridad_Vehiculos", arrCampos, arrValoresCampos, arrCampos.Length, General.CAMPO_NUMERICO, "Numero", sdrVehiculo("ESSE_Numero").ToString, "", Me.strError) Then
'                                Me.objEstudioSeguridad.Numero = sdrVehiculo("ESSE_Numero").ToString
'                                Me.objEstudioSeguridad.Fecha = arrValoresCampos(1)
'                                Me.objEstudioSeguridad.FechaAprueba = arrValoresCampos(2)
'                            End If
'                        End If
'                    End If

'                    If sdrVehiculo("Foto") IsNot DBNull.Value Then
'                        bolFoto = True
'                    Else
'                        bolFoto = False
'                    End If
'                    Consultar_Vehiculo = True
'                Else
'                    Me.strPlaca = ""
'                    Consultar_Vehiculo = False
'                End If

'                Me.intTieneGPS = Val(sdrVehiculo("Tiene_GPS").ToString())
'                Me.strNombreEmpresaGPS = sdrVehiculo("Nombre_Empresa_GPS").ToString()
'                Me.strClaveGPS = sdrVehiculo("Clave_GPS").ToString()
'                Me.strTelefonoGPS = sdrVehiculo("Telefono_GPS").ToString()
'                Me.strUsuarioGPS = sdrVehiculo("Usuario_GPS").ToString()
'                Me.objCataTipoGps.Campo1 = sdrVehiculo("TGPS_Codigo").ToString()
'                Me.intInterfazAvantel = sdrVehiculo("Interfaz_Avantel").ToString()
'                Me.intInterfazTracker = sdrVehiculo("Interfaz_Tracker").ToString()
'                Me.intInterfazSatrack = sdrVehiculo("Interfaz_Satrack").ToString()
'                Me.intInterfazGpsColombia = sdrVehiculo("Interfaz_GPS_Colombia").ToString()
'                Me.objCataProyectoVehiculo.Campo1 = (sdrVehiculo("PRVE_Codigo").ToString())
'                Me.intTipoCapacidadVehiculo = Val(sdrVehiculo("CAVE_Codigo").ToString)

'                Me.strPoliza_Hidrocarburos = sdrVehiculo("Poliza_Hidrocarburos").ToString
'                Me.dteFecha_Vence_Poliza_Hidrocarburos = Date.Parse(sdrVehiculo("Fecha_Vence_Poliza_Hidrocarburos").ToString)

'                Me.strPoliza_RC = sdrVehiculo("Poliza_RC").ToString
'                Me.dteFecha_Vence_Poliza_RC = Date.Parse(sdrVehiculo("Fecha_Vence_Poliza_RC").ToString)

'                Me.strPoliza_RCE = sdrVehiculo("Poliza_RCE").ToString
'                Me.dteFecha_Vence_Poliza_RCE = Date.Parse(sdrVehiculo("Fecha_Vence_Poliza_RCE").ToString)

'                sdrVehiculo.Close()
'                sdrVehiculo.Dispose()

'            Catch ex As Exception
'                Me.strPlaca = ""
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Consultar_Vehiculo = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Consultar() As Boolean
'            Try
'                Dim sdrVehiculo As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo, Placa, TERC_Propietario, TERC_Tenedor, TERC_Conductor, "
'                strSQL += "TERC_Afiliador, SEMI_Placa, Propio, REGE_Codigo, Tarjeta_Propiedad, Organismo_Transito, "
'                strSQL += "MARC_Codigo, Modelo, Modelo_Repotenciado, TIVE_Codigo, LINE_Codigo, "
'                strSQL += "TIEQ_Codigo, Cilindraje, Clase, COLO_Codigo, TICA_Codigo, "
'                strSQL += "Numero_Ejes, Numero_Motor, Numero_Serie, Capacidad, Capacidad_Metro_Cubicos, "
'                strSQL += "Peso_Bruto, Registro_Nacional_Carga, Poliza_SOAT, Fecha_Vence_SOAT, TERC_Aseguradora, "
'                strSQL += "Certificado_Contaminacion, Fecha_Vence_Certificado, Valor_Vehiculo, Tiempo_Util,Valor_Seguro, "
'                strSQL += "Carnet_Empresa_Afiliadora, Vence_Empresa_Afiliadora, Registro_Nacional_Combustible, Vence_Registro_Combustible, Categoria_Peajes, "
'                strSQL += "Codigo, Estado, CIVE_Codigo, ESSE_Numero, Kilometraje, "
'                strSQL += "Fecha_Actualiza_Kilometraje, Fecha_Ultima_Revision_Mecanica, Fecha_Crea, USUA_Crea, Fecha_Modifica, "
'                strSQL += "USUA_Modifica, OFIC_Codigo, Justificacion_Bloqueo, Tiene_GPS, Nombre_Empresa_GPS, "
'                strSQL += "Clave_GPS, Telefono_GPS, Usuario_GPS, Interfaz_Avantel, Interfaz_Tracker, TGPS_Codigo, Interfaz_Satrack, Interfaz_GPS_Colombia, "
'                strSQL += "'Foto' = CASE WHEN Foto IS NULL THEN NULL ELSE 1 END, CAVE_Codigo "
'                strSQL += " FROM Vehiculos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Placa = '" & Me.strPlaca & "'"

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Call Instanciar_Objetos_Alternos()

'                Me.objGeneral.ConexionSQL.Open()
'                sdrVehiculo = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrVehiculo.Read() Then

'                    'Información Terceros
'                    Dim arrCampos(7), arrValoresCampos(7) As String

'                    arrCampos(0) = "Numero_Identificacion"
'                    arrCampos(1) = "Nombre"
'                    arrCampos(2) = "Apellido1"
'                    arrCampos(3) = "Apellido2"
'                    arrCampos(4) = "Telefono1"
'                    arrCampos(5) = "Celular"
'                    arrCampos(6) = "Estado"

'                    If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "Terceros", arrCampos, arrValoresCampos, 7, General.CAMPO_NUMERICO, "Codigo", sdrVehiculo("TERC_Propietario").ToString, "", Me.strError) Then
'                        Me.objPropietario.Codigo = sdrVehiculo("TERC_Propietario").ToString
'                        Me.objPropietario.NumeroIdentificacion = arrValoresCampos(0)
'                        Me.objPropietario.Nombre = arrValoresCampos(1) & " " & arrValoresCampos(2) & " " & arrValoresCampos(3)
'                        Me.objPropietario.Telefono1 = arrValoresCampos(4)
'                        Me.objPropietario.Celular = arrValoresCampos(5)
'                        Me.objPropietario.CataEstado.Campo1 = arrValoresCampos(6)
'                    End If

'                    If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "Terceros", arrCampos, arrValoresCampos, 7, General.CAMPO_NUMERICO, "Codigo", sdrVehiculo("TERC_Conductor").ToString, "", Me.strError) Then
'                        Me.objConductor.Codigo = sdrVehiculo("TERC_Conductor").ToString
'                        Me.objConductor.NumeroIdentificacion = arrValoresCampos(0)
'                        Me.objConductor.Nombre = arrValoresCampos(1) & " " & arrValoresCampos(2) & " " & arrValoresCampos(3)
'                        Me.objConductor.Telefono1 = arrValoresCampos(4)
'                        Me.objConductor.Celular = arrValoresCampos(5)
'                        Me.objConductor.CataEstado.Campo1 = arrValoresCampos(6)
'                    End If

'                    If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "Terceros", arrCampos, arrValoresCampos, 7, General.CAMPO_NUMERICO, "Codigo", sdrVehiculo("TERC_Tenedor").ToString, "", Me.strError) Then
'                        Me.objTenedor.Codigo = sdrVehiculo("TERC_Tenedor").ToString
'                        Me.objTenedor.NumeroIdentificacion = arrValoresCampos(0)
'                        Me.objTenedor.Nombre = arrValoresCampos(1) & " " & arrValoresCampos(2) & " " & arrValoresCampos(3)
'                        Me.objTenedor.Telefono1 = arrValoresCampos(4)
'                        Me.objTenedor.Celular = arrValoresCampos(5)
'                    End If

'                    If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "Terceros", arrCampos, arrValoresCampos, 7, General.CAMPO_NUMERICO, "Codigo", sdrVehiculo("TERC_Afiliador").ToString, "", Me.strError) Then
'                        Me.objAfiliador.Codigo = sdrVehiculo("TERC_Afiliador").ToString
'                        Me.objAfiliador.NumeroIdentificacion = arrValoresCampos(0)
'                        Me.objAfiliador.Nombre = arrValoresCampos(1) & " " & arrValoresCampos(2) & " " & arrValoresCampos(3)
'                        Me.objAfiliador.Telefono1 = arrValoresCampos(4)
'                        Me.objAfiliador.Celular = arrValoresCampos(5)
'                    End If


'                    '   Me.objSemirremolque.Placa = sdrVehiculo("SEMI_Placa").ToString()
'                    Me.objCataClasificacionVehiculo.Campo1 = (Val(sdrVehiculo("Propio").ToString()))
'                    Me.strTarjetaPropiedad = Val(sdrVehiculo("Tarjeta_Propiedad").ToString())
'                    Me.strOrganismoTransito = sdrVehiculo("Organismo_Transito").ToString()
'                    Me.lonMarca = Val(sdrVehiculo("MARC_Codigo").ToString())


'                    Me.intModelo = Val(sdrVehiculo("modelo").ToString())
'                    Me.intModeloRepotenciado = Val(sdrVehiculo("Modelo_Repotenciado").ToString())
'                    Me.objCataTipoVehiculo.Campo1 = (sdrVehiculo("TIVE_Codigo").ToString())
'                    Me.strCodigoLinea = Trim(sdrVehiculo("LINE_Codigo").ToString())
'                    Me.intCilindraje = sdrVehiculo("Cilindraje").ToString()

'                    Me.strClase = sdrVehiculo("Clase").ToString()
'                    Me.lonColor = Val(sdrVehiculo("COLO_Codigo").ToString())
'                    Me.objCataTipoCarroceria.Campo1 = (sdrVehiculo("TICA_Codigo").ToString())
'                    Me.intNumeroEjes = Val(sdrVehiculo("Numero_Ejes").ToString())
'                    Me.strNumeroMotor = sdrVehiculo("Numero_Motor").ToString()

'                    Me.strNumeroSerie = sdrVehiculo("Numero_Serie").ToString()
'                    Me.intCapacidad = Val(sdrVehiculo("Capacidad").ToString())
'                    Me.dblPesoBruto = Val(sdrVehiculo("Peso_Bruto").ToString())
'                    Me.strRegistroNacionalCarga = sdrVehiculo("Registro_Nacional_Carga").ToString()
'                    Me.strPolizaSOAT = sdrVehiculo("Poliza_SOAT").ToString()

'                    Me.objCataCausaInactividad.Campo1 = (sdrVehiculo("CIVE_Codigo").ToString())
'                    Me.dteFechaVenceSOAT = Date.Parse(sdrVehiculo("Fecha_Vence_Soat").ToString())
'                    Me.objAseguradora.Codigo = Val(sdrVehiculo("TERC_Aseguradora").ToString())
'                    Me.strCertificadoContaminacion = sdrVehiculo("Certificado_Contaminacion").ToString()
'                    Me.dblValorVehiculo = Val(sdrVehiculo("Valor_Vehiculo").ToString())

'                    Me.intTiempoUtil = Val(sdrVehiculo("Tiempo_Util").ToString())
'                    Me.dblValorSeguro = Val(sdrVehiculo("Valor_Seguro").ToString())
'                    Me.strCarnetEmpresaAfiliadora = sdrVehiculo("Carnet_Empresa_Afiliadora").ToString()
'                    Me.dteFechaVenceAfiliadora = Date.Parse(sdrVehiculo("Vence_Empresa_Afiliadora").ToString())
'                    Me.strRegistroNacionalCombustible = sdrVehiculo("Registro_Nacional_Combustible").ToString()

'                    Me.dteFechaVenceCertificado = Date.Parse(sdrVehiculo("Fecha_Vence_Certificado").ToString())
'                    Me.dteFechaVenceRegistroCombustible = Date.Parse(sdrVehiculo("Vence_Registro_Combustible").ToString())
'                    Date.TryParse(sdrVehiculo("Fecha_Actualiza_Kilometraje").ToString(), Me.dteFechaActualizoKilometraje)
'                    Me.dblKilometraje = Val(sdrVehiculo("Kilometraje").ToString())
'                    Me.dblCapacidadMetrosCubicos = Val(sdrVehiculo("Capacidad_Metro_Cubicos").ToString())

'                    Date.TryParse(sdrVehiculo("Fecha_Ultima_Revision_Mecanica").ToString(), Me.dteFechaUltimaRevisionMecanica)
'                    Me.CataEstado.Campo1 = Val(sdrVehiculo("Estado").ToString())
'                    Me.strCodigo = sdrVehiculo("Codigo").ToString()
'                    Me.objCataTipoEquipo.Campo1 = (sdrVehiculo("TIEQ_Codigo").ToString())
'                    Me.strJustificacionBloqueo = sdrVehiculo("Justificacion_Bloqueo").ToString()

'                    Me.intCategoriaPeajes = Val(sdrVehiculo("Categoria_Peajes").ToString())

'                    If Not IsNothing(Me.objEstudioSeguridad) Then
'                        If sdrVehiculo("ESSE_Numero") IsNot DBNull.Value Then
'                            ReDim arrCampos(2), arrValoresCampos(2)

'                            arrCampos(0) = "Numero"
'                            arrCampos(1) = "Fecha"
'                            arrCampos(2) = "Fecha_Aprueba"

'                            If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "Estudio_Seguridad_Vehiculos", arrCampos, arrValoresCampos, arrCampos.Length, General.CAMPO_NUMERICO, "Numero", sdrVehiculo("ESSE_Numero").ToString, "", Me.strError) Then
'                                Me.objEstudioSeguridad.Numero = sdrVehiculo("ESSE_Numero").ToString
'                                Me.objEstudioSeguridad.Fecha = arrValoresCampos(1)
'                                Me.objEstudioSeguridad.FechaAprueba = arrValoresCampos(2)
'                            End If
'                        End If
'                    End If

'                    If sdrVehiculo("Foto") IsNot DBNull.Value Then
'                        bolFoto = True
'                    Else
'                        bolFoto = False
'                    End If
'                    Consultar = True
'                Else
'                    Me.strPlaca = ""
'                    Consultar = False
'                End If

'                Me.intTieneGPS = Val(sdrVehiculo("Tiene_GPS").ToString())
'                Me.strNombreEmpresaGPS = sdrVehiculo("Nombre_Empresa_GPS").ToString()
'                Me.strClaveGPS = sdrVehiculo("Clave_GPS").ToString()
'                Me.strTelefonoGPS = sdrVehiculo("Telefono_GPS").ToString()
'                Me.strUsuarioGPS = sdrVehiculo("Usuario_GPS").ToString()

'                Me.intInterfazAvantel = sdrVehiculo("Interfaz_Avantel").ToString()
'                Me.intInterfazTracker = sdrVehiculo("Interfaz_Tracker").ToString()
'                Me.objCataTipoGps.Campo1 = sdrVehiculo("TGPS_Codigo").ToString()
'                Me.intInterfazSatrack = sdrVehiculo("Interfaz_Satrack").ToString()
'                Me.intInterfazGpsColombia = sdrVehiculo("Interfaz_GPS_Colombia").ToString()
'                Me.intTipoCapacidadVehiculo = Val(sdrVehiculo("CAVE_Codigo").ToString())
'                Me.IntRegionGeografica = (sdrVehiculo("REGE_Codigo").ToString())

'                sdrVehiculo.Close()
'                sdrVehiculo.Dispose()

'            Catch ex As Exception
'                Me.strPlaca = ""
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)

'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Consultar(ByRef Empresa As Empresas, ByVal Placa As String, Optional ByVal bolCargarTercero As Boolean = True) As Boolean

'            Try
'                Me.objEmpresa = Empresa
'                Me.strPlaca = Placa

'                Dim sdrVehiculo As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo, Placa, TERC_Propietario, TERC_Tenedor, TERC_Conductor, "
'                strSQL += "TERC_Afiliador, SEMI_Placa, Propio, REGE_Codigo, Tarjeta_Propiedad, Organismo_Transito, "
'                strSQL += "MARC_Codigo, Modelo, Modelo_Repotenciado, TIVE_Codigo, LINE_Codigo, "
'                strSQL += "TIEQ_Codigo, Cilindraje, Clase, COLO_Codigo, TICA_Codigo, "
'                strSQL += "Numero_Ejes, Numero_Motor, Numero_Serie, Capacidad, Capacidad_Metro_Cubicos, "
'                strSQL += "Peso_Bruto, Registro_Nacional_Carga, Poliza_SOAT, Fecha_Vence_SOAT, TERC_Aseguradora, "
'                strSQL += "Certificado_Contaminacion, Fecha_Vence_Certificado, Valor_Vehiculo, Tiempo_Util,Valor_Seguro, "
'                strSQL += "Carnet_Empresa_Afiliadora, Vence_Empresa_Afiliadora, Registro_Nacional_Combustible, Vence_Registro_Combustible, Categoria_Peajes, "
'                strSQL += "Codigo, Estado, CIVE_Codigo, ESSE_Numero, Kilometraje, "
'                strSQL += "Fecha_Actualiza_Kilometraje, Fecha_Ultima_Revision_Mecanica, Fecha_Crea, USUA_Crea, Fecha_Modifica, "
'                strSQL += "USUA_Modifica, OFIC_Codigo, Justificacion_Bloqueo, Tiene_GPS, Nombre_Empresa_GPS, "
'                strSQL += "Clave_GPS, Telefono_GPS, Usuario_GPS, Interfaz_Avantel, Interfaz_Tracker, Interfaz_Satrack, TGPS_Codigo, Interfaz_GPS_Colombia, "
'                strSQL += "'Foto' = CASE WHEN Foto IS NULL THEN NULL ELSE 1 END, CAVE_Codigo "
'                strSQL += " FROM Vehiculos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Placa = '" & Me.strPlaca & "'"

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Call Instanciar_Objetos_Alternos()

'                Me.objGeneral.ConexionSQL.Open()
'                sdrVehiculo = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrVehiculo.Read() Then

'                    'Información Terceros
'                    Dim arrCampos(7), arrValoresCampos(7) As String

'                    arrCampos(0) = "Numero_Identificacion"
'                    arrCampos(1) = "Nombre"
'                    arrCampos(2) = "Apellido1"
'                    arrCampos(3) = "Apellido2"
'                    arrCampos(4) = "Telefono1"
'                    arrCampos(5) = "Celular"
'                    arrCampos(6) = "Estado"


'                    If bolCargarTercero Then
'                        'Propietario
'                        If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "Terceros", arrCampos, arrValoresCampos, 7, General.CAMPO_NUMERICO, "Codigo", sdrVehiculo("TERC_Propietario").ToString, "", Me.strError) Then
'                            Me.objPropietario.Codigo = sdrVehiculo("TERC_Propietario").ToString
'                            Me.objPropietario.NumeroIdentificacion = arrValoresCampos(0)
'                            Me.objPropietario.Nombre = arrValoresCampos(1) & " " & arrValoresCampos(2) & " " & arrValoresCampos(3)
'                            Me.objPropietario.Telefono1 = arrValoresCampos(4)
'                            Me.objPropietario.Celular = arrValoresCampos(5)
'                            Me.objPropietario.CataEstado.Campo1 = arrValoresCampos(6)
'                        End If

'                        'Tenedor
'                        If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "Terceros", arrCampos, arrValoresCampos, 7, General.CAMPO_NUMERICO, "Codigo", sdrVehiculo("TERC_Tenedor").ToString, "", Me.strError) Then
'                            Me.objTenedor.Codigo = sdrVehiculo("TERC_Tenedor").ToString
'                            Me.objTenedor.NumeroIdentificacion = arrValoresCampos(0)
'                            Me.objTenedor.Nombre = arrValoresCampos(1) & " " & arrValoresCampos(2) & " " & arrValoresCampos(3)
'                            Me.objTenedor.Telefono1 = arrValoresCampos(4)
'                            Me.objTenedor.Celular = arrValoresCampos(5)
'                        End If

'                        If Me.objTenedor.NumeroCuentaTransferencia <> "" Then
'                            Me.objTenedor.BancoTransferencia.Consultar()
'                        Else
'                            Me.objTenedor.BancoTransferencia.Nombre = ""
'                        End If

'                        'Conductor
'                        If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "Terceros", arrCampos, arrValoresCampos, 7, General.CAMPO_NUMERICO, "Codigo", sdrVehiculo("TERC_Conductor").ToString, "", Me.strError) Then
'                            Me.objConductor.Codigo = sdrVehiculo("TERC_Conductor").ToString
'                            Me.objConductor.NumeroIdentificacion = arrValoresCampos(0)
'                            Me.objConductor.Nombre = arrValoresCampos(1) & " " & arrValoresCampos(2) & " " & arrValoresCampos(3)
'                            Me.objConductor.Telefono1 = arrValoresCampos(4)
'                            Me.objConductor.Celular = arrValoresCampos(5)
'                            Me.objConductor.CataEstado.Campo1 = arrValoresCampos(6)
'                        End If

'                        'Afiliador
'                        If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "Terceros", arrCampos, arrValoresCampos, 7, General.CAMPO_NUMERICO, "Codigo", sdrVehiculo("TERC_Afiliador").ToString, "", Me.strError) Then
'                            Me.objAfiliador.Codigo = sdrVehiculo("TERC_Afiliador").ToString
'                            Me.objAfiliador.NumeroIdentificacion = arrValoresCampos(0)
'                            Me.objAfiliador.Nombre = arrValoresCampos(1) & " " & arrValoresCampos(2) & " " & arrValoresCampos(3)
'                            Me.objAfiliador.Telefono1 = arrValoresCampos(4)
'                            Me.objAfiliador.Celular = arrValoresCampos(5)
'                        End If

'                        Me.objAseguradora.Codigo = Val(sdrVehiculo("TERC_Aseguradora").ToString())

'                    Else
'                        Me.objPropietario.Codigo = Val(sdrVehiculo("TERC_Propietario").ToString())
'                        Me.objTenedor.Codigo = Val(sdrVehiculo("TERC_Tenedor").ToString())
'                        Me.objConductor.Codigo = Val(sdrVehiculo("TERC_Conductor").ToString())
'                        Me.objAfiliador.Codigo = Val(sdrVehiculo("TERC_Afiliador").ToString())
'                        Me.objAseguradora.Codigo = Val(sdrVehiculo("TERC_Aseguradora").ToString())
'                    End If

'                    Me.objCataClasificacionVehiculo.Campo1 = (Val(sdrVehiculo("Propio").ToString()))

'                    Me.strTarjetaPropiedad = Val(sdrVehiculo("Tarjeta_Propiedad").ToString())
'                    Me.strOrganismoTransito = sdrVehiculo("Organismo_Transito").ToString()
'                    Me.lonMarca = Val(sdrVehiculo("MARC_Codigo").ToString())
'                    Me.intModelo = Val(sdrVehiculo("modelo").ToString())

'                    Me.intModeloRepotenciado = Val(sdrVehiculo("Modelo_Repotenciado").ToString())
'                    Me.objCataTipoVehiculo.Campo1 = (sdrVehiculo("TIVE_Codigo").ToString())
'                    Me.strCodigoLinea = Trim(sdrVehiculo("LINE_Codigo").ToString())
'                    Me.intCilindraje = sdrVehiculo("Cilindraje").ToString()
'                    Me.strClase = sdrVehiculo("Clase").ToString()

'                    Me.lonColor = Val(sdrVehiculo("COLO_Codigo").ToString())
'                    Me.objCataTipoCarroceria.Campo1 = Val(sdrVehiculo("TICA_Codigo").ToString())
'                    Me.intNumeroEjes = Val(sdrVehiculo("Numero_Ejes").ToString())
'                    Me.strNumeroMotor = sdrVehiculo("Numero_Motor").ToString()
'                    Me.strNumeroSerie = sdrVehiculo("Numero_Serie").ToString()

'                    Me.intCapacidad = Val(sdrVehiculo("Capacidad").ToString())
'                    Me.dblPesoBruto = Val(sdrVehiculo("Peso_Bruto").ToString())
'                    Me.strRegistroNacionalCarga = sdrVehiculo("Registro_Nacional_Carga").ToString()
'                    Me.strPolizaSOAT = sdrVehiculo("Poliza_SOAT").ToString()
'                    Me.objCataCausaInactividad.Campo1 = Val(sdrVehiculo("CIVE_Codigo").ToString())

'                    Me.dteFechaVenceSOAT = Date.Parse(sdrVehiculo("Fecha_Vence_Soat").ToString())
'                    '   Me.objSemirremolque.Placa = sdrVehiculo("SEMI_Placa").ToString()
'                    Me.strCertificadoContaminacion = sdrVehiculo("Certificado_Contaminacion").ToString()
'                    Me.dblValorVehiculo = Val(sdrVehiculo("Valor_Vehiculo").ToString())
'                    Me.intTiempoUtil = Val(sdrVehiculo("Tiempo_Util").ToString())

'                    Me.dblValorSeguro = Val(sdrVehiculo("Valor_Seguro").ToString())
'                    Me.strCarnetEmpresaAfiliadora = sdrVehiculo("Carnet_Empresa_Afiliadora").ToString()
'                    Me.dteFechaVenceAfiliadora = Date.Parse(sdrVehiculo("Vence_Empresa_Afiliadora").ToString())
'                    Me.strRegistroNacionalCombustible = sdrVehiculo("Registro_Nacional_Combustible").ToString()
'                    Me.dteFechaVenceCertificado = Date.Parse(sdrVehiculo("Fecha_Vence_Certificado").ToString())

'                    Me.dteFechaVenceRegistroCombustible = Date.Parse(sdrVehiculo("Vence_Registro_Combustible").ToString())
'                    Date.TryParse(sdrVehiculo("Fecha_Actualiza_Kilometraje").ToString(), Me.dteFechaActualizoKilometraje)
'                    Me.dblKilometraje = Val(sdrVehiculo("Kilometraje").ToString())
'                    Me.dblCapacidadMetrosCubicos = Val(sdrVehiculo("Capacidad_Metro_Cubicos").ToString())
'                    Date.TryParse(sdrVehiculo("Fecha_Ultima_Revision_Mecanica").ToString(), Me.dteFechaUltimaRevisionMecanica)
'                    Me.strCodigo = sdrVehiculo("Codigo").ToString()

'                    Me.CataEstado.Campo1 = Val(sdrVehiculo("Estado").ToString())
'                    Me.objCataTipoEquipo.Campo1 = Val(sdrVehiculo("TIEQ_Codigo").ToString())
'                    Me.strJustificacionBloqueo = sdrVehiculo("Justificacion_Bloqueo").ToString()
'                    Me.intCategoriaPeajes = Val(sdrVehiculo("Categoria_Peajes").ToString())

'                    If Not IsNothing(Me.objEstudioSeguridad) Then
'                        If sdrVehiculo("ESSE_Numero") IsNot DBNull.Value Then
'                            ReDim arrCampos(2), arrValoresCampos(2)

'                            arrCampos(0) = "Numero"
'                            arrCampos(1) = "Fecha"
'                            arrCampos(2) = "Fecha_Aprueba"

'                            If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "Estudio_Seguridad_Vehiculos", arrCampos, arrValoresCampos, arrCampos.Length, General.CAMPO_NUMERICO, "Numero", sdrVehiculo("ESSE_Numero").ToString, "", Me.strError) Then
'                                Me.objEstudioSeguridad.Numero = sdrVehiculo("ESSE_Numero").ToString
'                                Me.objEstudioSeguridad.Fecha = arrValoresCampos(1)
'                                Me.objEstudioSeguridad.FechaAprueba = arrValoresCampos(2)
'                            End If
'                        End If
'                    End If



'                    If sdrVehiculo("Foto") IsNot DBNull.Value Then
'                        bolFoto = True
'                    Else
'                        bolFoto = False
'                    End If

'                    Me.intTieneGPS = Val(sdrVehiculo("Tiene_GPS").ToString())
'                    Me.strNombreEmpresaGPS = sdrVehiculo("Nombre_Empresa_GPS").ToString()
'                    Me.strClaveGPS = sdrVehiculo("Clave_GPS").ToString()
'                    Me.strTelefonoGPS = sdrVehiculo("Telefono_GPS").ToString()
'                    Me.strUsuarioGPS = sdrVehiculo("Usuario_GPS").ToString()
'                    Me.intInterfazAvantel = sdrVehiculo("Interfaz_Avantel").ToString()
'                    Me.intInterfazTracker = sdrVehiculo("Interfaz_Tracker").ToString()
'                    Me.objCataTipoGps.Campo1 = sdrVehiculo("TGPS_Codigo").ToString()
'                    Me.intInterfazSatrack = sdrVehiculo("Interfaz_Satrack").ToString()
'                    Me.intInterfazGpsColombia = sdrVehiculo("Interfaz_GPS_Colombia").ToString()
'                    Me.intTipoCapacidadVehiculo = Val(sdrVehiculo("CAVE_Codigo").ToString())
'                    Me.IntRegionGeografica = (sdrVehiculo("REGE_Codigo").ToString())

'                    Consultar = True
'                Else
'                    Me.strPlaca = ""
'                    Consultar = False
'                End If

'                sdrVehiculo.Close()
'                sdrVehiculo.Dispose()

'            Catch ex As Exception
'                Me.strPlaca = ""
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Cargar_Web_Service(ByRef Empresa As Empresas, ByVal Placa As String, Optional ByVal bolCargarTercero As Boolean = True) As Boolean

'            Cargar_Web_Service = True

'            Me.objEmpresa = Empresa
'            Me.strPlaca = Placa

'            Dim sdrVehiculo As SqlDataReader

'            Dim ComandoSQL As SqlCommand

'            strSQL = "SELECT EMPR_Codigo, Placa, TERC_Propietario, TERC_Tenedor, TERC_Conductor, "
'            strSQL += "TERC_Afiliador, SEMI_Placa, Propio, REGE_Codigo, Tarjeta_Propiedad, Organismo_Transito, "
'            strSQL += "MARC_Codigo, Modelo, Modelo_Repotenciado, TIVE_Codigo, LINE_Codigo, "
'            strSQL += "TIEQ_Codigo, Cilindraje, Clase, COLO_Codigo, TICA_Codigo, "
'            strSQL += "Numero_Ejes, Numero_Motor, Numero_Serie, Capacidad, Capacidad_Metro_Cubicos, "
'            strSQL += "Peso_Bruto, Registro_Nacional_Carga, Poliza_SOAT, Fecha_Vence_SOAT, TERC_Aseguradora, "
'            strSQL += "Certificado_Contaminacion, Fecha_Vence_Certificado, Valor_Vehiculo, Tiempo_Util,Valor_Seguro, "
'            strSQL += "Carnet_Empresa_Afiliadora, Vence_Empresa_Afiliadora, Registro_Nacional_Combustible, Vence_Registro_Combustible, Categoria_Peajes, "
'            strSQL += "Codigo, Estado, CIVE_Codigo, ESSE_Numero, Kilometraje, "
'            strSQL += "Fecha_Actualiza_Kilometraje, Fecha_Ultima_Revision_Mecanica, Fecha_Crea, USUA_Crea, Fecha_Modifica, "
'            strSQL += "USUA_Modifica, OFIC_Codigo, Justificacion_Bloqueo, Tiene_GPS, Nombre_Empresa_GPS, "
'            strSQL += "Clave_GPS, Telefono_GPS, Usuario_GPS, Interfaz_Avantel, Interfaz_Tracker, Interfaz_Satrack, TGPS_Codigo, Interfaz_GPS_Colombia, "
'            strSQL += "'Foto' = CASE WHEN Foto IS NULL THEN NULL ELSE 1 END, CAVE_Codigo "
'            strSQL += " FROM Vehiculos"
'            strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'            strSQL += " AND Placa = '" & Me.strPlaca & "'"

'            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'            Call Instanciar_Objetos_Alternos()
'            Try
'                Me.objGeneral.ConexionSQL.Open()
'                sdrVehiculo = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrVehiculo.Read() Then


'                    'Información Terceros
'                    Dim arrCampos(7), arrValoresCampos(7) As String

'                    arrCampos(0) = "Numero_Identificacion"
'                    arrCampos(1) = "Nombre"
'                    arrCampos(2) = "Apellido1"
'                    arrCampos(3) = "Apellido2"
'                    arrCampos(4) = "Telefono1"
'                    arrCampos(5) = "Celular"
'                    arrCampos(6) = "Estado"


'                    If bolCargarTercero Then
'                        'Propietario
'                        If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "Terceros", arrCampos, arrValoresCampos, 7, General.CAMPO_NUMERICO, "Codigo", sdrVehiculo("TERC_Propietario").ToString, "", Me.strError) Then
'                            Me.objPropietario.Codigo = sdrVehiculo("TERC_Propietario").ToString
'                            Me.objPropietario.NumeroIdentificacion = arrValoresCampos(0)
'                            Me.objPropietario.Nombre = arrValoresCampos(1) & " " & arrValoresCampos(2) & " " & arrValoresCampos(3)
'                            Me.objPropietario.Telefono1 = arrValoresCampos(4)
'                            Me.objPropietario.Celular = arrValoresCampos(5)
'                            Me.objPropietario.CataEstado.Campo1 = arrValoresCampos(6)
'                        End If

'                        'Tenedor
'                        If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "Terceros", arrCampos, arrValoresCampos, 7, General.CAMPO_NUMERICO, "Codigo", sdrVehiculo("TERC_Tenedor").ToString, "", Me.strError) Then
'                            Me.objTenedor.Codigo = sdrVehiculo("TERC_Tenedor").ToString
'                            Me.objTenedor.NumeroIdentificacion = arrValoresCampos(0)
'                            Me.objTenedor.Nombre = arrValoresCampos(1) & " " & arrValoresCampos(2) & " " & arrValoresCampos(3)
'                            Me.objTenedor.Telefono1 = arrValoresCampos(4)
'                            Me.objTenedor.Celular = arrValoresCampos(5)
'                        End If

'                        If Me.objTenedor.NumeroCuentaTransferencia <> "" Then
'                            Me.objTenedor.BancoTransferencia.Consultar()
'                        Else
'                            Me.objTenedor.BancoTransferencia.Nombre = ""
'                        End If

'                        'Conductor
'                        If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "Terceros", arrCampos, arrValoresCampos, 7, General.CAMPO_NUMERICO, "Codigo", sdrVehiculo("TERC_Conductor").ToString, "", Me.strError) Then
'                            Me.objConductor.Codigo = sdrVehiculo("TERC_Conductor").ToString
'                            Me.objConductor.NumeroIdentificacion = arrValoresCampos(0)
'                            Me.objConductor.Nombre = arrValoresCampos(1) & " " & arrValoresCampos(2) & " " & arrValoresCampos(3)
'                            Me.objConductor.Telefono1 = arrValoresCampos(4)
'                            Me.objConductor.Celular = arrValoresCampos(5)
'                            Me.objConductor.CataEstado.Campo1 = arrValoresCampos(6)
'                        End If

'                        'Afiliador
'                        If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "Terceros", arrCampos, arrValoresCampos, 7, General.CAMPO_NUMERICO, "Codigo", sdrVehiculo("TERC_Afiliador").ToString, "", Me.strError) Then
'                            Me.objAfiliador.Codigo = sdrVehiculo("TERC_Afiliador").ToString
'                            Me.objAfiliador.NumeroIdentificacion = arrValoresCampos(0)
'                            Me.objAfiliador.Nombre = arrValoresCampos(1) & " " & arrValoresCampos(2) & " " & arrValoresCampos(3)
'                            Me.objAfiliador.Telefono1 = arrValoresCampos(4)
'                            Me.objAfiliador.Celular = arrValoresCampos(5)
'                        End If

'                        Me.objAseguradora.Codigo = Val(sdrVehiculo("TERC_Aseguradora").ToString())

'                    Else
'                        Me.objPropietario.Codigo = Val(sdrVehiculo("TERC_Propietario").ToString())
'                        Me.objTenedor.Codigo = Val(sdrVehiculo("TERC_Tenedor").ToString())
'                        Me.objConductor.Codigo = Val(sdrVehiculo("TERC_Conductor").ToString())
'                        Me.objAfiliador.Codigo = Val(sdrVehiculo("TERC_Afiliador").ToString())
'                        Me.objAseguradora.Codigo = Val(sdrVehiculo("TERC_Aseguradora").ToString())
'                    End If

'                    Me.objCataClasificacionVehiculo.Campo1 = Val(sdrVehiculo("Propio").ToString())
'                    Me.strTarjetaPropiedad = Val(sdrVehiculo("Tarjeta_Propiedad").ToString())
'                    Me.strOrganismoTransito = sdrVehiculo("Organismo_Transito").ToString()
'                    Me.lonMarca = Val(sdrVehiculo("MARC_Codigo").ToString())
'                    Me.intModelo = Val(sdrVehiculo("modelo").ToString())

'                    Me.intModeloRepotenciado = Val(sdrVehiculo("Modelo_Repotenciado").ToString())
'                    Me.objCataTipoVehiculo.Campo1 = (sdrVehiculo("TIVE_Codigo").ToString())
'                    Me.strCodigoLinea = Trim(sdrVehiculo("LINE_Codigo").ToString())
'                    Me.intCilindraje = sdrVehiculo("Cilindraje").ToString()
'                    Me.strClase = sdrVehiculo("Clase").ToString()

'                    Me.lonColor = Val(sdrVehiculo("COLO_Codigo").ToString())
'                    Me.objCataTipoCarroceria.Campo1 = (sdrVehiculo("TICA_Codigo").ToString())
'                    Me.intNumeroEjes = Val(sdrVehiculo("Numero_Ejes").ToString())
'                    Me.strNumeroMotor = sdrVehiculo("Numero_Motor").ToString()
'                    Me.strNumeroSerie = sdrVehiculo("Numero_Serie").ToString()

'                    Me.intCapacidad = Val(sdrVehiculo("Capacidad").ToString())
'                    Me.dblPesoBruto = Val(sdrVehiculo("Peso_Bruto").ToString())
'                    Me.strRegistroNacionalCarga = sdrVehiculo("Registro_Nacional_Carga").ToString()
'                    Me.strPolizaSOAT = sdrVehiculo("Poliza_SOAT").ToString()
'                    Me.objCataCausaInactividad.Campo1 = Val(sdrVehiculo("CIVE_Codigo").ToString())

'                    Me.dteFechaVenceSOAT = Date.Parse(sdrVehiculo("Fecha_Vence_Soat").ToString())
'                    '      Me.objSemirremolque.Placa = sdrVehiculo("SEMI_Placa").ToString()
'                    Me.strCertificadoContaminacion = sdrVehiculo("Certificado_Contaminacion").ToString()
'                    Me.dblValorVehiculo = Val(sdrVehiculo("Valor_Vehiculo").ToString())
'                    Me.intTiempoUtil = Val(sdrVehiculo("Tiempo_Util").ToString())

'                    Me.dblValorSeguro = Val(sdrVehiculo("Valor_Seguro").ToString())
'                    Me.strCarnetEmpresaAfiliadora = sdrVehiculo("Carnet_Empresa_Afiliadora").ToString()
'                    Me.dteFechaVenceAfiliadora = Date.Parse(sdrVehiculo("Vence_Empresa_Afiliadora").ToString())
'                    Me.strRegistroNacionalCombustible = sdrVehiculo("Registro_Nacional_Combustible").ToString()
'                    Me.dteFechaVenceCertificado = Date.Parse(sdrVehiculo("Fecha_Vence_Certificado").ToString())

'                    Me.dteFechaVenceRegistroCombustible = Date.Parse(sdrVehiculo("Vence_Registro_Combustible").ToString())
'                    Date.TryParse(sdrVehiculo("Fecha_Actualiza_Kilometraje").ToString(), Me.dteFechaActualizoKilometraje)
'                    Me.dblKilometraje = Val(sdrVehiculo("Kilometraje").ToString())
'                    Me.dblCapacidadMetrosCubicos = Val(sdrVehiculo("Capacidad_Metro_Cubicos").ToString())
'                    Date.TryParse(sdrVehiculo("Fecha_Ultima_Revision_Mecanica").ToString(), Me.dteFechaUltimaRevisionMecanica)
'                    Me.strCodigo = sdrVehiculo("Codigo").ToString()

'                    Me.CataEstado.Campo1 = Val(sdrVehiculo("Estado").ToString())
'                    Me.objCataTipoEquipo.Campo1 = Val(sdrVehiculo("TIEQ_Codigo").ToString())
'                    Me.strJustificacionBloqueo = sdrVehiculo("Justificacion_Bloqueo").ToString()
'                    Me.intCategoriaPeajes = Val(sdrVehiculo("Categoria_Peajes").ToString())


'                    If Not IsNothing(Me.objEstudioSeguridad) Then
'                        If sdrVehiculo("ESSE_Numero") IsNot DBNull.Value Then
'                            ReDim arrCampos(2), arrValoresCampos(2)

'                            arrCampos(0) = "Numero"
'                            arrCampos(1) = "Fecha"

'                            If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "Estudio_Seguridad_Vehiculos", arrCampos, arrValoresCampos, arrCampos.Length, General.CAMPO_NUMERICO, "Numero", sdrVehiculo("ESSE_Numero").ToString, "", Me.strError) Then
'                                Me.objEstudioSeguridad.Numero = sdrVehiculo("ESSE_Numero").ToString
'                                Me.objEstudioSeguridad.Fecha = arrValoresCampos(1)
'                            End If
'                        End If
'                    End If

'                    If sdrVehiculo("Foto") IsNot DBNull.Value Then
'                        bolFoto = True
'                    Else
'                        bolFoto = False
'                    End If

'                    Me.intTieneGPS = Val(sdrVehiculo("Tiene_GPS").ToString())
'                    Me.strNombreEmpresaGPS = sdrVehiculo("Nombre_Empresa_GPS").ToString()
'                    Me.strClaveGPS = sdrVehiculo("Clave_GPS").ToString()
'                    Me.strTelefonoGPS = sdrVehiculo("Telefono_GPS").ToString()
'                    Me.strUsuarioGPS = sdrVehiculo("Usuario_GPS").ToString()
'                    Me.intInterfazAvantel = sdrVehiculo("Interfaz_Avantel").ToString()
'                    Me.intInterfazTracker = sdrVehiculo("Interfaz_Tracker").ToString()
'                    Me.objCataTipoGps.Campo1 = sdrVehiculo("TGPS_Codigo").ToString()
'                    Me.intInterfazSatrack = sdrVehiculo("Interfaz_Satrack").ToString()
'                    Me.intInterfazGpsColombia = sdrVehiculo("Interfaz_GPS_Colombia").ToString()
'                    Me.intTipoCapacidadVehiculo = Val(sdrVehiculo("CAVE_Codigo").ToString())
'                    Me.IntRegionGeografica = (sdrVehiculo("REGE_Codigo").ToString())

'                    Cargar_Web_Service = True
'                Else
'                    Me.strPlaca = ""
'                    Cargar_Web_Service = False
'                End If

'                sdrVehiculo.Close()
'                sdrVehiculo.Dispose()

'            Catch ex As Exception
'                Me.strPlaca = ""
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Cargar_Web_Service: " & Me.objGeneral.Traducir_Error(ex.Message)

'                End Try
'                Cargar_Web_Service = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Cargar_Validar(ByRef Empresa As Empresas, ByVal Placa As String, Optional ByVal bolCargarTercero As Boolean = True) As Boolean
'            Cargar_Validar = True

'            Me.objEmpresa = Empresa
'            Me.strPlaca = Placa

'            Dim sdrVehiculo As SqlDataReader

'            Dim ComandoSQL As SqlCommand

'            strSQL = "SELECT EMPR_Codigo, Placa, TERC_Propietario, TERC_Tenedor, TERC_Conductor, "
'            strSQL += "TERC_Afiliador, SEMI_Placa, Propio, Capacidad_Metro_Cubicos, TIVE_Codigo,"
'            strSQL += "TIEQ_Codigo,Estado"
'            strSQL += " FROM Vehiculos"
'            strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'            strSQL += " AND Placa = '" & Me.strPlaca & "'"

'            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'            Call Instanciar_Objetos_Alternos()
'            Try
'                Me.objGeneral.ConexionSQL.Open()
'                sdrVehiculo = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrVehiculo.Read() Then

'                    If bolCargarTercero Then
'                        Me.objPropietario.Existe_Registro(Me.objEmpresa, Val(sdrVehiculo("TERC_Propietario").ToString()))
'                        Me.objTenedor.Existe_Registro(Me.objEmpresa, Val(sdrVehiculo("TERC_Tenedor").ToString()))
'                        Me.objConductor.Existe_Registro(Me.objEmpresa, Val(sdrVehiculo("TERC_Conductor").ToString()))
'                        Me.objAfiliador.Existe_Registro(Me.objEmpresa, Val(sdrVehiculo("TERC_Afiliador").ToString()))
'                    Else
'                        Me.objPropietario.Codigo = Val(sdrVehiculo("TERC_Propietario").ToString())
'                        Me.objTenedor.Codigo = Val(sdrVehiculo("TERC_Tenedor").ToString())
'                        Me.objConductor.Codigo = Val(sdrVehiculo("TERC_Conductor").ToString())
'                        Me.objAfiliador.Codigo = Val(sdrVehiculo("TERC_Afiliador").ToString())
'                        Me.objAseguradora.Codigo = Val(sdrVehiculo("TERC_Aseguradora").ToString())
'                    End If

'                    Me.objCataClasificacionVehiculo.Campo1 = Val(sdrVehiculo("Propio").ToString())
'                    Me.objCataTipoVehiculo.Consultar(sdrVehiculo("TIVE_Codigo").ToString())
'                    '   Me.objSemirremolque.Placa = sdrVehiculo("SEMI_Placa").ToString()
'                    Me.CataEstado.Campo1 = Val(sdrVehiculo("Estado").ToString())

'                    Me.objCataTipoEquipo.Campo1 = Val(sdrVehiculo("TIEQ_Codigo").ToString())
'                    Me.dblCapacidadMetrosCubicos = Val(sdrVehiculo("Capacidad_Metro_Cubicos").ToString())

'                    Cargar_Validar = True
'                Else
'                    Me.strPlaca = ""
'                    Cargar_Validar = False
'                End If

'                sdrVehiculo.Close()
'                sdrVehiculo.Dispose()

'            Catch ex As Exception
'                Me.strPlaca = ""
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & "se presento un error en la función Cargar_Validar: " & Me.objGeneral.Traducir_Error(ex.Message)

'                End Try
'                Cargar_Validar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Existe_Registro() As Boolean
'            Existe_Registro = True

'            Dim sdrVehiculo As SqlDataReader

'            Dim ComandoSQL As SqlCommand

'            strSQL = "SELECT EMPR_Codigo, Estado, SEMI_Placa, TERC_Propietario, TERC_Tenedor, TERC_Conductor, Interfaz_Avantel, Interfaz_Tracker, TIVE_Codigo FROM Vehiculos"
'            strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'            strSQL += " AND Placa = '" & Me.strPlaca & "'"

'            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'            Try
'                Me.objGeneral.ConexionSQL.Open()
'                sdrVehiculo = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrVehiculo.Read() Then
'                    Me.CataEstado.Campo1 = Val(sdrVehiculo("Estado").ToString())
'                    '   Me.objSemirremolque.Placa = sdrVehiculo("SEMI_Placa").ToString()
'                    Me.objPropietario.Codigo = Val(sdrVehiculo("TERC_Propietario").ToString())
'                    Me.objTenedor.Codigo = Val(sdrVehiculo("TERC_Tenedor").ToString())
'                    Me.objConductor.Codigo = Val(sdrVehiculo("TERC_Conductor").ToString())
'                    Me.intInterfazAvantel = sdrVehiculo("Interfaz_Avantel").ToString()
'                    Me.intInterfazTracker = sdrVehiculo("Interfaz_Tracker").ToString()
'                    Me.CataTipoVehiculo.Campo1 = sdrVehiculo("TIVE_Codigo").ToString()
'                    Existe_Registro = True
'                Else
'                    Me.strPlaca = ""
'                    Existe_Registro = False
'                End If

'                sdrVehiculo.Close()
'                sdrVehiculo.Dispose()

'            Catch ex As Exception
'                Me.strPlaca = ""
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Existe_Registro: " & Me.objGeneral.Traducir_Error(ex.Message)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Existe_Registroe: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Existe_Registro = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Existe_Registro(ByRef Empresa As Empresas, ByVal Placa As String, Optional ByVal bolCargarTercero As Boolean = True) As Boolean
'            Existe_Registro = True

'            Me.objEmpresa = Empresa
'            Me.strPlaca = Placa

'            Dim sdrVehiculo As SqlDataReader

'            Dim ComandoSQL As SqlCommand

'            strSQL = "SELECT EMPR_Codigo, Estado, SEMI_Placa, TERC_Propietario, TERC_Tenedor, "
'            strSQL += " TERC_Conductor, INTERFAZ_AVANTEL, Interfaz_Tracker, TIVE_Codigo, CAVE_Codigo, Capacidad_Metro_Cubicos"
'            strSQL += " FROM Vehiculos"
'            strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'            strSQL += " AND Placa = '" & Me.strPlaca & "'"

'            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'            Try
'                Me.objGeneral.ConexionSQL.Open()
'                sdrVehiculo = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrVehiculo.Read() Then
'                    Me.CataEstado.Campo1 = Val(sdrVehiculo("Estado").ToString())
'                    '   Me.objSemirremolque.Placa = sdrVehiculo("SEMI_Placa").ToString()
'                    Me.objPropietario.Codigo = Val(sdrVehiculo("TERC_Propietario").ToString())
'                    Me.objTenedor.Codigo = Val(sdrVehiculo("TERC_Tenedor").ToString())
'                    Me.objConductor.Codigo = Val(sdrVehiculo("TERC_Conductor").ToString())
'                    Me.intInterfazAvantel = sdrVehiculo("Interfaz_Avantel").ToString()
'                    Me.intInterfazTracker = sdrVehiculo("Interfaz_Tracker").ToString()
'                    Me.objCataTipoVehiculo.Campo1 = (sdrVehiculo("TIVE_Codigo").ToString())
'                    Me.intTipoCapacidadVehiculo = Val(sdrVehiculo("CAVE_Codigo").ToString)
'                    Double.TryParse((sdrVehiculo("Capacidad_Metro_Cubicos").ToString()), Me.dblCapacidadMetrosCubicos)
'                    Existe_Registro = True
'                Else
'                    Me.strPlaca = ""
'                    Existe_Registro = False
'                End If

'                sdrVehiculo.Close()
'                sdrVehiculo.Dispose()

'            Catch ex As Exception
'                Me.strPlaca = ""
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Existe_Registroe: " & Me.objGeneral.Traducir_Error(ex.Message)
'                Catch Exc As Exception
'                    Me.strError = Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Existe_Registro = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Cargar_X_Conductor(ByRef Empresa As Empresas, ByVal Conductor As Tercero) As Boolean
'            Cargar_X_Conductor = True

'            Me.objEmpresa = Empresa
'            Me.objConductor = Conductor

'            Dim sdrVehiculo As SqlDataReader

'            Dim ComandoSQL As SqlCommand

'            strSQL = "SELECT TOP 1 EMPR_Codigo, Placa, TERC_Propietario, TERC_Tenedor, TERC_Conductor, "
'            strSQL += "TERC_Afiliador, SEMI_Placa, Propio, REGE_Codigo, Tarjeta_Propiedad, Organismo_Transito, "
'            strSQL += "MARC_Codigo, Modelo, Modelo_Repotenciado, TIVE_Codigo, LINE_Codigo, "
'            strSQL += "TIEQ_Codigo, Cilindraje, Clase, COLO_Codigo, TICA_Codigo, "
'            strSQL += "Numero_Ejes, Numero_Motor, Numero_Serie, Capacidad, Capacidad_Metro_Cubicos, "
'            strSQL += "Peso_Bruto, Registro_Nacional_Carga, Poliza_SOAT, Fecha_Vence_SOAT, TERC_Aseguradora, "
'            strSQL += "Certificado_Contaminacion, Fecha_Vence_Certificado, Valor_Vehiculo, Tiempo_Util,Valor_Seguro, "
'            strSQL += "Carnet_Empresa_Afiliadora, Vence_Empresa_Afiliadora, Registro_Nacional_Combustible, Vence_Registro_Combustible, Categoria_Peajes, "
'            strSQL += "Codigo, Estado, CIVE_Codigo, ESSE_Numero, Kilometraje, "
'            strSQL += "Fecha_Actualiza_Kilometraje, Fecha_Ultima_Revision_Mecanica, Fecha_Crea, USUA_Crea, Fecha_Modifica, "
'            strSQL += "USUA_Modifica, OFIC_Codigo, Justificacion_Bloqueo, Tiene_GPS, Nombre_Empresa_GPS, "
'            strSQL += "Clave_GPS, Telefono_GPS, Usuario_GPS, TGPS_Codigo"
'            strSQL += "'Foto' = CASE WHEN Foto IS NULL THEN NULL ELSE 1 END, CAVE_Codigo"
'            strSQL += " FROM Vehiculos"
'            strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'            strSQL += " AND TERC_Conductor = " & Me.objConductor.Codigo

'            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'            Call Instanciar_Objetos_Alternos()
'            Try
'                Me.objGeneral.ConexionSQL.Open()
'                sdrVehiculo = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrVehiculo.Read() Then

'                    Me.strPlaca = sdrVehiculo("Placa").ToString()

'                    'Información Terceros
'                    Dim arrCampos(7), arrValoresCampos(7) As String

'                    arrCampos(0) = "Numero_Identificacion"
'                    arrCampos(1) = "Nombre"
'                    arrCampos(2) = "Apellido1"
'                    arrCampos(3) = "Apellido2"
'                    arrCampos(4) = "Telefono1"
'                    arrCampos(5) = "Celular"
'                    arrCampos(6) = "Estado"

'                    If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "Terceros", arrCampos, arrValoresCampos, 7, General.CAMPO_NUMERICO, "Codigo", sdrVehiculo("TERC_Propietario").ToString, "", Me.strError) Then
'                        Me.objPropietario.Codigo = sdrVehiculo("TERC_Propietario").ToString
'                        Me.objPropietario.NumeroIdentificacion = arrValoresCampos(0)
'                        Me.objPropietario.Nombre = arrValoresCampos(1) & " " & arrValoresCampos(2) & " " & arrValoresCampos(3)
'                        Me.objPropietario.Telefono1 = arrValoresCampos(4)
'                        Me.objPropietario.Celular = arrValoresCampos(5)
'                        Me.objPropietario.CataEstado.Campo1 = arrValoresCampos(6)
'                    End If

'                    If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "Terceros", arrCampos, arrValoresCampos, 7, General.CAMPO_NUMERICO, "Codigo", sdrVehiculo("TERC_Conductor").ToString, "", Me.strError) Then
'                        Me.objConductor.Codigo = sdrVehiculo("TERC_Conductor").ToString
'                        Me.objConductor.NumeroIdentificacion = arrValoresCampos(0)
'                        Me.objConductor.Nombre = arrValoresCampos(1) & " " & arrValoresCampos(2) & " " & arrValoresCampos(3)
'                        Me.objConductor.Telefono1 = arrValoresCampos(4)
'                        Me.objConductor.Celular = arrValoresCampos(5)
'                        Me.objConductor.CataEstado.Campo1 = arrValoresCampos(6)
'                    End If

'                    If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "Terceros", arrCampos, arrValoresCampos, 7, General.CAMPO_NUMERICO, "Codigo", sdrVehiculo("TERC_Tenedor").ToString, "", Me.strError) Then
'                        Me.objTenedor.Codigo = sdrVehiculo("TERC_Tenedor").ToString
'                        Me.objTenedor.NumeroIdentificacion = arrValoresCampos(0)
'                        Me.objTenedor.Nombre = arrValoresCampos(1) & " " & arrValoresCampos(2) & " " & arrValoresCampos(3)
'                        Me.objTenedor.Telefono1 = arrValoresCampos(4)
'                        Me.objTenedor.Celular = arrValoresCampos(5)
'                    End If

'                    If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "Terceros", arrCampos, arrValoresCampos, 7, General.CAMPO_NUMERICO, "Codigo", sdrVehiculo("TERC_Afiliador").ToString, "", Me.strError) Then
'                        Me.objAfiliador.Codigo = sdrVehiculo("TERC_Afiliador").ToString
'                        Me.objAfiliador.NumeroIdentificacion = arrValoresCampos(0)
'                        Me.objAfiliador.Nombre = arrValoresCampos(1) & " " & arrValoresCampos(2) & " " & arrValoresCampos(3)
'                        Me.objAfiliador.Telefono1 = arrValoresCampos(4)
'                        Me.objAfiliador.Celular = arrValoresCampos(5)
'                    End If


'                    '  Me.objSemirremolque.Placa = sdrVehiculo("SEMI_Placa").ToString()
'                    Me.objCataClasificacionVehiculo.Campo1 = (Val(sdrVehiculo("Propio").ToString()))
'                    Me.strTarjetaPropiedad = Val(sdrVehiculo("Tarjeta_Propiedad").ToString())
'                    Me.strOrganismoTransito = sdrVehiculo("Organismo_Transito").ToString()
'                    Me.lonMarca = Val(sdrVehiculo("MARC_Codigo").ToString())

'                    Me.intModelo = Val(sdrVehiculo("modelo").ToString())
'                    Me.intModeloRepotenciado = Val(sdrVehiculo("Modelo_Repotenciado").ToString())
'                    Me.objCataTipoVehiculo.Campo1 = (sdrVehiculo("TIVE_Codigo").ToString())
'                    Me.strCodigoLinea = Trim(sdrVehiculo("LINE_Codigo").ToString())
'                    Me.intCilindraje = sdrVehiculo("Cilindraje").ToString()

'                    Me.strClase = sdrVehiculo("Clase").ToString()
'                    Me.lonColor = Val(sdrVehiculo("COLO_Codigo").ToString())
'                    Me.objCataTipoCarroceria.Campo1 = (sdrVehiculo("TICA_Codigo").ToString())
'                    Me.intNumeroEjes = Val(sdrVehiculo("Numero_Ejes").ToString())
'                    Me.strNumeroMotor = sdrVehiculo("Numero_Motor").ToString()

'                    Me.strNumeroSerie = sdrVehiculo("Numero_Serie").ToString()
'                    Me.intCapacidad = Val(sdrVehiculo("Capacidad").ToString())
'                    Me.dblPesoBruto = Val(sdrVehiculo("Peso_Bruto").ToString())
'                    Me.strRegistroNacionalCarga = sdrVehiculo("Registro_Nacional_Carga").ToString()
'                    Me.strPolizaSOAT = sdrVehiculo("Poliza_SOAT").ToString()

'                    Me.objCataCausaInactividad.Campo1 = Val(sdrVehiculo("CIVE_Codigo").ToString())
'                    Me.dteFechaVenceSOAT = Date.Parse(sdrVehiculo("Fecha_Vence_Soat").ToString())
'                    Me.objAseguradora.Codigo = Val(sdrVehiculo("TERC_Aseguradora").ToString())
'                    Me.strCertificadoContaminacion = sdrVehiculo("Certificado_Contaminacion").ToString()
'                    Me.dblValorVehiculo = Val(sdrVehiculo("Valor_Vehiculo").ToString())

'                    Me.intTiempoUtil = Val(sdrVehiculo("Tiempo_Util").ToString())
'                    Me.dblValorSeguro = Val(sdrVehiculo("Valor_Seguro").ToString())
'                    Me.strCarnetEmpresaAfiliadora = sdrVehiculo("Carnet_Empresa_Afiliadora").ToString()
'                    Me.dteFechaVenceAfiliadora = Date.Parse(sdrVehiculo("Vence_Empresa_Afiliadora").ToString())
'                    Me.strRegistroNacionalCombustible = sdrVehiculo("Registro_Nacional_Combustible").ToString()

'                    Me.dteFechaVenceCertificado = Date.Parse(sdrVehiculo("Fecha_Vence_Certificado").ToString())
'                    Me.dteFechaVenceRegistroCombustible = Date.Parse(sdrVehiculo("Vence_Registro_Combustible").ToString())
'                    Date.TryParse(sdrVehiculo("Fecha_Actualiza_Kilometraje").ToString(), Me.dteFechaActualizoKilometraje)
'                    Me.dblKilometraje = Val(sdrVehiculo("Kilometraje").ToString())
'                    Me.dblCapacidadMetrosCubicos = Val(sdrVehiculo("Capacidad_Metro_Cubicos").ToString())

'                    Date.TryParse(sdrVehiculo("Fecha_Ultima_Revision_Mecanica").ToString(), Me.dteFechaUltimaRevisionMecanica)
'                    Me.strCodigo = sdrVehiculo("Codigo").ToString()
'                    Me.CataEstado.Campo1 = Val(sdrVehiculo("Estado").ToString())
'                    Me.objCataTipoEquipo.Campo1 = Val(sdrVehiculo("TIEQ_Codigo").ToString())
'                    Me.strJustificacionBloqueo = sdrVehiculo("Justificacion_Bloqueo").ToString()

'                    Me.intCategoriaPeajes = Val(sdrVehiculo("Categoria_Peajes").ToString())

'                    If Not IsNothing(Me.objEstudioSeguridad) Then
'                        If sdrVehiculo("ESSE_Numero") IsNot DBNull.Value Then
'                            ReDim arrCampos(2), arrValoresCampos(2)

'                            arrCampos(0) = "Numero"
'                            arrCampos(1) = "Fecha"

'                            If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "Estudio_Seguridad_Vehiculos", arrCampos, arrValoresCampos, arrCampos.Length, General.CAMPO_NUMERICO, "Numero", sdrVehiculo("ESSE_Numero").ToString, "", Me.strError) Then
'                                Me.objEstudioSeguridad.Numero = sdrVehiculo("ESSE_Numero").ToString
'                                Me.objEstudioSeguridad.Fecha = arrValoresCampos(1)
'                            End If
'                        End If
'                    End If

'                    If sdrVehiculo("Foto") IsNot DBNull.Value Then
'                        bolFoto = True
'                    Else
'                        bolFoto = False
'                    End If

'                    Me.intTieneGPS = Val(sdrVehiculo("Tiene_GPS").ToString())
'                    Me.strNombreEmpresaGPS = sdrVehiculo("Nombre_Empresa_GPS").ToString()
'                    Me.strClaveGPS = sdrVehiculo("Clave_GPS").ToString()
'                    Me.strTelefonoGPS = sdrVehiculo("Telefono_GPS").ToString()
'                    Me.strUsuarioGPS = sdrVehiculo("Usuario_GPS").ToString()
'                    Me.objCataTipoGps.Campo1 = sdrVehiculo("TGPS_Codigo").ToString()
'                    Me.intTipoCapacidadVehiculo = Val(sdrVehiculo("CAVE_Codigo").ToString())
'                    Me.IntRegionGeografica = (sdrVehiculo("REGE_Codigo").ToString())

'                    Cargar_X_Conductor = True
'                Else
'                    Me.strPlaca = ""
'                    Cargar_X_Conductor = False
'                End If

'                sdrVehiculo.Close()
'                sdrVehiculo.Dispose()

'            Catch ex As Exception
'                Me.strPlaca = ""
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Cargar_X_Conductor: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Cargar_X_Conductor = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Guardar(ByRef Mensaje As String) As Boolean

'            Try
'                Dim intCont As Integer
'                Dim intCodigoInicialEmpresa As Integer
'                Dim sdsEmpresas As DataSet
'                Dim strCodiCiud As String = ""

'                Me.objEmpresa.Ciudad = New Ciudad(Me.objEmpresa)
'                Me.objEmpresa.Retorna_Campo(Me.objEmpresa.Codigo, "CIUD_Codigo", strCodiCiud)
'                Me.objEmpresa.Ciudad.Codigo = Val(strCodiCiud)
'                Me.objEmpresa.Ciudad.Consultar(Me.objEmpresa, Me.objEmpresa.Ciudad.Codigo)

'                If Datos_Requeridos(Mensaje) Then
'                    If Not bolModificar Then
'                        Guardar = Insertar_SQL()
'                    Else
'                        Guardar = Modificar_SQL()
'                    End If

'                    If Guardar Then
'                        If Val(Me.objCataEstado.Retorna_Codigo) = ESTADO_ACTIVO Then
'                            If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_FECHAS_VENCIDAS_VEHICULO) Then
'                                Verificar_Fechas_Vencidas()
'                            End If
'                        End If
'                    End If
'                Else
'                    Guardar = False
'                End If
'                Mensaje = Me.strError

'                If Guardar Then
'                    If Me.objValidacion.Consultar(Me.objEmpresa, REPLICA_INFORMACION) Then
'                        intCodigoInicialEmpresa = Me.objEmpresa.Codigo

'                        strSQL = "SELECT Codigo FROM Empresas WHERE Codigo <> " & Me.objEmpresa.Codigo
'                        sdsEmpresas = Me.objGeneral.Retorna_Dataset(strSQL, Me.strError)
'                        If Len(Trim(Me.strError)) = 0 Then
'                            If sdsEmpresas.Tables(Lista.PRIMERA_TABLA).Rows.Count > 0 Then
'                                For intCont = 0 To sdsEmpresas.Tables(Lista.PRIMERA_TABLA).Rows.Count - 1
'                                    Me.objEmpresa.Codigo = (Val(sdsEmpresas.Tables(Lista.PRIMERA_TABLA).Rows(intCont).Item("Codigo").ToString()))
'                                    If Datos_Requeridos(Mensaje) Then
'                                        If Not bolModificar Then
'                                            Guardar = Insertar_SQL()
'                                        Else
'                                            Guardar = Modificar_SQL()
'                                        End If
'                                    End If
'                                    Mensaje = Me.strError
'                                Next
'                            End If
'                        End If
'                        Me.objEmpresa.Consultar(intCodigoInicialEmpresa)
'                        sdsEmpresas.Dispose()

'                    End If

'                End If
'            Catch ex As Exception
'                Guardar = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Guardar: " & Me.objGeneral.Traducir_Error(ex.Message)
'            End Try

'        End Function

'        Public Function Eliminar(Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                Eliminar = Borrar_SQL(Me.strError)
'                Mensaje = Me.strError
'            Catch ex As Exception
'                Eliminar = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Eliminar: " & Me.objGeneral.Traducir_Error(ex.Message)
'            End Try
'        End Function

'        Public Function Eliminar_Foto(ByVal Placa As String) As Boolean

'            Try
'                Dim lonNumeRegi As Long
'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_eliminar_foto_Vehiculo", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Placa", SqlDbType.VarChar) : ComandoSQL.Parameters("@par_Placa").Value = Placa

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Eliminar_Foto = True
'                Else
'                    Eliminar_Foto = False
'                End If

'            Catch ex As Exception
'                Eliminar_Foto = False
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Eliminar_Foto: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try

'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Tiene_Estudio_Seguridad_Vehiculo(ByVal Placa As String) As Boolean
'            Try
'                Dim sdrTieneEstudioSeguridad As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT TOP 1 Numero"
'                strSQL += " FROM Estudio_seguridad_Vehiculos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Placa = '" & Me.strPlaca & "'"
'                strSQL += " AND Estado_Estudio = " & ESTADO_ESTUDIO_SEGURIDAD_APROBADO
'                strSQL += " AND Estado = " & General.ESTADO_DEFINITIVO
'                strSQL += " AND OFIC_Codigo = " & Me.objUsuario.Oficina.Codigo
'                strSQL += " AND DATEDIFF(day, Fecha_Aprueba, getdate()) <= " & EstudioSeguiridadVehiculos.DIAS_VIGENCIA_ESTUDIO

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()

'                sdrTieneEstudioSeguridad = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrTieneEstudioSeguridad.Read Then
'                    Me.objEstudioSeguridad.Numero = sdrTieneEstudioSeguridad.Item("Numero")
'                    Tiene_Estudio_Seguridad_Vehiculo = True
'                Else
'                    Tiene_Estudio_Seguridad_Vehiculo = False
'                End If

'                sdrTieneEstudioSeguridad.Close()
'                sdrTieneEstudioSeguridad.Dispose()

'            Catch ex As Exception
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Tiene_Estudio_Seguridad_Vehiculo: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Tiene_Estudio_Seguridad_Vehiculo = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Cambiar_Estado(ByVal intEstado As Integer, ByVal intCausaBloqueo As Integer) As Boolean
'            Try
'                Dim strSQL As String = ""

'                strSQL = "UPDATE Vehiculos"
'                strSQL += " SET Estado = " & intEstado
'                strSQL += " ,CIVE_Codigo = " & intCausaBloqueo
'                strSQL += " ,USUA_Modifica = '" & objUsuario.Codigo & "'"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo

'                strSQL += " AND Placa = '" & strPlaca & "'"

'                Cambiar_Estado = True
'                Me.objGeneral.Ejecutar_SQL(strSQL)
'            Catch ex As Exception
'                Cambiar_Estado = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Cambiar_Estado: " & Me.objGeneral.Traducir_Error(ex.Message)
'            End Try
'        End Function

'        Public Function Existe_Tipo_Tarifa_Tipo_Vehiculo(ByVal TipoContrato As Short, ByVal Contrato As Long, ByVal Ruta As Short) As Boolean

'            Try
'                Dim sdsTipoTarifas As DataSet
'                Dim strSQL As String
'                Dim intCont As Integer

'                strSQL = "SELECT DISTINCT(Valor_Tipo_Tarifa) as Valor_Tipo_Tarifa FROM V_Detalle_Ruta_Contratos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND ENCO_Numero = " & Contrato
'                strSQL += " AND ENCO_TIDO_Codigo = " & TipoContrato
'                strSQL += " AND RUTA_Codigo = " & Ruta
'                '    strSQL += " AND TRCC_Codigo = " & clsContratoClienteDetalleRuta.CODIGO_TARIFA_TIPO_VEHICULO

'                sdsTipoTarifas = Me.objGeneral.Retorna_Dataset(strSQL, Me.strError)
'                Existe_Tipo_Tarifa_Tipo_Vehiculo = False

'                If Len(Trim(strError)) = 0 Then
'                    If sdsTipoTarifas.Tables(Lista.PRIMERA_TABLA).Rows.Count > 0 Then
'                        For intCont = 0 To sdsTipoTarifas.Tables(Lista.PRIMERA_TABLA).Rows.Count - 1
'                            If Me.objCataTipoVehiculo.Retorna_Codigo = sdsTipoTarifas.Tables(Lista.PRIMERA_TABLA).Rows(intCont).Item("Valor_Tipo_Tarifa").ToString() Then
'                                Existe_Tipo_Tarifa_Tipo_Vehiculo = True
'                                Exit Function
'                            End If
'                        Next
'                    Else
'                        Existe_Tipo_Tarifa_Tipo_Vehiculo = True
'                    End If
'                End If
'                sdsTipoTarifas.Dispose()

'            Catch ex As Exception
'                Existe_Tipo_Tarifa_Tipo_Vehiculo = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Existe_Tipo_Tarifa_Tipo_Vehiculo: " & Me.objGeneral.Traducir_Error(ex.Message)
'            End Try
'        End Function

'        Public Function Verificar_Fechas_Vencidas() As Boolean
'            Try
'                Dim lonNumeRegi As Long
'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_deshabilitar_vehiculos", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure
'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Verificar_Fechas_Vencidas = True
'                Else
'                    Verificar_Fechas_Vencidas = False
'                End If

'            Catch ex As Exception
'                Verificar_Fechas_Vencidas = False
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Verificar_Fechas_Vencidas: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'#End Region

'        Public Function Tiene_Documentos_Estudio_Seguridad() As Boolean
'            Try
'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT COUNT(*)"
'                strSQL += " FROM Estudio_Seguridad_Vehiculo_Documentos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND EOES_Codigo = " & ORIGEN_VEHICULO

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionDocumentalSQL)
'                Me.objGeneral.ConexionDocumentalSQL.Open()

'                If ComandoSQL.ExecuteScalar.ToString > General.CERO Then
'                    Tiene_Documentos_Estudio_Seguridad = True
'                Else
'                    Tiene_Documentos_Estudio_Seguridad = False
'                End If

'            Catch ex As Exception
'                Me.strPlaca = ""
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)

'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Tiene_Documentos_Estudio_Seguridad = False
'            Finally
'                If Me.objGeneral.ConexionDocumentalSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionDocumentalSQL.Close()
'                End If
'            End Try

'        End Function

'#Region "Funciones Privadas"

'        Private Sub Instanciar_Objetos_Alternos()
'            Try
'                If IsNothing(Me.objEstudioSeguridad) Then
'                    Me.objEstudioSeguridad = New EstudioSeguiridadVehiculos(Me.objEmpresa)
'                End If
'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Instanciar_Objetos_Alternos: " & Me.objGeneral.Traducir_Error(ex.Message)
'            End Try
'        End Sub

'        Private Function Insertar_SQL() As Boolean
'            Dim lonNumeRegi As Long


'            Me.objGeneral.ConexionSQL.Open()
'            Try
'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_insertar_vehiculos", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Placa", SqlDbType.VarChar, 10) : ComandoSQL.Parameters("@par_Placa").Value = Me.strPlaca
'                ComandoSQL.Parameters.Add("@par_TERC_Propietario", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_TERC_Propietario").Value = Me.objPropietario.Codigo
'                ComandoSQL.Parameters.Add("@par_TERC_Tenedor", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_TERC_Tenedor").Value = Me.objTenedor.Codigo
'                ComandoSQL.Parameters.Add("@par_TERC_Conductor", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_TERC_Conductor").Value = Me.objConductor.Codigo

'                '   ComandoSQL.Parameters.Add("@par_SEMI_Placa", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_SEMI_Placa").Value = Me.objSemirremolque.Placa
'                ComandoSQL.Parameters.Add("@par_Propio", SqlDbType.Int) : ComandoSQL.Parameters("@par_Propio").Value = Me.objCataClasificacionVehiculo.Retorna_Codigo
'                ComandoSQL.Parameters.Add("@par_Tarjeta_Propiedad", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Tarjeta_Propiedad").Value = Me.strTarjetaPropiedad
'                ComandoSQL.Parameters.Add("@par_Organismo_Transito", SqlDbType.VarChar, 30) : ComandoSQL.Parameters("@par_Organismo_Transito").Value = Me.strOrganismoTransito
'                ComandoSQL.Parameters.Add("@par_MARC_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_MARC_Codigo").Value = Me.lonMarca

'                ComandoSQL.Parameters.Add("@par_Modelo", SqlDbType.Int) : ComandoSQL.Parameters("@par_Modelo").Value = Me.intModelo
'                ComandoSQL.Parameters.Add("@par_Modelo_Repotenciado", SqlDbType.Int) : ComandoSQL.Parameters("@par_Modelo_Repotenciado").Value = Me.intModeloRepotenciado
'                ComandoSQL.Parameters.Add("@par_TIVE_Codigo", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_TIVE_Codigo").Value = Me.objCataTipoVehiculo.Retorna_Codigo
'                ComandoSQL.Parameters.Add("@par_LINE_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_LINE_Codigo").Value = Me.strCodigoLinea
'                ComandoSQL.Parameters.Add("@par_Cilindraje", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Cilindraje").Value = Me.intCilindraje

'                ComandoSQL.Parameters.Add("@par_Clase", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Clase").Value = Me.strClase
'                ComandoSQL.Parameters.Add("@par_COLO_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_COLO_Codigo").Value = Val(Me.lonColor)
'                ComandoSQL.Parameters.Add("@par_TICA_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_TICA_Codigo").Value = Me.objCataTipoCarroceria.Retorna_Codigo
'                ComandoSQL.Parameters.Add("@par_Numero_Ejes", SqlDbType.Int) : ComandoSQL.Parameters("@par_Numero_Ejes").Value = Me.intNumeroEjes
'                ComandoSQL.Parameters.Add("@par_Numero_Motor", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Numero_Motor").Value = Me.strNumeroMotor

'                ComandoSQL.Parameters.Add("@par_Numero_Serie", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Numero_Serie").Value = Me.strNumeroSerie
'                ComandoSQL.Parameters.Add("@par_Capacidad", SqlDbType.Int) : ComandoSQL.Parameters("@par_Capacidad").Value = Me.intCapacidad
'                ComandoSQL.Parameters.Add("@par_Peso_Bruto", SqlDbType.Float) : ComandoSQL.Parameters("@par_Peso_Bruto").Value = Me.dblPesoBruto
'                ComandoSQL.Parameters.Add("@par_Registro_Nacional_Carga", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Registro_Nacional_Carga").Value = Me.strRegistroNacionalCarga
'                ComandoSQL.Parameters.Add("@par_Poliza_SOAT", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Poliza_SOAT").Value = Me.strPolizaSOAT

'                ComandoSQL.Parameters.Add("@par_Fecha_Vence_SOAT", SqlDbType.DateTime) : ComandoSQL.Parameters("@par_Fecha_Vence_SOAT").Value = Me.dteFechaVenceSOAT
'                ComandoSQL.Parameters.Add("@par_TERC_Aseguradora", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_TERC_Aseguradora").Value = Me.objAseguradora.Codigo
'                ComandoSQL.Parameters.Add("@par_Certificado_Conta", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Certificado_Conta").Value = Me.strCertificadoContaminacion
'                ComandoSQL.Parameters.Add("@par_Fecha_Vence_Certi", SqlDbType.DateTime) : ComandoSQL.Parameters("@par_Fecha_Vence_Certi").Value = Me.dteFechaVenceCertificado
'                ComandoSQL.Parameters.Add("@par_Valor_Vehiculo", SqlDbType.Float) : ComandoSQL.Parameters("@par_Valor_Vehiculo").Value = Me.dblValorVehiculo

'                ComandoSQL.Parameters.Add("@par_Tiempo_Util", SqlDbType.Int) : ComandoSQL.Parameters("@par_Tiempo_Util").Value = Me.intTiempoUtil
'                ComandoSQL.Parameters.Add("@par_Valor_Seguro", SqlDbType.Float) : ComandoSQL.Parameters("@par_Valor_Seguro").Value = Me.dblValorSeguro
'                ComandoSQL.Parameters.Add("@par_TERC_Afiliador", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_TERC_Afiliador").Value = Me.objAfiliador.Codigo
'                ComandoSQL.Parameters.Add("@par_Carnet_Empresa_Afiliadora", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Carnet_Empresa_Afiliadora").Value = Me.strCarnetEmpresaAfiliadora
'                ComandoSQL.Parameters.Add("@par_Vence_Empresa_Afiliadora", SqlDbType.DateTime) : ComandoSQL.Parameters("@par_Vence_Empresa_Afiliadora").Value = Me.dteFechaVenceAfiliadora

'                ComandoSQL.Parameters.Add("@par_Registro_Nacional_Combustible", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Registro_Nacional_Combustible").Value = Me.strRegistroNacionalCombustible
'                ComandoSQL.Parameters.Add("@par_Vence_Registro_Combustible", SqlDbType.DateTime) : ComandoSQL.Parameters("@par_Vence_Registro_Combustible").Value = Me.dteFechaVenceRegistroCombustible
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Codigo").Value = Me.strCodigo
'                ComandoSQL.Parameters.Add("@par_Estado", SqlDbType.Int) : ComandoSQL.Parameters("@par_Estado").Value = Me.objCataEstado.Retorna_Codigo
'                ComandoSQL.Parameters.Add("@par_Kilometraje", SqlDbType.Float) : ComandoSQL.Parameters("@par_Kilometraje").Value = Me.dblKilometraje

'                ComandoSQL.Parameters.Add("@par_Fecha_Actualiza_Kilometraje", SqlDbType.DateTime) : ComandoSQL.Parameters("@par_Fecha_Actualiza_Kilometraje").Value = Me.dteFechaActualizoKilometraje
'                ComandoSQL.Parameters.Add("@par_USUA_Crea", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_USUA_Crea").Value = Me.objUsuario.Codigo
'                ComandoSQL.Parameters.Add("@par_OFIC_codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_OFIC_codigo").Value = Me.objUsuario.Oficina.Codigo
'                ComandoSQL.Parameters.Add("@par_Capacidad_Metro_Cubicos", SqlDbType.Float) : ComandoSQL.Parameters("@par_Capacidad_Metro_Cubicos").Value = Me.dblCapacidadMetrosCubicos
'                ComandoSQL.Parameters.Add("@par_Fecha_Ultima_Revision_Mecanica", SqlDbType.DateTime) : ComandoSQL.Parameters("@par_Fecha_Ultima_Revision_Mecanica").Value = Me.dteFechaUltimaRevisionMecanica

'                ComandoSQL.Parameters.Add("@par_CIVE_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_CIVE_Codigo").Value = Me.objCataCausaInactividad.Retorna_Codigo
'                ComandoSQL.Parameters.Add("@par_TIEQ_Codigo", SqlDbType.VarChar, 10) : ComandoSQL.Parameters("@par_TIEQ_Codigo").Value = Me.objCataTipoEquipo.Retorna_Codigo
'                ComandoSQL.Parameters.Add("@par_Oficina_Principal", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Oficina_Principal").Value = Me.objUsuario.Oficina.Principal
'                ComandoSQL.Parameters.Add("@par_Justificacion_Bloqueo", SqlDbType.VarChar, 150) : ComandoSQL.Parameters("@par_Justificacion_Bloqueo").Value = Me.strJustificacionBloqueo
'                ComandoSQL.Parameters.Add("@par_Categoria_Peajes", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Categoria_Peajes").Value = Me.intCategoriaPeajes

'                ComandoSQL.Parameters.Add("@par_ESSE_Numero", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_ESSE_Numero").Value = Me.objEstudioSeguridad.Numero
'                ComandoSQL.Parameters.Add("@par_Tiene_GPS", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Tiene_GPS").Value = Me.intTieneGPS
'                ComandoSQL.Parameters.Add("@par_Nombre_Empresa_GPS", SqlDbType.VarChar, 50) : ComandoSQL.Parameters("@par_Nombre_Empresa_GPS").Value = Me.strNombreEmpresaGPS
'                ComandoSQL.Parameters.Add("@par_Clave_GPS", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Clave_GPS").Value = Me.strClaveGPS
'                ComandoSQL.Parameters.Add("@par_Telefono_GPS", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Telefono_GPS").Value = Me.strTelefonoGPS

'                ComandoSQL.Parameters.Add("@par_Usuario_GPS", SqlDbType.VarChar, 30) : ComandoSQL.Parameters("@par_Usuario_GPS").Value = Me.strUsuarioGPS
'                ComandoSQL.Parameters.Add("@par_REGE_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_REGE_Codigo").Value = Me.IntRegionGeografica

'                If Not IsNothing(Me.bytFoto) Then
'                    If Me.bytFoto(0) = BYTES_IMAGEN_DEFECTO Then
'                        ComandoSQL.Parameters.Add("@par_Foto", SqlDbType.Image) : ComandoSQL.Parameters("@par_Foto").Value = DBNull.Value
'                    Else
'                        ComandoSQL.Parameters.Add("@par_Foto", SqlDbType.Image) : ComandoSQL.Parameters("@par_Foto").Value = Me.bytFoto
'                    End If
'                Else
'                    ComandoSQL.Parameters.Add("@par_Foto", SqlDbType.Image) : ComandoSQL.Parameters("@par_Foto").Value = DBNull.Value
'                End If

'                ComandoSQL.Parameters.Add("@par_TGPS_Codigo", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_TGPS_Codigo").Value = Me.objCataTipoGps.Retorna_Codigo
'                ComandoSQL.Parameters.Add("@par_Interfaz_Avantel", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Interfaz_Avantel").Value = Me.intInterfazAvantel
'                ComandoSQL.Parameters.Add("@par_Interfaz_Tracker", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Interfaz_Tracker").Value = Me.intInterfazTracker
'                ComandoSQL.Parameters.Add("@par_Interfaz_Satrack", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Interfaz_Satrack").Value = Me.intInterfazSatrack
'                ComandoSQL.Parameters.Add("@par_Interfaz_Gps_Colombia", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Interfaz_Gps_Colombia").Value = Me.intInterfazGpsColombia
'                ComandoSQL.Parameters.Add("@par_PRVE_Codigo", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_PRVE_Codigo").Value = Val(Me.objCataProyectoVehiculo.Retorna_Codigo)
'                ComandoSQL.Parameters.Add("@par_CAVE_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_CAVE_Codigo").Value = Me.intTipoCapacidadVehiculo

'                ComandoSQL.Parameters.Add("@par_Poliza_Hidrocarburos", SqlDbType.VarChar) : ComandoSQL.Parameters("@par_Poliza_Hidrocarburos").Value = Me.strPoliza_Hidrocarburos
'                ComandoSQL.Parameters.Add("@par_Fecha_Vence_Poliza_Hidrocarburos", SqlDbType.Date) : ComandoSQL.Parameters("@par_Fecha_Vence_Poliza_Hidrocarburos").Value = Me.dteFecha_Vence_Poliza_Hidrocarburos
'                ComandoSQL.Parameters.Add("@par_Poliza_RC", SqlDbType.VarChar) : ComandoSQL.Parameters("@par_Poliza_RC").Value = Me.strPoliza_RC
'                ComandoSQL.Parameters.Add("@par_Fecha_Vence_Poliza_RC", SqlDbType.Date) : ComandoSQL.Parameters("@par_Fecha_Vence_Poliza_RC").Value = Me.dteFecha_Vence_Poliza_RC
'                ComandoSQL.Parameters.Add("@par_Poliza_RCE", SqlDbType.VarChar) : ComandoSQL.Parameters("@par_Poliza_RCE").Value = Me.strPoliza_RCE
'                ComandoSQL.Parameters.Add("@par_Fecha_Vence_Poliza_RCE", SqlDbType.Date) : ComandoSQL.Parameters("@par_Fecha_Vence_Poliza_RCE").Value = Me.dteFecha_Vence_Poliza_RCE

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Insertar_SQL = True
'                Else
'                    Insertar_SQL = False
'                End If

'            Catch ex As Exception
'                Insertar_SQL = False
'                Me.strPlaca = ""
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Insertar_SQL: " & Me.objGeneral.Traducir_Error(ex.Message)
'                    Insertar_SQL = False
'                End Try
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Modificar_SQL() As Boolean
'            Dim lonNumeRegi As Long


'            Me.objGeneral.ConexionSQL.Open()
'            Try
'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_modificar_vehiculos", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Placa", SqlDbType.VarChar, 10) : ComandoSQL.Parameters("@par_Placa").Value = Me.strPlaca
'                ComandoSQL.Parameters.Add("@par_TERC_Propietario", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_TERC_Propietario").Value = Me.objPropietario.Codigo
'                ComandoSQL.Parameters.Add("@par_TERC_Tenedor", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_TERC_Tenedor").Value = Me.objTenedor.Codigo
'                ComandoSQL.Parameters.Add("@par_TERC_Conductor", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_TERC_Conductor").Value = Me.objConductor.Codigo

'                ' ComandoSQL.Parameters.Add("@par_SEMI_Placa", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_SEMI_Placa").Value = Me.objSemirremolque.Placa
'                ComandoSQL.Parameters.Add("@par_Propio", SqlDbType.Int) : ComandoSQL.Parameters("@par_Propio").Value = Me.objCataClasificacionVehiculo.Retorna_Codigo
'                ComandoSQL.Parameters.Add("@par_Tarjeta_Propiedad", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Tarjeta_Propiedad").Value = Me.strTarjetaPropiedad
'                ComandoSQL.Parameters.Add("@par_Organismo_Transito", SqlDbType.VarChar, 30) : ComandoSQL.Parameters("@par_Organismo_Transito").Value = Me.strOrganismoTransito
'                ComandoSQL.Parameters.Add("@par_MARC_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_MARC_Codigo").Value = Me.lonMarca

'                ComandoSQL.Parameters.Add("@par_Modelo", SqlDbType.Int) : ComandoSQL.Parameters("@par_Modelo").Value = Me.intModelo
'                ComandoSQL.Parameters.Add("@par_Modelo_Repotenciado", SqlDbType.Int) : ComandoSQL.Parameters("@par_Modelo_Repotenciado").Value = Me.intModeloRepotenciado
'                ComandoSQL.Parameters.Add("@par_TIVE_Codigo", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_TIVE_Codigo").Value = Me.objCataTipoVehiculo.Retorna_Codigo
'                ComandoSQL.Parameters.Add("@par_LINE_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_LINE_Codigo").Value = Me.strCodigoLinea
'                ComandoSQL.Parameters.Add("@par_Cilindraje", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Cilindraje").Value = Me.intCilindraje

'                ComandoSQL.Parameters.Add("@par_Clase", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Clase").Value = Me.strClase
'                ComandoSQL.Parameters.Add("@par_COLO_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_COLO_Codigo").Value = Val(Me.lonColor)
'                ComandoSQL.Parameters.Add("@par_TICA_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_TICA_Codigo").Value = Me.objCataTipoCarroceria.Retorna_Codigo
'                ComandoSQL.Parameters.Add("@par_Numero_Ejes", SqlDbType.Int) : ComandoSQL.Parameters("@par_Numero_Ejes").Value = Me.intNumeroEjes
'                ComandoSQL.Parameters.Add("@par_Numero_Motor", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Numero_Motor").Value = Me.strNumeroMotor

'                ComandoSQL.Parameters.Add("@par_Numero_Serie", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Numero_Serie").Value = Me.strNumeroSerie
'                ComandoSQL.Parameters.Add("@par_Capacidad", SqlDbType.Int) : ComandoSQL.Parameters("@par_Capacidad").Value = Me.intCapacidad
'                ComandoSQL.Parameters.Add("@par_Peso_Bruto", SqlDbType.Float) : ComandoSQL.Parameters("@par_Peso_Bruto").Value = Me.dblPesoBruto
'                ComandoSQL.Parameters.Add("@par_Registro_Nacional_Carga", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Registro_Nacional_Carga").Value = Me.strRegistroNacionalCarga
'                ComandoSQL.Parameters.Add("@par_Poliza_SOAT", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Poliza_SOAT").Value = Me.strPolizaSOAT

'                ComandoSQL.Parameters.Add("@par_Fecha_Vence_SOAT", SqlDbType.DateTime) : ComandoSQL.Parameters("@par_Fecha_Vence_SOAT").Value = Me.dteFechaVenceSOAT
'                ComandoSQL.Parameters.Add("@par_TERC_Aseguradora", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_TERC_Aseguradora").Value = Me.objAseguradora.Codigo
'                ComandoSQL.Parameters.Add("@par_Certificado_Conta", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Certificado_Conta").Value = Me.strCertificadoContaminacion
'                ComandoSQL.Parameters.Add("@par_Fecha_Vence_Certi", SqlDbType.DateTime) : ComandoSQL.Parameters("@par_Fecha_Vence_Certi").Value = Me.dteFechaVenceCertificado
'                ComandoSQL.Parameters.Add("@par_Valor_Vehiculo", SqlDbType.Float) : ComandoSQL.Parameters("@par_Valor_Vehiculo").Value = Me.dblValorVehiculo

'                ComandoSQL.Parameters.Add("@par_Tiempo_Util", SqlDbType.Int) : ComandoSQL.Parameters("@par_Tiempo_Util").Value = Me.intTiempoUtil
'                ComandoSQL.Parameters.Add("@par_Valor_Seguro", SqlDbType.Float) : ComandoSQL.Parameters("@par_Valor_Seguro").Value = Me.dblValorSeguro
'                ComandoSQL.Parameters.Add("@par_TERC_Afiliador", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_TERC_Afiliador").Value = Me.objAfiliador.Codigo
'                ComandoSQL.Parameters.Add("@par_Carnet_Empresa_Afiliadora", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Carnet_Empresa_Afiliadora").Value = Me.strCarnetEmpresaAfiliadora
'                ComandoSQL.Parameters.Add("@par_Vence_Empresa_Afiliadora", SqlDbType.DateTime) : ComandoSQL.Parameters("@par_Vence_Empresa_Afiliadora").Value = Me.dteFechaVenceAfiliadora

'                ComandoSQL.Parameters.Add("@par_Registro_Nacional_Combustible", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Registro_Nacional_Combustible").Value = Me.strRegistroNacionalCombustible
'                ComandoSQL.Parameters.Add("@par_Vence_Registro_Combustible", SqlDbType.DateTime) : ComandoSQL.Parameters("@par_Vence_Registro_Combustible").Value = Me.dteFechaVenceRegistroCombustible
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Codigo").Value = Me.strCodigo
'                ComandoSQL.Parameters.Add("@par_Estado", SqlDbType.Int) : ComandoSQL.Parameters("@par_Estado").Value = Me.objCataEstado.Retorna_Codigo
'                ComandoSQL.Parameters.Add("@par_Kilometraje", SqlDbType.Float) : ComandoSQL.Parameters("@par_Kilometraje").Value = Me.dblKilometraje

'                ComandoSQL.Parameters.Add("@par_Fecha_Actualiza_Kilometraje", SqlDbType.DateTime) : ComandoSQL.Parameters("@par_Fecha_Actualiza_Kilometraje").Value = Me.dteFechaActualizoKilometraje
'                ComandoSQL.Parameters.Add("@par_USUA_Modifica", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_USUA_Modifica").Value = Me.objUsuario.Codigo
'                ComandoSQL.Parameters.Add("@par_OFIC_codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_OFIC_codigo").Value = Me.objUsuario.Oficina.Codigo
'                ComandoSQL.Parameters.Add("@par_Capacidad_Metro_Cubicos", SqlDbType.Float) : ComandoSQL.Parameters("@par_Capacidad_Metro_Cubicos").Value = Me.dblCapacidadMetrosCubicos
'                ComandoSQL.Parameters.Add("@par_Fecha_Ultima_Revision_Mecanica", SqlDbType.DateTime) : ComandoSQL.Parameters("@par_Fecha_Ultima_Revision_Mecanica").Value = Me.dteFechaUltimaRevisionMecanica

'                ComandoSQL.Parameters.Add("@par_CIVE_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_CIVE_Codigo").Value = Me.objCataCausaInactividad.Retorna_Codigo
'                ComandoSQL.Parameters.Add("@par_TIEQ_Codigo", SqlDbType.VarChar, 10) : ComandoSQL.Parameters("@par_TIEQ_Codigo").Value = Me.objCataTipoEquipo.Retorna_Codigo
'                ComandoSQL.Parameters.Add("@par_Oficina_Principal", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Oficina_Principal").Value = Me.objUsuario.Oficina.Principal
'                ComandoSQL.Parameters.Add("@par_Justificacion_Bloqueo", SqlDbType.VarChar, 150) : ComandoSQL.Parameters("@par_Justificacion_Bloqueo").Value = Me.strJustificacionBloqueo
'                ComandoSQL.Parameters.Add("@par_Categoria_Peajes", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Categoria_Peajes").Value = Me.intCategoriaPeajes
'                ComandoSQL.Parameters.Add("@par_ESSE_Numero", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_ESSE_Numero").Value = Me.objEstudioSeguridad.Numero

'                If Not IsNothing(Me.bytFoto) Then
'                    If Me.bytFoto(0) = BYTES_IMAGEN_DEFECTO Then
'                        ComandoSQL.Parameters.Add("@par_Foto", SqlDbType.Image) : ComandoSQL.Parameters("@par_Foto").Value = DBNull.Value
'                    Else
'                        ComandoSQL.Parameters.Add("@par_Foto", SqlDbType.Image) : ComandoSQL.Parameters("@par_Foto").Value = Me.bytFoto
'                    End If
'                Else
'                    ComandoSQL.Parameters.Add("@par_Foto", SqlDbType.Image) : ComandoSQL.Parameters("@par_Foto").Value = DBNull.Value
'                End If

'                ComandoSQL.Parameters.Add("@par_Tiene_GPS", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Tiene_GPS").Value = Me.intTieneGPS
'                ComandoSQL.Parameters.Add("@par_Nombre_Empresa_GPS", SqlDbType.VarChar, 50) : ComandoSQL.Parameters("@par_Nombre_Empresa_GPS").Value = Me.strNombreEmpresaGPS
'                ComandoSQL.Parameters.Add("@par_Clave_GPS", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Clave_GPS").Value = Me.strClaveGPS
'                ComandoSQL.Parameters.Add("@par_Telefono_GPS", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Telefono_GPS").Value = Me.strTelefonoGPS
'                ComandoSQL.Parameters.Add("@par_Usuario_GPS", SqlDbType.VarChar, 30) : ComandoSQL.Parameters("@par_Usuario_GPS").Value = Me.strUsuarioGPS

'                ComandoSQL.Parameters.Add("@par_TGPS_Codigo", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_TGPS_Codigo").Value = Me.objCataTipoGps.Retorna_Codigo
'                ComandoSQL.Parameters.Add("@par_Interfaz_Avantel", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Interfaz_Avantel").Value = Me.intInterfazAvantel
'                ComandoSQL.Parameters.Add("@par_Interfaz_Tracker", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Interfaz_Tracker").Value = Me.intInterfazTracker
'                ComandoSQL.Parameters.Add("@par_Interfaz_Satrack", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Interfaz_Satrack").Value = Me.intInterfazSatrack
'                ComandoSQL.Parameters.Add("@par_Interfaz_Gps_Colombia", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Interfaz_Gps_Colombia").Value = Me.intInterfazGpsColombia
'                ComandoSQL.Parameters.Add("@par_PRVE_Codigo", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_PRVE_Codigo").Value = Val(Me.objCataProyectoVehiculo.Retorna_Codigo)
'                ComandoSQL.Parameters.Add("@par_CAVE_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_CAVE_Codigo").Value = Me.intTipoCapacidadVehiculo

'                ComandoSQL.Parameters.Add("@par_Poliza_Hidrocarburos", SqlDbType.VarChar) : ComandoSQL.Parameters("@par_Poliza_Hidrocarburos").Value = Me.strPoliza_Hidrocarburos
'                ComandoSQL.Parameters.Add("@par_Fecha_Vence_Poliza_Hidrocarburos", SqlDbType.Date) : ComandoSQL.Parameters("@par_Fecha_Vence_Poliza_Hidrocarburos").Value = Me.dteFecha_Vence_Poliza_Hidrocarburos
'                ComandoSQL.Parameters.Add("@par_Poliza_RC", SqlDbType.VarChar) : ComandoSQL.Parameters("@par_Poliza_RC").Value = Me.strPoliza_RC
'                ComandoSQL.Parameters.Add("@par_Fecha_Vence_Poliza_RC", SqlDbType.Date) : ComandoSQL.Parameters("@par_Fecha_Vence_Poliza_RC").Value = Me.dteFecha_Vence_Poliza_RC
'                ComandoSQL.Parameters.Add("@par_Poliza_RCE", SqlDbType.VarChar) : ComandoSQL.Parameters("@par_Poliza_RCE").Value = Me.strPoliza_RCE
'                ComandoSQL.Parameters.Add("@par_Fecha_Vence_Poliza_RCE", SqlDbType.Date) : ComandoSQL.Parameters("@par_Fecha_Vence_Poliza_RCE").Value = Me.dteFecha_Vence_Poliza_RCE
'                ComandoSQL.Parameters.Add("@par_REGE_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_REGE_Codigo").Value = Me.IntRegionGeografica

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Modificar_SQL = True
'                Else
'                    Modificar_SQL = False
'                End If

'            Catch ex As Exception
'                Modificar_SQL = False
'                Me.strPlaca = ""
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Modificar_SQL: " & Me.objGeneral.Traducir_Error(ex.Message)
'                    Modificar_SQL = False
'                End Try
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Borrar_SQL(Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                Dim lonNumeRegi As Long
'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_borrar_vehiculos", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Placa", SqlDbType.VarChar, 10) : ComandoSQL.Parameters("@par_Placa").Value = Me.strPlaca
'                ComandoSQL.Parameters.Add("@par_Mensaje", SqlDbType.VarChar, 200) : ComandoSQL.Parameters("@par_Mensaje").Direction = ParameterDirection.Output

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Borrar_SQL = True
'                    Mensaje = ComandoSQL.Parameters("@par_Mensaje").Value
'                Else
'                    Borrar_SQL = False
'                    Mensaje = ComandoSQL.Parameters("@par_Mensaje").Value
'                End If

'            Catch ex As Exception
'                Borrar_SQL = False
'                Me.strPlaca = ""
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Borrar_SQL = False
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Borrar_SQL: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Datos_Requeridos(ByRef Mensaje As String) As Boolean

'            Try

'                Me.strError = ""
'                Dim intCont As Short
'                intCont = 0
'                Datos_Requeridos = True
'                Dim bolValEmpAfi As Boolean = True
'                Dim bolSemiremolqueValido As Boolean = True
'                'Dim bolVerificaPesoMinisterio As Boolean = Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_PESO_VEHICULO)

'                Call Instanciar_Objetos_Alternos()

'                If Len(Trim(Me.strPlaca)) = 0 Then
'                    intCont += 1
'                    Me.strError = intCont & ". Debe digitar la placa del vehículo."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                Else
'                    If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_ESTUDIO_SEGURIDAD) Then
'                        If Me.objValidacion.Valida Then
'                            If Not bolModificar Then
'                                If Not Me.Tiene_Estudio_Seguridad_Vehiculo(Me.strPlaca) Then
'                                    intCont += 1
'                                    Me.strError += intCont & ". El vehículo ingresado no tiene un Estudio de seguridad vigente aprobado para esta oficina."
'                                    Me.strError += " <Br />"
'                                    Datos_Requeridos = False
'                                End If
'                            End If
'                        End If
'                    End If

'                    If Me.objGeneral.Validar_Placa_Vehiculo(Me.strPlaca) = False Then
'                        intCont += 1
'                        Me.strError = intCont & ". La placa del vehículo no es correcta."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If

'                    If Vehiculo_Lista_Negra() Then
'                        intCont += 1
'                        Me.strError = intCont & ". La placa del vehículo se encuentra en la lista negra de vehículos, no es posible crear el mismo."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If

'                End If

'                '   Me.objSemirremolque.Consultar()
'                '  Me.objSemirremolque.CataTipoSemirremolque.Consultar()
'                Me.objCataTipoVehiculo.Consultar()
'                If Val(Me.objCataTipoVehiculo.Campo5) = Vehiculos.TIPO_VEHICULO_CODIGO_MINISTERIO_50 Or Val(Me.objCataTipoVehiculo.Campo5) = Vehiculos.TIPO_VEHICULO_CODIGO_MINISTERIO_51 Then
'                    If strPlaca <> String.Empty Then
'                        '     If (Val(Me.objSemirremolque.CataTipoSemirremolque.Campo3) = Semirremolque.TIPO_SEMIREMOLQUE_COD_MIN_61 Or
'                        '    Val(Me.objSemirremolque.CataTipoSemirremolque.Campo3) = Semirremolque.TIPO_SEMIREMOLQUE_COD_MIN_62 Or
'                        '  Val(Me.objSemirremolque.CataTipoSemirremolque.Campo3) = Semirremolque.TIPO_SEMIREMOLQUE_COD_MIN_63 Or
'                        '   Val(Me.objSemirremolque.CataTipoSemirremolque.Campo3) = Semirremolque.TIPO_SEMIREMOLQUE_COD_MIN_64) Then

'                        'intCont += 1
'                        '    Me.strError += intCont & ". El vehículo es un camión rigido de 2 ejes, por lo tanto NO puede estar asociado a un semiremolque."
'                        '    Me.strError += " <Br />"

'                        '    Datos_Requeridos = False
'                        '    bolSemiremolqueValido = False

'                        ''  End If

'                        ''  If (Val(Me.objSemirremolque.CataTipoSemirremolque.Campo3) = Semirremolque.TIPO_SEMIREMOLQUE_COD_MIN_73) Then

'                        'intCont += 1
'                        '    Me.strError += intCont & ". El vehículo es un camión rigido de 2 ejes, por lo tanto NO puede estar asociado a un remolque de 4 Ejes."
'                        '    Me.strError += " <Br />"

'                        '    bolSemiremolqueValido = False
'                        '    Datos_Requeridos = False
'                        ' End If
'                    End If
'                End If

'                If Val(Me.objCataTipoVehiculo.Campo5) = Vehiculos.TIPO_VEHICULO_CODIGO_MINISTERIO_52 Then
'                    If strPlaca <> String.Empty Then
'                        ' If (Val(Me.objSemirremolque.CataTipoSemirremolque.Campo3) = Semirremolque.TIPO_SEMIREMOLQUE_COD_MIN_61 Or
'                        'Val(Me.objSemirremolque.CataTipoSemirremolque.Campo3) = Semirremolque.TIPO_SEMIREMOLQUE_COD_MIN_62 Or
'                        'Val(Me.objSemirremolque.CataTipoSemirremolque.Campo3) = Semirremolque.TIPO_SEMIREMOLQUE_COD_MIN_63 Or
'                        'Val(Me.objSemirremolque.CataTipoSemirremolque.Campo3) = Semirremolque.TIPO_SEMIREMOLQUE_COD_MIN_64) Then

'                        '     intCont += 1
'                        '     Me.strError += intCont & ". El vehículo es un camión rigido de 3 ejes, por lo tanto NO puede estar asociado a un semiremolque."
'                        '     Me.strError += " <Br />"

'                        '     bolSemiremolqueValido = False
'                        '     Datos_Requeridos = False
'                        ' End If
'                    End If
'                End If

'                If Val(Me.objCataTipoVehiculo.Campo5) = Vehiculos.TIPO_VEHICULO_CODIGO_MINISTERIO_53 Then
'                    If strPlaca <> String.Empty Then
'                        'If Not (Val(Me.objSemirremolque.CataTipoSemirremolque.Campo3) = Semirremolque.TIPO_SEMIREMOLQUE_COD_MIN_61 Or
'                        'Val(Me.objSemirremolque.CataTipoSemirremolque.Campo3) = Semirremolque.TIPO_SEMIREMOLQUE_COD_MIN_62 Or
'                        'Val(Me.objSemirremolque.CataTipoSemirremolque.Campo3) = Semirremolque.TIPO_SEMIREMOLQUE_COD_MIN_63) Then

'                        '    intCont += 1
'                        '    Me.strError += intCont & ". El vehículo es un tractocamión de 2 ejes, por lo tanto debe estar asociado a un semiremolque de 1, 2 o 3 Ejes."
'                        '    Me.strError += " <Br />"

'                        '    bolSemiremolqueValido = False
'                        '    Datos_Requeridos = False
'                        'End If
'                    Else

'                        intCont += 1
'                        Me.strError += intCont & ". El vehículo es un tractocamión de 2 ejes, por lo tanto debe estar asociado a un semiremolque."
'                        Me.strError += " <Br />"

'                        bolSemiremolqueValido = False
'                        Datos_Requeridos = False
'                    End If

'                End If

'                If Val(Me.objCataTipoVehiculo.Campo5) = Vehiculos.TIPO_VEHICULO_CODIGO_MINISTERIO_54 Then
'                    If strPlaca <> String.Empty Then
'                        'If Not (Val(Me.objSemirremolque.CataTipoSemirremolque.Campo3) = Semirremolque.TIPO_SEMIREMOLQUE_COD_MIN_61 Or
'                        'Val(Me.objSemirremolque.CataTipoSemirremolque.Campo3) = Semirremolque.TIPO_SEMIREMOLQUE_COD_MIN_62 Or
'                        'Val(Me.objSemirremolque.CataTipoSemirremolque.Campo3) = Semirremolque.TIPO_SEMIREMOLQUE_COD_MIN_63 Or
'                        'Val(Me.objSemirremolque.CataTipoSemirremolque.Campo3) = Semirremolque.TIPO_SEMIREMOLQUE_COD_MIN_64) Then

'                        '    intCont += 1
'                        '    Me.strError += intCont & ". El vehículo es un tractocamión de 3 ejes, por lo tanto debe estar asociado a un remolque de tipo semiremolque."
'                        '    Me.strError += " <Br />"

'                        '    bolSemiremolqueValido = False
'                        '    Datos_Requeridos = False
'                        'End If
'                    Else
'                        intCont += 1
'                        Me.strError += intCont & ".El vehículo es un tractocamión de 3 ejes, por lo tanto debe estar asociado a un semiremolque."
'                        Me.strError += " <Br />"

'                        bolSemiremolqueValido = False
'                        Datos_Requeridos = False
'                    End If
'                End If

'                If Me.objCataClasificacionVehiculo.Campo1 = PROPIO Then
'                    'EG: Validación que permite en el despacho validar de forma correcta el valor del flete
'                    If Me.objPropietario.Codigo <> 0 Then
'                        Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "Terceros", "CONCAT(Numero_Identificacion, Digito_Chequeo) As Identificacion", "Codigo", General.CAMPO_NUMERICO, Me.objPropietario.Codigo, Me.objPropietario.NumeroIdentificacion, "", "", "Identificacion")
'                        Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "Terceros", "CONCAT(Numero_Identificacion, Digito_Chequeo) As Identificacion", "Codigo", General.CAMPO_NUMERICO, Me.objTenedor.Codigo, Me.objTenedor.NumeroIdentificacion, "", "", "Identificacion")
'                        'EG: Se valida con respecto al Nit_Manifiesto_Electronico dado que con este NIT es que el RNDC valida que el vehículos sea propio
'                        Me.objEmpresa.Retorna_Campo(Me.objEmpresa.Codigo, "Nit_Manifiesto_Electronico", Me.objEmpresa.NitManifiestoElectronico)

'                        If Me.objPropietario.NumeroIdentificacion <> Me.objEmpresa.NitManifiestoElectronico Then
'                            If Me.objTenedor.NumeroIdentificacion <> Me.objEmpresa.NitManifiestoElectronico Then
'                                intCont += 1
'                                Me.strError += intCont & ". El vehículo no puede ser marcado como propio dado que el propietario o tenedor del vehículo no corresponde a la empresa de transporte"
'                                Me.strError += " <Br />"
'                                Datos_Requeridos = False
'                            End If
'                        End If
'                    End If
'                End If


'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_CODIGO_VEHICULO) Then
'                    If Me.objValidacion.Valida Then
'                        If Trim(Me.strCodigo) = "" Then
'                            intCont += 1
'                            Me.strError += intCont & ". Debe ingresar un código de vehículo válido"
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If
'                    End If
'                End If

'                If Me.objPropietario.Codigo = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe seleccionar a un propietario."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.objTenedor.Codigo = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe seleccionar a un tenedor."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.objConductor.Codigo = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe seleccionar a un conductor."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.intModelo = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar el modelo del vehículo."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.objCataTipoVehiculo.Retorna_Codigo = "" Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe seleccionar el tipo de vehículo."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.intNumeroEjes = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar el número de ejes del vehículo."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.dblPesoBruto = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar el peso bruto del vehículo."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                Else
'                    'If bolVerificaPesoMinisterio Then
'                    If Me.objCataTipoVehiculo.Retorna_Codigo <> "" Then
'                        If Me.dblPesoBruto > (General.PESO_VALOR_DEFECTO_CABEZOTE / General.FACTOR_CONVERSION_X_MIL) Then
'                            intCont += 1
'                            Me.strError += intCont & ". El peso bruto del vehículo no puede ser mayor a 16 toneladas."
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If
'                    End If
'                    'End If
'                End If

'                If Trim(Me.strNumeroMotor) = "" Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar el número del motor del vehículo."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.intCapacidad = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar la capacidad del vehículo."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                Else
'                    'If bolVerificaPesoMinisterio Then
'                    'If Me.intCapacidad > VehiculosRNDC.PESO_MAXIMO_VEHICULO_SIN_REMOLQUE And _
'                    'Val(Me.objCataTipoVehiculo.Retorna_Codigo) <> VehiculosRNDC.CABEZOTE_TRES_EJES _
'                    'And Val(Me.objCataTipoVehiculo.Retorna_Codigo) <> VehiculosRNDC.CABEZOTE_DOS_EJES _
'                    'And Me.objSemirremolque.Placa.Trim = String.Empty Then
'                    '    intCont += 1
'                    '    Me.strError += intCont & ". La capacidad para este tipo de vehículo no puede ser mayor a 37 toneladas sin tener un semirremolque asociado."
'                    '    Me.strError += " <Br />"
'                    '    Datos_Requeridos = False
'                    'End If
'                    'End If

'                    'If Not Validar_Pesos(intCont, Me.strPlaca, Me.objSemirremolque.Placa) Then
'                    '    Datos_Requeridos = False
'                    'End If
'                End If

'                If Trim(Me.strTarjetaPropiedad) = "" Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar el número de la tarjeta de propiedad del vehículo."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_CLASE_VEHICULO) Then
'                    If Me.objValidacion.Valida Then
'                        If Trim(Me.strClase) = "" Then
'                            intCont += 1
'                            Me.strError += intCont & ". " & Me.objValidacion.MensajeValidacion
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If
'                    End If
'                End If

'                If Trim(Me.strNumeroSerie) = "" Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar el número de serie del vehículo."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_CERTIFICADO_COMBUSTIBLE) Then
'                    If Me.objValidacion.Valida Then
'                        If Trim(Me.strRegistroNacionalCombustible) = String.Empty Then
'                            intCont += 1
'                            Me.strError += intCont & ". " & objValidacion.MensajeValidacion
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If

'                        If Not (Me.dteFechaVenceRegistroCombustible.Date = Date.MinValue.Date Or Me.dteFechaVenceRegistroCombustible.Date = Date.Parse(General.FECHA_NULA).Date) Then
'                            If Not Me.objGeneral.Fecha_Valida(Me.dteFechaVenceRegistroCombustible) Then
'                                intCont += 1
'                                Me.strError += intCont & ". " & Me.objValidacion.MensajeValidacion
'                                Me.strError += " <Br />"
'                                Datos_Requeridos = False
'                            End If
'                        Else
'                            intCont += 1
'                            Me.strError += intCont & ". " & Me.objValidacion.MensajeValidacion
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If

'                    Else
'                        If Me.dteFechaVenceRegistroCombustible.Date = Date.MinValue.Date Or Me.dteFechaVenceRegistroCombustible.Date = Date.Parse(General.FECHA_NULA).Date Then
'                            Me.dteFechaVenceRegistroCombustible = General.FECHA_NULA
'                        End If
'                    End If
'                Else
'                    If Me.dteFechaVenceRegistroCombustible.Date = Date.MinValue.Date Or Me.dteFechaVenceRegistroCombustible.Date = Date.Parse(General.FECHA_NULA).Date Then
'                        Me.dteFechaVenceRegistroCombustible = General.FECHA_NULA
'                    End If
'                End If

'                If Trim(Me.strPolizaSOAT) = "" Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar la póliza SOAT."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Not (Me.dteFechaVenceSOAT = Date.MinValue Or Me.dteFechaVenceSOAT = Date.Parse(General.FECHA_NULA)) Then
'                    If Not Me.objGeneral.Fecha_Valida(Me.dteFechaVenceSOAT) Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar la fecha de vencimiento del SOAT, digite dd/mm/aaaa."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If

'                    If Me.dteFechaVenceSOAT <= Date.Today Then
'                        intCont += 1
'                        Me.strError += intCont & ". La fecha de vencimiento del SOAT no puede ser anterior a la fecha actual."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                Else
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar la fecha de vencimiento del SOAT, digite dd/mm/aaaa."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.objAseguradora.Codigo = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe seleccionar a una aseguradora."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Trim(Me.strOrganismoTransito) = "" Then
'                    If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_ORGANISMO_TRANSITO) Then
'                        If Me.objValidacion.Valida Then
'                            intCont += 1
'                            Me.strError += intCont & ". Debe ingresar el organismo de tránsito."
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If
'                    End If
'                End If

'                If Trim(Me.strOrganismoTransito) = "" Then
'                    If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_ORGANISMO_TRANSITO) Then
'                        If Me.objValidacion.Valida Then
'                            intCont += 1
'                            Me.strError += intCont & ". Debe ingresar el organismo de tránsito."
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If
'                    End If
'                End If

'                'También se llama Quita Rueda
'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_CERTIFICADO_COMTAMINACION) Then
'                    If Me.objValidacion.Valida Then
'                        If Trim(Me.strCertificadoContaminacion) = "" Then
'                            intCont += 1
'                            Me.strError += intCont & ". " & Me.objValidacion.MensajeValidacion
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If

'                        If Me.dteFechaVenceCertificado = Date.MinValue Then
'                            Me.dteFechaVenceCertificado = General.FECHA_NULA
'                            intCont += 1
'                            Me.strError += intCont & ". " & Me.objValidacion.MensajeValidacion
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        Else
'                            If Not (Me.dteFechaVenceCertificado = Date.MinValue Or Me.dteFechaVenceCertificado = Date.Parse(General.FECHA_NULA)) Then
'                                If Not Me.objGeneral.Fecha_Valida(Me.dteFechaVenceCertificado) Then
'                                    intCont += 1
'                                    Me.strError += intCont & ". " & Me.objValidacion.MensajeValidacion
'                                    Me.strError += " <Br />"
'                                    Datos_Requeridos = False
'                                End If
'                            Else
'                                intCont += 1
'                                Me.strError += intCont & ". " & Me.objValidacion.MensajeValidacion
'                                Me.strError += " <Br />"
'                                Datos_Requeridos = False
'                            End If
'                        End If

'                    End If
'                End If



'                If Me.dteFechaVenceCertificado = Date.MinValue Then
'                    Me.dteFechaVenceCertificado = General.FECHA_NULA
'                End If

'                If Me.dteFechaActualizoKilometraje = Date.MinValue Then
'                    Me.dteFechaActualizoKilometraje = General.FECHA_NULA
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_REVISION_TECNICOMECANICA) Then
'                    If Me.objValidacion.Valida Then
'                        If Trim(Me.strRegistroNacionalCarga) = "" Then
'                            intCont += 1
'                            Me.strError += intCont & ". Debe ingresar la revisión Tecnicomecánica."
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If

'                        If Not (Me.dteFechaUltimaRevisionMecanica = Date.MinValue Or Me.dteFechaUltimaRevisionMecanica = Date.Parse(General.FECHA_NULA)) Then
'                            If Not Me.objGeneral.Fecha_Valida(Me.dteFechaUltimaRevisionMecanica) Then
'                                intCont += 1
'                                Me.strError += intCont & ". Fecha de vencimiento de la última revisión Tecnicomecánica invalida, digite dd/mm/aaaa."
'                                Me.strError += " <Br />"
'                                Datos_Requeridos = False
'                            Else
'                                If Me.dteFechaUltimaRevisionMecanica <= Date.Now Then
'                                    intCont += 1
'                                    Me.strError += intCont & ". La Fecha de vencimiento de la última revisión Tecnicomecánica no puede ser menor a la fecha actual."
'                                    Me.strError += " <Br />"
'                                    Datos_Requeridos = False
'                                End If
'                            End If

'                        Else
'                            intCont += 1
'                            Me.strError += intCont & ". Debe ingresar la fecha de vencimiento de la última Revisión Tecnicomecánica, digite dd/mm/aaaa."
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If
'                    Else
'                        If Me.dteFechaUltimaRevisionMecanica = Date.MinValue Then
'                            Me.dteFechaUltimaRevisionMecanica = General.FECHA_NULA
'                        End If
'                    End If
'                Else
'                    If Me.dteFechaUltimaRevisionMecanica = Date.MinValue Then
'                        Me.dteFechaUltimaRevisionMecanica = General.FECHA_NULA
'                    End If
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_DATOS_POLIZA_HIDROCARBUROS) Then
'                    If Trim(Me.strPoliza_Hidrocarburos) = "" Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar la póliza de Hidrocarburos"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                    If Not (Me.dteFecha_Vence_Poliza_Hidrocarburos = Date.MinValue Or Me.dteFecha_Vence_Poliza_Hidrocarburos = Date.Parse(General.FECHA_NULA)) Then
'                        If Not Me.objGeneral.Fecha_Valida(Me.dteFecha_Vence_Poliza_Hidrocarburos) Then
'                            intCont += 1
'                            Me.strError += intCont & ". Fecha de póliza de Hidrocarburos invalida, digite dd/mm/aaaa."
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If
'                    Else
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar la fecha de la póliza de Hidrocarburos, digite dd/mm/aaaa."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'                If Me.dteFecha_Vence_Poliza_Hidrocarburos = Date.MinValue Then
'                    Me.dteFecha_Vence_Poliza_Hidrocarburos = General.FECHA_NULA
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_DATOS_POLIZA_RC) Then
'                    If Trim(Me.strPoliza_RC) = "" Then
'                        intCont += 1
'                        Me.strError += intCont & " . Debe ingresar la información de " & Me.objValidacion.MensajeValidacion
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                    If Not (Me.dteFecha_Vence_Poliza_RC = Date.MinValue Or Me.dteFecha_Vence_Poliza_RC = Date.Parse(General.FECHA_NULA)) Then
'                        If Not Me.objGeneral.Fecha_Valida(Me.dteFecha_Vence_Poliza_RC) Then
'                            intCont += 1
'                            Me.strError += intCont & ". Fecha de " & Me.objValidacion.MensajeValidacion & " es inválida, digite dd/mm/aaaa."
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If
'                    Else
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar la fecha de " & Me.objValidacion.MensajeValidacion & ", digite dd/mm/aaaa."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_DATOS_POLIZA_RCE) Then
'                    If Trim(Me.strPoliza_RCE) = "" Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar informacion de " & Me.objValidacion.MensajeValidacion
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                    If Not (Me.dteFecha_Vence_Poliza_RCE = Date.MinValue Or Me.dteFecha_Vence_Poliza_RCE = Date.Parse(General.FECHA_NULA)) Then
'                        If Not Me.objGeneral.Fecha_Valida(Me.dteFecha_Vence_Poliza_RCE) Then
'                            intCont += 1
'                            Me.strError += intCont & ". Fecha de " & Me.objValidacion.MensajeValidacion & " es inválida, digite dd/mm/aaaa."
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If
'                    Else
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar la fecha de " & Me.objValidacion.MensajeValidacion & ", digite dd/mm/aaaa."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If


'                If Me.dteFecha_Vence_Poliza_RC = Date.MinValue Then
'                    Me.dteFecha_Vence_Poliza_RC = General.FECHA_NULA
'                End If
'                If Me.dteFecha_Vence_Poliza_RCE = Date.MinValue Then
'                    Me.dteFecha_Vence_Poliza_RCE = General.FECHA_NULA
'                End If


'                If Me.objCataClasificacionVehiculo.Retorna_Codigo = Vehiculos.PROPIO Then
'                    If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_VALOR_VEHICULO) Then
'                        If Me.objValidacion.Valida Then
'                            If Me.dblValorVehiculo = 0 Then
'                                intCont += 1
'                                Me.strError += intCont & ". Debe ingresar el valor del vehículo."
'                                Me.strError += " <Br />"
'                                Datos_Requeridos = False
'                            End If
'                        End If
'                    End If
'                    If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_AÑOS_UTILIDAD) Then
'                        If Me.objValidacion.Valida Then
'                            If Me.intTiempoUtil = 0 Then
'                                intCont += 1
'                                Me.strError += intCont & ". Debe ingresar el número de años de utilidad del vehículo."
'                                Me.strError += " <Br />"
'                                Datos_Requeridos = False
'                            End If
'                        End If
'                    End If
'                End If


'                If Me.objCataEstado.Retorna_Codigo = Vehiculos.ESTADO_INACTIVO Or Me.objCataEstado.Retorna_Codigo = Vehiculos.ESTADO_BLOQUEADO Then
'                    If Me.objCataCausaInactividad.Retorna_Codigo = Vehiculos.SIN_CAUSA_INACTIVIDAD Then
'                        intCont += 1
'                        Me.strError += intCont & ". Vehículo inactivo o bloqueado. Debe ingresar una causa de inactividad para el vehículo."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                Else
'                    If Me.objCataCausaInactividad.Retorna_Codigo <> Vehiculos.SIN_CAUSA_INACTIVIDAD Then
'                        intCont += 1
'                        Me.strError += intCont & ". Vehículo activo. Este vehículo no debe tener causas de inactividad."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'                If Me.strCodigoLinea = TIPO_LINEA_VEHICULO_SIN_LINEA Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar la línea del vehículo."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_INGRESO_REGION) Then
'                    If Me.objValidacion.Valida Then
'                        If Me.IntRegionGeografica = General.CERO Then
'                            intCont += 1
'                            strError += intCont & ". Seleccione la Región Geográfica."
'                            strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If
'                    End If
'                End If


'                If Me.intTieneGPS = 1 Then
'                    If Trim(Me.strNombreEmpresaGPS) = "" Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar el nombre de la Empresa GPS"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                    If Trim(Me.strUsuarioGPS) = "" Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar el nombre del Usuario GPS"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                    If Trim(Me.strClaveGPS) = "" Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar la clave GPS"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                    If Trim(Me.strTelefonoGPS) = "" Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar el número de teléfono de la Empresa GPS"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If

'                End If
'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_SEMIRREMOLQUE_X_TIPO_VEHICULO) Then
'                    If Me.objValidacion.Valida Then
'                        If bolSemiremolqueValido Then
'                            'If RTrim(LTrim(objSemirremolque.Placa)) <> "" Then
'                            '    If objSemirremolque.Consultar Then
'                            '        If objSemirremolque.CataEstado.Retorna_Codigo = Vehiculos.ESTADO_INACTIVO Then
'                            '            intCont += 1
'                            '            Me.strError += intCont & ". El semirremolque esta inactivo."
'                            '            Me.strError += " <Br />"
'                            '            Datos_Requeridos = False
'                            '        ElseIf objSemirremolque.CataEstado.Retorna_Codigo = Vehiculos.ESTADO_BLOQUEADO Then
'                            '            intCont += 1
'                            '            Me.strError += intCont & ". El semirremolque esta bloqueado."
'                            '            Me.strError += " <Br />"
'                            '            Datos_Requeridos = False
'                            '        End If
'                            '    Else
'                            '        intCont += 1
'                            '        Me.strError += intCont & ". El semirremolque no existe."
'                            '        Me.strError += " <Br />"
'                            '        Datos_Requeridos = False
'                            '    End If
'                            'End If
'                        End If
'                    End If
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_FOTO_VEHICULO) Then
'                    If Me.objValidacion.Valida Then
'                        If Me.bolFoto = False Then
'                            intCont += 1
'                            Me.strError += intCont & ". " & Me.objValidacion.MensajeValidacion
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If
'                    End If
'                End If

'            Catch ex As Exception
'                Datos_Requeridos = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Datos_Requeridos: " & Me.objGeneral.Traducir_Error(ex.Message)
'            End Try
'        End Function

'        Public Function Validar_Pesos(ByRef ConsecutivoError As Integer, ByVal PlacaVehiculo As String, ByVal PlacaSemirremolque As String) As Boolean
'            Try
'                Dim dtsConsulta As New DataSet

'                Validar_Pesos = False

'                Dim intPesoMinimo As Integer
'                Dim intPesoMaximo As Integer
'                Dim intCapacidadKg As Integer = Me.intCapacidad * 1000

'                Me.CataTipoVehiculo.Consultar()

'                'Me.objSemirremolque.Placa = PlacaSemirremolque
'                'Me.objSemirremolque.Consultar()
'                'Me.objSemirremolque.CataTipoSemirremolque.Consultar()

'                strSQL = "SELECT Peso_Minimo, Peso_Maximo, Descripcion_Vehiculo, Descripcion_Semirremolque"
'                strSQL += " FROM V_Peso_Carga_Ministerio_Transportes "
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += "   AND Codigo_Vehiculo = '" & Val(Me.CataTipoVehiculo.Campo5) & "'"
'                'strSQL += "   AND Codigo_Semirremolque = '" & Val(Me.objSemirremolque.CataTipoSemirremolque.Campo3) & "'"

'                dtsConsulta = Me.objGeneral.Retorna_Dataset(strSQL, strError)

'                If Not IsNothing(dtsConsulta) Then
'                    If dtsConsulta.Tables(0).Rows.Count > General.CERO Then
'                        For i = 0 To (dtsConsulta.Tables(0).Rows.Count - 1)
'                            intPesoMinimo = Val(dtsConsulta.Tables(0).Rows(i).Item("Peso_Minimo").ToString)
'                            intPesoMaximo = Val(dtsConsulta.Tables(0).Rows(i).Item("Peso_Maximo").ToString)

'                            'intPesoMaximo = intPesoMaximo - (Me.dblPesoBruto * 1000) - (Me.objSemirremolque.PesoBruto * 1000)

'                            If intCapacidadKg > intPesoMinimo And intCapacidadKg < intPesoMaximo Then
'                                Validar_Pesos = True
'                            Else
'                                If intCapacidadKg <= intPesoMinimo Then
'                                    ConsecutivoError += 1
'                                    Me.strError += ConsecutivoError & ". Dado el tipo del vehículo ( " & dtsConsulta.Tables(0).Rows(i).Item("Descripcion_Vehiculo").ToString & ")"
'                                    'If Me.objSemirremolque.Placa <> String.Empty Then
'                                    '    Me.strError += " y dado el tipo del semirremolque ( " & dtsConsulta.Tables(0).Rows(i).Item("Descripcion_Semirremolque").ToString & ")"
'                                    'End If
'                                    Me.strError += ". La capacidad del vehículo debe ser mayor a" & intPesoMinimo & " Kg"
'                                    Me.strError += " <Br />"
'                                End If

'                                If intCapacidadKg >= intPesoMaximo Then
'                                    ConsecutivoError += 1
'                                    Me.strError += ConsecutivoError & ". Dado el tipo del vehículo ( " & dtsConsulta.Tables(0).Rows(i).Item("Descripcion_Vehiculo").ToString & ") y su peso bruto de " & (Me.dblPesoBruto * 1000) & "Kg"
'                                    'If Me.objSemirremolque.Placa <> String.Empty Then
'                                    '    Me.strError += " y dado el tipo del semirremolque ( " & dtsConsulta.Tables(0).Rows(i).Item("Descripcion_Semirremolque").ToString & ") y su peso bruto de " & (Val(Me.objSemirremolque.PesoBruto) * 1000) & "Kg"
'                                    'End If
'                                    Me.strError += ". La capacidad del vehículo debe ser menor a  " & intPesoMaximo & " Kg"
'                                    Me.strError += " <Br />"
'                                End If

'                            End If


'                        Next
'                    End If
'                Else

'                    ConsecutivoError += 1
'                    Me.strError += ConsecutivoError & ". No existe el catálogo Peso_Carga_Ministerio_Transportes"
'                    Me.strError += " <Br />"

'                End If

'            Catch ex As Exception
'                Validar_Pesos = False
'                Me.strError = "En la clase clsManifiestoEncabezado se presentó un error en la función Validar_Pesos: " & Me.objGeneral.Traducir_Error(ex.Message)
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Vehiculo_Lista_Negra() As Boolean
'            Try
'                Dim dtsConsulta As New DataSet

'                Vehiculo_Lista_Negra = True

'                strSQL = "SELECT Codigo"
'                strSQL += " FROM Lista_Negra_Vehiculos "
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += "   AND VEHI_Placa = '" & Me.strPlaca & "'"
'                strSQL += "   AND Estado = " & General.ESTADO_ACTIVO

'                dtsConsulta = Me.objGeneral.Retorna_Dataset(strSQL, strError)

'                If Not IsNothing(dtsConsulta) Then
'                    If dtsConsulta.Tables(0).Rows.Count > General.CERO Then
'                        For i = 0 To (dtsConsulta.Tables(0).Rows.Count - 1)
'                            If Val(dtsConsulta.Tables(0).Rows(i).Item("Codigo").ToString) > General.CERO Then
'                                Vehiculo_Lista_Negra = True
'                            Else
'                                Vehiculo_Lista_Negra = False
'                            End If
'                        Next
'                    Else
'                        Vehiculo_Lista_Negra = False
'                    End If

'                End If

'            Catch ex As Exception
'                Vehiculo_Lista_Negra = False
'                Me.strError = "En la clase Vehiculos se presentó un error en la función Validar_Lista_Negra: " & Me.objGeneral.Traducir_Error(ex.Message)
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Insertar_T_Documento_Vehiculos(ByRef Mensaje As String) As Boolean
'            Dim lonNumeRegi As Long
'            Try
'                Me.objGeneral.ComandoSQL = New SqlCommand("sp_insertar_t_vehiculo_documentos", Me.objGeneral.ConexionDocumentalSQL)
'                Me.objGeneral.ComandoSQL.CommandType = CommandType.StoredProcedure
'                Me.objGeneral.ComandoSQL.Parameters.Clear()

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_USUA_Codigo", SqlDbType.Char, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_USUA_Codigo").Value = Me.objUsuario.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_VEHI_Placa", SqlDbType.VarChar, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_VEHI_Placa").Value = Me.Placa
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_ESVE_Numero", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_ESVE_Numero").Value = Me.EstudioSeguridad.Numero

'                Me.objGeneral.ConexionDocumentalSQL.Open()
'                lonNumeRegi = Val(Me.objGeneral.ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Insertar_T_Documento_Vehiculos = True
'                Else
'                    Insertar_T_Documento_Vehiculos = False
'                End If

'            Catch ex As Exception
'                Insertar_T_Documento_Vehiculos = False
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Insertar_Documentos_SQL: " & Me.objGeneral.Traducir_Error(ex.Message)
'                    Insertar_T_Documento_Vehiculos = False
'                End Try
'            Finally
'                If Me.objGeneral.ConexionDocumentalSQL.State = ConnectionState.Open Then
'                    Me.objGeneral.ConexionDocumentalSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Eliminar_T_Detalle_Documentos(ByVal Usuario As String, ByVal TipoDocumento As Integer, ByVal Placa As String) As Boolean
'            Dim lonNumeRegi As Long
'            Try
'                Me.objGeneral.ComandoSQL = New SqlCommand("sp_eliminar_documento_t_vehiculo_documentos", Me.objGeneral.ConexionDocumentalSQL)
'                Me.objGeneral.ComandoSQL.CommandType = CommandType.StoredProcedure

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_USUA_Codigo", SqlDbType.Char, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_USUA_Codigo").Value = Usuario
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_CATA_TIDV_Codigo", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_CATA_TIDV_Codigo").Value = TipoDocumento
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_VEHI_Placa", SqlDbType.Char, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_VEHI_Placa").Value = Placa

'                Me.objGeneral.ConexionDocumentalSQL.Open()

'                lonNumeRegi = Val(Me.objGeneral.ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Eliminar_T_Detalle_Documentos = True
'                Else
'                    Eliminar_T_Detalle_Documentos = False
'                End If

'            Catch ex As Exception
'                Eliminar_T_Detalle_Documentos = False
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Eliminar_T_Detalle_Documentos: " & Me.objGeneral.Traducir_Error(ex.Message)
'                    Eliminar_T_Detalle_Documentos = False
'                End Try
'            Finally
'                If Me.objGeneral.ConexionDocumentalSQL.State = ConnectionState.Open Then
'                    Me.objGeneral.ConexionDocumentalSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Trasladar_T_Documentos(ByVal A_Temporal As Byte) As Boolean
'            Dim lonNumeRegi As Long
'            Try
'                Me.objGeneral.ComandoSQL = New SqlCommand("sp_trasladar_t_estudio_seguridad_vehiculo_documentos", Me.objGeneral.ConexionDocumentalSQL)
'                Me.objGeneral.ComandoSQL.CommandType = CommandType.StoredProcedure
'                Me.objGeneral.ComandoSQL.Parameters.Clear()
'                Me.objGeneral.ConexionDocumentalSQL.Open()

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_A_Temporal", SqlDbType.Bit) : Me.objGeneral.ComandoSQL.Parameters("@par_A_Temporal").Value = A_Temporal
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_USUA_Codigo", SqlDbType.Char, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_USUA_Codigo").Value = Me.objUsuario.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                'Me.objGeneral.ComandoSQL.Parameters.Add("@par_ESVE_Numero", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_ESVE_Numero").Value = Me.lonNumero

'                lonNumeRegi = Val(Me.objGeneral.ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Trasladar_T_Documentos = True
'                Else
'                    Trasladar_T_Documentos = False
'                End If

'            Catch ex As Exception
'                Trasladar_T_Documentos = False
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Insertar_Documentos_SQL: " & Me.objGeneral.Traducir_Error(ex.Message)
'                    Trasladar_T_Documentos = False
'                End Try
'            Finally
'                If Me.objGeneral.ConexionDocumentalSQL.State = ConnectionState.Open Then
'                    Me.objGeneral.ConexionDocumentalSQL.Close()
'                End If
'            End Try
'        End Function
'#End Region

'    End Class

'End Namespace
