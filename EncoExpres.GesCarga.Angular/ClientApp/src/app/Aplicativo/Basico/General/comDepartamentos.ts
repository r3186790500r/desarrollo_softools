import { Component, OnInit, Input, NgModule } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { debounceTime, timeout } from 'rxjs/operators';
// Services
import { serDepartamentos } from '../../../services/serDepartamentos';
import { serPaises } from '../../../services/serPaises';
// Interfaces
import { Filtro, Departamento, Pais } from '../../../interfaces/ifaGeneral';

@Component({
  selector: 'app-departamentos',
  templateUrl: './comDepartamentos.html'
})
export class comDepartamentos implements OnInit {
  form: FormGroup;
  // Variables 
  public lstPaises: Pais[]; lstFiltPais: Pais[];
  public strJson: string; bolProcesoExitoso: boolean; objFiltro: Filtro;
  public objDepartamento: Departamento; strMensaje: string; objPaises;
  public options: string[];

  constructor(public http: HttpClient, protected serDepartamentos: serDepartamentos, protected serPaises: serPaises) {
    this.funConsultarPaises();
    this.FormControles();
  }

  ngOnInit() {}

  public funGuardar() {
    try {

      this.objDepartamento = {
        Codigo: 0,
        Pais: { Codigo: 242, Nombre: this.form.controls.txtPais.value },
        CodigoEmpresa: 6,
        CodigoAlterno: this.form.controls.txtCodigo.value,
        Nombre: this.form.controls.txtNombre.value,
        Estado: 1,
        UsuarioCrea: { Codigo: 1, Nombre: "" },
        UsuarioModifica: 0
      };

      this.serDepartamentos.apiGuardar(this.objDepartamento).subscribe(resp =>
      {
        if (resp.ProcesoExitoso === true) {
          this.form.controls.txtCodigo.setValue("");
          this.form.controls.txtNombre.setValue("");
          this.strMensaje = "Insertó el código " + resp.Datos;
        }
        else {
          this.strMensaje = "Se presentó el error " + resp.MensajeOperacion;
        }
      }
      );

    }
    catch (error) {
      console.log(error);
    }
  }

  public funConsultarPaises() {
    try {
      this.objFiltro = {
        CodigoEmpresa: 6,
        Codigo: 0,
        CodigoAlterno: "",
        Nombre: "",
        Estado: 1,
        Pagina: 0,
        RegistrosPagina: 0
      };

      this.serPaises.apiConsultar(this.objFiltro).subscribe(resp => {
        if (resp.ProcesoExitoso === true) {
          // El objeto objPost.Datos se convierte en un string JSON
          this.strJson = JSON.stringify(resp.Datos);
          // El string strJson se convierte en arreglo
          this.lstPaises = JSON.parse(this.strJson);
        }
        else {
          console.log(resp.MensajeOperacion);
        }
      }
      );

    }
    catch (error) {
      console.log(error);
    }
  }

  public funConsultarPaises1() {
    const UrlApi = "http://localhost:50395/api/v1/Paises/Consultar";

    this.http.post(UrlApi,
      {
        CodigoEmpresa: 6,
        Codigo: 0,
        CodigoAlterno: "",
        Nombre: "",
        Estado: 1,
        Pagina: 0,
        RegistrosPagina: 0
      }).subscribe(apiData => {
        console.log(apiData);
      }, error => { console.log(error); }
      )
  }

  private FormControles() {
    this.form = new FormGroup({
      txtCodigo: new FormControl('', [Validators.required, Validators.maxLength(6)]),
      txtNombre: new FormControl('', [Validators.required, Validators.maxLength(50), Validators.pattern(/^[a-zA-Z ]+$/)]),
      txtPais: new FormControl('', [Validators.required, Validators.maxLength(50)]),
    });
    
    this.form.controls.txtPais.valueChanges.pipe(
      debounceTime(300)
    ).subscribe(value => {
      this.lstFiltPais = this.lstPaises.filter(option => option.Nombre.toLowerCase().indexOf(value) === 0);
      console.log(this.lstFiltPais);
    });

  }
  
}
