﻿EncoExpresApp.controller("ConsultarTarifarioComprasCtrl", ['$scope', '$timeout', 'TarifarioComprasFactory', '$linq', 'blockUI', '$routeParams', function ($scope, $timeout, TarifarioComprasFactory, $linq, blockUI, $routeParams) {

    $scope.MapaSitio = [{ Nombre: 'Comercial' }, { Nombre: 'Documentos' }, { Nombre: 'Tarifario Compras' }];


    // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

    var filtros = {};
    $scope.cantidadRegistrosPorPaginas = 10;
    $scope.paginaActual = 1;
    $scope.ModalErrorCompleto = ''
    $scope.ModalError = ''
    $scope.MostrarMensajeError = false
    $scope.ListadoEstados = [
        { Nombre: '(TODAS)', Codigo: -1 },
        { Nombre: 'ACTIVA', Codigo: 1 },
        { Nombre: 'INACTIVA', Codigo: 0 }
    ];
    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
    $scope.Modelo = {
        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
    }
    $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
    try {
    $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_TARIFARIO_COMPRAS);
    } catch (e) {
        ShowError('No tiene permiso para visualizar esta pagina')
        document.location.href = '#'
    }


    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

    /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*-------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR ---------------------------------------------------------------------------- */
    $scope.PrimerPagina = function () {
        $scope.paginaActual = 1;
        Find();
    };

    $scope.Siguiente = function () {
        if ($scope.paginaActual < $scope.totalPaginas) {
            $scope.paginaActual = $scope.paginaActual + 1;
            Find();
        }
    };

    $scope.Anterior = function () {
        if ($scope.paginaActual > 1) {
            $scope.paginaActual = $scope.paginaActual - 1;
            Find();
        }
    };

    $scope.UltimaPagina = function () {
        $scope.paginaActual = $scope.totalPaginas;
        Find();
    };


    //Funcion Nuevo Documento
    $scope.NuevoDocumento = function () {
        if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
            document.location.href = '#!GestionarTarifarioCompras';
        }
    };
    $scope.CopiarDocumento = function (Codigo) {
        if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
            document.location.href = '#!GestionarTarifarioCompras/' + Codigo.toString() + ',1';
        }
    };
    //Exportar tarifarios a EXCEL 

    $scope.ExportarTarifarioExcel = function (codigo) {
        $scope.urlASP = '';
        $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
        $scope.codigo = codigo;
        $scope.NombreReporte = NOMBRE_LISTADO_EXPORTAR_TARIFARIO_COMPRA;


        //Arma el filtro
        $scope.FiltroArmado = '&Numero=' + $scope.codigo;


        //Llama a la pagina ASP con el filtro por GET


        window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + 1 + '&Prefijo=' + $scope.pref);


    }
    //-------------------------------------------------FUNCIONES--------------------------------------------------------
    $scope.Buscar = function () {
        if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
            $scope.Codigo = 0
            Find()
        }
    };
    if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
        if ($routeParams.Codigo > 0) {
            $scope.Modelo.Numero = $routeParams.Codigo;
            Find();
            $scope.Modelo.Numero = '';
        }

    }
    function Find() {
        blockUI.start('Buscando registros ...');

        $timeout(function () {
            blockUI.message('Espere por favor ...');
        }, 100);
        $scope.Buscando = true;
        $scope.MensajesError = [];
        $scope.ListadoTarifarioCompras = [];
        if ($scope.Buscando) {
            if ($scope.MensajesError.length == 0) {

                blockUI.delay = 1000;
                $scope.Modelo.Pagina = $scope.paginaActual
                $scope.Modelo.RegistrosPagina = 10
                $scope.Modelo.Sync = true;
                var Response = TarifarioComprasFactory.Consultar($scope.Modelo);
                   
                if (Response.ProcesoExitoso === true) {
                    $scope.Modelo.Codigo = 0
                    if (Response.Datos.length > 0) {
                        $scope.ListadoTarifarioCompras = Response.Datos
                        for (var i = 0; i < $scope.ListadoTarifarioCompras.length; i++) {
                            if ($scope.ListadoTarifarioCompras[i].Codigo == 0) {
                                $scope.ListadoTarifarioCompras.splice(i, 1)
                                break;
                            }
                        }
                        OrderBy('Codigo', undefined, $scope.ListadoTarifarioCompras);
                        $scope.totalRegistros = Response.Datos[0].TotalRegistros;
                        $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                        $scope.Buscando = false;
                        $scope.ResultadoSinRegistros = '';
                    }
                    else {
                        $scope.totalRegistros = 0;
                        $scope.totalPaginas = 0;
                        $scope.paginaActual = 1;
                        $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                        $scope.Buscando = false;
                    }
                    var listado = [];
                    Response.Datos.forEach(function (item) {
                        listado.push(item);
                    });
                }
                   
            }
        }
        blockUI.stop();
    }
    /*------------------------------------------------------------------------------------Eliminar Linea Vehiculos-----------------------------------------------------------------------*/



    $scope.AnularDocumento = function (Codigo, Nombre) {
        $scope.Codigo = Codigo
        $scope.ModalErrorCompleto = ''
        $scope.Nombre = Nombre
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        showModal('modalAnular');
    };


    $scope.Anular = function () {

        var entidad = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Codigo: $scope.Codigo
        };

        TarifarioComprasFactory.Anular(entidad).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.Codigo = '';
                    ShowSuccess('Se Anulo el Tarifario  ' + $scope.Nombre);
                    closeModal('modalAnular');
                    Find();
                }
                else {
                    ShowError(response.data.MensajeOperacion);
                }
            }, function (response) {
                var result = ''
                showModal('modalMensajeAnularImpuesto');
                result = InternalServerError(response.statusText)
                $scope.ModalError = 'No se puede anular el Tarifario ' + $scope.Nombre + ' ya que se encuentra relacionada con ' + result;
                $scope.ModalErrorCompleto = response.statusText

            });


    };


    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    $scope.MostrarMensajeErrorModal = function () {
        $scope.MostrarMensajeErrorCompleto = true
    }
    $scope.CerrarModal = function () {
        closeModal('modalEliminarTarifarioCompras');
        closeModal('modalMensajeEliminarTarifarioCompras');
    }

    $scope.MaskNumero = function () {
        $scope.CodigoAlterno = MascaraNumero($scope.CodigoAlterno)
    };

}]);