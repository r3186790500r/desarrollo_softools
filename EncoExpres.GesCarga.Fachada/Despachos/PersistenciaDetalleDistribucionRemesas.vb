﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Despachos.Remesa
Imports EncoExpres.GesCarga.Repositorio.Despachos.Remesa

Namespace Despachos.Remesa
    Public NotInheritable Class PersistenciaDetalleDistribucionRemesas
        Implements IPersistenciaBase(Of DetalleDistribucionRemesas)

        Public Function Consultar(filtro As DetalleDistribucionRemesas) As IEnumerable(Of DetalleDistribucionRemesas) Implements IPersistenciaBase(Of DetalleDistribucionRemesas).Consultar
            Return New RepositorioDetalleDistribucionRemesas().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As DetalleDistribucionRemesas) As Long Implements IPersistenciaBase(Of DetalleDistribucionRemesas).Insertar
            Return New RepositorioDetalleDistribucionRemesas().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As DetalleDistribucionRemesas) As Long Implements IPersistenciaBase(Of DetalleDistribucionRemesas).Modificar
            Return New RepositorioDetalleDistribucionRemesas().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As DetalleDistribucionRemesas) As DetalleDistribucionRemesas Implements IPersistenciaBase(Of DetalleDistribucionRemesas).Obtener
            Return New RepositorioDetalleDistribucionRemesas().Obtener(filtro)
        End Function
        Public Function Anular(entidad As DetalleDistribucionRemesas) As Boolean
            Return New RepositorioDetalleDistribucionRemesas().Anular(entidad)
        End Function

    End Class

End Namespace

