﻿Imports System.Transactions
Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Despachos
Imports EncoExpres.GesCarga.Entidades.Despachos.Planilla
Imports EncoExpres.GesCarga.Entidades.Despachos.Remesa
Imports EncoExpres.GesCarga.Entidades.Despachos.Procesos
Imports EncoExpres.GesCarga.Repositorio.Despachos.Procesos
Imports EncoExpres.GesCarga.Entidades.Facturacion
Imports EncoExpres.GesCarga.Repositorio.Facturacion
Imports EncoExpres.GesCarga.Repositorio.Basico.General
Imports EncoExpres.GesCarga.Repositorio.Paqueteria
Imports EncoExpres.GesCarga.Repositorio.Basico.Tesoreria
Imports EncoExpres.GesCarga.Entidades.Basico.Tesoreria

Namespace Despachos

    ''' <summary>
    ''' Clase <see cref="RepositorioRemesas"/>
    ''' </summary>
    Public NotInheritable Class RepositorioRemesas
        Inherits RepositorioBase(Of Remesas)
        Dim ObjReporte As New ReporteMinisterio
        Dim ProcesoRNDC As New RepositorioReporteMinisterio
        Dim AuxRemesaRNDC As New Remesas
        Public Overrides Function Consultar(filtro As Remesas) As IEnumerable(Of Remesas)
            Dim lista As New List(Of Remesas)
            Dim item As Remesas
            Dim resultado As IDataReader

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.NumeroDocumento > Cero Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.NumeroDocumento)
                End If

                If filtro.FechaInicial > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                End If

                If filtro.FechaFinal > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                End If

                'If filtro.NumeroOrdenServicio > 0 Then
                'conexion.AgregarParametroSQL("@par_ESOS_Numero", filtro.NumeroOrdenServicio)
                'End If
                If filtro.NumeroDocumentoOrdenServicio > 0 Then
                    conexion.AgregarParametroSQL("@par_ESOS_Numero_Documento", filtro.NumeroDocumentoOrdenServicio)
                End If
                If Not IsNothing(filtro.Manifiesto) Then
                    If filtro.Manifiesto.NumeroDocumento > 0 Then
                        conexion.AgregarParametroSQL("@par_ENMC_Numero", filtro.Manifiesto.NumeroDocumento)
                    End If
                End If

                If Not String.IsNullOrWhiteSpace(filtro.FiltroCliente) Then
                    If Len(Trim(filtro.FiltroCliente)) > Cero Then
                        conexion.AgregarParametroSQL("@par_Cliente", filtro.FiltroCliente)
                    End If
                End If

                If Not IsNothing(filtro.Cliente) Then
                    If filtro.Cliente.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_CLIE_Codigo", filtro.Cliente.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.NumeroContenedor) And filtro.NumeroContenedor <> String.Empty Then
                    conexion.AgregarParametroSQL("@par_Numero_Contenedor", filtro.NumeroContenedor)
                End If
                If Not IsNothing(filtro.NumeroDocumentoCliente) And filtro.NumeroDocumentoCliente <> String.Empty Then
                    conexion.AgregarParametroSQL("@par_Documento_Cliente", filtro.NumeroDocumentoCliente)
                End If

                If Not IsNothing(filtro.Vehiculo) Then
                    If Not IsNothing(filtro.Vehiculo.Placa) And filtro.Vehiculo.Placa <> String.Empty Then
                        conexion.AgregarParametroSQL("@par_Placa", filtro.Vehiculo.Placa)
                    End If
                End If
                If Not IsNothing(filtro.FiltroConductor) And filtro.FiltroConductor <> String.Empty Then
                    conexion.AgregarParametroSQL("@par_Conductor", filtro.FiltroConductor)
                End If

                If Not IsNothing(filtro.FiltroTenedor) And filtro.FiltroTenedor <> String.Empty Then
                    conexion.AgregarParametroSQL("@par_Tenedor", filtro.FiltroTenedor)
                End If
                If Not IsNothing(filtro.TipoDocumento) Then
                    If filtro.TipoDocumento.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.TipoRemesa) Then
                    If filtro.TipoRemesa.Codigo <> Cero Then
                        conexion.AgregarParametroSQL("@par_CATA_TIRE_Codigo", filtro.TipoRemesa.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Oficina) Then
                    If filtro.Oficina.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficina.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.CiudadDestinatario) Then
                    If filtro.CiudadDestinatario.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo_Destino", filtro.CiudadDestinatario.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.CiudadRemitente) Then
                    If filtro.CiudadRemitente.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo_origen", filtro.CiudadRemitente.Codigo)
                    End If
                End If

                If filtro.PendienteFacturadas Then
                    conexion.AgregarParametroSQL("@par_PendienteFacturadas", NO_APLICA)
                End If

                If filtro.Estado >= 0 And filtro.Estado <> 2 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                    conexion.AgregarParametroSQL("@par_Anulado", 0)

                ElseIf filtro.Estado = 2 Then
                    conexion.AgregarParametroSQL("@par_Anulado", 1)
                End If


                If filtro.Cumplido > 0 Then
                    conexion.AgregarParametroSQL("@par_Cumplido", filtro.Cumplido)
                    '    conexion.AgregarParametroSQL("@par_Planillado", Cero)
                End If

                If filtro.NumeroDocumentoPlanillaDespacho > 0 Then
                    conexion.AgregarParametroSQL("@par_ENPD_Numero_Documento", filtro.NumeroDocumentoPlanillaDespacho)
                End If
                If filtro.NumeroPlanilla = -1 Then
                    conexion.AgregarParametroSQL("@par_ENPD_Numero", 0)
                ElseIf filtro.NumeroPlanilla > 0 Then
                    conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.NumeroPlanilla)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > Cero Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > Cero Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                If filtro.CodigoSede > 0 Then
                    conexion.AgregarParametroSQL("@par_TEDI_Codigo", filtro.CodigoSede)
                End If

                If Not IsNothing(filtro.UsuarioConsulta) Then
                    If filtro.UsuarioConsulta.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_Usuario_Consulta", filtro.UsuarioConsulta.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.TarifaTransporte) Then
                    If filtro.TarifaTransporte > 0 Then
                        conexion.AgregarParametroSQL("@par_Tarifa_Transporte", filtro.TarifaTransporte)
                    End If
                End If

                If filtro.FacturarA > 0 Then
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_FacturarA", filtro.FacturarA)
                End If

                If Not IsNothing(filtro.TarifaTransporte) Then
                    If filtro.TarifaTransporte > 0 Then
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_control_contenedores_remesas")

                    Else
                        If Not IsNothing(filtro.Cumplida) Then
                            If filtro.Cumplida > Cero Then
                                resultado = conexion.ExecuteReaderStoreProcedure("gsp_Cumplir_consultar_remesas")
                            Else
                                resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_remesas")
                            End If
                        Else

                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_remesas")
                        End If
                    End If
                    While resultado.Read
                        item = New Remesas(resultado)
                        lista.Add(item)
                    End While

                End If



            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As Remesas) As Long

            Dim inserto As Boolean = True
            Dim insertoRecorrido As Boolean = True
            Dim insertoConcepto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.CleanParameters()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento.Codigo)
                    conexion.AgregarParametroSQL("@par_CATA_TIRE_Codigo", entidad.TipoRemesa.Codigo)
                    If Not IsNothing(entidad.NumeroDocumentoCliente) Then
                        conexion.AgregarParametroSQL("@par_Documento_Cliente", entidad.NumeroDocumentoCliente)
                    Else
                        conexion.AgregarParametroSQL("@par_Documento_Cliente", VACIO)
                    End If
                    conexion.AgregarParametroSQL("@par_Fecha_Documento_Cliente", entidad.FechaDocumentoCliente, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_RUTA_Codigo", entidad.Ruta.Codigo)
                    conexion.AgregarParametroSQL("@par_PRTR_Codigo", entidad.ProductoTransportado.Codigo)
                    conexion.AgregarParametroSQL("@par_CATA_FOPR_Codigo", entidad.FormaPago.Codigo)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", entidad.Cliente.Codigo)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Remitente", entidad.Remitente.Codigo)
                    conexion.AgregarParametroSQL("@par_CIUD_Codigo_Remitente", entidad.CiudadRemitente.Codigo)
                    If Not IsNothing(entidad.DireccionRemitente) Then
                        conexion.AgregarParametroSQL("@par_Direccion_Remitente", entidad.DireccionRemitente)
                    Else
                        conexion.AgregarParametroSQL("@par_Direccion_Remitente", VACIO)
                    End If
                    If Not IsNothing(entidad.TelefonoRemitente) Then
                        conexion.AgregarParametroSQL("@par_Telefonos_Remitente", entidad.TelefonoRemitente)
                    Else
                        conexion.AgregarParametroSQL("@par_Telefonos_Remitente", VACIO)
                    End If
                    If Not IsNothing(entidad.Observaciones) Then
                        conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    Else
                        conexion.AgregarParametroSQL("@par_Observaciones", VACIO)
                    End If
                    conexion.AgregarParametroSQL("@par_Cantidad_Cliente", entidad.CantidadCliente)
                    conexion.AgregarParametroSQL("@par_Peso_Cliente", entidad.PesoCliente, SqlDbType.Decimal)
                    conexion.AgregarParametroSQL("@par_Peso_Volumetrico_Cliente", entidad.PesoVolumetricoCliente, SqlDbType.Decimal)
                    conexion.AgregarParametroSQL("@par_DTCV_Codigo", entidad.DetalleTarifaVenta.Codigo)
                    conexion.AgregarParametroSQL("@par_Valor_Flete_Cliente", entidad.ValorFleteCliente, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Manejo_Cliente", entidad.ValorManejoCliente, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Seguro_Cliente", entidad.ValorSeguroCliente, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Descuento_Cliente", entidad.ValorDescuentoCliente, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Total_Flete_Cliente", entidad.TotalFleteCliente, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Comercial_Cliente", entidad.ValorComercialCliente, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Cantidad_Transportador", entidad.CantidadTransportador)
                    conexion.AgregarParametroSQL("@par_Peso_Transportador", entidad.PesoTransportador, SqlDbType.Decimal)
                    conexion.AgregarParametroSQL("@par_Valor_Flete_Transportador", entidad.ValorFleteTransportador, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Total_Flete_Transportador", entidad.TotalFleteTransportador, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Destinatario", entidad.Destinatario.Codigo)
                    conexion.AgregarParametroSQL("@par_CIUD_Codigo_Destinatario", entidad.CiudadDestinatario.Codigo)
                    If Not IsNothing(entidad.DireccionDestinatario) Then
                        conexion.AgregarParametroSQL("@par_Direccion_Destinatario", entidad.DireccionDestinatario)
                    Else
                        conexion.AgregarParametroSQL("@par_Direccion_Destinatario", VACIO)
                    End If
                    If Not IsNothing(entidad.TelefonoDestinatario) Then
                        conexion.AgregarParametroSQL("@par_Telefonos_Destinatario", entidad.TelefonoDestinatario)
                    Else
                        conexion.AgregarParametroSQL("@par_Telefonos_Destinatario", VACIO)
                    End If
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
                    If Not IsNothing(entidad.Numeracion) Then
                        conexion.AgregarParametroSQL("@par_Numeracion", entidad.Numeracion)
                    Else
                        conexion.AgregarParametroSQL("@par_Numeracion", VACIO)
                    End If
                    conexion.AgregarParametroSQL("@par_ETCC_Numero", entidad.TarifarioCompra.Numero)
                    conexion.AgregarParametroSQL("@par_ETCV_Numero", entidad.TarifarioVenta.Codigo)
                    conexion.AgregarParametroSQL("@par_ESOS_Numero", entidad.NumeroOrdenServicio)
                    conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", entidad.Conductor.Codigo)
                    conexion.AgregarParametroSQL("@par_ENOC_Numero", entidad.OrdenCargue.Numero)
                    conexion.AgregarParametroSQL("@par_SEMI_Codigo", entidad.Semirremolque.Codigo)
                    conexion.AgregarParametroSQL("@par_Numero_Contenedor", entidad.NumeroContenedor)
                    conexion.AgregarParametroSQL("@par_MBL_Contenedor", entidad.HBLContenedor)
                    conexion.AgregarParametroSQL("@par_HBL_Contenedor", entidad.MBLContenedor)
                    conexion.AgregarParametroSQL("@par_DO_Contenedor", entidad.DOContenedor)
                    conexion.AgregarParametroSQL("@par_Valor_FOB", entidad.ValorFOB)
                    conexion.AgregarParametroSQL("@par_ID_Solicitud", entidad.IDDetalleSolicitud)
                    conexion.AgregarParametroSQL("@par_Remesa_Cortesia", entidad.RemesaCortesia)

                    If Not IsNothing(entidad.SICDDevolucionContenedor) Then
                        If entidad.SICDDevolucionContenedor.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_SICD_Devolucion_Contenedor", entidad.SICDDevolucionContenedor.Codigo)
                        End If

                    End If
                    If Not IsNothing(entidad.FechaDevolucionContenedor) Then
                        If entidad.FechaDevolucionContenedor > Date.MinValue Then
                            conexion.AgregarParametroSQL("@par_Fecha_Devolucion_Contenedor", entidad.FechaDevolucionContenedor, SqlDbType.Date)
                        End If

                    End If
                    If Not IsNothing(entidad.CiudadDevolucion) Then
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo_Devolucion_Contenedor", entidad.CiudadDevolucion.Codigo)
                    End If
                    conexion.AgregarParametroSQL("@par_Patio_Devolcuion", entidad.PatioDevolucion)
                    If entidad.FechaDevolucion > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Devolcuion", entidad.FechaDevolucion, SqlDbType.Date)
                    End If
                    If Not IsNothing(entidad.PermisoInvias) Then
                        conexion.AgregarParametroSQL("@par_Permiso_Invias", entidad.PermisoInvias)
                    End If

                    conexion.AgregarParametroSQL("@par_Valor_Declarado", entidad.ValorDeclarado, SqlDbType.Money)

                    If entidad.FacturarA > 0 Then
                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Facturar_A", entidad.FacturarA)
                    End If

                    If Not IsNothing(entidad.ValorCargue) Then
                        conexion.AgregarParametroSQL("@par_Valor_Cargue", entidad.ValorCargue)
                    End If

                    If Not IsNothing(entidad.ValorDescargue) Then
                        conexion.AgregarParametroSQL("@par_Valor_Descargue", entidad.ValorDescargue)
                    End If

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_remesas")
                    While resultado.Read
                        entidad.NumeroDocumento = resultado.Item("Numero_Documento").ToString()
                        entidad.Numero = resultado.Item("Numero").ToString()
                    End While
                    resultado.Close()

                    If entidad.Numero <= 0 Then
                        Return entidad.Numero
                    Else
                        If entidad.DesdeDespacharSolicitud Then
                            If Not ActualizarDespachoSolicitudServicio(entidad, conexion) Then
                                Return -1
                            End If
                        End If
                        ''Proceso reportar remsa RNDC 
                        If entidad.NumeroDocumento.Equals(Cero) Then
                            Return Cero
                        End If
                        If Not IsNothing(entidad.ListaPrecintos) Then
                            If entidad.ListaPrecintos.Count > 0 Then
                                For Each precinto In entidad.ListaPrecintos
                                    inserto = ModificarPrecitos(precinto, entidad.Numero, entidad.CodigoEmpresa, conexion)
                                Next
                            End If
                        End If
                        If inserto Then
                            transaccion.Complete()
                        Else
                            entidad.NumeroDocumento = Cero
                        End If
                    End If
                End Using
            End Using

            If entidad.Estado = 1 Then
                Dim CajRepo = New RepositorioCajas()
                Dim CajaAforador
                Dim cajaCodigo = Nothing

                Using conexionUsa = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                        conexionUsa.CreateConnection()
                        conexionUsa.CleanParameters()

                        conexionUsa.AgregarParametroSQL("@par_TERC_Codigo", entidad.Conductor.Codigo)
                        Dim codigo
                        Dim resultado As IDataReader = conexionUsa.ExecuteReaderStoreProcedure("gsp_consultar_codigo_USA")

                        While resultado.Read
                            codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
                        End While

                        resultado.Close()
                        Dim USA As New Entidades.SeguridadUsuarios.Usuarios With {
                                    .Codigo = codigo
                                }

                        CajaAforador = New Cajas With {
                                        .CodigoEmpresa = entidad.CodigoEmpresa,
                                        .UsuarioCrea = USA,
                                        .Pagina = 1,
                                        .RegistrosPagina = 1,
                                        .Estado = 1,
                                        .Oficina = entidad.Oficina
                                    }
                        conexionUsa.CloseConnection()
                        conexionUsa.Dispose()
                    End Using
                    CajRepo = New RepositorioCajas()
                Dim ResultCajRepo = CajRepo.Consultar(CajaAforador)

                If ResultCajRepo.Count > 0 Then
                    If ResultCajRepo.First().Codigo > 0 Then
                        cajaCodigo = ResultCajRepo.First().Codigo
                    End If
                End If

                Dim Factura As Facturas = New Facturas()
                Factura.FormaPago = entidad.FormaPago
                Factura.CodigoEmpresa = entidad.CodigoEmpresa
                Factura.Moneda = New Monedas With {.Codigo = 1}
                Factura.UsuarioCrea = entidad.UsuarioCrea
                Factura.Estado = entidad.Estado
                Factura.NumeroPreImpreso = 0
                Factura.ControlImpresion = 1
                Factura.ResolucionFacturacion = ""
                Factura.CataTipoFactura = 5201
                Factura.TipoDocumento = 170

                If Not IsNothing(cajaCodigo) Then
                    Factura.Caja = cajaCodigo
                End If

                Factura.Fecha = Date.Now
                If Not IsNothing(entidad.FormaPago) Then
                    Select Case entidad.FormaPago.Codigo
                        Case 4902 'Contado
                            If Not IsNothing(entidad.Cliente) Then
                                Factura.Cliente = entidad.Cliente
                                Factura.FacturarA = entidad.Cliente
                            Else
                                Factura.Cliente = entidad.Remitente
                                Factura.FacturarA = entidad.Remitente
                            End If
                        Case 4903 'Contra Entregas
                            Factura.Cliente = entidad.Destinatario
                            Factura.FacturarA = entidad.Destinatario
                    End Select
                    If Not IsNothing(Factura.Cliente) Then
                        Factura.OficinaFactura = entidad.Oficina
                        Factura.OficinaFactura.CodigoEmpresa = entidad.CodigoEmpresa
                        Dim OfiRepo = New RepositorioOficinas()
                        Dim ResultOfiRepo = OfiRepo.Obtener(Factura.OficinaFactura)
                        If Not IsNothing(ResultOfiRepo) AndAlso ResultOfiRepo.OficinaFactura.Codigo > 0 Then
                            Factura.OficinaFactura.Codigo = ResultOfiRepo.OficinaFactura.Codigo
                        End If

                        Dim RemeFactura As List(Of DetalleFacturas) = New List(Of DetalleFacturas)
                        RemeFactura.Add(New DetalleFacturas With {.Remesas = entidad})

                        Factura.ValorRemesas = entidad.TotalFleteCliente
                        Factura.ValorFactura = entidad.TotalFleteCliente
                        Factura.Subtotal = entidad.TotalFleteCliente
                        Factura.Remesas = RemeFactura
                        Dim FactRepo = New RepositorioFacturas()
                        Dim ResultFactRepo = FactRepo.InsertarFacturaRemesasPaqueteria(Factura)

                        If ResultFactRepo <= 0 Then
                            If ResultFactRepo = -1 Then
                                entidad.MensajeSQL = "La oficina no cuenta con consecutivo de facturación asignado"
                            Else
                                entidad.MensajeSQL = "No se pudo Generar la factura"
                            End If
                        End If
                    End If
                End If

            End If

            Return entidad.NumeroDocumento

        End Function

        Public Overrides Function Modificar(entidad As Remesas) As Long
            Dim inserto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                    conexion.AgregarParametroSQL("@par_FechaDevolucionReal", entidad.FechaDevolucion, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)


                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_fecha_observaciones_remesa")
                    While resultado.Read

                        entidad.Numero = resultado.Item("Numero").ToString
                    End While
                    resultado.Close()

                    If inserto Then
                        transaccion.Complete()
                    Else
                        entidad.Numero = Cero
                    End If

                End Using
            End Using
            Return entidad.Numero
        End Function

        Public Overrides Function Obtener(filtro As Remesas) As Remesas
            Dim item As New Remesas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_remesa")

                While resultado.Read
                    item = New Remesas(resultado)
                End While
            End Using

            item.ListaPrecintos = ObtenerPrecintos(filtro)

            Return item
        End Function

        Public Function ConsultarRemesasPendientesFacturar(filtro As Remesas) As IEnumerable(Of Remesas)
            Dim lista As New List(Of Remesas)
            Dim item As Remesas
            Dim resultado As IDataReader

            Dim RepoRemesaPaqueteria = New RepositorioRemesaPaqueteria()

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.TipoDocumento) Then
                    If filtro.TipoDocumento.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento.Codigo)
                    End If
                End If

                If filtro.NumeroDocumento > Cero Then
                    conexion.AgregarParametroSQL("@par_Numero_Documento", filtro.NumeroDocumento)
                End If

                If filtro.FechaInicial > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                End If

                If filtro.FechaFinal > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                End If

                If Not IsNothing(filtro.Cliente) Then
                    If filtro.Cliente.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_CLIE_Codigo", filtro.Cliente.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.NumeroDocumentoCliente) And filtro.NumeroDocumentoCliente <> String.Empty Then
                    conexion.AgregarParametroSQL("@par_Documento_Cliente", filtro.NumeroDocumentoCliente)
                End If

                If filtro.NumeroDocumentoPlanillaDespacho > 0 Then
                    conexion.AgregarParametroSQL("@par_ENPD_Numero_Documento", filtro.NumeroDocumentoPlanillaDespacho)
                End If

                If filtro.NumeroDocumentoOrdenServicio > 0 Then
                    conexion.AgregarParametroSQL("@par_ESOS_Numero_Documento", filtro.NumeroDocumentoOrdenServicio)
                End If

                If filtro.CodigoSede > 0 Then
                    conexion.AgregarParametroSQL("@par_TEDI_Codigo", filtro.CodigoSede)
                End If

                If Not IsNothing(filtro.CiudadRemitente) Then
                    If filtro.CiudadRemitente.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_CIUD_Origen", filtro.CiudadRemitente.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.CiudadDestinatario) Then
                    If filtro.CiudadDestinatario.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_CIUD_Destino", filtro.CiudadDestinatario.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.ProductoTransportado) Then
                    If filtro.ProductoTransportado.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_PRTR_Codigo", filtro.ProductoTransportado.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > Cero Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > Cero Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If
                If Not IsNothing(filtro.FacturarA) Then
                    If filtro.FacturarA > Cero Then
                        conexion.AgregarParametroSQL("@par_Facturar_A", filtro.FacturarA)
                    End If
                End If

                If Not IsNothing(filtro.Oficina) Then
                    If filtro.Oficina.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficina.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Estado) Then
                    If filtro.Estado > 0 And filtro.Estado > 6000 Then
                        conexion.AgregarParametroSQL("@par_CATA_ESRP_Codigo", filtro.Estado)
                    End If
                End If

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_remesas_pendientes_facturar")

                While resultado.Read
                    item = New Remesas(resultado)
                    If Not IsNothing(filtro.TipoDocumento) Then
                        If filtro.TipoDocumento.Codigo = 110 Then 'remesa paqueteria
                            item.GestionDocumentosRemesa = RepoRemesaPaqueteria.ConsultarDetalleGestionDocumentos(filtro.CodigoEmpresa, item.Numero)
                        End If
                    End If
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Function ActualizarRemesasExcel(filtro As Remesas) As Long
            Dim inserto As Boolean = True
            Dim resultado As IDataReader
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()

                    For Each itemremesa In filtro.Remesas
                        conexion.CleanParameters()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento.Codigo)
                        conexion.AgregarParametroSQL("@par_Numero_Documento_Remesa", itemremesa.NumeroDocumento)
                        conexion.AgregarParametroSQL("@par_Numero_Identificacion_Facturar_A", itemremesa.CodigoClientePagador)
                        conexion.AgregarParametroSQL("@par_DT", itemremesa.DT)
                        conexion.AgregarParametroSQL("@par_Entrega", itemremesa.Entrega)
                        conexion.AgregarParametroSQL("@par_Codigo_Ministerio_Producto", itemremesa.NumeroConfirmacionMinisterio)
                        conexion.AgregarParametroSQL("@par_Total_Flete_Cliente", itemremesa.TotalFleteCliente, SqlDbType.Money)

                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_modificar_remesa_archivo_excel")

                        While resultado.Read
                            inserto = IIf(resultado.Item("Numero") > 0, True, False)
                            filtro.Numero += resultado.Item("Numero")
                            'If inserto = False Then
                            '    transaccion.Dispose()
                            '    Exit For
                            'End If
                        End While
                        resultado.Close()

                    Next
                    'If inserto = True Then
                    transaccion.Complete()
                    'End If
                End Using
            End Using
            Return filtro.Numero
        End Function

        Public Function ConsultarNumerosRemesas(filtro As Remesas) As IEnumerable(Of Remesas)
            Dim lista As New List(Of Remesas)
            Dim item As Remesas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If Not IsNothing(filtro.Conductor) Then
                    If filtro.Conductor.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo_Conductor", filtro.Conductor.Codigo)
                    End If
                End If
                ''0 consulta remesas masivas 1 consulta remesas paqueteria
                'conexion.AgregarParametroSQL("@par_Tipo_Consulta", filtro.TipoConsulta)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_numeros_remesas")

                While resultado.Read
                    item = New Remesas(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Function ActualizarDistribuido(entidad As Remesas) As Remesas
            Dim item As New Remesas
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_actualizar_remesa_distribuida")

                While resultado.Read
                    item.Anulado = Read(resultado, "Distribuido")
                End While

            End Using
            Return item
        End Function

        Public Function MarcarRemesasFacturadas(entidad As Remesas) As Remesas
            Dim item As New Remesas
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero_Remesas", entidad.NumerosRemesas)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_marcar_remesas_facturadas")

                While resultado.Read
                    item.NumerosRemesas = Read(resultado, "RemesasMarcadas")
                End While

            End Using
            Return item
        End Function

        Public Function Anular(entidad As Remesas) As Remesas
            Dim item As New Remesas
            Dim itemDocumento As EncabezadoDocumentos
            Dim lista As New List(Of EncabezadoDocumentos)
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Anula", entidad.UsuarioAnula.Codigo)
                conexion.AgregarParametroSQL("@par_Causa_Anula", entidad.CausaAnula)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_anular_encabezado_remesas")

                While resultado.Read
                    itemDocumento = New EncabezadoDocumentos With {
                     .Planilla = New Planillas With {.Numero = Read(resultado, "ENPD_Numero_Documento")},
                     .FacturaVentas = New Entidades.Facturacion.Facturas With {.Numero = Read(resultado, "ENFA_Numero_Documento")},
                     .Manifiesto = New Manifiesto With {.Numero = Read(resultado, "ENMC_Numero_Documento")}
                     }
                    lista.Add(itemDocumento)
                    item.Anulado = Read(resultado, "Anulo")

                End While
                item.DocumentosRelacionados = lista

            End Using

            Return item
        End Function

        Private Function ActualizarDespachoSolicitudServicio(entidad As Remesas, contextoConexion As Conexion.DataBaseFactory) As Boolean

            Dim Actualizo As Boolean

            contextoConexion.CleanParameters()
            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_ESOS_Codigo", entidad.NumeroOrdenServicio)
            contextoConexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
            contextoConexion.AgregarParametroSQL("@par_Numero_Documento", entidad.NumeroDocumento)
            contextoConexion.AgregarParametroSQL("@par_ID", entidad.IDDetalleSolicitud)
            contextoConexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("gsp_actualizar_documento_solicitud_Servicios")

            While resultado.Read
                Actualizo = IIf(resultado.Item("Cantidad") > Cero, True, False)
            End While

            resultado.Close()

            Return Actualizo

        End Function

        Public Function CumplirRemesas(entidad As Remesas) As Boolean
            Dim Cumplio As Boolean = True
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)

                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    Dim resultado As IDataReader
                    If Not IsNothing(entidad.CumplidoRemesas) Then
                        conexion.CreateConnection()
                        For Each item In entidad.CumplidoRemesas
                            conexion.CleanParameters()
                            conexion.AgregarParametroSQL("@par_EMPR_Codigo", item.CodigoEmpresa)
                            conexion.AgregarParametroSQL("@par_ENRE_Numero", item.NumeroRemesa)
                            conexion.AgregarParametroSQL("@par_Fecha_Recibe", item.FechaRecibe, SqlDbType.Date)
                            conexion.AgregarParametroSQL("@par_Nombre_Recibe", item.NombreRecibe)
                            conexion.AgregarParametroSQL("@par_Numero_Identificacion_Recibe", item.IdentificacionRecibe)
                            conexion.AgregarParametroSQL("@par_Telefonos_Recibe", item.TelefonoRecibe)
                            conexion.AgregarParametroSQL("@par_Observaciones_Recibe", item.ObservacionesRecibe)
                            conexion.AgregarParametroSQL("@par_Cantidad_Recibe", item.CantidadRecibida)
                            conexion.AgregarParametroSQL("@par_Peso_Recibe", item.PesoRecibido)
                            conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", item.UsuarioCrea.Codigo)
                            conexion.AgregarParametroSQL("@par_OFIC_Codigo", item.Oficina.Codigo)
                            conexion.AgregarParametroSQL("@par_Cantidad_Faltante", item.CantidadFaltante)
                            conexion.AgregarParametroSQL("@par_Peso_Faltante", item.PesoFaltante)
                            conexion.AgregarParametroSQL("@par_Valor_Faltante", item.ValorFlatante, SqlDbType.Money)
                            conexion.AgregarParametroSQL("@par_Peso_Cargue", item.PesoCargue)
                            conexion.AgregarParametroSQL("@par_Peso_Descargue", item.PesoDescargue)


                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_cumplir_remesas")
                            While resultado.Read
                                If Not (resultado.Item("ENRE_Numero") > 0) Then
                                    Cumplio = False
                                    Exit For
                                End If
                            End While
                            resultado.Close()
                        Next
                    End If
                End Using
                If Cumplio Then
                    transaccion.Complete()
                End If
            End Using

            Return Cumplio
        End Function

        Public Function ObtenerPrecintos(entidad As Remesas) As IEnumerable(Of DetallePrecintos)
            Dim item As New DetallePrecintos
            Dim lista As New List(Of DetallePrecintos)
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENRE_Codigo", entidad.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_precintos_remesas")

                While resultado.Read
                    item = New DetallePrecintos(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Function ModificarPrecitos(entidad As DetallePrecintos, NumeroRemesa As Integer, CodigoEmpresa As Integer, conexion As Conexion.DataBaseFactory) As Boolean
            Dim Actualizo As Boolean

            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_ID", entidad.IdPreci)
            conexion.AgregarParametroSQL("@par_ENRE_Numero", NumeroRemesa)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_precintos_remesas")

            While resultado.Read
                Actualizo = IIf(resultado.Item("Cantidad") > Cero, True, False)
            End While
            resultado.Close()
            Return Actualizo
        End Function

        Public Function ObtenerPrecintosAleatorios(entidad As Remesas) As IEnumerable(Of DetallePrecintos)
            Dim item As New DetallePrecintos
            Dim lista As New List(Of DetallePrecintos)
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Cantidad_Precintos", entidad.CantidadPrecintos)
                conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
                conexion.AgregarParametroSQL("@par_CATA_TPRE_Codigo", entidad.TipoPrecinto.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_precintos_aleatorios_despachos")

                While resultado.Read
                    item = New DetallePrecintos(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Function ConsultarNumeroConfirmacion(Filtro As Remesas) As Remesas

            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", Filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", Filtro.Numero)

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_numero_ministerio")

                While resultado.Read
                    AuxRemesaRNDC = New Remesas(resultado)
                End While

            End Using

            Return AuxRemesaRNDC

        End Function
        Public Function ConsultarRemesasPlanillaDespachos(filtro As Remesas) As IEnumerable(Of Remesas)
            Dim lista As New List(Of Remesas)
            Dim item As Remesas
            Dim resultado As IDataReader

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.Oficina) Then
                    If filtro.Oficina.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficina.Codigo)
                    End If
                End If

                If filtro.NumeroDocumento > Cero Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.NumeroDocumento)
                End If

                If filtro.FechaInicial > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                End If

                If filtro.FechaFinal > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                End If
                If Not IsNothing(filtro.Cliente) Then
                    If filtro.Cliente.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_CLIE_Codigo", filtro.Cliente.Codigo)
                    End If
                End If
                If filtro.NumeroDocumentoOrdenServicio > 0 Then
                    conexion.AgregarParametroSQL("@par_ESOS_Numero_Documento", filtro.NumeroDocumentoOrdenServicio)
                End If
                If Not IsNothing(filtro.CiudadDestinatario) Then
                    If filtro.CiudadDestinatario.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo_Destino", filtro.CiudadDestinatario.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.CiudadRemitente) Then
                    If filtro.CiudadRemitente.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo_origen", filtro.CiudadRemitente.Codigo)
                    End If
                End If
                If filtro.NumeroPlanilla = -1 Then
                    conexion.AgregarParametroSQL("@par_ENPD_Numero", 0)
                ElseIf filtro.NumeroPlanilla > 0 Then
                    conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.NumeroPlanilla)
                End If
                If filtro.Estado >= 0 And filtro.Estado <> 2 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                    conexion.AgregarParametroSQL("@par_Anulado", 0)

                ElseIf filtro.Estado = 2 Then
                    conexion.AgregarParametroSQL("@par_Anulado", 1)
                End If
                If Not IsNothing(filtro.TipoDocumento) Then
                    If filtro.TipoDocumento.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > Cero Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > Cero Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_remesas_planillas_despachos")

                While resultado.Read
                    item = New Remesas(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function


        Public Function ConsultarCumplidoRemesas(filtro As Remesas) As IEnumerable(Of Remesas)
            Dim lista As New List(Of Remesas)
            Dim item As Remesas
            Dim resultado As IDataReader

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.NumeroDocumento > Cero Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.NumeroDocumento)
                End If

                If filtro.FechaInicial > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                End If

                If filtro.FechaFinal > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                End If
                If Not IsNothing(filtro.Vehiculo) Then
                    If Not IsNothing(filtro.Vehiculo.Placa) And filtro.Vehiculo.Placa <> String.Empty Then
                        conexion.AgregarParametroSQL("@par_Placa", filtro.Vehiculo.Placa)
                    End If
                End If


                If filtro.NumeroOrdenServicio > 0 Then
                    conexion.AgregarParametroSQL("@par_ESOS_Numero", filtro.NumeroOrdenServicio)
                End If
                If filtro.NumeroDocumentoOrdenServicio > 0 Then
                    conexion.AgregarParametroSQL("@par_ESOS_Numero_Documento", filtro.NumeroDocumentoOrdenServicio)
                End If
                If Not IsNothing(filtro.Manifiesto) Then
                    If filtro.Manifiesto.NumeroDocumento > 0 Then
                        conexion.AgregarParametroSQL("@par_ENMC_Numero", filtro.Manifiesto.NumeroDocumento)
                    End If
                End If

                If Not String.IsNullOrWhiteSpace(filtro.FiltroCliente) Then
                    If Len(Trim(filtro.FiltroCliente)) > Cero Then
                        conexion.AgregarParametroSQL("@par_Cliente", filtro.FiltroCliente)
                    End If
                End If

                If Not IsNothing(filtro.Cliente) Then
                    If filtro.Cliente.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_CLIE_Codigo", filtro.Cliente.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.NumeroContenedor) And filtro.NumeroContenedor <> String.Empty Then
                    conexion.AgregarParametroSQL("@par_Numero_Contenedor", filtro.NumeroContenedor)
                End If
                If Not IsNothing(filtro.NumeroDocumentoCliente) And filtro.NumeroDocumentoCliente <> String.Empty Then
                    conexion.AgregarParametroSQL("@par_Documento_Cliente", filtro.NumeroDocumentoCliente)
                End If

                If Not IsNothing(filtro.Vehiculo) Then
                    If Not IsNothing(filtro.Vehiculo.Placa) And filtro.Vehiculo.Placa <> String.Empty Then
                        conexion.AgregarParametroSQL("@par_Placa", filtro.Vehiculo.Placa)
                    End If
                End If
                If Not IsNothing(filtro.FiltroConductor) And filtro.FiltroConductor <> String.Empty Then
                    conexion.AgregarParametroSQL("@par_Conductor", filtro.FiltroConductor)
                End If

                If Not IsNothing(filtro.FiltroTenedor) And filtro.FiltroTenedor <> String.Empty Then
                    conexion.AgregarParametroSQL("@par_Tenedor", filtro.FiltroTenedor)
                End If
                If Not IsNothing(filtro.TipoDocumento) Then
                    If filtro.TipoDocumento.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.TipoRemesa) Then
                    If filtro.TipoRemesa.Codigo <> Cero Then
                        conexion.AgregarParametroSQL("@par_CATA_TIRE_Codigo", filtro.TipoRemesa.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Oficina) Then
                    If filtro.Oficina.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficina.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.CiudadDestinatario) Then
                    If filtro.CiudadDestinatario.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo_Destino", filtro.CiudadDestinatario.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.CiudadRemitente) Then
                    If filtro.CiudadRemitente.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo_origen", filtro.CiudadRemitente.Codigo)
                    End If
                End If

                If filtro.PendienteFacturadas Then
                    conexion.AgregarParametroSQL("@par_PendienteFacturadas", NO_APLICA)
                End If

                If filtro.Estado >= 0 And filtro.Estado <> 2 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                    conexion.AgregarParametroSQL("@par_Anulado", 0)

                ElseIf filtro.Estado = 2 Then
                    conexion.AgregarParametroSQL("@par_Anulado", 1)
                End If


                If filtro.Cumplido > 0 Then
                    conexion.AgregarParametroSQL("@par_Cumplido", filtro.Cumplido)
                    '    conexion.AgregarParametroSQL("@par_Planillado", Cero)
                End If

                If filtro.NumeroDocumentoPlanillaDespacho > 0 Then
                    conexion.AgregarParametroSQL("@par_ENPD_Numero_Documento", filtro.NumeroDocumentoPlanillaDespacho)
                End If
                If filtro.NumeroPlanilla = -1 Then
                    conexion.AgregarParametroSQL("@par_ENPD_Numero", 0)
                ElseIf filtro.NumeroPlanilla > 0 Then
                    conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.NumeroPlanilla)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > Cero Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > Cero Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_Cumplir_consultar_remesas")

                While resultado.Read
                    item = New Remesas(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function


        Public Function ConsultarDocumentoSoporte(filtro As Remesas) As IEnumerable(Of Remesas)
            Dim lista As New List(Of Remesas)
            Dim item As Remesas
            Dim resultado As IDataReader

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.Estado >= 0 And filtro.Estado <> 2 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                    conexion.AgregarParametroSQL("@par_Anulado", 0)

                ElseIf filtro.Estado = 2 Then
                    conexion.AgregarParametroSQL("@par_Anulado", 1)
                End If
                If Not IsNothing(filtro.TipoRemesa) Then
                    If filtro.TipoRemesa.Codigo <> Cero Then
                        conexion.AgregarParametroSQL("@par_CATA_TIRE_Codigo", filtro.TipoRemesa.Codigo)
                    End If
                End If
                If filtro.NumeroDocumento > Cero Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.NumeroDocumento)
                End If


                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > Cero Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > Cero Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_remesas_documento_soporte")

                While resultado.Read
                    item = New Remesas(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

    End Class

End Namespace