﻿EncoExpresApp.controller("ConsultarIndicadoresDespachosCtrl", ['$scope', '$timeout', '$linq', '$routeParams', 'blockUIConfig', 'TercerosFactory', 'SitiosCargueDescargueFactory', 'RemesaGuiasFactory', 'OficinasFactory', 'RutasFactory', 'VehiculosFactory', 'SemirremolquesFactory', 'ProductoTransportadosFactory','PlanillaDespachosFactory',
    function ($scope, $timeout, $linq, $routeParams, blockUIConfig, TercerosFactory, SitiosCargueDescargueFactory, RemesaGuiasFactory, OficinasFactory, RutasFactory, VehiculosFactory, SemirremolquesFactory, ProductoTransportadosFactory, PlanillaDespachosFactory) {

        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        $scope.MapaSitio = [{ Nombre: 'Despachos' }, { Nombre: 'Indicadores' }];
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        $scope.ListadoConsulta = [];
        $scope.ListadoTiposGrafica = [];
        $scope.MensajesError = [];
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
        }
        var IngresoSitio = 0;
        //--------------Parametros URL
        if ($routeParams !== undefined && $routeParams.Codigo !== null) {
            IngresoSitio = parseInt($routeParams.Codigo);
        }


        $scope.ListadoIndicadores = [
            { Nombre: 'Cantidad Despachos por Oficina', Codigo: 1 },
            { Nombre: 'Cantidad Despachos por Cliente', Codigo: 2 },
            { Nombre: 'Cantidad Despachos por Ruta', Codigo: 3 },
            { Nombre: 'Cantidad Despachos por Producto', Codigo: 4 },
            { Nombre: 'Peso Despachos por Oficina', Codigo: 5 },
            { Nombre: 'Peso Despachos por Cliente', Codigo: 6 },
            { Nombre: 'Peso Despachos por Ruta', Codigo: 7 },
            { Nombre: 'Peso Despachos por Producto', Codigo: 8 }
        ];
        $scope.Indicador = $linq.Enumerable().From($scope.ListadoIndicadores).First('$.Codigo == 1');

        switch (IngresoSitio) {
            case CODIGO_APLICACION_PORTAL:
                try {
                    $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_INDICADORES_DESPACHOS);
                    if ($scope.Sesion.UsuarioAutenticado.Cliente.Codigo > 0 && $scope.Sesion.UsuarioAutenticado.Cliente.Codigo != undefined && $scope.Sesion.UsuarioAutenticado.Cliente.Codigo != '') {
                        $scope.Modelo.Cliente = $scope.CargarTercero($scope.Sesion.UsuarioAutenticado.Cliente.Codigo);
                        $scope.DeshabilitarCliente = true;
                        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

                        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
                        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
                        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
                        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
                    }
                    else {
                        if ($scope.Sesion.UsuarioAutenticado.Codigo > 0) {
                            ShowError('El usuario ingresado no tiene clientes asociados');
                            $scope.DeshabilitarConsulta = true;
                            $scope.DeshabilitarEliminarAnular = true;
                            $scope.DeshabilitarImprimir = true;
                            $scope.DeshabilitarActualizar = true;

                        }
                    }
                }
                catch (e) {
                    ShowError('No tiene permiso para visualizar esta pagina');
                    document.location.href = '#';
                }
                break;
            default:
                try {
                    $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_INDICADORES_DESPACHOS);
                    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

                    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
                    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
                    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
                    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
                }
                catch (e) {
                    ShowError('No tiene permiso para visualizar esta pagina');
                    document.location.href = '#';
                }
                break;
        }
        //Listado Oficinas:

        $scope.ListadoOficinas = [];
        OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso == true) {
                    $scope.ListadoOficinas = response.data.Datos;
                }
            });

        $scope.Modelo.Oficina = '';

        //Listado Rutas:

        $scope.ListadoRutas = [];
        RutasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado : 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso == true) {
                    
                    $scope.ListadoRutas = response.data.Datos;
                }
            });

        $scope.Modelo.Ruta = '';
        

        //Listado Vehiculos:

        $scope.ListadoVehiculos = [];
        VehiculosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso == true) {   
                    console.log("Vehiculos: ", response);
                    $scope.ListadoVehiculos = response.data.Datos;
                }
            });

        $scope.Modelo.Vehiculo = '';
        $scope.Modelo.Semirremolque = '';
        $scope.Modelo.Conductor = '';
        $scope.Modelo.Tenedor = '';
        $scope.Modelo.Producto = '';

        $scope.CargarDatos = function () {
            $scope.Modelo.Semirremolque = $scope.Modelo.Vehiculo.Semirremolque;
            //$scope.Modelo.Conductor = $scope.Modelo.Vehiculo.Conductor;
            $scope.Modelo.Tenedor = $scope.Modelo.Vehiculo.Tenedor;
        }

        //Listado Semirremolques:

        $scope.ListadoSemirremolques = [];
        SemirremolquesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso == true) {
                    
                    $scope.ListadoSemirremolques = response.data.Datos;
                }
            });

       


        //AutocompleteConductores:

       

        $scope.ListadoConductores = [];

        $scope.AutocompleteConductores = function (value) {
            if (value.length > 0) {
                blockUIConfig.autoBlock = false;
                var Response = TercerosFactory.Consultar({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    CadenaPerfiles: PERFIL_CONDUCTOR,
                    ValorAutocomplete: value,
                    Sync: true
                })
                $scope.ListadoConductores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoConductores)

            }
            return $scope.ListadoConductores;
        }

        //AutocompleteTenedores:

        $scope.Modelo.Tenedor = '';

        $scope.ListadoTenedores = [];

        $scope.AutocompleteTenedores = function (value) {
            if (value.length > 0) {
                blockUIConfig.autoBlock = false;
                var Response = TercerosFactory.Consultar({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    CadenaPerfiles: PERFIL_TENEDOR,
                    ValorAutocomplete: value,
                    Sync: true
                })
                $scope.ListadoTenedores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoTenedores)

            }
            return $scope.ListadoTenedores;
        }

        //Listado Productos:

        $scope.ListadoProductos = [];
        ProductoTransportadosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa}).
            then(function (response) {
                if (response.data.ProcesoExitoso == true) {

                    $scope.ListadoProductos = response.data.Datos;
                    console.log("ListadoProductos: ",$scope.ListadoProductos);
                }
            });

        $scope.Modelo.Producto = '';

        //-- Consulta Clientes --//
        $scope.ListadoCliente = []
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CLIENTE,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCliente)
                }
            }
            return $scope.ListadoCliente
        }
        //-- Consulta Destinatarios --//
        $scope.ListadoDestinatarios = []
        $scope.AutocompleteDestinatario = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_DESTINATARIO,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoDestinatarios = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoDestinatarios)
                }
            }
            return $scope.ListadoDestinatarios
        }
       
        //-- Consulta Remitentes --//
        
        $scope.ListadoTiposGrafica = [
            { Nombre: 'Circular', Codigo: 2 },
            { Nombre: 'Barras Verticales', Codigo: 0 },
            { Nombre: 'Barras Horizontales', Codigo: 1 },
            { Nombre: 'Linea', Codigo: 3 },
        ];
        $scope.Modelo.TipoGrafica = $scope.ListadoTiposGrafica[0]


        $scope.BuscarGrafica = function () {
        }




        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };

        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }
        };

        function Find() {
            if ($scope.Modelo.Oficina == undefined || $scope.Modelo.Oficina == null || $scope.Modelo.Oficina == '') {
                $scope.Modelo.Oficina = { Codigo: 0, Nombre: '' };
            }
            if ($scope.Modelo.Ruta == undefined || $scope.Modelo.Ruta == null || $scope.Modelo.Ruta == '') {
                $scope.Modelo.Ruta = { Codigo: 0, Nombre: '' };
            }
            if ($scope.Modelo.Vehiculo == undefined || $scope.Modelo.Vehiculo == null || $scope.Modelo.Vehiculo == '') {
                $scope.Modelo.Vehiculo = { Codigo: 0, Placa: '' };
            }
            if ($scope.Modelo.Semirremolque == undefined || $scope.Modelo.Semirremolque == null || $scope.Modelo.Semirremolque == '') {
                $scope.Modelo.Semirremolque = { Codigo: 0, Placa: '' };
            }
            if ($scope.Modelo.Conductor == undefined || $scope.Modelo.Conductor == null || $scope.Modelo.Conductor == '') {
                $scope.Modelo.Conductor = { Codigo: 0, NombreCompleto: '' };
            }
            if ($scope.Modelo.Tenedor == undefined || $scope.Modelo.Tenedor == null || $scope.Modelo.Tenedor == '') {
                $scope.Modelo.Tenedor = { Codigo: 0, NombreCompleto: '' };
            }
            if ($scope.Modelo.Cliente == undefined || $scope.Modelo.Cliente == null || $scope.Modelo.Cliente == '') {
                $scope.Modelo.Cliente = { Codigo: 0, NombreCompleto: '' };
            }
            if ($scope.Modelo.Producto == undefined || $scope.Modelo.Producto == null || $scope.Modelo.Producto == '') {
                $scope.Modelo.Producto = { Codigo: 0, Nombre: '' };
            }

            $scope.MensajesError = [];
            $scope.ListadoConsulta = [];
            if (DatosRequeridos()) {
                BloqueoPantalla.start('Buscando registros ...');
                if ($scope.MensajesError.length == 0) {
                    filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        FechaInicial: $scope.Modelo.FechaInicial,
                        FechaFinal: $scope.Modelo.FechaFinal,
                        Oficina: { Codigo: $scope.Modelo.Oficina.Codigo },
                        Ruta: { Codigo: $scope.Modelo.Ruta.Codigo },
                        Vehiculo: { Codigo: $scope.Modelo.Vehiculo.Codigo },
                        Semirremolque: { Codigo: $scope.Modelo.Semirremolque.Codigo },
                        Conductor: { Codigo: $scope.Modelo.Conductor.Codigo },
                        Tenedor: { Codigo: $scope.Modelo.Tenedor.Codigo },
                        Cliente: { Codigo: $scope.Modelo.Cliente.Codigo },
                        Producto: {Codigo: $scope.Modelo.Producto.Codigo},
                        Pagina: $scope.paginaActual,
                        RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                    }

                    PlanillaDespachosFactory.Consultar(filtros).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.length > 0) {
                                    $scope.ListadoConsulta = response.data.Datos
                                    $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                    $scope.ResultadoSinRegistros = '';
                                    ConsultarGrafica()
                                }
                                else {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                }
                                var listado = [];
                                response.data.Datos.forEach(function (item) {
                                    listado.push(item);
                                });
                            }
                            BloqueoPantalla.stop();
                        }, function (response) {
                            ShowError(response.statusText);
                            BloqueoPantalla.stop();
                        });
                }
            }
        }

        $scope.ValidarFechas = function () {
            if ($scope.Modelo.Tipo.Codigo == 0) {
                $scope.DeshabilitarFechas = false
            }
            else {
                $scope.DeshabilitarFechas = true
            }
        }

        $scope.ConsultaGrafica = function () {
            ConsultarGrafica();
        }

        function ConsultarGrafica() {
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                FechaInicial: $scope.Modelo.FechaInicial,
                FechaFinal: $scope.Modelo.FechaFinal,
                Oficina: { Codigo: $scope.Modelo.Oficina.Codigo },
                Ruta: { Codigo: $scope.Modelo.Ruta.Codigo },
                Vehiculo: { Codigo: $scope.Modelo.Vehiculo.Codigo },
                Semirremolque: { Codigo: $scope.Modelo.Semirremolque.Codigo },
                Conductor: { Codigo: $scope.Modelo.Conductor.Codigo },
                Tenedor: { Codigo: $scope.Modelo.Tenedor.Codigo },
                Cliente: { Codigo: $scope.Modelo.Cliente.Codigo },
                Producto: { Codigo: $scope.Modelo.Producto.Codigo },
                IndicadorDespachos: $scope.Indicador.Codigo
            }

            PlanillaDespachosFactory.Consultar(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            console.log("DatosGrafica: ", response);
                            Graficar(response.data.Datos)
                        }
                    }
                    BloqueoPantalla.stop();
                }, function (response) {
                    ShowError(response.statusText);
                    BloqueoPantalla.stop();
                });
        }


        //inicializacion de divisiones
        $('#Grafica').show(); $('#ConsultaViajes').hide();


        $scope.MostrarGrafica = function () {
            $('#Grafica').show()
            $('#ConsultaViajes').hide();
            Find()
        }
        $scope.MostrarConsulta = function () {
            $('#Grafica').hide()
            $('#ConsultaViajes').show();
            Find()
        }
       // if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.Modelo.FechaInicial = new Date();
            $scope.Modelo.FechaFinal = new Date();
       // }
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.Modelo.FechaInicial == null || $scope.Modelo.FechaInicial == undefined || $scope.Modelo.FechaInicial == '')
                && ($scope.Modelo.FechaFinal == null || $scope.Modelo.FechaFinal == undefined || $scope.Modelo.FechaFinal == '')
            ) {
                $scope.MensajesError.push('Debe ingresar un rango de fechas');
                continuar = false

            } else if (($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')
                || ($scope.Modelo.FechaFinal !== null && $scope.Modelo.FechaFinal !== undefined && $scope.Modelo.FechaFinal !== '')

            ) {
                if (($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')
                    && ($scope.Modelo.FechaFinal !== null && $scope.Modelo.FechaFinal !== undefined && $scope.Modelo.FechaFinal !== '')) {
                    if ($scope.Modelo.FechaFinal < $scope.Modelo.FechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.Modelo.FechaFinal - $scope.Modelo.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }
            }

            return continuar
        }

        function Graficar(Datos) {

            var DatosAjustados = [];

            //// Load the Visualization API and the corechart package.
            google.charts.load('current', { 'packages': ['corechart'] });

            // Set a callback to run when the Google Visualization API is loaded.
            google.charts.setOnLoadCallback(drawChart);

            // Callback that creates and populates a data table,
            // instantiates the pie chart, passes in the data and
            // draws it.
            function drawChart() {

                // Create the data table.
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Oficina');
                data.addColumn('number', 'Cantidad');

                for (var i = 0; i < Datos.length; i++) {
                    DatosAjustados.push([Datos[i].Nombre, Datos[i].Cantidad])
                }
                data.addRows(DatosAjustados);
                //// Set chart options
                //var options = {
                //    'title': 'How Much Pizza I Ate Last Night',
                //    'width': 400,
                //    'height': 300
                //};

                if ($scope.Indicador.Codigo == 1) {
                    $scope.TituloGrafica = 'CANTIDAD DESPACHOS POR OFICINA'
                } else if ($scope.Indicador.Codigo == 2) {
                    $scope.TituloGrafica = 'CANTIDAD DESPACHOS POR CLIENTE'
                } else if ($scope.Indicador.Codigo == 3) {
                    $scope.TituloGrafica = 'CANTIDAD DESPACHOS POR RUTA'
                } else if ($scope.Indicador.Codigo == 4) {
                    $scope.TituloGrafica = 'CANTIDAD DESPACHOS POR PRODUCTO'
                } 
                else if ($scope.Indicador.Codigo == 5) {
                    $scope.TituloGrafica = 'PESO TOTAL(kg) DESPACHOS POR OFICINA'
                } 
                else if ($scope.Indicador.Codigo == 6) {
                    $scope.TituloGrafica = 'PESO TOTAL(kg) DESPACHOS POR CLIENTE'
                } 
                else if ($scope.Indicador.Codigo == 7) {
                    $scope.TituloGrafica = 'PESO TOTAL(kg) DESPACHOS POR RUTA'
                } 
                else if ($scope.Indicador.Codigo == 8) {
                    $scope.TituloGrafica = 'PESO TOTAL(kg) DESPACHOS POR PRODUCTO'
                } 
               

                var options = {
                    title: $scope.TituloGrafica,
                    pieHole: 0.4,
                    is3D: true,
                };

                if ($scope.Modelo.TipoGrafica.Codigo == 0) {
                    var chart = new google.visualization.ColumnChart(document.getElementById('donutchart'));
                    chart.draw(data, options);
                }
                else if ($scope.Modelo.TipoGrafica.Codigo == 2) {
                    var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
                    chart.draw(data, options);
                }
                else if ($scope.Modelo.TipoGrafica.Codigo == 3) {
                    var chart = new google.visualization.LineChart(document.getElementById('donutchart'));
                    chart.draw(data, options);
                }
                else if ($scope.Modelo.TipoGrafica.Codigo == 1) {
                    var chart = new google.visualization.BarChart(document.getElementById('donutchart'));
                    chart.draw(data, options);
                }
                //// Instantiate and draw our chart, passing in some options.
                //var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
                //chart.draw(data, options);
            }
        }


        //Desplegar Reporte
        $scope.DesplegarInforme = function () {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);

            $scope.FiltroArmado = '';

            //Depende del listado seleccionado se enviará el nombre por parametro
            $scope.NombreReporte = 'lstIndicadoresDespachos'
            //Arma el filtro
            $scope.ArmarFiltro();
            //Llama a la pagina ASP con el filtro por GET

            window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + 1);

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';

        }

        $scope.ArmarFiltro = function () {
            $scope.FiltroArmado = '';

            if ($scope.Modelo.Oficina != undefined && $scope.Modelo.Oficina != null && $scope.Modelo.Oficina !== '' && $scope.Modelo.Oficina.Codigo != 0) {
                $scope.FiltroArmado += '&Oficina=' + $scope.Modelo.Oficina.Codigo;
            }
            if ($scope.Modelo.Ruta != undefined && $scope.Modelo.Ruta != null && $scope.Modelo.Ruta != '' && $scope.Modelo.Ruta.Codigo != 0) {
                $scope.FiltroArmado += '&Ruta=' + $scope.Modelo.Ruta.Codigo;
            }
            if ($scope.Modelo.Vehiculo != undefined && $scope.Modelo.Vehiculo != null && $scope.Modelo.Vehiculo != '' && $scope.Modelo.Vehiculo.Codigo != 0) {
                $scope.FiltroArmado += '&Vehiculo=' + $scope.Modelo.Vehiculo.Codigo;
            }
            if ($scope.Modelo.Tenedor != undefined && $scope.Modelo.Tenedor != null && $scope.Modelo.Tenedor != '' && $scope.Modelo.Tenedor.Codigo != 0) {
                $scope.FiltroArmado += '&Tenedor=' + $scope.Modelo.Tenedor.Codigo;
            }
            if ($scope.Modelo.Cliente != undefined && $scope.Modelo.Cliente != null && $scope.Modelo.Cliente != '' && $scope.Modelo.Cliente.Codigo != 0) {
                $scope.FiltroArmado += '&Cliente=' + $scope.Modelo.Cliente.Codigo;
            }
            if ($scope.Modelo.Conductor != undefined && $scope.Modelo.Conductor != null && $scope.Modelo.Conductor != '' && $scope.Modelo.Conductor.Codigo != 0) {
                $scope.FiltroArmado += '&Conductor=' + $scope.Modelo.Conductor.Codigo;
            }
            if ($scope.Modelo.Semirremolque != undefined && $scope.Modelo.Semirremolque != null && $scope.Modelo.Semirremolque != '' && $scope.Modelo.Semirremolque.Codigo != 0) {
                $scope.FiltroArmado += '&Semirremolque=' + $scope.Modelo.Semirremolque.Codigo;
            }
            if ($scope.Modelo.Producto != undefined && $scope.Modelo.Producto != null && $scope.Modelo.Producto != '' && $scope.Modelo.Producto.Codigo != 0) {
                $scope.FiltroArmado += '&Producto=' + $scope.Modelo.Producto.Codigo;
            }
            if ($scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '' && $scope.Modelo.FechaInicial !== null) {
                var FechaInicial = Formatear_Fecha_Mes_Dia_Ano($scope.Modelo.FechaInicial);
                $scope.FiltroArmado += '&FechaInicial=' + FechaInicial;
            }
            if ($scope.Modelo.FechaFinal !== undefined && $scope.Modelo.FechaFinal !== '' && $scope.Modelo.FechaFinal !== null) {
                var FechaFinal = Formatear_Fecha_Mes_Dia_Ano($scope.Modelo.FechaFinal);
                $scope.FiltroArmado += '&FechaFinal=' + FechaFinal;
            }

        };

        $scope.DesplegarDescargaConsulta = function () {
            if ($scope.Modelo.Oficina == undefined || $scope.Modelo.Oficina == null || $scope.Modelo.Oficina == '') {
                $scope.Modelo.Oficina = { Codigo: 0, Nombre: '' };
            }
            if ($scope.Modelo.Ruta == undefined || $scope.Modelo.Ruta == null || $scope.Modelo.Ruta == '') {
                $scope.Modelo.Ruta = { Codigo: 0, Nombre: '' };
            }
            if ($scope.Modelo.Vehiculo == undefined || $scope.Modelo.Vehiculo == null || $scope.Modelo.Vehiculo == '') {
                $scope.Modelo.Vehiculo = { Codigo: 0, Placa: '' };
            }
            if ($scope.Modelo.Semirremolque == undefined || $scope.Modelo.Semirremolque == null || $scope.Modelo.Semirremolque == '') {
                $scope.Modelo.Semirremolque = { Codigo: 0, Placa: '' };
            }
            if ($scope.Modelo.Conductor == undefined || $scope.Modelo.Conductor == null || $scope.Modelo.Conductor == '') {
                $scope.Modelo.Conductor = { Codigo: 0, NombreCompleto: '' };
            }
            if ($scope.Modelo.Tenedor == undefined || $scope.Modelo.Tenedor == null || $scope.Modelo.Tenedor == '') {
                $scope.Modelo.Tenedor = { Codigo: 0, NombreCompleto: '' };
            }
            if ($scope.Modelo.Cliente == undefined || $scope.Modelo.Cliente == null || $scope.Modelo.Cliente == '') {
                $scope.Modelo.Cliente = { Codigo: 0, NombreCompleto: '' };
            }
            if ($scope.Modelo.Producto == undefined || $scope.Modelo.Producto == null || $scope.Modelo.Producto == '') {
                $scope.Modelo.Producto = { Codigo: 0, Nombre: '' };
            }
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                FechaInicial: $scope.Modelo.FechaInicial,
                FechaFinal: $scope.Modelo.FechaFinal,
                Oficina: { Codigo: $scope.Modelo.Oficina.Codigo },
                Ruta: { Codigo: $scope.Modelo.Ruta.Codigo },
                Vehiculo: { Codigo: $scope.Modelo.Vehiculo.Codigo },
                Semirremolque: { Codigo: $scope.Modelo.Semirremolque.Codigo },
                Conductor: { Codigo: $scope.Modelo.Conductor.Codigo },
                Tenedor: { Codigo: $scope.Modelo.Tenedor.Codigo },
                Cliente: { Codigo: $scope.Modelo.Cliente.Codigo },
                Producto: { Codigo: $scope.Modelo.Producto.Codigo }
               
            };

            PlanillaDespachosFactory.Consultar(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            var ListadoIndicadores = response.data.Datos;
                            var xmlObj = '<?xml version="1.0" encoding="UTF-8"?>';
                            var Columnas = {
                                columna1: "Número_Documento",
                                columna2: "Fecha",
                                columna3: "Vehículo",
                                columna4: "Semirremolque",
                                columna5: "Tenedor",
                                columna6: "Conductor",
                                columna7: "Flete_Transportador",
                                columna8: "Número_Remesa",
                                columna9: "Cliente",
                                columna10: "Ruta",
                                columna11: "Producto",
                                columna12: "Cantidad_Cliente",
                                columna13: "Peso_Cliente",
                                columna14: "Flete_Cliente",
                                columna15: "Oficina"
                            };
                            xmlObj += "<row>";
                            for (var i = 0; i < ListadoIndicadores.length; i++) {
                               // var sitioDescargue = ListadoIndicadores[i].SitioDescargue.Nombre == null ? "" : ListadoIndicadores[i].SitioDescargue.Nombre;
                               // Formatear_Fecha
                                xmlObj += "<reg " + Columnas.columna1 + "='" + ListadoIndicadores[i].NumeroDocumento + "' " +
                                    Columnas.columna2 + "='" + Formatear_Fecha(ListadoIndicadores[i].Fecha, FORMATO_FECHA_dd + "/" + FORMATO_FECHA_MM + "/" + FORMATO_FECHA_yyyy) + "' " +
                                   // Columnas.columna2 + "='" + ListadoIndicadores[i].Fecha + "' " +
                                    Columnas.columna3 + "='" + ListadoIndicadores[i].Vehiculo.Placa + "' " +
                                    Columnas.columna4 + "='" + ListadoIndicadores[i].Semirremolque.Placa + "' " +
                                    Columnas.columna5 + "='" + ListadoIndicadores[i].Tenedor.Nombre + "' " +
                                    Columnas.columna6 + "='" + ListadoIndicadores[i].Conductor.Nombre + "' " +
                                    Columnas.columna7 + "='" + ListadoIndicadores[i].ValorFleteTransportador + "' " +
                                    Columnas.columna8 + "='" + ListadoIndicadores[i].Remesa.NumeroDocumento + "' " +                                   
                                    Columnas.columna9 + "='" + ListadoIndicadores[i].Cliente.Nombre + "' " +
                                    Columnas.columna10 + "='" + ListadoIndicadores[i].Ruta.Nombre + "' " +
                                    Columnas.columna11 + "='" + ListadoIndicadores[i].Producto.Nombre + "' " +
                                    Columnas.columna12 + "='" + ListadoIndicadores[i].Remesa.CantidadCliente + "' " +
                                    Columnas.columna13 + "='" + ListadoIndicadores[i].Remesa.PesoCliente + "' " +
                                    Columnas.columna14 + "='" + ListadoIndicadores[i].Remesa.ValorFleteCliente + "' " +
                                    Columnas.columna15 + "='" + ListadoIndicadores[i].Oficina.Nombre + "' />";
                            }
                            xmlObj += "</row>";
                            download('IndicadoresDespachos.xls', xmlObj);
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };

        function download(filename, text) {
            var element = document.createElement('a');
            element.setAttribute('href', 'data:text/xml;charset=utf-8,' + encodeURIComponent(text));
            element.setAttribute('download', filename);
            element.style.display = 'none';
            document.body.appendChild(element);
            $timeout(function () { element.click(); }, 300);
            document.body.removeChild(element);
        }
    }]);