﻿Imports System.Data.SqlClient
Imports System.IO
Public Class ArchivoUIAF
    Inherits System.Web.UI.Page
    Dim objGeneral As clsGeneral

    Dim strNombreArchivo As String
    Dim strPathArchivo As String
    Dim strFila As String
    Dim ComandoSQL As SqlCommand
    Dim ComandoSQL2 As SqlCommand
    Dim stmStreamW As Stream
    Dim stwStreamWriter As StreamWriter
    Const CAMPO_NUMERICO As Integer = 1
    Const CODIGO_TIPO_ENTIDAD As Short = 1
    Const CAMPO_ALFANUMERICO As Integer = 2
    Const CAMPO_DECIMAL As Integer = 3
    Const CAMPO_FECHA As Integer = 4
    Const NOMBRE_MAESTRO_UIAF As String = "UIAF.txt"
    Const DIRECTORIO_ARCHIVOS_PLANOS As String = "Archivos_Ministerio_Transporte\"
    Const CARACTER_SALTO_LINEA As String = Chr(10)
    Const CAMPO_CEROS_DERECHA As Integer = 5
    Const CAMPO_CEROS_IZQUIERDA As Integer = 6
    Const CAMPO_FECHA_UIAF As Integer = 7
    Const CARACTER_ENTER As String = Chr(13)
    Const CAMPO_ALFANUMERICO_UIAF As Integer = 8
    Const CAMPO_VACIO_IZQUIERDA_UIAF As Integer = 9
    Const CAMPO_VACIO_DERECHA_UIAF As Integer = 10
    Const CODIGO_SECTOR_TRANSPORTE As Short = 15

    Const INICIO_REGISTRO_UIAF As String = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    Const FIN_REGISTRO_UIAF As String = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsNothing(Request.QueryString("UIAF")) Then
            objGeneral = New clsGeneral
            Call GenerarImpreso()
        End If

    End Sub
    Protected Sub GenerarImpreso()
        Dim strMensaje As String = ""
        Generar_Archivo_UIAF(strMensaje)

        If Len(Trim(strNombreArchivo)) > clsGeneral.CERO Then
            Dim strFiltro As String = "<script language='JavaScript'>window.open('../Tesoreria/DescargaArchivoUIAF.aspx?Archivo=" & strNombreArchivo & "&Mensaje=" & strMensaje & "')</script>"
            Me.ltrFiltro.Text = strFiltro
        End If

        'Response.Redirect("~\Contabilidad\GenerarInterfazContable.aspx?Archivo=" & strNombreArchivo & "&Mensaje=" & strMensaje)
    End Sub


    Private Sub Armar_Nombre_Archivo(ByRef strNombreArchivo As String, ByRef strPathArchivo As String)
        Try
            strPathArchivo = System.AppDomain.CurrentDomain.BaseDirectory() & DIRECTORIO_ARCHIVOS_PLANOS

            Dim strAnoMesDia As String

            strAnoMesDia = Date.Today.Year & FormCampMiniTran(Date.Today.Month, 2, CAMPO_NUMERICO) & FormCampMiniTran(Date.Today.Day, 2, CAMPO_NUMERICO)


            strNombreArchivo += strAnoMesDia & NOMBRE_MAESTRO_UIAF

            strNombreArchivo = strNombreArchivo.Replace("\", "")
            strPathArchivo += strNombreArchivo
            Me.objGeneral.ConexionSQL.Close()
        Catch ex As Exception
            Me.lblMensajeError.Visible = True
            Me.lblMensajeError.Text = "En la página " & Me.ToString & " se presentó un error en la función btnAceptar_Click: " & ex.Message.ToString()

        End Try
    End Sub

    Private Function Generar_Archivo_UIAF(ByRef Mensaje As String) As Boolean

        Dim Continuar As Boolean = True
        Dim strConsultaSql As String
        Dim lonNumeRegi As Long = 0, cont As Long = 1, lonNumeDest As Long = 0, lonValorViaje As Long = 0
        Dim lonNumeroDocumento As Long = 0
        Dim strFilaPiePagina As String = String.Empty
        Dim FilePath As String, strNumeRegi As String = String.Empty
        Dim strCodigoUIAF As String = String.Empty
        Dim strFilaEncabezado As String = String.Empty
        Dim CodigoRegionalMinisterio As String = String.Empty
        Dim strCodiEmprTran As String = String.Empty
        Dim dteFechFina As Date
        Dim dteFechaInicial As Date
        Dim dteFechaFinal As Date
        Dim objGeneralAux As New clsGeneral

        Try

            'Si el archivo existe se borra primero para evitar incongruencias
            Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
            FilePath = strPathArchivo
            If File.Exists(FilePath) Then
                File.Delete(FilePath)
            End If


            Dim arrCampos(2), arrValoresCampos(2) As String
            arrCampos(0) = "Codigo_UIAF"
            arrCampos(1) = "Codigo_Regional_Ministerio"
            arrCampos(2) = "Codigo_Ministerio"


            objGeneralAux.Retorna_Campos_Empresa(Val(Request.QueryString("Empresa")), arrCampos, arrValoresCampos, arrCampos.Count, "")
            strCodigoUIAF = arrValoresCampos(0)
            CodigoRegionalMinisterio = arrValoresCampos(1)
            strCodiEmprTran = arrValoresCampos(2)



            Date.TryParse(Request.QueryString("FechInic"), dteFechaInicial)
            Date.TryParse(Request.QueryString("FechFina"), dteFechaFinal)
            stmStreamW = System.IO.File.OpenWrite(FilePath)
            stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)



            ' Si existe se elimina y se vuelve a crear

            strConsultaSql = "  DECLARE @FECHA1 DATE  "
            strConsultaSql += " DECLARE @FECHA2 DATE  "
            strConsultaSql += " SET @FECHA1  = CONVERT(DATE,'" & dteFechaInicial.Month & "/" & dteFechaInicial.Day & "/" & dteFechaInicial.Year & "',101) "
            strConsultaSql += " SET @FECHA2  = CONVERT(DATE,'" & dteFechaFinal.Month & "/" & dteFechaFinal.Day & "/" & dteFechaFinal.Year & "',101) "

            strConsultaSql += " exec gsp_generar_consulta_SIPLAFT "

            strConsultaSql += Request.QueryString("Empresa") & ", "
            strConsultaSql += "@FECHA1, @FECHA2"

            'Me.objGeneral.ConexionSQL.Open()
            Dim connectionString As String = Me.objGeneral.ConexionSQL.ConnectionString
            Dim cnn As SqlConnection = New SqlConnection(connectionString)
            cnn.Open()
            Dim sqlAdapter As SqlDataAdapter
            sqlAdapter = New SqlDataAdapter(strConsultaSql, connectionString)

            Dim dsArchInte As New DataSet
            sqlAdapter.Fill(dsArchInte)

            If Not dsArchInte Is Nothing Then
                If Not dsArchInte.Tables(1) Is Nothing Then
                    strNumeRegi = dsArchInte.Tables(1).Rows(0)("CantidadRegistros").ToString
                End If
            End If

            strCodigoUIAF = FormCampMiniTran(strCodigoUIAF, 4, CAMPO_CEROS_IZQUIERDA)


            strFilaEncabezado = FormCampMiniTran(clsGeneral.CERO.ToString, 10, CAMPO_VACIO_IZQUIERDA_UIAF)
            strFilaEncabezado = strFilaEncabezado & CODIGO_SECTOR_TRANSPORTE &
                        FormCampMiniTran(CODIGO_TIPO_ENTIDAD, 3, CAMPO_CEROS_IZQUIERDA) &
                        FormCampMiniTran(strCodigoUIAF, 4, CAMPO_CEROS_IZQUIERDA)

            dteFechFina = Request.QueryString("FechFina")
            strFilaEncabezado = strFilaEncabezado & Format(dteFechFina, "yyyy-MM-dd") & FormCampMiniTran(strNumeRegi, 10, CAMPO_VACIO_DERECHA_UIAF)
            strFilaEncabezado = strFilaEncabezado & INICIO_REGISTRO_UIAF

            stwStreamWriter.WriteLine(strFilaEncabezado)

            Dim rowAux As Integer = 0
            For Each row As DataRow In dsArchInte.Tables(0).Rows
                Try
                    Dim IdentiRegistro As String = dsArchInte.Tables(0).Rows(rowAux)("ID").ToString()
                    strFila = FormCampMiniTran(IdentiRegistro, 10, CAMPO_VACIO_IZQUIERDA_UIAF)
                    strFila = strFila & FormCampMiniTran(dsArchInte.Tables(0).Rows(rowAux)("FechaComprobante").ToString(), 10, CAMPO_FECHA_UIAF)
                    lonNumeroDocumento = dsArchInte.Tables(0).Rows(rowAux)("NumeroManifiesto").ToString()
                    strFila = strFila & FormCampMiniTran(CodigoRegionalMinisterio & strCodiEmprTran & lonNumeroDocumento.ToString, 20, CAMPO_VACIO_IZQUIERDA_UIAF)

                    strFila = strFila & FormCampMiniTran(dsArchInte.Tables(0).Rows(rowAux)("CIUD_Origen").ToString(), 5, CAMPO_VACIO_IZQUIERDA_UIAF)
                    strFila = strFila & FormCampMiniTran(dsArchInte.Tables(0).Rows(rowAux)("CIUD_Destino").ToString(), 5, CAMPO_VACIO_IZQUIERDA_UIAF)
                    strFila = strFila & FormCampMiniTran(dsArchInte.Tables(0).Rows(rowAux)("Placa").trim.ToString(), 10, CAMPO_VACIO_DERECHA_UIAF)
                    strFila = strFila & dsArchInte.Tables(0).Rows(rowAux)("TipoIdenPropVehi").ToString()
                    strFila = strFila & FormCampMiniTran(dsArchInte.Tables(0).Rows(rowAux)("IdenPropVehi").ToString(), 20, CAMPO_VACIO_DERECHA_UIAF)
                    strFila = strFila & FormCampMiniTran(dsArchInte.Tables(0).Rows(rowAux)("NombPropVehi").trim.ToString(), 100, CAMPO_VACIO_DERECHA_UIAF)
                    strFila = strFila & dsArchInte.Tables(0).Rows(rowAux)("TipoIdenTeneVehi").ToString()
                    strFila = strFila & FormCampMiniTran(dsArchInte.Tables(0).Rows(rowAux)("IdenTeneVehi").ToString(), 20, CAMPO_VACIO_DERECHA_UIAF)
                    strFila = strFila & FormCampMiniTran(dsArchInte.Tables(0).Rows(rowAux)("NombTeneVehi").trim.ToString(), 100, CAMPO_VACIO_DERECHA_UIAF)
                    strFila = strFila & dsArchInte.Tables(0).Rows(rowAux)("TipoIdenCondVehi").ToString()
                    strFila = strFila & FormCampMiniTran(dsArchInte.Tables(0).Rows(rowAux)("IdenCondVehi").ToString(), 20, CAMPO_VACIO_DERECHA_UIAF)
                    strFila = strFila & FormCampMiniTran(dsArchInte.Tables(0).Rows(rowAux)("NombCondVehi").trim.ToString(), 100, CAMPO_VACIO_DERECHA_UIAF)
                    strFila = strFila & FormCampMiniTran(dsArchInte.Tables(0).Rows(rowAux)("PlacaSemirremolque").ToString(), 10, CAMPO_VACIO_DERECHA_UIAF)
                    strFila = strFila & FormCampMiniTran(dsArchInte.Tables(0).Rows(rowAux)("TipoIdenPropSemi").ToString(), 2, CAMPO_VACIO_IZQUIERDA_UIAF)
                    strFila = strFila & FormCampMiniTran(dsArchInte.Tables(0).Rows(rowAux)("IdenPropSemi").ToString(), 20, CAMPO_VACIO_IZQUIERDA_UIAF)
                    strFila = strFila & FormCampMiniTran(dsArchInte.Tables(0).Rows(rowAux)("NombPropSemi").trim.ToString(), 100, CAMPO_VACIO_DERECHA_UIAF)
                    strFila = strFila & FormCampMiniTran(dsArchInte.Tables(0).Rows(rowAux)("NombreProducto").trim.ToString(), 100, CAMPO_VACIO_DERECHA_UIAF)
                    strFila = strFila & dsArchInte.Tables(0).Rows(rowAux)("TipoIdenRemi").ToString()
                    strFila = strFila & FormCampMiniTran(dsArchInte.Tables(0).Rows(rowAux)("IdenRemi").ToString(), 20, CAMPO_VACIO_DERECHA_UIAF)
                    strFila = strFila & FormCampMiniTran(dsArchInte.Tables(0).Rows(rowAux)("NombreRemi").trim.ToString(), 100, CAMPO_VACIO_DERECHA_UIAF)
                    strFila = strFila & dsArchInte.Tables(0).Rows(rowAux)("TipoIdenDesti").ToString()
                    strFila = strFila & FormCampMiniTran(dsArchInte.Tables(0).Rows(rowAux)("IdenDesti").ToString(), 20, CAMPO_VACIO_DERECHA_UIAF)
                    strFila = strFila & FormCampMiniTran(dsArchInte.Tables(0).Rows(rowAux)("NombreDest").trim.ToString(), 100, CAMPO_VACIO_DERECHA_UIAF)

                    lonValorViaje = dsArchInte.Tables(0).Rows(rowAux)("ValorTotalViaje").ToString()
                    strFila = strFila & FormCampMiniTran(lonValorViaje, 20, CAMPO_DECIMAL)
                    strFila = strFila & FormCampMiniTran(dsArchInte.Tables(0).Rows(rowAux)("ValorPagadoEfectivo").ToString(), 20, CAMPO_DECIMAL)
                    strFila = strFila & FormCampMiniTran(dsArchInte.Tables(0).Rows(rowAux)("Observaciones").trim.ToString(), 100, CAMPO_VACIO_DERECHA_UIAF)

                    stwStreamWriter.WriteLine(strFila)

                    rowAux += 1
                Catch ex As Exception
                    lblMensajeError.Text = False
                    Return False
                End Try
            Next


            strFilaPiePagina = FormCampMiniTran(clsGeneral.CERO.ToString, 10, CAMPO_VACIO_IZQUIERDA_UIAF)
            strFilaPiePagina = strFilaPiePagina & CODIGO_SECTOR_TRANSPORTE &
                            FormCampMiniTran(CODIGO_TIPO_ENTIDAD, 3, CAMPO_CEROS_IZQUIERDA) &
                            FormCampMiniTran(strCodigoUIAF, 4, CAMPO_CEROS_IZQUIERDA)

            strFilaPiePagina = strFilaPiePagina & FormCampMiniTran(strNumeRegi, 10, CAMPO_VACIO_IZQUIERDA_UIAF)

            strFilaPiePagina = strFilaPiePagina & FIN_REGISTRO_UIAF

            stwStreamWriter.WriteLine(strFilaPiePagina)

            'sdrArchInte.Close()
            stwStreamWriter.Close()
            stmStreamW.Close()
            cnn.Close()
            objGeneral.ConexionSQL.Close()

            Return True
        Catch ex As Exception
            lblMensajeError.Text = False
            Return False
        End Try
    End Function

    Public Function FormCampMiniTran(ByVal strValor As String, ByVal intLongitud As Integer, ByVal intTipoCampo As Integer, Optional ByVal intPresicion As Integer = 0) As String
        FormCampMiniTran = String.Empty

        Try
            Dim intCon%, strFormato$, intPos%, strValorEntero$, strValorDecimal$, intLon%, strSignoNegativo$, intLongSignoNegativo%

            strValor = strValor.Replace(CARACTER_SALTO_LINEA, "")
            strValor = strValor.Replace(CARACTER_ENTER, "")
            strSignoNegativo = ""

            strFormato = ""
            strValor = Trim$(strValor)

            If intTipoCampo = CAMPO_CEROS_DERECHA Then
                strValor = Trim$(strValor)
                intLon = Len(strValor)

                For intCon = intLon To intLongitud - 1
                    strValor = strValor & "0"
                Next
                FormCampMiniTran = strValor

            ElseIf intTipoCampo = CAMPO_CEROS_IZQUIERDA Then
                strValor = Trim$(strValor)
                intLon = Len(strValor)

                For intCon = intLon To intLongitud - 1
                    strValor = "0" & strValor
                Next
                FormCampMiniTran = strValor

            ElseIf intTipoCampo = CAMPO_NUMERICO Then

                ' Si es vacio
                If strValor = "" Then
                    For intCon = 1 To intLongitud
                        strFormato = strFormato & "0"
                    Next
                    FormCampMiniTran = strFormato

                Else
                    ' Buscar signo negativo
                    intPos = InStr(strValor, "-")
                    If intPos > 0 Then
                        strSignoNegativo = "-"
                        intLongSignoNegativo = 1
                        strValor = Mid$(strValor, 2)
                    End If

                    For intCon = 1 To intLongitud - intLongSignoNegativo
                        strFormato = strFormato & "0"
                    Next

                    FormCampMiniTran = strSignoNegativo & Format$(Val(strValor), strFormato)

                End If

            ElseIf intTipoCampo = CAMPO_ALFANUMERICO Then

                If Len(strValor) > intLongitud Then
                    strValor = Mid$(strValor, 1, intLongitud)
                End If
                FormCampMiniTran = strValor & Space$(intLongitud - Len(strValor))

            ElseIf intTipoCampo = CAMPO_VACIO_DERECHA_UIAF Then

                If Len(strValor) > intLongitud Then
                    strValor = Mid$(strValor, 1, intLongitud)
                End If
                FormCampMiniTran = strValor & Space$(intLongitud - Len(strValor))

            ElseIf intTipoCampo = CAMPO_VACIO_IZQUIERDA_UIAF Then

                If Len(strValor) > intLongitud Then
                    strValor = Mid$(strValor, 1, intLongitud)
                End If
                FormCampMiniTran = Space$(intLongitud - Len(strValor)) & strValor


            ElseIf intTipoCampo = CAMPO_DECIMAL Then

                ' Si es vacio
                If strValor = "" Then
                    For intCon = 1 To intLongitud
                        strFormato = strFormato & "0"
                    Next
                    FormCampMiniTran = strFormato

                Else

                    ' Buscar signo negativo
                    intPos = InStr(strValor, "-")
                    If intPos > 0 Then
                        strSignoNegativo = "-"
                        intLongSignoNegativo = 1
                        strValor = Mid$(strValor, 2)
                    End If

                    ' Buscar signo decimal
                    intPos = InStr(strValor, ".")
                    If intPos > 0 Then
                        strValorEntero = Mid$(strValor, 1, intPos - 1)
                        strValorDecimal = Mid$(strValor, intPos + 1, intPresicion)
                    Else
                        strValorEntero = strValor
                        strValorDecimal = ""
                    End If
                    strValorEntero = Trim$(strValorEntero)
                    strValorDecimal = Trim$(strValorDecimal)

                    If strValorEntero = "" Then
                        strValorEntero = strFormato
                    Else
                        strValorEntero = Format$(Val(strValorEntero), strFormato)
                    End If

                    ' Formatear Decimal
                    strFormato = ""
                    intLon = Len(strValorDecimal)

                    strValorEntero = strValorEntero & Space$(intLongitud - Len(strValorEntero))

                    FormCampMiniTran = strValorEntero

                End If

            ElseIf intTipoCampo = CAMPO_FECHA Then
                FormCampMiniTran = Format$(CDate(strValor), "yyyy/MM/dd")
                'FormCampMiniTran = Format$(CDate(strValor), "DD/MM/YYYY")

            ElseIf intTipoCampo = CAMPO_FECHA_UIAF Then
                FormCampMiniTran = Format$(CDate(strValor), "yyyy-MM-dd")

            ElseIf intTipoCampo = CAMPO_ALFANUMERICO_UIAF Then

                If Len(strValor) > intLongitud Then
                    strValor = Mid$(strValor, 1, intLongitud)
                End If
                FormCampMiniTran = Space$(intLongitud - Len(strValor)) & strValor
            End If
        Catch ex As Exception
            Me.lblMensajeError.Visible = True
            Me.lblMensajeError.Text = "En la página " & Me.ToString & " se presentó un error en la función btnAceptar_Click: " & ex.Message.ToString()
        End Try
        Return FormCampMiniTran

    End Function





End Class