﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.Almacen
Imports EncoExpres.GesCarga.Negocio.Basico.Almacen

''' <summary>
''' Controlador <see cref="ReferenciasController"/>
''' </summary>
<Authorize>
Public Class ReferenciasController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Referencias) As Respuesta(Of IEnumerable(Of Referencias))
        Return New LogicaReferencias(CapaPersistenciaReferencias).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Referencias) As Respuesta(Of Referencias)
        Return New LogicaReferencias(CapaPersistenciaReferencias).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As Referencias) As Respuesta(Of Long)
        Return New LogicaReferencias(CapaPersistenciaReferencias).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As Referencias) As Respuesta(Of Boolean)
        Return New LogicaReferencias(CapaPersistenciaReferencias).Anular(entidad)
    End Function


End Class