﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.SeguridadUsuarios
Imports EncoExpres.GesCarga.Entidades.Despachos.OrdenCargue
Namespace Basico.Despachos
    ''' <summary>
    ''' Clase <see cref=" Precintos"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class GuiaPreimpresa
        Inherits BaseBasico
        Sub New()
        End Sub


        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Numero = Read(lector, "Numero")
            TAPR = New Catalogos With {.Nombre = Read(lector, "NombreProceso"), .Codigo = Read(lector, "CATA_TAPR_Codigo")}
            Oficina = New Oficinas With {.Nombre = Read(lector, "NombreOficina"), .Codigo = Read(lector, "OFIC_Codigo")}
            OficinaDestino = New Oficinas With {.Nombre = Read(lector, "NombreOficinaDestino"), .Codigo = Read(lector, "OFIC_Codigo_Destino")}
            Tipo_Presinto = New Catalogos With {.Nombre = Read(lector, "TipoPrecinto"), .Codigo = Read(lector, "CATA_TPRE_Codigo")}
            Fecha_Entrega = Read(lector, "Fecha_Entrega")
            Responsable = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Responsable"), .Nombre = Read(lector, "NombreResponsable"), .NumeroIdentificacion = Read(lector, "NumeroIdentificaionTercero")}
            PrecintoInicial = Read(lector, "Numero_Preimpreso_Inicial")
            PrecintoFinal = Read(lector, "Numero_Preimpreso_Final")
            Anulado = Read(lector, "Anulado")
            Usuario_Crea = New Usuarios With {.Codigo = Read(lector, "USUA_Codigo_Crea"), .Nombre = Read(lector, "NombreUsuaCrea")}
            Usuario_Modifica = New Usuarios With {.Codigo = Read(lector, "USUA_Modifica"), .Nombre = Read(lector, "NombreUsuaModifica")}

            If Obtener = 0 Then
                RegistrosPagina = Read(lector, "RegistrosPagina")
                TotalRegistros = Read(lector, "TotalRegistros")
            Else
                FechaCrea = Read(lector, "Fecha_Crea")
                Fecha_Anula = Read(lector, "Fecha_Anula")
                FechaModifica = Read(lector, "Fecha_Modifica")
                Usuario_Anula = New Usuarios With {.Codigo = Read(lector, "USUA_Codigo_Anula"), .Nombre = Read(lector, "NombreUsuaAnula")}
            End If
            ENPD_Numero = Read(lector, "ENPD_Numero")
            ENRE_Numero = Read(lector, "ENRE_Numero")
            ENMD_Numero = Read(lector, "ENMD_Numero")

            OrdenCargue = New OrdenCargue With {.Numero = Read(lector, "ENOC_Numero"), .NumeroDocumento = Read(lector, "OrdenCargue")}

            REM: Validaciones Precinto Disponible
            ValidaNumero = Read(lector, "Numero_Amarra_Precinto")
            ValidaNumeroDocumento = Read(lector, "Numero_Documento_Amarra_Precinto")
            ValidaTipoDocumento = Read(lector, "Tipo_Documento_Amarra_Precinto")

        End Sub
        <JsonProperty>
        Public Property NumerosPreimpresos As String
        <JsonProperty>
        Public Property OrdenCargue As OrdenCargue

        <JsonProperty>
        Public Property FechaInicial As Date
        <JsonProperty>
        Public Property FechaFinal As Date
        <JsonProperty>
        Public Property NumeroTranslados As Integer
        <JsonProperty>
        Public Property Numero As Double
        <JsonProperty>
        Public Property TAPR As Catalogos
        <JsonProperty>
        Public Property Oficina As Oficinas
        <JsonProperty>
        Public Property OficinaDestino As Oficinas
        <JsonProperty>
        Public Property Tipo_Presinto As Catalogos
        <JsonProperty>
        Public Property Fecha_Entrega As Date
        <JsonProperty>
        Public Property TempNumero As Double
        <JsonProperty>
        Public Property Responsable As Tercero
        <JsonProperty>
        Public Property TiempoEstimado As Double
        <JsonProperty>
        Public Property PrecintoInicial As Double
        <JsonProperty>
        Public Property PrecintoFinal As Double
        <JsonProperty>
        Public Property Anulado As Integer
        <JsonProperty>
        Public Property Fecha_Anula As DateTime
        <JsonProperty>
        Public Property Causa_Anula As String
        <JsonProperty>
        Public Property Usuario_Crea As Usuarios
        <JsonProperty>
        Public Property Usuario_Anula As Usuarios
        <JsonProperty>
        Public Property Usuario_Modifica As Usuarios
        <JsonProperty>
        Public Property Lista As IEnumerable(Of DetalleGuiaPreimpresa)
        <JsonProperty>
        Public Property ENPD_Numero As Long
        <JsonProperty>
        Public Property ENRE_Numero As Long
        <JsonProperty>
        Public Property ENMD_Numero As Long
#Region "Validacion Precintos"
        <JsonProperty>
        Public Property ValidaNumero As Long
        <JsonProperty>
        Public Property ValidaNumeroDocumento As Long
        <JsonProperty>
        Public Property ValidaTipoDocumento As Integer
#End Region

    End Class

End Namespace
