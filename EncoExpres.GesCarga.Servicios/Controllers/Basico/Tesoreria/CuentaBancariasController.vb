﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.Tesoreria
Imports EncoExpres.GesCarga.Negocio.Basico.Tesoreria

''' <summary>
''' Controlador <see cref="CuentaBancariasController"/>
''' </summary>
<Authorize>
Public Class CuentaBancariasController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As CuentaBancarias) As Respuesta(Of IEnumerable(Of CuentaBancarias))
        Return New LogicaCuentaBancarias(CapaPersistenciaCuentaBancarias, CapaPersistenciaCuentasBancariaOficinas).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As CuentaBancarias) As Respuesta(Of CuentaBancarias)
        Return New LogicaCuentaBancarias(CapaPersistenciaCuentaBancarias, CapaPersistenciaCuentasBancariaOficinas).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As CuentaBancarias) As Respuesta(Of Long)
        Return New LogicaCuentaBancarias(CapaPersistenciaCuentaBancarias, CapaPersistenciaCuentasBancariaOficinas).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As CuentaBancarias) As Respuesta(Of Boolean)
        Return New LogicaCuentaBancarias(CapaPersistenciaCuentaBancarias, CapaPersistenciaCuentasBancariaOficinas).Anular(entidad)
    End Function
    <HttpPost>
    <ActionName("ConsultarCuentaOficinas")>
    Public Function ConsultarCuentaOficinas(ByVal filtro As CuentaBancariaOficinas) As Respuesta(Of IEnumerable(Of CuentaBancariaOficinas))
        Return New LogicaCuentaBancarias(CapaPersistenciaCuentaBancarias, CapaPersistenciaCuentasBancariaOficinas).ConsultarCuentaOficinas(filtro)
    End Function

    <HttpPost>
    <ActionName("GuardarCuentaOficinas")>
    Public Function GuardarCuentaOficinas(ByVal detalle As IEnumerable(Of CuentaBancariaOficinas)) As Respuesta(Of Long)
        Return New LogicaCuentaBancarias(CapaPersistenciaCuentaBancarias, CapaPersistenciaCuentasBancariaOficinas).GuardarCuentaOficinas(detalle)
    End Function
    <HttpPost>
    <ActionName("CuentaBancariaOficinas")>
    Public Function CuentaBancariaOficinas(ByVal entidad As CuentaBancarias) As Respuesta(Of IEnumerable(Of CuentaBancariaOficinas))
        Return New LogicaCuentaBancarias(CapaPersistenciaCuentaBancarias, CapaPersistenciaCuentasBancariaOficinas).CuentaBancariaOficinas(entidad)
    End Function
End Class
