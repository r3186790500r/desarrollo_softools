﻿'Imports System.Data
'Imports System.Data.SqlClient
'Namespace Entidades.ReporteMinisterio

'    Public Class Ciudad

'#Region "Declaracion Variables"

'        Dim strSQL As String
'        Dim objGeneral As New General
'        Private objDepartamento As Departamento
'        Public strError As String
'        Public bolModificar As Boolean

'        Private objEmpresa As Empresas
'        Private objValidacion As Validacion

'        Private lonCodigo As Long
'        Private strCodigoAlterno As String
'        Private strCodificacionAlterna As String
'        Private strNombre As String
'        Private dblTarifaIca As Double

'        Private dblValorBaseIca As Double
'        Private intTieneSucursal As Byte
'        Private intIndicativoCiudad As Short
'        Private intRegionGeografica As Integer

'#End Region

'#Region "Declaracion Constantes"

'        Public Const TIENE_SUCURSAL As Byte = 1
'        Public Const CODIGO_CIUDAD_BUENAVENTURA As Integer = 76109
'        Public Const CODIGO_CIUDAD_MEDELLIN As Short = 5001
'        Public Const CODIGO_CIUDAD_ENVIGADO As Short = 5266
'        Public Const CODIGO_CIUDAD_CARTAGENA As Short = 13001

'        Public Const TARIFA_ICA_4_X_MIL As Byte = 4
'        Public Const TARIFA_ICA_7_X_MIL As Byte = 7
'        Const VALIDA_INGRESO_REGION As String = "clsCiudVali1"

'#End Region

'#Region "Constructor"

'        Sub New()

'            Me.objEmpresa = New Empresas(0)
'            Me.objDepartamento = New Departamento(Me.objEmpresa)
'            Me.objValidacion = New Validacion(Me.objEmpresa)
'            Me.lonCodigo = 0
'            Me.strCodigoAlterno = ""
'            Me.strCodificacionAlterna = ""
'            Me.strNombre = ""

'            Me.dblTarifaIca = 0
'            Me.dblValorBaseIca = 0
'            Me.intTieneSucursal = 0
'            Me.intRegionGeografica = 0

'        End Sub

'        Sub New(ByRef Empresa As Empresas)

'            Me.objEmpresa = Empresa
'            Me.objDepartamento = New Departamento(Me.objEmpresa)
'            Me.objValidacion = New Validacion(Me.objEmpresa)
'            Me.lonCodigo = 0
'            Me.strCodigoAlterno = ""
'            Me.strCodificacionAlterna = ""
'            Me.strNombre = ""

'            Me.dblTarifaIca = 0
'            Me.dblValorBaseIca = 0
'            Me.intTieneSucursal = 0
'            Me.intRegionGeografica = 0

'        End Sub

'#End Region

'#Region "Propiedades"

'        Public Property Codigo() As Long
'            Get
'                Return Me.lonCodigo
'            End Get
'            Set(ByVal value As Long)
'                Me.lonCodigo = value
'            End Set
'        End Property

'        Public Property TarifaIca() As Double
'            Get
'                Return Me.dblTarifaIca
'            End Get
'            Set(ByVal value As Double)
'                Me.dblTarifaIca = value
'            End Set
'        End Property

'        Public Property ValorBaseIca() As Double
'            Get
'                Return Me.dblValorBaseIca
'            End Get
'            Set(ByVal value As Double)
'                Me.dblValorBaseIca = value
'            End Set
'        End Property

'        Public Property Departamento() As Departamento
'            Get
'                Return Me.objDepartamento
'            End Get
'            Set(ByVal value As Departamento)
'                Me.objDepartamento = value
'            End Set
'        End Property

'        Public Property CodigoAlterno() As String
'            Get
'                Return Me.strCodigoAlterno
'            End Get
'            Set(ByVal value As String)
'                Me.strCodigoAlterno = value
'            End Set
'        End Property
'        Public Property CodificacionAlterna() As String
'            Get
'                Return Me.strCodificacionAlterna
'            End Get
'            Set(ByVal value As String)
'                Me.strCodificacionAlterna = value
'            End Set
'        End Property


'        Public Property Nombre() As String
'            Get
'                Return Me.strNombre
'            End Get
'            Set(ByVal value As String)
'                Me.strNombre = value
'            End Set
'        End Property

'        Public Property TieneSucursal() As Byte
'            Get
'                Return Me.intTieneSucursal
'            End Get
'            Set(ByVal value As Byte)
'                Me.intTieneSucursal = value
'            End Set
'        End Property

'        Public Property IndicativoCiudad() As Short
'            Get
'                Return Me.intIndicativoCiudad
'            End Get
'            Set(ByVal value As Short)
'                Me.intIndicativoCiudad = value
'            End Set
'        End Property

'        Public Property RegionGeografica() As Integer
'            Get
'                Return Me.intRegionGeografica
'            End Get
'            Set(ByVal value As Integer)
'                Me.intRegionGeografica = value
'            End Set
'        End Property
'#End Region

'#Region "Funciones Publicas"

'        Public Function Consultar() As Boolean
'            Try
'                Consultar = True
'                Dim sdrCiudad As SqlDataReader
'                Dim dblAuxiliar As Double
'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT Nombre, Codificacion_Alterna, Codigo_Alterno, DEPA_Codigo, Tarifa_ICA, Valor_Base_ICA, Tiene_Sucursal, Indicativo, REGE_Codigo FROM Ciudades"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()
'                sdrCiudad = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrCiudad.Read() Then
'                    Me.strNombre = sdrCiudad("Nombre").ToString()
'                    Me.strCodificacionAlterna = sdrCiudad("Codificacion_Alterna").ToString()
'                    Me.strCodigoAlterno = sdrCiudad("Codigo_Alterno").ToString()
'                    Me.objDepartamento.Codigo = Val(sdrCiudad("DEPA_Codigo").ToString())
'                    Double.TryParse(sdrCiudad("Tarifa_ICA").ToString(), dblAuxiliar)
'                    Me.dblTarifaIca = dblAuxiliar
'                    Me.dblValorBaseIca = Val(sdrCiudad("Valor_Base_ICA").ToString())
'                    Me.intTieneSucursal = Val(sdrCiudad("Tiene_Sucursal").ToString())
'                    Me.intIndicativoCiudad = Val(sdrCiudad("Indicativo").ToString())
'                    Me.intRegionGeografica = Val(sdrCiudad("REGE_Codigo").ToString())
'                    Consultar = True
'                Else
'                    Me.lonCodigo = 0
'                    Consultar = False
'                End If

'                sdrCiudad.Close()
'                sdrCiudad.Dispose()

'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & ex.Message.ToString()
'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Consultar(ByRef Empresa As Empresas, ByVal Codigo As Long) As Boolean
'            Try
'                Consultar = True
'                Me.objEmpresa = Empresa
'                Me.lonCodigo = Codigo

'                Dim sdrCiudad As SqlDataReader
'                Dim dblAuxiliar As Double
'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT Nombre, Codificacion_Alterna, Codigo_Alterno, DEPA_Codigo, Tarifa_ICA, Valor_Base_ICA, Tiene_Sucursal, Indicativo, REGE_Codigo FROM Ciudades"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()
'                sdrCiudad = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrCiudad.Read() Then
'                    Me.strNombre = sdrCiudad("Nombre").ToString()
'                    Me.strCodificacionAlterna = sdrCiudad("Codificacion_Alterna").ToString()
'                    Me.strCodigoAlterno = sdrCiudad("Codigo_Alterno").ToString()
'                    Me.objDepartamento.Codigo = Val(sdrCiudad("DEPA_Codigo").ToString())
'                    Double.TryParse(sdrCiudad("Tarifa_ICA").ToString(), dblAuxiliar)
'                    Me.dblTarifaIca = dblAuxiliar
'                    Me.dblValorBaseIca = Val(sdrCiudad("Valor_Base_ICA").ToString())
'                    Me.intTieneSucursal = Val(sdrCiudad("Tiene_Sucursal").ToString())
'                    Me.intIndicativoCiudad = Val(sdrCiudad("Indicativo").ToString())
'                    Me.intRegionGeografica = Val(sdrCiudad("REGE_Codigo").ToString())
'                    Consultar = True
'                Else
'                    Me.lonCodigo = 0
'                    Consultar = False
'                End If

'                sdrCiudad.Close()
'                sdrCiudad.Dispose()

'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & ex.Message.ToString()
'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Cargar_Objeto(ByRef Empresa As Empresas, ByVal Codigo As Long) As Boolean
'            Try
'                Cargar_Objeto = True

'                Me.objEmpresa = Empresa
'                Me.lonCodigo = Codigo

'                Dim sdrCiudad As SqlDataReader
'                Dim dblAuxiliar As Double
'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT Nombre, Codigo_Alterno, DEPA_Codigo, Tarifa_ICA, Valor_Base_ICA, Tiene_Sucursal, Indicativo, REGE_Codigo FROM Ciudades"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()
'                sdrCiudad = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrCiudad.Read() Then
'                    Me.strNombre = sdrCiudad("Nombre").ToString()
'                    Me.strCodigoAlterno = sdrCiudad("Codigo_Alterno").ToString()
'                    Me.objDepartamento.Codigo = Val(sdrCiudad("DEPA_Codigo").ToString())
'                    Double.TryParse(sdrCiudad("Tarifa_ICA").ToString(), dblAuxiliar)
'                    Me.dblTarifaIca = dblAuxiliar
'                    Me.dblValorBaseIca = Val(sdrCiudad("Valor_Base_ICA").ToString())
'                    Me.intTieneSucursal = Val(sdrCiudad("Tiene_Sucursal").ToString())
'                    Me.intIndicativoCiudad = Val(sdrCiudad("Indicativo").ToString())
'                    Me.intRegionGeografica = Val(sdrCiudad("REGE_Codigo").ToString())
'                    Cargar_Objeto = True
'                Else
'                    Me.lonCodigo = 0
'                    Cargar_Objeto = False
'                End If

'                sdrCiudad.Close()
'                sdrCiudad.Dispose()

'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Cargar_Objeto: " & ex.Message.ToString()
'                Cargar_Objeto = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Datos_Existe_Registro(ByRef Empresa As Empresas, ByVal Codigo As Long) As Boolean

'            Try
'                Dim sdrCiudad As SqlDataReader
'                Me.objEmpresa = Empresa
'                Me.lonCodigo = Codigo

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo, Nombre, Indicativo FROM Ciudades"
'                strSQL += " WHERE EMPR_Codigo = " & objEmpresa.Codigo
'                strSQL += " AND Codigo = " & lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Datos_Existe_Registro = True
'                Me.objGeneral.ConexionSQL.Open()
'                sdrCiudad = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrCiudad.Read() Then
'                    Me.strNombre = sdrCiudad("Nombre").ToString()
'                    Me.intIndicativoCiudad = Val(sdrCiudad("Indicativo").ToString())
'                    Datos_Existe_Registro = True
'                Else
'                    Me.lonCodigo = 0
'                    Datos_Existe_Registro = False
'                End If

'                sdrCiudad.Close()
'                sdrCiudad.Dispose()

'            Catch ex As Exception

'                Me.lonCodigo = 0
'                Datos_Existe_Registro = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Datos_Existe_Registro: " & ex.Message.ToString()

'            Finally

'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If

'            End Try

'        End Function

'        Public Function Existe_Registro() As Boolean

'            Try
'                Dim sdrCiudad As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo, Nombre FROM Ciudades"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Existe_Registro = True
'                Me.objGeneral.ConexionSQL.Open()
'                sdrCiudad = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrCiudad.Read() Then
'                    Me.strNombre = sdrCiudad("Nombre").ToString()
'                    Existe_Registro = True
'                Else
'                    Me.lonCodigo = 0
'                    Existe_Registro = False
'                End If

'                sdrCiudad.Close()
'                sdrCiudad.Dispose()

'            Catch ex As Exception

'                Me.lonCodigo = 0
'                Existe_Registro = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Existe_Registro: " & ex.Message.ToString()

'            Finally

'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If

'            End Try

'        End Function

'        Public Function Existe_Registro(ByRef Empresa As Empresas, ByVal Codigo As Long) As Boolean

'            Try

'                Dim sdrCiudad As SqlDataReader
'                Dim ComandoSQL As SqlCommand
'                Me.objEmpresa = Empresa
'                Me.lonCodigo = Codigo

'                strSQL = "SELECT EMPR_Codigo, Nombre, Tiene_Sucursal, Indicativo FROM Ciudades"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Existe_Registro = True
'                Me.objGeneral.ConexionSQL.Open()
'                sdrCiudad = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrCiudad.Read() Then
'                    Me.strNombre = sdrCiudad("Nombre").ToString()
'                    Me.TieneSucursal = Val(sdrCiudad("Tiene_Sucursal").ToString())
'                    Me.intIndicativoCiudad = Val(sdrCiudad("Indicativo"))

'                    Existe_Registro = True
'                Else
'                    Me.lonCodigo = 0
'                    Existe_Registro = False
'                End If

'                sdrCiudad.Close()
'                sdrCiudad.Dispose()

'            Catch ex As Exception

'                Me.lonCodigo = 0
'                Existe_Registro = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Existe_Registro: " & ex.Message.ToString()

'            Finally

'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If

'            End Try
'        End Function

'        Public Function Guardar(Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                If Datos_Requeridos(Mensaje) Then
'                    If Not bolModificar Then
'                        Guardar = Insertar_SQL()
'                    Else
'                        Guardar = Modificar_SQL()
'                    End If
'                Else
'                    Guardar = False
'                End If
'                Mensaje = Me.strError
'            Catch ex As Exception
'                Guardar = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Guardar: " & ex.Message.ToString()
'            End Try
'        End Function

'        Public Function Eliminar(Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                Eliminar = Borrar_SQL()
'                Mensaje = Me.strError
'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Eliminar: " & ex.Message.ToString()
'                Eliminar = False
'            End Try
'        End Function

'#End Region

'#Region "Funciones Privadas"

'        Private Function Datos_Requeridos(Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                Me.strError = ""
'                Dim intCont As Integer = 0
'                Datos_Requeridos = True

'                If Me.lonCodigo = 0 Then
'                    intCont += 1
'                    strError += intCont & ". Digite un código válido para la ciudad"
'                    strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.strNombre = "" Then
'                    intCont += 1
'                    strError += intCont & ". Digite el nombre de la ciudad"
'                    strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Val(Me.strCodigoAlterno) = General.CERO Then
'                    intCont += 1
'                    strError += intCont & ". Debe indicar un Código de Ministerio"
'                    strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.dblTarifaIca > 100 Then
'                    intCont += 1
'                    strError += intCont & ". La tarifa ICA de la ciudad debe ser un número menor a 100"
'                    strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.dblTarifaIca < 0 Then
'                    intCont += 1
'                    strError += intCont & ". La tarifa ICA no puede ser un número negativo"
'                    strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.strNombre <> "" And Len(Me.strNombre) < General.MINIMO_CUATRO_CARACTERES_INGRESO_NOMBRE Then
'                    intCont += 1
'                    strError += intCont & ". El nombre de la ciudad no puede tener menos de cuatro (4) carácteres"
'                    strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_INGRESO_REGION) Then
'                    If Me.objValidacion.Valida Then
'                        If Me.intRegionGeografica = General.CERO Then
'                            intCont += 1
'                            strError += intCont & ". Seleccione la Región Geográfica."
'                            strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If
'                    End If
'                End If

'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Datos_Requeridos: " & ex.Message.ToString()
'                Datos_Requeridos = False
'            End Try

'        End Function

'        Private Function Borrar_SQL() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()
'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_borrar_ciudades", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure
'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_Codigo").Value = Me.lonCodigo

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Borrar_SQL = True
'                Else
'                    Borrar_SQL = False
'                End If

'            Catch ex As Exception
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Borrar_SQL: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Borrar_SQL = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Insertar_SQL() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_insertar_ciudades", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Decimal, 10) : ComandoSQL.Parameters("@par_Codigo").Value = Me.lonCodigo
'                ComandoSQL.Parameters.Add("@par_Codificacion_Alterna", SqlDbType.VarChar, 10) : ComandoSQL.Parameters("@par_Codificacion_Alterna").Value = Me.strCodificacionAlterna
'                ComandoSQL.Parameters.Add("@par_Codigo_Alterno", SqlDbType.VarChar, 10) : ComandoSQL.Parameters("@par_Codigo_Alterno").Value = Me.strCodigoAlterno
'                ComandoSQL.Parameters.Add("@par_Nombre", SqlDbType.VarChar, 40) : ComandoSQL.Parameters("@par_Nombre").Value = Me.strNombre
'                ComandoSQL.Parameters.Add("@par_DEPA_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_DEPA_Codigo").Value = Me.objDepartamento.Codigo
'                ComandoSQL.Parameters.Add("@par_Tarifa_ICA", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Tarifa_ICA").Value = Me.dblTarifaIca
'                ComandoSQL.Parameters.Add("@par_Valor_Base_ICA", SqlDbType.Money) : ComandoSQL.Parameters("@par_Valor_Base_ICA").Value = Me.dblValorBaseIca
'                ComandoSQL.Parameters.Add("@par_Tiene_Sucursal", SqlDbType.Int) : ComandoSQL.Parameters("@par_Tiene_Sucursal").Value = Me.intTieneSucursal
'                ComandoSQL.Parameters.Add("@par_Indicativo", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Indicativo").Value = Me.intIndicativoCiudad
'                ComandoSQL.Parameters.Add("@par_REGE_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_REGE_Codigo").Value = Me.intRegionGeografica

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Insertar_SQL = True
'                Else
'                    Insertar_SQL = False
'                End If

'            Catch ex As Exception
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Insertar_SQL: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Insertar_SQL = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Modificar_SQL() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_modificar_ciudades", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Codigo").Value = Me.lonCodigo
'                ComandoSQL.Parameters.Add("@par_Codificacion_Alterna", SqlDbType.VarChar, 10) : ComandoSQL.Parameters("@par_Codificacion_Alterna").Value = Me.strCodificacionAlterna
'                ComandoSQL.Parameters.Add("@par_Codigo_Alterno", SqlDbType.VarChar, 10) : ComandoSQL.Parameters("@par_Codigo_Alterno").Value = Me.strCodigoAlterno
'                ComandoSQL.Parameters.Add("@par_Nombre", SqlDbType.VarChar, 40) : ComandoSQL.Parameters("@par_Nombre").Value = Me.strNombre
'                ComandoSQL.Parameters.Add("@par_DEPA_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_DEPA_Codigo").Value = Me.objDepartamento.Codigo
'                ComandoSQL.Parameters.Add("@par_Tarifa_ICA", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Tarifa_ICA").Value = Me.dblTarifaIca
'                ComandoSQL.Parameters.Add("@par_Valor_Base_ICA", SqlDbType.Money) : ComandoSQL.Parameters("@par_Valor_Base_ICA").Value = Me.dblValorBaseIca
'                ComandoSQL.Parameters.Add("@par_Tiene_Sucursal", SqlDbType.Int) : ComandoSQL.Parameters("@par_Tiene_Sucursal").Value = Me.intTieneSucursal
'                ComandoSQL.Parameters.Add("@par_Indicativo", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Indicativo").Value = Me.intIndicativoCiudad
'                ComandoSQL.Parameters.Add("@par_REGE_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_REGE_Codigo").Value = Me.intRegionGeografica

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Modificar_SQL = True
'                Else
'                    Modificar_SQL = False
'                End If

'            Catch ex As Exception
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Modificar_SQL: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Modificar_SQL = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'#End Region

'    End Class

'End Namespace
