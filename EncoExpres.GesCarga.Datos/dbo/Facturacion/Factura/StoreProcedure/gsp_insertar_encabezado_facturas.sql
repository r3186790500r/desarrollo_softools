﻿
Print 'CREATE PROCEDURE gsp_insertar_encabezado_facturas'
GO
DROP PROCEDURE gsp_insertar_encabezado_facturas
GO

CREATE PROCEDURE gsp_insertar_encabezado_facturas (  

@par_EMPR_Codigo      smallint,  
@par_TIDO_Codigo      numeric,  
@par_Numero_Preimpreso     varchar(20),  
@par_CATA_TIFA_Codigo     numeric,  
@par_Fecha        date,  
@par_OFIC_Factura      numeric,  
@par_TERC_Cliente      numeric,  
@par_TERC_Facturar      numeric,  
@par_CATA_FPVE_Codigo     numeric,  
@par_Dias_Plazo       smallint,  
@par_Observaciones      varchar(500),  
@par_Valor_Remesas      money,  
@par_Valor_Otros_Conceptos    money,  
@par_Valor_Descuentos     money,  
@par_Subtotal       money,  
@par_Valor_Impuestos     money,  
@par_Valor_Factura      money,  
@par_Valor_TRM       money,  
@par_MONE_Codigo_Alterna    numeric,  
@par_Valor_Moneda_Alterna    money,  
@par_Valor_Anticipo      money,  
@par_Resolucion_Facturacion    varchar(500),  
@par_Control_Impresion     smallint,  
@par_Estado        smallint,  
@par_USUA_Codigo_Crea     smallint,  
@par_Anulado       smallint  


)  

AS  
BEGIN  

DECLARE @numNumeroFactura NUMERIC = 0  

--Consumo Prefactura  
IF ISNULL(@par_Estado,0) = 0 BEGIN  
SET @par_TIDO_Codigo = 175 --> Prefactura  
END  

EXEC gsp_generar_consecutivo @par_EMPR_Codigo, @par_TIDO_Codigo, @par_OFIC_Factura, @numNumeroFactura OUTPUT  


INSERT INTO Encabezado_Facturas (  
EMPR_Codigo,  
TIDO_Codigo,  
Numero_Documento,  
Numero_Preimpreso,  
CATA_TIFA_Codigo, 
Fecha,  
OFIC_Factura,  
TERC_Cliente,  
TERC_Facturar,  
CATA_FPVE_Codigo,  
Dias_Plazo,  
Observaciones,  
Valor_Remesas,  
Valor_Otros_Conceptos,  
Valor_Descuentos,  
Subtotal,  
Valor_Impuestos,  
Valor_Factura,  
Valor_TRM,  
MONE_Codigo_Alterna,  
Valor_Moneda_Alterna,  
Valor_Anticipo,  
Resolucion_Facturacion,  
Control_Impresion,  
Estado,  
Fecha_Crea,  
USUA_Codigo_Crea,  
Anulado,  
Factura_Electronica  

)  

VALUES(  
@par_EMPR_Codigo,  
@par_TIDO_Codigo,  
@numNumeroFactura,  
@par_Numero_Preimpreso, 
@par_CATA_TIFA_Codigo,
@par_Fecha,  
@par_OFIC_Factura,  
@par_TERC_Cliente,  
@par_TERC_Facturar,  
@par_CATA_FPVE_Codigo,  
@par_Dias_Plazo,  
@par_Observaciones,  
@par_Valor_Remesas,  
@par_Valor_Otros_Conceptos,  
@par_Valor_Descuentos,  
@par_Subtotal,  
@par_Valor_Impuestos,  
@par_Valor_Factura,  
@par_Valor_TRM,  
@par_MONE_Codigo_Alterna,  
@par_Valor_Moneda_Alterna,  
@par_Valor_Anticipo,  
@par_Resolucion_Facturacion,  
@par_Control_Impresion,  
@par_Estado,  
GETDATE(),  
@par_USUA_Codigo_Crea,  
@par_Anulado,  
0  
)  

SELECT Numero, Numero_Documento FROM Encabezado_Facturas   
WHERE EMPR_Codigo = @par_EMPR_Codigo  
AND Numero_Documento = @numNumeroFactura  
AND TIDO_Codigo = @par_TIDO_Codigo  
AND CATA_TIFA_Codigo = @par_CATA_TIFA_Codigo

END  
GO
