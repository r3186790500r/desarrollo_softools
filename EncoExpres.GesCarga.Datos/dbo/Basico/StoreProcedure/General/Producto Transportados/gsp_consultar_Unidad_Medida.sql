﻿PRINT 'gsp_consultar_Unidad_Medida'
GO
DROP PROCEDURE gsp_consultar_Unidad_Medida
GO
CREATE PROCEDURE gsp_consultar_Unidad_Medida  
(  
@par_EMPR_Codigo SMALLINT,  
@par_Codigo SMALLINT = NULL
)  
AS  
BEGIN  
 SELECT  
  UMPT.EMPR_Codigo,  
  UMPT.Codigo,  
  ISNULL(UMPT.Nombre_Corto,'') AS Nombre_Corto,  
  ISNULL(UMPT.Descripcion,'') AS Descripcion,  
  ISNULL(UMPT.Estado,'') AS Estado
 FROM  
  Unidad_Medida_Producto_Transportados AS UMPT

 WHERE  
  UMPT.EMPR_Codigo = @par_EMPR_Codigo
  AND UMPT.Codigo = ISNULL(@par_Codigo, UMPT.Codigo)  
END  
GO