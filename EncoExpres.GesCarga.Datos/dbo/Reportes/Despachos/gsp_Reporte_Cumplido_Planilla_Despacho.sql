﻿PRINT 'gsp_Reporte_Cumplido_Planilla_Despacho'
GO
DROP PROCEDURE gsp_Reporte_Cumplido_Planilla_Despacho
GO
CREATE PROCEDURE gsp_Reporte_Cumplido_Planilla_Despacho 
(@par_EMPR_Codigo NUMERIC,    
@par_Numero NUMERIC )    
AS BEGIN     
SELECT     
EMPR.Nombre_Razon_Social     
,USCR.Codigo_Usuario UsuarioCrea       
,EMPR.Numero_Identificacion IDENTIFICACIONEMPRESA    
,EMPR.Direccion DIREEMPRESA    
,EMPR.Telefonos TELEEMPRESA    
,ENCU.Numero_Documento NumeroCumplido    
,ENCU.FECHA FechaCumplido    
,ENPD.Numero_Documento NumeroPlanilla
,ENMC.Numero_Documento NumeroManifiesto
,VEHI.Placa vehiculo    
,TENE.Numero_Identificacion IdentificacionTenedor    
, ISNULL(TENE.Razon_Social,'')+' '+ISNULL(TENE.Nombre,'')          
+ ' '+ISNULL(TENE.Apellido1,'')+' '+ISNULL(TENE.Apellido2,'') AS NombreTenedor        
,TENE.Numero_Identificacion IdentificacionConductor    
, ISNULL(TECO.Razon_Social,'')+' '+ISNULL(TECO.Nombre,'')          
+ ' '+ISNULL(TECO.Apellido1,'')+' '+ISNULL(TECO.Apellido2,'') AS NombreConductor      
,ENCU.FECHA_ENTREGA     
,ENCU.VALOR_MULTA_EXTEMPORANEO     
,ENCU.NOMBRE_ENTREGO_CUMPLIDO     
,ENCU.OBSERVACIONES     
    
FROM Encabezado_Cumplido_Planilla_Despachos ENCU    
    
INNER JOIN Empresas EMPR    
ON ENCU.EMPR_Codigo = EMPR.Codigo    
    
LEFT JOIN Encabezado_Planilla_Despachos ENPD ON     
ENCU.EMPR_Codigo = ENPD.EMPR_Codigo     
AND ENCU.ENPD_Numero = ENPD.Numero

LEFT JOIN Encabezado_Manifiesto_Carga ENMC ON     
ENPD.EMPR_Codigo = ENMC.EMPR_Codigo     
AND ENPD.ENMC_Numero = ENMC.Numero
  
LEFT JOIN Usuarios USCR    
ON  ENCU.EMPR_Codigo = USCR.EMPR_Codigo    
AND ENCU.USUA_Codigo_Crea = USCR.Codigo        
    
LEFT JOIN Vehiculos VEHI ON     
ENPD.EMPR_Codigo = VEHI.EMPR_Codigo     
AND ENPD.VEHI_Codigo = VEHI.Codigo    
    
INNER JOIN TERCEROS TENE ON     
ENPD.EMPR_Codigo = TENE.EMPR_Codigo     
AND ENPD.TERC_Codigo_Tenedor = TENE.Codigo     
    
INNER JOIN TERCEROS TECO ON     
ENPD.EMPR_Codigo = TECO.EMPR_Codigo     
AND ENPD.TERC_Codigo_Conductor = TECO.Codigo     
WHERE     
ENCU.EMPR_CODIGO= @PAR_EMPR_CODIGO     
AND ENCU.NUMERO = @PAR_NUMERO    
END     
GO