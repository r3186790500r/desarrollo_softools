﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Despachos.Paqueteria
Imports System.Transactions
Imports EncoExpres.GesCarga.Entidades.Despachos.Remesa
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Comercial.Documentos

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="RepositorioRecolecciones"/>
    ''' </summary>
    Public NotInheritable Class RepositorioRecolecciones
        Inherits RepositorioBase(Of Recolecciones)

        Public Overrides Function Consultar(filtro As Recolecciones) As IEnumerable(Of Recolecciones)
            Dim lista As New List(Of Recolecciones)
            Dim item As Recolecciones

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If

                If filtro.NumeroDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero_Documento", filtro.NumeroDocumento)
                End If

                If filtro.FechaInicio > DateTime.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicio, SqlDbType.DateTime)
                End If

                If filtro.FechaFin > DateTime.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFin, SqlDbType.DateTime)
                End If

                If Not IsNothing(filtro.Oficinas) Then
                    If filtro.Oficinas.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficinas.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Ciudad) Then
                    If filtro.Ciudad.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo", filtro.Ciudad.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Zonas) Then
                    If filtro.Zonas.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_ZOCI_Codigo", filtro.Zonas.Codigo)
                    End If

                    If filtro.Zonas.Nombre <> "" Then
                        conexion.AgregarParametroSQL("@par_ZOCI_Nombre", filtro.Zonas.Nombre)
                    End If


                End If

                If Not String.IsNullOrWhiteSpace(filtro.NombreZona) Then
                    conexion.AgregarParametroSQL("@par_Nombre_Zona", filtro.NombreZona)
                End If

                If Not String.IsNullOrWhiteSpace(filtro.Barrio) Then
                    conexion.AgregarParametroSQL("@par_Nombre_Barrio", filtro.Barrio)
                End If

                If Not IsNothing(filtro.NombreCliente) Then
                    conexion.AgregarParametroSQL("@par_Nombre_Cliente", filtro.NombreCliente)
                End If

                If Not IsNothing(filtro.Cliente) Then
                    If filtro.Cliente.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo_Cliente", filtro.Cliente.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.NombreRemitente) Then
                    conexion.AgregarParametroSQL("@par_Nombre_Remitente", filtro.NombreRemitente)
                End If
                If Not IsNothing(filtro.Remitente) Then
                    If filtro.Remitente.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo_Remitente", filtro.Remitente.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.OficinaGestiona) Then
                    If filtro.OficinaGestiona.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Oficina_Gestiona", filtro.OficinaGestiona.Codigo)
                    End If
                End If
                If filtro.Estado = -1 Then
                    conexion.AgregarParametroSQL("@par_Planillado", -1)
                End If
                If filtro.Estado = 1 Then
                    conexion.AgregarParametroSQL("@par_Estado", 1)
                    conexion.AgregarParametroSQL("@par_Anulado", 0)
                    conexion.AgregarParametroSQL("@par_Planillado", 0)
                End If
                If filtro.Estado = 2 Then
                    conexion.AgregarParametroSQL("@par_Estado", 1)
                    conexion.AgregarParametroSQL("@par_Anulado", 0)
                    conexion.AgregarParametroSQL("@par_Planillado", 1)
                End If
                If filtro.Estado = 3 Then
                    conexion.AgregarParametroSQL("@par_Estado", 1)
                    conexion.AgregarParametroSQL("@par_Anulado", 1)
                    conexion.AgregarParametroSQL("@par_Planillado", 0)
                End If


                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If

                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If
                If filtro.AsociarRecoleccionPlanilla = 1 Then
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_recolecciones_pendientes_asociar")
                    While resultado.Read
                        item = New Recolecciones(resultado)
                        lista.Add(item)
                    End While
                ElseIf filtro.Planilla > 0 Then
                    conexion.AgregarParametroSQL("@par_ENPR_Numero", filtro.Planilla)
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_recolecciones_planilla")
                    While resultado.Read
                        item = New Recolecciones(resultado)
                        lista.Add(item)
                    End While
                Else
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_recolecciones")
                    While resultado.Read
                        item = New Recolecciones(resultado)
                        lista.Add(item)
                    End While
                End If


            End Using

            Return lista
        End Function

        Public Function ConsultarENCOE(filtro As Recolecciones) As IEnumerable(Of Recolecciones)
            Dim lista As New List(Of Recolecciones)
            Dim item As Recolecciones

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If

                If filtro.NumeroDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero_Documento", filtro.NumeroDocumento)
                End If

                If filtro.FechaInicio > DateTime.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicio, SqlDbType.Date)
                End If

                If filtro.FechaFin > DateTime.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFin, SqlDbType.Date)
                End If

                If Not IsNothing(filtro.Oficinas) Then
                    If filtro.Oficinas.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficinas.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Ciudad) Then
                    If filtro.Ciudad.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo", filtro.Ciudad.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Zonas) Then
                    If filtro.Zonas.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_ZOCI_Codigo", filtro.Zonas.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Cliente) Then
                    If filtro.Cliente.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo_Cliente", filtro.Cliente.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Remitente) Then
                    If filtro.Remitente.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo_Remitente", filtro.Remitente.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.OficinaGestiona) Then
                    If filtro.OficinaGestiona.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Oficina_Gestiona", filtro.OficinaGestiona.Codigo)
                    End If
                End If

                If filtro.Estado > -1 And filtro.Estado < 2 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If

                If filtro.Estado = 2 Then
                    conexion.AgregarParametroSQL("@par_Anulado", SI_APLICA)
                End If

                If Not IsNothing(filtro.EstadoRecoleccion) Then
                    If filtro.EstadoRecoleccion.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_CATA_EREP_Codigo", filtro.EstadoRecoleccion.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If

                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_recolecciones_v2")
                While resultado.Read
                    item = New Recolecciones(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Function ConsultarRecoleccionesPorPlanillar(filtro As Recolecciones) As IEnumerable(Of Recolecciones)
            Dim lista As New List(Of Recolecciones)
            Dim item As Recolecciones

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If

                If filtro.NumeroDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero_Documento", filtro.NumeroDocumento)
                End If

                If filtro.FechaInicio > DateTime.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicio, SqlDbType.Date)
                End If

                If filtro.FechaFin > DateTime.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFin, SqlDbType.Date)
                End If

                If Not IsNothing(filtro.Ciudad) Then
                    If filtro.Ciudad.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo", filtro.Ciudad.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Zonas) Then
                    If filtro.Zonas.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_ZOCI_Codigo", filtro.Zonas.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Cliente) Then
                    If filtro.Cliente.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo_Cliente", filtro.Cliente.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.OficinaGestiona) Then
                    If filtro.OficinaGestiona.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Oficina_Gestiona", filtro.OficinaGestiona.Codigo)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_recolecciones_por_planillar")
                While resultado.Read
                    item = New Recolecciones(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As Recolecciones) As Long
            Dim tercero = New Repositorio.Basico.General.RepositorioTerceros
            If entidad.Version1 = 0 Then
                'Insertar Remitente/Cliente
                If Not IsNothing(entidad.Remitente) Then
                    'Valida Documento Remitente
                    If entidad.Remitente.Codigo = 0 And Not IsNothing(entidad.Remitente.NumeroIdentificacion) Then
                        Dim Remi As Terceros = New Terceros With {.CodigoEmpresa = entidad.CodigoEmpresa, .NumeroIdentificacion = entidad.Remitente.NumeroIdentificacion}
                        Dim listaRemi = New List(Of Terceros)
                        listaRemi = tercero.Consultar(Remi)
                        If listaRemi.Count > 0 Then
                            entidad.Remitente.Codigo = listaRemi(0).Codigo
                        End If
                    End If


                    If entidad.Remitente.Codigo = 0 Then
                        If Not IsNothing(entidad.Remitente.NumeroIdentificacion) Then
                            entidad.Remitente.CodigoEmpresa = entidad.CodigoEmpresa
                            entidad.Remitente.UsuaCodigoCrea = entidad.UsuarioCrea.Codigo
                            entidad.Remitente.CadenaPerfiles = "1404,1410,"
                            entidad.Remitente.Estado = New Estado With {.Codigo = 1}
                            If entidad.Remitente.TipoIdentificacion.Codigo = 102 Then
                                'NIT
                                entidad.Remitente.RazonSocial = entidad.Remitente.Nombre
                                entidad.Remitente.TipoNaturaleza = New ValorCatalogos With {.Codigo = 502}
                            Else
                                entidad.Remitente.RazonSocial = ""
                                entidad.Remitente.TipoNaturaleza = New ValorCatalogos With {.Codigo = 501}
                            End If
                            entidad.Remitente.Correo = entidad.Remitente.CorreoFacturacion
                            entidad.Remitente.Codigo = tercero.Insertar(entidad.Remitente)
                            'entidad.Cliente = entidad.Remitente
                        End If
                    Else
                        If entidad.Remitente.CorreoFacturacion <> String.Empty Then
                            Dim Terc As Terceros = New Terceros With {.CodigoEmpresa = entidad.CodigoEmpresa, .Codigo = entidad.Remitente.Codigo}
                            Terc = tercero.Obtener(Terc)
                            If Terc.CorreoFacturacion <> entidad.Remitente.CorreoFacturacion Then
                                entidad.Remitente.CodigoEmpresa = entidad.CodigoEmpresa
                                entidad.Remitente.UsuaCodigoCrea = entidad.UsuarioCrea.Codigo
                                Terc.Codigo = tercero.ActualizarEmailFacturaElectronica(entidad.Remitente)
                            End If
                        End If
                    End If
                End If

                'Insertar Destinatario
                If Not IsNothing(entidad.Destinatario) Then
                    'Valida Documento Destinatario
                    If entidad.Destinatario.Codigo = 0 And Not IsNothing(entidad.Destinatario.NumeroIdentificacion) Then
                        Dim Dest As Terceros = New Terceros With {.CodigoEmpresa = entidad.CodigoEmpresa, .NumeroIdentificacion = entidad.Destinatario.NumeroIdentificacion}
                        Dim listaDest = New List(Of Terceros)
                        listaDest = tercero.Consultar(Dest)
                        If listaDest.Count > 0 Then
                            entidad.Destinatario.Codigo = listaDest(0).Codigo
                        End If
                    End If


                    If entidad.Destinatario.Codigo = 0 And Not IsNothing(entidad.Destinatario.NumeroIdentificacion) Then
                        If entidad.Destinatario.NumeroIdentificacion <> entidad.Remitente.NumeroIdentificacion Then
                            entidad.Destinatario.CodigoEmpresa = entidad.CodigoEmpresa
                            entidad.Destinatario.UsuaCodigoCrea = entidad.UsuarioCrea.Codigo
                            entidad.Destinatario.CadenaPerfiles = "1404,1410," ' Destinatario y Remitente
                            entidad.Destinatario.Estado = New Estado With {.Codigo = 1}
                            If entidad.Destinatario.TipoIdentificacion.Codigo = 102 Then
                                'NIT
                                entidad.Destinatario.RazonSocial = entidad.Destinatario.Nombre
                                entidad.Destinatario.TipoNaturaleza = New ValorCatalogos With {.Codigo = 502}
                            Else
                                entidad.Destinatario.RazonSocial = ""
                                entidad.Destinatario.TipoNaturaleza = New ValorCatalogos With {.Codigo = 501}
                            End If
                            entidad.Destinatario.Codigo = tercero.Insertar(entidad.Destinatario)
                        Else
                            entidad.Destinatario.Codigo = entidad.Remitente.Codigo
                        End If
                    End If
                End If
            End If


            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)

                    If Not IsNothing(entidad.LineaNegocioPaqueteria) Then
                        If entidad.LineaNegocioPaqueteria.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_Linea_Negocio_Paqueteria", entidad.LineaNegocioPaqueteria.Codigo)
                        End If
                    End If

                    '----Cliente O Remitente
                    If Not IsNothing(entidad.Cliente) Then
                        If entidad.Cliente.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", entidad.Cliente.Codigo)
                        End If
                    End If

                    If Not IsNothing(entidad.Remitente) Then
                        If entidad.Remitente.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Remitente", entidad.Remitente.Codigo)
                        End If
                    End If
                    If Not IsNothing(entidad.Remitente.Ciudad) Then
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo_Remitente", entidad.Remitente.Ciudad.Codigo)
                    End If
                    If Not IsNothing(entidad.Remitente.Direccion) Then
                        conexion.AgregarParametroSQL("@par_Direccion_Remitente", entidad.Remitente.Direccion)
                    End If
                    If Not IsNothing(entidad.Remitente.Telefonos) Then
                        conexion.AgregarParametroSQL("@par_Telefonos_Remitente", entidad.Remitente.Telefonos, SqlDbType.VarChar)
                    End If

                    '----Cliente O Remitente
                    '----Destinatario
                    If Not IsNothing(entidad.Destinatario) Then
                        If entidad.Destinatario.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Destinatario", entidad.Destinatario.Codigo)
                        End If
                        If Not IsNothing(entidad.Destinatario.Ciudad) Then
                            If entidad.Destinatario.Ciudad.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_CIUD_Codigo_Destinatario", entidad.Destinatario.Ciudad.Codigo)
                            End If
                        End If
                        If Len(entidad.Destinatario.Direccion) > 0 Then
                            conexion.AgregarParametroSQL("@par_Direccion_Destinatario", entidad.Destinatario.Direccion)
                        End If
                        If Len(entidad.Destinatario.Barrio) > 0 Then
                            conexion.AgregarParametroSQL("@par_Barrio_Destinatario", entidad.Destinatario.Barrio)
                        End If
                        If Len(entidad.Destinatario.CodigoPostal) > 0 Then
                            conexion.AgregarParametroSQL("@par_CodigoPostal_Destinatario", entidad.Destinatario.CodigoPostal)
                        End If
                        If Len(entidad.Destinatario.Telefonos) > 0 Then
                            conexion.AgregarParametroSQL("@par_Telefono_Destinatario", entidad.Destinatario.Telefonos)
                        End If
                    End If
                    conexion.AgregarParametroSQL("@par_Fecha_Entrega", entidad.FechaEntrega, SqlDbType.Date)
                    If Not IsNothing(entidad.HorarioEntrega) Then
                        If entidad.HorarioEntrega.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_CATA_HERP_Codigo", entidad.HorarioEntrega.Codigo)
                        End If
                    End If
                    '----Destinatario
                    '----Informacion Recoleccion
                    conexion.AgregarParametroSQL("@par_Fecha_Recoleccion", entidad.FechaRecoleccion, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Contacto", entidad.NombreContacto)

                    If entidad.IdentificacionContacto > 0 Then
                        conexion.AgregarParametroSQL("@par_Identifiacion_Contacto", entidad.IdentificacionContacto)
                    End If

                    conexion.AgregarParametroSQL("@par_CIUD_Codigo", entidad.Ciudad.Codigo)
                    If Not IsNothing(entidad.Zonas) Then
                        If entidad.Zonas.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_ZOCI_Codigo", entidad.Zonas.Codigo)
                        End If
                    End If
                    conexion.AgregarParametroSQL("@par_Direccion", entidad.Direccion)
                    conexion.AgregarParametroSQL("@par_Telefonos", entidad.Telefonos)
                    If Not IsNothing(entidad.OficinaGestiona) Then
                        If entidad.OficinaGestiona.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_Oficina_Gestiona", entidad.OficinaGestiona.Codigo)
                        End If
                    End If
                    conexion.AgregarParametroSQL("@par_Barrio", entidad.Barrio)
                    conexion.AgregarParametroSQL("@par_Latitud", entidad.Latitud, SqlDbType.Decimal)
                    conexion.AgregarParametroSQL("@par_Longitud", entidad.Longitud, SqlDbType.Decimal)
                    '----Informacion Recoleccion
                    '----Tarifas
                    If Not IsNothing(entidad.FormaPago) Then
                        If entidad.FormaPago.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_FPVE_Codigo", entidad.FormaPago.Codigo)
                        End If
                    End If
                    If Not IsNothing(entidad.DetalleTarifaVenta) Then
                        If entidad.DetalleTarifaVenta.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_DTCV_Codigo", entidad.DetalleTarifaVenta.Codigo)
                        End If
                    End If
                    If Not IsNothing(entidad.ProductoTransportado) Then
                        If entidad.ProductoTransportado.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_PRTR_Codigo", entidad.ProductoTransportado.Codigo)
                        End If
                    End If
                    conexion.AgregarParametroSQL("@par_Mercancia", entidad.Mercancia)
                    conexion.AgregarParametroSQL("@par_UEPT_Codigo", entidad.CodigoUnidadEmpaque)
                    conexion.AgregarParametroSQL("@par_Cantidad", entidad.Cantidad)
                    conexion.AgregarParametroSQL("@par_Peso", entidad.Peso, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Largo", entidad.Largo, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Alto", entidad.Alto, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Ancho", entidad.Ancho, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_PesoVolumetrico", entidad.PesoVolumetrico, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Peso_Cobrar", entidad.PesoCobrar, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Comercial", entidad.ValorComercial, SqlDbType.Money)
                    '----Tarifas
                    '----Totales
                    conexion.AgregarParametroSQL("@par_Valor_Flete_Cliente", entidad.ValorFleteCliente, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Manejo_Cliente", entidad.ValorManejoCliente, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Seguro_Cliente", entidad.ValorSeguroCliente, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Total_Flete_Cliente", entidad.TotalFleteCliente, SqlDbType.Money)
                    '----Totales

                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficinas.Codigo)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)


                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_recolecciones")

                    While resultado.Read
                        entidad.NumeroDocumento = resultado.Item("Numero_Documento").ToString()
                        entidad.Numero = resultado.Item("Numero").ToString()
                    End While

                    resultado.Close()
                End Using
                If entidad.Numero > 0 Then
                    transaccion.Complete()
                End If
            End Using
            Return entidad.NumeroDocumento
        End Function

        Public Overrides Function Modificar(entidad As Recolecciones) As Long
            Dim tercero = New Repositorio.Basico.General.RepositorioTerceros
            'Insertar Remitente/Cliente
            If Not IsNothing(entidad.Remitente) Then
                'Valida Documento Remitente
                If entidad.Remitente.Codigo = 0 And Not IsNothing(entidad.Remitente.NumeroIdentificacion) Then
                    Dim Remi As Terceros = New Terceros With {.CodigoEmpresa = entidad.CodigoEmpresa, .NumeroIdentificacion = entidad.Remitente.NumeroIdentificacion}
                    Dim listaRemi = New List(Of Terceros)
                    listaRemi = tercero.Consultar(Remi)
                    If listaRemi.Count > 0 Then
                        entidad.Remitente.Codigo = listaRemi(0).Codigo
                    End If
                End If

                If entidad.Remitente.Codigo = 0 Then
                    If Not IsNothing(entidad.Remitente.NumeroIdentificacion) Then
                        entidad.Remitente.CodigoEmpresa = entidad.CodigoEmpresa
                        entidad.Remitente.UsuaCodigoCrea = entidad.UsuarioCrea.Codigo
                        entidad.Remitente.CadenaPerfiles = "1404,1410,"
                        entidad.Remitente.Estado = New Estado With {.Codigo = 1}
                        If entidad.Remitente.TipoIdentificacion.Codigo = 102 Then
                            'NIT
                            entidad.Remitente.RazonSocial = entidad.Remitente.Nombre
                            entidad.Remitente.TipoNaturaleza = New ValorCatalogos With {.Codigo = 502}
                        Else
                            entidad.Remitente.RazonSocial = ""
                            entidad.Remitente.TipoNaturaleza = New ValorCatalogos With {.Codigo = 501}
                        End If
                        entidad.Remitente.Correo = entidad.Remitente.CorreoFacturacion
                        entidad.Remitente.Codigo = tercero.Insertar(entidad.Remitente)
                        'entidad.Cliente = entidad.Remitente
                    End If
                Else
                    If entidad.Remitente.CorreoFacturacion <> String.Empty Then
                        Dim Terc As Terceros = New Terceros With {.CodigoEmpresa = entidad.CodigoEmpresa, .Codigo = entidad.Remitente.Codigo}
                        Terc = tercero.Obtener(Terc)
                        If Terc.CorreoFacturacion <> entidad.Remitente.CorreoFacturacion Then
                            entidad.Remitente.CodigoEmpresa = entidad.CodigoEmpresa
                            entidad.Remitente.UsuaCodigoCrea = entidad.UsuarioCrea.Codigo
                            Terc.Codigo = tercero.ActualizarEmailFacturaElectronica(entidad.Remitente)
                        End If
                    End If
                End If
            End If

            'Insertar Destinatario
            If Not IsNothing(entidad.Destinatario) Then
                'Valida Documento Destinatario
                If entidad.Destinatario.Codigo = 0 And Not IsNothing(entidad.Destinatario.NumeroIdentificacion) Then
                    Dim Dest As Terceros = New Terceros With {.CodigoEmpresa = entidad.CodigoEmpresa, .NumeroIdentificacion = entidad.Destinatario.NumeroIdentificacion}
                    Dim listaDest = New List(Of Terceros)
                    listaDest = tercero.Consultar(Dest)
                    If listaDest.Count > 0 Then
                        entidad.Destinatario.Codigo = listaDest(0).Codigo
                    End If
                End If

                If entidad.Destinatario.Codigo = 0 And Not IsNothing(entidad.Destinatario.NumeroIdentificacion) Then
                    If entidad.Destinatario.NumeroIdentificacion <> entidad.Remitente.NumeroIdentificacion Then
                        entidad.Destinatario.CodigoEmpresa = entidad.CodigoEmpresa
                        entidad.Destinatario.UsuaCodigoCrea = entidad.UsuarioCrea.Codigo
                        entidad.Destinatario.CadenaPerfiles = "1404,1410," ' Destinatario y Remitente
                        entidad.Destinatario.Estado = New Estado With {.Codigo = 1}
                        If entidad.Destinatario.TipoIdentificacion.Codigo = 102 Then
                            'NIT
                            entidad.Destinatario.RazonSocial = entidad.Destinatario.Nombre
                            entidad.Destinatario.TipoNaturaleza = New ValorCatalogos With {.Codigo = 502}
                        Else
                            entidad.Destinatario.RazonSocial = ""
                            entidad.Destinatario.TipoNaturaleza = New ValorCatalogos With {.Codigo = 501}
                        End If
                        entidad.Destinatario.Codigo = tercero.Insertar(entidad.Destinatario)
                    Else
                        entidad.Destinatario.Codigo = entidad.Remitente.Codigo
                    End If
                End If
            End If

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)

                If Not IsNothing(entidad.LineaNegocioPaqueteria) Then
                    If entidad.LineaNegocioPaqueteria.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Linea_Negocio_Paqueteria", entidad.LineaNegocioPaqueteria.Codigo)
                    End If
                End If
                '----Cliente O Remitente
                If Not IsNothing(entidad.Cliente) Then
                    If entidad.Cliente.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", entidad.Cliente.Codigo)
                    End If
                End If
                If Not IsNothing(entidad.Remitente) Then
                    If entidad.Remitente.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Remitente", entidad.Remitente.Codigo)
                    End If
                End If
                If Not IsNothing(entidad.Remitente.Ciudad) Then
                    conexion.AgregarParametroSQL("@par_CIUD_Codigo_Remitente", entidad.Remitente.Ciudad.Codigo)
                End If
                If Not IsNothing(entidad.Remitente.Direccion) Then
                    conexion.AgregarParametroSQL("@par_Direccion_Remitente", entidad.Remitente.Direccion)
                End If
                If Not IsNothing(entidad.Remitente.Telefonos) Then
                    conexion.AgregarParametroSQL("@par_Telefonos_Remitente", entidad.Remitente.Telefonos, SqlDbType.VarChar)
                End If

                '----Cliente O Remitente
                '----Destinatario
                If Not IsNothing(entidad.Destinatario) Then
                    If entidad.Destinatario.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Destinatario", entidad.Destinatario.Codigo)
                    End If
                End If
                If Not IsNothing(entidad.Destinatario.Ciudad) Then
                    If entidad.Destinatario.Ciudad.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo_Destinatario", entidad.Destinatario.Ciudad.Codigo)
                    End If
                End If
                If Len(entidad.Destinatario.Direccion) > 0 Then
                    conexion.AgregarParametroSQL("@par_Direccion_Destinatario", entidad.Destinatario.Direccion)
                End If
                If Len(entidad.Destinatario.Barrio) > 0 Then
                    conexion.AgregarParametroSQL("@par_Barrio_Destinatario", entidad.Destinatario.Barrio)
                End If
                If Len(entidad.Destinatario.CodigoPostal) > 0 Then
                    conexion.AgregarParametroSQL("@par_CodigoPostal_Destinatario", entidad.Destinatario.CodigoPostal)
                End If
                If Len(entidad.Destinatario.Telefonos) > 0 Then
                    conexion.AgregarParametroSQL("@par_Telefono_Destinatario", entidad.Destinatario.Telefonos)
                End If
                conexion.AgregarParametroSQL("@par_Fecha_Entrega", entidad.FechaEntrega, SqlDbType.Date)
                If Not IsNothing(entidad.HorarioEntrega) Then
                    If entidad.HorarioEntrega.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_CATA_HERP_Codigo", entidad.HorarioEntrega.Codigo)
                    End If

                End If
                '----Destinatario
                '----Informacion Recoleccion
                conexion.AgregarParametroSQL("@par_Fecha_Recoleccion", entidad.FechaRecoleccion, SqlDbType.DateTime)
                conexion.AgregarParametroSQL("@par_Contacto", entidad.NombreContacto)
                If entidad.IdentificacionContacto > 0 Then
                    conexion.AgregarParametroSQL("@par_Identifiacion_Contacto", entidad.IdentificacionContacto)
                End If
                conexion.AgregarParametroSQL("@par_CIUD_Codigo", entidad.Ciudad.Codigo)
                If Not IsNothing(entidad.Zonas) Then
                    If entidad.Zonas.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_ZOCI_Codigo", entidad.Zonas.Codigo)
                    End If
                End If
                conexion.AgregarParametroSQL("@par_Direccion", entidad.Direccion)
                conexion.AgregarParametroSQL("@par_Telefonos", entidad.Telefonos)
                If Not IsNothing(entidad.OficinaGestiona) Then
                    If entidad.OficinaGestiona.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Oficina_Gestiona", entidad.OficinaGestiona.Codigo)
                    End If
                End If
                conexion.AgregarParametroSQL("@par_Barrio", entidad.Barrio)
                conexion.AgregarParametroSQL("@par_Latitud", entidad.Latitud, SqlDbType.Decimal)
                conexion.AgregarParametroSQL("@par_Longitud", entidad.Longitud, SqlDbType.Decimal)
                '----Informacion Recoleccion
                '----Tarifas
                If Not IsNothing(entidad.FormaPago) Then
                    If entidad.FormaPago.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_FPVE_Codigo", entidad.FormaPago.Codigo)
                    End If
                End If
                If Not IsNothing(entidad.DetalleTarifaVenta) Then
                    If entidad.DetalleTarifaVenta.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_DTCV_Codigo", entidad.DetalleTarifaVenta.Codigo)
                    End If
                End If
                If Not IsNothing(entidad.ProductoTransportado) Then
                    If entidad.ProductoTransportado.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_PRTR_Codigo", entidad.ProductoTransportado.Codigo)
                    End If
                End If
                conexion.AgregarParametroSQL("@par_Mercancia", entidad.Mercancia)
                conexion.AgregarParametroSQL("@par_UEPT_Codigo", entidad.CodigoUnidadEmpaque)
                conexion.AgregarParametroSQL("@par_Cantidad", entidad.Cantidad)
                conexion.AgregarParametroSQL("@par_Peso", entidad.Peso, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Largo", entidad.Largo, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Alto", entidad.Alto, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Ancho", entidad.Ancho, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_PesoVolumetrico", entidad.PesoVolumetrico, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Peso_Cobrar", entidad.PesoCobrar, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Valor_Comercial", entidad.ValorComercial, SqlDbType.Money)
                '----Tarifas
                '----Totales
                conexion.AgregarParametroSQL("@par_Valor_Flete_Cliente", entidad.ValorFleteCliente, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Valor_Manejo_Cliente", entidad.ValorManejoCliente, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Valor_Seguro_Cliente", entidad.ValorSeguroCliente, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Total_Flete_Cliente", entidad.TotalFleteCliente, SqlDbType.Money)
                '----Totales


                conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficinas.Codigo)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioCrea.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_recolecciones")

                While resultado.Read
                    entidad.NumeroDocumento = resultado.Item("Numero_Documento").ToString()
                    entidad.Numero = resultado.Item("Numero").ToString()
                End While

                resultado.Close()

                If entidad.NumeroDocumento.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.NumeroDocumento
        End Function

        Public Overrides Function Obtener(filtro As Recolecciones) As Recolecciones
            Dim item As New Recolecciones

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Numero > Cero Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_recolecciones")

                While resultado.Read
                    item = New Recolecciones(resultado)
                End While

            End Using

            Return item
        End Function

        Public Function Anular(entidad As Recolecciones) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_recolecciones]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > Cero, True, False)
                End While

            End Using

            Return anulo
        End Function

        Public Function Cancelar(entidad As Recolecciones) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_estado_recoleccion")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > Cero, True, False)
                End While

            End Using

            Return anulo
        End Function

        Public Function AsociarRecoleccionesPlanilla(filtro As Recolecciones) As Boolean
            Dim Inserto As Boolean = True
            Dim resultado As IDataReader
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)

                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    For Each item In filtro.Recolecciones
                        conexion.CleanParameters()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.NumeroPlanilla)
                        conexion.AgregarParametroSQL("@par_ENRE_Numero", item.Numero)
                        conexion.AgregarParametroSQL("@par_ENRE_Numero_Documento", item.NumeroDocumento)
                        If Not IsNothing(filtro.TipoDocumento) Then
                            If filtro.TipoDocumento = 205 Then
                                resultado = conexion.ExecuteReaderStoreProcedure("gsp_asociar_recoleccion_planilla")
                                While resultado.Read
                                    If Not (resultado.Item("Codigo") > 0) Then
                                        Inserto = False
                                        Exit For
                                    End If
                                End While
                                resultado.Close()
                            ElseIf filtro.TipoDocumento = 210 Then
                                resultado = conexion.ExecuteReaderStoreProcedure("gsp_asociar_recoleccion_planilla_entrega")
                                While resultado.Read
                                    If Not (resultado.Item("Codigo") > 0) Then
                                        Inserto = False
                                        Exit For
                                    End If
                                End While
                                resultado.Close()

                            ElseIf filtro.TipoDocumento = 135 Then ' Planilla Paquetería
                                resultado = conexion.ExecuteReaderStoreProcedure("gsp_asociar_recoleccion_planilla_paqueteria")
                                While resultado.Read
                                    If Not (resultado.Item("Codigo") > 0) Then
                                        Inserto = False
                                        Exit For
                                    End If
                                End While
                                resultado.Close()
                            End If


                        End If
                        ' resultado = conexion.ExecuteReaderStoreProcedure("gsp_asociar_recoleccion_planilla")


                    Next
                    conexion.CloseConnection()
                End Using
                If Inserto Then
                    transaccion.Complete()
                Else
                    transaccion.Dispose()
                End If
            End Using
            Return Inserto
        End Function

        Public Function GuardarEntrega(entidad As Recolecciones) As Long

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    Dim resultado As IDataReader
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_ENRE_Numero", entidad.Numero)
                    'If entidad.DevolucionRemesa > 0 Then
                    '    resultado = conexion.ExecuteReaderStoreProcedure("gsp_registrar_devolucion_remesa_paqueteria")
                    'Else
                    conexion.AgregarParametroSQL("@par_Cantidad_Recibe", entidad.Cantidad, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Peso_Recibe", entidad.Peso, SqlDbType.Money)
                    'conexion.AgregarParametroSQL("@par_Fecha_Recibe", entidad.FechaRecibe, SqlDbType.Date)
                    'conexion.AgregarParametroSQL("@par_CATA_TIID_Codigo_Recibe", entidad.CodigoTipoIdentificacionRecibe)
                    '    conexion.AgregarParametroSQL("@par_Numero_Identificacion_Recibe", entidad.NumeroIdentificacionRecibe)
                    conexion.AgregarParametroSQL("@par_Nombre_Recibe", entidad.NombreContacto)
                    conexion.AgregarParametroSQL("@par_Telefonos_Recibe", entidad.Telefonos)
                    If Not IsNothing(entidad.NovedadRecoleccion) Then
                        conexion.AgregarParametroSQL("@par_CATA_NORP_Codigo", entidad.NovedadRecoleccion.Codigo)
                    End If
                    conexion.AgregarParametroSQL("@par_Observaciones_Recibe", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_Firma", entidad.Firma, SqlDbType.Image)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)

                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_registrar_entrega_recoleccion_paqueteria")
                    'End If
                    While resultado.Read
                        entidad.Numero = resultado.Item("Codigo").ToString()
                    End While

                    resultado.Close()
                    conexion.CloseConnection()

                    If entidad.Numero.Equals(Cero) Then
                        transaccion.Dispose()
                        Return Cero
                    Else
                        transaccion.Complete()
                    End If

                End Using

            End Using

            If entidad.Numero > 0 Then

                If Not IsNothing(entidad.Fotografias) Then
                    For Each Foto In entidad.Fotografias
                        Foto.CodigoEmpresa = entidad.CodigoEmpresa
                        Foto.NumeroRemesa = entidad.Numero
                        Foto.Codigo = entidad.Numero
                        Foto.CodigoUsuarioCrea = entidad.UsuarioModifica.Codigo
                        Foto.ListaFotos = True
                        If entidad.DevolucionRemesa > 0 Then
                            Foto.EntregaDevolucion = 1
                        Else
                            Foto.EntregaDevolucion = 0
                        End If
                        InsertarFoto(Foto)
                    Next
                End If
            End If

            Return entidad.Numero

        End Function

        Public Function InsertarFoto(entidad As FotoDetalleDistribucionRemesas) As Boolean
            Dim Inserto As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENRE_Numero", entidad.NumeroRemesa)
                ' conexion.AgregarParametroSQL("@par_DEDR_ID", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Foto", entidad.FotoBits, SqlDbType.VarBinary)
                conexion.AgregarParametroSQL("@par_Nombre_Foto", entidad.NombreFoto)
                conexion.AgregarParametroSQL("@par_Extension_Foto", entidad.ExtensionFoto)
                conexion.AgregarParametroSQL("@par_Tipo_Foto", entidad.TipoFoto)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.CodigoUsuarioCrea)
                conexion.AgregarParametroSQL("@par_Entrega_Devolucion", entidad.EntregaDevolucion)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_fotos_detalle_distribucion_recolecciones]")
                While resultado.Read
                    Inserto = IIf((resultado.Item("Codigo")) > 0, True, False)
                End While

                resultado.Close()




                conexion.CloseConnection()

            End Using

            Return Inserto
        End Function

    End Class

End Namespace