﻿PRINT 'gsp_insertar_detalle_tarifario_compras'
GO
DROP PROCEDURE gsp_insertar_detalle_tarifario_compras
GO
CREATE PROCEDURE gsp_insertar_detalle_tarifario_compras
(
@par_EMPR_Codigo smallint
           ,@par_ETCC_Numero numeric(18,0)
           ,@par_LNTC_Codigo smallint
           ,@par_TLNC_Codigo smallint
           ,@par_RUTA_Codigo numeric(18,0)
           ,@par_TATC_Codigo smallint
           ,@par_TTTC_Codigo smallint
           ,@par_Valor_Flete money
           ,@par_Valor_Escolta money = null
           --,@par_Valor_Kilo_Adicional money = null
           ,@par_Valor_Otros1 money = null
           ,@par_Valor_Otros2 money = null
           ,@par_Valor_Otros3 money = null
           ,@par_Valor_Otros4 money = null
           ,@par_Fecha_Vigencia_Inicio date = null
           ,@par_Fecha_Vigencia_Fin date = null
           ,@par_Estado smallint
           ,@par_USUA_Codigo_Crea smallint
)
AS BEGIN

declare @par_Codigo as numeric =(select ISNULL(max(Codigo),0)+1 from Detalle_Tarifario_Carga_compras WHERE EMPR_Codigo = @par_EMPR_Codigo AND ETCC_Numero = @par_ETCC_Numero)

INSERT INTO Detalle_Tarifario_Carga_compras
           (
		   EMPR_Codigo
           ,ETCC_Numero
           ,Codigo
           ,LNTC_Codigo
           ,TLNC_Codigo
           ,RUTA_Codigo
           ,TATC_Codigo
           ,TTTC_Codigo
           ,Valor_Flete
           ,Valor_Escolta
           --,Valor_Kilo_Adicional
           ,Valor_Otros1
           ,Valor_Otros2
           ,Valor_Otros3
           ,Valor_Otros4
           ,Fecha_Vigencia_Inicio
           ,Fecha_Vigencia_Fin
           ,Estado
           ,USUA_Codigo_Crea
           ,Fecha_Crea
		   )
     VALUES
           (@par_EMPR_Codigo
           ,@par_ETCC_Numero
           ,@par_Codigo
           ,@par_LNTC_Codigo
           ,@par_TLNC_Codigo
           ,@par_RUTA_Codigo
           ,@par_TATC_Codigo
           ,@par_TTTC_Codigo
           ,@par_Valor_Flete
           ,isnull(@par_Valor_Escolta,0)
           --,isnull(@par_Valor_Kilo_Adicional,0)
           ,isnull(@par_Valor_Otros1,0)
           ,isnull(@par_Valor_Otros2,0)
           ,isnull(@par_Valor_Otros3,0)
           ,isnull(@par_Valor_Otros4,0)
           ,isnull(@par_Fecha_Vigencia_Inicio,'')
           ,isnull(@par_Fecha_Vigencia_Fin,'')
           ,@par_Estado
           ,@par_USUA_Codigo_Crea
           ,GETDATE()
           )
SELECT @par_Codigo AS Codigo

END
GO