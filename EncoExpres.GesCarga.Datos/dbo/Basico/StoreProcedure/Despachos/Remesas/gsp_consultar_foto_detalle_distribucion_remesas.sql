﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 07/12/2018
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	

PRINT 'gsp_consultar_foto_detalle_distribucion_remesas'
GO
DROP PROCEDURE gsp_consultar_foto_detalle_distribucion_remesas
GO
CREATE PROCEDURE gsp_consultar_foto_detalle_distribucion_remesas 
(  
@par_EMPR_Codigo SMALLINT,  
@par_ENRE_Numero NUMERIC,  
@par_DEDR_Codigo NUMERIC  
)  
AS  
BEGIN  
   SELECT    
   Nombre_Foto,
   Tipo,   
   Extension,
   Foto
  FROM Foto_Detalle_Distribucion_Remesas  
  WHERE   
  EMPR_Codigo = @par_EMPR_Codigo    
  AND ENRE_Numero = @par_ENRE_Numero   
  AND DEDR_ID = @par_DEDR_Codigo   
  
END  
GO