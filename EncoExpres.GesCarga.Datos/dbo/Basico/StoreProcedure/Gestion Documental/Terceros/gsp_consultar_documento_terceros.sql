﻿print 'gsp_consultar_documento_terceros'
go
drop procedure gsp_consultar_documento_terceros
go
create procedure gsp_consultar_documento_terceros
(
@par_EMPR_Codigo smallint ,
@par_TERC_Codigo numeric
)
as
begin
select
	EMPR_Codigo ,
	TERC_Codigo ,
	CDGD_Codigo ,
	Referencia,
	Emisor,
	Fecha_Emision,
	Fecha_Vence,
	CASE WHEN Archivo IS NULL THEN 0 ELSE 1 END AS ValorDocumento,
	NULL AS Archivo 
from Tercero_Documentos
WHERE EMPR_Codigo = @par_EMPR_Codigo  AND TERC_Codigo = @par_TERC_Codigo 
end
go