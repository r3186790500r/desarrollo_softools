﻿PRINT 'gsp_consultar_secciones_formularios_inspecciones'
GO
DROP PROCEDURE gsp_consultar_secciones_formularios_inspecciones
GO
CREATE PROCEDURE gsp_consultar_secciones_formularios_inspecciones 
(  
@par_EMPR_Codigo SMALLINT,  
@par_Codigo NUMERIC  
)  
AS  
BEGIN  
SELECT   
EMPR_Codigo,  
ENFI_Codigo,  
Codigo,  
Orden_Formulario,  
Nombre,  
Estado  
FROM  
Detalle_Secciones_Formularios_Inspecciones  
WHERE   
EMPR_Codigo = @par_EMPR_Codigo  
AND ENFI_Codigo = @par_Codigo  
  
END  
GO