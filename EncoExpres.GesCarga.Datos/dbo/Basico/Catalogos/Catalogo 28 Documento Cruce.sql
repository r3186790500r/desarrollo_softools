﻿----------------------------------------------------------------------------------------------------------
Print 'Catalogo 28 Documento Cruce'
GO
DELETE Valor_Catalogos WHERE CATA_Codigo = 28
GO
DELETE Configuracion_Catalogos WHERE CATA_Codigo = 28
GO
DELETE Catalogos WHERE Codigo = 28
GO
------------------------------------------CATALOGOS------------------------------------------------------
INSERT INTO Catalogos VALUES (1,28,'DCPC','Documento Cruce Parametrizacion Contable',0,2,1)
GO
-----------------------------------------VALOR CATALOGOS--------------------------------------------------
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,28,2800,'(NO APLICA)',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,28,2801,'Documento Origen',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,28,2802,'Documento Genera',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,28,2803,'Auxiliar',1,GETDATE())
GO
-----------------------------------------CONFIGURACION CATALOGOS------------------------------------------
INSERT INTO configuracion_catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
VALUES (1,28,1,'Codigo',1,1,1,1,0,0,0,0,0,1,0)
GO 
INSERT INTO configuracion_catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
VALUES (1,28,2,'Nombre',1,1,1,1,0,0,0,0,0,1,0)
GO 
----------------------------------------------------------------------------------------------------------