﻿EncoExpresApp.controller("GestionarConceptosFacturacionCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'TercerosFactory', 'OficinasFactory', 'VehiculosFactory', 'ConceptosFacturacionFactory', 'TercerosFactory', 'ImpuestosFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, TercerosFactory, OficinasFactory, VehiculosFactory, ConceptosFacturacionFactory, TercerosFactory, ImpuestosFactory) {

        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Facturación' }, { Nombre: 'Concepto Factura' }, { Nombre: 'Gestionar' }];

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;



        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CONCEPTOS_FACTURA);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.paginaActual = 1;
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.Codigo = 0;
        $scope.Numero = 0;
        $scope.Deshabilitar = false;
        $scope.ModeloFechaCrea = '';

        $scope.ListadoEstados = [
            { Nombre: '(Seleccione Item)', Codigo: -1 },
            { Nombre: 'Activo', Codigo: 1 },
            { Nombre: 'Inactivo', Codigo: 0 }
        ];
        $scope.ListadoOperacion = [
            { Nombre: '(Seleccione Item)', Codigo: -1 },
            { Nombre: 'Suma (+)', Codigo: 1 },
            { Nombre: 'Resta (-)', Codigo: 2 }
        ];

        $scope.ModeloEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
        $scope.ModeloOperacion = $linq.Enumerable().From($scope.ListadoOperacion).First('$.Codigo == -1');

        /* Obtener parametros del enrutador*/
        if ($routeParams.Codigo !== undefined) {
            $scope.Codigo = $routeParams.Codigo;
            if ($scope.Codigo > 0) {
                Obtener()
            }
        }
        else {
            $scope.Codigo = 0;
        }

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando Planilla Entregada Código ' + $scope.Codigo);

            $timeout(function () {
                blockUI.message('Cargando Planilla Entregada Código ' + $scope.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
                Numero: $scope.Numero,
            };

            blockUI.delay = 1000;
            ConceptosFacturacionFactory.Obtener(filtros).
                then(function (response) {
                    console.log(response);
                    if (response.data.ProcesoExitoso === true) {

                        $scope.ModeloCodigo = response.data.Datos.Codigo;
                        $scope.ModeloCodigoAlterno = response.data.Datos.CodigoAlterno;

                        $scope.ModeloNombre = response.data.Datos.Nombre;

                        $scope.ModeloEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado);
                        $scope.ModeloOperacion = $linq.Enumerable().From($scope.ListadoOperacion).First('$.Codigo == ' + response.data.Datos.Operacion);
                        $scope.ModeloFechaCrea = new Date(response.data.Datos.FechaCrea);
                        $scope.ModeloConceptoSistema = response.data.Datos.ConceptoSistema;

                        if ($scope.ModeloConceptoSistema == 1) {
                            $scope.Deshabilitar = true
                        }
                    }
                    else {
                        ShowError('No se logro consultar la Orden de Cargue ' + $scope.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarConceptosLiquidacion';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarConceptosLiquidacion';
                });
            blockUI.stop();
        };

        /*-----------------------------------------------------------Funcion validar datos requeridos para poder guardar/modificar el tercero -------------------------------------------------------------------*/
        $scope.Guardar = function () {
            /*Verificar si la página actual ya está guardada antes de proceder a guardar*/
            closeModal('modalConfirmacionGuardarConcepto');
            if (DatosRequeridos()) {



                $scope.objEnviar = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.ModeloCodigo,
                    CodigoAlterno: $scope.ModeloCodigoAlterno,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    Nombre: $scope.ModeloNombre,
                    Operacion: $scope.ModeloOperacion.Codigo,
                    FechaCrea: $scope.ModeloFechaCrea,
                    Estado: $scope.ModeloEstado.Codigo,
                    AplicaImpuesto: ESTADO_INACTIVO,
                };

                ConceptosFacturacionFactory.Guardar($scope.objEnviar).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Numero == 0) {
                                    ShowSuccess('Se guardó el concepto : ' + $scope.ModeloNombre);
                                }
                                else {
                                    ShowSuccess('Se modificó el concepto : ' + $scope.ModeloNombre);
                                }
                                location.href = '#!ConsultarConceptosFacturacion/' + response.data.Datos;
                            }
                            else {
                                ShowError(response.data.MensajeOperacion);
                            }
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        if (response.statusText.includes('IX_Conceptos_Ventas_Nombre')) {
                            ShowError('No se pudo guardar, el nombre ingresado ya está siendo utilizado')
                        } else {
                            ShowError(response.statusText);
                        }
                    });
            }
        };

        $scope.ConfirmacionGuardarConcepto = function () {
            showModal('modalConfirmacionGuardarConcepto');
        };

        function DatosRequeridos() {
            window.scrollTo(top, top);
            $scope.MensajesError = [];
            var continuar = true;


            if ($scope.ModeloNombre == -1 || $scope.ModeloNombre == undefined || $scope.ModeloNombre == null || $scope.ModeloNombre == "") {  //NO APLICA
                $scope.MensajesError.push('Debe ingresar el Nombre');
                continuar = false
            }
            if ($scope.ModeloEstado.Codigo == -1) {  //NO APLICA
                $scope.MensajesError.push('Debe Seleccionar un estado ');
                continuar = false
            }
            return continuar;
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Metodo para volver a la página de consulta
        $scope.VolverMaster = function () {
            if ($routeParams.Numero > 0) {
                document.location.href = '#!ConsultarConceptosFacturacion/' + $scope.ModeloCodigo;
            } else {
                document.location.href = '#!ConsultarConceptosFacturacion';
            }
        };
        $scope.AgregarAuxiliar = function () {
            showModal('ModalVerAuxiliar');
        }
        $scope.CerrarModal = function () {
            closeModal('ModalVerAuxiliar');

        }
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope);
        };

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);

        };

        $scope.MaskDecimales = function () {
            return MascaraDecimalesGeneral($scope);
        }

        $scope.MaskTelefono = function () {
            return MascaraTelefono($scope);
        }
        $scope.MaskDireccion = function () {
            return MascaraDireccion($scope)
        }

        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };
    }]);