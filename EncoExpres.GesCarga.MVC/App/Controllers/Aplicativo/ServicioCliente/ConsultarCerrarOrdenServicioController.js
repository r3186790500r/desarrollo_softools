﻿EncoExpresApp.controller("ConsultarCierreOrdenServicioCtrl", ['$scope', '$timeout', '$linq', '$routeParams', 'blockUI', 'blockUIConfig', 'EmpresasFactory', 'TercerosFactory',
    'CerrarOrdenServicioFactory', 'ValorCatalogosFactory',
    function ($scope, $timeout, $linq, $routeParams, blockUI, blockUIConfig, EmpresasFactory, TercerosFactory,
        CerrarOrdenServicioFactory, ValorCatalogosFactory) {
        $scope.Titulo = 'CERRAR ORDEN SERVICIO';
        $scope.MapaSitio = [{ Nombre: 'Servicio al Cliente' }, { Nombre: 'Procesos' }, { Nombre: 'Cierre Orden Servicio' }];
        /*-----------------------------------------------------------------DECLARACION VARIABLES--------------------------------------------------------------------------------- */
        $scope.Buscando = false;
        $scope.ResultadoSinRegistros = '';
        $scope.Deshabilitar = false;
        $scope.MensajesError = [];
        var filtros = {};
        $scope.pref = '';
        $scope.totalRegistros = 0;
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.totalPaginas = 0;
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CERRAR_ORDEN_SERVICIO); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;

        $scope.ListadoCerrarOrdenServicios = [];
        $scope.ListadoCausalesCierre = [];
        $scope.Cliente = {
            Codigo: 0,
            NombreCompleto: ''
        }

        $scope.ListadoEstadoProgramacion = [];
        $scope.ListadoEstadoProgramacion.push({ Codigo: -1, Nombre: "(TODAS)" });
        $scope.ListadoEstadoProgramacion.push({ Codigo: CODIGO_CATALOGO_TIPO_ORDEN_SERVICIO_PENDIENTE, Nombre: "PENDIENTE" });
        $scope.ListadoEstadoProgramacion.push({ Codigo: CODIGO_CATALOGO_TIPO_ORDEN_SERVICIO_INCOMPLETA, Nombre: "INCOMPLETA" });
        $scope.ListadoEstadoProgramacion.push({ Codigo: CODIGO_CATALOGO_TIPO_ORDEN_SERVICIO_FINALIZADA, Nombre: "FINALIZADA" });
        $scope.EstadoProgramacion = $linq.Enumerable().From($scope.ListadoEstadoProgramacion).First('$.Codigo == -1');
        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };
        //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
        //------------------------------------------------------------OBTENER INFORMACION------------------------------------------------------------//
        //-- catalogo Causales Cierre
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_CAUSAL_CIERRE_ORDEN_SERVICIO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoCausalesCierre.push({ Nombre: 'SELECCIONE', Codigo: -1 });
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            $scope.ListadoCausalesCierre.push({ Nombre: response.data.Datos[i].Nombre, Codigo: response.data.Datos[i].Codigo })
                        }
                        $scope.CausaCierre = $linq.Enumerable().From($scope.ListadoCausalesCierre).First('$.Codigo == -1');
                    }
                    else {
                        $scope.ListadoCausalesCierre = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        //------------------------------------------------------------FUNCIONES AUTOCOMPLETE------------------------------------------------------------//
        //--Clientes
        $scope.ListadoClientes = [];
        $scope.AutocompleteClientes = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CLIENTE,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoClientes = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoClientes);
                }
            }
            return $scope.ListadoClientes;
        }
        //------------------------------------------------------------FUNCIONES AUTOCOMPLETE------------------------------------------------------------//
        $scope.Buscar = function () {
            if (DatosRequeridos()) {
                if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                    Find();
                }
            }
        };

        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoCerrarOrdenServicios = [];
            if ($scope.Buscando) {
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    OrdenServicio: {
                        NumeroDocumento: $scope.Numero,
                        Estado: ESTADO_DEFINITIVO,
                        Anulado: ESTADO_NO_ANULADO,
                        TipoDocumento: CODIGO_TIPO_DOCUMENTO_ORDEN_SERVICIO,
                        Cliente: { Codigo: $scope.Cliente.Codigo}
                    },
                    EstadoProgramacion: { Codigo: $scope.EstadoProgramacion.Codigo},
                    FechaInicial: $scope.FechaInicial,
                    FechaFinal: $scope.FechaFinal,
                    Pagina: $scope.paginaActual,
                    RegistrosPagina: $scope.cantidadRegistrosPorPagina,
                }
                blockUI.delay = 1000;
                CerrarOrdenServicioFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoCerrarOrdenServicios = response.data.Datos
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                    }, function (response) {
                        ShowError(response.stuatusText);
                    });
                blockUI.stop();
            }
        }

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.FechaInicial === null || $scope.FechaInicial === undefined || $scope.FechaInicial === '')
                && ($scope.FechaFinal === null || $scope.FechaFinal === undefined || $scope.FechaFinal === '')
                && ($scope.Numero === null || $scope.Numero === undefined || $scope.Numero === '' || $scope.Numero === 0 || isNaN($scope.Numero) === true)

            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false

            } else if (($scope.Numero !== null && $scope.Numero !== undefined && $scope.Numero !== '' && $scope.Numero !== 0)
                || ($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')
                || ($scope.FechaFinal !== null && $scope.FechaFinal !== undefined && $scope.FechaFinal !== '')

            ) {
                if (($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')
                    && ($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')) {
                    if ($scope.FechaFinal < $scope.FechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.FechaFinal - $scope.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }
                else {
                    if (($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')) {
                        $scope.FechaFinal = $scope.FechaInicial;
                    } else {
                        $scope.FechaInicial = $scope.FechaFinal;
                    }
                }
            }
            return continuar;
        }
        //------------------------------------------------------------CERRAR ORDEN SERVICIO------------------------------------------------------------//
        //--Validacion boton Cerrar
        $scope.ConfirmarCerrarOrdenServicio = function (InfoCerrar) {
            $scope.ESOSNumeroCerrar = InfoCerrar.OrdenServicio.Numero;
            $scope.ESOSNumeroDocumentoCerrar = InfoCerrar.OrdenServicio.NumeroDocumento;
            $scope.CausalCierre = 0;
            $scope.ObservacionesCierre = "";
            $scope.MensajesCerrar = [];
            showModal('modalConfirmacionCerrarOrdenServicio');
        };
        //--Funcion Cerrar Orden Servicio
        $scope.CerrarOrdenServicio = function () {
            if (DatosRequeridosCierreOrdenServicio()) {
                var entidad = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    OrdenServicio: {
                        Numero: $scope.ESOSNumeroCerrar,
                        TipoDocumento: CODIGO_TIPO_DOCUMENTO_ORDEN_SERVICIO,
                    },
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    CausalCierre: { Codigo: $scope.CausaCierre.Codigo },
                    Observaciones: $scope.ObservacionesCierre
                };

                CerrarOrdenServicioFactory.CerrarOrdenServicio(entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            ShowSuccess('Se Cerro la orden de servicio N. ' + $scope.ESOSNumeroDocumentoCerrar);
                            closeModal('modalConfirmacionCerrarOrdenServicio');
                            Find();
                        }
                        else {
                            ShowError('No se puede cerrar la orden debido a que ya hay despachos iniciados con remesas y/o planillas pendientes por generar');
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }
        function DatosRequeridosCierreOrdenServicio() {
            $scope.MensajesCerrar = [];
            var continuar = true;
            if ($scope.CausaCierre.Codigo <= 0) {
                $scope.MensajesCerrar.push('Debe seleccionar una causa de cierre');
                continuar = false;
            }
            if ($scope.ObservacionesCierre == "") {
                $scope.MensajesCerrar.push('Debe ingresar una observación de cierre');
                continuar = false;
            }
            return continuar;
        }
    }]);