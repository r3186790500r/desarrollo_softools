﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Basico.Tesoreria

Namespace Tesoreria

    ''' <summary>
    ''' Clase <see cref="DetalleDocumentoComprobantes"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class DetalleDocumentoComprobantes
        Inherits BaseDocumento

        ''' <summary>
        ''' Incializa una nueva instancia de la clase <see cref="DetalleDocumentoComprobantes"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleDocumentoComprobantes"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)


            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            CuentaPUC = New PlanUnicoCuentas With {.Codigo = Read(lector, "PLUC_Codigo"), .Nombre = Read(lector, "NombreCuenta"), .CodigoCuenta = Read(lector, "CuentaPUC")}
            Tercero = New Tercero With {.Codigo = Read(lector, "TERC_Codigo"), .Nombre = Read(lector, "NombreTercero")}
            ValorBase = Read(lector, "Valor_Base")
            ValorCredito = Read(lector, "Valor_Credito")
            ValorDebito = Read(lector, "Valor_Debito")
            GeneraCuenta = Read(lector, "Genera_Cuenta")
            Observaciones = Read(lector, "Observaciones")
            DocumentoCruce = New ValorCatalogos With {.Codigo = Read(lector, "CATA_DOCR_Codigo"), .Nombre = Read(lector, "DocumentoCruze")}
            TerceroParametrizacion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TEPC_Codigo"), .Nombre = Read(lector, "TerceroParametrizacion")}
            CentroCostoParametrizacion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_CCPC_Codigo"), .Nombre = Read(lector, "CentroCostoParametrizacion")}


        End Sub

        <JsonProperty>
        Public Property CuentaPUC As PlanUnicoCuentas
        <JsonProperty>
        Public Property Tercero As Tercero
        <JsonProperty>
        Public Property ValorBase As Double
        <JsonProperty>
        Public Property ValorDebito As Double
        <JsonProperty>
        Public Property ValorCredito As Double
        <JsonProperty>
        Public Property GeneraCuenta As Integer
        <JsonProperty>
        Public Property Prefijo As String
        <JsonProperty>
        Public Property CodigoAnexo As String
        <JsonProperty>
        Public Property SufijoCodigoAnexo As String
        <JsonProperty>
        Public Property CampoAuxiliar As String
        <JsonProperty>
        Public Property DocumentoCruce As ValorCatalogos
        <JsonProperty>
        Public Property TerceroParametrizacion As ValorCatalogos
        <JsonProperty>
        Public Property CentroCostoParametrizacion As ValorCatalogos

    End Class

End Namespace
