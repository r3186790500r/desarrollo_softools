﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.ServicioCliente
Imports EncoExpres.GesCarga.Repositorio.ServicioCliente

Namespace ServicioCliente
    Public NotInheritable Class PersistenciaDetalleDespachoSolicitudOrdenServicios
        Implements IPersistenciaBase(Of DetalleDespachoSolicitudOrdenServicios)

        Public Function Consultar(filtro As DetalleDespachoSolicitudOrdenServicios) As IEnumerable(Of DetalleDespachoSolicitudOrdenServicios) Implements IPersistenciaBase(Of DetalleDespachoSolicitudOrdenServicios).Consultar
            Return New RepositorioDetalleDespachoSolicitudOrdenServicios().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As DetalleDespachoSolicitudOrdenServicios) As Long Implements IPersistenciaBase(Of DetalleDespachoSolicitudOrdenServicios).Insertar
            Return New RepositorioDetalleDespachoSolicitudOrdenServicios().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As DetalleDespachoSolicitudOrdenServicios) As Long Implements IPersistenciaBase(Of DetalleDespachoSolicitudOrdenServicios).Modificar
            Return New RepositorioDetalleDespachoSolicitudOrdenServicios().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As DetalleDespachoSolicitudOrdenServicios) As DetalleDespachoSolicitudOrdenServicios Implements IPersistenciaBase(Of DetalleDespachoSolicitudOrdenServicios).Obtener
            Return New RepositorioDetalleDespachoSolicitudOrdenServicios().Obtener(filtro)
        End Function
        Public Function Anular(entidad As DetalleDespachoSolicitudOrdenServicios) As Boolean
            Return New RepositorioDetalleDespachoSolicitudOrdenServicios().Anular(entidad)
        End Function
    End Class

End Namespace

