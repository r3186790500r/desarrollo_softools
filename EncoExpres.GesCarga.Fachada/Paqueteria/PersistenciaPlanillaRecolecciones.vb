﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Despachos.Paqueteria
Imports EncoExpres.GesCarga.Repositorio.Basico.General


Namespace Despachos.Paqueteria
    Public NotInheritable Class PersistenciaPlanillaRecolecciones
        Implements IPersistenciaBase(Of PlanillaRecolecciones)

        Public Function Consultar(filtro As PlanillaRecolecciones) As IEnumerable(Of PlanillaRecolecciones) Implements IPersistenciaBase(Of PlanillaRecolecciones).Consultar
            Return New RepositorioPlanillaRecolecciones().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As PlanillaRecolecciones) As Long Implements IPersistenciaBase(Of PlanillaRecolecciones).Insertar
            Return New RepositorioPlanillaRecolecciones().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As PlanillaRecolecciones) As Long Implements IPersistenciaBase(Of PlanillaRecolecciones).Modificar
            Return New RepositorioPlanillaRecolecciones().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As PlanillaRecolecciones) As PlanillaRecolecciones Implements IPersistenciaBase(Of PlanillaRecolecciones).Obtener
            Return New RepositorioPlanillaRecolecciones().Obtener(filtro)
        End Function

        Public Function Anular(entidad As PlanillaRecolecciones) As Boolean
            Return New RepositorioPlanillaRecolecciones().Anular(entidad)
        End Function

    End Class

End Namespace

