﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Repositorio.Basico.General

Namespace Basico.General
    Public NotInheritable Class PersistenciaValorCatalogos
        Implements IPersistenciaBase(Of ValorCatalogos)

        Public Function Consultar(filtro As ValorCatalogos) As IEnumerable(Of ValorCatalogos) Implements IPersistenciaBase(Of ValorCatalogos).Consultar
            Return New RepositorioValorCatalogos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As ValorCatalogos) As Long Implements IPersistenciaBase(Of ValorCatalogos).Insertar
            Return New RepositorioValorCatalogos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As ValorCatalogos) As Long Implements IPersistenciaBase(Of ValorCatalogos).Modificar
            Return New RepositorioValorCatalogos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As ValorCatalogos) As ValorCatalogos Implements IPersistenciaBase(Of ValorCatalogos).Obtener
            Return New RepositorioValorCatalogos().Obtener(filtro)
        End Function
        Public Function Anular(entidad As ValorCatalogos) As Boolean
            Return New RepositorioValorCatalogos().Anular(entidad)
        End Function
    End Class

End Namespace

