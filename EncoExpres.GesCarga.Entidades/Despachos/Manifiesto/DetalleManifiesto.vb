﻿Imports Newtonsoft.Json
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Despachos.Remesa

Namespace Despachos
    <JsonObject>
    Public NotInheritable Class DetalleManifiesto
        Inherits Base
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleManifiesto"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            NumeroManifiesto = New Manifiesto With {.Numero = Read(lector, "ENMC_Numero")}
            Remesa = New Remesas With {.Numero = Read(lector, "ENRE_Numero"), .NumeroDocumento = Read(lector, "ENRE_NumeroDocumento")}
            Cliente = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Cliente"), .NombreCompleto = Read(lector, "Cliente")}
            Producto = New ProductoTransportados With {.Codigo = Read(lector, "PRTR_Codigo"), .Nombre = Read(lector, "Producto")}
            Cantidad = New Remesas With {.CantidadCliente = Read(lector, "Cantidad_Cliente")}
            Peso = New Remesas With {.PesoCliente = Read(lector, "Peso_Cliente")}
            Destinatario = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Destinatario"), .NombreCompleto = Read(lector, "Destinatario")}

        End Sub
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        ''' 
        <JsonProperty>
        Public Property CodigoEmpresa As Integer

        <JsonProperty>
        Public Property NumeroManifiesto As Manifiesto

        <JsonProperty>
        Public Property Remesa As Remesas

        <JsonProperty>
        Public Property Cantidad As Remesas

        <JsonProperty>
        Public Property Peso As Remesas

        <JsonProperty>
        Public Property CantidadRecibida As Double

        <JsonProperty>
        Public Property PesoRecibido As Double

        <JsonProperty>
        Public Property CantidadFaltante As Double

        <JsonProperty>
        Public Property PesoFaltante As Double

        <JsonProperty>
        Public Property ValorFaltante As Double

        <JsonProperty>
        Public Property Numero_Remesa_Electronico As Integer

        <JsonProperty>
        Public Property Fecha_Reporte_Electronico As Date

        <JsonProperty>
        Public Property Mensaje_Reporte_Electronico As String

        <JsonProperty>
        Public Property Cliente As Terceros

        <JsonProperty>
        Public Property Destinatario As Terceros

        <JsonProperty>
        Public Property Producto As ProductoTransportados
    End Class
End Namespace
