﻿PRINT 'gsp_obtener_planilla_recolecciones'
GO
DROP PROCEDURE gsp_obtener_planilla_recolecciones
GO
CREATE PROCEDURE gsp_obtener_planilla_recolecciones    
(    
@par_EMPR_Codigo SMALLINT,    
@par_Numero NUMERIC 
)    
AS    
BEGIN    
 SELECT 
  0 AS Obtener,    
  ENRE.EMPR_Codigo,    
  ENRE.Numero,    
  ENRE.Numero_Documento,       
  ISNULL(ENRE.Fecha,'') AS Fecha_Planilla,
  ISNULL(ENRE.VEHI_Codigo, '') AS Codigo_Vehiculo,
  ISNULL(VEHI.Placa, '') AS Placa_Vehiculo,  
  ISNULL(ENRE.TERC_Codigo_Conductor, 0) AS TERC_Codigo_Conductor, 
  ISNULL(TERC.Nombre,'') + ' ' + ISNULL(TERC.Apellido1, '')+ ' ' + ISNULL(TERC.Apellido2,'')+ ' ' + ISNULL(TERC.Razon_Social,'') AS Nombre_Conductor,       
  ISNULL(ENRE.OFIC_Codigo, 0) AS Codigo_Oficina,     
  ISNULL(OFIC.Nombre, '') AS Nombre_Oficina,       
  ISNULL(ENRE.Cantidad_Total,0) AS Cantidad_Total,    
  ISNULL(ENRE.Peso_Total,0) AS Peso_Total, 
  ISNULL(ENRE.Estado,'') AS Estado,
  ISNULL(ENRE.Observaciones,'') AS Observaciones

 FROM   
  
  Encabezado_Planilla_Recolecciones AS ENRE 
  
  LEFT JOIN Terceros AS TERC ON  
  ENRE.EMPR_Codigo = TERC.EMPR_Codigo  
  AND ENRE.TERC_Codigo_Conductor = TERC.Codigo  
  
  LEFT JOIN Oficinas AS OFIC ON  
  ENRE.EMPR_Codigo = OFIC.EMPR_Codigo  
  AND ENRE.OFIC_Codigo = OFIC.Codigo   

  LEFT JOIN Vehiculos AS VEHI ON  
  ENRE.EMPR_Codigo = VEHI.EMPR_Codigo  
  AND ENRE.VEHI_Codigo = VEHI.Codigo  
     
 WHERE    
  ENRE.EMPR_Codigo = @par_EMPR_Codigo  
  AND ENRE.Numero = @par_Numero 
END 
GO 