﻿Print 'Catalogo 49 Rango Pesos Valor Kilos'
GO
DELETE Valor_Catalogos WHERE CATA_Codigo = 49
GO
DELETE Configuracion_Catalogos WHERE CATA_Codigo = 49
GO
DELETE Catalogos WHERE Codigo = 49
GO
------------------------------------------CATALOGOS------------------------------------------------------
INSERT INTO Catalogos VALUES (1,49,'RPVK ','Rango Pesos Valor Kilo',0,2,1)
GO
-----------------------------------------VALOR CATALOGOS--------------------------------------------------
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1, USUA_Codigo_Crea,Fecha_Crea) 
VALUES 
(1,49,4900,UPPER('(No Aplica)'),1,GETDATE()),
(1,49,4901,UPPER('1–5 Kilos'),1,GETDATE()),
(1,49,4902,UPPER('6-10 Kilos'),1,GETDATE()),
(1,49,4903,UPPER('11-20 Kilos'),1,GETDATE()),
(1,49,4904,UPPER('20-30 Kilos'),1,GETDATE())

GO
INSERT INTO configuracion_catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
VALUES 
(1,49,1,'Codigo',1,1,1,1,0,0,0,0,0,1,0),
(1,49,2,'Nombre',1,1,1,1,0,0,0,0,0,1,0)
GO 