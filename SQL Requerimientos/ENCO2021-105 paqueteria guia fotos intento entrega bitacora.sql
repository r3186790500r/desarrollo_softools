USE [ENCOEXPRES_DOCU]
GO

/****** Object:  StoredProcedure [dbo].[gsp_consultar_foto_detalle_distribucion_remesas]    Script Date: 11/02/2022 9:58:50 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_consultar_foto_detalle_distribucion_remesas]  
(      
 @par_EMPR_Codigo SMALLINT,      
 @par_ENRE_Numero NUMERIC,      
 @par_DEDR_Codigo NUMERIC = NULL,
  @par_Entrega_Devolucion SMALLINT = NULL  
)      
AS      
BEGIN      
   SELECT        
   Nombre_Foto,    
   Tipo,       
   Extension,    
   Foto,
   Entrega_Devolucion    
  FROM Foto_Detalle_Distribucion_Remesas      
  WHERE       
  EMPR_Codigo = @par_EMPR_Codigo        
  AND ENRE_Numero = @par_ENRE_Numero  
  AND Entrega_Devolucion = ISNULL(@par_Entrega_Devolucion, Entrega_Devolucion)
  --AND DEDR_ID = ISNULL(@par_DEDR_Codigo, DEDR_ID)  
      
END  
GO


