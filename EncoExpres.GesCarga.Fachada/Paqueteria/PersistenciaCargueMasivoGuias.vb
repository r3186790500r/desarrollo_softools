﻿Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Entidades.Paqueteria
Imports EncoExpres.GesCarga.Repositorio.Paqueteria
Imports EncoExpres.GesCarga.Entidades.Despachos.Remesa
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Entidades.Basico.General

Namespace Paqueteria
    Public NotInheritable Class PersistenciaCargueMasivoGuias
        Implements IPersistenciaBase(Of RemesaPaqueteria)

        Public Function Consultar(filtro As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria) Implements IPersistenciaBase(Of RemesaPaqueteria).Consultar
            Return New RepositorioCargueMasivoGuias().Consultar(filtro)
        End Function


        ''Consultar
        Public Function ConsultarGuiasEtiquetasPreimpresas(filtro As RemesaPaqueteria) As IEnumerable(Of DetalleGuiaPreimpresa)
            Return New RepositorioCargueMasivoGuias().ConsultarGuiasEtiquetasPreimpresas(filtro)
        End Function

        Public Function ConsultarGuiasPlanillaEntrega(filtro As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria)
            Return New RepositorioCargueMasivoGuias().ConsultarGuiasPlanillaEntrega(filtro)
        End Function

        Public Function Insertar(entidad As RemesaPaqueteria) As Long Implements IPersistenciaBase(Of RemesaPaqueteria).Insertar
            Return New RepositorioCargueMasivoGuias().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As RemesaPaqueteria) As Long Implements IPersistenciaBase(Of RemesaPaqueteria).Modificar
            Return New RepositorioCargueMasivoGuias().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As RemesaPaqueteria) As RemesaPaqueteria Implements IPersistenciaBase(Of RemesaPaqueteria).Obtener
            Return New RepositorioCargueMasivoGuias().Obtener(filtro)
        End Function
        Public Function Anular(entidad As RemesaPaqueteria) As Boolean
            Return New RepositorioCargueMasivoGuias().Anular(entidad)
        End Function

        Public Function CumplirGuias(entidad As RemesaPaqueteria) As Boolean
            Return New RepositorioCargueMasivoGuias().CumplirGuias(entidad)
        End Function
        Public Function GenerarPlantilla(filtro As RemesaPaqueteria) As RemesaPaqueteria
            Return New RepositorioCargueMasivoGuias().GenerarPlantilla(filtro)
        End Function
        Public Function GuardarEntrega(Entidad As Remesas) As Long
            Return New RepositorioCargueMasivoGuias().GuardarEntrega(Entidad)
        End Function
        Public Function ConsultarIndicadores(Entidad As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria)
            Return New RepositorioCargueMasivoGuias().ConsultarIndicadores(Entidad)
        End Function
        Public Function AsociarGuiaPlanilla(entidad As RemesaPaqueteria) As Boolean
            Return New RepositorioCargueMasivoGuias().AsociarGuiaPlanilla(entidad)
        End Function
        Public Function ConsultarEstadosRemesa(Entidad As RemesaPaqueteria) As IEnumerable(Of DetalleEstadosRemesa)
            Return New RepositorioCargueMasivoGuias().ConsultarEstadosRemesa(Entidad)
        End Function
        Public Function ObtenerControlEntregas(Entidad As RemesaPaqueteria) As RemesaPaqueteria
            Return New RepositorioCargueMasivoGuias().ObtenerControlEntregas(Entidad)
        End Function
        Public Function ConsultarRemesasPendientesControlEntregas(filtro As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria)
            Return New RepositorioCargueMasivoGuias().ConsultarRemesasPendientesControlEntregas(filtro)
        End Function
        Public Function ConsultarRemesasPorPlanillar(filtro As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria)
            Return New RepositorioCargueMasivoGuias().ConsultarRemesasPorPlanillar(filtro)
        End Function
        Public Function ConsultarRemesasRecogerOficina(filtro As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria)
            Return New RepositorioCargueMasivoGuias().ConsultarRemesasRecogerOficina(filtro)
        End Function
        Public Function ConsultarRemesasRetornoContado(filtro As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria)
            Return New RepositorioCargueMasivoGuias().ConsultarRemesasRetornoContado(filtro)
        End Function
        Public Function ValidarPreimpreso(entidad As RemesaPaqueteria) As Long
            Return New RepositorioCargueMasivoGuias().ValidarPreimpreso(entidad)
        End Function

        Public Function CambiarEstadoRemesasPaqueteria(entidad As RemesaPaqueteria) As Long
            Return New RepositorioCargueMasivoGuias().CambiarEstadoRemesasPaqueteria(entidad)
        End Function
        Public Function ConsultarRemesasPendientesRecibirOficina(filtro As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria)
            Return New RepositorioCargueMasivoGuias().ConsultarRemesasPendientesRecibirOficina(filtro)
        End Function
        Public Function RecibirRemesaPaqueteriaOficinaDestino(entidad As RemesaPaqueteria) As Long
            Return New RepositorioCargueMasivoGuias().RecibirRemesaPaqueteriaOficinaDestino(entidad)
        End Function
        Public Function ConsultarRemesasPorLegalizar(filtro As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria)
            Return New RepositorioCargueMasivoGuias().ConsultarRemesasPorLegalizar(filtro)
        End Function
        Public Function ConsultarRemesasPorLegalizarRecaudo(filtro As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria)
            Return New RepositorioCargueMasivoGuias().ConsultarRemesasPorLegalizarRecaudo(filtro)
        End Function

        Public Function InsertarMasivo(filtro As RemesaPaqueteria) As Long
            Return New RepositorioCargueMasivoGuias().InsertarMasivo(filtro)
        End Function

        Public Function ConsultarTercerosIdentificacion(filtro As Terceros) As IEnumerable(Of Terceros)
            Return New RepositorioCargueMasivoGuias().ConsultarTercerosIdentificacion(filtro)
        End Function


        Public Function ObtenerUltimoConsecutivoTercero(filtro As Terceros) As Terceros
            Return New RepositorioCargueMasivoGuias().ObtenerUltimoConsecutivoTercero(filtro)
        End Function


        Public Function ObtenerUltimaGuia(filtro As RemesaPaqueteria) As RemesaPaqueteria
            Return New RepositorioCargueMasivoGuias().ObtenerUltimaGuia(filtro)
        End Function

        Public Function InsertarNuevosTerceros(filtro As Terceros) As Long
            Return New RepositorioCargueMasivoGuias().InsertarNuevosTerceros(filtro)
        End Function
    End Class

End Namespace

