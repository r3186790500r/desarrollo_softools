﻿Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Mantenimiento
Imports EncoExpres.GesCarga.IFachada
Imports EncoExpres.GesCarga.Fachada.Mantenimiento
Imports EncoExpres.GesCarga.Mensajes.My.Resources

Namespace Mantenimiento


    Public NotInheritable Class LogicaDetalleTareaMantenimiento
        Inherits LogicaBase(Of DetalleTareaMantenimiento)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of DetalleTareaMantenimiento)

        Sub New(persistencia As IPersistenciaBase(Of DetalleTareaMantenimiento))
            _persistencia = persistencia

        End Sub

        Public Overrides Function Consultar(filtro As DetalleTareaMantenimiento) As Respuesta(Of IEnumerable(Of DetalleTareaMantenimiento))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of DetalleTareaMantenimiento))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of DetalleTareaMantenimiento))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As DetalleTareaMantenimiento) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            If entidad.Numero.Equals(0) Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If


            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo.Equals(0), String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As DetalleTareaMantenimiento) As Respuesta(Of DetalleTareaMantenimiento)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of DetalleTareaMantenimiento)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of DetalleTareaMantenimiento)(consulta)
        End Function

        Public Function Anular(entidad As DetalleTareaMantenimiento) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False


            anulo = CType(_persistencia, PersistenciaDetalleTareaMantenimiento).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, Recursos.OperacionAnulo)}
        End Function

        Private Function DatosRequeridos(entidad As DetalleTareaMantenimiento) As Respuesta(Of DetalleTareaMantenimiento)
            Throw New NotImplementedException()
        End Function
    End Class
End Namespace
