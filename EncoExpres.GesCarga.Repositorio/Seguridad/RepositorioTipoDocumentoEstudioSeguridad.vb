﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Seguridad

Namespace Seguridad

    ''' <summary>
    ''' Clase <see cref="RepositorioTipoDocumentoEstudioSeguridad"/>
    ''' </summary>
    Public NotInheritable Class RepositorioTipoDocumentoEstudioSeguridad
        Inherits RepositorioBase(Of TipoDocumentoEstudioSeguridad)

        Public Overrides Function Consultar(filtro As TipoDocumentoEstudioSeguridad) As IEnumerable(Of TipoDocumentoEstudioSeguridad)
            Dim lista As New List(Of TipoDocumentoEstudioSeguridad)
            Dim item As TipoDocumentoEstudioSeguridad

            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                If filtro.EOESCodigo > 0 Then
                    conexion.AgregarParametroSQL("@par_EOES_Codigo", filtro.EOESCodigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_tipo_documento_estudio_seguridad]")

                While resultado.Read
                    item = New TipoDocumentoEstudioSeguridad(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As TipoDocumentoEstudioSeguridad) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As TipoDocumentoEstudioSeguridad) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As TipoDocumentoEstudioSeguridad) As TipoDocumentoEstudioSeguridad
            Throw New NotImplementedException()
        End Function

    End Class

End Namespace