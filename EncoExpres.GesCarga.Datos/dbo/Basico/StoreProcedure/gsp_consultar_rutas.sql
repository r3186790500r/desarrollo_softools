﻿PRINT 'gsp_consultar_rutas'
GO
DROP PROCEDURE gsp_consultar_rutas
GO
CREATE PROCEDURE gsp_consultar_rutas  
(  
 @par_EMPR_Codigo SMALLINT,  
 @par_Codigo NUMERIC = NULL,  
 @par_Codigo_Alterno VARCHAR(20) = NULL,  
 @par_Nombre VARCHAR(50) = NULL,  
 @par_Ciudad_Origen VARCHAR(50) = NULL,  
 @par_Ciudad_Destino VARCHAR(50) = NULL,  
 @par_Codigo_Tipo_Ruta NUMERIC = NULL,  
 @par_Estado SMALLINT = NULL,  
 @par_NumeroPagina INT = NULL,  
 @par_RegistrosPagina INT = NULL  
)  
AS  
BEGIN  
 SET NOCOUNT ON;  
  DECLARE  
   @CantidadRegistros INT  
  SELECT @CantidadRegistros = (  
    SELECT DISTINCT   
     COUNT(1)   
    FROM  
     Rutas RUTA  
     LEFT JOIN Valor_Catalogos as TIRU  
     ON  RUTA.EMPR_Codigo = TIRU.EMPR_Codigo   
     AND RUTA.CATA_TIRU_Codigo = TIRU.Codigo  
     ,  
     Ciudades CIOR,  
     Ciudades CIDE  
    WHERE  
     RUTA.Codigo <> 0  
  
     AND RUTA.EMPR_Codigo = CIOR.EMPR_Codigo   
     AND RUTA.CIUD_Codigo_Origen = CIOR.Codigo  
  
     AND RUTA.EMPR_Codigo = CIDE.EMPR_Codigo   
     AND RUTA.CIUD_Codigo_Destino = CIDE.Codigo  
  
     AND RUTA.EMPR_Codigo = @par_EMPR_Codigo  
     AND RUTA.Codigo = ISNULL(@par_Codigo, RUTA.Codigo)  
     AND RUTA.Estado = ISNULL(@par_Estado, RUTA.Estado)  
	 AND RUTA.CATA_TIRU_Codigo = ISNULL(@par_Codigo_Tipo_Ruta, RUTA.CATA_TIRU_Codigo)
     AND ((RUTA.Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))  
     AND ((RUTA.Codigo_Alterno LIKE '%' + @par_Codigo_Alterno + '%') OR (@par_Codigo_Alterno IS NULL))  
     AND ((CIOR.Nombre LIKE '%' + @par_Ciudad_Origen + '%') OR (@par_Ciudad_Origen IS NULL))  
     AND ((CIDE.Nombre LIKE '%' + @par_Ciudad_Destino + '%') OR (@par_Ciudad_Destino IS NULL))  
     );  
              
    WITH Pagina AS  
    (  
  
    SELECT  
     0 AS Obtener,  
     RUTA.EMPR_Codigo,  
     RUTA.Codigo,  
     RUTA.Codigo_Alterno,  
     RUTA.Nombre,  
     RUTA.CIUD_Codigo_Origen,  
     RUTA.CIUD_Codigo_Destino,  
	 RUTA.Duracion_Horas,
     RUTA.Estado,  
     CIOR.Nombre AS CiudadOrigen,  
     CIDE.Nombre AS CiudadDestino,  
     ISNULL(RUTA.CATA_TIRU_Codigo,4400) AS CATA_TIRU_Codigo ,  
     ISNULL(TIRU.Campo1,'') as TipoRuta,  
     ROW_NUMBER() OVER(ORDER BY RUTA.Nombre) AS RowNumber  
    FROM  
     Rutas RUTA  
     LEFT JOIN Valor_Catalogos as TIRU  
     ON  RUTA.EMPR_Codigo = TIRU.EMPR_Codigo   
     AND RUTA.CATA_TIRU_Codigo = TIRU.Codigo  
     ,  
     Ciudades CIOR,  
     Ciudades CIDE  
    WHERE  
     RUTA.Codigo <> 0  
  
     AND RUTA.EMPR_Codigo = CIOR.EMPR_Codigo   
     AND RUTA.CIUD_Codigo_Origen = CIOR.Codigo  
  
     AND RUTA.EMPR_Codigo = CIDE.EMPR_Codigo   
     AND RUTA.CIUD_Codigo_Destino = CIDE.Codigo  
  
       
     AND RUTA.EMPR_Codigo = @par_EMPR_Codigo  
     AND RUTA.Codigo = ISNULL(@par_Codigo, RUTA.Codigo) 
	 AND RUTA.CATA_TIRU_Codigo = ISNULL(@par_Codigo_Tipo_Ruta, RUTA.CATA_TIRU_Codigo) 
     AND RUTA.Estado = ISNULL(@par_Estado, RUTA.Estado)  
     AND ((RUTA.Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))  
     AND ((RUTA.Codigo_Alterno LIKE '%' + @par_Codigo_Alterno + '%') OR (@par_Codigo_Alterno IS NULL))  
     AND ((CIOR.Nombre LIKE '%' + @par_Ciudad_Origen + '%') OR (@par_Ciudad_Origen IS NULL))  
     AND ((CIDE.Nombre LIKE '%' + @par_Ciudad_Destino + '%') OR (@par_Ciudad_Destino IS NULL))  
  )  
  SELECT DISTINCT  
  0 AS Obtener,  
  EMPR_Codigo,  
  Codigo,  
  Codigo_Alterno,  
  Nombre,  
  CIUD_Codigo_Origen,  
  CIUD_Codigo_Destino, 
  Duracion_Horas, 
  Estado,  
  CiudadOrigen,  
  CiudadDestino,  
  CATA_TIRU_Codigo,  
  TipoRuta,  
  @CantidadRegistros AS TotalRegistros,  
  @par_NumeroPagina AS PaginaObtener,  
  @par_RegistrosPagina AS RegistrosPagina  
  FROM  
   Pagina  
  WHERE  
   RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
   AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
  order by Nombre  
END 
GO