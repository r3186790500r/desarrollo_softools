﻿EncoExpresApp.controller("NovedadesDespachoCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'PlanillaDespachosFactory', 'ValorCatalogosFactory', 'blockUI',
    function ($scope, $routeParams, $timeout, $linq, PlanillaDespachosFactory, ValorCatalogosFactory, blockUI) {
        $scope.MapaSitio = [{ Nombre: 'Despachos' }, { Nombre: 'Procesos' }, { Nombre: 'Novedades Despacho' }];


        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        $scope.Buscando = false;
        $scope.ResultadoSinRegistros = '';
        $scope.MensajesError = [];
        var filtros = {};
        $scope.ListadoEstados = [];

        $scope.MensajesErrorAnula = [];
        $scope.MostrarMensajeError = false
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.Anulado = 0;
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_NOVEDADES_DESPACHOS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };
        /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/

        /*Metodo buscar*/
        $scope.Buscar = function () {

            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }

        };

        /*-------------------------------------------------------------------------------------------Funcion Buscar----------------------------------------------------------------*/
        function Find() {
            $scope.DatosRequeridos();
            if (DatosRequeridos == true) {
                blockUI.start('Buscando registros ...');

                $timeout(function () {
                    blockUI.message('Espere por favor ...');
                }, 100);

                $scope.Buscando = true;
                $scope.MensajesError = [];

                $scope.ListadoPlanillaDespacho = [];

                filtros = {
                    Pagina: $scope.paginaActual,
                    RegistrosPagina: $scope.cantidadRegistrosPorPagina,
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroInicial: $scope.ModeloNumeroinicial,
                    NumeroFinal: $scope.ModeloNumerofinal,
                    FechaInicial: $scope.ModeloFechaInicial,
                    FechaFinal: $scope.ModeloFechaFinal,
                    TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO,
                    Estado: { Codigo: ESTADO_DEFINITIVO }
                };

                if ($scope.MensajesError.length == 0) {
                    blockUI.delay = 1000;
                    PlanillaDespachosFactory.Consultar(filtros).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.length > 0) {
                                    $scope.ListadoPlanillaDespacho = response.data.Datos;
                                    $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                    $scope.Buscando = true;
                                    $scope.ResultadoSinRegistros = '';
                                }
                                else {
                                    $scope.ListadoPlanillaDespacho = "";
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }
                            }
                        }, function (response) {
                            $scope.Buscando = false;
                        });
                }
                else {
                    $scope.Buscando = false;
                }
                blockUI.stop();
            }
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.Anular = function (Numero, NumeroDocumento) {
            $scope.NumeroDocumento = NumeroDocumento
            $scope.ModalErrorCompleto = ''
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false
            showModal('modalNovedades');
        };

        $scope.ConfirmaAnular = function () {
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                CausaAnulacion: $scope.ModeloCausaAnula,
                Codigo: $scope.Codigo,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO,
            };

            if ($scope.ModeloCausaAnula == '' || $scope.ModeloCausaAnula == undefined || $scope.ModeloCausaAnula == null) {
                $scope.MensajesErrorAnula.push('Debe ingresar un causa');
            } else {

                PlanillaDespachosFactory.Anular(entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            ShowSuccess('Se Anulo la Planilla de Despacho: ' + $scope.NumeroDocumento);
                            closeModal('modalAnular');
                            closeModal('modalAnular');
                            Find();
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        var result = ''
                        showModal('modalMensajePlanillaDespacho');
                        result = InternalServerError(response.statusText)
                        $scope.ModalError = 'No se puede anular la Planilla de entrega' + $scope.Nombre + ' ya que se encuentra relacionada con ' + result;
                        $scope.ModalErrorCompleto = response.statusText

                    });
            }
        };

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /* Obtener parametros*/
        if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
            $scope.Codigo = $routeParams.Codigo;
            Find();
        }

        $scope.CerrarModal = function () {
            closeModal('modalAnularImpuesto');
            closeModal('modalMensajeAnuloImpuesto');
        }

        $scope.ArmarFiltro = function () {
            if ($scope.NumeroDocumento != undefined && $scope.NumeroDocumento != '') {
                $scope.FiltroArmado = '&Numero=' + $scope.NumeroDocumento;
            }

        }

        $scope.DatosRequeridos = function () {
            DatosRequeridos = true;
            $scope.MensajesError = [];
            $scope.Rango = $scope.ModeloNumerofinal - $scope.ModeloNumeroinicial;

            if (($scope.ModeloFechaInicial == null || $scope.ModeloFechaInicial == undefined || $scope.ModeloFechaInicial == '')
                && ($scope.ModeloFechaFinal == null || $scope.ModeloFechaFinal == undefined || $scope.ModeloFechaFinal == '')
                && ($scope.ModeloNumeroinicial == null || $scope.ModeloNumeroinicial == undefined || $scope.ModeloNumeroinicial == '' || $scope.ModeloNumeroinicial == 0)
                && ($scope.ModeloNumerofinal == null || $scope.ModeloNumerofinal == undefined || $scope.ModeloNumerofinal == '' || $scope.ModeloNumerofinal == 0)
            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de números o fechas');
                DatosRequeridos = false

            } else if (($scope.ModeloNumeroinicial !== null && $scope.ModeloNumeroinicial !== undefined && $scope.ModeloNumeroinicial !== '' && $scope.ModeloNumeroinicial !== 0)
                || ($scope.ModeloNumerofinal !== null && $scope.ModeloNumerofinal !== undefined && $scope.ModeloNumerofinal !== '' && $scope.ModeloNumerofinal !== 0)
                || ($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')
                || ($scope.ModeloFechaFinal !== null && $scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '')

            ) {
                if (($scope.ModeloNumeroinicial !== null && $scope.ModeloNumeroinicial !== undefined && $scope.ModeloNumeroinicial !== '' && $scope.ModeloNumeroinicial !== 0)
                    && ($scope.ModeloNumerofinal !== null && $scope.ModeloNumerofinal !== undefined && $scope.ModeloNumerofinal !== '' && $scope.ModeloNumerofinal !== 0)) {
                    if ($scope.ModeloNumerofinal < $scope.ModeloNumeroinicial) {
                        $scope.MensajesError.push('El número final debe ser mayor al número final');
                        DatosRequeridos = false
                    }
                    else if ($scope.Rango > RANGO_MAXIMO_CANTIDAD_DOCUMENTOS_LISTADOS) {
                        $scope.MensajesError.push('La diferencia del rango entre el numero inicial y el numero final no puede ser mayor a 2000');
                        DatosRequeridos = false
                    }
                } else {
                    if (($scope.ModeloNumeroinicial !== null && $scope.ModeloNumeroinicial !== undefined && $scope.ModeloNumeroinicial !== '' && $scope.ModeloNumeroinicial !== 0)) {
                        $scope.ModeloNumerofinal = $scope.ModeloNumeroinicial
                    } else {
                        $scope.ModeloNumeroinicial = $scope.ModeloNumerofinal
                    }
                }
                if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')
                    && ($scope.ModeloFechaFinal !== null && $scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '')) {
                    if ($scope.ModeloFechaFinal < $scope.ModeloFechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        DatosRequeridos = false
                    } else if ((($scope.ModeloFechaFinal - $scope.ModeloFechaInicial) / (1000 * 60 * 60 * 24)) > 60) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a 60 dias');
                        DatosRequeridos = false
                    }
                }

                else {
                    if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')) {
                        $scope.ModeloFechaFinal = $scope.ModeloFechaInicial
                    } else {
                        $scope.ModeloFechaInicial = $scope.ModeloFechaFinal
                    }
                }
            }

            return DatosRequeridos;

        }

    }]);