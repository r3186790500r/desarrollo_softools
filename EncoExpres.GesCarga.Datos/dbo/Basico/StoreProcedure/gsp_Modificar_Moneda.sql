﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */

	PRINT 'gsp_Modificar_Moneda'
GO
DROP PROCEDURE gsp_Modificar_Moneda
GO
CREATE PROCEDURE gsp_Modificar_Moneda(  
@par_EMPR_Codigo NUMERIC,  
@par_Codigo NUMERIC,  
@par_Nombre VARCHAR(20),   
@par_Nombre_Corto VARCHAR(10),   
@par_Simbolo VARCHAR(5),   
@par_Local SMALLINT,   
@par_Estado SMALLINT)  
  
AS BEGIN   
UPDATE Monedas SET Nombre=@par_Nombre  
,Nombre_Corto=@par_Nombre_Corto  
,Simbolo=@par_Simbolo  
,Local=@par_Local  
,Estado=@par_Estado  
WHERE  EMPR_Codigo= @par_EMPR_Codigo 
AND Codigo= @par_Codigo  
END  
GO