﻿PRINT 'gsp_eliminar_orden_servicio_documentos'
GO
DROP PROCEDURE gsp_eliminar_orden_servicio_documentos
GO
CREATE PROCEDURE gsp_eliminar_orden_servicio_documentos 
(      
@par_EMPR_Codigo SMALLINT,  
@par_TIDO_Codigo SMALLINT,    
@par_CDGD_Codigo NUMERIC,  
@par_Numero NUMERIC    
)      
AS      
 BEGIN      
   DELETE Gestion_Documental_Documentos       
   WHERE       
   EMPR_Codigo =  @par_EMPR_Codigo      
   AND CDGD_Codigo = @par_CDGD_Codigo  
   AND TIDO_Codigo = @par_TIDO_Codigo     
   AND Numero = @par_Numero     
       
 SELECT Codigo = @par_CDGD_Codigo      
END      
GO