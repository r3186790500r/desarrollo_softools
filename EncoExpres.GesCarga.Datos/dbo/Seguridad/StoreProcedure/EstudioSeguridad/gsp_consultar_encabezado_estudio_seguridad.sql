﻿PRINT 'gsp_consultar_encabezado_estudio_seguridad'
GO
DROP PROCEDURE gsp_consultar_encabezado_estudio_seguridad
GO
CREATE PROCEDURE gsp_consultar_encabezado_estudio_seguridad 
(   
  @par_EMPR_Codigo SMALLINT,           
  @par_Numero_Documento NUMERIC = NULL,     
  @par_Fecha_Inicial DATETIME =NULL,                          
  @par_Fecha_Final DATETIME =NULL,                  
  @par_OFIC_Codigo NUMERIC = NULL,     
  @par_Placa VARCHAR(50) = NULL,   
  @par_Nombre_Propietario VARCHAR(100) = NULL,            
  @par_Nombre_Conductor VARCHAR(100) = NULL,      
  @par_Nombre_Tenedor VARCHAR(100) = NULL,   
  @par_Usuario_Solicito VARCHAR(50) = NULL,             
  @par_Usuario_Estudio VARCHAR(50) = NULL,        
  @par_Estado_Autorizacion SMALLINT = NULL,  
  @par_Estado SMALLINT = NULL,     
  @par_Anulado SMALLINT = NULL,   
  @par_Numero_Autorizacion NUMERIC = NULL,         
  @par_NumeroPagina INT = NULL,           
  @par_RegistrosPagina INT = NULL    
           
  )          
AS          
BEGIN        
    
 set @par_Fecha_Inicial = dbo.Func_Fecha_Quita_Segundos(@par_Fecha_Inicial)                     
 set @par_Fecha_Inicial = DATEADD(MINUTE, -(DATEPART(MINUTE,@par_Fecha_Inicial)),@par_Fecha_Inicial)                            
 set @par_Fecha_Inicial = DATEADD(HOUR, -(DATEPART(HOUR,@par_Fecha_Inicial)),@par_Fecha_Inicial)                              
 set @par_Fecha_Final = dbo.Func_Fecha_Quita_Segundos(@par_Fecha_Final)                          
 set @par_Fecha_Final = DATEADD(MINUTE, -(DATEPART(MINUTE,@par_Fecha_Final)),@par_Fecha_Final)                            
 set @par_Fecha_Final = DATEADD(HOUR, -(DATEPART(HOUR,@par_Fecha_Final)),@par_Fecha_Final)                            
 set @par_Fecha_Final = DATEADD(MILLISECOND, 998,@par_Fecha_Final)                            
 set @par_Fecha_Final = DATEADD(SECOND,59,@par_Fecha_Final)                            
 set @par_Fecha_Final = DATEADD(MINUTE,59,@par_Fecha_Final)                            
 set @par_Fecha_Final = DATEADD(HOUR,23,@par_Fecha_Final)       
      
  SET NOCOUNT ON;          
  DECLARE @CantidadRegistros INT          
               
  BEGIN          
   SELECT @CantidadRegistros = (          
    SELECT DISTINCT           
    COUNT(1)           
     FROM           
    
    Encabezado_Estudio_Seguridad ENES              
         
     INNER JOIN Oficinas AS OFIC                       
     ON ENES.EMPR_Codigo = OFIC.EMPR_Codigo                        
     AND ENES.OFIC_Codigo = OFIC.Codigo        
                 
     LEFT JOIN Usuarios AS USSO                       
     ON ENES.EMPR_Codigo = USSO.EMPR_Codigo                        
     AND ENES.USUA_Codigo_Crea = USSO.Codigo    
    
     LEFT JOIN Usuarios AS USES                       
     ON ENES.EMPR_Codigo = USES.EMPR_Codigo                        
     AND ENES.USUA_Codigo_Estudio_Seguridad = USES.Codigo        
    
   WHERE                
     ENES.EMPR_Codigo = @par_EMPR_Codigo         
     AND ENES.Numero_Documento = ISNULL(@par_Numero_Documento, ENES.Numero_Documento)     
     AND ENES.Fecha >= (ISNULL(@par_Fecha_Inicial, ENES.Fecha))                            
     AND ENES.Fecha <= (ISNULL(@par_Fecha_Final, ENES.Fecha))          
     AND OFIC.Codigo = ISNULL(@par_OFIC_Codigo, OFIC.Codigo)    
     AND ((ENES.Placa LIKE '%' + RTRIM(LTRIM(@par_Placa)) + '%' OR (@par_Placa IS NULL)))
	 AND(ISNULL(ENES.Nombre_Propietario, '') + ' ' + ISNULL(ENES.Apellido1_Propietario, '') + ' ' + ISNULL(ENES.Apellido2_Propietario, '') + ' ' + ISNULL(ENES.Razon_Social_Propietario, '')  LIKE             
     CONCAT('%', RTRIM(LTRIM  (ISNULL(@par_Nombre_Propietario, ISNULL(ENES.Nombre_Propietario, '') + ' ' + ISNULL(ENES.Apellido1_Propietario, '') + ' ' + ISNULL(ENES.Apellido2_Propietario, '') + ' ' + ISNULL(ENES.Razon_Social_Propietario, '')))), '%'))
	 AND(ISNULL(ENES.Nombre_Tenedor, '') + ' ' + ISNULL(ENES.Apellido1_Tenedor, '') + ' ' + ISNULL(ENES.Apellido2_Tenedor, '') + ' ' + ISNULL(ENES.Razon_Social_Tenedor, '')  LIKE             
     CONCAT('%', RTRIM(LTRIM  (ISNULL(@par_Nombre_Tenedor, ISNULL(ENES.Nombre_Tenedor, '') + ' ' + ISNULL(ENES.Apellido1_Tenedor, '') + ' ' + ISNULL(ENES.Apellido2_Tenedor, '') + ' ' + ISNULL(ENES.Razon_Social_Tenedor, '')))), '%'))
	 AND(ISNULL(ENES.Nombre_Conductor, '') + ' ' + ISNULL(ENES.Apellido1_Conductor, '') + ' ' + ISNULL(ENES.Apellido2_Conductor, '') + ' ' + ISNULL(ENES.Razon_Social_Conductor, '')  LIKE             
     CONCAT('%', RTRIM(LTRIM  (ISNULL(@par_Nombre_Conductor, ISNULL(ENES.Nombre_Conductor, '') + ' ' + ISNULL(ENES.Apellido1_Conductor, '') + ' ' + ISNULL(ENES.Apellido2_Conductor, '') + ' ' + ISNULL(ENES.Razon_Social_Conductor, '')))), '%'))     
     AND ((USSO.Nombre LIKE '%' + RTRIM(LTRIM(@par_Usuario_Solicito)) + '%' OR (@par_Usuario_Solicito IS NULL)))         
     AND ((USES.Nombre LIKE '%' + RTRIM(LTRIM(@par_Usuario_Estudio)) + '%' OR (@par_Usuario_Estudio IS NULL)))   
     AND ENES.CATA_ESES_Codigo = ISNULL(@par_Estado_Autorizacion, ENES.CATA_ESES_Codigo)      
     AND ENES.Estado = ISNULL(@par_Estado, ENES.Estado)          
     AND ENES.Anulado = ISNULL(@par_Anulado, ENES.Anulado)       
     AND ENES.Numero_Autorizacion = ISNULL(@par_Numero_Autorizacion, ENES.Numero_Autorizacion)      
   );                 
  WITH Pagina AS          
  (          
  SELECT DISTINCT     
      0 AS Obtener    
     ,ENES.EMPR_Codigo  
     ,ENES.Numero    
     ,ENES.Numero_Documento          
     ,ENES.Fecha            
     ,ISNULL(ENES.Fecha, '') AS Fecha_Solicitud     
     ,ISNULL(USSO.Nombre, '') AS Usuario_Solicitud  
     ,ISNULL(ENES.Placa, '') AS Placa   
	 ,ISNULL(ENES.Razon_Social_Propietario, '')+ISNULL(ENES.Nombre_Propietario, '')+' '+ ISNULL(ENES.Apellido1_Propietario, '')+ ' ' +ISNULL(ENES.Apellido2_Propietario, '') AS Nombre_Propietario 
	 ,ISNULL(ENES.Razon_Social_Conductor, '')+ISNULL(ENES.Nombre_Conductor, '')+' '+ ISNULL(ENES.Apellido1_Conductor, '')+ ' ' +ISNULL(ENES.Apellido2_Conductor, '') AS Nombre_Conductor       
     ,ISNULL(ENES.Razon_Social_Tenedor, '')+ISNULL(ENES.Nombre_Tenedor, '')+' '+ ISNULL(ENES.Apellido1_Tenedor, '')+ ' ' +ISNULL(ENES.Apellido2_Tenedor, '') AS Nombre_Tenedor        
     ,ISNULL(ENES.Fecha_Estudio_Seguridad, '') AS Fecha_Estudio    
     ,ISNULL(USES.Nombre, '') AS Usuario_Estudio  
     ,ISNULL(ENES.CATA_ESES_Codigo, 0) AS Estado_Solicitud    
     ,ISNULL(ENES.Numero_Autorizacion, 0) AS Numero_Autorizacion          
     ,CASE WHEN ENES.Estado = 1 AND ENES.Anulado = 0 THEN 1       
      WHEN ENES.Estado = 0 AND ENES.Anulado = 0 THEN 0     
      WHEN ENES.Anulado = 1 THEN 2 END AS Estado               
     ,ROW_NUMBER() OVER(ORDER BY ENES.Numero) AS RowNumber          
                 
  FROM           
    
    Encabezado_Estudio_Seguridad ENES              
         
     INNER JOIN Oficinas AS OFIC                       
     ON ENES.EMPR_Codigo = OFIC.EMPR_Codigo                        
     AND ENES.OFIC_Codigo = OFIC.Codigo        
                 
     LEFT JOIN Usuarios AS USSO                       
     ON ENES.EMPR_Codigo = USSO.EMPR_Codigo                        
     AND ENES.USUA_Codigo_Crea = USSO.Codigo    
    
     LEFT JOIN Usuarios AS USES                       
     ON ENES.EMPR_Codigo = USES.EMPR_Codigo                        
     AND ENES.USUA_Codigo_Estudio_Seguridad = USES.Codigo        
    
   WHERE                
     ENES.EMPR_Codigo = @par_EMPR_Codigo               
     AND ENES.Numero_Documento = ISNULL(@par_Numero_Documento, ENES.Numero_Documento)       
     AND ENES.Fecha >= (ISNULL(@par_Fecha_Inicial, ENES.Fecha))                            
     AND ENES.Fecha <= (ISNULL(@par_Fecha_Final, ENES.Fecha))          
     AND OFIC.Codigo = ISNULL(@par_OFIC_Codigo, OFIC.Codigo)    
     AND ((ENES.Placa LIKE '%' + RTRIM(LTRIM(@par_Placa)) + '%' OR (@par_Placa IS NULL)))    
	 AND(ISNULL(ENES.Nombre_Propietario, '') + ' ' + ISNULL(ENES.Apellido1_Propietario, '') + ' ' + ISNULL(ENES.Apellido2_Propietario, '') + ' ' + ISNULL(ENES.Razon_Social_Propietario, '')  LIKE             
     CONCAT('%', RTRIM(LTRIM  (ISNULL(@par_Nombre_Propietario, ISNULL(ENES.Nombre_Propietario, '') + ' ' + ISNULL(ENES.Apellido1_Propietario, '') + ' ' + ISNULL(ENES.Apellido2_Propietario, '') + ' ' + ISNULL(ENES.Razon_Social_Propietario, '')))), '%'))
	 AND(ISNULL(ENES.Nombre_Tenedor, '') + ' ' + ISNULL(ENES.Apellido1_Tenedor, '') + ' ' + ISNULL(ENES.Apellido2_Tenedor, '') + ' ' + ISNULL(ENES.Razon_Social_Tenedor, '')  LIKE             
     CONCAT('%', RTRIM(LTRIM  (ISNULL(@par_Nombre_Tenedor, ISNULL(ENES.Nombre_Tenedor, '') + ' ' + ISNULL(ENES.Apellido1_Tenedor, '') + ' ' + ISNULL(ENES.Apellido2_Tenedor, '') + ' ' + ISNULL(ENES.Razon_Social_Tenedor, '')))), '%'))
	 AND(ISNULL(ENES.Nombre_Conductor, '') + ' ' + ISNULL(ENES.Apellido1_Conductor, '') + ' ' + ISNULL(ENES.Apellido2_Conductor, '') + ' ' + ISNULL(ENES.Razon_Social_Conductor, '')  LIKE             
     CONCAT('%', RTRIM(LTRIM  (ISNULL(@par_Nombre_Conductor, ISNULL(ENES.Nombre_Conductor, '') + ' ' + ISNULL(ENES.Apellido1_Conductor, '') + ' ' + ISNULL(ENES.Apellido2_Conductor, '') + ' ' + ISNULL(ENES.Razon_Social_Conductor, '')))), '%'))          
     AND ((USSO.Nombre LIKE '%' + RTRIM(LTRIM(@par_Usuario_Solicito)) + '%' OR (@par_Usuario_Solicito IS NULL)))         
     AND ((USES.Nombre LIKE '%' + RTRIM(LTRIM(@par_Usuario_Estudio)) + '%' OR (@par_Usuario_Estudio IS NULL)))   
     AND ENES.CATA_ESES_Codigo = ISNULL(@par_Estado_Autorizacion, ENES.CATA_ESES_Codigo)      
     AND ENES.Estado = ISNULL(@par_Estado, ENES.Estado)          
     AND ENES.Anulado = ISNULL(@par_Anulado, ENES.Anulado)       
     AND ENES.Numero_Autorizacion = ISNULL(@par_Numero_Autorizacion, ENES.Numero_Autorizacion)    
    )          
          
   SELECT      
      Obtener    
     ,EMPR_Codigo  
     ,Numero    
     ,Numero_Documento          
     ,Fecha            
     ,Fecha_Solicitud     
     ,Usuario_Solicitud  
     ,Placa      
     ,Nombre_Propietario         
     ,Nombre_Conductor          
     ,Nombre_Tenedor       
     ,Fecha_Estudio  
     ,Usuario_Estudio  
     ,Estado_Solicitud    
     ,Numero_Autorizacion         
     ,Estado    
     ,@CantidadRegistros AS TotalRegistros          
     ,@par_NumeroPagina AS PaginaObtener          
     ,@par_RegistrosPagina AS RegistrosPagina          
   FROM          
    Pagina          
   WHERE          
    RowNumber > (ISNULL(@par_NumeroPagina,1) -1) * ISNULL(@par_RegistrosPagina,10000)          
    AND RowNumber <=ISNULL(@par_NumeroPagina,1) *ISNULL( @par_RegistrosPagina,10000)          
   ORDER BY Numero ASC          
  END          
          
END          
GO  