﻿EncoExpresApp.factory('PlanillaDespachosFactory', ['$http', function ($http) {

    var urlService = GetUrl('PlanillaDespachos');
    var factory = {};
    var request = {
        method: '',
        url: '',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        data: {}
    }

    factory.Consultar = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Consultar';
        request.data = filtro; if (filtro.Sync == true) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Obtener = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Obtener';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };

    factory.Anular = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Anular';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Guardar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Guardar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.GenerarManifiesto = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/GenerarManifiesto';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Obtener_Detalle_Tiempos = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Obtener_Detalle_Tiempos';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Obtener_Detalle_Tiempos_Remesas = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Obtener_Detalle_Tiempos_Remesas';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.Insertar_Detalle_Tiempos = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Insertar_Detalle_Tiempos';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.ConsultarListaPlanillas = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarListaPlanillas';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    
    factory.ActualizarEntregayDTRemesasporPlanilla = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ActualizarEntregayDTRemesasporPlanilla';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    return factory;
}]);