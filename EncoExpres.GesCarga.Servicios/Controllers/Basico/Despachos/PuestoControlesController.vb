﻿Imports System.Web.Http
Imports EncoExpres.GesCarga.Entidades
Imports EncoExpres.GesCarga.Entidades.Basico.Despachos
Imports EncoExpres.GesCarga.Negocio.Basico.Despachos

''' <summary>
''' Controlador <see cref="PuestoControlesController"/>
''' </summary>
<Authorize>
Public Class PuestoControlesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As PuestoControles) As Respuesta(Of IEnumerable(Of PuestoControles))
        Return New LogicaPuestoControles(CapaPersistenciaPuestoControles).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As PuestoControles) As Respuesta(Of PuestoControles)
        Return New LogicaPuestoControles(CapaPersistenciaPuestoControles).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As PuestoControles) As Respuesta(Of Long)
        Return New LogicaPuestoControles(CapaPersistenciaPuestoControles).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As PuestoControles) As Respuesta(Of Boolean)
        Return New LogicaPuestoControles(CapaPersistenciaPuestoControles).Anular(entidad)
    End Function
End Class
