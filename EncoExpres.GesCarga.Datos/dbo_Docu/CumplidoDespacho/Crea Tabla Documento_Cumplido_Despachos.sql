﻿Print 'Crea Tabla T_Documento_Cumplido_Despachos'
GO
CREATE TABLE [dbo].[T_Documento_Cumplido_Despachos](
	[EMPR_Codigo] [smallint] NOT NULL,
	[ID] [numeric](18, 0) NOT NULL,
	[USUA_Codigo] [numeric](18, 0) NOT NULL,
	[ENCU_Numero] [numeric](18, 0) NOT NULL,
	[Nombre] [varchar](100) NULL,
	[NombreArchivo] [varchar](200) NOT NULL,
	[Archivo] [varbinary](max)	NULL,
	[ExtensionArchivo] [varchar](20) NOT NULL,
	[Tipo] [varchar](200) NULL,
	[Fecha_Crea] [datetime] NOT NULL,
 CONSTRAINT [PK_T_Documento_Cumplido_Despachos] PRIMARY KEY CLUSTERED 
(
	[EMPR_Codigo] ASC,
	[ID] ASC,
	[USUA_Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
------------------------------------------------------------------------------------------------------

Print 'Crea Tabla Documento_Cumplido_Despachos'
GO
CREATE TABLE [dbo].[Documento_Cumplido_Despachos](
	[EMPR_Codigo] [smallint] NOT NULL,
	[ID] [numeric](18, 0) NOT NULL,
	[ENCU_Numero] [numeric](18, 0) NOT NULL,
	[Nombre] [varchar](100) NULL,
	[NombreArchivo] [varchar](200) NOT NULL,
	[Archivo] [varbinary](max)	NULL,
	[ExtensionArchivo] [varchar](20) NOT NULL,
	[Tipo] [varchar](200) NULL,
	[Fecha_Crea] [datetime] NOT NULL,
	[USUA_Codigo_Crea] [datetime] NOT NULL,
 CONSTRAINT [PK_Documento_Cumplido_Despachos] PRIMARY KEY CLUSTERED 
(
	[EMPR_Codigo] ASC,
	[ID] ASC,
	[ENCU_Numero] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO