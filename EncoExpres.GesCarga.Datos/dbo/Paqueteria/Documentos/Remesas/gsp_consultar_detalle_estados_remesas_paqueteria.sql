﻿PRINT 'gsp_consultar_detalle_estados_remesas_paqueteria'
GO
DROP PROCEDURE gsp_consultar_detalle_estados_remesas_paqueteria
GO
CREATE PROCEDURE gsp_consultar_detalle_estados_remesas_paqueteria
(          
	@par_EMPR_Codigo SMALLINT,          
	@par_ENRE_Numero NUMERIC          
)          
AS          
BEGIN          
          
	SELECT DISTINCT  
          
	DERP.EMPR_Codigo,          
	DERP.ENRE_Numero,          
	DERP.CATA_ESPR_Codigo,          
	CONVERT(SmallDateTime, DERP.Fecha_Crea) AS Fecha_Crea,          
	ESRP.Campo1 AS Estado,        
	case when DERP.CATA_ESPR_Codigo = 6005 THEN OFICOR.Nombre ELSE  OFIC.Nombre END as Oficina        
          
	FROM           
	Detalle_Estados_Remesas_Paqueteria AS DERP          
	INNER JOIN Valor_Catalogos AS ESRP          
	ON DERP.EMPR_Codigo = ESRP.EMPR_Codigo          
	AND DERP.CATA_ESPR_Codigo = ESRP.Codigo          
        
	LEFT JOIN Oficinas as OFIC        
	ON DERP.EMPR_Codigo = OFIC.EMPR_Codigo        
	AND DERP.OFIC_Codigo = OFIC.Codigo     
  
	left join Remesas_Paqueteria as ENRE on  
	DERP.EMPR_Codigo = ENRE.EMPR_Codigo  
	and DERP.ENRE_Numero = ENRE.ENRE_Numero  
  
	 LEFT JOIN Oficinas as OFICOR       
	ON ENRE.EMPR_Codigo = OFICOR.EMPR_Codigo        
	AND ENRE.OFIC_Codigo_Origen = OFICOR.Codigo     
         
          
	WHERE DERP.EMPR_Codigo = @par_EMPR_Codigo 
	AND DERP.ENRE_Numero = @par_ENRE_Numero      
	AND (DERP.CATA_ESPR_Codigo = 6005 OR OFIC.Nombre IS NOT NULL)
    
	--ORDER BY DERP.ID    
          
END
GO