USE [GESCARGA50_DOCU_DESA]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE [dbo].[gsp_eliminar_t_documento_cumplido_remesa] (

	@par_EMPR_Codigo SMALLINT,
	@par_ENRE_Numero NUMERIC
)

AS
BEGIN
	
	IF EXISTS(SELECT EMPR_Codigo FROM T_Documentos_Cumplido_Remesa WHERE EMPR_Codigo = @par_EMPR_Codigo 
	AND ENRE_Numero = @par_ENRE_Numero) BEGIN

			DELETE T_Documentos_Cumplido_Remesa  
			WHERE EMPR_Codigo = @par_EMPR_Codigo  
			AND ENRE_Numero = @par_ENRE_Numero  

			SELECT @@ROWCOUNT As RegistrosAfectados
	END ELSE
	BEGIN
		SELECT 1 As RegistrosAfectados
	END

END
GO
