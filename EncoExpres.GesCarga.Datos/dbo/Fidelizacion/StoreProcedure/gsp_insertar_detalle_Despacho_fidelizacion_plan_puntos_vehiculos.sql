﻿PRINT 'gsp_insertar_detalle_Despacho_fidelizacion_plan_puntos_vehiculos'
GO
DROP PROCEDURE gsp_insertar_detalle_Despacho_fidelizacion_plan_puntos_vehiculos 
GO
CREATE PROCEDURE gsp_insertar_detalle_Despacho_fidelizacion_plan_puntos_vehiculos 
(
	@par_EMPR_Codigo smallint,
	@par_Vehi_Codigo numeric,
	@par_Planilla numeric,
	@par_Manifiesto numeric, 
	@par_Ruta numeric, 
	@par_Puntos numeric,
	@par_Fecha_Inicio_Vigencia varchar(20),
	@par_Fecha_Fin_Vigencia varchar(20),
	@par_Estado smallint,
	@par_Anulado smallint,
	@par_USUA_Codigo_Crea numeric
)
AS
BEGIN

	INSERT INTO Detalle_Despachos_Plan_Puntos_Vehiculos
	(
	EMPR_Codigo,
	VEHI_Codigo,
	ENPD_Numero,
	RUTA_Codigo,
	ENMC_Numero,
	Puntos,
	Fecha_Inicio,
	Fecha_Vence,
	Estado,
	USUA_Codigo_Crea,
	Fecha_Crea,
	Anulado
	)
	VALUES(
	@par_EMPR_Codigo,
	@par_Vehi_Codigo,
	@par_Planilla,
	@par_Ruta,
	@par_Manifiesto,
	@par_Puntos,
	@par_Fecha_Inicio_Vigencia,
	@par_Fecha_Fin_Vigencia,
	@par_Estado,
	@par_USUA_Codigo_Crea,
	GETDATE(),
	@par_Anulado
	)

	SELECT ID as Codigo FROM Detalle_Despachos_Plan_Puntos_Vehiculos
	WHERE EMPR_Codigo = @par_EMPR_Codigo      
	AND VEHI_Codigo = @par_Vehi_Codigo
	AND ENPD_Numero = @par_Planilla
	AND ENMC_Numero = @par_Manifiesto
	AND ID = @@identity
END 
GO
