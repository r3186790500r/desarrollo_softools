﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Seguridad

Namespace Seguridad

    ''' <summary>
    ''' Clase <see cref="RepositorioEntidadOrigenEstudioSeguridad"/>
    ''' </summary>
    Public NotInheritable Class RepositorioEntidadOrigenEstudioSeguridad
        Inherits RepositorioBase(Of EntidadOrigenEstudioSeguridad)

        Public Overrides Function Consultar(filtro As EntidadOrigenEstudioSeguridad) As IEnumerable(Of EntidadOrigenEstudioSeguridad)
            Dim lista As New List(Of EntidadOrigenEstudioSeguridad)
            Dim item As EntidadOrigenEstudioSeguridad

            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                If Not String.IsNullOrWhiteSpace(filtro.Nombre) Then
                    conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_entidad_origen_estudio_seguridad]")

                While resultado.Read
                    item = New EntidadOrigenEstudioSeguridad(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As EntidadOrigenEstudioSeguridad) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As EntidadOrigenEstudioSeguridad) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As EntidadOrigenEstudioSeguridad) As EntidadOrigenEstudioSeguridad
            Throw New NotImplementedException()
        End Function
    End Class

End Namespace