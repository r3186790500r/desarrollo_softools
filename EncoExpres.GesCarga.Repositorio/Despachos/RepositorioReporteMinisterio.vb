﻿Imports EncoExpres.GesCarga.Conexion
Imports EncoExpres.GesCarga.Entidades.Basico.General
Imports EncoExpres.GesCarga.Entidades.Despachos.Remesa
Imports EncoExpres.GesCarga.Entidades.Despachos
Imports EncoExpres.GesCarga.Entidades.Despachos.Procesos
Imports EncoExpres.GesCarga.Entidades.Despachos.Procesos.General
Imports EncoExpres.GesCarga.Repositorio.Basico.General
Imports System.Data.SqlClient

Namespace Despachos.Procesos

    ''' <summary>
    ''' Clase <see cref="RepositorioReporteMinisterio"/>
    ''' </summary>


    Public NotInheritable Class RepositorioReporteMinisterio

        Inherits RepositorioBase(Of ReporteMinisterio)

        'Public objReporte As InterfazMinisterioTransporte
        Public Overrides Function Consultar(filtro As ReporteMinisterio) As IEnumerable(Of ReporteMinisterio)
            Dim lista As New List(Of ReporteMinisterio)
            Dim item As ReporteMinisterio

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                Dim resultado As IDataReader

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                'If Not IsNothing(filtro.OpcionReporte) Then
                '    conexion.AgregarParametroSQL("@par_opcion_Consulta", filtro.OpcionReporte)
                'End If
                If Not IsNothing(filtro.Oficina) Then
                    If filtro.Oficina.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficina.Codigo)
                    End If
                End If
                If filtro.FechaInicial > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.DateTime)
                End If

                If filtro.FechaFinal > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.DateTime)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > Cero Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > Cero Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                If Not IsNothing(filtro.OpcionReporte) Then
                    Select Case filtro.OpcionReporte
                        Case OPCIONES_REPORTE_RNDC.REMESA
                            If Not IsNothing(filtro.Remesa) Then
                                If filtro.Remesa.NumeroDocumento > Cero Then
                                    conexion.AgregarParametroSQL("@par_ENRE_NumeroDocumento", filtro.Remesa.NumeroDocumento)
                                End If
                                If Not IsNothing(filtro.Remesa.Manifiesto) Then
                                    If filtro.Remesa.Manifiesto.NumeroDocumento > Cero Then
                                        conexion.AgregarParametroSQL("@par_ENMC_NumeroDocumento", filtro.Remesa.Manifiesto.NumeroDocumento)
                                    End If
                                End If
                            End If
                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_remesas_reporte_rndc")
                        Case OPCIONES_REPORTE_RNDC.REMESA_CUMPLIDA
                            If Not IsNothing(filtro.Remesa) Then
                                If filtro.Remesa.NumeroDocumento > Cero Then
                                    conexion.AgregarParametroSQL("@par_ENRE_NumeroDocumento", filtro.Remesa.NumeroDocumento)
                                End If
                                If Not IsNothing(filtro.Remesa.Manifiesto) Then
                                    If filtro.Remesa.Manifiesto.NumeroDocumento > Cero Then
                                        conexion.AgregarParametroSQL("@par_ENMC_NumeroDocumento", filtro.Remesa.Manifiesto.NumeroDocumento)
                                    End If
                                End If
                            End If
                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_remesas_cumplido_reporte_rndc")
                        Case OPCIONES_REPORTE_RNDC.REMESA_CUMPLIDO_INICIAL
                            If Not IsNothing(filtro.Remesa) Then
                                If filtro.Remesa.NumeroDocumento > Cero Then
                                    conexion.AgregarParametroSQL("@par_ENRE_NumeroDocumento", filtro.Remesa.NumeroDocumento)
                                End If
                                If Not IsNothing(filtro.Remesa.Manifiesto) Then
                                    If filtro.Remesa.Manifiesto.NumeroDocumento > Cero Then
                                        conexion.AgregarParametroSQL("@par_ENMC_NumeroDocumento", filtro.Remesa.Manifiesto.NumeroDocumento)
                                    End If
                                End If
                            End If
                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_remesas_cumplido_inicial_reporte_rndc")
                        Case OPCIONES_REPORTE_RNDC.REMESA_ANULADA
                            If Not IsNothing(filtro.Remesa) Then
                                If filtro.Remesa.NumeroDocumento > Cero Then
                                    conexion.AgregarParametroSQL("@par_ENRE_NumeroDocumento", filtro.Remesa.NumeroDocumento)
                                End If
                                If Not IsNothing(filtro.Remesa.Manifiesto) Then
                                    If filtro.Remesa.Manifiesto.NumeroDocumento > Cero Then
                                        conexion.AgregarParametroSQL("@par_ENMC_NumeroDocumento", filtro.Remesa.Manifiesto.NumeroDocumento)
                                    End If
                                End If
                            End If
                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_remesas_anuladas_reporte_rndc")
                        Case OPCIONES_REPORTE_RNDC.REMESA_CUMLIDA_ANULADA
                            If Not IsNothing(filtro.Remesa) Then
                                If filtro.Remesa.NumeroDocumento > Cero Then
                                    conexion.AgregarParametroSQL("@par_ENRE_NumeroDocumento", filtro.Remesa.NumeroDocumento)
                                End If
                                If Not IsNothing(filtro.Remesa.Manifiesto) Then
                                    If filtro.Remesa.Manifiesto.NumeroDocumento > Cero Then
                                        conexion.AgregarParametroSQL("@par_ENMC_NumeroDocumento", filtro.Remesa.Manifiesto.NumeroDocumento)
                                    End If
                                End If
                            End If
                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_remesas_cumplido_anuladas_reporte_rndc")
                        Case OPCIONES_REPORTE_RNDC.MANIFIESTO
                            If Not IsNothing(filtro.Manifiesto) Then
                                If filtro.Manifiesto.NumeroDocumento > Cero Then
                                    conexion.AgregarParametroSQL("@par_ENMC_NumeroDocumento", filtro.Manifiesto.NumeroDocumento)
                                End If
                            End If
                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_manifiesto_reporte_rndc")
                        Case OPCIONES_REPORTE_RNDC.MANIFIESTO_CUMPLIDO
                            If Not IsNothing(filtro.Manifiesto) Then
                                If filtro.Manifiesto.NumeroDocumento > Cero Then
                                    conexion.AgregarParametroSQL("@par_ENMC_NumeroDocumento", filtro.Manifiesto.NumeroDocumento)
                                End If
                            End If
                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_manifiesto_cumplido_reporte_rndc")
                        Case OPCIONES_REPORTE_RNDC.MANIFIESTO_ANULADO
                            If Not IsNothing(filtro.Manifiesto) Then
                                If filtro.Manifiesto.NumeroDocumento > Cero Then
                                    conexion.AgregarParametroSQL("@par_ENMC_NumeroDocumento", filtro.Manifiesto.NumeroDocumento)
                                End If
                            End If
                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_manifiesto_anulado_reporte_rndc")
                        Case OPCIONES_REPORTE_RNDC.MANIFIESTO_CUMPLIDO_ANULADO
                            If Not IsNothing(filtro.Manifiesto) Then
                                If filtro.Manifiesto.NumeroDocumento > Cero Then
                                    conexion.AgregarParametroSQL("@par_ENMC_NumeroDocumento", filtro.Manifiesto.NumeroDocumento)
                                End If
                            End If
                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_manifiesto_cumplido_anulado_reporte_rndc")
                        Case OPCIONES_REPORTE_RNDC.ACEPTACION_ELECTRONICA
                            If Not IsNothing(filtro.Manifiesto) Then
                                If filtro.Manifiesto.NumeroDocumento > Cero Then
                                    conexion.AgregarParametroSQL("@par_ENMC_NumeroDocumento", filtro.Manifiesto.NumeroDocumento)
                                End If
                            End If
                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_manifiesto_aceptacion_electronica_rndc")
                    End Select
                End If
                While resultado.Read
                    item = New ReporteMinisterio(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As ReporteMinisterio) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As ReporteMinisterio) As Long

            'If entidad.OpcionReporte = 0 Then

            '    If Not IsNothing(entidad.ListadoBase) Then
            '        For Each listadobase In entidad.ListadoBase
            '            objReporte.Reportar_Remesa(listadobase.Numero)
            '        Next
            '    End If

            'End If
            'Return entidad.NumeroCumplido
        End Function

        Public Overrides Function Obtener(filtro As ReporteMinisterio) As ReporteMinisterio
            Throw New NotImplementedException()
        End Function

        Public Function Anular(entidad As ReporteMinisterio) As Boolean
            Throw New NotImplementedException()
        End Function

        Public Function ReportarRNDC(entidad As ReporteMinisterio) As ReporteMinisterio

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                Dim resultado As IDataReader

                REM: Cargar informacion de Empresa
                Dim Empresa As New Empresas()
                Empresa.Codigo = entidad.CodigoEmpresa
                Dim RepositoroEmpresa As New RepositorioEmpresas()
                Empresa = RepositoroEmpresa.Obtener(Empresa)

                If Empresa.ReportaRNDC > Cero Then
                    REM: Validacion Acceso
                    Dim acceso As New Acceso(Empresa.ListadoRNDC.UsuarioManifiestoElectronico, Empresa.ListadoRNDC.ClaveManifiestoElectronico, Acceso.AMBIENTE_WS_MINISTERIO_R)
                    Dim solicitudConsulta = New Solicitud

                    If Not IsNothing(Empresa.ListadoRNDC) Then
                        If Not IsNothing(entidad.OpcionReporte) Then
                            Select Case entidad.OpcionReporte
                                Case OPCIONES_REPORTE_RNDC.REMESA
                                    If Not IsNothing(entidad.DetalleReporteRemesas) Then
                                        Call ProcesoReporteRemesa(Empresa, acceso, solicitudConsulta, conexion, entidad.DetalleReporteRemesas)
                                    End If
                                Case OPCIONES_REPORTE_RNDC.REMESA_CUMPLIDO_INICIAL
                                    If Not IsNothing(entidad.DetalleReporteRemesas) Then
                                        Call ProcesoReporteRemesaCumplidoInicial(Empresa, acceso, solicitudConsulta, conexion, entidad.DetalleReporteRemesas)
                                    End If
                                Case OPCIONES_REPORTE_RNDC.REMESA_CUMPLIDA
                                    If Not IsNothing(entidad.DetalleReporteRemesas) Then
                                        Call ProcesoReporteRemesaCumplido(Empresa, acceso, solicitudConsulta, conexion, entidad.DetalleReporteRemesas)
                                    End If
                                Case OPCIONES_REPORTE_RNDC.REMESA_ANULADA
                                    If Not IsNothing(entidad.DetalleReporteRemesas) Then
                                        Call ProcesoReporteRemesaAnulado(Empresa, acceso, solicitudConsulta, conexion, entidad.DetalleReporteRemesas)
                                    End If
                                Case OPCIONES_REPORTE_RNDC.REMESA_CUMLIDA_ANULADA
                                    If Not IsNothing(entidad.DetalleReporteRemesas) Then
                                        Call ProcesoReporteRemesaCumplidaAnulado(Empresa, acceso, solicitudConsulta, conexion, entidad.DetalleReporteRemesas)
                                    End If
                                Case OPCIONES_REPORTE_RNDC.MANIFIESTO
                                    If Not IsNothing(entidad.DetalleReporteManifiesto) Then
                                        Call ProcesoReporteManifiesto(Empresa, acceso, solicitudConsulta, conexion, entidad.DetalleReporteManifiesto)
                                    End If
                                Case OPCIONES_REPORTE_RNDC.MANIFIESTO_CUMPLIDO
                                    If Not IsNothing(entidad.DetalleReporteManifiesto) Then
                                        Call ProcesoReporteManifiestoCumplido(Empresa, acceso, solicitudConsulta, conexion, entidad.DetalleReporteManifiesto)
                                    End If
                                Case OPCIONES_REPORTE_RNDC.MANIFIESTO_ANULADO
                                    If Not IsNothing(entidad.DetalleReporteManifiesto) Then
                                        Call ProcesoReporteManifiestoAnulado(Empresa, acceso, solicitudConsulta, conexion, entidad.DetalleReporteManifiesto)
                                    End If
                                Case OPCIONES_REPORTE_RNDC.MANIFIESTO_CUMPLIDO_ANULADO
                                    If Not IsNothing(entidad.DetalleReporteManifiesto) Then
                                        Call ProcesoReporteManifiestoCumplidoAnulado(Empresa, acceso, solicitudConsulta, conexion, entidad.DetalleReporteManifiesto)
                                    End If
                                Case OPCIONES_REPORTE_RNDC.ACEPTACION_ELECTRONICA
                                    If Not IsNothing(entidad.DetalleReporteManifiesto) Then
                                        Call ProcesoAceptacionElectronica(Empresa, acceso, solicitudConsulta, conexion, entidad.DetalleReporteManifiesto)
                                    End If
                            End Select
                            'error enviando las opciones de reporte
                        End If
                    Else
                        'no se ha asignado los accesos del rndc
                    End If
                End If

            End Using
            Return entidad
        End Function

        Public Sub ProcesoReporteRemesaAnulado(Empresa As Empresas, acceso As Acceso, solicitudConsulta As Solicitud, conexion As DataBaseFactory, DetalleReporteRemesas As IEnumerable(Of Remesas))
            For Each DetalleRemesa In DetalleReporteRemesas
                Dim Result As New Resultado_Proceso_Reporte
                Dim resultado As IDataReader
                Dim RemesaReporteRNDC As New RemesasRNDC
                conexion.CleanParameters()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", Empresa.Codigo)
                conexion.AgregarParametroSQL("@par_Enre_Numero", DetalleRemesa.Numero)
                resultado = conexion.ExecuteReaderStoreProcedure("gsp_obtener_remesa_rndc")
                While resultado.Read
                    RemesaReporteRNDC = New RemesasRNDC(resultado)
                End While
                resultado.Close()
                Dim ResultadoReporteRemitente As New ResultReporteMinisterio()
                Dim ResultadoReporteDestinatario As New ResultReporteMinisterio()
                Dim ResultadoReporteRemesa As New ResultReporteMinisterio()
                REM reporta remesa
                ResultadoReporteRemesa = ReportarAnulacionRemesa(Empresa, acceso, solicitudConsulta, RemesaReporteRNDC)
                If ResultadoReporteRemesa.StrError.Contains("DUPLICADO:") Then
                    ResultadoReporteRemesa.NumConfirmacion = ResultadoReporteRemesa.StrError.Substring(10, 9)
                End If
                REM Almacena Respuesta Reporte Remesa, así sea o no exitoso el reporte
                ActualizarResultadoReporteAnulacionRemesaRNDC(conexion, ResultadoReporteRemesa, Empresa.Codigo, DetalleRemesa)
            Next
        End Sub

        Public Sub ProcesoReporteRemesaCumplidaAnulado(Empresa As Empresas, acceso As Acceso, solicitudConsulta As Solicitud, conexion As DataBaseFactory, DetalleReporteRemesas As IEnumerable(Of Remesas))
            For Each DetalleRemesa In DetalleReporteRemesas
                Dim Result As New Resultado_Proceso_Reporte
                Dim resultado As IDataReader
                Dim RemesaReporteRNDC As New RemesasRNDC
                conexion.CleanParameters()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", Empresa.Codigo)
                conexion.AgregarParametroSQL("@par_Enre_Numero", DetalleRemesa.Numero)
                resultado = conexion.ExecuteReaderStoreProcedure("gsp_obtener_remesa_rndc")
                While resultado.Read
                    RemesaReporteRNDC = New RemesasRNDC(resultado)
                End While
                resultado.Close()
                Dim ResultadoReporteRemitente As New ResultReporteMinisterio()
                Dim ResultadoReporteDestinatario As New ResultReporteMinisterio()
                Dim ResultadoReporteRemesa As New ResultReporteMinisterio()
                REM reporta remesa
                ResultadoReporteRemesa = ReportarAnulacionCumplidoRemesa(Empresa, acceso, solicitudConsulta, RemesaReporteRNDC)
                If ResultadoReporteRemesa.StrError.Contains("DUPLICADO:") Then
                    ResultadoReporteRemesa.NumConfirmacion = ResultadoReporteRemesa.StrError.Substring(10, 9)
                End If
                REM Almacena Respuesta Reporte Remesa, así sea o no exitoso el reporte
                ActualizarResultadoReporteAnulacionCumpldioRemesaRNDC(conexion, ResultadoReporteRemesa, Empresa.Codigo, DetalleRemesa)
            Next
        End Sub
        Public Sub ProcesoReporteRemesaCumplido(Empresa As Empresas, acceso As Acceso, solicitudConsulta As Solicitud, conexion As DataBaseFactory, DetalleReporteRemesas As IEnumerable(Of Remesas))
            For Each DetalleRemesa In DetalleReporteRemesas
                Dim Result As New Resultado_Proceso_Reporte
                Dim resultado As IDataReader
                Dim RemesaReporteRNDC As New RemesasRNDC
                conexion.CleanParameters()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", Empresa.Codigo)
                conexion.AgregarParametroSQL("@par_Enre_Numero", DetalleRemesa.Numero)
                resultado = conexion.ExecuteReaderStoreProcedure("gsp_obtener_remesa_rndc")
                While resultado.Read
                    RemesaReporteRNDC = New RemesasRNDC(resultado)
                End While
                resultado.Close()
                Dim ResultadoReporteRemitente As New ResultReporteMinisterio()
                Dim ResultadoReporteDestinatario As New ResultReporteMinisterio()
                Dim ResultadoReporteRemesa As New ResultReporteMinisterio()
                REM reporta remesa
                ResultadoReporteRemesa = ReportarCumplidoRemesa(Empresa, acceso, solicitudConsulta, RemesaReporteRNDC)
                If ResultadoReporteRemesa.StrError.Contains("DUPLICADO:") Then
                    ResultadoReporteRemesa.NumConfirmacion = ResultadoReporteRemesa.StrError.Substring(10, 9)
                End If
                REM Almacena Respuesta Reporte Remesa, así sea o no exitoso el reporte
                ActualizarResultadoReporteCumplidoRemesaRNDC(conexion, ResultadoReporteRemesa, Empresa.Codigo, DetalleRemesa)
            Next
        End Sub
        Public Sub ProcesoReporteRemesaCumplidoInicial(Empresa As Empresas, acceso As Acceso, solicitudConsulta As Solicitud, conexion As DataBaseFactory, DetalleReporteRemesas As IEnumerable(Of Remesas))
            For Each DetalleRemesa In DetalleReporteRemesas
                Dim Result As New Resultado_Proceso_Reporte
                Dim resultado As IDataReader
                Dim RemesaReporteRNDC As New RemesasRNDC
                conexion.CleanParameters()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", Empresa.Codigo)
                conexion.AgregarParametroSQL("@par_Enre_Numero", DetalleRemesa.Numero)
                resultado = conexion.ExecuteReaderStoreProcedure("gsp_obtener_remesa_rndc")
                While resultado.Read
                    RemesaReporteRNDC = New RemesasRNDC(resultado)
                End While
                resultado.Close()
                Dim ResultadoReporteRemitente As New ResultReporteMinisterio()
                Dim ResultadoReporteDestinatario As New ResultReporteMinisterio()
                Dim ResultadoReporteRemesa As New ResultReporteMinisterio()
                REM reporta remesa
                ResultadoReporteRemesa = ReportarCumplidoInicialRemesa(Empresa, acceso, solicitudConsulta, RemesaReporteRNDC)
                If ResultadoReporteRemesa.StrError.Contains("DUPLICADO:") Then
                    ResultadoReporteRemesa.NumConfirmacion = ResultadoReporteRemesa.StrError.Substring(10, 9)
                End If
                REM Almacena Respuesta Reporte Remesa, así sea o no exitoso el reporte
                ActualizarResultadoReporteCumplidoInicialRemesaRNDC(conexion, ResultadoReporteRemesa, Empresa.Codigo, DetalleRemesa)
            Next
        End Sub

        Public Sub ProcesoReporteRemesa(Empresa As Empresas, acceso As Acceso, solicitudConsulta As Solicitud, conexion As DataBaseFactory, DetalleReporteRemesas As IEnumerable(Of Remesas))
            For Each DetalleRemesa In DetalleReporteRemesas
                Dim Result As New Resultado_Proceso_Reporte
                Dim resultado As IDataReader
                Dim RemesaReporteRNDC As New RemesasRNDC
                conexion.CleanParameters()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", Empresa.Codigo)
                conexion.AgregarParametroSQL("@par_Enre_Numero", DetalleRemesa.Numero)
                resultado = conexion.ExecuteReaderStoreProcedure("gsp_obtener_remesa_rndc")
                While resultado.Read
                    RemesaReporteRNDC = New RemesasRNDC(resultado)
                End While
                resultado.Close()
                Dim ResultadoReporteRemitente As New ResultReporteMinisterio()
                Dim ResultadoReporteDestinatario As New ResultReporteMinisterio()
                Dim ResultadoReporteRemesa As New ResultReporteMinisterio()

                REM reporta remitente para la remesa
                If RemesaReporteRNDC.Remitente.Reportar_RNDC = 1 Then
                    ResultadoReporteRemitente = ReportarTercero(Empresa, acceso, solicitudConsulta, RemesaReporteRNDC.Remitente, Tipos_Roles.REMITENTE)
                    REM Almacena Respuesta Reporte Remitente, si existe error
                    If ResultadoReporteRemitente.NumConfirmacion = Cero Then
                        'error al reportar remitente
                        ActualizarResultadoReporteRNDC(conexion, ResultadoReporteRemitente, Empresa.Codigo, DetalleRemesa)
                    End If
                    ActualizaroReporteRNDCTercero(conexion, Empresa.Codigo, RemesaReporteRNDC.Remitente.Codigo)
                End If

                REM reporta destinatario para la remesa
                If RemesaReporteRNDC.Destinatario.Reportar_RNDC = 1 Then
                    ResultadoReporteDestinatario = ReportarTercero(Empresa, acceso, solicitudConsulta, RemesaReporteRNDC.Destinatario, Tipos_Roles.DESTINATARIO)
                    REM Almacena Respuesta Reporte Destinatario, si existe error
                    If ResultadoReporteDestinatario.NumConfirmacion = Cero Then
                        ActualizarResultadoReporteRNDC(conexion, ResultadoReporteDestinatario, Empresa.Codigo, DetalleRemesa)
                    End If
                    ActualizaroReporteRNDCTercero(conexion, Empresa.Codigo, RemesaReporteRNDC.Destinatario.Codigo)
                End If
                REM reporta remesa
                ResultadoReporteRemesa = ReportarRemesa(Empresa, acceso, solicitudConsulta, RemesaReporteRNDC)
                If ResultadoReporteRemesa.StrError.Contains("DUPLICADO:") Then
                    ResultadoReporteRemesa.NumConfirmacion = 0
                End If
                REM Almacena Respuesta Reporte Remesa, así sea o no exitoso el reporte
                ActualizarResultadoReporteRNDC(conexion, ResultadoReporteRemesa, Empresa.Codigo, DetalleRemesa)
            Next
        End Sub

        Public Sub ProcesoReporteManifiestoCumplido(Empresa As Empresas, acceso As Acceso, solicitudConsulta As Solicitud, conexion As DataBaseFactory, DetalleReporteManifiesto As IEnumerable(Of Manifiesto))
            For Each Detalle In DetalleReporteManifiesto
                Dim Result As New Resultado_Proceso_Reporte
                Dim resultado As IDataReader
                Dim ReporteRNDC As New ManifiestoRNDC
                Dim BolReportaManifiesto = True
                conexion.CleanParameters()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", Empresa.Codigo)
                conexion.AgregarParametroSQL("@par_ENMC_Numero", Detalle.Numero)
                resultado = conexion.ExecuteReaderStoreProcedure("gsp_obtener_manifiesto_rndc")
                While resultado.Read
                    ReporteRNDC = New ManifiestoRNDC(resultado)
                End While
                resultado.Close()
                Dim ResultadoReporte As New ResultReporteMinisterio()
                ResultadoReporte = ReportarCumplidoManifiesto(Empresa, acceso, solicitudConsulta, ReporteRNDC)
                BolReportaManifiesto = False
                If ResultadoReporte.StrError.Contains("DUPLICADO:") Then
                    ResultadoReporte.NumConfirmacion = ResultadoReporte.StrError.Substring(10, 9)
                End If
                ActualizarResultadoReporteCumplidoRNDC(conexion, ResultadoReporte, Empresa.Codigo, Detalle)
            Next
        End Sub
        Public Sub ProcesoReporteManifiestoCumplidoAnulado(Empresa As Empresas, acceso As Acceso, solicitudConsulta As Solicitud, conexion As DataBaseFactory, DetalleReporteManifiesto As IEnumerable(Of Manifiesto))
            For Each Detalle In DetalleReporteManifiesto
                Dim Result As New Resultado_Proceso_Reporte
                Dim resultado As IDataReader
                Dim ReporteRNDC As New ManifiestoRNDC
                Dim BolReportaManifiesto = True
                conexion.CleanParameters()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", Empresa.Codigo)
                conexion.AgregarParametroSQL("@par_ENMC_Numero", Detalle.Numero)
                resultado = conexion.ExecuteReaderStoreProcedure("gsp_obtener_manifiesto_rndc")
                While resultado.Read
                    ReporteRNDC = New ManifiestoRNDC(resultado)
                End While
                resultado.Close()
                Dim ResultadoReporte As New ResultReporteMinisterio()
                ResultadoReporte = ReportarCumplidoAnuladoManifiesto(Empresa, acceso, solicitudConsulta, ReporteRNDC)
                BolReportaManifiesto = False
                If ResultadoReporte.StrError.Contains("DUPLICADO:") Then
                    ResultadoReporte.NumConfirmacion = ResultadoReporte.StrError.Substring(10, 9)
                End If
                ActualizarResultadoReporteAnulacionCumplidoRNDC(conexion, ResultadoReporte, Empresa.Codigo, Detalle)
            Next
        End Sub
        Public Sub ProcesoReporteManifiestoAnulado(Empresa As Empresas, acceso As Acceso, solicitudConsulta As Solicitud, conexion As DataBaseFactory, DetalleReporteManifiesto As IEnumerable(Of Manifiesto))
            For Each Detalle In DetalleReporteManifiesto
                Dim Result As New Resultado_Proceso_Reporte
                Dim resultado As IDataReader
                Dim ReporteRNDC As New ManifiestoRNDC
                Dim BolReportaManifiesto = True
                conexion.CleanParameters()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", Empresa.Codigo)
                conexion.AgregarParametroSQL("@par_ENMC_Numero", Detalle.Numero)
                resultado = conexion.ExecuteReaderStoreProcedure("gsp_obtener_manifiesto_rndc")
                While resultado.Read
                    ReporteRNDC = New ManifiestoRNDC(resultado)
                End While
                resultado.Close()
                Dim ResultadoReporte As New ResultReporteMinisterio()
                ResultadoReporte = ReportarAnuladoManifiesto(Empresa, acceso, solicitudConsulta, ReporteRNDC)
                BolReportaManifiesto = False
                If ResultadoReporte.StrError.Contains("DUPLICADO:") Then
                    ResultadoReporte.NumConfirmacion = ResultadoReporte.StrError.Substring(10, 7)
                End If
                ActualizarResultadoReporteAnulacionManifiestoRNDC(conexion, ResultadoReporte, Empresa.Codigo, Detalle)
            Next
        End Sub
        Public Sub ProcesoReporteManifiesto(Empresa As Empresas, acceso As Acceso, solicitudConsulta As Solicitud, conexion As DataBaseFactory, DetalleReporteManifiesto As IEnumerable(Of Manifiesto))
            For Each Detalle In DetalleReporteManifiesto
                Dim Result As New Resultado_Proceso_Reporte
                Dim resultado As IDataReader
                Dim ReporteRNDC As New ManifiestoRNDC
                Dim BolReportaManifiesto = True
                Dim MensajeMInisterio As String = ""
                conexion.CleanParameters()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", Empresa.Codigo)
                conexion.AgregarParametroSQL("@par_ENMC_Numero", Detalle.Numero)
                resultado = conexion.ExecuteReaderStoreProcedure("gsp_obtener_manifiesto_rndc")
                While resultado.Read
                    ReporteRNDC = New ManifiestoRNDC(resultado)
                End While
                resultado.Close()
                Dim ResultadoReporte As New ResultReporteMinisterio()

                'Pasos Del Reporte
                '1. Se reporta el Tenedor
                '2. Se reporta el Porpietario
                '3. Se reporta el Conductor
                '4. Se reporta el Vehìculo
                '5. Se reporta el Semirremolque si existe
                '6. Se reportan las remesas del manifiesto que esten sin reportar
                '7. Se reporta el manifiesto

                'reporta Tenedor del vehículo
                If ReporteRNDC.Tenedor.Reportar_RNDC = 1 Then
                    ResultadoReporte = ReportarTercero(Empresa, acceso, solicitudConsulta, ReporteRNDC.Tenedor, Tipos_Roles.TENEDOR)
                    If ResultadoReporte.NumConfirmacion = Cero Then
                        BolReportaManifiesto = False
                        ActualizarResultadoReporteRNDC(conexion, ResultadoReporte, Empresa.Codigo, Detalle)
                        MensajeMInisterio += "TENEDOR (" + ResultadoReporte.StrError + ")"
                    End If
                    ActualizaroReporteRNDCTercero(conexion, Empresa.Codigo, ReporteRNDC.Tenedor.Codigo)
                End If
                'reporta Propietario del vehículo
                If ReporteRNDC.Propietario.Reportar_RNDC = 1 Then
                    ResultadoReporte = ReportarTercero(Empresa, acceso, solicitudConsulta, ReporteRNDC.Propietario, Tipos_Roles.PROPIETARIO)
                    If ResultadoReporte.NumConfirmacion = Cero Then
                        BolReportaManifiesto = False
                        ActualizarResultadoReporteRNDC(conexion, ResultadoReporte, Empresa.Codigo, Detalle)
                        MensajeMInisterio += "PROPIETARIO (" + ResultadoReporte.StrError + ")"
                    End If
                    ActualizaroReporteRNDCTercero(conexion, Empresa.Codigo, ReporteRNDC.Propietario.Codigo)
                End If

                'reporta el Conductor del Vehículo
                If ReporteRNDC.Conductor.Reportar_RNDC = 1 Then
                    ResultadoReporte = ReportarTercero(Empresa, acceso, solicitudConsulta, ReporteRNDC.Conductor, Tipos_Roles.CONDUCTOR)
                    If ResultadoReporte.NumConfirmacion = Cero Then
                        BolReportaManifiesto = False
                        ActualizarResultadoReporteRNDC(conexion, ResultadoReporte, Empresa.Codigo, Detalle)
                        MensajeMInisterio += "CONDUCTOR (" + ResultadoReporte.StrError + ")"
                    End If
                    ActualizaroReporteRNDCTercero(conexion, Empresa.Codigo, ReporteRNDC.Conductor.Codigo)
                End If

                'reporta el Vehículo
                If ReporteRNDC.Vehiculo.Reportar_RNDC = 1 Then
                    ResultadoReporte = ReportarVehiculo(Empresa, acceso, solicitudConsulta, ReporteRNDC.Vehiculo)
                    If ResultadoReporte.NumConfirmacion = Cero And (Not ResultadoReporte.StrError.Contains("DUPLICADO")) Then
                        BolReportaManifiesto = False
                        ActualizarResultadoReporteRNDC(conexion, ResultadoReporte, Empresa.Codigo, Detalle)
                        MensajeMInisterio += "VEHÍCULO (" + ResultadoReporte.StrError + ")"
                    End If
                    ActualizaroReporteRNDCVehiculo(conexion, Empresa.Codigo, ReporteRNDC.Vehiculo.Codigo)
                End If
                'reporta el Semirremolque
                If Not IsNothing(ReporteRNDC.NUMPLACAREMOLQUE) And ReporteRNDC.NUMPLACAREMOLQUE <> String.Empty Then
                    'ReporteRNDC.Semirremolque = ReporteRNDC.Vehiculo
                    'ReporteRNDC.Semirremolque.NUMPLACA = ReporteRNDC.NUMPLACAREMOLQUE
                    If ReporteRNDC.Semirremolque.Reportar_RNDC = 1 Then
                        ReporteRNDC.Semirremolque.CODCONFIGURACIONUNIDADCARGA = 63
                        ResultadoReporte = ReportarSemirremolque(Empresa, acceso, solicitudConsulta, ReporteRNDC.Semirremolque)
                        If ResultadoReporte.NumConfirmacion = Cero And (Not ResultadoReporte.StrError.Contains("DUPLICADO")) Then
                            BolReportaManifiesto = False
                            ActualizarResultadoReporteRNDC(conexion, ResultadoReporte, Empresa.Codigo, Detalle)
                            MensajeMInisterio += "SEMIRREMOLQUE (" + ResultadoReporte.StrError + ")"
                        End If
                        ActualizaroReporteRNDCSemirremolque(conexion, Empresa.Codigo, ReporteRNDC.Semirremolque.Codigo)
                    End If
                End If
                'reporta el Manifiesto

                Me.Retornar_Remesas(Detalle.Numero, ReporteRNDC.CONSECUTIVOREMESA, Empresa)
                ResultadoReporte = ReportarManifiesto(Empresa, acceso, solicitudConsulta, ReporteRNDC)
                MensajeMInisterio += "MANIFIESTO (" + ResultadoReporte.StrError + ")"
                BolReportaManifiesto = False
                If ResultadoReporte.StrError.Contains("DUPLICADO:") Then
                    ResultadoReporte.NumConfirmacion = ResultadoReporte.StrError.Substring(10, 9)
                    MensajeMInisterio += ResultadoReporte.StrError
                End If
                ResultadoReporte.StrError = MensajeMInisterio
                ActualizarResultadoReporteRNDC(conexion, ResultadoReporte, Empresa.Codigo, Detalle)
            Next
        End Sub

        Public Sub ProcesoAceptacionElectronica(Empresa As Empresas, acceso As Acceso, solicitudConsulta As Solicitud, conexion As DataBaseFactory, DetalleReporteManifiesto As IEnumerable(Of Manifiesto))
            For Each Detalle In DetalleReporteManifiesto
                Dim Result As New Resultado_Proceso_Reporte
                Dim resultado As IDataReader
                Dim ReporteRNDC As New AceptacionElectronicaRNDC
                Dim BolReportaManifiesto = True
                Dim MensajeMInisterio As String = ""
                conexion.CleanParameters()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", Empresa.Codigo)
                conexion.AgregarParametroSQL("@par_ENMC_Numero", Detalle.Numero)
                resultado = conexion.ExecuteReaderStoreProcedure("gsp_obtener_aceptacion_electronica_rndc")
                While resultado.Read
                    ReporteRNDC = New AceptacionElectronicaRNDC(resultado)
                End While
                resultado.Close()
                Dim ResultadoReporte As New ResultAceptacionMinisterio()

                'Consulta de Aceptacion Electronica

                ResultadoReporte = ConsultaAceptacionElectronica(Empresa, acceso, solicitudConsulta, ReporteRNDC)
                MensajeMInisterio += "ACEPTACION (" + ResultadoReporte.StrError + ")"
                BolReportaManifiesto = False
                ResultadoReporte.StrError = MensajeMInisterio
                ActualizarAceptacionElectronicaManifiestoRNDC(conexion, ResultadoReporte, Empresa.Codigo, Detalle)
            Next
        End Sub

        Private Sub Retornar_Remesas(ByVal lonNumeroManifiesto As Long, ByRef arrRemesas As String(), Empresa As Empresas)
            Try
                Dim strSQL As String
                REM: CONSULTA LOS NUMEROS DE LAS REMESAS PARA ASOCIARLAS AL MANIFIESTO EN EL MINISTERIO DE TRANSPORTE
                Dim dtsDetalleManifiesto As New DataSet
                Dim ComandoSQL As SqlCommand
                Dim intCont As Short = 1

                strSQL = "SELECT ENRE.Numero_Documento"
                strSQL += " FROM Detalle_Manifiesto_Carga DEMA, Encabezado_Remesas ENRE, Rutas RUTA"
                strSQL += " WHERE DEMA.EMPR_Codigo = ENRE.EMPR_Codigo"
                strSQL += " AND DEMA.ENRE_Numero = ENRE.Numero"
                strSQL += " AND DEMA.EMPR_Codigo = " & Empresa.Codigo
                strSQL += " AND DEMA.ENMC_Numero = " & lonNumeroManifiesto
                strSQL += " GROUP BY ENRE.Numero_Documento"
                Dim Conexion As New SqlConnection(ConnectionString)
                ComandoSQL = New SqlCommand(strSQL, Conexion)
                Conexion.Open()
                dtsDetalleManifiesto = Retorna_Dataset(strSQL)

                ReDim arrRemesas(dtsDetalleManifiesto.Tables(0).Rows.Count - 1)

                For intCont = 0 To dtsDetalleManifiesto.Tables(0).Rows.Count - 1
                    arrRemesas(intCont) = dtsDetalleManifiesto.Tables(0).Rows(intCont).Item("Numero_Documento").ToString()
                Next

                ComandoSQL.Dispose()

            Catch ex As Exception
                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
                'Dim NombreFuncion As String = SeguimientoPila.GetFrame(clsGeneral.CERO).GetMethod().Name
                'Me.strError = "En la clase " & Me.ToString & " se present・un error en la funci " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
            Finally

            End Try
        End Sub
        Function Retorna_Dataset(ByVal strSQL As String, Optional ByRef strError As String = "") As DataSet
            Dim daDataAdapter As SqlDataAdapter
            Dim dsDataSet As DataSet = New DataSet

            Try
                Using ConexionSQL = New SqlConnection(ConnectionString)
                    daDataAdapter = New SqlDataAdapter(strSQL, ConexionSQL)
                    daDataAdapter.Fill(dsDataSet)
                    ConexionSQL.Close()
                End Using

            Catch ex As Exception
                strError = ex.Message.ToString()
            Finally
                'If ConexionSQL.State() = ConnectionState.Open Then
                '    ConexionSQL.Close()
                'End If
            End Try

            Retorna_Dataset = dsDataSet

            Try
                dsDataSet.Dispose()
            Catch ex As Exception
                strError = ex.Message.ToString()
            End Try

        End Function

        Public Function ReportarTercero(Empresa As Empresas, acceso As Acceso, solicitudConsulta As Solicitud, Tercero As TerceroRNDC, Tipos_Rol As Tipos_Roles) As ResultReporteMinisterio
            Dim objTercero As New GestionReporteRNDC(acceso, solicitudConsulta, Tercero, Empresa)
            Dim ResultReporte As ResultReporteMinisterio = New ResultReporteMinisterio()
            objTercero.Reportar_Tercero(Tipos_Procesos.FORMATO_INGRESO, Tipos_Rol, ResultReporte)
            Return ResultReporte
        End Function

        Public Function ReportarRemesa(Empresa As Empresas, acceso As Acceso, solicitudConsulta As Solicitud, Remesa As RemesasRNDC) As ResultReporteMinisterio
            Dim objRemesa As New GestionReporteRemesaRNDC(Empresa, acceso, solicitudConsulta, Remesa)
            Dim ResultReporte As ResultReporteMinisterio = New ResultReporteMinisterio()
            objRemesa.Reportar_Remesa(Tipos_Procesos.FORMATO_INGRESO, ResultReporte)
            Return ResultReporte
        End Function
        Public Function ReportarCumplidoRemesa(Empresa As Empresas, acceso As Acceso, solicitudConsulta As Solicitud, Remesa As RemesasRNDC) As ResultReporteMinisterio
            Dim objRemesa As New GestionReporteRemesaRNDC(Empresa, acceso, solicitudConsulta, Remesa)
            Dim ResultReporte As ResultReporteMinisterio = New ResultReporteMinisterio()
            objRemesa.Reportar_Cumplido_Remesa(Tipos_Procesos.FORMATO_INGRESO, ResultReporte)
            Return ResultReporte
        End Function
        Public Function ReportarCumplidoInicialRemesa(Empresa As Empresas, acceso As Acceso, solicitudConsulta As Solicitud, Remesa As RemesasRNDC) As ResultReporteMinisterio
            Dim objRemesa As New GestionReporteRemesaRNDC(Empresa, acceso, solicitudConsulta, Remesa)
            Dim ResultReporte As ResultReporteMinisterio = New ResultReporteMinisterio()
            objRemesa.Reportar_Cumplido_Inicial_Remesa(Tipos_Procesos.FORMATO_INGRESO, ResultReporte)
            Return ResultReporte
        End Function
        Public Function ReportarAnulacionRemesa(Empresa As Empresas, acceso As Acceso, solicitudConsulta As Solicitud, Remesa As RemesasRNDC) As ResultReporteMinisterio
            Dim objRemesa As New GestionReporteRemesaRNDC(Empresa, acceso, solicitudConsulta, Remesa)
            Dim ResultReporte As ResultReporteMinisterio = New ResultReporteMinisterio()
            objRemesa.Reportar_Anulado_Remesa(Tipos_Procesos.FORMATO_INGRESO, ResultReporte)
            Return ResultReporte
        End Function
        Public Function ReportarAnulacionCumplidoRemesa(Empresa As Empresas, acceso As Acceso, solicitudConsulta As Solicitud, Remesa As RemesasRNDC) As ResultReporteMinisterio
            Dim objRemesa As New GestionReporteRemesaRNDC(Empresa, acceso, solicitudConsulta, Remesa)
            Dim ResultReporte As ResultReporteMinisterio = New ResultReporteMinisterio()
            objRemesa.Reportar_Anulado_Cumplido_Remesa(Tipos_Procesos.FORMATO_INGRESO, ResultReporte)
            Return ResultReporte
        End Function
        Public Function ReportarVehiculo(Empresa As Empresas, acceso As Acceso, solicitudConsulta As Solicitud, Vehiculo As VehiculoRNDC) As ResultReporteMinisterio
            Dim objVehiculo As New GestionReporteRNDC(acceso, solicitudConsulta, Vehiculo, Empresa)
            Dim ResultReporte As ResultReporteMinisterio = New ResultReporteMinisterio()
            objVehiculo.Reportar_Vehiuclo(Tipos_Procesos.FORMATO_INGRESO, ResultReporte)
            Return ResultReporte
        End Function
        Public Function ReportarSemirremolque(Empresa As Empresas, acceso As Acceso, solicitudConsulta As Solicitud, Vehiculo As VehiculoRNDC) As ResultReporteMinisterio
            Dim objVehiculo As New GestionReporteRNDC(acceso, solicitudConsulta, Vehiculo, Empresa)
            Dim ResultReporte As ResultReporteMinisterio = New ResultReporteMinisterio()
            objVehiculo.Reportar_Semirremolque(Tipos_Procesos.FORMATO_INGRESO, ResultReporte)
            Return ResultReporte
        End Function
        Public Function ReportarManifiesto(Empresa As Empresas, acceso As Acceso, solicitudConsulta As Solicitud, Manifiesto As ManifiestoRNDC) As ResultReporteMinisterio
            Dim objVehiculo As New GestionReporteRNDC(acceso, solicitudConsulta, Manifiesto, Empresa)
            Dim ResultReporte As ResultReporteMinisterio = New ResultReporteMinisterio()
            objVehiculo.Reportar_Manifiesto(Tipos_Procesos.FORMATO_INGRESO, ResultReporte)
            Return ResultReporte
        End Function

        Public Function ConsultaAceptacionElectronica(Empresa As Empresas, acceso As Acceso, solicitudConsulta As Solicitud, Manifiesto As AceptacionElectronicaRNDC) As ResultAceptacionMinisterio
            Dim objVehiculo As New GestionReporteRNDC(acceso, solicitudConsulta, Manifiesto, Empresa)
            Dim ResultReporte As ResultAceptacionMinisterio = New ResultAceptacionMinisterio()
            objVehiculo.Consultar_Aceptacion_Electronica_Manifiesto(Tipos_Procesos.FORTMATO_CONSULTA_PROCESOS, ResultReporte)
            Return ResultReporte
        End Function

        Public Function ReportarCumplidoManifiesto(Empresa As Empresas, acceso As Acceso, solicitudConsulta As Solicitud, Manifiesto As ManifiestoRNDC) As ResultReporteMinisterio
            Dim objVehiculo As New GestionReporteRNDC(acceso, solicitudConsulta, Manifiesto, Empresa)
            Dim ResultReporte As ResultReporteMinisterio = New ResultReporteMinisterio()
            objVehiculo.Reportar_Cumplido_Manifiesto(Tipos_Procesos.FORMATO_INGRESO, ResultReporte)
            Return ResultReporte
        End Function
        Public Function ReportarCumplidoAnuladoManifiesto(Empresa As Empresas, acceso As Acceso, solicitudConsulta As Solicitud, Manifiesto As ManifiestoRNDC) As ResultReporteMinisterio
            Dim objVehiculo As New GestionReporteRNDC(acceso, solicitudConsulta, Manifiesto, Empresa)
            Dim ResultReporte As ResultReporteMinisterio = New ResultReporteMinisterio()
            objVehiculo.Reportar_Cumplido_Anulado_Manifiesto(Tipos_Procesos.FORMATO_INGRESO, ResultReporte)
            Return ResultReporte
        End Function
        Public Function ReportarAnuladoManifiesto(Empresa As Empresas, acceso As Acceso, solicitudConsulta As Solicitud, Manifiesto As ManifiestoRNDC) As ResultReporteMinisterio
            Dim objVehiculo As New GestionReporteRNDC(acceso, solicitudConsulta, Manifiesto, Empresa)
            Dim ResultReporte As ResultReporteMinisterio = New ResultReporteMinisterio()
            objVehiculo.Reportar_Anulado_Manifiesto(Tipos_Procesos.FORMATO_INGRESO, ResultReporte)
            Return ResultReporte
        End Function

        ''Constructor de actualización estado
        ''Actualiza remesa
        Public Sub ActualizarResultadoReporteRNDC(conexion As DataBaseFactory, ResultadoReporte As ResultReporteMinisterio, CodigoEmpresa As Long, Optional Remesa As Remesas = Nothing)

            Dim resultado As IDataReader
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_ENRE_Numero", Remesa.Numero)
            conexion.AgregarParametroSQL("@par_Resultado_Reporte_Remesa", ResultadoReporte.NumConfirmacion)
            conexion.AgregarParametroSQL("@par_Mensaje_Reporte_Remesa", ResultadoReporte.StrError)
            resultado = conexion.ExecuteReaderStoreProcedure("gsp_actualizar_Resultado_Reporte_Remesa")
            resultado.Close()
        End Sub
        Public Sub ActualizaroReporteRNDCTercero(conexion As DataBaseFactory, CodigoEmpresa As Long, CodigoTercero As Integer)

            Dim resultado As IDataReader
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", CodigoTercero)
            resultado = conexion.ExecuteReaderStoreProcedure("gsp_actualizar_reporte_tercero")
            resultado.Close()
        End Sub
        Public Sub ActualizaroReporteRNDCVehiculo(conexion As DataBaseFactory, CodigoEmpresa As Long, CodigoVehiculo As Integer)

            Dim resultado As IDataReader
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_VEHI_Codigo", CodigoVehiculo)
            resultado = conexion.ExecuteReaderStoreProcedure("gsp_actualizar_reporte_vehiculo")
            resultado.Close()
        End Sub
        Public Sub ActualizaroReporteRNDCSemirremolque(conexion As DataBaseFactory, CodigoEmpresa As Long, CodigoSemirremolque As Integer)

            Dim resultado As IDataReader
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_SEMI_Codigo", CodigoSemirremolque)
            resultado = conexion.ExecuteReaderStoreProcedure("gsp_actualizar_reporte_semirremolque")
            resultado.Close()
        End Sub
        Public Sub ActualizarResultadoReporteCumplidoRemesaRNDC(conexion As DataBaseFactory, ResultadoReporte As ResultReporteMinisterio, CodigoEmpresa As Long, Optional Remesa As Remesas = Nothing)

            Dim resultado As IDataReader
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_ENRE_Numero", Remesa.Numero)
            conexion.AgregarParametroSQL("@par_Resultado_Reporte_Remesa", ResultadoReporte.NumConfirmacion)
            conexion.AgregarParametroSQL("@par_Mensaje_Reporte_Remesa", ResultadoReporte.StrError)
            resultado = conexion.ExecuteReaderStoreProcedure("gsp_actualizar_Resultado_Reporte_Cumplido_Remesa")
            resultado.Close()
        End Sub
        Public Sub ActualizarResultadoReporteCumplidoInicialRemesaRNDC(conexion As DataBaseFactory, ResultadoReporte As ResultReporteMinisterio, CodigoEmpresa As Long, Optional Remesa As Remesas = Nothing)

            Dim resultado As IDataReader
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_ENRE_Numero", Remesa.Numero)
            conexion.AgregarParametroSQL("@par_Resultado_Reporte_Remesa", ResultadoReporte.NumConfirmacion)
            conexion.AgregarParametroSQL("@par_Mensaje_Reporte_Remesa", ResultadoReporte.StrError)
            resultado = conexion.ExecuteReaderStoreProcedure("gsp_actualizar_Resultado_Reporte_Cumplido_Inicial_Remesa")
            resultado.Close()
        End Sub
        Public Sub ActualizarResultadoReporteAnulacionRemesaRNDC(conexion As DataBaseFactory, ResultadoReporte As ResultReporteMinisterio, CodigoEmpresa As Long, Optional Remesa As Remesas = Nothing)

            Dim resultado As IDataReader
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_ENRE_Numero", Remesa.Numero)
            conexion.AgregarParametroSQL("@par_Resultado_Reporte_Remesa", ResultadoReporte.NumConfirmacion)
            conexion.AgregarParametroSQL("@par_Mensaje_Reporte_Remesa", ResultadoReporte.StrError)
            resultado = conexion.ExecuteReaderStoreProcedure("gsp_actualizar_Resultado_Reporte_anulacion_Remesa")
            resultado.Close()
        End Sub
        Public Sub ActualizarResultadoReporteAnulacionCumpldioRemesaRNDC(conexion As DataBaseFactory, ResultadoReporte As ResultReporteMinisterio, CodigoEmpresa As Long, Optional Remesa As Remesas = Nothing)

            Dim resultado As IDataReader
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_ENRE_Numero", Remesa.Numero)
            conexion.AgregarParametroSQL("@par_Resultado_Reporte_Remesa", ResultadoReporte.NumConfirmacion)
            conexion.AgregarParametroSQL("@par_Mensaje_Reporte_Remesa", ResultadoReporte.StrError)
            resultado = conexion.ExecuteReaderStoreProcedure("gsp_actualizar_Resultado_Reporte_anulacion_cumplido_Remesa")
            resultado.Close()
        End Sub
        ''Actualiza Manifiesto
        Public Sub ActualizarResultadoReporteRNDC(conexion As DataBaseFactory, ResultadoReporte As ResultReporteMinisterio, CodigoEmpresa As Long, Optional Manifiesto As Manifiesto = Nothing)

            Dim resultado As IDataReader
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_ENMC_Numero", Manifiesto.Numero)
            conexion.AgregarParametroSQL("@par_Resultado_Reporte_Remesa", ResultadoReporte.NumConfirmacion)
            conexion.AgregarParametroSQL("@par_Mensaje_Reporte_Remesa", ResultadoReporte.StrError)
            conexion.AgregarParametroSQL("@par_Codigo_Seguridad_Manifiesto_Electronico", ResultadoReporte.seguridadqr)
            conexion.AgregarParametroSQL("@par_Observacion_Manifiesto_Electronico", ResultadoReporte.observacionesqr)
            resultado = conexion.ExecuteReaderStoreProcedure("gsp_actualizar_Resultado_Reporte_Manifiesto")
            resultado.Close()
        End Sub

        Public Sub ActualizarAceptacionElectronicaManifiestoRNDC(conexion As DataBaseFactory, ResultadoReporte As ResultAceptacionMinisterio, CodigoEmpresa As Long, Optional Manifiesto As Manifiesto = Nothing)

            Dim resultado As IDataReader
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_ENMC_Numero", Manifiesto.Numero)
            conexion.AgregarParametroSQL("@par_Resultado_Aceptacion_Electronica", IIf(ResultadoReporte.NumConfirmacion = 0, DBNull.Value, ResultadoReporte.NumConfirmacion))
            conexion.AgregarParametroSQL("@par_Mensaje_Aceptacion_Electronica", ResultadoReporte.StrError)
            conexion.AgregarParametroSQL("@par_Fecha_Aceptacion_Electronica", ResultadoReporte.fechaAceptacion.Substring(0, 19))
            resultado = conexion.ExecuteReaderStoreProcedure("gsp_actualizar_Resultado_Aceptacion_Electronica_Manifiesto")
            resultado.Close()
        End Sub
        Public Sub ActualizarResultadoReporteCumplidoRNDC(conexion As DataBaseFactory, ResultadoReporte As ResultReporteMinisterio, CodigoEmpresa As Long, Optional Manifiesto As Manifiesto = Nothing)

            Dim resultado As IDataReader
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_ENMC_Numero", Manifiesto.Numero)
            conexion.AgregarParametroSQL("@par_Resultado_Reporte_Remesa", ResultadoReporte.NumConfirmacion)
            conexion.AgregarParametroSQL("@par_Mensaje_Reporte_Remesa", ResultadoReporte.StrError)
            resultado = conexion.ExecuteReaderStoreProcedure("gsp_actualizar_Resultado_Reporte_Cumplido_Manifiesto")
            resultado.Close()
        End Sub
        Public Sub ActualizarResultadoReporteAnulacionCumplidoRNDC(conexion As DataBaseFactory, ResultadoReporte As ResultReporteMinisterio, CodigoEmpresa As Long, Optional Manifiesto As Manifiesto = Nothing)

            Dim resultado As IDataReader
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_ENMC_Numero", Manifiesto.Numero)
            conexion.AgregarParametroSQL("@par_Resultado_Reporte_Remesa", ResultadoReporte.NumConfirmacion)
            conexion.AgregarParametroSQL("@par_Mensaje_Reporte_Remesa", ResultadoReporte.StrError)
            resultado = conexion.ExecuteReaderStoreProcedure("gsp_actualizar_Resultado_Reporte_Anulacion_Cumplido_Manifiesto")
            resultado.Close()
        End Sub
        Public Sub ActualizarResultadoReporteAnulacionManifiestoRNDC(conexion As DataBaseFactory, ResultadoReporte As ResultReporteMinisterio, CodigoEmpresa As Long, Optional Manifiesto As Manifiesto = Nothing)

            Dim resultado As IDataReader
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_ENMC_Numero", Manifiesto.Numero)
            conexion.AgregarParametroSQL("@par_Resultado_Reporte_Remesa", ResultadoReporte.NumConfirmacion)
            conexion.AgregarParametroSQL("@par_Mensaje_Reporte_Remesa", ResultadoReporte.StrError)
            resultado = conexion.ExecuteReaderStoreProcedure("gsp_actualizar_Resultado_Reporte_Anulacion_Manifiesto")
            resultado.Close()
        End Sub
    End Class

End Namespace