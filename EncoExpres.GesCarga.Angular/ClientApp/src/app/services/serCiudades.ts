import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// Interfaces
import { Respuesta, Filtro, Ciudad } from '../interfaces/ifaGeneral';
// Observable
import { Observable } from 'rxjs';
// Constantes
const UrlApiConsultar = "http://localhost:50395/api/v1/Ciudades/Consultar";
const UrlApiGuardar = "http://localhost:50395/api/v1/Ciudades/Guardar";
const UrlApiEliminar = "http://localhost:50395/api/v1/Ciudades/Anular";
const UrlApiObtener = "http://localhost:50395/api/v1/Ciudades/Obtener";
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'my-auth-token'
  })
}

@Injectable({
  providedIn: 'root'
})
export class serCiudades {
     
  constructor(protected http: HttpClient) {}

  public apiConsultar(objFiltro: Filtro): Observable<Respuesta>{

    return this.http.post<Respuesta>(UrlApiConsultar,
      {
        CodigoEmpresa: objFiltro.CodigoEmpresa,
        Codigo: objFiltro.Codigo,
        CodigoAlterno: objFiltro.CodigoAlterno,
        Nombre: objFiltro.Nombre,
        Estado: objFiltro.Estado,
        Pagina: objFiltro.Pagina,
        RegistrosPagina: objFiltro.RegistrosPagina
      }, httpOptions);

  }

  public apiGuardar(objCiudad: Ciudad): Observable<Respuesta> {

    return this.http.post<Respuesta>(UrlApiGuardar,
      {
        CodigoEmpresa: objCiudad.CodigoEmpresa,
        Codigo: objCiudad.Codigo,
        CodigoCiudad: objCiudad.CodigoCiudad,
        CodigoAlterno: objCiudad.CodigoAlterno,
        Nombre: objCiudad.Nombre,
        Departamento: objCiudad.Departamento,
        Estado: objCiudad.Estado
      }, httpOptions);

  }

  public apiEliminar(objCiudad: Ciudad): Observable<Respuesta> {

    return this.http.post<Respuesta>(UrlApiEliminar,
      {
        CodigoEmpresa: objCiudad.CodigoEmpresa,
        Codigo: objCiudad.Codigo
      }, httpOptions);

  }

}
